// Queez show/hide buttons
function toggleSolution(button) {
  const solution = button.nextElementSibling;
  const listItems = button.previousElementSibling.querySelectorAll('li');

  // Toggle the visibility of the solution
  if (solution.style.display === 'none' || solution.style.display === '') {
    solution.style.display = 'block';
    button.textContent = 'Hide Solution';

    // Highlight the correct answer
    listItems.forEach(item => {
      if (item.classList.contains('q-good')) {
        item.classList.remove('q-good');
        item.classList.add('q-good-on');
      }
    });
  } else {
    solution.style.display = 'none';
    button.textContent = 'Reveal Solution';

    // Revert the highlight
    listItems.forEach(item => {
      if (item.classList.contains('q-good-on')) {
        item.classList.remove('q-good-on');
        item.classList.add('q-good');
      }
    });
  }
}
