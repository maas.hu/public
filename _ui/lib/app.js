// Generate main element of the page
const MAINBODY = document.querySelector('body');

// Invoke Google Search code
const GCSE_SCR = document.createElement('script');
GCSE_SCR.setAttribute('src', 'https://cse.google.com/cse.js?cx=cbc1575c6dccfc56f');
MAINBODY.appendChild(GCSE_SCR);

const TOPNAV = '<a href="/sysadmin">System Administration</a>\
<a href="/prog">Programming</a>\
<a href="/multimedia">Computer Graphics and Multimedia</a>\
<a href="/electronics">Electronics</a>\
<a href="/linguistics">Linguistics</a>';

// Generate the basic elements of the page
const HEADER = document.createElement('header');
  MAINBODY.appendChild(HEADER);
  const DIV_TOPNAV = document.createElement('div');
    DIV_TOPNAV.setAttribute('id', 'topnav');
    DIV_TOPNAV.innerHTML = TOPNAV;
    HEADER.appendChild(DIV_TOPNAV);
  const A_SBUTT = document.createElement('a');
    A_SBUTT.setAttribute('href', '/');
    HEADER.appendChild(A_SBUTT);
  const DIV_SBUTT = document.createElement('div');
    DIV_SBUTT.setAttribute('id', 'sbutt');
    A_SBUTT.appendChild(DIV_SBUTT);
  const DIV_MBUTT = document.createElement('div');
    DIV_MBUTT.setAttribute('id', 'mbutt');
    HEADER.appendChild(DIV_MBUTT);
  const DIV_FMENU = document.createElement('div');
    DIV_FMENU.setAttribute('id', 'fmenu');
    HEADER.appendChild(DIV_FMENU);
const ARTICLE = document.createElement('article');
  MAINBODY.appendChild(ARTICLE);
const FOOTER = document.createElement('footer');
  MAINBODY.appendChild(FOOTER);
// <a href="#0" class="cd-top">Top</a>
const CDTOP = document.createElement('a');
  CDTOP.setAttribute('href', '#0');
  CDTOP.setAttribute('class', 'cd-top');
  CDTOP.innerHTML = 'Top';
  MAINBODY.appendChild(CDTOP);

// Speak buttons
let speak = (sID) => {
  let but = document.createElement('audio');
  but.setAttribute('src', 'audio/'+ sID +'.mp3');
  but.play();
}

// Assign an ID to the current document
const DIR = window.location.pathname.split('/').slice(0, -1).join('');
let docID = 'start';
if (DIR) {
  docID = DIR;
}

// Get the last modified date of the current document
let xhReq = new XMLHttpRequest();
xhReq.open('HEAD', '/_pages/md/'+ docID +'.md', false);
xhReq.send(null);
let lastModified = xhReq.getResponseHeader('Last-Modified');
let dateObj = new Date(lastModified);
let timeStamp = new Intl.DateTimeFormat('sv-SE', { month: '2-digit', day: '2-digit', year: 'numeric', hour: 'numeric', minute: 'numeric' }).format(dateObj);

// Some pages need special formatting
// Set "Disqus" for pages, where commenting and suggesting is allowed
if ((docID === 'contact') ||
    (docID === 'electronicscircuitschristmas' ) ||
    (docID === 'electronicscircuitsgates' ) ||
    (docID === 'linguisticsfinnish') ||
    (docID === 'progbash') ||
    (docID === 'sysadminlinuxnetwork') ||
    (docID === 'sysadminlinuxstorage') ||
    (docID === 'sysadminqemu') ||
    (docID === 'sysadminxen')) {
  const DIV_DISQ = document.createElement('div');
    DIV_DISQ.setAttribute('id', 'disqus_thread');
    MAINBODY.appendChild(DIV_DISQ);
    MAINBODY.insertBefore(DIV_DISQ, FOOTER);
  // Disqus code
  (() => {
    const DISQ_SCR = document.createElement('script');
    DISQ_SCR.setAttribute('src', 'https://maas-hu.disqus.com/embed.js');
    DISQ_SCR.setAttribute('data-timestamp', +new Date());
    DIV_DISQ.appendChild(DISQ_SCR);
  })();
}

// Set frame and background for all the images for "/reference/vectorart" page
if (docID === 'referencevectorart') {
  const IMG_V = document.createElement('style');
    IMG_V.innerHTML = 'img { padding: 20px; background-color: #008c64; } \
      @media screen and (max-width: 1081px) { article img { max-width: 290px; } }';
    MAINBODY.appendChild(IMG_V);
}

// Convert the .md page to HTML and show it
let conv = new showdown.Converter();
conv.setOption('noHeaderId', 'true');
fetch('/_ui/lib/app_labels.json')
  .then(response => response.json())
  .then((data) => {
    for (let i of data) {
      if (i.docID == docID) {
        document.title = i.title;
      }
    }
  });
fetch('/_pages/md/'+ docID +'.md')
  .then(response => response.text())
  .then((data) => {
    let html = conv.makeHtml(data);
    document.querySelector('article').innerHTML = html;
    document.querySelector('h6').innerText = 'Last updated: '+timeStamp;
  });

// Generate the hamburger menu and get it working
fetch('/_ui/lib/app_menu.json')
  .then(response => response.json())
  .then((data) => {
//    const IFRAME_DDG = document.createElement('iframe');
//    IFRAME_DDG.setAttribute('src', 'https://duckduckgo.com/search.html?width=137&duck=yes&site=maas.hu&bgcolor=c6e5dc');
//    IFRAME_DDG.setAttribute('style', 'overflow:hidden;margin:0;padding:0;width:270px;height:60px;');
//    IFRAME_DDG.setAttribute('frameborder', '0');
//    DIV_FMENU.appendChild(IFRAME_DDG);
    const DIV_GCSE = document.createElement('div');
    DIV_GCSE.setAttribute('class', 'gcse-search');
    DIV_FMENU.appendChild(DIV_GCSE);
    for (let i of data) {
      if (( i.href === '..' ) && ( docID === 'start' )) {
        null;
      } else {
        const DIV_MLINK = document.createElement('div');
        DIV_MLINK.setAttribute('class', 'mlink');
        DIV_FMENU.appendChild(DIV_MLINK);
        const ALINK = document.createElement('a');
        ALINK.setAttribute('href', i.href);
        ALINK.innerHTML = i.label;
        DIV_MLINK.appendChild(ALINK);
      }
    }
  });
DIV_MBUTT.onclick = () => {
  if (getComputedStyle(DIV_MBUTT).backgroundPosition === '50% 0%') {
    DIV_MBUTT.style.backgroundPosition = '50% 100%';
    DIV_FMENU.style.transform = 'scale(1,1)';
    DIV_FMENU.style.visibility = 'visible';
  }
  else {
    DIV_MBUTT.style.backgroundPosition = '50% 0%';
    DIV_FMENU.style.transform = 'scale(0,0)';
    DIV_FMENU.style.visibility = 'hidden';
  }
}

// Create the Mission block
const DIV_MI = document.createElement('div');
  DIV_MI.setAttribute('id', 'div_mi');
  FOOTER.appendChild(DIV_MI);
  const H2_MI = document.createElement('h2');
    H2_MI.innerHTML = 'About';
    DIV_MI.appendChild(H2_MI);
  const P_MI = document.createElement('p');
    P_MI.innerHTML = '<a href="/about">maas.hu - Mihály as a Service</a><br> \
    © 2017 maas.hu';
    DIV_MI.appendChild(P_MI);
  const P_OT = document.createElement('p');
    P_OT.innerHTML = '<a href="/sitemap">🗺️ Site Map</a>';
    DIV_MI.appendChild(P_OT);

// Create the Powered By block
const DIV_PB = document.createElement('div');
  DIV_PB.setAttribute('id', 'div_pb');
  FOOTER.appendChild(DIV_PB);
  const H2_PB = document.createElement('h2');
    H2_PB.innerHTML = 'Powered by';
    DIV_PB.appendChild(H2_PB);
fetch('/_ui/lib/app_poweredby.json')
  .then(response => response.json())
  .then((data) => {
    for (let i of data) {
      const DIV_PBIMG = document.createElement('div');
      DIV_PBIMG.setAttribute('id', i.id);
      DIV_PB.appendChild(DIV_PBIMG);
      const A_PBIMG = document.createElement('a');
      A_PBIMG.setAttribute('href', i.href);
      A_PBIMG.setAttribute('title', i.title);
      A_PBIMG.setAttribute('target', '_blank');
      DIV_PBIMG.appendChild(A_PBIMG);
    }
  });
