document.title = "Restricted Page";
document.addEventListener("DOMContentLoaded", function () {
  const keyword = "OpenIt";

  if (!localStorage.getItem("accessGranted")) {
    const userInput = prompt("Please enter the keyword to access this page:");

    if (userInput === keyword) {
      localStorage.setItem("accessGranted", true);
    } else {
      alert("Access denied.");
      window.location.href = "/";
    }
  }
});
