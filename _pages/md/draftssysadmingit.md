# ![alt text](/_ui/img/icons/git_m.svg) Git, GitLab, GitLab CI/CD

## Git in General

- ![alt text](/_ui/img/icons/git_s.svg) [Basics](/sysadmin/git/basics)

## GitLab

- ![alt text](/_ui/img/icons/gitlab_s.svg) [GitLab](/sysadmin/git/gitlab)
- ![alt text](/_ui/img/icons/gitlab_s.svg) [GitLab CI/CD](/sysadmin/git/gitlab_cicd)
