# Ⓜ️ MarkDown

<div style="text-align: right;"><h7>Posted: 2024-04-12 10:46</h7></div>
###### .

![alt_text](vscode-md.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Paragraphs](#2)
3. [Headings](#3)
4. [Lists and Numbered Lists](#4)
5. [Emphasis and Strikethrough](#5)
6. [Blockquote](#6)
7. [Links](#7)
8. [Images](#8)
9. [Code](#9)
10. [Tables](#10)
11. [MarkDown Viewers](#11)


<a name="1"></a>
## 1. Introduction

**MarkDown** is a lightweight markup language designed to be easy to read and write, while still providing enough power to create well-structured documents. It was created to allow easy formatting in plain text files without needing to know HTML. With MarkDown, you can create structured content that is easy to convert into HTML, yet readable in its original form. One of its advantages is that it can be used without knowledge of HTML, yet it still allows for sophisticated formatting. For example the Reddit social platform allows users to enrich their posts and comments in MarkDown.


<a name="2"></a>
## 2. Paragraphs

Creating paragraphs in MarkDown is straightforward: simply insert a blank line between two blocks of text. To create a line break without starting a new paragraph, add two spaces at the end of the line.

```
This is a paragraph. 

This is another paragraph.

This is the first line of a paragraph.<space><space>
This is another line of it, without a new paragraph.
```


<a name="3"></a>
## 3. Headings

Headings are crucial for organizing content, especially in long articles. They help guide the reader through sections and make the document easier to navigate.

Headings in MarkDown are defined using the `#` symbol. The number of `#` symbols determines the level of the heading.

```
# This is an "h1" heading
## This is an "h2" heading
### This is an "h3" heading
#### This is an "h4" heading
##### This is an "h5" heading
```

The rendered result on how these headings look like of course heavly relies on your current CSS formatting of your page and/or the applied MarkDown reader itself. Here's how this website's CSS enforces them:

<div class="preview">
<h1>This is an "h1" heading</h1>
<h2>This is an "h2" heading</h2>
<h3>This is an "h3" heading</h3>
<h4>This is an "h4" heading</h4>
<h5>This is an "h5" heading</h5>
</div>


<a name="4"></a>
## 4. Lists and Numbered Lists

### 4.1 Unordered Lists

Unordered lists are created using `-` or `*` followed by a space.

```
- First list item
- Second list item
```

<div class="preview">
<ul>
  <li>First list item</li>
  <li>Second list item</li>
</ul>
</div>

Nested lists can also be created by indenting sub-items:

```
- First list item
    - Sub-item 1
    - Sub-item 2
- Second list item
```

<div class="preview">
<ul>
  <li>First list item</li>
  <ul>
    <li>Sub-item 1</li>
    <li>Sub-item 2</li>
  </ul>
<li>Second list item</li>
</ul>
</div>

### 4.2 Numbered Lists

Numbered lists are created by starting each line with a number followed by a period.

```
1. First item
2. Second item
3. Third item
```

<div class="preview">
<ol>
  <li>First item</li>
  <li>Second item</li>
  <li>Third item</li>
</ol>
</div>


<a name="5"></a>
## 5. Emphasis and Strikethrough

Emphasis is often used to highlight key terms or concepts within a document, making it more engaging for the reader. You can use `*` or `_` for italics, `**` or `__` for bold, and `***` or `___` for bold and italics.

```
This is *italic*, this is **bold**, and this is ***bold and italic***.
```

<div class="preview">
<p>This is <i>italic</i>, this is <b>bold</b>, and this is <b><i>bold and italic</i></b></p>
</div>

You can also create strikethrough text using `~~` around the text.


<a name="6"></a>
## 6. Blockquote

You can create blockquotes by using the `>` symbol before text.

```
>One must still have chaos in oneself to be able to give birth to a dancing star.

— Friedrich Nietzsche
```

<div class="preview">
  <blockquote>
One must still have chaos in oneself to be able to give birth to a dancing star.
  </blockquote>
<p>— Friedrich Nietzsche</p>
</div>


<a name="7"></a>
## 7. Links

Links are essential for referencing external resources, documentation, or related topics, and they enhance the interactivity of your document.

Links are created using square brackets `[]` for the link text and parentheses `()` for the URL.

```
Visit the official MarkDown guide [here](https://www.markdownguide.org/cheat-sheet/).
```

<div class="preview">
<p>Visit the official MarkDown guide <a href="https://www.markdownguide.org/cheat-sheet/">here</a>.</p>
</div>


<a name="8"></a>
## 8. Images

Images are commonly used to illustrate concepts, provide examples, or simply enrich the content of your document. They help readers visually connect with the text.

In MarkDown, images are embedded using a similar syntax to links, with an exclamation mark `!` before the square brackets. The `alt text` is optional but recommended.

Simple example as it's most commonly used:

```
![alt_text](/_ui/img/imgfx/office_desk.png)
```

The same, with a tooltip:

```
![alt_text](/_ui/img/imgfx/office_desk.png "Hover text")
```

<div class="preview">
<img src="/_ui/img/imgfx/office_desk.png" title="Hover text">
</div>


<a name="9"></a>
## 9. Code

Code blocks are often used to display configuration examples, scripts, or command-line instructions in a readable format.

### 9.1 Inline Code

Inline code is placed between single backticks `` ` ``. 

<div class="preview">
<p>The <code>format c:</code> command formats the C: drive.</p>
</div>

### 9.2 Code Blocks

Code blocks can be created using triple backticks `` ``` `` or indentation. 

<pre>
&#96;&#96;&#96;
# date "+%Y%m%d"
20201231
&#96;&#96;&#96;
</pre>

<div class="preview">
<pre>
# date "+%Y%m%d"
20201231
</pre>
</div>


<a name="10"></a>
## 10. Tables

Tables are excellent for presenting data in a structured format, making it easier for readers to digest information.

In MarkDown, tables are created using pipes `|` and hyphens `-` for the column headers.

```
| Animal | Weight | Color |
|--------|--------|-------|
| Dog    | 20 kg  | Brown |
| Cat    | 10 kg  | Black |
| Horse  | 600 kg | Beige |
```

<div class="preview">
<table class="tan">
  <tr>
    <th>Animal</th><th>Weight</th><th>Color</th>
  </tr>
  <tr>
    <td>Dog</td><td>20 kg</td><td>Brown</td>
  </tr>
  <tr>
    <td>Cat</td><td>10 kg</td><td>Black</td>
  </tr>
  <tr>
    <td>Horse</td><td>600 kg</td><td>Beige</td>
  </tr>
</table>
</div>

For more complex tables, it's very useful to use an online [Excel Table -> MD Converter](https://thisdavej.com/copy-table-in-excel-and-paste-as-a-markdown-table/).


<a name="11"></a>
## 11. MarkDown Viewers

MarkDown files can be viewed in text editors like **VSCode** with the Markdown Preview Enhanced extension. For an even better viewing experience, you can associate `.md` files with your favorite browser. There are several extensions available for browsers such as **Markdown Viewer Webext** for Firefox:

![alt_text](markdown_viewer_webext.png)

With such an extension, you can even apply custom CSS to make the appearance of your MarkDown files more tailored to your personal preferences.

Markdown is not only readable as plain text but can also be beautifully displayed in web browsers or editors.

I hope you now have a deeper understanding of how to use MarkDown effectively! Happy writing!
