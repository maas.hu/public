# ![alt text](/_ui/img/icons/statsd_m.svg) StatsD, Graphite, Grafana

###### .

>Ezek is még csak jegyzetek, szóval "szerkesztés alatt"...

## Tartalomjegyzék

1. [Bevezetés](#1)

...

<a name="1"></a>
## 1. Bevezetés

...

<a name="2"></a>
## 2. StatsD

A StatsD előfeltétele a Node.js megléte. Itt most [a másik fejezetben látható módon](/learning/it/prog/javascript/nodejs/#2) került ez telepítésre, sima arhívumból az `/opt` alá, manuálisan beállítva a `$PATH` változót: 

```ksh
# which node
/opt/Node.js/bin/node

# node -v
v16.10.0
```

### 2.1 Letöltés, telepítés

Alakítsuk ki a telepítést a szoftver GitHub-os repóját használva, az `/opt` alá, készítsünk másolatot a példaconfigból és indítsuk is el: 

```ksh
# cd /opt

# git clone https://github.com/etsy/statsd.git
Cloning into 'statsd'...
remote: Enumerating objects: 3541, done.
remote: Total 3541 (delta 0), reused 0 (delta 0), pack-reused 3541
Receiving objects: 100% (3541/3541), 801.94 KiB | 3.01 MiB/s, done.
Resolving deltas: 100% (1988/1988), done.

# ls -la statsd/
total 244
...
-rw-r--r--.  1 root root  8002 Sep 26 06:06 exampleConfig.js
...
-rw-r--r--.  1 root root 15626 Sep 26 06:06 stats.js
...

# cd statsd

# cp -p exampleConfig.js Config.js

# cd

# node /opt/statsd/stats.js /opt/statsd/Config.js
26 Sep 06:09:37 - [7346] reading config file: /opt/statsd/Config.js
26 Sep 06:09:37 - server is up INFO
```

A daemon tehát fut; ezt egy `Ctrl+C`-vel bármikor le is állíthatjuk. 

### 2.2 Config

Ez itt egy minimális config és debug módba állítás: 

```ksh
# vi /opt/statsd/Config.js
...
{
//  graphitePort: 2003
//, graphiteHost: "graphite.example.com"
  debug: "True"
, port: 8125
, backends: [ "./backends/graphite" ]
}

# node /opt/statsd/stats.js /opt/statsd/Config.js
26 Sep 06:13:28 - [7366] reading config file: /opt/statsd/Config.js
26 Sep 06:13:28 - DEBUG: Loading server: ./servers/udp
26 Sep 06:13:28 - server is up INFO
26 Sep 06:13:28 - DEBUG: Loading backend: ./backends/graphite
26 Sep 06:13:38 - DEBUG: numStats: 3
```

## 2.3 Metrikatípusok

...

```
<bucket>:<value>|<metric_type>[|@<sampling_rate>]
```

...

Az alaptelepítéssel érkező metrikatípusok: 

- **Counter**: növeli vagy csökkenti a bucket értékét (pl.: `email_sending.emails.sent:300|c`)  
- **Timer**: valamilyen folyamat végrehajtásának időtartama (pl. `email_sending.render.recommendations:560|ms`)  
- **Gauge**: tetszőleges, rögzítendő érték, ami frissítéskor nem resetelődik automatikusan (pl. `email_sending.render.num_recommendations:5|g` vagy `email_sending.render.num_recommendations:+1|g`)  
- **Set**:  

A mintavételezés mértéke is meghatározható `email_sending.render.recommendations:560|ms|@0.1`

### 2.4 Tesztelés

Shellből metrika küldése (az `nc` (NetCat) parancshoz az "nmap-ncat" csomag megléte szükséges, ami gyorsan telepíthető, ha kell): 

```ksh
# echo "hello:1|c" | nc -w 1 -u 127.0.0.1 8125
```

Egy tisztességesebb teszthez az előfeltételek (Docker-es, előre telepített alkalmazásokkal együtt futó konténerben): 

- egy VM-ben fusson a Docker daemon (telepítés, alapconfig egy másik bejegyzésben)
- Python (a példában a Python 3.9, parancsa a `python3`)  
- a statd modul a Python3-hoz, `pip3`-al telepítve  

Ez mind ugyanabban a VM-ben történik.

#### 2.4.1 Előre telepített Grafana/Graphite konténer lehúzása, indítása

```ksh
# docker run -id -p 8000:8000 -p 3000:3000 -p 8125:8125/udp kamon/grafana_graphite
Unable to find image 'kamon/grafana_graphite:latest' locally
latest: Pulling from kamon/grafana_graphite
402ae6c1c9cf: Pull complete
...
Status: Downloaded newer image for kamon/grafana_graphite:latest

# docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                                                                                                                                                  NAMES
bf9b15a02786   kamon/grafana_graphite   "/usr/bin/supervisord"   44 minutes ago   Up 44 minutes   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp, 80-81/tcp, 8126/tcp, 0.0.0.0:8000->8000/tcp, :::8000->8000/tcp, 0.0.0.0:8125->8125/udp, :::8125->8125/udp   vigorous_kepler
```

#### 2.4.2 Python és egy tesztprogram telepítése

```ksh
# dnf install python39
...
Install  6 Packages
...
Total download size: 13 M
Installed size: 45 M
Is this ok [y/N]: y
...
Complete!

# pip3 install statsd
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Collecting statsd
  Downloading statsd-3.3.0-py2.py3-none-any.whl (11 kB)
Installing collected packages: statsd
Successfully installed statsd-3.3.0
```

A teszteléshez szükséges Python programocska, a `statsd_script.py` tartalma: 

```
import random
import statsd
import time

HOST = 'localhost'
PORT = 8125

client = statsd.StatsClient(HOST, PORT)

while True:
    random.seed()
    random_sleep_interval = random.randint(5, 15)
    print('Got random interval to sleep: {}'.format(random_sleep_interval))

    time.sleep(random_sleep_interval)
    client.incr('statsd_script.sleep_calls')
```

Indítás: 

```ksh
# python3 statsd_script.py
Got random interval to sleep: 7
Got random interval to sleep: 5
...
```

## 2.4.3 Grafana kipróbálása

Mivel már a konténerünk is egy VM-ben fut, nyissunk egy SSH tunnelezett session-t rá (PuTTY-ban: "SSH" &rarr; "Tunnels" &rarr; `L3000:127.0.0.1:3000`), majd nyissunk meg böngészőben a [http://localhost:3000](http://localhost:3000) címet. Máris meg kell jelenjen a Grafana, amenyhez a felhasználónév és kezdeti jelszó egyaránt az "admin". 

Válasszuk itt a "Home" &rarr; "+ New Dashboard" funkciót, majd a "Graph" &rarr; "Panel Title" &rarr; "Edit" gombot. Állítsuk be az imént elindított metrikát: 

![alt_text](grafana_01.png "statsd_script")

<a name="3"></a>
# 3. Graphite

A Graphite összetevői: 

- Carbon  
- Whisper  
- Graphite-Web

...

<a name="4"></a>
# 4. Grafana

...

## 4.1 Telepítés

```ksh
# dnf install grafana
...
Install  2 Packages
...
Total download size: 49 M
Installed size: 201 M
Is this ok [y/N]: y
Complete!

# systemctl status grafana-server
● grafana-server.service - Grafana instance
   Loaded: loaded (/usr/lib/systemd/system/grafana-server.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: http://docs.grafana.org

# systemctl start grafana-server

# systemctl status grafana-server
● grafana-server.service - Grafana instance
   Loaded: loaded (/usr/lib/systemd/system/grafana-server.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-10-07 16:20:54 CEST; 1s ago
     Docs: http://docs.grafana.org
 Main PID: 1817 (grafana-server)
    Tasks: 13 (limit: 17861)
   Memory: 48.2M
...
```

## 4.2 Biztonsági alapbeállítások

Ezeket kell ellenőriznünk ill. beállítanunk rögtön a telepítés és a `grafana-server` indítása után: 

- az "admin" felhasználó jelszavát (erre az első bejelentkezéskor ("admin"/"admin") amúgyis kényszerítve vagyunk)  
- új user regisztrációjának tiltását (a `grafana.ini`-ben)  
- anonymous hozzáférés tiltását (a `grafana.ini`-ben)  

Ellenőrizzük tehát az alábbiak meglétét a Grafana fő konfigurációs fájljában: 

```ksh
# vi /etc/grafana/grafana.ini
...
[users]
# disable user signup / registration
allow_sign_up = false
...
[auth.anonymous]
# enable anonymous access
enabled = false
```

Ha módosítanunk kellett valamit, indítsuk újra a `grafana-server` service-t. Ezután nyissunk egy tunnelezett kapcsolatot a 3000-es port helyi átirányítására egy böngészőben és kezdjük meg a beállításokat. 

## 4.3 Data Source hozzáadása

Itt a másik VM-ben egyszerűen egy Docker konténert indítottam (ami egy baromság, mert helyileg is szépen be lehet állítani, de ez a luvnya állandóan a Docker-es megoldásokat erőlteti) így: 

```ksh
# docker run -d  --name graphite  --restart=always  -p 80:80  -p 2003-2004:2003-2004  -p 2023-2024:2023-2024  -p 8125:8125/udp  -p 8126:8126  graphiteapp/graphite-statsd
...
```

Az adatforrás tehát elvileg elérhető a sima 80-as HTTP porton a másik VM IP címén: 

```ksh
# ip a s
...
    inet 192.168.56.108/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
...
```

A "Home" screen-ben válasszuk az "Add your first data source" funkciót, majd ott a Graphite típust. Az URL-hez a másik VM IP-jét írhatjuk, majd "Save & Test". 

![alt_text](grafana_02.png "New Data Source")

Grafana paneltípusok: 

- Graph  
- Singlestat  
- Table  
- Dashboard list  
- Text  
- Heatmap  
- Alert list

A [play.grafana.org](https://play.grafana.org) oldalon szuperül eljátszogathatunk a különféle paneltípusokkal, azok finombeállításaival. 

...