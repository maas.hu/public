# ![alt text](/_ui/img/flags/epo_m.svg) Bevezető az eszperantó nyelvbe - Enkonduko al Esperanto lingvo

###### .

## Tartalomjegyzék

1. [Bevezető - Enkonduko](#1)
2. [Az ábécé - Alfabeto](#2)
3. [Alapmondatok, kifejezések - Bazaj frazoj, esprimoj](#3)
4. [Főnév - Substantivo](#4)
5. [Melléknév - Adjektivo](#5)
6. [Számnév - Numeralo](#6)
7. [Névmás - Pronomo](#7)
8. [Ige - Verbo](#8)
9. [Határozószó - Adverbo](#9)
10. [Névelő - Artikolo](#10)
11. [Elöljárószók - Prepozicoj](#11)
12. [Kötőszavak, módosítószók és egyéb határozószók - Konjunkcioj, ŝanĝado konjunkcioj kaj aliaj adverboj](#12)
13. [Állítás és tagadás - Aserto kaj negado](#13)
14. [Tabellaszavak - Tabelvortoj](#14)
15. [A szóképző rendszer - Sistemo de la vortfarado](#15)

<a name="1"></a>
## 1. Bevezető - Enkonduko

<img src="epo_book.png" title="" align="right" class="right">
Az **eszperantó** (eredeti nevén ***Lingvo Internacia***) egy nemzetektől független, mindenki számára egyenjogú nyelvhasználatot biztosító, élő, mesterséges, semleges nemzetközi segédnyelv. 

Anyanyelvként megközelítően 1000-en használják, a folyékonyan beszélők száma nagyjából 10.000-re tehető, az aktív használók száma pedig már százezres nagyságrendű. Mivel a világon milliók tanulták és tanulják ilyen-olyan szinten a nyelvet, ezért a pontos számok nem ismertek, de egyértelműen kijelenthető, hogy ez az abszolút legismertebb és legsikeresebb mesterséges nyelv. 

Nyelvtanrendszere rendkívül logikus, könnyen áttekinthető, nincsenek kivételek, mindezek ellenére rendkívül ritmikus, jól kiejthető. Az eszperantó nyelv tanulása segíti más nyelvekét is, ezt *propedeutikai* hatásnak nevezik.

A nyelvet a lengyel **Lazar Markovics Zamenhof** (eszperantóul **Lazaro Ludoviko Zamenhof**) zsidó származású lengyel szemészorvos, filológus alkotta meg. Zamenhof első, a nyelvet részletező kiadványa 1887-ben jelent meg először oroszul (Zamenhof eleve kétnyelvű volt: apja oroszul, anyja jiddis nyelven beszélt hozzá), majd később lengyelül, németül, franciául és angolul.  
A nyelv szókincse jórészt nyugat-európai nyelvekből származik, nyelvtana erős szláv hatást mutat. A szóelemek nem változnak, és szinte határtalanul kombinálhatók, hasonlóan a kínaihoz; belső szerkezete azonban inkább a ragozó nyelvekére hasonlít, mint a magyar, a török, a japán és a szuahéli.

Az eszperantót támogatja az UNESCO, és több ismert közéleti személyiség is kiállt mellette. Használják utazáshoz, levelezéshez, nemzetközi találkozókhoz, művészeti rendezvényeken és tudományos kongresszusokon, tudományos vitákhoz. Van irodalma, zenéje, színháza, nyomtatott és internetes sajtója, készültek eszperantó nyelvű filmek, és vannak eszperantó rádió- és tévéadások is. 

<a name="2"></a>
## 2. Az ábécé - Alfabeto

Az eszperantóban az írás és kiejtés teljesen fonetikus. Minden betűt mindig ugyanúgy ejtünk kivétel nélkül.
A következő táblázatban az eszperantó ábécéjének betűit és azok kiejtését láthatjuk ill. zárójelben a speciális betűk interneten legjobban elterjedt helyettesítő jelölését:

<table border="0px">
  <tr>
    <td width="130px" valign="top">
<p><b>A</b> - á<br>
<b>B</b> - bo<br>
<b>C</b> - co<br>
<b>&#264;</b> - cso (cx)<br>
<b>D</b> - do<br>
<b>E</b> - e<br>
<b>F</b> - fo</p>
    </td>
    <td width="130px" valign="top">
<p><b>G</b> - go<br>
<b>&#284;</b> - dzso (gx)<br>
<b>H</b> - ho<br>
<b>&#292;</b> - hho (hx)<br>
<b>I</b> - i<br>
<b>J</b> - jo<br>
<b>&#308;</b> - zso (jx)</p>
    </td>
    <td width="130px" valign="top">
<p><b>K</b> - ko<br>
<b>L</b> - lo<br>
<b>M</b> - mo<br>
<b>N</b> - no<br>
<b>O</b> - o<br>
<b>P</b> - po<br>
<b>R</b> - ro</p>
    </td>
    <td width="130px" valign="top">
<p><b>S</b> - szo<br>
<b>&#348;</b> - so (sx)<br>
<b>T</b> - to<br>
<b>U</b> - u<br>
<b>&#364;</b> - kettős vo (ux)<br>
<b>V</b> - vo<br>
<b>Z</b> - zo</p>
    </td>
  </tr>
</table>

A betűk helyes kiejtését meg is hallgathatod <a href="http://hu.lernu.net/enkonduko/lingvoprezento/alfabeto.php" target="_blank">itt</a>.

A **hangsúlyt mindig** az utolsó előtti szótag kapja. Erre kétféle változat van:

1. Megnyújtjuk a hangsúlyos magánhangzót, ha utána azt magánhangzó vagy másslahangzó követi (**nacio**, **bruo**, **heroo**, **balai**, **amo**, **homo**, **bela**, **epoko**)
2. Röviden és erősen ejtjük ki a hangsúlyos magánhangzót, ha azt több mássalhangzó követi (**lando**, **averto**, **barakti**, **fenestro**, **sporto**)

Az egyszerű szavak magánhangzói mindig rövidek (**nun**, **jes**, **jam**, **nur**).

Az **ŭ** csak közvetlanül **a**, **e** vagy **o** után használatos és azokkal egy szótagot képez.

Nagyjából ennyi az egész kiejtési szabályrendszer (**akcento**).

<a name="3"></a>
## 3. Alapmondatok, kifejezések - Bazaj frazoj, esprimoj

**Saluton!** - üdvözlöm, üdvözlet, szervusz, szia (rövidítve is használatos: Salut')  
**Bonan matenon!** - Jó reggelt!  
**Bonan tagon!** - Jó napot!  
**Bonan vesperon!** - Jó estét!  
**Bonan nokton!** - Jó éjszakát!  
**Ĝis revido!** - Viszont látásra!  
**Bonvole** - Kérem  
**Dankon** - Köszönöm  
**Volonte** - Szívesen  
**Ĝu vi parolas esperanton?** - Beszélsz eszperantóul?  
**Mi ne komprenas.** - Nem értem.  
**Kiel vi fartas?** - Hogy vagy?  
**Tre bone.** - Nagyon jól.  
**Kiel vi nomiĝas?** - Mi a neved?  
**Mi nomiĝas...** - A nevem...  
**Unu bieron, mi petas.** - Egy sört kérek.  

<a name="4"></a>
## 4. Főnév - Substantivo

Minden főnév **o**-ra végződik egyes szám alanyesetben (**domo** - ház, **hundo** - kutya, **arbo** - fa, **libro** - könyv).

**Többesszám**: a **j** hozzáadásával (**domoj** házak, **hundoj** - kutyák)

**Tárgyeset**: az **n** hozzáadásával (**domon** - házat, **hundon** - kutyát, **domojn** - házakat, **hundojn** - kutyákat)  
Használatával a mondat szórendjét is megváltoztathatjuk:
**La hundo amas la katon** = **La katon amas la hundo** (A kutya szereti a macskát.)

<a name="5"></a>
## 5. Melléknév - Adjektivo

Minden melléknév **a**-ra végződik (**alta** - magas, **bela** - szép, **nova** - új, **varma** - meleg, **blanka** - fehér, **blua** - kék, **nigra** - fekete).

Az összetartozó főnevet és melléknevet **mindig** ugyanolyan végződéssel kell ellátni (**novan libron** - új könyvet, **novajn librojn** - új könyveket)

A **mal** előképző az ellentétjére változtatja a melléknevet (**malalta** - alacsony, **malbela** - csúnya, **malvarma** - hideg, **malnova** - régi).

Az **et** utóképző kicsinyítést végez (**dometo** - házacska, **beleta** - csinos, **varmeta** - langyos, **hundeto** - kis kutya)

A melléknév fokozása:

- középfok: **pli** (**pli granda** - nagyobb; **malpli granda** - kisebb, azaz kevésbé nagy)
- felső fok: **plej** (**plej granda** - legnagyobb; **malplej granda** - legkisebb, azaz a legkevésbé nagy)

Az összehasonlító szó az **ol**:  
**La tablo estas pli granda ol la seĝo.** - Az asztal nagyobb a széknél.

<a name="6"></a>
## 6. Számnév - Numeralo

Számjegyek - **Ciferoj**

### Tőszámnevek *(hány?)*

1 - **unu**, 2 - **du**, 3 - **tri**, 4 - **kvar**, 5 - **kvin**, 6 - **ses**, 7 - **sep**, 8 - **ok**, 9 - **naŭ**, 10 - **dek**,
11 - **dek unu**, 12 - **dek du**, 13 - **dek** tri, 20 - **dudek**, 30 - **tridek**, 35 - **tridek kvin**, 40 - **kvardek**, 67 - **sesdek sep**, 80 - **okdek**,
100 - **cent**, 800 - **okcent**, 1000 - **mil**, 1000000 - **miliono**,
1848 - **mil okcent kvardek ok**

- Sorszámok *(hányadik?)*: tőszám + **a** (**tria** - harmadik)
- Sorszámi határozó *(hányadszor?)*: tőszám + **e** (**due** - másodszor, **kvine** - ötödször)
- Szorzószámnevek *(hányszoros?)*: az **-obl-** képzővel (**duobla** - kétszeres)
- Törtszámnevek *(hányad?)*: az **-on-** képzővel (**duono** - 1/2 (ketted), **triono** - 1/3, **tri kvaronoj** - három negyed, **centono** - 1/100 vagy 0.01, **du milonoj** - 0.002)
- Gyűjtőszámnevek *(hányan?)*: az **-op-** képzővel (**duope** - ketten, **dekope** - tízen)
- Osztószámnevek *(hányasával?)*: a **po** előtaggal (**po du** - kettesével, **po dektu** - tucatjával)

<a name="7"></a>
## 7. Névmás - Pronomo

### Személyes névmások - personaj pronomoj

**mi** - én  
**vi** - te (ön)  
**li** - ő (férfi)  
**&#349;i** - ő (nő)  
**&#285;i** - az (állat, tárgy)  
**ni** - mi  
**vi** - ti (önök)  
**ili** - ők (azok)  

### Birtokos névmások - prosedpronomoj:

**mia** - enyém  
**via** - tiéd (ön)  
**lia** - övé (férfi)  
**&#349;ia** - övé (nő)  
**&#285;ia** - azé (állat, tárgy)  
**nia** - miénk  
**via** - tiétek (önöké)  
**ilia** - ővék (azoké)  

### Visszaható névmások - refleksivoj:

**si** - saját maga (csak 3. személy): 

**Li lavas sin.** - Mosakodik. (mossa magát)  
**Ili (oni) lavas sin.** - Mosakodnak.

A **mem** nyomatékosító névmás: maga, saját maga.

A **sia** jelentése: a saját..., a saját maga valamilye

<a name="8"></a>
## 8. Ige - verbo

A főnévi igenevek mind i-re végződnek (**doni** - adni, **fari** - csinálni, **ludi** - játszani, **vidi** - látni, **esti** - lenni)

A 3 igeidő és a végződések kijelentő módban:

- Jelen idő: **as** (mi donas - adok, mi estas - vagyok)
- Múlt idő: **is** (mi donis - adtam)
- Jövő idő: **os** (mi donos - adni fogok)

Mivel az eszperantóban **igeragozás nincs**, a személyes névmást mindig ki kell tenni.

### 8.1 Felszólító mód - Volitivo kaj imperativo

Képzése: **u** (**mi donu** - adjak, **vi donu** - adj)

**Kuru!** - Fuss!  
**Pafu lin!** - Lődd le!  
**Bonvolu helpi min!** - Kérlek segíts!  
**Georgo faru ĝin!** - György csinálja ezt!

### 8.2 Feltételes mód - Kondicionalo

Képzése: **us** (**mi donus** - adnék)

A feltételes mód múlt idejű alakját az **esti** ige feltételes módjával és a ragozandó ige **-int-** képzős alakjával (lásd múlt idejű melléknévi igenév) alkotjuk meg: 

**Mi estus irinta.** - Mentem volna.  
**Mi estus skribinta leteron, se mi estus havinta tempon.** - Megírtam volna a levelet, ha lett volna időm.  
**Mi estus leganta.** - Olvasnék.  
**Mi estus leginta.** - Elolvastam volna.  
**Ili estus legantaj.** - Olvasnának.  
**Ili estus legintaj.** - Elolvasták volna.

### 8.3 A melléknévi és határozói igenevek

A **melléknévi és határozói igenevek**, vagyis az igéből képzett melléknevek (pl. dolgozó, éneklő) és igéből képzett határozószók (pl. sírva, futva), lehetnek cselekvők (aki/ami cselekszik) vagy szenvedők (akivel/amivel történik a cselekvés).
Az alábbi képzők után melléknévi igenévnél az **-a**, határozói igenévnél az **-e** betűt adjuk hozzá:

- Cselekvő:
    - Jelen ideje: **-ant-** (**laboranta homo** - dolgozó ember)
    - Múlt ideje: **-int-** (**laborinta homo** - ember, aki dolgozott)
    - Jövő ideje: **-ont-** (**laboronta homo** - ember, aki dolgozni fog)
- Szenvedő:
    - Jelen ideje: **-at-** (**kantata kanto** - éppen éneklés alatt álló dal)
    - Múlt ideje: **-it-** (**kantita kanto** - már elénekelt dal)
    - Jövő ideje: **-ot-** (**kantota kanto** - dal, ami majd el lesz énekelve)

<a name="9"></a>
## 9. Határozószó - Adverbo

Az általános határozószókat az **e** végződéssel képezzük:  
**bone** - jól, **aktive** - tevékenyen, **rapide** - gyorsan, **silente** - csendesen, **due** - másodjára, **ridante** - nevetve, **plorante** - sírva

<a name="10"></a>
## 10. Névelő - Artikolo

Csak határozott névelő van, a **la**. Ennek hiánya jelzi a határozatlanságot.

**La katoj estas belaj.** - A macskák szépek.  
**La viro parolas Esperanton.** - A férfi beszél eszperantóul.
**Birdo flugis la superbazaron.** - Egy madár szállt be a szupermarketbe. 

<a name="11"></a>
## 11. Elöljárószók - prepozicoj

**al** - -nak, -nek, -hoz, -hez, -höz, felé, nagyobb távolság-ba, be, mint (Li donas libron al la knabo. - Könyvet ad a fiúnak.)  
**anstataŭ** - helyett (Anstataŭ mi venos mia amiko. - Helyettem a barátom fog jönni.)  
**antaŭ** - előtt, elé (Antaŭ la domo estas arbo. - A ház előtt egy fa van.)  
**apud** - mellett, mellé (La tablo estas apud la muro. - Az asztal a fal mellett van.)  
**ĉe** - -nál, -nél (Mi estis ĉe la kuracisto. - Az orvosnál voltam.)  
**ĉirkaŭ** - körül, köré (Ĉirkaŭ la urbo estas arbaro. - A város körül van egy erdő.)  
**da** - -ból, -ből mennyiség kifejezésére (Mi petas glason da akvo. - Kérek egy pohár vizet.)  
**de** - -tól, -től, -nak a, -nek a, által (Mi petas libron de Petro. - Péter könyvét kérem. , Helena estas amata de ĉiuj - Ilona mindenki által szeretve van.)  
**dum** - időtartam alatt (Dum la prelego mi dormis - Az előadás alatt aludtam.)  
**ekster** - helyileg kívül (La direktoro estas ekster la domo. - Az igazgató házon kívül van.)  
**el** - -ból, -ből (Ŝi venis el Moskvo. - Moszkvából jött.)  
**en** - -ban, -ben, -ba, -be (Mi iras en la parkon. - Megyek a parkba.)  
**ĝis** - -ig (Iru ĝis la arbo! Menj el a fáig!)  
**inter** - között, közé (Inter la montoj estas lago. - A hegyek között van egy tó.)  
**je** - -kor, az óra jelzésére (Je la deka horo. - Tíz órakor.)  
**kontraŭ** - ellen (Kontraŭ morto ne estas kuracilo. - Halál ellen nincs orvosság.)  
**krom** - kívül, de átvitt értelemben (Krom la knaboj estas ĉi tie ankaŭ Anna. - A fiúkon kívül itt van Anna is.)  
**kun** - -val, -vel társhatározóként (Mi promenas kun mia amikino. - A barátnőmmel sétálok.)  
**laŭ** - szerint, mentén (Laŭ mia opinio... - Véleményem szerint...)  
**malgraŭ** - ellenére, dacára (Malgraŭ la pluvo ni ekskursos. - az eső ellenére kirándulni fogunk.)  
**malantaŭ** - mögött, mögé (Malantaŭ la monto situas la urbo. A hegy mögött van a város.)  
**per** - -val, -vel eszközhatározóként (Ni veturas per aŭtobuso. - Autóbusszal utazunk.)  
**por** - -ért célhatározóként (Ni laboras por vi. - Érted dolgozunk.)  
**post** - után, múlva (Post du tagoj mi forveturos. - Két nap múlva elutazom.)  
**preter** - valami mellett el (Li iris preter la domo. - Elment a ház mellett.)  
**pri** - -ról, -ről elvont értelemben (Ni paroladis pri la politiko. - A politikáról beszélgettünk.)  
**pro** - miatt okhatározóként (Mi malfruis pro la pluvo. - Az eső miatt késtem.)  
**sen** - nélkül (Sen benzino la aŭtoj ne povas funkcii. - Benzin nélkül az autók nem működnek.)  
**sub** - alatt, alá (La benko estas sub la arbo. - A pad a fa alatt van.)  
**super** - fölött, fölé (Super la nuboj la ĉ ielo ĉ iam estas blua. - A felhők fölött mindig kék az ég.)  
**sur** - -n, -on, -en, -ön, -ra, -re (La libro kuŝ as sur la tablo. - A könyv az asztalon fekszik.)  
**tra** - át, keresztül, rajta, benne (Ni iras tra la ponto. - Átmegyünk a hídon.)  
**trans** - túl, túlra (Mi loĝas trans la rivero. - A folyón túl lakom.)

<a name="12"></a>
## 12. Kötőszavak, módosítószók és egyéb határozószók - Konjunkcioj, ŝanĝado konjunkcioj kaj aliaj adverboj

**jes** - igen  
**ne** - nem  
**nek** - sem  
**ja** - hiszen, persze  
**sed** - de  
**kaj** - és  
**aŭ** - vagy  
**jam** - már  
**ankoraŭ** - még, ismét  
**ankaŭ** - szintén  
**se** - ha  
**adiaŭ** - isten vele  
**ajn** - *lásd tabellaszavak*  
**almenaŭ** - legalább  
**ambaŭ** - mindkét  
**apenaŭ** - alig, alighogy  
**baldaŭ** - hamarosan  
**ĉar** - mert  
**ĉi** - *lásd tabellaszavak*  
**ĉu** - -e, vajon  
**des** - annál (inkább, -bb)  
**do** - tehát  
**eĉ** - sõt  
**hieraŭ** - tegnap  
**hodiaŭ** - ma  
**morgaŭ** - holnap  
**jen** - íme  
**ju** - minél (inkább, -bb)  
**ĵus** - imént  
**ke** - hogy  
**kvankam** - habár  
**kvazaŭ** - mintha  
**la** - a, az  
**mem** - önmaga  
**minus** - mínusz  
**nu** - nos  
**nun** - most  
**nur** - csak  
**ol** - mint *(összehasonlító)*  
**plej** - leg-  
**pli** - inkább, -bb  
**plu** - tovább  
**plus** - plusz  
**preskaŭ** - majdnem  
**tamen** - mégis  
**tre** - nagyon  
**tro** - túl  
**tuj** - azonnal  

<a name="13"></a>
## 13. Állítás és tagadás - Aserto kaj negado

Az állítást, létezést az **estas** szóval fejezzük ki.
A létezést kifejező igét a magyarban nem mindig tesszük ki, de beleértjük. Az eszperantóban viszont minden mondatnak kell, hogy legyen állítmánya, ezért az estas egy gyakori szó.

**Mia familio estas granda.** - Nagy családom van.  
**Li estas bakisto.** - Ő pék. 

A tagadást a **ne** szócskával fejezzük ki, amely mindig az előtt a szó előtt áll, amelyet tagadni akarunk:

**Mi ne trinkas kafon.** - Nem iszom kávét.  
**Ne mi trinkas kafon.** - Nem én iszom kávét.  
**Mi trinkas ne kafon.** - Nem kávét iszom.

Az eszperantóban sincs dupla tagadás. (**Mi agis nenion.** - Nem csináltam semmit. (azaz szó szerint "Én csináltam semmit."))

<a name="14"></a>
## 14. Tabellaszavak - Tabelvortoj

<table class="tan">
  <tr>
    <th>kérdő</th>
    <td>kiu<br>ki</td>
    <td>kio<br>mi</td>
    <td>kia<br>milyen</td>
    <td>kie<br>hol</td>
    <td>kiam<br>mikor</td>
  </tr>
  <tr>
    <th>mutató</th>
    <td>tiu<br>ő, amely</td>
    <td>tio<br>az</td>
    <td>olyan</td>
    <td>tie<br>ott</td>
    <td>tiam<br>akkor</td>
  </tr>
  <tr>
    <th>általánosító</th>
    <td>&#265;iu<br>mindenki</td>
    <td>&#265;io<br>mindaz</td>
    <td>&#265;ia<br>mindenféle</td>
    <td>&#265;ie<br>mindenütt</td>
    <td>&#265;iam<br>mindig</td>
  </tr>
  <tr>
    <th>tagadó</th>
    <td>neniu<br>senki</td>
    <td>nenio<br>semmi</td>
    <td>nenia<br>semmilyen</td>
    <td>nenie<br>sehol</td>
    <td>neniam<br>soha</td>
    </td>
  </tr>
  <tr>
    <th>határozatlan</th>
    <td>iu<br>valaki</td>
    <td>io<br>valami</td>
    <td>ia<br>valamilyen</td>
    <td>ie<br>valahol</td>
    <td>iam<br>valamikor</td>
  </tr>
</table>

<table class="tan">
  <tr>
    <th>kérdő</th>
    <td>kial<br>miért</td>
    <td>kiel<br>hogyan</td>
    <td>kiom<br>mennyi</td>
    <td>kies<br>kié</td>
  </tr>
  <tr>
    <th>mutató</th>
    <td>tial<br>azért</td>
    <td>tiel<br>úgy</td>
    <td>tiom<br>annyi</td>
    <td>ties<br>azé</td>
  </tr>
  <tr>
    <th>általánosító</th>
    <td>&#265;ial<br>mindenért</td>
    <td>&#265;iel<br>mindenképp</td>
    <td>&#265;iom<br>mindannyi</td>
    <td>&#265;es<br>mindenkié</td>
  </tr>
  <tr>
    <th>tagadó</th>
    <td>nenial<br>semmiért</td>
    <td>neniel<br>semmiképp</td>
    <td>neniom<br>semennyi</td>
    <td><p>nenies<br>senkié</p>
    </td>
  </tr>
  <tr>
    <th>határozatlan</th>
    <td>ial<br>valamiért</td>
    <td>iel<br>valamiképp</td>
    <td>iom<br>valahány</td>
    <td>ies<br>valakié</td>
  </tr>
</table>

A táblázatban látható mutató névmások önmagukban csak a távolra mutatást végzik: **tio** (az), a **tie** (ott) és a **tiu** (az a...).
A közelre mutatást a **ĉi** elöljáróval kiegészítve fejezzük ki (**ĉi tio** - ez, **ĉi tie** - itt és **ĉi tiu** - ez a...)

<a name="15"></a>
## 15. A szóképző rendszer - Sistemo de la vortfarado

Az előző részben megismert négy, számokra vonatkozó képzővel együtt összesen 44 elő- és utóképző áll rendelkezésünkre.

### Főnévi elő- és utóképzők

**bo-** házasság folytán származott rokon (patro → bopatro - apa → após)  
**ge-** pár a két nemből (patro → gepatroj - apa → szülők, edzo → geedzoj - férj → házastársak)  
**pra-** ős, előd, déd (homo → prahomo - ember → ősember, avo → praavo - nagyapa → dédnagyapa)  
**eks-** volt, egykori, ex (ministro → eksministro - miniszter → exminiszter)  
**-in-** nő, nőnem, nőstény (viro → virino - férfi → nő, kolego → kolegino, kolléga → kolléganő)  
**-id-** utód, leszármazott (porko → porkido - disznó → malac, reĝo → reĝido - király → királyfi)  
**-ul-** személy, amelyre a szótő jellemző (juna → junulo - fiatal → egy ifjú)  
**-an-** tag, lakos (urbo → urbano - város → városi ember, kristo → kristano - krisztus → keresztény)  
**-ist-** foglalkozás, valamely irányzat (instrui → instruisto - tanítani → tanár, esperanto → esperantisto - eszperantó → eszperantista)  
**-estr-** vezető, parancsnok, főnök (urbo → urbestro - város → polgármester)  
**-aĵ-** a cselekvés eredménye (pentri → pentraĵo - festeni → festmény, trinki → trinkaĵo - inni → ital)  
**-ec-** -ság, -ség, elvont fogalom (bela → beleco - szép → szépség)  
**-ar-** azonos dolgok összessége, gyűjteménye (vorto → vortaro - szó → szótár, arbo → arbaro - fa → erdő)  
**-er-** az egész része (pano → panero - kenyér → kenyérmorzsa)  
**-ej-** helyiségképző (lerni → lernejo - tanulni → iskola, loĝi → loĝejo - lakni → lakás)  
**-uj-** tok, tartó, amelyből a tárgy nem lóg ki (salo → salujo - só → sótartó)  
**-ing-** tok, tartó, amelyben a tárgy csak részben van benne (glavo → glavingo - kard → kardhüvely, kandelo → kandelingo - gyertya → gyertyatartó)  
**-il-** eszköz, szerszám (tranĉi → tranĉilo - vágni → kés)  
**-ism-** -izmus (alkoholo → alkoholismo - alkohol → alkoholizmus)

### Melléknévi elő- és utóképzők

**mal-** ellentét (bona → malbona - jó → rossz, granda → malgranda - nagy → kicsi)  
**-em-** hajlam (labori → laborema - dolgozni → dolgos)  
**-ebl-** -ható, -hető (manĝi → manĝebla - enni → ehető, kompari → komparebla - hasonlít → összehasonlítható)  
**-ind-** méltó (laŭdi → laŭdinda - dícséret → dícséretre méltó)  
**-end-** -andó, -endő, kötelező (lerni → lernenda - tanulni → tanulandó)

### Igei elő- és utóképzők

**dis-** szét (sendi → dissendi - küldeni → szétküldeni)  
**for-** el, tova (iri → foriri - menni → elmenni)  
**mis-** rosszul, félre (kompreni → miskompreni - érteni → félreérteni)  
**ek-** kezdés (iri → ekiri - menni → elindulni, paroli → ekparoli - beszélni → megszólalni)  
**re-** újra, vissza, ismét (sendi → resendi - küldeni → visszaküldeni, doni → redoni - adni → visszaadni)  
**-ad-** tartós cselekvés ( bati → batadi - ütni → ütlegelni, sidi → sidadi - ülni → üldögélni)  
**-ig-** valamilyenné tenni (bela → beligi - szép → szépíteni, sidi → sidigi - ülni → leültetni)  
**-iĝ-** valamilyenné válni (bela → beliĝi - szép → szépülni, dolĉa → dolĉiĝi - édes → édesedni)

### További elő- és utóképzők

**-et-** kicsinyítő (homo → hometo - ember → emberke, varma → varmeta - meleg → langyos)  
**-eg-** nagyító (bona → bonega - jó → kitűnő, remek, vento → ventego - szél → vihar)  
**-aĉ-** külső megvetés (ĉevalo → ĉevalaĉo - ló → gebe)  
**fi-** erkölcsi (belső) megvetés (homo → fihomo - ember → gazember)  
**-ĉj-** férfi becéző (patro → paĉjo - apa → apuka)  
**-nj-** női becéző (patrino → panjo - anya → anyuka)  
**retro-** visszafelé, hátra (spegulo → retrospegulo - tükör → visszapillantó tükör)  
**-um-** főnévből ige (?) (aero → aerumi - levegő → szellőztetni, somero → somerumi - nyár → nyaralni)
