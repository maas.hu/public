# ![alt text](/_ui/img/icons/docker_m.svg) Docker

<div style="text-align: right;"><h7>Posted: 2019-05-24 07:25</h7></div>

###### .

## Tartalomjegyzék

1. [Bevezető](#1)
2. [A Docker telepítése](#2)
3. [Alapműveletek](#3)
4. [Tároló](#4)
5. [Hálózat](#5)
6. [A Docker registry-k](#6)
7. [A Dockerfile](#7)
8. [Docker image építése a semmiből](#8)
9. [A Portainer UI](#9)

<a name="1"></a>
## 1. Bevezető

![alt_text](docker_-_intro.png)

A Docker napjainkban a legnépszerűbb, egyben a legszélesebb körben támogatott konténerizációs virtualizációt kínáló technológia, ennélfogva jelenleg (2022) a Dockert tekinthetjük a de facto operációs rendszer virtualizációs eszköznek. 

### 1.1 Rövid történelmi áttekintés

A konténerizációs technológiákat tekintve az alábbiakat nevezhetjük a legfontosabb mérföldköveknek, kapcsolódó technológiáknak: 

 - **1979**: Unix V7: `chroot`
 - **2000**: FreeBSD: Jails
 - **2001**: Linux VServer
 - **2004**: Solaris Zones
 - **2007**: AIX WPARs
 - **2013**: Docker

(Jó, az AIX WPAR-okról szinte sehol sem igazán tesznek említést, mint széles körben alkalmazott technológia, de azért kötelességemnek éreztem a felsorolásba betenni, mivel volt vele legalább dolgom.)

Ahogyan az a felsorolásból is feltűnhet, a `chroot`, mint a felhasználó által meghatározott könyvtár új `/` (root) könyvtárként való megadása, egy már nagyon régóta ismert lehetőség a különféle Unix-szerű rendszerekben az ezt igénylő szolgáltatások egymástól való szinte teljes mértékű izolálására. Ezt aztán – legalábbis a saját tapasztalatom szerint – a Sun gondolt tovább igazán jól egy kényelmesen kezelhető konténerizációs megoldássá a **Solaris zónák** segítségével. Ezt már valóban sok production rendszerben is használták, de a virtuális gépek széleskörű terjedése kezdte egyre inkább elhalványítani az általa nyújtott előnyöket. Végül a Docker élesztette újra az egészet azáltal, hogy a konténer image-ek készítését, tárolását, online megosztását jelentősen leegyszerűsítette. Az eredetileg **dotCloud** nevű, 2008-ban alapított cégnél, annak egyik alapítója, a francia-amerikai **Solomon Hykes** volt a Docker eredeti fejlesztője. 

A Docker rugalmassága kézzelfogható értelmet ad a DevOps filozófiában meghatározott **Continuous Integration** (CI) és **Continuous Delivery** (CD) modelleknek is.

### 1.2 A konténerizáció alapjai

Ezt már sokan, sokféleképpen elmagyarázták, így inkább beszéljen az alábbi ábra: 

![alt_text](from_physical_servers_to_containers.png)

(Persze ez kissé túlságosan is egyszerűsített, ugyanis a középső kiépítés a Type-2 virtualizációt ábrázolja.)

### 1.3 Namespace-ek és cgroup-ok

A különféle konténerizációs technológiák, úgy mint a Docker és a vele szoros kapcsolatban álló, konténerek clusterizációját biztosító **Kubernetes** napjainkra a modern alkalmazásfejlesztés alapját képezik, mégis a legtöbben keveset tudnak ezek működéséről úgymond a motorháztető alatt. 

#### 1.3.1 Namespace-ek

A Namespace-ek már a Linux kernel részét képezik 2002 óta, de a valódi konténer-támogatás csak 2013-ban került be abba. 

A hivatalos definíciójuk: 

>Namespaces are a feature of the Linux kernel that partitions kernel resources such that one set of processes sees one set of resources while another set of processes sees a different set of resources.

Azaz, a Linuxon futó processzek teljesen izolálhatóak egymástól, ami nagyobb stabilitást és a biztonság fokozását jelenthetik. Az egyes processzek önálló számítógépeknek tekinthetik a processz futásához szükséges erőforrásokat. 

Namespace típusok: 

- **user namespace**: a user ID-n és group ID-n alapuló izoláció
- **process ID (PID) namespace**: minden processz külön PID-el fut, ugyanakkor közöttük child és parent kapcsolat állhat fent
- **network namespace**: az egymástól független network stack-ek, saját routing táblák, IP-k stb-k szintű izoláció
- **mount namespace**: az egyes processzek önálló mountpont listával rendelkezhetnek
- **interprocess communication (IPC) namespace**: pl. POSIX message queue-k (?)
- **UNIX Time-Sharing (UTS) namespace**: lehetővé teszik egyazon rendszeren belül a különböző host és domain neveket különböző processzek számára

##### 1.3.1.1 Az `unshare` parancs

A parancs feladatának hivatalos megfogalmazása: 

>Run a program with some namespaces unshared from the parent.

Pl.:

```ksh
# su - tstusr

$ id
uid=1002(tstusr) gid=100(users) groups=100(users)

$ unshare --user --pid --map-root-user --mount-proc --fork bash

# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 17:02 pts/0    00:00:00 bash
root          20       1  0 17:02 pts/0    00:00:00 ps -ef

# id
uid=0(root) gid=0(root) groups=0(root)

$ logout
#
```

Ezzel így egészen olyan processzlistát kaphatunk és egészen olyan izolációt értünk el, mint majd később láthatjuk a `docker exec -it <image> /bin/bash` parancs esetében, amikor sessiont nyitunk egy futó konténerbe. 

##### 1.3.1.2 Az `nsenter` parancs

A parancs feladatának hivatalos megfogalmazása: 

>A Run a program with namespaces of other processes.

Szóval ezzel... valahogy programot lehet futtatni egy másik processz namespace-ében (vagy mi). 

Pl. (feltételezve, hogy továbbra is fut a fenti namespace a tstusr alól): 

```ksh
# ps -fu tstusr
UID          PID    PPID  C STIME TTY          TIME CMD
tstusr   1917434 1917433  0 17:12 pts/0    00:00:00 -bash
tstusr   1917462 1917434  0 17:12 pts/0    00:00:00 unshare --user --pid --map-root-user --mount-proc --fork bash
tstusr   1917463 1917462  0 17:12 pts/0    00:00:00 bash

# nsenter -t 1917463 -p -r top
top - 17:28:35 up 22 days,  1:28,  2 users,  load average: 0.11, 0.09, 0.09
Tasks:   2 total,   1 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.2 us,  0.2 sy,  0.0 ni, 99.5 id,  0.0 wa,  0.2 hi,  0.0 si,  0.0 st
MiB Mem :  15832.5 total,   4225.5 free,   4276.7 used,   7330.3 buff/cache
MiB Swap:   4096.0 total,   4096.0 free,      0.0 used.  11191.5 avail Mem

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
      1 tstusr    20   0   16380   5152   3260 S   0.0   0.0   0:00.02 bash
     24 root      20   0   54168   4324   3608 R   0.0   0.0   0:00.00 top
```

#### 1.3.2 cgroup-ok

Ez a Linux kernel szintű képesség az erőforrások korlátozását teszi lehetővé. 

A hivatalos megfogalmazás: 

>A control group (cgroup) is a Linux kernel feature that limits, accounts for, and isolates the resource usage (CPU, memory, disk I/O, network, and so on) of a collection of processes. 

Segítségével adott processz egyes vagy akár több erőforráslimitjét (pl. RAM, CPU) szabályozhatjuk, az erőforrások elérésének prioritását állíthatunk be a processzek között, továbbá az egy cgroup-ba tartozó összes processz állapotát (frozen, stopped, restarted) módosíthatjuk. 

Ennek kényelmesebb használatához telepíthetjük a `libcgroup-tools` csomagot is, de alapszinten is támogatja már minden Linux.

Pl.:

```ksh
# cat > test.sh
echo "cgroup testing tool"
sleep 9999

# chmod +x test.sh

# mkdir -p /sys/fs/cgroup/memory/foo

# echo 50000000 > /sys/fs/cgroup/memory/foo/memory.limit_in_bytes

# ./test.sh &
[1] 1919913
cgroup testing tool

# echo 1919913 > /sys/fs/cgroup/memory/foo/cgroup.procs

# ps -o cgroup 1919913
CGROUP
9:pids:/system.slice/sshd.service,5:memory:/foo,2:devices:/system.slice/sshd.service,1:name=systemd:/system.slice/sshd.service

# kill 1919913
#
[1]+  Terminated              ./test.sh
```

Alapértelmezés szerint az operációs rendszer megszakítja a process-t, amikor az meghaladná a cgroup által megszabott erőforráslimitet. 

#### 1.3.3 Konténerek

A namespace-ek és a cgroup-ok által felvázolt képességeket teszik kényelmessé, átláthatóvá és valóban használhatóvá a különféle konténerező technológiák. A jelenleg legrugalmasabb és legsokoldalúbb megvalósítást pedig a Dockeren keresztül ismerhetjük meg igazán. 

<a name="2"></a>
## 2. A Docker telepítése

A továbbiakban egy Linux (Rocky 8 x86_64) VirtualBox-ban futó VM-ben fogunk ügyködni. 

### 2.1 A Docker telepítése csomagkezelő nélkül

Csomagkezelőtől független telepítésre és minimális configra példa: 

```ksh
# cd /opt

# wget https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz
--2021-09-26 07:03:56--  https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz
Resolving download.docker.com (download.docker.com)... 52.84.109.14, 52.84.109.107, 52.84.109.33, ...
Connecting to download.docker.com (download.docker.com)|52.84.109.14|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 63350495 (60M) [application/x-tar]
Saving to: ‘docker-20.10.9.tgz’
...

# tar -xzf docker-20.10.9.tgz

# ls -la
total 61884
...
drwxrwxr-x.  2 1000 1000     4096 Oct  4 18:08 docker
-rw-r--r--.  1 root root 63350495 Oct 19 12:49 docker-20.10.9.tgz
...

# rm docker-20.10.9.tgz
rm: remove regular file 'docker-20.10.9.tgz'? y

# chown -R root:root docker

# vi ~/.bash_profile
...
export PATH=$PATH:$HOME/bin:/opt/Node.js/bin:/opt/docker
...

# useradd -m docker

# id docker
uid=1000(docker) gid=1000(docker) groups=1000(docker)

# ^D
```

Relogin, majd a Docker daemon elindítása (itt előtérben, de persze háttérben is indítható): 

```ksh
# docker -v
Docker version 20.10.9, build c2ea9bc

# dockerd
INFO[2021-09-26T07:09:15.788317804+02:00] Starting up
INFO[2021-09-26T07:09:15.792271323+02:00] libcontainerd: started new containerd process  pid=8671
INFO[2021-09-26T07:09:15.792405279+02:00] parsed scheme: "unix"                         module=grpc
...
```

### 2.2 Docker daemon beállítása a Systemd számára

Amennyiben automatikusan indíthatóvá állítanánk az előbbi kreálmányt (sima `.tgz` állományból kibontás az `/opt/docker` alá), úgy itt van a néhány rövid lépés ehhez: 

1. Készítsünk egy `/usr/lib/systemd/system/dockerd.service` fájlt az alábbi tartalommal (nem kell futtathatónak lennie): 
```
[Unit]
Description=Docker daemon

[Service]
ExecStart=/opt/docker/dockerd

[Install]
WantedBy=multi-user.target
```

2. Két további symlink (mert elég abnormális helyen vannak a daemon indításához szükséges további binárisok): 

```ksh
# ln -s /opt/docker/containerd /usr/bin

# ln -s /opt/docker/runc /usr/bin
```

3. Az service beállítása és indítása: 

```ksh
# systemctl enable dockerd.service
Created symlink /etc/systemd/system/multi-user.target.wants/dockerd.service → /usr/lib/systemd/system/dockerd.service.

# systemctl start dockerd.service

# systemctl status dockerd.service
● dockerd.service - Docker daemon
   Loaded: loaded (/usr/lib/systemd/system/dockerd.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2022-05-09 19:39:13 CEST; 1s ago
 Main PID: 9101 (dockerd)
[...]
```


<a name="3"></a>
## 3. Alapműveletek

A **Docker image** egy elképzelhető legminimálisabb OS-t tartalmaz, ami amit csak lehet, a gazda OS-től kap, mégis önálló rendszernek tekinthető. Emiatt a mérete (egy okosan összeállított image esetében) nem haladja meg a néhány 100 MB-ot. 

A Docker image és az abból indított **konténer** életútja alapértelmezés szerint az alábbi: 

```
image ➡️ running container ➡️ stopped container
```

Mindez a gyakorlatban azt jelenti, hogy a már rendelkezésünkre álló image-ből a `docker run` parancs fogja a futó konténert felépíteni. Amennyiben az image még nincs a gépünkön, a Docker okos és akkor ezt megpróbálja előbb a **Docker Hubról** letölteni. 

Fontos továbbá már itt megemlíteni, hogy az image-ek **állandó, fix tartalommal rendelkeznek**, így akármit is művelünk egy konténerben, a változtatásaink az eredeti image szempontjából (!) a konténer leállítása után azzal együtt megsemmisülnek. 

### 3.1 A "Hello World" Docker konténer

A legegyszerűbb "Hello World" image letöltése és indítása. Ez az image tényleg csak ennyit tartalmaz, hogy egy 

```ksh
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:37a0...9c51
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

### 3.2 A Docker Hub

Docker image-ek keresése a Docker Hubban, parancssorból: 

```ksh
$ docker search rocky
NAME                                     DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
rockylinux/rockylinux                                                                    40
rockyluke/debian                         Docker images of Debian.                        5
rockyluke/ubuntu                         Docker images of Ubuntu.                        4
rockyluke/nodejs                         Docker images of Node.js.                       3                    [OK]
...
```

Ahogyan az előző bekezdésben a "Hello World" konténerünk is tájékoztatott erről, böngészőben, talán kicsit kényelmesebben is keresgélhetünk mások által készített image-ekre a [hub.docker.com](https://hub.docker.com/) oldalon. 

### 3.3 Interaktív terminál

Egy gyári image letöltése egy új konténer létrehozása interaktív terminállal: 

```ksh
$ docker run -ti rockylinux/rockylinux bash
Unable to find image 'rockylinux/rockylinux:latest' locally
latest: Pulling from rockylinux/rockylinux
1b474f8e669e: Pull complete
Digest: sha256:8122...7846
Status: Downloaded newer image for rockylinux/rockylinux:latest
[root@9f9a836ddedd /]#
[root@9f9a836ddedd /]# cat /etc/redhat-release
Rocky Linux release 8.4 (Green Obsidian)
[root@9f9a836ddedd /]#
[root@9f9a836ddedd /]# uname -a
Linux 9f9a836ddedd 4.18.0-305.19.1.el8_4.x86_64 #1 SMP Wed Sep 15 19:12:32 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
[root@9f9a836ddedd /]#
[root@9f9a836ddedd /]# df -hPT
Filesystem                Type     Size  Used Avail Use% Mounted on
overlay                   overlay  9.8G  2.8G  6.6G  30% /
tmpfs                     tmpfs     64M     0   64M   0% /dev
tmpfs                     tmpfs    1.9G     0  1.9G   0% /sys/fs/cgroup
shm                       tmpfs     64M     0   64M   0% /dev/shm
/dev/mapper/rootvg-rootlv ext4     9.8G  2.8G  6.6G  30% /etc/hosts
tmpfs                     tmpfs    1.9G     0  1.9G   0% /proc/asound
tmpfs                     tmpfs    1.9G     0  1.9G   0% /proc/acpi
tmpfs                     tmpfs    1.9G     0  1.9G   0% /proc/scsi
tmpfs                     tmpfs    1.9G     0  1.9G   0% /sys/firmware
[root@9f9a836ddedd /]#
[root@9f9a836ddedd /]# date
Thu Oct 21 18:31:00 UTC 2021
[root@9f9a836ddedd /]#
[root@9f9a836ddedd /]# exit
$ 
```

### 3.4 További alapműveletek

Futó konténerek listázása: 

```ksh
$ docker ps
CONTAINER ID   IMAGE                   COMMAND   CREATED         STATUS         PORTS     NAMES
5dba08d5b856   rockylinux/rockylinux   "bash"    6 seconds ago   Up 6 seconds             kind_mccarthy
```

Itt láthatjuk, hogy amennyiben a `docker run` során nem adtunk konténerünknek nevet, úgy a Docker ad neki valami random, de megjegyezhetőt annak ID-ján kívül. 

Valaha itt ismert és futott (még nem törölt) konténerek listázása: 

```ksh
$ docker ps -a
CONTAINER ID   IMAGE                   COMMAND   CREATED          STATUS                     PORTS     NAMES
5dba08d5b856   rockylinux/rockylinux   "bash"    54 seconds ago   Up 54 seconds                        kind_mccarthy
9f9a836ddedd   rockylinux/rockylinux   "bash"    10 minutes ago   Exited (0) 9 minutes ago             quizzical_easley
```

Utoljára létrehozottak: 

```ksh
$ docker ps -l
CONTAINER ID   IMAGE                   COMMAND   CREATED              STATUS              PORTS     NAMES
5dba08d5b856   rockylinux/rockylinux   "bash"    About a minute ago   Up About a minute             kind_mccarthy
```

Containerek leállítása (akár több is), pl.: 

```ksh
$ docker stop vm01 vm02
vm01
vm02
```


<a name="4"></a>
## 4. Tároló

A fentiek alapján látható, hogy a Docker alapértelmezés szerint arra lett kitalálva, hogy könnyedén, online elérhető image-ekből dolgozzunk. Szükség lehet azonban arra, hogy saját image-eket hozzunk létre, vagy csak egyszerűen nem szeretnénk egyből elindítani az online forrásból letöltött image-et. 

Ez uttóbira való a `docker pull` (ami a `docker run` esetében, a még nem letöltött image-eknél automatikusan is lefut). Ellentétje a `docker push`, amivel a központilag manage-elt Docker registry-nkbe (vagy repository-nkba) tölthetjük fel az image-einket.

### 4.1 A commit

A korábbiakban megismertek szerint, az eredeti image szempontjából mindennemű változtatás, amit a futó konténerben végeztünk elvész. Ez viszont nem jelenti azt, hogy a már leállított konténer is semmivé válik. A `docker commit` művelet arra való, hogy a konténer életútját kibővíthessük igény szerint: 

```
image ➡️ running container ➡️ stopped container ➡️ new image
```

A containernek tehát le kell állnia, ezután már következhet a változtatások mentése egy új image-be: 

```ksh
$ docker ps -a
CONTAINER ID   IMAGE                   COMMAND            CREATED          STATUS                        PORTS     NAMES
99b22e9529e1   rockylinux/rockylinux   "/usr/sbin/init"   38 minutes ago   Exited (137) 5 minutes ago              rockyssh

$ docker commit 99b22e9529e1
sha256:078e90a9b5bbae6b5a814e5fa18587d7bc78c494e474bfd255bc24ded883e54a

$ docker images
REPOSITORY              TAG       IMAGE ID       CREATED          SIZE
&lt;none&gt;                  &lt;none&gt;    078e90a9b5bb   16 seconds ago   249MB
rockylinux/rockylinux   latest    a1e37a3cce8f   9 days ago       205MB

$ docker tag 078e90a9b5bbae6b5a814e5fa18587d7bc78c494e474bfd255bc24ded883e54a rockyssh

$ docker images
REPOSITORY              TAG       IMAGE ID       CREATED          SIZE
rockyssh                latest    078e90a9b5bb   52 seconds ago   249MB
rockylinux/rockylinux   latest    a1e37a3cce8f   9 days ago       205MB
```

A `commit` és külön `tag` műveletek szerencsére egyetlen paranccsal is megoldhatóak: 

```ksh
$ docker commit romantic_cartwright rockyssh
[...]
```


### 4.2 Image-ek fájlba való mentése és visszatöltése

...

```ksh
$ docker save -o rockysshd.tar.gz rockysshd

$ docker rmi rockysshd
[...]

$ docker load -i rockysshd.tar.gz
```

### 4.3 Image-ek tag-elése

A tag-elés struktúrája eképpen néz ki: 

```
registry.example.com:port/organization/image-name:version-tag
```

Mindebből persze több rész is elhagyható. Ha pl. nem adunk meg `version-tag`-et, akkor a `latest` kerül az image-hez alapértelmezés szerint hozzárendelésre. A gyakorlatban többnyire az alábbi, rövid változat használatos:

```
organization/image-name
```

Ez így már a Docker Hub-ra "néz fel" és onnan is a `latest` tag-elésű image-re keres.


### 4.4 Konténerek és image-ek törlése

Image-eket könnyen törölhetünk a `docker rmi image-name:tag` vagy `docker rmi image-id` szintaktikákkal megadott parancsok valamelyikével. 

Amennyiben viszont az image-re hivatkozik egy futó vagy akár már leállított konténerünk (ami ugyebár `commit`-al még akár új image-é is alakítható lenne), hibaüzenetet kapunk, pl.:

```ksh
$ docker images
REPOSITORY                                                          TAG               IMAGE ID       CREATED        SIZE
[...]
cytopia/ansible                                                     latest            fc33944dcd00   3 months ago   487MB
[...]

$ docker rmi fc33944dcd00
Error response from daemon: conflict: unable to delete fc33944dcd00 (must be forced) - image is being used by stopped container a2e5fcbbf1e4
```

#### 4.4.1 Totális kipucolás

Az alábbiakkal minden leállított konténert eltávolítunk, majd a nem használt image-eket is töröljük:

```ksh
$ docker container prune
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Deleted Containers:
[...]
Total reclaimed space: 32B

$ docker image prune -a --force
Deleted Images:
[...]
Total reclaimed space: 72.79MB
```

### 4.5 Virtuális kötetek

Két fő változatuk lehet a Docker konténerek szempontjából: 

- állandó
- tiszavirág életű

Fontos, hogy olyan kötetekről beszélünk itt, amiket az image nem tartalmaz, hanem külön kerülnek felcsatolásra.

#### 4.5.1 Adatok megosztása a gazdagép és a konténer között

Itt egy jó és egyszerű példa minderre (itt most ráadásul a `docker` userként csináltuk az egészet):

```ksh
$ pwd
/home/docker

$ mkdir inventory

$ docker run -ti -v /home/docker/inventory:/inventory rockylinux bash

[root@dacb786f4117 /]# cd /inventory/

[root@dacb786f4117 inventory]# ls -la
total 8
drwxrwxr-x. 2 1000 1000 4096 May 18 08:09 .
drwxr-xr-x. 1 root root 4096 May 18 08:10 ..

[root@dacb786f4117 inventory]# df -hPT .
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-homelv ext4  1.5G  4.6M  1.4G   1% /inventory

[root@dacb786f4117 inventory]# touch valami

[root@dacb786f4117 inventory]# ls -la
total 8
drwxrwxr-x. 2 1000 1000 4096 May 18 08:10 .
drwxr-xr-x. 1 root root 4096 May 18 08:10 ..
-rw-r--r--. 1 root root    0 May 18 08:10 valami

[root@dacb786f4117 inventory]# exit
exit

$ ls -la inventory/
total 8
drwxrwxr-x. 2 docker docker 4096 May 18 10:10 .
drwx------. 3 docker docker 4096 May 18 10:09 ..
-rw-r--r--. 1 root   root      0 May 18 10:10 valami
```

#### 4.5.2 Adatok megosztása konténerek között

Az ilyen kötetek már nem kerülnek a gazdagép számára elérhetővé és a kötet tartalma csak addig létezik, amíg a konténer él, ezáltal "tiszavirág életű". 

Egyik konténer előkészítése, majd futó állapotban hagyása:

```ksh
$ docker run -ti -v /inventory rockylinux bash

[root@08b6dd403b72 /]# ls -la /inventory
total 8
drwxr-xr-x. 2 root root 4096 May 18 08:18 .
drwxr-xr-x. 1 root root 4096 May 18 08:18 ..

[root@08b6dd403b72 /]# df -hPT /inventory
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv ext4  9.8G  2.8G  6.6G  30% /inventory

[root@08b6dd403b72 /]# touch /inventory/an_empty_file
```

Másik konténer előkészítése és a megosztott kötet használatba vétele: 

```ksh
$ docker ps
CONTAINER ID   IMAGE        COMMAND   CREATED         STATUS         PORTS     NAMES
08b6dd403b72   rockylinux   "bash"    2 minutes ago   Up 2 minutes             sleepy_galois

$ docker run -ti --volumes-from sleepy_galois rockylinux bash

[root@68890491108f /]# ls -la /inventory
total 8
drwxr-xr-x. 2 root root 4096 May 18 08:22 .
drwxr-xr-x. 1 root root 4096 May 18 08:23 ..
-rw-r--r--. 1 root root    0 May 18 08:22 an_empty_file

[root@68890491108f /]# cat >> /inventory/an_empty_file
It's not empty anymore.
```

A tartalom azonnal látható és használható az első konténerben is természetesen. Ahogyan az első konténert leállítjuk, a kötet továbbra is használható a másik által (és igény esetén a harmadik, negyedik stb. által is). Azonban amint az utolsó ilyen konténert is leállítjuk, az adat elvész. 

### 4.6 Space management

No matter what user we use for docker commands, the images and all the saved data is under `/var/lib/docker` by default, which may eventually take too much space. E.g.:

```ksh
# cd /var/lib/docker

# du -sm * | sort -n
1	buildkit
1	containerd
1	containers
1	network
1	plugins
1	runtimes
1	swarm
1	tmp
1	trust
1	volumes
11	image
3405	overlay2

# df -hPT .
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv ext4   30G   20G  8.6G  70% /
```

If we want to save some space, the easiest way is to just move the `docker` folder somewhere else and create a symlink when `dockerd` is NOT running. The other way is to use an `/etc/docker/daemon.json` and define it there, like this: 

```
[...]
  "data-root": "/path/to/docker-data",
[...]
```

There is nothing wrong with the symlink method, so let's go for it. In this example, there is huge amount of free space under `/home`, so our `docker` user's home is a good fit. 

It is also good to know that the `dockerd` always fixes the permissions under its `/var/lib/docker`, so if the `docker` user's `/home/docker/.docker` is thought as a good candidate, it also stores personal data for the user, so better to have a separated directory for it. 

We will act accordingly: 

```ksh
# df -hPT /home
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-homelv ext4  423G   66G  336G  17% /home

# pwd
/var/lib/docker

# ls -la /home/docker/.docker
total 12
drwx------ 2 docker docker 4096 Aug 30 15:36 .
drwx------ 9 docker docker 4096 Sep  1 17:46 ..
-rw------- 1 docker docker  127 Aug 30 15:36 config.json

# mkdir /home/docker/.docker/data

# mv * /home/docker/.docker/data

# cd ..

# rmdir docker

# ln -s /home/docker/.docker/data docker

# dockerd
INFO[2022-09-02T20:40:24.994731607+03:00] Starting up
[...]
```


<a name="5"></a>
## 5. Hálózat

...

### 5.1 A konténer IP-je

A futó konténer IP-jének megkaparintása (akár a `CONTAINER ID`-ra, akár a nevére való kereséssel működik ez): 

```ksh
$ docker ps
CONTAINER ID   IMAGE                   COMMAND   CREATED              STATUS              PORTS     NAMES
879511857cb2   rockylinux/rockylinux   "bash"    About a minute ago   Up About a minute             romantic_cartwright

$ docker inspect 879511857cb2 | grep IPAddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```

...

### 5.2 Csatlakozás SSH-val

A legjobb leírást erről [itt](https://www.linkedin.com/pulse/ssh-docker-container-centos-yashwanth-medisetti/) találtam meg (CentOS-hez, de ugyanúgy alkalmazható Rocky Linuxhoz). 

```ksh
$ docker run -dit --name rockyssh --privileged rockylinux/rockylinux /usr/sbin/init
99b22e9529e1873b42c21f9acbcda9d98b6bf1db857019b87dee1fc1a5c5e395

[docker@rockysrv1 ~]$ docker ps
CONTAINER ID   IMAGE                   COMMAND            CREATED         STATUS         PORTS     NAMES
99b22e9529e1   rockylinux/rockylinux   "/usr/sbin/init"   6 seconds ago   Up 5 seconds             rockyssh

[docker@rockysrv1 ~]$ docker exec -it rockyssh bash
[root@99b22e9529e1 /]# 
[root@99b22e9529e1 /]# dnf install -y openssh-server openssh-clients net-tools passwd
[...]
Complete!
[root@99b22e9529e1 /]# systemctl start sshd
[root@99b22e9529e1 /]# passwd root
Changing password for user root.
New password:
BAD PASSWORD: The password fails the dictionary check - it is based on a dictionary word
Retype new password:
passwd: all authentication tokens updated successfully.
[root@99b22e9529e1 /]#
```

Ezután egy másik session-ből már menni is fog az SSH-zás: 

```ksh
$ ssh root@172.17.0.2
...
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ECDSA) to the list of known hosts.
root@172.17.0.2's password:
[root@99b22e9529e1 ~]#
```


<a name="6"></a>
## 6. A Docker registry-k

A registry-k az image-ek forrásai, ahonnan és ahová le- illetve feltölthetjük az image-eket. A fentiekben már sokat foglalkoztunk a Docker (mint cég) saját registry-jével (vagy repository-jával), a Docker Hubbal. 

### 6.1 Bejelentkezés a Docker Hubra

```ksh
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: 
[...]
```

Belejentkezés után mehet is a saját image-ünk publikálása. Itt pl. egy már korábban letöltött image `tag`-elése, majd `push`-olása: 

```ksh
$ docker tag debian:sid gjakab/test-image-42:v99.9

$ docker push gjakab/test-image-42:v99.9
[...]
```

### 6.2 Registry szerver építése

A hivatalos [docs.docker.com](https://docs.docker.com/registry/deploying/) leírja ennek menetét. 

A lényeg, hogy még maga a registry is egy Docker konténerben fut. Érdemes neki a gazdagép fájlrendszeréből állandó tárhelyet adni, pl.: 

```ksh
$ docker run -d -p 5000:5000 --restart=always --name registry -v /mnt/registry:/var/lib/registry registry:2
[...]
```

Hogy a registry-nk más gépek számára is elérhető legyen, biztonságossá kell azt előbb tenni TLS-el. Itt jön képbe a **Let’s Encrypt** használatba vétele is, ami automatikusan állítja be a browser-trusted certificate-et. Lehet self-signed certificate-et is használni (csak tesztelési célra ajánlott), amiről [itt](https://docs.docker.com/registry/insecure/) ír a Docker dokumentáció. 

>Ez a fejezet még elég hiányos.


<a name="7"></a>
## 7. A `Dockerfile`

Segítségével egyből úgy indíthatunk el egy konténert egy kezdetleges állapotból, hogy azon utána sorban haladva előre konfigurálhatunk, másolhatunk, telepíthetünk komponenseket. 

Egy példa a gyors megértéshez: 

```
FROM rocky:latest

LABEL maintainer="gjakab@example.com"
RUN dnf update -y
RUN dnf upgrade -y
RUN dnf install epel-release -y
RUN dnf install ansible git -y
```

Az adott könyvtárban lévő `Dockerfile` nevű állományban a fentieket tartva nincs más dolgunk, mint meghívjuk a `docker build` parancsot megfelelően felparaméterezve: 

```ksh
$ docker build --tag rockyansible .
Sending build context to Docker daemon  9.728kB
Step 1/6 : FROM rocky:latest
 ---> 991f1cea8a22
Step 2/6 : LABEL maintainer="gjakab@example.com"
 ---> Running in 827d0284f650
Removing intermediate container 827d0284f650
 ---> 913223dbc58a
Step 3/6 : RUN dnf update -y
 ---> Running in 2787cdb359f8
Rocky Linux 8 - AppStream                       8.5 MB/s | 7.8 MB     00:00    
Rocky Linux 8 - BaseOS                          4.7 MB/s | 2.6 MB     00:00    
Rocky Linux 8 - Extras                           35 kB/s |  11 kB     00:00    
Dependencies resolved.
Nothing to do.
Complete!
Removing intermediate container 2787cdb359f8
 ---> 03b53075df68
Step 4/6 : RUN dnf upgrade -y
 ---> Running in e7b281c684e3
[...]
 ---> cb6174fdafce
Step 5/6 : RUN dnf install epel-release -y
 ---> Running in cceffc46bb79
[...]
 ---> 2843f70ec76a
Step 6/6 : RUN dnf install ansible git -y
 ---> Running in 810838c36f47
[...]
Complete!
Removing intermediate container 810838c36f47
 ---> 53260ff9151f
Successfully built 53260ff9151f
Successfully tagged rockyansible:latest

$ docker images
REPOSITORY            TAG       IMAGE ID       CREATED         SIZE
rockyansible          latest    53260ff9151f   4 minutes ago   683MB
rocky                 latest    991f1cea8a22   19 hours ago    314MB
portainer/portainer   latest    12b0b8dced14   10 days ago     75.4MB
```


...

>Ez a fejezet még sokat bővül.

<a name="8"></a>
## 8. Docker image építése a semmiből

A `Dockerfile` alapvető része a `FROM` paraméter, aminek értéke a `scratch` is lehet. Ennek használatakor azonban szó szerint egy teljesen üres konténert kapunk, amiben még shell sincs, emiatt folyton csak az alábbiakhoz hasonló hibaüzenetekkel találkozhatunk, ha bármit szeretnénk kezdeni egy ilyen `Dockerfile` alapján épített konténerrel: 

```ksh
[...]
failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: "/bin/sh": stat /bin/sh: no such file or directory: unknown
[...]
docker: Error response from daemon: failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: "/usr/bin/bash": stat /usr/bin/bash: no such file or directory: unknown.
[...]
```

Az alábbiak egy egészen más megközelítést alkalmaznak: készítsünk elő egy teljesen szeparált fájlstruktúrát, megfelelően telepített csomagokkal, amit a `tar`-ral arhíválunk, majd rögtön a `docker import`-al Docker image-é alakítunk. 

A leírás eredeti forrása: [www.sidorenko.io/post/2016/07/creating-container-base-image-of-centos/](https://www.sidorenko.io/post/2016/07/creating-container-base-image-of-centos/). 

### 8.1 Előkészületek

Ajánlott, hogy pontosan olyan rendszeren csináld mindezt, amilyen disztribúciót tartalmazó konténert szeretnél is készíteni. Jelen esetben még Rocky 8.5-el futott a VM, de simán elkészíthettem vele egy épp megjelenés alatt álló Rocky 8.6-ot. 

A Docker természetesen telepítve kell legyen (akár a fenti sima, `.tgz` arhívumból való, manuális telepítéssel) és a `dockerd` daemon fusson. A műveleteket `root`-ként tudjuk elvégezni. 

### 8.2 Az image elkészítése

1. Készítsd el a könyvtárat az új root struktúrához:

```ksh
# export rocky_root='/rocky_image/rootfs'

# mkdir -p $rocky_root
```

2. Inicializáld az `rpm` database-t: 

```ksh
# rpm --root $rocky_root --initdb
```

3. Töltsd le, majd telepítsd a `rocky-release` csomagot, ami majd a mi repo forrásainkat tartalmazza: 

Ehhez először keresd meg, hogy mik az épp aktuális csomagok nevei a [download.rockylinux.org/...](https://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os/Packages/r) oldalon. 

```ksh
# wget https://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os/Packages/r/rocky-release-8.6-2.el8.noarch.rpm
[...]

# wget https://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os/Packages/r/rocky-repos-8.6-2.el8.noarch.rpm
[...]

# wget https://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os/Packages/r/rocky-gpg-keys-8.6-2.el8.noarch.rpm
[...]

# rpm --root $rocky_root -ivh rocky-release*rpm rocky-repos*rpm rocky-gpg-keys*rpm
[...]

# rpm --root $rocky_root --import $rocky_root/etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial
```

4. Telepíts a `dnf`-el az új root-unk alá docs és egyéb nyelvi fájlok nélkül (mondjuk ez a rész itt failelt, amint látható, így már más lehet a paraméter): 

```ksh
# dnf -y --installroot=$rocky_root --setopt=tsflags='nodocs' --setopt=override_install_langs=en_US.utf8 install dnf
Main config did not have a override_install_langs attr. before setopt
[...]
```

5. Konfiguráld a `dnf`-et úgy, hogy ne telepítsen `docs` és mindenféle más nyelvet tartalmazó csomagokat az angolon kívül: 

```ksh
# sed -i "/distroverpkg=rocky-release/a override_install_langs=en_US.utf8\ntsflags=nodocs" $rocky_root/etc/dnf/dnf.conf
```

6. `chroot`-olj az új környezetbe és telepíts néhány további tool-t:

```ksh
# cp /etc/resolv.conf $rocky_root/etc

# chroot $rocky_root /bin/bash &lt;&lt;EOF
dnf install -y procps-ng iputils
dnf clean all
EOF

# rm -f $rocky_root/etc/resolv.conf
```

7. Még egy utolsó pucolás (egy feleslegesnek tűnő, 200+ MB-os fájlt találtam): 

```ksh
# cd $rocky_root/usr/lib/locale/

# ls -la
total 212684
drwxr-xr-x.  3 root root      4096 May 18 15:07 .
dr-xr-xr-x. 24 root root      4096 May 18 15:07 ..
drwxr-xr-x.  3 root root      4096 May 18 15:06 C.utf8
-rw-r--r--.  1 root root 217800224 May 18 15:07 locale-archive
-rw-r--r--.  1 root root         0 May 18 15:07 locale-archive.tmpl

# >locale-archive

# ls -la
[...]
-rw-r--r--.  1 root root    0 May 18 15:34 locale-archive
[...]
```

8. Arhíváld, majd egyből importáld a Docker image-et: 

```ksh
# tar -C $rocky_root -c . | docker import - rocky
[...]

# docker images
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
rocky        latest    991f1cea8a22   4 minutes ago   314MB
```

### 8.3 A konténer indítása és tesztelése

```ksh
$ docker run --rm rocky cat /etc/rocky-release
Rocky Linux release 8.6 (Green Obsidian)

# docker run -ti rocky bash

bash-4.4# uname -a
Linux 3d5544c6842a 4.18.0-348.23.1.el8_5.x86_64 #1 SMP Wed Apr 27 15:32:52 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux

bash-4.4# exit              
exit

# docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED              STATUS                      PORTS     NAMES
3d5544c6842a   rocky     "bash"    About a minute ago   Exited (1) 2 seconds ago              gallant_babbage
```


<a name="9"></a>
## 9. A Portainer UI

Sok webes GUI elérhető a Dockerhez, de most épp ez tetszett a legjobban. 

Telepítést sem igényel különösebben, hiszen csak a webes applikációt előre telepítve és konfigurálva tartalmazó image-et kell megfelelően felparaméterezve elindítanunk, és már használható is. 

### 9.1 Előkészületek

Fontos, hogy mint minden más GUI, ez iss a `docker-proxy` binárist használja, így ha az nincs benne a hagyományos elérési útvonalak között (még ha benne is van pl. az `/opt/docker` a felhasználónk `$PATH` változójában, akkor hibaüzenetet kapunk. Bizonyosodjunk meg hát arról, hogy az legalább megfelelően linkelve van. Ha nincs, akkor...

```ksh
# ln -s /opt/docker/docker-proxy /usr/bin
```

### 9.2 Indítás

Én azért szenvedtem, mert a gyárilag előírt helyi `9000`-es port valamiért foglalt volt már, így próbáltam mást, a `9999`-et: 

```ksh
# docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

# docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
7d9676afa8489c87a725e85558669099a1af41492f5e5dc6cffe65e1ece091e1
docker: Error response from daemon: driver failed programming external connectivity on endpoint exciting_banach (49cb167e24af87171662ca4e7609044ea68b4722b9d1a7bce4d97029f3c5f0a6): Bind for 0.0.0.0:9000 failed: port is already allocated.

# docker run -d -p 9999:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
8225d7a924d3f52618911c77e22131f9623b18fa3d929c30d007248371d38cd1
[root@rockysrv2 ~]# docker ps
CONTAINER ID   IMAGE                 COMMAND        CREATED         STATUS         PORTS                                       NAMES
8225d7a924d3   portainer/portainer   "/portainer"   7 seconds ago   Up 6 seconds   0.0.0.0:9999->9000/tcp, :::9999->9000/tcp   sleepy_heyrovsky

# ip -4 a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 192.168.56.102/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
[...]
```

Ez a dolog is VM-en belül fut nálam (de persze lokálisan is lehet Dockerezni), így a [192.168.56.102:9999](http://192.168.56.102:9999) címet használtam, de lokálist Dockerezés esetében ez [localhost:9999](http://localhost:9999)

### 9.3 Regisztráció és körülnézés

Elsőként egy felhasználó elkészítésével fogad: 

![alt_text](portainer_-_initial_administrator.png)

Kapcsolódás a helyi Docker környezethez: 

![alt_text](portainer_-_connect_to_local.png)

A helyi konténerekhez tartozó **Home** képernyő: 

![alt_text](portainer_-_local_home.png)

Helyi konténerek listája és menedzselése: 

![alt_text](portainer_-_container_list.png)
