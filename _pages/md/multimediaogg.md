# 🎵 OGG Vorbis

<div style="text-align: right;"><h7>Posted: 2023-10-04 12:12</h7></div>

###### .

![alt_text](ogg_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Converting](#2)


<a name="1"></a>
## 1. Introduction

Ogg is a free, open container format maintained by the **Xiph.Org Foundation**. It was initially released in **2004**, but it became significantly more popular around **2007**, when MP3 really started to be identified as a proprietary format, and even several software developers had to remove the factory default MP3 support from their products, licensed the support separately. 

On **May 16, 2007**, the **Free Software Foundation** started a campaign to increase the use of Vorbis...

>as an ethically, legally and technically superior audio alternative to the proprietary MP3 format.

And indeed, OGG provides not even fully opensource solution but provides better quality with slightly less filesize.


<a name="2"></a>
## 2. Converting

Once you're done with reviewing/fixing the "Song Info" (ID3 tags) of your lossless audio files, here is an example for converting all the `.flac` files under your current directory with `9` quality, which results **~320 kbps**, variable bitrate `.ogg` files with less than 20% in size in an average: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">for i in *flac ; do echo $i ; oggenc -q 9 $i ; echo ----- ; done</span>
01_-_Komatic_-_Make_Me_Feel.flac
Opening with flac module: FLAC file reader
Encoding "01_-_Komatic_-_Make_Me_Feel.flac" to 
         "01_-_Komatic_-_Make_Me_Feel.ogg" 
at quality 9.00
	[100.0%] [ 0m00s remaining] | 
[...]
Done encoding file "01_-_Komatic_-_Make_Me_Feel.ogg"
[...]
	File length:  4m 57.0s
	Elapsed time: 0m 03.1s
	Rate:         95.6003
	Average bitrate: 358.5 kb/s
[...]
-----
02_-_Technicolour_-_Satisfy.flac
Opening with flac module: FLAC file reader
[...]
</pre>

...
