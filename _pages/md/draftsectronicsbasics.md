# 🔋 Basics about Electronic Components

<div style="text-align: right;"><h7>Posted: 2022-11-09 9:13</h7></div>

###### .

## Table of contents

1. [Introduction](#1)
2. [Electromechanics](#2)
3. [Passives](#3)
4. [Active Discrete Components](#4)
5. [Optoelectronics](#4)
6. [Integrated Circuits](#4)

![alt_text](basics_bg.jpg)

<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Electromechanics

...


<a name="3"></a>
## 3. Passives

...


<a name="4"></a>
## 4. Active Discrete Components

...

- [How does the diode mode of a multimeter work](https://electronics.stackexchange.com/questions/426442/how-does-the-diode-mode-of-a-multimeter-work)

...

### Diodes

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="diode.svg" title="" class="cat">
  </div>
</div>

...

#### Special Diodes


##### Schottky diodes

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="diode_-_schottky.svg" title="" class="cat">
  </div>
</div>

A normal diode will have a voltage drop between 0.6 to 1.7 volts, while a Schottky diode voltage drop is usually between 0.15 and 0.45 volts. This lower voltage drop provides better system efficiency and higher switching speed.

...



### UJT

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="ujt.svg" title="" class="cat">
  </div>
</div>

...

### Transistors

...

#### Bipolar Transistors, BJTs

...

This semiconductor family is also known as **BJT** (Bipolar Junction Transistor). 

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="bjt_-_npn.svg" title="" class="cat">
    <img src="bjt_-_pnp.svg" title="" class="cat">
  </div>
</div>

...

#### Unipolar Transistors

...

##### JFETs

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="jfet_-_n.svg" title="" class="cat">
    <img src="jfet_-_p.svg" title="" class="cat">
  </div>
</div>

...

- To turn an N-channel JFET off, we need to make the gate *negative* with respect to the Source.
- To turn an P-channel JFET off, we need to make the gate *positive* with respect to the Source.

...

- [Introduction to Junction Field-effect Transistors (JFET)](https://www.allaboutcircuits.com/textbook/semiconductors/chpt-5/junction-field-effect-transistors-jfet/)

...

##### MOSFETs

There are several good articles about the basics, the differences, and the operating models. Here are three of them: 
- [electricaltechnology.org: Difference Between D-MOSFET and E-MOSFET](https://www.electricaltechnology.org/2021/04/difference-depletion-enhancement-mosfet.html)
- [electronicshub.org: Introduction to MOSFET: Enhancement, Depletion, Amplifier, Applications](https://www.electronicshub.org/mosfet/)
- [aldinc.com: An introduction to Depletion-mode MOSFETs](https://www.aldinc.com/pdf/IntroDepletionModeMOSFET.pdf)
- [rfwireless-world.com: Difference between DMOS, VMOS, LDMOS](https://www.rfwireless-world.com/Terminology/DMOS-vs-VMOS-vs-LDMOS.html)
- [allaboutcircuits.com: Insulated Gate Field-Effect Transistors](https://www.allaboutcircuits.com/worksheets/insulated-gate-field-effect-transistors/)

**In theory**, there are 4 main types of them: 

- **Enhancement-Mode** MOSFETs (**E-MOSFETs**) - the *"normally open"* or *"normally off"* switches: 
    - **N**-Channel Enhancement-Mode MOSFETs
    - **P**-Channel Enhancement-Mode MOSFETs
- **Depletion-Mode** MOSFETs (**D-MOSFETs**) - the *"normally closed"* or *"normally-on"* switches: 
    - **N**-Channel Depletion-Mode MOSFETs
    - **P**-Channel Depletion-Mode MOSFETs

**Enhancement-Mode** MOSFETs: 

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="mosfet_-_e_-_n.svg" title="" class="cat">
    <img src="mosfet_-_e_-_p.svg" title="" class="cat">
  </div>
</div>

...

In almost all the datasheets, these components are drawn semantically with an **additional "protection" diode** (see the below diagram), but in reality, these are usually **not additional diodes**, built-in the packaging. It comes naturally from the design of the component: 
- In an **N-Channel MOSFET**, there is a "natural" diode in forward biased direction, **from the Terminal to the Drain**. As the Terminal is directly connected with the Source, therefore, the diode is **forward biased from Source to Drain**. 
- In a **P-Channel MOSFET**, the diode points the other way around: the Terminal is a big N-layer, which is **reverse biased from Source to Drain**. 

![alt_text](mosfet_-_e_-_in_datasheets.svg)

Potential test circuits for N and P Channel, Enhancement-Mode MOSFETs: 

![alt_text](mosfet_-_e_-_test_circuits.svg)

...

**Depletion-Mode** MOSFETs: 

...
<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="mosfet_-_d_-_n.svg" title="" class="cat">
    <img src="mosfet_-_d_-_p.svg" title="" class="cat">
  </div>
</div>

Although MOSFETs can be made in either polarity, N-channel MOSFETs are available in all four types while P-Channel Depletion-Mode devices are **not generally available**. However, manufacturers sometimes create P-Channel Depletion-Mode devices during the manufacture of certain analog and digital ICs.

The MOSFET families can be divided furhter by their structure and manufacturing technology. The below list is not complete, but these are the most known ones: 

- **DMOS**: **Double-Diffused Metal Oxide Silicon**; it is the most common one for discrete MOSFET components
- **VMOS**: **Vertical MOS**; both for power amplifiers and switching
- **VDMOS**: **Vertical DMOS**; the most common sulution for power MOSFETs
- **LDMOS**: **Lateral DMOS**; these are asymmetric power MOSFET devices


#### IGBTs

...

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="igbt_-_n.svg" title="" class="cat">
    <img src="igbt_-_p.svg" title="" class="cat">
  </div>
</div>


<a name="5"></a>
## 5. Optoelectronics

...


<a name="6"></a>
## 6. Integrated Circuits

...

![alt_text](ic_-_dip14.svg)

...
