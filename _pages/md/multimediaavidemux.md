# 🎬 Avidemux / ffmpeg

<div style="text-align: right;"><h7>Posted: 2022-08-27 21:44</h7></div>

###### .

![alt_text](avidemux_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Avidemux](#2)
3. [ffmpeg](#3)


<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Avidemux

Not sure if it's solved by now but in ~2020, it was not a dependency on Linux and GNOME Shell with Wayland rendering. So, in order to install Avidemux on Linux, you also need to have the `qt5` library: 


<pre class="con">
<span class="psu">$</span> <span class="cmd">rpm -qa | grep avidemux</span>
avidemux3-qt5-2.8.0-1.2.x86_64
avidemux3-2.8.0-1.2.x86_64
</pre>

...

<a name="3"></a>
## 3. ffmpeg

Sometimes it is needed to mass encode/convert your videos. If you really don't have to edit anything else but re-encoding, the `ffmpeg` is one of the best tool for that.

### 3.1 Introduction to ffmpeg

The command `ffmpeg` has lots of parameters. Getting help: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">ffmpeg -h</span>
[...]
    -h      -- print basic options
    -h long -- print more options
    -h full -- print all options (including all format and codec specific options, very long)
    -h type=name -- print all options for the named decoder/encoder/demuxer/muxer/filter/bsf/protocol
    See man ffmpeg for detailed description of the options.
[...]
</pre>

Below is an example for re-encoding a video using `libx264` encoder library with the quality of `25`, while the audio part is just a direct copy. It has absolutely the same outcome when you open a videofile in Avidemux, then you go to **Video Output** &rarr; **Configure** &rarr; **General** tab &rarr; set the **Quality** to `25`, and save the video. 

Theoretically, the procedure takes even some seconds less with `ffmpeg` per file, but it's much easier to be scripted: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">ffmpeg -i myvideo.mp4 -c:v libx264 -preset fast -crf 25 -c:a copy myvideo_re-encoded.mp4</span>
[...]
</pre>

The tool's [website](https://trac.ffmpeg.org/wiki/Encode/H.264) also provides really good hints, how to use the tool. 

### 3.2 Mass conversion

In this section, we try to achieve the same with multiple video files. Make sure in advance the file names don't include spaces in advance.

<pre class="con">
<span class="psu">$</span> <span class="cmd">mkdir Re-encoded</span>

<span class="psu">$</span> <span class="cmd">ls -la</span>
total 6522964
drwxr-xr-x 3 gjakab users       4096 Sep 10 10:32 .
drwxr-xr-x 3 gjakab users       4096 Sep 10 10:08 ..
-rw-r--r-- 1 gjakab users  475996380 Jan  9  2018 Adam_csirket_eszik.MP4
-rw-r--r-- 1 gjakab users  671426308 Sep 10 10:08 Alagutak.MP4
-rw-r--r-- 1 gjakab users  542820631 Aug  5  2018 Donka_eskuvo_enek_02.mp4
-rw-r--r-- 1 gjakab users  567937296 Feb  3  2018 Farsang_Mamma_mia_02.MP4
-rw-r--r-- 1 gjakab users 1327608496 Oct 21  2018 Handel_Passacaglia.MP4
-rw-r--r-- 1 gjakab users  738237404 Aug 29  2018 Palermo_31.mp4
-rw-r--r-- 1 gjakab users 2355432576 Dec 25  2018 Petike_ajandekot_bont.MP4
drwxr-xr-x 2 gjakab users       4096 Sep 10 10:32 Re-encoded

<span class="psu">$</span> <span class="cmd">for i in *.* ; do echo $i ; ffmpeg -i $i -c:v libx264 -preset fast -crf 25 -c:a copy Re-encoded/$i ; echo ----- ; done</span>
[...]
</pre>

#### 3.2.1 Preserving timestamps

If you also want to even keep the **original timestamps**, you can use the combination of `ls` and `touch` commands. 

The relevant sections in their `man`, and checking if `ls` really works with custom `TIME_STYLE`: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">man touch</span>
[...]
       -t STAMP
              use [[CC]YY]MMDDhhmm[.ss] instead of current time
[...]

<span class="psu">$</span> <span class="cmd">man ls</span>
[...]
       --time-style=TIME_STYLE
              time/date format with -l; see TIME_STYLE below
[...]

<span class="psu">$</span> <span class="cmd">ls -l --time-style="+%Y%m%d%H%M.%S"</span>
total 6522956
-rw-r--r-- 1 gjakab users  475996380 201801091930.00 Adam_csirket_eszik.MP4
[...]
</pre>

Improving the above, mass conversion script with this feature: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">for i in *.* ; do echo $i ; ffmpeg -i $i -c:v libx264 -preset fast -crf 25 -c:a copy Re-encoded/$i ; TS=$(ls -l --time-style="+%Y%m%d%H%M.%S" | awk "/$i/{print \$6}") ; touch -t $TS Re-encoded/$i ; echo ----- ; done</span>
[...]
</pre>

Now let's say you keep your videos in folders by year, then you also prepared these folders by the previously favored `Re-encoded` sub-folders. Here's how you can improve the previous solution with even entering the `<DATE>` directories: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -type d</span>
.
./2022
./2022/Re-encoded
./2021
./2021/Re-encoded
./2020
./2020/Re-encoded
<span class="psu">$</span> <span class="cmd">for DATE in {2020..2022} ; do echo $DATE ; cd $DATE ; for i in *.* ; do echo $i ; ffmpeg -i $i -c:v libx264 -preset fast -crf 25 -c:a copy Re-encoded/$i ; TS=$(ls -l --time-style="+%Y%m%d%H%M.%S" | awk "/$i/{print \$6}") ; touch -t $TS Re-encoded/$i ; echo ----- ; sleep 10 ; done ; echo "##########" ; cd .. ; sleep 20 ; done</span>
[...]
</pre>

Of course, it uses significant **CPU power**. The cores became fully used: 

![alt_text](ffmpeg_mass_encoding_-_resources.png)

...and their temperature also becomes higher than usual after some hours of heavy encoding (in this case, it was a laptop with 4, 11th Gen Intel(R) Core(TM) i7-1185G7 @ 3.00GHz cores, and usually the cores are around 40.0°C): 

<pre class="con">
<span class="psu">$</span> <span class="cmd">sensors</span>
[...]
coretemp-isa-0000
Adapter: ISA adapter
Package id 0:  +62.0°C  (high = +100.0°C, crit = +100.0°C)
Core 0:        +62.0°C  (high = +100.0°C, crit = +100.0°C)
Core 1:        +61.0°C  (high = +100.0°C, crit = +100.0°C)
Core 2:        +59.0°C  (high = +100.0°C, crit = +100.0°C)
Core 3:        +59.0°C  (high = +100.0°C, crit = +100.0°C)
[...]
</pre>
