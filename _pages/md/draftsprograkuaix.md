# ![alt text](/_ui/img/icons/raku_m.svg) Raku fordítása AIX-on

###### .

## Tartalomjegyzék

1. [Előfeltételek](#1)
2. [Jelenlegi extra tennivalók](#2)
3. [A fordítás menete](#3)
4. [Korábbi gondok AIX-en](#4)
5. [Letöltés](#5)

<a name="1"></a>
## 1. Előfeltételek

Az alábbiak a jelenleg legújabb 2021.02-es Raku 6.d fordításához adnak segítséget.

A források (MoarVM, NQP és Rakudo) az Rakuról szóló [általános leírásban](..) leírtak szerint tölthetőek le. 

### 1.1 Csomagok

AIX-en telepíteni kell RPM-ből az alábbi csomagokat mindenképpen:
- gcc
- make
- libffi
- libffi-devel

Ezek az alábbi verziójukkal biztosan működőképesek: 

```ksh
# rpm -qa | grep -E "gcc-|make-|libffi-"
gcc-8.2.0-2.ppc
make-4.2.1-4.ppc
libgcc-8.2.0-2.ppc
gcc-cpp-8.2.0-2.ppc
libffi-3.2.1-2.ppc
libffi-devel-3.2.1-2.ppc
```

### 1.2 A `make` átlinkelése

A GNU C fordító használatához az IBM saját `make` parancsát a `/usr/bin/make`-től unlinkelni kell, valamint helyette a GNU `make`-et linkelni: 

```ksh
# ls -l `which make`
lrwxrwxrwx    1 bin      bin              17 Aug 23 2013  /usr/bin/make -> /usr/ccs/bin/make

# unlink /usr/bin/make

# ln -s /opt/freeware/bin/make /usr/bin/make
```

### 1.3 A `LIBPATH`

A `$LIBPATH` változónak a Raku `lib` könyvtárára kell mutatnia: 
```ksh
# export LIBPATH=/opt/raku/lib
```

<a name="2"></a>
## 2. Jelenlegi extra tennivalók

Az alábbiak most (2021-03-04) még hibák, de a következő kiadásban talán már nem lesznek benne. Éppen emiatt, egy új verzió letöltésekor érdemes ezeket ellenőriznünk, mielőtt a következő lépést végrehajtjuk. 

A lépéseket a következő fejezetben egyesével, részletesen áttekintjük, itt most csak a jelenlegi hibára hívnám fel a figyelmet: 

- az **NQP** és a **Rakudo** esetében ezt a hibát kapjuk a `make` kiadására:
```ksh
gcc: error: ,-bmaxdata:0x80000000: No such file or directory
```
Ennek oka, hogy a `Makefile` egy szintaktikai hibát tartalmaz AIX alatt. 

<a name="3"></a>
## 3. A fordítás menete

### 3.1 MoarVM

#### 3.1.1 Előkészületek

Először is menjünk be a kicsomagolt MoarVM forrásának könyvtárába, majd...

```ksh
# vi build/setup.pm
```

Itt az `OS_AIX` részben a `ldmiscflags         => '-Wl,-brtl',` javítása erősen ajánlott a `ldmiscflags         => '-Wl,-bmaxdata:0x80000000,-brtl',`-ra. 

A teljes blokk a változtatás után: 
```
...
our %OS_AIX = (
    %OS_POSIX,

    defs                => [ qw( _ALL_SOURCE _XOPEN_SOURCE=500 _LINUX_SOURCE_COMPAT ) ],
    syslibs             => [ @{$OS_POSIX{syslibs}}, qw( rt dl perfstat ) ],
    ldmiscflags         => '-Wl,-bmaxdata:0x80000000,-brtl',
    ldrpath             => '-L"/@libdir@"',
    ldrpath_relocatable => '-L"/@libdir@"',
...
```

#### 3.1.2 A fordítás

A fordítás csak úgy megy le, ha konfiguráláskor a LibFFI-t is igénybe vesszük, de egyébként a fordítás menete a Linuxokon megszokott. 

```ksh
# perl Configure.pl --has-libffi --prefix /opt/raku
...

# make
...

# make install
...
```

#### 3.1.3 Teszt

```ksh
# /opt/raku/bin/moar --version
This is MoarVM version 2021.02
```

Ha a `libmoar.so` fájlt hiányolja, akkor nem adtuk meg a `$LIBPATH`-ot az elején. 

### 3.2 NQP

#### 3.2.1 Configure

```ksh
# perl Configure.pl --prefix /opt/raku
...
```

#### 3.2.2 A `Makefile` javítása

Tehát `vi Makefile`, majd ezt a részt:

```
...
M_LD_INST_NQP=gcc -Wl,-bmaxdata:0x80000000,-brtl -O3 -DNDEBUG ,-bmaxdata:0x80000000 -L"//opt/raku/lib"
...
M_LD_BUILD_RUNNER=gcc -Wl,-bmaxdata:0x80000000,-brtl -O3 -DNDEBUG ,-bmaxdata:0x80000000 -L"//opt/raku/lib"
...
```

...cseréljük le erre (a második `,-bmaxdata:0x80000000` hibásan kerül bele a configure során, tehát azt vegyük ki): 

```
...
M_LD_INST_NQP=gcc -Wl,-bmaxdata:0x80000000,-brtl -O3 -DNDEBUG -L"//opt/raku/lib"
...
M_LD_BUILD_RUNNER=gcc -Wl,-bmaxdata:0x80000000,-brtl -O3 -DNDEBUG -L"//opt/raku/lib"
...
```

#### 3.2.3 A fordítás

```ksh
# make
...

# make test
...

# make install
...
```

A `make test` során kaphatunk hibákat, de ezeket kézzel megfuttatva azt kell tapasztalnunk, hogy külön meghívva nincs probléma, pl.: 

```ksh
# ./nqp-m t/nqp/111-spawnprocasync.t
1..4
ok 1 - called the ready callback once
ok 2 - got the correct output on stdout
ok 3 - called the ready callback once
ok 4 - got the correct output on stderr
```

### 3.3 Rakudo

#### 3.3.1 Configure

```ksh
# perl Configure.pl --prefix /opt/raku
...
```

#### 3.3.2 A `Makefile` javítása

Itt ugyanúgy ki kell venni a hibásan beszúrt `,-bmaxdata:0x80000000` részt, mint az NQP esetében, de már csak egy helyen, tehát `vi Makefile` következik itt.

#### 3.3.3 A fordítás

```ksh
# make
...

# make test
...

# make install
...
```

Amennyiben nem volt még korábbi verzió, akkor az `/usr/bin/raku` linket is érdemes elkészíteni: 
```ksh
# ln -s /opt/raku/bin/raku /usr/bin/raku
```

#### 3.3.4 A végeredmény

```ksh
# raku -v
Welcome to Rakudo(tm) v2021.02.1.
Implementing the Raku(tm) programming language v6.d.
Built on MoarVM version 2021.02.

# raku -e 'print "Yeyy, it is Raku "~ $*RAKU.version ~" on ";shell "uname";'
Yeyy, it is Raku 6.d on AIX
```

Az elkészült `/opt/raku` struktúrát becsomagolhatjuk (pl. az `/opt` alatt taroljuk, majd gzip-eljük egy `raku-6.d-2021.02.1-aix7.tar.gz` nevű állományba), majd terjeszthetjük más AIX rendszerekre. 

<a name="4"></a>
## 4. Korábbi gondok AIX-en

Korábbi plusz tennivalók a MoarVM fordításához, amik mindegyikét a fejlesztők megoldották már az én megkeresésem nyomán: 

### 4.1 `vi src/io/syncfile.c`  
Itt a "STAT" szó 4 előfordulását ki kell cserélni valami másra, pl. `THESTAT`-ra. A leggyorsabb ezt a VI-ban így tehetjük meg: `:%s/STAT/THESTAT/g`

### 4.2 `vi src/strings/siphash/csiphash.h`  
Itt az Endian-ságot kell javítani a <endian.h> rész <sys/machine.h>-re való cseréjével, hogy végül így nézzen ki ez a rész a fájlban:  
```
...
  if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__)
    include <sys/endian.h>
  elif defined(_AIX)
    include <sys/machine.h>
  else
    include <endian.h>
  endif
...
```

### 4.3 `vi build/Makefile.in`  
Itt az alábbi 2. sort is be kellett szúrni:  
```
...
UV_AIX = 3rdparty/libuv/src/unix/aix@obj@ \
         3rdparty/libuv/src/unix/aix-common@obj@ \
         $(UV_UNIX)
...
```
### 4.4 `vi build/setup.pm` 
Itt az `ldmiscflags` részt kellett javítani, hogy megfelelő `LDFLAGS`-t generáljon a létrejövő `Makefile`-ban:  
```
...
our %OS_AIX
...
    ldmiscflags => '-Wl,-bmaxdata:0x80000000,-brtl',
...
```

Ezek után már jöhettek a parancsok sorban (`perl Configure.pl --has-libffi...` stb.)

### 4.5 Linkelési hiba

A **Rakudo** fordítása esetében minden lefutott, de a `make install` fázisában minden egyéb dolog megfelelő telepítése után az alábbi hibát kaptuk még a 2021.05 verzió előtt: 
```ksh
...
+++ Creating Raku executable alias
Usage: ln [-f|n] [-s] src [target]
   or: ln [-f|n] [-s] src1 ... srcN directory
make: *** [Makefile:1509: m-runner-default-install] Error 1
```

Emiatt nem volt képes a `/opt/raku/bin/raku` és `/opt/raku/bin/raku-debug` linkeket elkészíteni. 

Ezt kellett külön javítanunk: 

```ksh
# cd /opt/raku/bin

# ln -s rakudo raku

# ln -s rakudo-debug raku-debug
```


Megérte a vesződség, mert sokat javult a Raku közel 2 év alatt (2x olyan gyors lett): 

A régi: 
```ksh
# raku -v
This is Rakudo version 2019.03 built on MoarVM version 2019.03
implementing Perl 6.d.

# time raku -e 'my $s;for (1..10000) {my $i=$_; $s=$i*$i*$i;for (1..10000) {my $j=$_;$s=$j/$j/$j}};say $s'
0.0001

real    11m27.38s
user    5m2.15s
sys     0m0.01s
```

És az új: 
```ksh
# raku -v
Welcome to Rakudo(tm) v2021.02.1.
Implementing the Raku(tm) programming language v6.d.
Built on MoarVM version 2021.02.

# time raku -e 'my $s;for (1..10000) {my $i=$_; $s=$i*$i*$i;for (1..10000) {my $j=$_;$s=$j/$j/$j}};say $s'
0.0001

real    5m47.81s
user    2m33.07s
sys     0m0.01s
```

<a name="5"></a>
## 5. Letöltés

Az elmúlt hónapokban mindig csináltam friss build-et, amiket mind meg is tartottam, ha már máshol amúgysem igazán érhető el AIX-hoz. 

A [Letöltések](/downloads) oldalon a mindenkori aktuális kiadások AIX 7.2-n fordított binárisa innen is letölthető. Csak bontsd ki az `/opt` alá, linkeld, állítsd be a `$LIBPATH`-ot és ha nem panaszkodik egyéb függőségre (további RPM csomagok), már működnie is kell. 
