# ![alt text](/_ui/img/icons/vsdc_m.svg) VSDC Video Editor

###### .

## Tartalomjegyzék

1. [Bevezető](#1)
2. [Szöveganimációk](#2)

<a name="1"></a>
## 1. Bevezető

...

<a name="2"></a>
## 2. Szöveganimációk

Ezekhez a rövid gyakorlatokhoz hozzunk létre egy új, üres projektet, amely egy **Blank project** legyen. Állítsuk be neki a kívánt felbontást: 

![alt_text](vsdc_-_2_-_new_project.png "")

### 2.1 Felnagyítás, majd elhalványulás

Az **Editor** menüben az **Add Object** &rarr; **Text** &rarr; **Text** menüpontban, vagy egyszerűen a szövegbeszúrás ikonnal adjunk a jelenethez egy új Text objektumot. A pozícióját igazítsuk a jelenet elejéhez a **From start of the scene** opcióval: 

![alt_text](vsdc_-_2_1_1_-_add_text.png "")

Először a szövegdoboz hozzávetőleges méretét adhatjuk meg, begépeljük a szöveget, majd az **Editor** menüben maradva, formázzuk meg tetszés szerint. A képernyőhöz  történő vízszintes és függőleges igazítást is itt találjuk: 

![alt_text](vsdc_-_2_1_2_-_align_text.png "")

A felnagyításos animáláshoz válasszuk ki a **Video effects** &rarr; **Transform** &rarr; **Zoom** menüpontot. Az effekthez is válasszuk ki a **From start of the scene** opciót. 

Az effekthez tartozó jobb oldali **Properties** panelen állítsuk be a hatás időtartamát (**Duration**) és a kezdeti értéket (**Initial value**), hogy 0%-tól nagyítódhasson ki 100%-ra: 

![alt_text](vsdc_-_2_1_3_-_zoom_properties.png "")

A piros **Play / pause scene** gombbal már most is kipróbálhatjuk, hogy valóban megfelelően animálódik-e a feliratunk. 

Kattintsunk vissza a jelenet folyamatjelzőjéhez a **Text: Text 1** fül bezárásával, vagy a tőle jobbra lévő **Scene 0** fülre kattintva. Ezzel mindig kiléphetünk az objektumon belüli effektek szerkesztőjéből: 

![alt_text](vsdc_-_2_1_4_-_exit_to_scene.png "")

A Text objektumunk Duration-jét is állítsuk át a kívánt hosszúságúra, jelen esetben 5 mp-re. 

Bizonyosodjunk meg róla, hogy a Text objektum van továbbra is kijelölve, majd kattintsunk újra a **Video effects** menüpontba, de ezúttal válasszuk a Transparency &rarr; **Fade out** menüpontot. A felbukkanó **Object position settings** ablakban most a **To the end of the scene** opció kell legyen bejelölve. Ezt kell elfogadnunk, majd **OK**. 

A lenti pozícionáló sávoknál nyugodtan húzzuk ugyanabba a sorba a **FadeOut 1** objektumot, mint amiben a korábbi **Zoom 1** objektum is van: 

![alt_text](vsdc_-_2_1_5_-_fade_out.png "")

Bizonyosodjunk meg róla, hogy a **FadeOut 1** objektum **Properties** panelében a **Duration** 1 mp. 

Ezzel készen is vagyunk. A Text objektumunk így már magában foglal két további effektet beépített objektumok formájában, amiket tetszőlegesen újra- és újrahasználhatunk, ha pl. egy több rövid jelenetből, előtte szöveges "felkonferálásból" álló videót szeretnénk összeállítani: 

![alt_text](vsdc_-_2_1_6_-_zoom_and_fade_out.gif "")

### 2.2 Push Effect

Ez egy elég látványos szövegbeúsztatós animáció, ami pl. természetes környezeteket bemutató videóknál kifejezetten hatásos. 

Először az **Add object** &rarr; **Rectangle** menüponttal, majd a **From start of the scene** opciót választva hozzunk létre egy téglalapot, amit alakítsunk keskeny, hosszúkás pálcikává: 

![alt_text](vsdc_-_2_2_1_-_add_rectangle.png "")

Az objektum **Properties** panelében állítsuk a **Duration**-jét 7 mp-esre, majd a **Video effects** &rarr; **Transition** &rarr; **Wipe** menüpontban, majd a felugró **Object position settings** ablakban a **From start of the scene** opciót választva adjuk hozzá az ide szükséges effektet. 

A Wipe objektum **Properties** panelében a **Duration** valószínűleg 1 mp lesz; győződjünk meg erről. Továbbá, a **Mode** legyen **Vertical out**. 

![alt_text](vsdc_-_2_2_2_-_wipe1_properties.png "")

Lépjünk vissza a Scene-hez (mint ahogy az előző lépéssornál a Scene 0 fülre kattintottunk), maradjon a téglalap objektum továbbra is kijelölve, és adjunk hozzá mégegy Wipe effektet, de ezúttal a **To the end of the scene** opciót válasszuk az **Object position settings** ablakban. Ennek is **Vertical out** legyen a **Mode**-ja. 

A második Wipe objektumot nyugodtan beállíthatjuk ugyanabba a sorba, ahol az előző is van. 

Adjunk most hozzá egy szöveges objektumot is a jelenethez: **Add Object** &rarr; **Text** &rarr; **Text** menüpont. Állítsuk be a tetszőleges betűtípust, méretet stb.-t, majd igazítsuk a szöveg objektumot a vászon közepéhez vízszintesen és függőlegesen egyaránt. A pálcikánkat csak függőlegesen igazítsuk középre, vízszíntesen a szövegtől egy kicsit balra: 

![alt_text](vsdc_-_2_2_3_-_rectangle_and_text_alignment.png "")

A szövegobjektum Duration-jét állítsuk jóval kisebbre, mint a pálcikáét; jelen példában 4 mp-re, valamint a pálcika kiterjedésének közepéhez igazítsuk (**Time (ms)**: 00:00:01:500): 

![alt_text](vsdc_-_2_2_4_-_text_timing.png "")

A szövegjobjektum kijelölésének megtartása után a **Video effects** &rarr; **Transition** &rarr; **Push** menüpontot választva, majd a **From start of the scene** opcióval hozzunk létre egy Push effektet megvalósító objektumot, amelynek **Duration**-je maradhat 1 mp, **Type**-ja **Slide** és **Mode**-ja **Right to left**. 

Ha ez kész, lépjünk vissza a Text objektumunkhoz és csináljuk ezt meg mégegyszer, de most **To the end of the scene** opcióval. Igazítsuk őket egy vonalba: 

![alt_text](vsdc_-_2_2_5_-_push_timing.png "")

Próbáljuk ki a piros **Play / pause scene** gombbal, hogy hogyan néz ki animációnk, igazítsuk, időzítsük egyedi kívánalmainknak megfelelően, majd használjuk újra annyiszor a videóban, ahányszor csak szeretnénk: 

![alt_text](vsdc_-_2_2_6_-_push.gif "")

...