# ![alt text](/_ui/img/flags/fin_m.svg) Finnish Language / Suomen Kieli

<div style="text-align: right;"><h7>Posted: 2019-10-25 17:41</h7></div>

###### .

![alt_text](finnish_-_bg.jpg)


## Table of Contents

1. [Introduction – Johdanto](#1)
2. [Alphabet, Pronounciation and the Finnish Keyboard Layout](#2)
3. [Everyday Expressions, Idioms and Phrases](#3)
4. [Vowel Harmony – Vokaaliharmonia](#4)
5. [K-P-T Consonant Gradation – K-P-T-Vaihtelu](#5)
6. [Verb Types and Present Forms – Verbityypit ja Preesensmuodot](#6)
7. [Understanding Object Cases in Finnish](#7)
8. [Directions and Locations – Reittiohjeet ja Paikat](#8)
9. [Past Tenses – Menneet Aikamuodot](#9)
10. [Conditional Mood – Konditionaali](#10)
11. [The Imperative Mood – Imperatiivi](#11)
12. [The Impersonal Form – Passiivi](#12)


<a name="1"></a>
## 1. Introduction – Johdanto

**Finnish** (**suomi** or **suomen kieli**) is the most widely spoken language of the **Balto-Finnic branch** in the **Finno-Ugric language family**. It is spoken as a native language by **approximately 4.8 million people**, with another **half a million** using it as a second language.

Finnish is **very closely related to Estonian**, to the extent that **both languages are somewhat mutually intelligible**. However, this mutual understanding is **limited to basic sentences and common expressions**, and in practice, **Estonians tend to understand Finnish more easily than Finns understand Estonian**.

### 1.1 Origin

The **Finno-Ugric languages** evolved from **Proto-Uralic**, an ancient language spoken **around 6000 years ago**. Some linguistic studies suggest that **Proto-Uralic may have first emerged around 10,000 years ago**, before later splitting into different branches due to **migration and cultural changes**.

Over time, **Proto-Uralic divided into two major branches**:

- **Proto-Finno-Permic**, which later led to **Finnish, Estonian, Sami, and Permic languages**.
- **Proto-Ugric**, which led to **Hungarian, Mansi, and Khanty**.

The **Proto-language reconstructions** seen in the illustration are based on **centuries of linguistic research**, where scholars have used **shared vocabulary, phonetic shifts, and historical texts** to reconstruct the ancestral languages.

![alt_text](uralic_lang_en.png "Relationships between extinct Uralic languages and the currently living Finno-Ugric ones")

Sources for the above illustration: [Proto-Uralic #1](https://www.britannica.com/topic/Uralic-languages), [Proto-Uralic #2](https://en.wikipedia.org/wiki/Proto-Uralic_language), [Finno-Permic](https://en.wikipedia.org/wiki/Finno-Permic_languages), [Finno-Ugric, Samoyedic](https://en.wikipedia.org/wiki/Finno-Ugric_languages), [Proto-Ugric](https://en.wikipedia.org/wiki/Ugric_languages), [Proto-Samoyedic](https://en.wikipedia.org/wiki/Proto-Samoyedic_language).

### 1.2 Similarities with Hungarian

Although **Finnish and Hungarian are not mutually intelligible**, they share a common origin as members of the **Finno-Ugric language family**. Due to this shared ancestry, Finnish and Hungarian have **several grammatical similarities** and approximately **800 words with common roots**.  

Some of these **cognates** are easy to recognize and accept as related words, such as:

- **käsi** (*kéz* - *hand*),  
- **vesi** (*víz* - *water*),  
- **kivi** (*kő* - *stone*),  
- **sydän** (*szív* - *heart*),  
- **kala** (*hal* - *fish*),  
- **veri** (*vér* - *blood*),  
- **uida** (*úszni* - *to swim*),  
- **mennä** (*menni* - *to go*),  
- **tehdä** (*tenni* - *to do*).  

<div class="info">
<p>For a list of Finnish-Hungarian words with common origins, see the subpage <b><a href="dictionary">Dictionary of Finnish-Hungarian Common Origin Words</a></b>.</p>
</div>

In some cases, the **shared origin** of words is not immediately obvious to the average speaker and has been identified through linguistic research. However, even these words are often easier for Hungarians to learn due to their **historical connections**.  
For example, the Finnish **missä?** (*where?*) is related to the Hungarian **hol?**, derived from **messze** (*far away*).  

#### Key Structural Similarities

In addition to the **large number of common-rooted words**, Finnish and Hungarian share several **structural characteristics**:

1. **No grammatical gender**  
  Neither Finnish nor Hungarian distinguishes between **masculine, feminine, or neuter nouns**.
2. **Fixed word stress**  
  In both languages, the **primary stress always falls on the first syllable** of a word.
3. **Strongly agglutinative structure**  
  Both are **agglutinative languages**, meaning that words are **formed by adding suffixes (affixes) instead of using prepositions**.  
  In Hungarian, this process is called **"ragozó"** or **"toldalékoló"**.
4. **Vowel harmony**  
  In both languages, **suffixes change** based on the vowels of the root word (**front vowels vs. back vowels**).
5. **Similar logic in time expressions and verb conjugation**  
  Both languages follow **parallel logic** when describing **time, verb conjugations, and noun cases**.
6. **Similar word formation patterns**  
  **Compound words** in both languages oten follow **the same logic**, often combining **descriptive words** in a clear way:
    - **lentokone** (*repülőgép* - airplane) → *"flying machine"*.
    - **sateenvarjo** (*esernyő* - umbrella) → *"rain shield"*.
    - **ilotyttö** (*örömlány* - prostitute) → *"joy girl"*.

This list is **far from exhaustive**, but it highlights some of the most **important similarities** between Finnish and Hungarian.  

While **both languages are considered complex and difficult to learn**, it is clear that **a Hungarian speaker will generally find Finnish easier to learn than an English speaker would** due to their **shared grammatical structures and vocabulary connections**.  


<a name="2"></a>
## 2. Alphabet, Pronounciation and the Finnish Keyboard Layout

Finnish uses the **Latin alphabet** with three additional letters (**Ä, Ö, Å**) and follows a **highly phonetic spelling system**. Unlike English, **there are no silent letters**, and pronunciation follows **consistent rules**.  

This chapter introduces the **Finnish alphabet**, explains **pronunciation challenges**, explores the **keyboard layout**, and highlights **unique features of Finnish spelling and phonology**.

### 2.1 The Finnish Alphabet

Finnish uses the **basic Latin alphabet** with **29 letters**:  

<table class="tan">
  <tr>
    <td><b>A a</b></td><td><b>B b</b></td><td><b>C c</b></td><td><b>D d</b></td><td><b>E e</b></td><td><b>F f</b></td><td><b>G g</b></td><td><b>H h</b></td>
  </tr>
  <tr>
    <td><b>I i</b></td><td><b>J j</b></td><td><b>K k</b></td><td><b>L l</b></td><td><b>M m</b></td><td><b>N n</b></td><td><b>O o</b></td><td><b>P p</b></td>
  </tr>
  <tr>
    <td><b>Q q</b></td><td><b>R r</b></td><td><b>S s</b></td><td><b>T t</b></td><td><b>U u</b></td><td><b>V v</b></td><td><b>W w</b></td><td><b>X x</b></td>
  </tr>
  <tr>
    <td><b>Y y</b></td><td><b>Z z</b></td><td><b>Å å</b></td><td><b>Ä ä</b></td><td><b>Ö ö</b></td>
  </tr>
</table>  

**Notes on Rare Letters**:

- The letters **Q, W, X, Z, Å** appear mostly in **foreign loanwords or names**.  
- The letter **Å** is used in **Swedish-origin words** and is pronounced **the same as "O"** in Finnish.  

### 2.1.1 Pronunciation

Finnish pronunciation is **almost entirely phonetic**, meaning words are pronounced **as they are spelled**.

Below is a short list of **challenging Finnish sounds** along with their **IPA symbols**:

- **y** → [y]
    - A **rounded front vowel**, similar to the **French "u"** or **German "ü"**.  
- **s** → [s]
    - Unlike in Hungarian, where **"s"** is pronounced as **"sh"**, Finnish **"s"** is generally a clear **[s]** sound, though in some words and speech patterns, it may be **slightly softer**, somewhat between "s" and "sh".
    - The Hungarian **"sh"** sound exists in Finnish, and while it is usually written as **"sh"** (e.g., *shamaani* - shaman), the **letter "š"** is also occasionally used, particularly in **linguistic transcriptions, Slavic loanwords, or historical contexts**.
- **e** → [e]
    - A **slightly more closed sound** than the Hungarian **"e"**, closer to the **Hungarian dialectal "ë"**.  
- **ä** → [æ]
    - Similar to the **"a" in English words like "cat", "hat", and "fat"**.  
- **å** → [o]
    - Pronounced as a **regular "o"**, appearing only in **Swedish loanwords**.  

### 2.2 The Finnish Keyboard Layout

Finnish keyboards follow the **standard QWERTY layout** but include additional keys for **Ä, Ö, and Å**.  

![Finnish Keyboard Layout](keyboard_fi.png)  

### 2.3 Interesting Features of Finnish Spelling and Phonology

Finnish has some **unique spelling and pronunciation characteristics** that differ from most European languages:  

- **Native Finnish Words Never Contain "B", "C" or "G"**: the letters **B** and **G** exist in Finnish but **only in loanwords**, e.g. *banaani* (banana), *generaattori* (generator).  
- Native Finnish words **always replace "B" with "P"** and "G" with "K".
- Loanwords containing "C" are either **kept as they are** or changed:  
    - **celsius** → **Celsius** (*unchanged*)  
    - **cirkus** → **sirkus** (circus)  
- **No Hard Double Consonants at the Beginning of Words**: unlike many European languages, **Finnish avoids hard double consonants (like ST, SK, SP) at the beginning of words**. This is why foreign names and country names often **change in Finnish**, e.g.:
    - **Stockholm** → **Tukholma**  
    - **France** → **Ranska**  


<a name="3"></a>
## 3. Everyday Expressions, Idioms and Phrases

This list provides an **overview of essential Finnish expressions** for daily life. While it's far from complete, it introduces the **most commonly used greetings, polite phrases, and everyday interactions**.  

<img src="/_ui/img/imgfx/conversation.png" title="" align="right" class="right">

### 3.1 General Greetings

- **Moi!** / **Hei!** – Hi!  
- **Moikka!** – Hi! / Bye!  
- **Heippa!** – Bye! (Casual)  
- **Terve!** – Hello!  
- **Moro!** – Hello! *(originated from the Tampere region)*  
- **Hei hei!** / **Moi moi!** – Bye! *(Short goodbye)*  
- **Tervetuloa!** – Welcome!  

### 3.2 More Formal Greetings

- **Hyvää huomenta!** / **Huomenta!** – Good morning!  
- **Hyvää päivää!** – Good day!  
- **Hyvää iltaa!** – Good evening!  
- **Hyvää yötä!** – Good night!  
- **Nähdään!** – See you!  
- **Nähdään pian! Hei!** – See you soon! Bye!  
- **Nähdään myöhemmin!** – See you later!  
- **Hyvää päivänjatkoa!** / **Mukavaa päivänjatkoa!** – Have a nice day!  

### 3.3 Asking & Thanking for Things
- **Kiitos!** – Thank you!  
- **Kiitti!** – Thanks! *(Casual)*  
- **…, kiitos.** – …, please.  
- **Ole hyvä!** – Here you are! / You're welcome!  
- **Ei kestä!** – Don't mention it! / Not at all!  

### 3.4 Other Greetings, Expressions, & Wishes

- **Tsemppiä!** – Good luck!  
- **Hyvää joulua!** – Merry Christmas!  
- **Hyvää uutta vuotta!** – Happy New Year!  
- **Kiitos samoin!** – Thanks, same to you!  
- **Ystävällisin terveisin** – Kind regards *(formal closing in letters; literally "friendliest regards")*  

### 3.5 Asking How Someone Is Doing

- **Mitä kuuluu?** – How are you?  
- **Ihan hyvää, kai.** – Quite well, I guess.  
- **Ihan hyvää, kiitos. Entä sinulle?** – Quite well, thank you. And you?  
- **No, ihan hyvää, kiitos.** – Well, quite well, thank you.  
- **No, ei mitään.** – Well, nothing special.  
- **Oletko kunnossa?** – Are you all right?  
- **Kaikki on ihan hyvin.** – Everything is all right.  

### 3.6 Apologizing & Expressing Concern

- **No, ei se mitään.** – Well, it happens.  
- **Olen pahoillani.** – I'm sorry. / My apologies.  
- **Olen todella pahoillani.** / **Olen tosi pahoillani.** – My deepest apologies.  
- **Olemme pahoillamme.** – Our apologies.  
- **Pahoittelen.** – I apologize.  

### 3.7 Basic Introduction & Identifying People

- **Minä olen Elsa.** – I am Elsa.  
- **Minun nimeni on Elsa.** – My name is Elsa.  
- **Sinä olet Joni.** – You are Joni.  
- **Anteeksi, kuka sinä olet?** – Excuse me, who are you?  
- **Anteeksi, oletko sinä Pöllönen?** – Excuse me, are you Pöllönen?  

### 3.8 Agreeing & Disagreeing

- **Kyllä.** – Yes.  
- **Joo.** – Yeah. *(Casual)*  
- **Ei.** – No.  
- **Totta kai!** – Of course!  
- **Tietenkin!** – Naturally!  
- **Totta!** – True!  
- **Olen samaa mieltä.** – I agree.  
- **En ole samaa mieltä.** – I disagree.  
- **Se riippuu.** – It depends.  
- **Ehkä.** – Maybe.  
- **En tiedä.** – I don’t know.  

### 3.9 Asking for Help & Common Questions

- **Anteeksi!** – Excuse me! / Sorry!  
- **Voitko auttaa minua?** – Can you help me?  
- **Mitä tämä tarkoittaa?** – What does this mean?  
- **Miten sanotaan tämä suomeksi?** – How do you say this in Finnish?  
- **Voitko puhua hitaammin?** – Can you speak more slowly?  
- **Voitko toistaa?** – Can you repeat?  
- **En ymmärrä.** – I don’t understand.  

### 3.10 Everyday Social Interactions

- **Ei se mitään.** – No worries.  
- **Ei haittaa.** – It’s okay.  
- **Ei voi mitään.** – Nothing can be done.  
- **Anna olla.** – Let it be.  
- **Ole varovainen!** – Be careful!  
- **Hauska tutustua!** – Nice to meet you!  
- **Samoin!** – Likewise!  


<a name="4"></a>
## 4. Vowel Harmony – Vokaaliharmonia

Finnish **vowel harmony** is a fundamental rule that determines how vowels are used within a word and how suffixes are attached to them. It ensures that certain vowels do not mix within a single word, making the language sound natural and fluent.

This rule is not only relevant for conjugation but also affects how **native Finnish words** are formed. In native Finnish words, **front and back vowels never appear together in the same morpheme**. However, many **loanwords ignore vowel harmony**, such as *paperi*, *olio*, and *radio*. While vowel harmony is sometimes disregarded in loanwords, it is **always followed when conjugating Finnish words**.

To apply vowel harmony correctly, you must conjugate words based on the **last part of the word** and the vowels it contains.

### 4.1 The General Rule of Vowel Harmony

Finnish vowels are categorized into three groups:

- **Front vowels**: **ä, ö, y**
- **Back vowels**: **a, o, u**
- **Neutral vowels**: **e, i**

The **basic principle** is that in **single, non-compound words**, if a word contains **front vowels**, it cannot contain **back vowels**, and vice versa. **Neutral vowels (e, i) can appear in both types of words.**

There is a simple rule in Finnish to determine which vowel to use when conjugating words:

- Words containing **only front vowels** get **front vowels** in their endings:
    - **hyvä** → **hyvässä** (*in the good one*)
    - **säästää** → **säästävät** (*they save*)
- Words containing **only neutral vowels** follow the **front vowel** rule:
    - **kieli** → **kielikö** (*language?*)
- Words containing **a mix of front and neutral vowels** also get **front vowels** in endings:
    - **päivä** → **päivässä** (*in the day*)
- Words containing **only back vowels** get **back vowels** in their endings:
    - **auto** → **autossa** (*in the car*)
    - **tutustua** → **tutustuvat** (*they get to know*)
- Words containing **back vowels, even if they also have neutral vowels**, take **back-vowel** endings:
    - **mestari** → **mestarilla** (*on the champion*)
    - **kiva** → **kivassa** (*in the nice one*)

### 4.2 Compound Words and Vowel Harmony

In **compound words** (**yhdyssanat**), vowel harmony is determined by **the last part of the word**, regardless of what vowels appear in the first part:

- **suklaajäätelö** → **suklaajäätelössä** (*in the chocolate ice cream*)
- **jäätelökioski** → **jäätelökioskilla** (*at the ice cream kiosk*)

Additionally, **some native Finnish words** ending in *"-i"* behave slightly irregularly, even though they contain front vowels:

- **pieni** (*small*) → **pienessä** (*in the small one*) → not *pienissä*
- **meri** (*sea*) → **meressä** (*in the sea*) → not *merissä*

A similar principle applies to **compound words**, where the last part of the word dictates the suffix harmony:

- **vapaapäivä** (day off) → **vapaapäivänä** (on the day off)


<a name="5"></a>
## 5. K-P-T Consonant Gradation – K-P-T-Vaihtelu

One of the most important phonetic features in Finnish is **K-P-T consonant gradation** (**K-P-T-vaihtelu**). This means that certain consonants (not only *k*, *p*, *t*) change into a **weaker** form when the word is conjugated. This variation is essential in **case endings, verb conjugations, and possessive forms**, making it a fundamental part of Finnish grammar.

In most cases, the **weakened (soft) form** appears when a word is declined or conjugated **within a closed syllable** (i.e., the syllable is no longer open-ended). Understanding these changes helps in forming correct Finnish words in different cases.

### 5.1 Hard vs. Soft Forms of K-P-T

Below is a list of common K-P-T consonant gradation patterns with examples:

<table class="tan">
  <tr>
    <td colspan="2"><span style="font-size: 20px">vahva</span> (hard)</td>
    <td colspan="2"><span style="font-size: 20px">heikko</span> (soft)</td>
  </tr>
  <tr>
    <td>Afri<r>kk</r>a, Tur<r>kk</r>i, vii<r>kk</r>o</th>
    <th>kk</th>
    <th>k</th>
    <td>Afri<r>k</r>asta, Tur<r>k</r>issa, vii<r>k</r>olla</th>
  </tr>
  <tr>
    <td>Euroo<r>pp</r>a, La<r>pp</r>i, kaa<r>pp</r>i</th>
    <th>pp</th>
    <th>p</th>
    <td>Afrikasta, la<r>p</r>issa, kaa<r>p</r>in</th>
  </tr>
  <tr>
    <td>konser<r>tt</r>i, ty<r>tt</r>ö, planee<r>tt</r>a</th>
    <th>tt</th>
    <th>t</th>
    <td>konser<r>t</r>in, ty<r>t</r>ön, planee<r>t</r>alla</th>
  </tr>
  <tr>
    <td>Tur<r>k</r>u, ruo<r>k</r>a, sel<r>k</r>ä</th>
    <th>k</th>
    <th>-</th>
    <td>Turussa, ruoasta, selässä</th>
  </tr>
  <tr>
    <td>lei<r>p</r>ä, kyl<r>p</r>y, a<r>p</r>u</th>
    <th>p</th>
    <th>v</th>
    <td>lei<r>v</r>ällä, kyl<r>v</r>yssä, a<r>v</r>ulla</th>
  </tr>
  <tr>
    <td>ka<r>t</r>u, mai<r>t</r>o, pöy<r>t</r>ä</th>
    <th>t</th>
    <th>d</th>
    <td>ka<r>d</r>ulla, mai<r>d</r>ossa, pöy<r>d</r>ältä</th>
  </tr>
  <tr>
    <td>Helsi<r>nk</r>i, kaupu<r>nk</r>i, auri<r>nk</r>o</th>
    <th>nk</th>
    <th>ng</th>
    <td>Helsi<r>ng</r>issä, kaupu<r>ng</r>issa, auri<r>ng</r>on</th>
  </tr>
  <tr>
    <td>Holla<r>nt</r>i, Isla<r>nt</r>i, ra<r>nt</r>a</th>
    <th>nt</th>
    <th>nn</th>
    <td>Holla<r>nn</r>in, Isla<r>nn</r>ista, ra<r>nn</r>alle</th>
  </tr>
  <tr>
    <td>ru<r>mp</r>u, ka<r>mp</r>a, la<r>mp</r>pu*</th>
    <th>mp</th>
    <th>mm</th>
    <td>ru<r>mm</r>un, ka<r>mm</r>assa, la<r>m</r>pussa*</th>
  </tr>
  <tr>
    <td>i<r>lt</r>a, Itäva<r>lt</r>a, ku<r>lt</r>a</th>
    <th>lt</th>
    <th>ll</th>
    <td>illa<r>ll</r>a, Itäva<r>ll</r>assa, ku<r>ll</r>an</th>
  </tr>
  <tr>
    <td>pa<r>rt</r>a, yläke<r>rt</r>a, vi<r>rt</r>a</th>
    <th>rt</th>
    <th>rr</th>
    <td>pa<r>rr</r>an, yläke<r>rr</r>asa, vi<r>rr</r>an</th>
  </tr>
</table>

### 5.2 Cases Where K-P-T Variation Does Not Occur

There are specific situations where K-P-T variation does not apply:

- **Certain consonant clusters (hk, sk, st, tk)**
    - **posti** → **postissa** (*in the post office*)
    - **matka** → **matkalla** (*on the trip*)
    - **keittiö** → **keittiössä** (*in the kitchen*)
- **Words ending in two vowels (like vapaa, tuo, maa)**
    - **vapaa** → **vapaassa** (*in the free one*)
    - **maa** → **maassa** (*in the land*)
    - **suo** → **suossa** (*in the swamp*)
- **Loanwords and some proper nouns**
    - **kapteeni** → **kapteenilla** (not *kavteenilla*)
    - **taksi** → **taksissa** (not *tassissa*)


<a name="6"></a>
## 6. Verb Types and Present Forms – Verbityypit ja Preesensmuodot

![alt_text](finnish_-_verbtypes.jpg)

Finnish uses a **systematic** but **mostly straightforward** verb conjugation system. Understanding the **logic and steps for conjugating verbs** is crucial when learning the language. A key factor in this process is **K-P-T consonant gradation**, but it does **not apply to all verbs**.

### 6.1 Verb Types and Their Simple Present Tense

To conjugate **any verb in the simple present tense** (**preesens**), you need to follow **five basic steps**:  

![Finnish Verb Conjugation Steps](finnish_-_verb_conjugation_steps.svg)  

<div class="info">
<p>For a list of the most common verbs, see the subpage <b><a href="verbs">Finnish verbs, types, and conjugations</a></b>.</p>
</div>

#### 6.1.1 Type 1 Verbs

Type 1 verbs are the **most common** verb type in Finnish. They always end in **-a** or **-ä** in their **infinitive form** (**perusmuoto**):

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_1.svg" title="" align="left" /></div>
</div>

Let's take **tanssia** (*to dance*) as our very first example.  
**Key points to remember here**:

- **tanssia** is the **dictionary form** of the verb (**infinitiivi / perusmuoto**).
- To form the **verb stem** (**vartalo**), the final **-a** or **-ä** **is dropped**.
- The correct **personal ending** is then added to the stem, so it's:

<table class="tan">
  <tr>
    <th>Pronoun</th>
    <th>Simple Present Form</th>
  </tr>
  <tr>
    <td>minä</td>
    <td><b>tanssin</b> (<i>I dance</i>)</td>
  </tr>
  <tr>
    <td>sinä</td>
    <td><b>tanssit</b> (<i>you dance</i>)</td>
  </tr>
  <tr>
    <td>hän/se</td>
    <td><b>tanssii</b> (<i>she/he/it dances</i>)</td>
  </tr>
  <tr>
    <td>me</td>
    <td><b>tanssimme</b> (<i>we dance</i>)</td>
  </tr>
  <tr>
    <td>te</td>
    <td><b>tanssitte</b> (<i>you all dance</i>)</td>
  </tr>
  <tr>
    <td>he/ne</td>
    <td><b>tanssivat</b> (<i>they dance</i>)</td>
  </tr>
</table>

Some Type 1 verbs also undergo **K-P-T consonant gradation** before forming the stem. Let's look at **nukkua** (*to sleep*), our second example for this:

- **Infinitive form:** **nukkua**
- **Before forming the stem**, **kk → k** due to consonant gradation, so it leads to the stem **nuku-**
- **Personal endings** are then added as usual.

Type 1 verbs follow **a predictable pattern**, and the **biggest challenge** is recognizing **when consonant gradation applies**.

#### 6.1.2 Type 2 Verbs

**Type 2 verbs always end in -da or -dä in their infinitive form.** They are conjugated by removing the final **-da/-dä** and adding the correct personal ending:

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_2.svg" title="" align="left" /></div>
</div>  

**Key points to remember**:

- The **infinitive** form always ends in **-da/-dä** (**syödä, juoda, nähdä**).
- To form the **verb stem**, simply **remove -da or -dä**.
- **K-P-T consonant gradation does not apply** to Type 2 verbs.

Since **no consonant gradation** applies, Type 2 verbs follow **a consistent and straightforward pattern**.

#### 6.1.3 Type 3 Verbs

**Type 3 verbs end in -lla, -llä, -sta, -stä, -nna, -nnä, -ra, -rä.** Their conjugation requires **removing the final -la/-lä/-sta/-stä, etc., and adding an "e" before the personal ending**.

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_3.svg" title="" align="left" /></div>
</div>

**Key points to remember**:

- The **infinitive** form always ends in **-lla/-llä, -sta/-stä, -nna/-nnä, -ra/-rä**.
- To form the **verb stem**, remove the **last two letters** and add **-e-** before the **personal ending**.
- **K-P-T consonant gradation applies**, where possible.

Since **K-P-T consonant gradation applies**, **double consonants (pp, tt, kk, etc.) often become single**.  

#### 6.1.4 Type 4 Verbs

**Type 4 verbs end in -ata, -ätä, -ota, -ötä, -uta, -ytä.** Their conjugation requires **removing the final -ta or -tä and adding an "a" or "ä" before the personal ending**.

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_4.svg" title="" align="left" /></div>
</div>

**Key points to remember**:

- The **infinitive** form always ends in **-ata, -ätä, -ota, -ötä, -uta, -ytä**.  
- To form the **verb stem**, remove **-ta/-tä** and **add -a/-ä** before the personal ending.  
- **K-P-T consonant gradation applies**, where possible.  

Since **K-P-T consonant gradation applies**, the **strong-to-weak** shift (e.g., **t → d**) might occur.  

#### 6.1.5 Type 5 Verbs

**Type 5 verbs always end in -ita or -itä.** Their conjugation follows a pattern where **-itse- is inserted** before the personal ending.

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_5.svg" title="" align="left" /></div>
</div>

**Key points to remember**:

- The **infinitive** form always ends in **-ita/-itä**.  
- To form the **verb stem**, remove **-ta/-tä** and insert **-itse-** before the personal ending.  
- **K-P-T consonant gradation does not apply**.  

Since **K-P-T consonant gradation does not apply**, there is **no sound weakening** in the stem.  

#### 6.1.6 Type 6 Verbs

**Type 6 verbs always end in -eta or -etä.** Their conjugation requires **replacing -eta/-etä with -ene- before the personal ending**.

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_6.svg" title="" align="left" /></div>
</div>

**Key points to remember**:

- The **infinitive** form always ends in **-eta/-etä**.  
- To form the **verb stem**, replace **-eta/-etä** with **-ene-** before the personal ending.  
- **K-P-T consonant gradation applies**, where possible.  

Since **K-P-T consonant gradation applies**, **strong-to-weak** shifts (e.g., **t → d**) may occur.  

<div class="info">
<p>Each <b>verb type follows a distinct conjugation pattern</b>, and recognizing <b>stem changes & consonant gradation</b> is key to mastering Finnish verbs.</p>
</div>

### 6.2 Forming Present Simple Negatives and Questions

Both **negative statements** and **questions** in the present tense are **straightforward** once you understand **the verb type and how it conjugates**.  

#### 6.2.1 Forming Negatives (Preesens, Negatiivinen)

To form the **negative present tense**, follow these **two simple steps**:

1. **Use the correct negation verb** (**en, et, ei, emme, ette, eivät**).
2. **Attach the verb stem** (without the personal ending).

**Example: kävellä** (*to walk*)

<table class="tan">
  <tr>
    <th>Pronoun</th>
    <th>Negative Form</th>
  </tr>
  <tr>
    <td>minä</td>
    <td><b>en</b> kävele</td>
  </tr>
  <tr>
    <td>sinä</td>
    <td><b>et</b> kävele</td>
  </tr>
  <tr>
    <td>hän/se</td>
    <td><b>ei</b> kävele</td>
  </tr>
  <tr>
    <td>me</td>
    <td><b>emme</b> kävele</td>
  </tr>
  <tr>
    <td>te</td>
    <td><b>ette</b> kävele</td>
  </tr>
  <tr>
    <td>he/ne</td>
    <td><b>eivät</b> kävele</td>
  </tr>
</table>

**Key notes**:
- The verb always **stays in the stem form** (i.e., **without personal endings**).  
- There are **no changes based on verb type**—this rule applies to **all verbs**.

#### 6.2.2 Forming Questions (Kysymyslauseet)

To form a **yes/no question**, simply **add the suffix -ko or -kö** to the verb in its **conjugated present tense form**. The choice between **-ko** and **-kö** follows **vowel harmony**:

- Use **-ko** if the verb contains **back vowels (a, o, u)**.
- Use **-kö** if the verb contains **front vowels (ä, ö, y)**.

**Example: kävellä** (*to walk*)

<table class="tan">
  <tr>
    <th>Pronoun</th>
    <th>Question Form</th>
  </tr>
  <tr>
    <td>minä</td>
    <td>kävelenkö?</td>
  </tr>
  <tr>
    <td>sinä</td>
    <td>käveletkö?</td>
  </tr>
  <tr>
    <td>hän/se</td>
    <td>käveleekö?</td>
  </tr>
  <tr>
    <td>me</td>
    <td>kävelemmekö?</td>
  </tr>
  <tr>
    <td>te</td>
    <td>kävelettekö?</td>
  </tr>
  <tr>
    <td>he/ne</td>
    <td>kävelevätkö?</td>
  </tr>
</table>

**Key notes**:

- Unlike **negatives**, **questions keep the full conjugated verb form**.  
- **Vowel harmony applies**: **-ko for back vowels**, **-kö for front vowels**.  


<a name="7"></a>
## 7. Understanding Object Cases in Finnish

Now that we have covered verb conjugation in Finnish, the next crucial step is forming complete sentences with **objects**. The way objects are expressed in Finnish depends on whether the action is **partial, indefinite, complete, or definite**. 

Unlike many languages, Finnish does not have a simple **Accusative case** for objects. Instead, it **distributes the functions** of an object among three cases:

<div class="info">
<ol>
  <li><strong>Partitive (Partitiivi)</strong> → for incomplete actions, indefinite objects, or partial amounts</li>
  <li><strong>Accusative (Akkusatiivi)</strong> → for completed actions or definite objects</li>
  <li><strong>Genitive (Genetiivi)</strong> → occasionally used for total objects in certain constructions</li>
</ol>
</div>

This distinction is essential in **sentence meaning**, as it affects how actions and objects are perceived. Consider:  
- **Partitiivi:** *Juon kahvia.* → "I am drinking coffee." (*some coffee, not all of it*)  
- **Akkusatiivi:** *Join kahvin.* → "I drank the coffee." (*the whole coffee, completely*)  

### 7.1 Partitiivi - The Incomplete or Indefinite Object

<img src="/_ui/img/imgfx/teaching.png" title="" align="right" class="right">

Partitiivi is used in multiple situations where an action does not fully affect the object or the object is **not fully defined**.  

**Use partitiivi when...**

- The action is **ongoing or incomplete**
    - *Luen kirjaa.* → "I am reading a book." (*not necessarily finishing it now*)
- The object is **indeterminate or uncountable**
    - *Haluan vettä.* → "I want some water." (*unspecified amount*)
- In **negative sentences**
    - *En näe koiraa.* → "I don't see a dog."
- In **expressing emotions and preferences** 
    - *Rakastan musiikkia.* → "I love music."
- With verbs that **typically take partitiivi**
    - *Minulla on rahaa.* → "I have money."

**Singular & Plural Examples**:

<table class="tan">
  <tr>
    <th>English</th>
    <th>Singular</th>
    <th>Plural</th>
  </tr>
  <tr>
    <td>I eat an apple.</td>
    <td>Syön omenaa.</td>
    <td>Syön omenoita.</td>
  </tr>
  <tr>
    <td>I drink coffee.</td>
    <td>Juon kahvia.</td>
    <td>Juon kahveja.</td>
  </tr>
  <tr>
    <td>I love books.</td>
    <td>Rakastan kirjaa.</td>
    <td>Rakastan kirjoja.</td>
  </tr>
</table>

### 7.2 Accusative Case (Akkusatiivi) – The Complete Object

Akkusatiivi is used when an action is **completed or intends to affect the entire object**.  

**Use akkusatiivi when...**

- The action is **definite and completed**
    - *Ostin kirjan.* → "I bought the book."
- The object is **specific and whole**
    - *Näen koiran.* → "I see the dog."
- When expressing **future intention with certainty**
    - *Aion lukea kirjan.* → "I am going to read the book (fully)."

**Singular & Plural Examples**:

<table class="tan">
  <tr>
    <th>English</th>
    <th>Singular</th>
    <th>Plural</th>
  </tr>
  <tr>
    <td>I read the book.</td>
    <td>Luin kirjan.</td>
    <td>Luin kirjat.</td>
  </tr>
  <tr>
    <td>I ate the apple.</td>
    <td>Söin omenan.</td>
    <td>Söin omenat.</td>
  </tr>
  <tr>
    <td>I met my friends.</td>
    <td>Tapasin ystävän.</td>
    <td>Tapasin ystävät.</td>
  </tr>
</table>

### 7.3 When to Use Genitive as an Object Case?

Sometimes, Finnish **uses Genitive (-n ending) for total objects**, especially in imperative or future contexts.  

- *Vie koira ulos!* → "Take the dog outside!"  
- *Aion siivota huoneen.* → "I will clean the room."  

<div class="info">
<p><strong>Genitive vs. Accusative?</strong></p>
<p>In modern Finnish, <strong>Genitive is often interchangeable with Accusative</strong>, but certain verb constructions require it.</p>
</div>

### 7.4 Comparing the Cases – Why It Matters?

Many verbs can take **either Partitiivi or Akkusatiivi**, changing the meaning:  

<table class="tan">
  <tr>
    <th>English Meaning</th>
    <th>Partitiivi (Ongoing, Partial)</th>
    <th>Akkusatiivi (Complete, Definite)</th>
  </tr>
  <tr>
    <td>I read the book.</td>
    <td>Luen kirjaa. (<i>I am reading it, not necessarily finishing it now.</i>)</td>
    <td>Luen kirjan. (<i>I will finish reading it.</i>)</td>
  </tr>
  <tr>
    <td>I eat an apple.</td>
    <td>Syön omenaa. (<i>I am eating some of it.</i>)</td>
    <td>Syön omenan. (<i>I eat the whole apple.</i>)</td>
  </tr>
  <tr>
    <td>I buy coffee.</td>
    <td>Ostan kahvia. (<i>I buy some coffee.</i>)</td>
    <td>Ostan kahvin. (<i>I buy the coffee, specific amount.</i>)</td>
  </tr>
</table>

### 7.5 Future Meaning with Partitiivi

Finnish doesn't have a **future tense**, but **Partitiivi often suggests a future intention**:

- *Ostamme autoa.* → "We will buy a car (sometime in the future)."  
- *Rakennan taloa.* → "I will build a house (planning phase)."  
- *Opiskelen suomen kieltä.* → "I will study Finnish (not yet completed)."  

This subtle distinction is **essential for fluency** and understanding **native speech patterns**.

### 7.6 Verbs That Always Take Partitiivi

Some verbs in Finnish **always** require partitive objects. **Common examples:**  

- *Rakastaa* (to love) → *Rakastan sinua.* (*I love you.*)  
- *Etsiä* (to search for) → *Etsin kirjaa.* (*I’m looking for a book.*)  
- *Kaivata* (to miss) → *Kaipaan kotia.* (*I miss home.*)  
- *Omistaa* (to own) → *Omistan koiraa.* (*I own a dog.*)  

### 7.7 Negative Sentences Always Use Partitiivi

Finnish follows a strict rule:  

<div class="warn">
<p>Negation always turns the object into Partitiivi.</p>
</div>

<table class="tan">
  <tr>
    <th>English</th>
    <th>Affirmative (Complete Object)</th>
    <th>Negative (Partitiivi)</th>
  </tr>
  <tr>
    <td>I see the dog.</td>
    <td>Näen koiran.</td>
    <td>En näe koiraa.</td>
  </tr>
  <tr>
    <td>I read the book.</td>
    <td>Luin kirjan.</td>
    <td>En lukenut kirjaa.</td>
  </tr>
  <tr>
    <td>I bought the apples.</td>
    <td>Ostin omenat.</td>
    <td>En ostanut omenoita.</td>
  </tr>
</table>

This makes negation a **reliable way to recognize Partitiivi usage** in Finnish sentences.


Here is your new chapter on **Directions and Locations – Reittiohjeet ja Paikat** with explanations about **Mihin, Missä, and Mistä**, including **S (sisällä) and L (päällä, lähellä) forms**. The chapter also expands beyond directions to show how these constructions apply in other contexts.  


<a name="8"></a>
## 8. Directions and Locations – Reittiohjeet ja Paikat  

In Finnish, location and movement are described using specific cases. The three fundamental **questions** related to direction and location are:  

- **Mihin?** (Where to?) – Describes movement **toward** a place.  
- **Missä?** (Where?) – Describes being **in** a place.  
- **Mistä?** (Where from?) – Describes movement **away** from a place.  

These can apply both to **interior** (S: **sisällä**) and **surface/close proximity** (L: **päällä, lähellä**) locations, depending on the context.  

![alt_text](finnish_-_directions.svg)

### 8.1 Interior and Surface Locations  

In Finnish, the location of an object or a person is categorized as either **inside something (S)** or **on top of / near something (L)**. This affects the case endings:  

<table class="tan">
  <tr>
    <th>Question</th>
    <th>Interior (S)</th>
    <th>Surface (L)</th>
  </tr>
  <tr>
    <td><strong>Mihin?</strong> (Where to?)</td>
    <td>-<strong>Vn</strong> / -<strong>hVn</strong> (<em>kirjastoon</em> – to the library)</td>
    <td>-<strong>lle</strong> (<em>pöydälle</em> – onto the table)</td>
  </tr>
  <tr>
    <td><strong>Missä?</strong> (Where?)</td>
    <td>-<strong>ssa/ssä</strong> (<em>kirjastossa</em> – in the library)</td>
    <td>-<strong>lla</strong> (<em>pöydällä</em> – on the table)</td>
  </tr>
  <tr>
    <td><strong>Mistä?</strong> (Where from?)</td>
    <td>-<strong>sta/stä</strong> (<em>kirjastosta</em> – from the library)</td>
    <td>-<strong>lta</strong> (<em>pöydältä</em> – from the table)</td>
  </tr>
</table>

#### Examples:  
- **Mihin?**
    - Menen **kouluun**. (*I am going to school.*)  
    - Laitan kirjan **pöydälle**. (*I put the book on the table.*)  
- **Missä?**
    - Olen **koulussa**. (*I am at school.*)  
    - Kirja on **pöydällä**. (*The book is on the table.*)  
- **Mistä?**
    - Tulen **koulusta**. (*I come from school.*)  
    - Otan kirjan **pöydältä**. (*I take the book from the table.*)  

### 8.2 Common Places and their Forms  

<table class="tan">
  <tr>
    <th>English</th>
    <th>Mihin? (Where to?)</th>
    <th>Missä? (Where?)</th>
    <th>Mistä? (Where from?)</th>
  </tr>
  <tr>
    <td>Home</td>
    <td>koti<strong>in</strong></td>
    <td>kod<strong>issa</strong></td>
    <td>kod<strong>ista</strong></td>
  </tr>
  <tr>
    <td>Work</td>
    <td>töi<strong>hin</strong></td>
    <td>tö<strong>issä</strong></td>
    <td>tö<strong>istä</strong></td>
  </tr>
  <tr>
    <td>Finland</td>
    <td>Suome<strong>en</strong></td>
    <td>Suom<strong>essa</strong></td>
    <td>Suom<strong>esta</strong></td>
  </tr>
  <tr>
    <td>Restaurant</td>
    <td>ravintola<strong>an</strong></td>
    <td>ravintola<strong>ssa</strong></td>
    <td>ravintola<strong>sta</strong></td>
  </tr>
  <tr>
    <td>Beach</td>
    <td>rannalle</td>
    <td>rannalla</td>
    <td>rannalta</td>
  </tr>
</table>

**Special case**:

- **Koti** does not follow the typical -**Vn/-hVn** rule for **Mihin?**. Instead of **kotiin**, it could be **koti** in some cases:  
    - "Menen kotiin" (*I go home*), but **"Olen kotona"** (*I am at home*).  

### 8.3 Movement and State: The Essential Difference  

The **accusative (-n, -t)** and **partitive (-a, -ä, -ta, -tä)** cases interact with Mihin/Missä/Mistä to indicate the nature of an action:  

- **Accusative → Complete action**  
    - **Maalaan talon.** (*I paint the house.*) → Whole house gets painted.  
    - **Rakennan sillan.** (*I build a bridge.*) → Entire bridge is built.  
- **Partitive → Ongoing or incomplete action**  
    - **Maalaan taloa.** (*I am painting a house.*) → Ongoing action.  
    - **Rakennan siltaa.** (*I am building a bridge.*) → Not yet complete.  

These distinctions apply not only to movement but also to activities where an action is **gradual, repetitive, or partial**.  

### 8.4 Beyond Directions: Abstract Uses of Mihin, Missä, Mistä  

These cases are not used only for physical movement but also in **abstract expressions**, such as **topics, discussions, and emotions**.  

#### Speaking about something (Mistä?)

- **Puhun Suomesta.** (*I talk about Finland.*)  
- **Hän kertoi kirjasta.** (*He/She told about the book.*)  
- **En tiedä tästä asiasta.** (*I don’t know about this matter.*)  

#### Emotional states (Missä?)

- **Olen rakkaudessa.** (*I am in love.*)  
- **Hän on hyvässä kunnossa.** (*He/She is in good condition.*)  
- **Olemme vaikeassa tilanteessa.** (*We are in a difficult situation.*)  

#### Direction of influence (Mihin?)

- **Tämä vaikuttaa minuun.** (*This affects me.*)  
- **Usko itseesi!** (*Believe in yourself!*)  
- **Olen tyytyväinen päätökseen.** (*I am satisfied with the decision.*)  

As shown above, **Missä?** is often used for **states or conditions**, **Mistä?** for **topics of discussion**, and **Mihin?** for **things affecting someone or something**.  

### 8.5 Asking for Directions  

In everyday Finnish, when asking for directions, **Mihin?**, **Missä?**, and **Mistä?** are essential.  

#### Asking "Where?"

- **Missä on rautatieasema?** (*Where is the train station?*)  
- **Missä on lähin apteekki?** (*Where is the nearest pharmacy?*)  

#### Asking "How to get somewhere?"

- **Miten pääsen keskustaan?** (*How do I get to the city center?*)  
- **Mihin suuntaan minun pitää mennä?** (*Which way should I go?*)  

#### Asking "Where from?"

- **Mistä löydän bussipysäkin?** (*Where can I find a bus stop?*)  
- **Mistä voi ostaa junalipun?** (*Where can I buy a train ticket?*)  

These question structures are essential for navigating daily life in Finland.  


<a name="9"></a>
## 9. Past Tenses – Menneet Aikamuodot

![alt_text](finnish_-_past.jpg)

The Finnish language has three main past tenses: **Imperfekti**, **Perfekti**, and **Pluskvamperfekti**. Each of these has both a **negative** and a **question** form. This chapter explains these past tenses in detail with structured examples.

The **tense choice depends on whether the action is completed, ongoing, or happened before another event**, making it crucial for constructing correct sentences.

### 9.1 Imperfekti – Simple Past

The **Imperfekti** tense is used to describe **past events, completed actions, or repeated past actions**. It is equivalent to the **simple past tense** in English.

#### Example Sentences:

- **<r>Menin</r> eilen kauppaan ja <r>ostin</r> litran maitoa. Kotimatkalla <r>näin</r> mustan kissan.**  
  *(I went to the shop yesterday and bought a liter of milk. On the way home, I saw a black cat.)*  
- **Hän <r>soitti</r> eilen pitkään.** *(He/She called for a long time yesterday.)*  

#### Question Form:

- **<r>Menitkö</r> eilen kauppaan ja <r>ostitko</r> maitoa?**  
  *(Did you go to the shop yesterday and buy milk?)*  
- **<r>Soittiko</r> hän sinulle eilen?** *(Did he/she call you yesterday?)*  

#### 9.1.1 Forming Imperfekti

To form the **Imperfekti**, the key element is adding **-i-** before the personal endings. However, the changes in the stem depend on the vowel structure of the verb.

##### Rule 1: Verbs where <r>O, Ö, U, Y</r> Remain Unchanged
For verbs where the last vowel before the personal ending is **o, ö, u, y**, the vowel remains, and **-i-** is added.

E.g.:

- **puhua** → **puhu<r>i</r>n** (*I spoke*)
- **säilöä** → **säilö<r>i</r>n** (*I preserved*)
- **syntyä** → **synty<r>i</r>n** (*I was born*)

##### Rule 2: Verbs where <r>A, Ä, E, I</r> Disappear

If the last vowel before the personal ending is **a, ä, e, i**, it disappears when **-i-** is added.

E.g.:

- **odottaa** → **odot<r>i</r>n** (*I waited*)
- **opiskella** → **opiskel<r>i</r>n** (*I studied*)
- **uida** → **ui<r>n</r>** (*I swam*)

##### Rule 3: If the <r>First Syllable Contains "A"</r> and the Verb is <r>Two Syllables</r> Long

If the verb is **two syllables long** and its **first syllable contains "a"**, the **<r>a</r> changes to <r>o</r>** when forming the past tense.

E.g.:
- **alkaa** → **alo<r>i</r>n** (*I began*)
- **laulaa** → **laul<r>o</r>in** (*I sang*)

##### Rule 4: Special Vowel Combinations

If the verb contains **uo, yö, ie**, these vowels change in a specific way before adding **-i-**.

<table class="tan">
  <tr>
    <th>Vowel Change</th>
    <th>Example Verb</th>
    <th>Imperfekti Form</th>
  </tr>
  <tr>
    <td><strong>uo → oi</strong></td>
    <td><strong>tuoda</strong> (<em>to bring</em>)</td>
    <td><strong>toi<r>n</r></strong> (<em>I brought</em>)</td>
  </tr>
  <tr>
    <td><strong>yö → öi</strong></td>
    <td><strong>lyödä</strong> (<em>to hit</em>)</td>
    <td><strong>lö<r>i</r>n</strong> (<em>I hit</em>)</td>
  </tr>
  <tr>
    <td><strong>ie → ei</strong></td>
    <td><strong>tietää</strong> (<em>to know</em>)</td>
    <td><strong>tie<r>si</r>n</strong> (<em>I knew</em>)</td>
  </tr>
  <tr>
    <td><strong>VV → Vi</strong></td>
    <td><strong>jäädä</strong> (<em>to stay</em>)</td>
    <td><strong>jä<r>i</r>n</strong> (<em>I stayed</em>)</td>
  </tr>
</table>

##### Rule 5: If the Verb is <r>Type 4</r>

For any verbs belonging to **verb type 4**, add **-si-** instead of just **-i-**.

E.g.:
- **haluta** → **halu<r>si</r>n** (*I wanted*)
- **siivota** → **siivo<r>si</r>n** (*I cleaned*)


##### Rule 6: If the Verb is <r>Type 1 and Ends in -la, -na, -ra, -sta, -tä</r>

Some **type 1 verbs** that end in **-la, -na, -ra, -sta, -tä** take **-si-** instead of just **-i-**.

E.g.:
- **ymmärtää** → **ymmär<r>si</r>n** (*I understood*)
- **rakentaa** → **rakensi<r>n</r>** (*I built*)

#### 9.1.2 Irregular Verbs in Imperfekti

Some Finnish verbs have irregular past tense forms that do not follow the above, standard rules.

E.g.:

- **mennä** (*to go*) → **men<r>i</r>n** (*I went*)
- **tehdä** (*to do/make*) → **te<r>i</r>n** (*I did/made*)
- **nähdä** (*to see*) → **nä<r>i</r>n** (*I saw*)
- **olla** (*to be*) → **ol<r>i</r>n** (*I was*)

### 9.2 Imperfekti, Negatiivinen – Simple Past, Negative

The **negative Imperfekti** describes past actions that **did not happen**. 🤷‍♂️

#### Example Sentences:
- **En mennyt eilen kauppaan enkä ostanut maitoa. Kotimatkalla en nähnyt mustaa kissaa.**  
  *(I did not go to the shop yesterday, and I did not buy milk. On the way home, I did not see a black cat.)*  
- **Hän ei soittanut minulle eilen.** *(He/She did not call me yesterday.)*  

#### Question Form:
- **Etkö mennyt eilen kauppaan?** *(Did you not go to the shop yesterday?)*  
- **Eikö hän soittanut sinulle?** *(Did he/she not call you?)*  

#### 9.2.1 Forming Negative Imperfekti

The negative past tense in Finnish is formed using the **negative verb** (**en**, **et**, **ei**, **emme**, **ette**, **eivät**) + **the verb stem in its past participle form**. 

Unlike the affirmative imperfekti, the negative form **does not take -i- or -si- endings**.

##### Verb Type <r>1 and 2</r>

Drop **-a/-ä**, and add **<r>-nut/-nyt</r>** in singular or **<r>-neet</r>** in plural.

E.g.:

- **tanssia** → **en/et/ei tanssi<r>nut</r>**, **emme/ette/eivät tanssi<r>neet</r>**
- **nukkua** → **en/et/ei nukku<r>nut</r>**, **emme/ette/eivät nukku<r>neet</r>**
- **syödä** → **en/et/ei syö<r>nyt</r>**, **emme/ette/eivät syö<r>neet</r>**
- **tehdä** → **en/et/ei teh<r>nyt</r>**, **emme/ette/eivät teh<r>neet</r>**

##### Verb Type <r>3</r>

Drop **-la/-lä**, **-ta/-tä**, **-na/-nä**, **-ra/-rä**, and add **<r>-lut/-lyt</r>**, **<r>-sut/-syt</r>**, **<r>-nut/-nyt</r>**, **<r>-rut/-ryt</r>** in singular or **<r>-leet</r>**, **<r>-seet</r>**, **<r>-neet</r>**, **<r>-reet</r>** in plural.

E.g.:

- **kävellä** → **en/et/ei kävel<r>lyt</r>**, **emme/ette/eivät kävel<r>leet</r>**
- **nousta** → **en/et/ei nous<r>sut</r>**, **emme/ette/eivät nous<r>seet</r>**
- **mennä** → **en/et/ei men<r>nyt</r>**, **emme/ette/eivät men<r>neet</r>**
- **surra** → **en/et/ei sur<r>rut</r>**, **emme/ette/eivät sur<r>reet</r>**

##### Verb Type <r>4, 5, 6</r>

Drop **-ta/-tä**, and add **<r>-nnut/-nnyt</r>** in singular or **<r>-nneet</r>** in plural.

E.g.:

- **siivota** → **en/et/ei siivo<r>nnut</r>**, **emme/ette/eivät siivo<r>nneet</r>**
- **haluta** → **en/et/ei halu<r>nnut</r>**, **emme/ette/eivät halu<r>nneet</r>**
- **valita** → **en/et/ei vali<r>nnut</r>**, **emme/ette/eivät vali<r>nneet</r>**
- **tarvita** → **en/et/ei tarvi<r>nnut</r>**, **emme/ette/eivät tarvi<r>nneet</r>**
- **vanheta** → **en/et/ei vanhe<r>nnut</r>**, **emme/ette/eivät vanhe<r>nneet</r>**
- **rohjeta** → **en/et/ei rohje<r>nnut</r>**, **emme/ette/eivät rohjen<r>neet</r>**

### 9.3 Perfekti – Present Perfect

The **Perfekti** tense describes **an action that happened in the past but has relevance to the present**. It is similar to the **present perfect tense** in English.

#### Example Sentences:
- **Olen mennyt kauppaan ja ostanut maitoa. Kotimatkalla olen nähnyt mustan kissan.**  
  *(I have gone to the shop and bought milk. On the way home, I have seen a black cat.)*  
- **Olemme asuneet täällä viisi vuotta.** *(We have lived here for five years.)*  

#### Question Form:
- **Oletko mennyt kauppaan ja ostanut maitoa?** *(Have you gone to the shop and bought milk?)*  
- **Oletteko asuneet täällä kauan?** *(Have you lived here for a long time?)*  

### 9.4 Perfekti, Negatiivinen – Present Perfect, Negative

The **negative Perfekti** expresses that an action **has not happened up to the present moment**.

#### Example Sentences:
- **En ole mennyt kauppaan enkä ole ostanut maitoa. Kotimatkalla en ole nähnyt mustaa kissaa.**  
  *(I have not gone to the shop and have not bought milk. On the way home, I have not seen a black cat.)*  
- **Emme ole koskaan käyneet Suomessa.** *(We have never been to Finland.)*  

#### Question Form:
- **Etkö ole mennyt kauppaan?** *(Have you not gone to the shop?)*  
- **Eivätkö he ole soittaneet sinulle?** *(Have they not called you?)*  

### 9.5 Pluskvamperfekti – Past Perfect

The **Pluskvamperfekti** tense describes an action that **happened before another past action**. It is equivalent to the **past perfect tense** in English.

#### Example Sentences:
- **Olin mennyt kauppaan ja olin ostanut maitoa. Kotimatkalla olin nähnyt mustan kissan.**  
  *(I had gone to the shop and had bought milk. On the way home, I had seen a black cat.)*  
- **Olimme syöneet ennen kuin elokuva alkoi.** *(We had eaten before the movie started.)*  

#### Question Form:
- **Olitko mennyt kauppaan ennen kuin myymälä suljettiin?**  
  *(Had you gone to the shop before it closed?)*  
- **Olitteko jo nähneet elokuvan ennen eilistä?** *(Had you already seen the movie before yesterday?)*  

### 9.6 Pluskvamperfekti, Negatiivinen – Past Perfect, Negative

The **negative Pluskvamperfekti** expresses that an action **had not happened before another past event**.

#### Example Sentences:
- **En ollut mennyt kauppaan enkä ollut ostanut maitoa. Kotimatkalla en ollut nähnyt mustaa kissaa.**  
  *(I had not gone to the shop and had not bought milk. On the way home, I had not seen a black cat.)*  
- **Hän ei ollut syönyt ennen elokuvan alkua.** *(He/She had not eaten before the movie started.)*  

#### Question Form:
- **Etkö ollut mennyt kauppaan ennen kuin myymälä suljettiin?**  
  *(Had you not gone to the shop before it closed?)*  
- **Eivätkö he olleet nähneet tätä aiemmin?** *(Had they not seen this before?)*  

### 9.7 The Object Cases in Past Sentences

In **past tenses**, Finnish typically uses the **accusative case** for the object when the action is **completed**. This follows the **general rule that accusative marks a fully affected object**, while **partitive is used for incomplete, ongoing, or indefinite actions**. Since past tense sentences often describe actions that **happened and were completed**, the **accusative is naturally more common in these contexts**.

For example:

- **Luin kirjan.** *(I read the book.)* → **Accusative** (the entire book was read, action is complete)  
- **Luin kirjaa.** *(I was reading a book.)* → **Partitive** (ongoing action, not necessarily finished)  

This applies to **all past tenses**, but particularly to **Imperfekti** (simple past), **Perfekti** (present perfect), and **Pluskvamperfekti** (past perfect). However, if the past action was **ongoing, indefinite, or habitual**, partitive can still appear:

- **Olin lukemassa kirjaa.** *(I was reading a book.)* → **Partitive** (not a completed action)  
- **Olen syönyt omenat.** *(I have eaten the apples.)* → **Accusative** (all apples are eaten)  
- **Olen syönyt omenoita.** *(I have eaten some apples.)* → **Partitive** (some but not necessarily all apples were eaten)  

So, while **accusative is the default for completed actions in the past**, partitive still plays a role when describing actions that were **not fully completed or when emphasizing a process**.


Sure! Here’s your **10th chapter on Finnish conditional cases**, formatted properly in **Markdown** with **HTML tables** where necessary.


<a name="10"></a>
## 10. Conditional Mood – Konditionaali

![alt_text](finnish_-_conditional.jpg)

The **conditional mood** (**konditionaali**) is one of the most important **verb forms** in Finnish, allowing you to express **politeness, hypothetical situations, wishes, and polite questions**. It often corresponds to the English **"would"**, **"could"**, or **"should"**.

By mastering the **conditional mood**, you’ll be able to **communicate more naturally and politely in Finnish**!

In Finnish, conditional verbs have the characteristic **-isi-** suffix, which is added to the verb stem before the personal endings.

### 10.1 Expressing Politeness

One of the most common uses of the **conditional mood** is **politeness**, especially when making requests or asking for something **in a friendly and considerate way**.

A quite typical phrase for this is:

- **Haluaisin, että...** → *(I would like that...)*

This form softens direct requests and sounds **more polite** than the present tense.

**Examples**:

- **Haluaisin kahvia, kiitos.** *(I would like coffee, please.)*
- **Voisitko auttaa minua?** *(Could you help me?)*
- **Voisin ehkä tulla huomenna.** *(I might be able to come tomorrow.)*

### 10.2 Forming the Conditional

The **Present Conditional** (**Konditionaalin Preesens**) is formed by adding **-isi-** before the verb's personal ending:

E.g.:

- **syödä** (*to eat*) → minä **sö<r>isi</r>n**, sinä **sö<r>isi</r>t**, hän **sö<r>isi</r>**, me **sö<r>isi</r>mme**, te **sö<r>isi</r>tte**, he **sö<r>isi</r>vät**
- **juoda** (*to drink*) → minä **jo<r>isi</r>n**, sinä **jo<r>isi</r>t**, hän **jo<r>isi</r>**, me **jo<r>isi</r>mme**, te **jo<r>isi</r>tte**, he **jo<r>isi</r>vat**

### 10.3 Conditional in Hypothetical Situations

Conditional is also used when talking about **hypothetical situations, wishes, or events that might happen under certain conditions**.

- **Jos minulla olisi rahaa, ostaisin uuden auton.** *(If I had money, I would buy a new car.)*
- **Jos olisit täällä, olisin iloinen.** *(If you were here, I would be happy.)*
- **Menisin ulos, jos ei sataisi.** *(I would go outside if it weren’t raining.)*
- **Jos voittaisin lotossa, ostaisin talon.** *(If I won the lottery, I would buy a house.)*

### 10.4 Negative Conditional

To form the **negative conditional**, use the negative verb (**en, et, ei, emme, ette, eivät**) + **the verb stem + -isi-** (without a personal ending).

**Examples:**
- **En ostaisi sitä.** *(I would not buy it.)*
- **Emme lähtisi ilman sinua.** *(We would not leave without you.)*
- **He eivät tulisi ajoissa.** *(They wouldn’t come on time.)*

### 10.5 Conditional in Past Tense (Perfect Conditional)

The **perfect conditional** is used to **talk about something that would have happened if conditions had been different**.

It is formed with the auxiliary verb **"olisi" (would have been)** + **the past participle of the main verb**.

**Examples**:

- **Jos olisin tiennyt, olisin tullut aikaisemmin.** *(If I had known, I would have come earlier.)*
- **Olisitko tehnyt sen, jos olisit ollut siellä?** *(Would you have done it if you had been there?)*
- **Jos olisit sanonut jotain, olisin auttanut.** *(If you had said something, I would have helped.)*

### 10.6 Conditional in Questions

Conditional is often used in **questions** to sound **more polite** or to express **uncertainty**.

**Examples:**
- **Voisitko auttaa minua?** *(Could you help me?)*
- **Haluaisitteko tilata jotain?** *(Would you like to order something?)*
- **Mitä tekisit, jos voisit matkustaa minne vain?** *(What would you do if you could travel anywhere?)*


<a name="11"></a>
## 11. The Imperative Mood – Imperatiivi

<img src="/_ui/img/imgfx/logistics.png" title="" align="right" class="right">

The **imperative mood** (**imperatiivi**) in Finnish is used to give **commands, instructions, or strong suggestions**. It is equivalent to the English **"Go!", "Listen!", "Let's go!"**, and similar forms.

The imperative in Finnish has several variations:

1. **Second-person singular (~ sinä)**
2. **Second-person plural (~ te)**
3. **First-person plural (~ me) – the "Let's" form**
4. **Third-person singular and plural**
5. **Passive imperative ("one must")**

### 11.1 Second-Person Singular Imperative (Sinä)

Used when giving direct commands to **one** person.

- Formed by removing the **personal ending** from the verb stem.
- No pronoun (**sinä**) is used.

#### Examples:

- **Tule!** *(Come!)*
- **Katso!** *(Look!)*
- **Odota!** *(Wait!)*

For **negative commands**, use **älä + verb's stem (vartalo)**:

- **Älä tule!** *(Don't come!)* – from **tulla** (*to come*), stem **tule-**
- **Älä katso!** *(Don't look!)* – from **katsoa** (*to look*), stem **katso-**
- **Älä odota!** *(Don't wait!)* – from **odottaa** (*to wait*), stem **odota-**

### 11.2 Second-Person Plural Imperative (Te)

Used when addressing **multiple people**.

- Formed by keeping the **personal ending -kaa/-kää** attached to the verb stem.

#### Examples:

- **Tulkaa!** *(Come!)*
- **Katsokaa!** *(Look!)*
- **Odottakaa!** *(Wait!)*

The negative **second-person plural imperative** (**te-form**) uses the **weak imperative form** of the verb, which follows the **KPT consonant gradation** rules.  
So, for negative commands, use **älkää + verb’s weak imperative form**:

- **Älkää tulko!** *(Don't come!)* – from **tulla** (*to come*), weak imperative form **tulko**
- **Älkää katsoko!** *(Don't look!)* – from **katsoa** (*to look*), weak imperative form **katsoko**
- **Älkää odottako!** *(Don't wait!)* – from **odottaa** (*to wait*), weak imperative form **odottako**

### 11.3 First-Person Plural Imperative (Me) – "Let's" Form

Used when suggesting an action that includes **the speaker and others** (similar to English **"Let's do something"**).

- Formed using the **passive form** of the verb.

#### Examples:

- **Mennään kotiin!** *(Let's go home!)*
- **Syödään nyt!** *(Let's eat now!)*

For negative commands, use **ei + passive verb**:
- **Ei mennä vielä!** *(Let's not go yet!)*
- **Ei syödä nyt!** *(Let's not eat now!)*

### 11.4 Third-Person Imperative (Hän/He)

Used to express a wish or instruction for **someone else** (he/she/they).

- Uses the **-koon/-köön** ending for singular (hän).
- Uses the **-koot/-kööt** ending for plural (he).

#### Examples:

- **Tulkoon sisään!** *(Let him/her come in!)*
- **Katsokoot itse!** *(Let them look themselves!)*

For negatives, use **älköön + verb** (hän) or **älkööt + verb** (he):
- **Älköön tulko!** *(Let him/her not come!)*

### 11.5 Passive Imperative (General Command)

Used when giving **general instructions**, rules, or unspecified commands.

- Uses **-ttakoon/-ttäköön**.

#### Examples:

- **Tehtäköön näin!** *(Let it be done like this!)*
- **Pidettäköön tauko!** *(Let there be a break!)*

Negative form uses **älköön + passive verb**:
- **Älköön unohdettako!** *(Let it not be forgotten!)*

### 11.6 Spoken Language: "Me mennään"

In everyday spoken Finnish, the **"let's" form** (passive imperative) often replaces the standard **me** form.

#### Examples:

Here is your **Markdown table converted to HTML** format:

<table class="tan">
  <tr>
    <th>Standard Finnish (Kirjakieli)</th>
    <th>Spoken Finnish (Puhekieli)</th>
  </tr>
  <tr>
    <td>Menemme kotiin. <i>(We go home.)</i></td>
    <td>Me mennään kotiin. <i>(We go home.)</i></td>
  </tr>
  <tr>
    <td>Olemme valmiita. <i>(We are ready.)</i></td>
    <td>Me ollaan valmiita. <i>(We are ready.)</i></td>
  </tr>
</table>

**In negative**:

<table class="tan">
  <tr>
    <th>Standard Finnish</th>
    <th>Spoken Finnish</th>
  </tr>
  <tr>
    <td>Emme mene kotiin. <i>(We are not going home.)</i></td>
    <td>Me ei mennä kotiin. <i>(We are not going home.)</i></td>
  </tr>
</table>

Spoken Finnish uses **"me mennään"** instead of **"menemme"**, which is grammatically passive but functions as a first-person plural.


<a name="12"></a>
## The Impersonal Form – Passiivi

In Finnish, "passiivi" is the **impersonal form** of a verb. It's used to describe actions **without specifying who performs** them. The action is general and does not specify the doer. In English, it can correspond to "people in general", "we", "they", or "one", depending on the context.

A common example of this sentence structure is:

**Syntymäpäivillä syödään täytekakkua.** *(At birthday parties, layered cake is eaten.)*

### Why is "syödään" used here instead of e.g. "syön"?

- "**Syön**" means "I eat" (first person singular). It refers **specifically to the speaker**.
- "**Syödään**" (passiivi) means "is eaten" or "people eat." It's used when **no specific subject is mentioned, and the action applies generally**. The focus is on the action (eating cake) as something generally done at birthday parties, without pointing to a specific person.

Some more examples:

- **Kaupungilla kävellään.** (*People walk in the city.*)
- **Juhannusta juhlitaan kesäkuussa.** (*Midsummer is celebrated in June.*)
- **Kirjoitetaan kirje.** (*A letter is written.*)

### 12.1 Constructing Passiivi Forms

Forming the passiivi is relatively easy, as it follows the same pattern for all verb types except Verb Type 1.

##### Verb Type 1

<div class="info">
<p>Take the "minä-vartalo", then add <strong><r>-taan/-tään</r></strong>.</p>
</div>

E.g.:

- (minä) **tanssin** → **tanssi<r>taan</r>**
- (minä) **nukun** → **nuku<r>taan</r>**
- (minä) **kysyn** → **kysy<r>tään</r>**

##### Verb Type 2, 3, 4, 5 and 6

<div class="info">
<p>Start with the verb's perusmuoto (infinitive form), then modify it slightly by adding <strong><r>-an/-än</r></strong>.</p>
</div>

E.g.:

- **syödä** → **syödä<r>än</r>**
- **kävellä** → **kävellä<r>än</r>**
- **nousta** → **nousta<r>an</r>**
- **mennä** → **mennä<r>än</r>**
- **surra** → **surra<r>an</r>**
- **siivota** → **siivota<r>an</r>**
- **valita** → **valita<r>an</r>**
- **vanheta** → **vanheta<r>an</r>**

### 12.2 Passiivi is Used Quite Often

Passiivi is very commonly used in Finnish. Here is an example of a passage about Christmas, written entirely in passiivi:

>Joulu on lempijuhlani. Jouluna vietetään aikaa perheen kanssa. Ruokia valmistetaan yhdessä koko perheelle. Talot koristellaan kauniilla valoilla. Sukulaisia käydään katsomassa, vaikka heitä nähdään harvoin. Lahjat paketoidaan huolella. Pienille lapsille annetaan lahjoja. Lahjojen avaamista odotetaan innolla. Pienten lasten ilosta nautitaan, kun paketit avataan. Jouluna iloitaan yhdessä.
