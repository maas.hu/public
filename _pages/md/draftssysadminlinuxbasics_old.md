# ![alt text](/_ui/img/icons/babytux_m.svg) Linux Basics

<div style="text-align: right;"><h7>Posted: 2021-10-25 17:41</h7></div>

###### .

>Under construction and under translation...

## Table of Contents

1. [Introduction](#1)
2. [Files and Permissions](#2)
3. [Scheduling Jobs](#3)
4. [Boot and System Services](#4)
5. [Storage](#5)
6. [Network](#6)
7. [Processes](#7)
8. [Package Management](#8)
9. [System Diagnostics Tools](#9)
10. [Monitoring and Logging](#10)

<a name="1"></a>
## 1. Introduction

![alt_text](linux_bg.jpg)
...

<a name="2"></a>
## 2. Files and Permissions

...

Feldolgozandó témakörök: 

- `getfacl`, `setfacl`
- `find / -size +10M -exec ls -l {} ;`
- `tar`

...


<a name="5"></a>
## 5. Storage

### 5.1 Re-partinioning and formatting disks

Ebben a példában egyszerűen csak egy pendrive fájlrendszerét hozzuk létre újra NTFS típusúra, de a folyamat megegyezik bármilyen más diszk újrapartícionálásával. 

Először nézzük meg, mink van pl. az `lsblk` paranccsal. Ellenőrizzük kétszer is, hogy biztosan jó diszket piszkálunk-e majd meg, majd hívjuk meg az `fdisk` programot: 

```ksh
# lsblk
NAME          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda             8:0    0 119.2G  0 disk  
├─sda1          8:1    0     8M  0 part  /boot/efi
├─sda2          8:2    0    20G  0 part  /
├─sda3          8:3    0     8G  0 part  [SWAP]
└─sda4          8:4    0  91.2G  0 part  
  └─cr-auto-1 254:0    0  91.2G  0 crypt /home
sdb             8:16   1  14.3G  0 disk  
└─sdb1          8:17   1  14.3G  0 part  

# fdisk /dev/sdb

Welcome to fdisk (util-linux 2.36.2).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): p
Disk /dev/sdb: 14.32 GiB, 15376000000 bytes, 30031250 sectors
Disk model: Ultra USB 3.0   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device     Boot Start      End  Sectors  Size Id Type
/dev/sdb1        2048 30031249 30029202 14.3G  7 HPFS/NTFS/exFAT

Command (m for help): d
Selected partition 1
Partition 1 has been deleted.

Command (m for help): p
Disk /dev/sdb: 14.32 GiB, 15376000000 bytes, 30031250 sectors
Disk model: Ultra USB 3.0   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): 

Using default response p.
Partition number (1-4, default 1): 
First sector (2048-30031249, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-30031249, default 30031249): 

Created a new partition 1 of type 'Linux' and of size 14.3 GiB.

Command (m for help): t
Selected partition 1
Hex code or alias (type L to list all): L

00 Empty            24 NEC DOS          81 Minix / old Lin  bf Solaris        
01 FAT12            27 Hidden NTFS Win  82 Linux swap / So  c1 DRDOS/sec (FAT-
02 XENIX root       39 Plan 9           83 Linux            c4 DRDOS/sec (FAT-
03 XENIX usr        3c PartitionMagic   84 OS/2 hidden or   c6 DRDOS/sec (FAT-
04 FAT16 &gt;32M       40 Venix 80286      85 Linux extended   c7 Syrinx         
05 Extended         41 PPC PReP Boot    86 NTFS volume set  da Non-FS data    
06 FAT16            42 SFS              87 NTFS volume set  db CP/M / CTOS / .
07 HPFS/NTFS/exFAT  4d QNX4.x           88 Linux plaintext  de Dell Utility   
08 AIX              4e QNX4.x 2nd part  8e Linux LVM        df BootIt         
09 AIX bootable     4f QNX4.x 3rd part  93 Amoeba           e1 DOS access     
0a OS/2 Boot Manag  50 OnTrack DM       94 Amoeba BBT       e3 DOS R/O        
0b W95 FAT32        51 OnTrack DM6 Aux  9f BSD/OS           e4 SpeedStor      
0c W95 FAT32 (LBA)  52 CP/M             a0 IBM Thinkpad hi  ea Linux extended 
0e W95 FAT16 (LBA)  53 OnTrack DM6 Aux  a5 FreeBSD          eb BeOS fs        
0f W95 Ext'd (LBA)  54 OnTrackDM6       a6 OpenBSD          ee GPT            
10 OPUS             55 EZ-Drive         a7 NeXTSTEP         ef EFI (FAT-12/16/
11 Hidden FAT12     56 Golden Bow       a8 Darwin UFS       f0 Linux/PA-RISC b
12 Compaq diagnost  5c Priam Edisk      a9 NetBSD           f1 SpeedStor      
14 Hidden FAT16 &gt;3  61 SpeedStor        ab Darwin boot      f4 SpeedStor      
16 Hidden FAT16     63 GNU HURD or Sys  af HFS / HFS+       f2 DOS secondary  
17 Hidden HPFS/NTF  64 Novell Netware   b7 BSDI fs          fb VMware VMFS    
18 AST SmartSleep   65 Novell Netware   b8 BSDI swap        fc VMware VMKCORE 
1b Hidden W95 FAT3  70 DiskSecure Mult  bb Boot Wizard hid  fd Linux raid auto
1c Hidden W95 FAT3  75 PC/IX            bc Acronis FAT32 L  fe LANstep        
1e Hidden W95 FAT1  80 Old Minix        be Solaris boot     ff BBT            

Aliases:
   linux          - 83
   swap           - 82
   extended       - 05
   uefi           - EF
   raid           - FD
   lvm            - 8E
   linuxex        - 85
Hex code or alias (type L to list all): 7
Changed type of partition 'Linux' to 'HPFS/NTFS/exFAT'.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

Az `mkfs`-nek számos "kiterjesztése" van, itt most a már évek óta Linuxon is biztonságosan használható NTFS-es modulját vesszük igénybe, amivel pendrive-unk bármilyen rendszeren és vélhetően még okostévéken is gond nélkül használható lesz. 

```ksh
# mkfs.ntfs --help

Usage: mkntfs [options] device [number-of-sectors]

Basic options:
    -f, --fast                      Perform a quick format
    -Q, --quick                     Perform a quick format
    -L, --label STRING              Set the volume label
    -C, --enable-compression        Enable compression on the volume
    -I, --no-indexing               Disable indexing on the volume
    -n, --no-action                 Do not write to disk

Advanced options:
    -c, --cluster-size BYTES        Specify the cluster size for the volume
    -s, --sector-size BYTES         Specify the sector size for the device
    -p, --partition-start SECTOR    Specify the partition start sector
    -H, --heads NUM                 Specify the number of heads
    -S, --sectors-per-track NUM     Specify the number of sectors per track
    -z, --mft-zone-multiplier NUM   Set the MFT zone multiplier
    -T, --zero-time                 Fake the time to be 00:00 UTC, Jan 1, 1970
    -F, --force                     Force execution despite errors

Output options:
    -q, --quiet                     Quiet execution
    -v, --verbose                   Verbose execution
        --debug                     Very verbose execution

Help options:
    -V, --version                   Display version
    -l, --license                   Display licensing information
    -h, --help                      Display this help

Developers' email address: ntfs-3g-devel@lists.sf.net
News, support and information:  http://tuxera.com

# mkfs.ntfs -f -L SanDisk15G /dev/sdb1
Cluster size has been automatically set to 4096 bytes.
Creating NTFS volume structures.
mkntfs completed successfully. Have a nice day.

# sync
```

Ekkor már kihúzhatjuk az USB-ből a pendrive-ot, amelynek újracsatlakoztatáskor fel kellene magától csatolódnia a `/run/media/&gt;usernév&lt;` alá: 

```ksh
# lsblk
NAME          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda             8:0    0 119.2G  0 disk  
├─sda1          8:1    0     8M  0 part  /boot/efi
├─sda2          8:2    0    20G  0 part  /
├─sda3          8:3    0     8G  0 part  [SWAP]
└─sda4          8:4    0  91.2G  0 part  
  └─cr-auto-1 254:0    0  91.2G  0 crypt /home
sdb             8:16   1  14.3G  0 disk  
└─sdb1          8:17   1  14.3G  0 part  /run/media/mishi/SanDisk15G
```

### 5.2 Bootolható pendrive

Miután csatlakoztattunk egy pendrive-ot, **kétszer is ellenőrizzük, hogy mi is a meghajtójának az eszközneve**! 

Akár sima userként is meggyőződhetünk a partíciók jelenlegi kiosztásáról és aktivitásáról az `lsblk` paranccsal.  
Mivel a rendszer GUI-ja (az alábbi példában a GNOME) az adott user nevében csatolta fel a pendrive-ot automatikusan, így umountolni is tudjuk userként. 

Tegyünk is így: 

```ksh
$ lsblk
NAME          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda             8:0    0 119,2G  0 disk  
├─sda1          8:1    0     8M  0 part  
├─sda2          8:2    0    20G  0 part  /
├─sda3          8:3    0     8G  0 part  [SWAP]
└─sda4          8:4    0  91,2G  0 part  
  └─cr-auto-1 254:0    0  91,2G  0 crypt /home
sdb             8:16   1  14,3G  0 disk  
└─sdb1          8:17   1  14,3G  0 part  /run/media/gjakab/SANDISK15G
$ umount /dev/sdb1
$ lsblk
NAME          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda             8:0    0 119,2G  0 disk  
├─sda1          8:1    0     8M  0 part  
├─sda2          8:2    0    20G  0 part  /
├─sda3          8:3    0     8G  0 part  [SWAP]
└─sda4          8:4    0  91,2G  0 part  
  └─cr-auto-1 254:0    0  91,2G  0 crypt /home
sdb             8:16   1  14,3G  0 disk  
└─sdb1          8:17   1  14,3G  0 part  
```

Az image kiírásához és formázáshoz már roottá kell válni. Menjünk be az adott könyvtárba és egyszerűen a `dd` parancsot megfelelően felparaméterezve írjuk fel a lemezképet egy legalább akkora, de lehetőleg inkább nagyobb kapacitású pendrive-ra. (Jelen esetben egy ~15 GB-osra.): 

```ksh
# du -m openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso
868	openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso

# dd if=openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso of=/dev/sdb bs=4M conv=fdatasync
216+1 records in
216+1 records out
909115392 bytes (909 MB, 867 MiB) copied, 71.4042 s, 12.7 MB/s

# sync
```

A `dd`-nek az `status=progress` opciót is megadhatjuk, amennyiben az a telepített verzión támogatott. Roppant látványos. 


### 5.3 Partícionálás desktop Linux esetén

A partíciók a minimális bonyolultságú, mégis biztonságosan újratelepíthető szemléletmóddal kialakítva az alábbiak szerint képzelhető el (a `/home` egy LUKS jelszóval védett, titkosított partíción helyezkedik el): 

![alt_text](partitions.png)

Ezt úgy tudjuk fokozni, ha LVM-et is bevonunk. Tradícionálisan ekkor a `/boot`-nak (és ha kell, a `/boot/efi`-nek) továbbra is hagyományos partíciónak kell lennie, de a `/`, a swap és a `/home` egyaránt elhelyezkedhetnek egy `rootvg` (vagy tetszőlegesen elnevezett) nevű VG alatt. 

Lehetséges azonban – legalábbis openSUSE-n –, hogy csak egy 8 MB-os "**BIOS Boot**" típusú partíciót készítünk, ami fel sincs fájlrendszerként mount-olva sehová és azon kívül minden LVM alá kerül az alábbihoz hasonló módon (itt a `/home` kapott egy LUKS titkosítást is): 

```ksh
# lsblk
NAME                   MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda                      8:0    0 465.8G  0 disk  
├─sda1                   8:1    0     8M  0 part  
└─sda2                   8:2    0 465.8G  0 part  
  ├─rootvg-swaplv      254:0    0     8G  0 lvm   [SWAP]
  ├─rootvg-rootlv      254:1    0    30G  0 lvm   /
  └─rootvg-homelv      254:2    0 427.8G  0 lvm   
    └─cr_rootvg-homelv 254:3    0 427.7G  0 crypt /home

# df -hPT
Filesystem                   Type      Size  Used Avail Use% Mounted on
devtmpfs                     devtmpfs  3.8G     0  3.8G   0% /dev
tmpfs                        tmpfs     3.8G     0  3.8G   0% /dev/shm
tmpfs                        tmpfs     1.5G  9.9M  1.5G   1% /run
/dev/mapper/rootvg-rootlv    ext4       30G  4.8G   24G  18% /
tmpfs                        tmpfs     3.8G   16K  3.8G   1% /tmp
/dev/mapper/cr_rootvg-homelv ext4      420G  7.2M  399G   1% /home
tmpfs                        tmpfs     764M  100K  763M   1% /run/user/1000

# cat /etc/fstab
/dev/rootvg/rootlv            /      ext4  defaults      0  1
/dev/mapper/cr_rootvg-homelv  /home  ext4  data=ordered  0  2
/dev/rootvg/swaplv            swap   swap  defaults      0  0

# fdisk -l /dev/sda
Disk /dev/sda: 465.76 GiB, 500107862016 bytes, 976773168 sectors
Disk model: WDC  WDS500G2B0A
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 55D13FFF-E8B7-479E-9313-08E4E4809A16

Device     Start       End   Sectors   Size Type
/dev/sda1   2048     18431     16384     8M BIOS boot
/dev/sda2  18432 976773134 976754703 465.8G Linux LVM
```

Telepítéskor egy így nézett ki (openSUSE telepítője): 

1. A diszk nézet: 

![alt_text](partitions_disk.png "")

2. Az LVM nézet: 

![alt_text](partitions_lvm.png "")

További feldolgozandó témakörök: 

- `blkid`
- `df`
- `du`
- `vgextend`

...

```ksh
# du -ch * | sort -rh | head -5
11G	total
4.3G	openSUSE-Tumbleweed-DVD-x86_64-Snapshot20220930-Media.iso
2.2G	rocky-8.6-x86_64-minimal.iso
1.4G	rocky-9.0-x86_64-minimal-20220805.iso
1.1G	rocky-9.0-x86_64-xen-img-raw-20220924.zip
```

...

```ksh
# lvcreate -n tsm_file_24lv -L 2400G -i8 -I 4096 filepoolvg
...

# mkfs.ext4 /dev/filepoolvg/tsm_file_24lv
...

# tune2fs -m 0 /dev/mapper/filepoolvg-tsm_file_24lv
...

# mount /tsm/file/24
...
```

<a name="6"></a>
## 6. Network

...

Az "iproute" csomaggal érkező programok: 

```ksh
# rpm -ql iproute | grep bin/
/usr/sbin/arpd
/usr/sbin/bridge
/usr/sbin/ctstat
/usr/sbin/devlink
/usr/sbin/genl
/usr/sbin/ifcfg
/usr/sbin/ifstat
/usr/sbin/ip
/usr/sbin/lnstat
/usr/sbin/nstat
/usr/sbin/rdma
/usr/sbin/routef
/usr/sbin/routel
/usr/sbin/rtacct
/usr/sbin/rtmon
/usr/sbin/rtpr
/usr/sbin/rtstat
/usr/sbin/ss
/usr/sbin/tipc
```

A "NetworkManager" csomaggal érkező programok: 

```ksh
# rpm -ql NetworkManager | grep bin/
/usr/bin/nm-online
/usr/bin/nmcli
/usr/sbin/NetworkManager
/usr/sbin/ifdown
/usr/sbin/ifup
```

Az `lshw` paranccsal is megtudhatjuk az alapvető információit a hálókártyáinknak, de az `ethtool` ennél is tovább megy: 

```ksh
# ethtool enp0s3
Settings for enp0s3:
        Supported ports: [ TP ]
        Supported link modes:   10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Supported pause frame use: No
        Supports auto-negotiation: Yes
        Supported FEC modes: Not reported
        Advertised link modes:  10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Advertised FEC modes: Not reported
        Speed: 1000Mb/s
        Duplex: Full
        Auto-negotiation: on
        Port: Twisted Pair
        PHYAD: 0
        Transceiver: internal
        MDI-X: off (auto)
        Supports Wake-on: umbg
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```

Csak egy interfész részleteinek kilistázása: 

```ksh
# ip a s dev enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:72:b6:d3 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 79295sec preferred_lft 79295sec
    inet6 fe80::a00:27ff:fe72:b6d3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

A routing tábla kilistázása: 

```ksh
# ip r
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.107 metric 101
```

Csak az IPv6-os routing tábla listázásához az `ip -6 route show` parancsot hasznád (ha valaha is szükséged lesz erre). 

IP hozzáadása egy interfészhez hagyományosan (az `ifconfig` paranccsal): 

```ksh
# ifconfig enp0s3 192.168.1.14/24
```

Ugyanez az `ip` tool-al: 

```ksh
# ip address add 192.168.1.14/24 dev enp0s3
```

Default GW hozzáadása hagyományosan: 

```ksh
# route add default gw 192.168.0.1
```

Adding a new static route, e.g.: 

```ksh
# ip route add 192.168.122.128 via 192.168.101.102
```

Example for changing the `default` route: 

```ksh
# ip route show
default via 10.2.205.251 dev team0.204 proto static metric 400 
10.2.205.251 dev team0.204 proto static scope link metric 400 
10.3.204.0/23 dev team0.204 proto kernel scope link src 10.3.204.1 metric 400 

# ip route del 10.2.205.251

# ip route add default via 10.3.205.251
```

DHCP: a DHCP-t kezelő programok, mint pl. a `dhclient` vagy a `dhcpcd` a hálózat DHCP szerverétől egy IP címet, a default route-ot és általában még a DNS szervert szerezhetik meg. 

### 6.2 Konfigurálás ifupdown script-ekkel

Ehhez ide kell lépnünk és az alábbihoz hasonló fájlt keresnünk: 
```ksh
# cd /etc/sysconfig/network-scripts/

# ls -la
total 12
drwxr-xr-x. 2 root root 4096 Sep 22 07:56 .
drwxr-xr-x. 5 root root 4096 Oct 21 17:05 ..
-rw-r--r--. 1 root root  248 Sep 22 07:56 ifcfg-enp0s3
```

DHCP-s beállítás (kliensekhez ez ajánlott): 
```ksh
# cat ifcfg-enp0s3
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s3
UUID=98013505-7e46-4bf7-b485-940fd16b97d8
DEVICE=enp0s3
ONBOOT=yes
```

Statikus beállítás (szerverekhez): 

```ksh
# cp -p ifcfg-enp0s3 ifcfg-enp0s3_old

# vi ifcfg-enp0s3
...
BOOTPROTO=none
...
IPADDR=10.0.2.15
NETMASK=255.255.255.0
GATEWAY=10.0.2.2
DNS1=192.168.1.1
...

# ifdown enp0s3
Connection 'enp0s3' successfully deactivated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)

# ip a s dev enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:72:b6:d3 brd ff:ff:ff:ff:ff:ff

# ifup enp0s3
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)

# ip a s dev enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:72:b6:d3 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe72:b6d3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

# ping example.com
PING example.com (93.184.216.34) 56(84) bytes of data.
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=1 ttl=52 time=112 ms
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=2 ttl=52 time=112 ms
^C
--- example.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 111.657/112.045/112.433/0.388 ms
```

### 6.3 NetworkManager

Általában a modern desktop linuxokon csakis ezt használjuk már. Parancssorból az `nm-connection-editor`-al hozhatjuk be GUI-n az általánosan beállítható dolgait. Ugyanennek a TUI-s változata (karakteres megjelenés) az `nmtui`. 

A legtöbbet tudó eszköz, amit a csomagban kapunk, az az `nmcli`. Egyszerű listázás ezzel: 

```ksh
# nmcli
enp0s3: connected to enp0s3
        "Intel 82540EM"
        ethernet (e1000), 08:00:27:72:B6:D3, hw, mtu 1500
        ip4 default
        inet4 10.0.2.15/24
        route4 0.0.0.0/0
        route4 10.0.2.0/24
        inet6 fe80::a00:27ff:fe72:b6d3/64
        route6 fe80::/64
        route6 ff00::/8
...
```

Kilistázása annak, hogy milyen hálózataink is vannak és mely eszközeinket kezeli (manage-eli) a NetworkManager: 

```ksh
# nmcli c
NAME                UUID                                  TYPE      DEVICE
enp0s3              98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8

# nmcli d
DEVICE  TYPE      STATE      CONNECTION
enp0s3  ethernet  connected  enp0s3
enp0s8  ethernet  connected  Wired connection 1
lo      loopback  unmanaged  --
```

A `c` a rövidített formájú megadása a `connection`-nek, míg a `d` a `device`-nek.

A tool fő konfigurációs állománya a `/etc/NetworkManager/NetworkManager.conf`. Ebben állíthatjuk be többek között, ha esetleg valamelyik eszközünket mégsem szeretnénk, hogy ő kezelje. 

Konfigurálás az `nmcli` saját editorában: 

```ksh
# nmcli c
NAME                UUID                                  TYPE      DEVICE
enp0s3              98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8

# nmcli c down enp0s3
Connection 'enp0s3' successfully deactivated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/8)

# nmcli c edit enp0s3

===| nmcli interactive connection editor |===

Editing existing '802-3-ethernet' connection: 'enp0s3'

Type 'help' or '?' for available commands.
Type 'print' to show all the connection properties.
Type 'describe [&lt;setting&gt;.&lt;prop&gt;]' for detailed property description.

You may edit the following settings: connection, 802-3-ethernet (ethernet), 802-1x, dcb, sriov, ethtool, match, ipv4, ipv6, hostname, tc, proxy
nmcli> print connection
['connection' setting values]
connection.id:                          enp0s3
connection.uuid:                        98013505-7e46-4bf7-b485-940fd16b97d8
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              enp0s3
...
nmcli> set connection.id NAT Adapter
nmcli> print ipv4
['ipv4' setting values]
ipv4.method:                            auto
ipv4.dns:                               --
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         --
ipv4.gateway:                           --
ipv4.routes:                            --
...
nmcli> set ipv4.method manual
nmcli> set ipv4.addresses 10.0.2.15/24
nmcli> set ipv4.gateway 10.0.2.2
nmcli> set ipv4.dns 192.168.1.1
nmcli> print ipv4
['ipv4' setting values]
ipv4.method:                            manual
ipv4.dns:                               192.168.1.1
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         10.0.2.15/24
ipv4.gateway:                           10.0.2.2
ipv4.routes:                            --
...
nmcli> save
Connection 'NAT Adapter' (98013505-7e46-4bf7-b485-940fd16b97d8) successfully updated.
nmcli> quit

# nmcli c
NAME                UUID                                  TYPE      DEVICE
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8
NAT Adapter         98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  --

# nmcli c up "NAT Adapter"
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/9)

# nmcli c
NAME                UUID                                  TYPE      DEVICE
NAT Adapter         98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8
```

Example for setting up a static route permanently: 

```ksh
# nmcli c
NAME  UUID                                  TYPE      DEVICE 
enX0  bb702f81-ea50-3178-9033-5e8252da8e7b  ethernet  enX0   

# nmcli con edit enX0
[...]
nmcli> set ipv4.routes 192.168.101.0/24 192.168.122.1
nmcli> save persistent
Connection 'enX0' (bb702f81-ea50-3178-9033-5e8252da8e7b) successfully updated.
nmcli> quit
```

A WIFI-t ugyanúgy beállíthatjuk akár a GUI-s eszközzel kényelmesen. Ki-be kapcsolgatni akár CLI-ből is könnyen tudjuk: 

```ksh
# nmcli radio
WIFI-HW  WIFI     WWAN-HW  WWAN
enabled  enabled  enabled  enabled

# nmcli radio wifi off

# nmcli radio
WIFI-HW  WIFI      WWAN-HW  WWAN
enabled  disabled  enabled  enabled

# nmcli radio wifi on

# nmcli radio
WIFI-HW  WIFI     WWAN-HW  WWAN
enabled  enabled  enabled  enabled
```

### 6.4 A hostname beállítása

Az `/etc/hostname` szerkesztése (majd `reboot`) önmagában még nem mindig elég. Egy valós szerver esetében szerkeszteni kell az `/etc/hosts`-ot is. Sőt, egyes logokban is fura dolgokat láthatunk. 

Az `nmcli general hostname` is jó lehet, de a javasolt hostname-et vezérlő eszköz a `hostnamectl`: 

```ksh
# hostnamectl
   Static hostname: rockysrv1
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 69f007ebb8e24dba853b62fd7ffe56fb
           Boot ID: 5a8744ccecd841aaac9aab3c8da01405
    Virtualization: oracle
  Operating System: Rocky Linux 8.4 (Green Obsidian)
       CPE OS Name: cpe:/o:rocky:rocky:8.4:GA
            Kernel: Linux 4.18.0-305.19.1.el8_4.x86_64
      Architecture: x86-64
```

A hostname megváltoztatása: 

```ksh
# hostnamectl set-hostname rockysrv2

# reboot
...
```

### 6.5 Tűzfal

Az `iptables` vagy az `nftables` a háttérben dolgozó szoftveres tűzfal, ám konfigurálásuk hamar átcsaphat túlságosan bonyolultba. Közvetlen kezelésük helyett vannak már olyan tool-ok, mint az UFW (Ubuntun és a belőle származó rendszereken) vagy a FirewallD (RHEL-alapú disztribúciókon). 

- UFW: `ufw status`, `ufw allow ssh`, `ufw delete allow ssh`
- FirewallD: `firewall-cmd --list-all`, `firewall-cmd --add-port=22/tcp --permanent`, `firewall-cmd --remove-port=22/tcp --permanent`

Egy újonnan telepített Rocky 8-on már alapból engedélyezett az SSH is: 

```ksh
# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Változtatás esetén `--reload` szükséges: 

```ksh
# firewall-cmd --reload
success
```

### 6.6 Hálózat és port aktivitás

A programok socket-eket használnak hálózati kapcsolatok létrehozásához. A socket az egy cím és egy port kombinációja. Egy gépen csak egy cím és port kombináció lehet. 

A socket infók nézegetéséhez tradícionálisan a `netstat` parancsot használtuk (a "net-tools" csomag része), de már itt van az `ss` (az új "iproute" csomag része). 

```ksh
# ss -tulp
Netid    State     Recv-Q    Send-Q    Local Address:Port    Peer Address:Port    Process
tcp      LISTEN    0         128             0.0.0.0:ssh          0.0.0.0:*        users:(("sshd",pid=860,fd=5))
tcp      LISTEN    0         128                [::]:ssh             [::]:*        users:(("sshd",pid=860,fd=7))
```

További feldolgozandó témakörök: 

- `tcpdump`
- `dig`

...

<a name="7"></a>
## 7. Processes

...

By default, `w` shows 8 characters from the usernames only. Overriding it is possible with the `PROCPS_USERLEN` env. variable: 

```ksh
# w
 10:45:09 up 14 days, 17:15,  3 users,  load average: 3.44, 3.22, 3.06
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
jakab.gi pts/0    10.201.116.74    10:36    0.00s  0.08s  0.03s sshd: jakab.gipsz [priv]
jakab.gi pts/1    10.201.116.74    10:39   13.00s  0.05s  0.03s sshd: jakab.gipsz [priv]
kalman.m pts/2    10.201.116.74    10:42   43.00s  0.06s  0.03s sshd: kalman.mikorka [priv]

# export PROCPS_USERLEN=20

# w
 10:46:16 up 14 days, 17:16,  3 users,  load average: 3.99, 3.38, 3.13
USER                 TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
jakab.gipsz        pts/0    10.201.116.74    10:36    0.00s  0.09s  0.03s sshd: jakab.gipsz [priv]
jakab.gipsz        pts/1    10.201.116.74    10:39   56.00s  0.05s  0.03s sshd: jakab.gipsz [priv]
kalman.mikorka     pts/2    10.201.116.74    10:42    1:50   0.06s  0.03s sshd: kalman.mikorka [priv]
```

...

Feldolgozandó témakörök: 

- `ps -e --format uid,pid,ppid,%cpu,cmd`
- `nice`
- `tee`
- `screen`

<a name="8"></a>
## 8. Csomagkezelés

...

Feldolgozandó témakörök: 

- `--downloadonly`
- `/etc/yum.repos.d/`

...

<a name="9"></a>
## 9. System Diagnostics Tools

...

Az `udev` (*"Userspace `/dev`"*) végzi el a hardware-eszközök feltérképezését boot és a rendszer egész futása során. A hálózati eszközöket az újabb rendszereken már a *"predictible network interface names"* alapján, tehát előre 

...

Az `lspci`, `lsusb` megmutatják a PCI és USB eszközöket röviden: milyen eszközként lettek felismerve és mik a gyártóik. 

...

Az `lshw` mindenféle hardware infókat kiír. Azt is megtudhatjuk, hogy milyen driverek és milyen `/dev` alatti eszközök kerültek az egyes eszközök számára társítva. 

Szűkíthetjük az eszközök részletezését csak egy bizonyos kategóriára, pl.:

```ksh
# lshw -class network
  *-network:0
       description: Ethernet interface
       product: 82540EM Gigabit Ethernet Controller
       vendor: Intel Corporation
       physical id: 3
       bus info: pci@0000:00:03.0
       logical name: enp0s3
       version: 02
       serial: 08:00:27:72:b6:d3
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 32 bits
       clock: 66MHz
       capabilities: pm pcix bus_master cap_list ethernet physical tp 10bt 10bt-fd 100bt 100bt-fd 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=e1000 driverversion=7.3.21-k8-NAPI duplex=full ip=10.0.2.15 latency=64 link=yes mingnt=255 multicast=yes port=twisted pair speed=1Gbit/s
       resources: irq:19 memory:f0200000-f021ffff ioport:d020(size=8)
  *-network:1
       description: Ethernet interface
       product: 82540EM Gigabit Ethernet Controller
       vendor: Intel Corporation
       physical id: 8
       bus info: pci@0000:00:08.0
       logical name: enp0s8
       version: 02
       serial: 08:00:27:1d:a5:ef
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 32 bits
       clock: 66MHz
       capabilities: pm pcix bus_master cap_list ethernet physical tp 10bt 10bt-fd 100bt 100bt-fd 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=e1000 driverversion=7.3.21-k8-NAPI duplex=full ip=192.168.56.107 latency=64 link=yes mingnt=255 multicast=yes port=twisted pair speed=1Gbit/s
       resources: irq:16 memory:f0820000-f083ffff ioport:d240(size=8)
```

A `dmesg` a "kernel ring buffer" kiíratására vagy vezérlésére való segédprogram. 

...

#### Laptop akkumulátorának töltöttsége és élettartama

Egyes modellekben több akksi is van, ilyen pl. a Lenovo ThinkPad T440s is (a `capacity` az élettartam): 

```ksh
# for i in $(upower -e | grep battery) ; do echo $i: ; upower -i $i | grep -E "vendor|model|percentage|capacity" ; echo ; done
/org/freedesktop/UPower/devices/battery_BAT0:
  vendor:               SONY
  model:                45N1111
    percentage:          79%
    capacity:            89,0086%

/org/freedesktop/UPower/devices/battery_BAT1:
  vendor:               LGC
  model:                45N1127
    percentage:          38%
    capacity:            94,6763%
```

...

<a name="10"></a>
## 10. Monitoring and Logging

...

Az `uptime` nem csak a rendszer futási idejét és a bejelentkezett felhasználók számát mutatja meg, hanem egy általános rendszer-leterheltségi mutatót is közöl. Ez a *Load Average*: 

```ksh
# uptime
 09:55:34 up 68 days, 22:59,  2 users,  load average: 0.83, 0.31, 0.16
```

- Az utóbbi **1 percben** az átlagos terhelés értéke itt 0.83 volt,
- az utóbbi **5 perc** átlaga 0.31,
- az utóbbi **15 percé** pedig 0.16. 

Ez még teljesen normálisnak mondható akár egy 1 CPU-s gépen is. 

Az általános ökölszabály az, hogy addig felesleges izgulni, amíg a load average nem haladja meg a CPU-k (akár virtuális CPU-k) számát. A 2.00-s load average egy 1 CPU-s gépen jelenti a 100%-os terhelést. Ekkor már van miért izgulni: csak egy processz használhatja ki így a CPU-t, míg ha lenne egy másik, az kénytelen lenne várni. 2 CPU-s gépen a 2.00 csak teljes kihasználtság: az esetleges másik processz is hozzáférhet processzoridőhöz. 

Mesterséges terhelésre példa (de jó ide a [raku](/learning/it/prog/raku/#2) fejezetben megismert számítási próbatétel is): 

```ksh
# dd if=/dev/urandom of=/dev/null bs=1M count=10240
10240+0 records in
10240+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 56.0403 s, 192 MB/s
```

Eközben figyelhetjük az `uptime`-al: 

```ksh
# while true ; do uptime ; sleep 5 ; done
 10:31:34 up 68 days, 23:35,  3 users,  load average: 0.10, 0.10, 0.09
...
 10:32:24 up 68 days, 23:35,  3 users,  load average: 0.99, 0.32, 0.17
...
 10:32:49 up 68 days, 23:36,  4 users,  load average: 1.34, 0.46, 0.22
...
 10:34:44 up 68 days, 23:38,  4 users,  load average: 0.29, 0.36, 0.21
...
 10:36:34 up 68 days, 23:40,  4 users,  load average: 0.27, 0.32, 0.20
^C
```

Ugyanez a `top`-al is követhető: 

```ksh
# top
top - 10:32:49 up 68 days, 23:36,  4 users,  load average: 1.34, 0.46, 0.22
Tasks: 290 total,   3 running, 286 sleeping,   0 stopped,   1 zombie
%Cpu(s): 49.7 us, 49.8 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.3 hi,  0.2 si,  0.0 st
MiB Mem :  15832.7 total,   2947.9 free,   5255.4 used,   7629.3 buff/cache
MiB Swap:   4096.0 total,   4096.0 free,      0.0 used.  10153.4 avail Mem

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
2569343 root      20   0    8388   2984   1972 R  99.3   0.0   0:48.81 dd
2569329 root      20   0  212008 114576  36284 R  99.0   0.7   1:04.56 raku
...
```



...

További feldolgozandó témakörök: 

- `nmon`
- `sar`
- `vmstat`
- `/etc/rsyslog.conf`
- logrotate

...
