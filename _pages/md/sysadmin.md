# ⚙️ System Administration

![alt_text](sysadmin_-_bg.jpg)

## Operating Systems

<!-- - ![alt text](/_ui/img/icons/aix_s.svg) [AIX](/sysadmin/aix) -->
- ![alt text](/_ui/img/icons/babytux_s.svg) [Linux](/sysadmin/linux)
<!-- - ![alt text](/_ui/img/icons/solaris_s.svg) [Solaris](/sysadmin/solaris) -->
<!-- - ![alt text](/_ui/img/icons/win_s.svg) [Windows](/sysadmin/windows) -->

## Virtualization

### Type-1 Hypervisors

<!-- - ![alt text](/_ui/img/icons/powervm_s.svg) [IBM PowerVM](/sysadmin/powervm) -->
<!-- - ![alt text](/_ui/img/icons/babytux_s.svg) [KVM](/sysadmin/kvm) -->
<!-- - ![alt text](/_ui/img/icons/win_s.svg) [Microsoft Hyper-V](/sysadmin/hyperv) -->
- ![alt text](/_ui/img/icons/xen_s.svg) [Xen Project](/sysadmin/xen)
<!-- - ![alt text](/_ui/img/icons/vmware_s.svg) [VMware ESXi](/sysadmin/vmwesxi) -->

### Type-2 Hypervisors

<!-- - ![alt text](/_ui/img/icons/virtualbox_s.svg) [Oracle VM VirtualBox](/sysadmin/virtualbox) -->
<!-- - ![alt text](/_ui/img/icons/vmware_s.svg) [VMware Workstation Player](/sysadmin/vmwwp) -->
- ![alt text](/_ui/img/icons/qemu_s.svg) [QEMU](/sysadmin/qemu)

### Container Virtualization

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/docker_s.svg) [Docker](/sysadmin/docker) -->
<!-- - ![alt text](/_ui/img/icons/wpar_s.svg) [IBM AIX WPAR](/sysadmin/wpar) -->
<!-- - ![alt text](/_ui/img/icons/zone_s.svg) [Solaris/OpenIndiana Zone](/sysadmin/zone) -->

## Cluster Solutions

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/powerha_s.svg) [IBM PowerHA SystemMirror](/sysadmin/powerha) -->
<!-- - ![alt text](/_ui/img/icons/kubernetes_s.svg) [Kubernetes](/sysadmin/kubernetes) -->

## DevOps Tools

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/ansible_s.svg) [Ansible](/sysadmin/ansible) -->
<!-- - ![alt text](/_ui/img/icons/git_s.svg) [Git, GitLab, GitLab CI/CD](/sysadmin/git) -->
<!-- - ![alt text](/_ui/img/icons/terraform_s.svg) [Terraform](/sysadmin/terraform) -->

## Backup Solutions

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/commvault_s.svg) [Commvault](/sysadmin/commvault) -->
<!-- - ![alt text](/_ui/img/icons/networker_s.svg) [Dell EMC NetWorker](/sysadmin/networker) -->
<!-- - ![alt text](/_ui/img/icons/sp_s.svg) [IBM Spectrum Protect](/sysadmin/sp) -->

## Cloud Computing Platforms

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/azure_s.svg) [Microsoft Azure](/sysadmin/azure) -->
