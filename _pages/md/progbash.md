# ![alt text](/_ui/img/icons/bash_m.svg) Bash, KSH

<div style="text-align: right;"><h7>Posted: 2017-05-15 21:37</h7></div>

###### .

![alt_text](bash_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Shell Scripting Basics](#2)
3. [Redirections](#3)
4. [Conditionals](#4)
5. [Loops](#5)
6. [Functions](#6)
7. [Essential Text Processing and Search Tools for Scripting](#7)
8. [Environment Configuration](#8)
9. [Scripting Practicalities](#9)


<a name="1"></a>
## 1. Introduction

As for any other articles here, this one doesn't cover all the features and topics either, but attempts to go through on everything that might be important, even for a beginner in the subject. Also, the article is something to be worth constantly updated and improved due to the importancy of this beautiful programming language. 

As the cover image suggests: **Shell scripts will remain fundamental in an especially long term, probably even in the next 30 years.**

### 1.1 What is a Shell for?

Roughly, a Shell is the **user interface** for command line operations. Instead of a built-in component, like in case of DOS and Windows, Unix always had the flexibility to choose between different user interface implementations. Let's see the general architecture design of any Unix-like systems: 

![alt_text](bash_-_architecture.png)

Looking at this onionish diagram, you can notice the Shell represents the intermediate layer between the user and the Unix-like operating system's **Kernel**. The kernel controls the hardware of the computer and resides at the core of the architecture. The Shell interprets the user's commands, then forwards the necessary instructions towards to kernel through **System Calls**. The kernel then interacts with hardware. Of course, it operates also in backwards: when the kernel has *something to "say"* to the user, then the shell forwards it.

The core services, also as applications, may have access to interact with kernel directly, without the need to 

### 1.2 Historical Overview

<a href="bash_-_bell_labs.jpg" target="_blank" title="">
  <img src="bash_-_bell_labs.gif" class="right"></a>

The first Unix shell was the **Thompson shell**, the `sh`, introduced in **1971** and written by **Ken Thompson** at **Bell Labs**. The shell was distributed with versions 1 through 6 of **Unix** between 1971 to 1975. Though rudimentary by modern standards, it introduced many of the basic features common to all later Unix shells, including piping, simple control structures using `if` and `goto`, and filename wildcarding.

As a successor of the previous one, the **Bourne shell** was the default shell for Version 7 Unix in **1979**, developed by **Stephen Bourne**, also at Bell Labs. Unix-like systems still continued to have `/bin/sh` which might refer to the real Bourne shell, or it's most likely a symbolic link or hard link to a compatible shell. 

The successor of the previous shell was intruduced also quite long ago, in June 8, 1989. **Brian Fox** began coding Bash on January, 1988. The shell's name is an acronym for **Bourne-Again SHell**, a pun on the name of the Bourne shell that it replaces. 

This article wouldn't be complete if we missed mentioning the history behind **KornShell** (KSH or `ksh`). It was also a possible shell for Unix originally, developed by **David Korn** at Bell Labs in the early 1980s. The shell was announced at **USENIX** on **July 14, 1983**. The KornShell as `ksh88`, became the default shell on **AIX** in version 4, with `ksh93` being available separately. KSH still remained the default shell even on most recent AIX 7.3 systems. 


<a name="2"></a>
## 2. Shell Scripting Basics

Grab a terminal emulator, so start a `gnome-terminal` (GNOME), a `konsole` (KDE) or `xterm` (any X Window System), and follow along.

Some notes for all the code snippets:

- The code with gray background indicates it's in a config file, without explicitely showing the command that opened it.
- When you see `$` or `[any directory]>` in the prompt's position, it refers that the command could be issued as any regular user.
- The `#` as prompt refers the example command was issued as `root` user.

### 2.1 Hello World - the `echo` Command

Before going any further, it's always useful to ensure which Shell you're just using: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo $0</span>
bash
</pre>

The `echo` command is for displaying *"a line of text"* to **standard output**. The standard output is our default display to see what's the general result of our command or set of commands. 

By default, `echo` operates in the `-E` mode, which means disabling interpretation of backslash (`\`) escapes. It has two other, sometimes very useful operating modes:

- `-n` - do not output the trailing newline
- `-e` - enable interpretation of backslash escapes

Now let's see some examples for: 

- print a simple text to standard output
- using line break with `\`-sensitive operator
- not using line break at all

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo "Hello World!"</span>
Hello World!

<span class="psu">$</span> <span class="cmd">echo 'Hello World!'</span>
Hello World!

<span class="psu">$</span> <span class="cmd">echo -e 'Hello\nWorld!'</span>
Hello
World!

<span class="psu">$</span> <span class="cmd">echo -n 'Hello World!'</span>
Hello World!$ 
</pre>

### 2.2 Introduction to Variables

A Shell, as your primary "environment" has it's built-in variables, the so called environmental variables. You can list all of the variable<->value assignments by the `env` command: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">env</span>
SHELL=/bin/bash
SESSION_MANAGER=local/H92VFK3:@/tmp/.ICE-unix/3875,unix/H92VFK3:/tmp/.ICE-unix/3875
COLORTERM=truecolor
XDG_CONFIG_DIRS=/etc/xdg:/usr/local/etc/xdg:/usr/etc/xdg
LESS=-M -I -R
[...]
</pre>

These are traditionally all declared with full capital letters. You can use either these ones in your programs or you can simply define new ones, or override old ones. A very simple example for it: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">h=Hello</span>

<span class="psu">$</span> <span class="cmd">w=World</span>

<span class="psu">$</span> <span class="cmd">echo "$h $w!"</span>
Hello World!
</pre>

#### 2.2.1 Exporting variables

It's always important to note that when you will have a Shell program, saved into a script file, then it runs in an internal Shell, in a lower level or nested one. If you want to use your variables inside of it, then you need to **export** them. It can be easily simulated by entering to a nested Shell by `bash` command, then coming back by `exit`. E.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo $h</span>

<span class="psu">$</span> <span class="cmd">h=Hello</span>

<span class="psu">$</span> <span class="cmd">echo $h</span>
Hello

<span class="psu">$</span> <span class="cmd">bash</span>

<span class="psu">$</span> <span class="cmd">echo $h</span>

<span class="psu">$</span> <span class="cmd">exit</span>
exit

<span class="psu">$</span> <span class="cmd">export h=Hello</span>

<span class="psu">$</span> <span class="cmd">bash</span>

<span class="psu">$</span> <span class="cmd">echo $h</span>
Hello

<span class="psu">$</span> <span class="cmd">exit</span>
exit
</pre>

Notice you were able to call the `h` variable in the nested Shell after you used `export`. 


<a name="3"></a>
## 3. Redirections

Redirections are very powerful tools in most of the CLI programs, not only on Unix-like Shells. Both Bash and KSH supports all of the below ones since they are and all Unix-like systems are the same in their core: 

>In Unix, everything is a file.

Have you ever used the classic `COPY CON` command combination on DOS? (E.g. `COPY CON TEXTFILE.TXT`.) Using it, you could type everything that you wanted to be saved into the given textfile until you pressed `[CTRL+Z]`.  
Here's the Unix equivalent for the same, which is closed by `[CTRL+D]` instead: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat > textfile.txt</span>
Hello
World!
^D
</pre>

It's based on the most frequently used redirection that a Shell could provide, combining that `cat` program simply prints everything that's in its given input. 

Here is a collection of redirection-related operators:

- `>` - redirecting the standard output (file descriptor 1) to another file, e.g. `echo hello > textfile`
- `>>` - appending the standard output (file descriptor 1) to another file, e.g. `echo hello > existing_textfile`
- `<` - redirecting to command from standard input, e.g. `sort < textfile`
- `<<` - appending from the standard input with the *"here document"* structure, e.g. `wc << EOF`
- `<<<` the *"here-string"*. Instead of typing in text, you give a pre-made string of text to a program, e.g. `bc <<< 5*4`
- `x>` - redirecting the given file descriptor (usually the error output, so `2>`) to another file, e.g. `2> errorfile`
- `&x` - specifying the receiving file descriptor, e.g. `2>&1` (redirecting error output also to standard output)
- `|` - (the pipe operator) further processing the standard output of the previous command, e.g. `echo "5*4" | bc`

A wild example for using both `>` and `<` in a oneliner:

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo -e 'World!\nHello' > textfile && sort < textfile</span>
Hello
World!
</pre>

### 3.1 Combining Redirection with SUDO

An often used redirection when you have `sudo` rights, and you don't want to (or can't) switch to `root`, but want to write to a text file anyway:

<pre class="con">
<span class="psu">/opt></span> <span class="cmd">sudo cat > textfile</span>
bash: textfile: Permission denied
</pre>

- Solution 1 (simply exiting with pressing `[Ctrl+D]`):
<pre class="con">
<span class="psu">/opt></span> <span class="cmd">sudo bash -c 'cat > textfile'</span>
Hello
World!
^D
</pre>
- Solution 2 (the elegant way):
<pre class="con">
<span class="psu">/opt></span> <span class="cmd">sudo bash -c 'cat > textfile' << EOF</span>
> Hello
> World!
> EOF
</pre>

Note that `EOF` is just a random keyword, declared by you, but it's very often used by sysadmins.


<a name="4"></a>
## 4. Conditionals

Just like in any other programming languages, the conditionals represent the core of all the logic in a program. Especially `if` statements. 

### 4.1 The `if` Statement

A simple example for an `if` conditional expression:

<pre class="con">
<span class="psu">$</span> <span class="cmd">if true ; then echo hello ; else echo uhh ; fi</span>
hello
</pre>

The same in a bit more sophisticated way, using the `[ ]` brackets, and also evaluating what happens when the conditional is evaluated as `false`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">if [ true = false ] ; then echo hello ; else echo uhh ; fi</span>
hello

<span class="psu">$</span> <span class="cmd">if [ true = false ] ; then echo hello ; else echo uhh ; fi</span>
uhh
</pre>

This is also often referenced as "test", since we can basically do everything with `test` calls. E.g. here is the `test` equivalent of the last `if` block: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">test true = false && echo hello || echo false</span>
false
</pre>

### 4.2 Difference Between `[ ]` and `[[ ]]` in Bash `if` Statements

Bash provides two different ways to write conditional expressions:

1. `[ ]` - **Single Brackets - POSIX Compatible**
    - It invokes the built-in `test` **command**.
    - Requires spaces around operators, e.g. `[ "$var" = "value" ]`.
    - Strings should be **quoted** to avoid issues with empty values.
    - Does **not** support `&&` and `||` (must use `-a` and `-o` instead).
    - Limited pattern matching (no `=~` for regex).
2. `[[ ]]` - **Double Brackets - Bash-Specific, More Powerful**
    - A Bash **keyword**, not an external command.
    - Supports `&&` and `||` directly inside conditions.
    - Allows **regex matching** using `=~` (e.g., `[[ $var =~ ^abc.* ]]`).
    - Does **not require quoting** around variables in many cases (`[[ $var = value ]]` works safely).
    - Supports **string comparisons** like `<` and `>` without needing escaping.

Both requires spaces around operators (e.g., `[ $var = value ]` or `[ "$var" = "value" ]`, but definitely not `[$var=value]`).

#### Example Comparisons:

Both they should return with the **same results**:

```
if [ "$var" = "hello" ]; then echo "Matched"; fi
if [[ $var = hello ]]; then echo "Matched"; fi
```

**Using `[[ ]]` for regex matching**:

```
if [[ $var =~ ^hello[0-9]+$ ]]; then echo "Regex match!"; fi
```

**Using `&&` and `||` in `[[ ]]` (not possible in `[ ]`)**:

```
if [[ -f "$file" && -r "$file" ]]; then echo "File exists and is readable"; fi
```

For further comparison operators and mentions on differences between brackets, see the next sub-chapter.

#### Which One Should You Use?

- Use `[[ ]]` when writing **Bash-specific** scripts (more powerful and safer).
- Use `[ ]` if you need **POSIX compatibility** (portable but more limited).

### 4.3 Comparison Operators

Let's go through on pretty much all the possible comparison operators. 

Comparing integer data when the standard (POSIX-compatible) `[ ]` brackets are used:

- `-eq` - is equal to, e.g. `if [ "$a" -eq "$b" ]`
- `-ne` - is not equal to, e.g. `if [ "$a" -ne "$b" ]`
- `-gt` - is greater than, e.g. `if [ "$a" -gt "$b" ]`
- `-ge` - is greater than or equal to, e.g. `if [ "$a" -ge "$b" ]`
- `-lt` - is less than, e.g. `if [ "$a" -lt "$b" ]`
- `-le` - is less than or equal to, e.g. `if [ "$a" -le "$b" ]`

Comparing integer data when the `(( ))` double parentheses are used:

- `<` - is less than, e.g. `(("$a" < "$b"))`
- `<=` - is less than or equal to, e.g. `(("$a" <= "$b"))`
- `>` - is greater than, e.g. `(("$a" > "$b"))`
- `>=` - is greater than or equal to, e.g. `(("$a" >= "$b"))`

String comparisons:

- `=` - is equal to, e.g. `if [ "$a" = "$b" ]`  
  Note the whitespace framing the `=.`, e.g. `if [ "$a"="$b" ]` is not equivalent to the above!
- `==` - is equal to, e.g. `if [ "$a" == "$b" ]`  
  This is a synonym for `=.`  
  Note that the `==` comparison operator behaves differently within a double-brackets test than within single brackets: 
    - `[[ $a == z* ]]` is `true` if `$a` starts with a `"z"` (pattern matching).
    - `[[ $a == "z*" ]]` is `true` if `$a` is equal to `z*` (literal matching).
    - `[ $a == z* ]` - file globbing and word splitting take place.
    - `[ "$a" == "z*" ]` is `true` if `$a` is equal to `z*` (literal matching).
- `!=` - is not equal to, e.g. `if [ "$a" != "$b" ]`  
  This operator uses pattern matching within a `[[ ... ]]` construct.
- `<` - is less than, in ASCII alphabetical order, e.g.
    - `if [[ "$a" < "$b" ]]`
    - `if [ "$a" \< "$b" ]` <-- Note that the `<` needs to be escaped within a `[ ]` construct.
- `>` - is greater than, in ASCII alphabetical order, e.g.
    - `if [[ "$a" > "$b" ]]`
    - `if [ "$a" \> "$b" ]` <-- Note that the `>` needs to be escaped within a `[ ]` construct.
- `-z` - string is null, that is, has zero length, e.g. `[ -z "$String" ]`
- `-n` - string is not null, e.g. `[ -n "$String" ]`

### 4.4 Boolean Operators

These are the classical `AND`, `OR`, `NOT` and their combinations, but described with signs, not with text: 

- `&&` - logical `AND`
- `||` - logical `OR`
- `!` - logical `NOT`

Be aware where to place and which bracket-style you're using for these boolean/logical operators. E.g.:

```
file="/etc/passwd"

# `[ ]` does NOT support `&&` and `||` directly
if [ -f "$file" ] && [ -r "$file" ]; then echo "File exists and is readable"; fi

# `[[ ]]` supports `&&` and `||`
if [[ -f "$file" && -r "$file" ]]; then echo "File exists and is readable"; fi
```

### 4.5 File Evaluating Operators

There are quite many of them:

- `-b` - the file exists and is block special
- `-c` - the file exists and is character special
- `-d` - the file exists and is a directory
- `-e` - the file (simply) exists
- `-f` - the file exists and is a regular file
- `-g` - the file exists and is set-group-ID
- `-G` - the file exists and is owned by the effective group ID
- `-h` - the file exists and is a symbolic link (same as `-L`)
- `-k` - the file exists and has its sticky bit set
- `-L` - the file exists and is a symbolic link (same as `-h`)
- `-N` - the file exists and has been modified since it was last read
- `-O` - the file exists and is owned by the effective user ID
- `-p` - the file exists and is a named pipe
- `-r` - the file exists and the user has read access
- `-s` - the file exists and has a size greater than zero
- `-S` - the file exists and is a socket
- `-t FD` the file descriptor FD is opened on a terminal
- `-u` - the file exists and its set-user-ID bit is set
- `-w` - the file exists and the user has write access
- `-x` - the file exists and the user has execute (or search) access

Out of all this list, the `-f` and `-d` used the most frequently of course. 

Combining comparison, boolean and file avaluating operators is a very usual task, e.g.:

```
[...]
if [ $BLOCK_COUNT -lt 1 ] || [ $BLOCK_COUNT -gt 100 ];
  then >&2 echo "Specify a number between 1 and 100 for parameter -c!"
  exit 1
fi
if [ ! -d $WORKDIR ];
  then >&2 echo "The \"$WORKDIR\" is not an existing directory!"
  exit 1
fi
[...]
```

### 4.6 The `case` Statement

In Shell scripting, this poor thing is much more frequently used than in other programming languages. 

General structure of it:

```
case EXPRESSION in
  PATTERN_1)
    STATEMENTS
    ;;
  PATTERN_2)
    STATEMENTS
    ;;
  PATTERN_N)
    STATEMENTS
    ;;
  *)
    STATEMENTS
    ;;
esac
```

The `EXPRESSION` is usually a simple reference to a variable to evaluate. If that matches with any of the patterns listed, then that block will be executed.

The `case` stateement is especially useful when we leverage the power of it as a typical ending for our Shell program. E.g.:

```
[...]
help () {
  echo "Usage: $0 [OPTION]"
  echo "Script for blabla"
  echo
  echo "  -h, --help            Displays this help and exists"
  echo "  -t1, --task1          Deals with blabla task1"
  echo "  -t2, --task2          Deals with blabla task2"
  echo "  'no OPTION'           Deals both with blabla task1 and task2"
}

case "$1" in
  -h|--help)
    help
    ;;
  -t1|--task1)
    task1
    ;;
  -t2|--task2)
    task2
    ;;
  *)
    task1
    task2
    ;;
esac
```

<a name="5"></a>
## 5. Loops

Loops allow scripts and one-liners to execute a series of commands **repeatedly** until a particular condition is met. They are essential for **automating repetitive tasks** in shell scripts.

### 5.1 `for` Loops

A **`for` loop** runs a fixed number of iterations, making it ideal when the number of iterations is **known in advance**. It continues running **as long as there are elements left** to process.

This basic example demonstrates a `for` loop iterating over a list of words:

<pre class="con">
<span class="psu">$</span> <span class="cmd">for i in apple peach; do echo $i; done</span>
apple
peach
</pre>

In **Bash**, a `for` loop can also iterate over a **range of numbers** using `{start..end}` syntax. However, this feature is not supported in **older versions of KSH**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">for i in {1..10} ; do echo $i ; done</span>
1
2
3
4
5
6
7
8
9
10
</pre>

A more **portable** approach (compatible with both Bash and KSH) is the **C-style `for` loop**, which explicitly defines the start, condition, and increment:

<pre class="con">
<span class="psu">$</span> <span class="cmd">for ((i=0; i<=10; i++)) ; do echo $i ; done</span>
0
[...]
10
</pre>

### 5.2 `while` Loops

A **`while` loop** continues execution **as long as its condition remains true**. This makes it ideal when the number of iterations is **unknown in advance**.

For example, we can achieve the same numbered output as the `for` loop but using `while`. However, unlike `for`, we need to **manually increment** the variable inside the loop:

<pre class="con">
<span class="psu">$</span> <span class="cmd">i=1 ; while (( i < 11 )) ; do echo $i ; (( i++ )) ; done</span>
1
[...]
10
</pre>

An alternative using **`[[` syntax** for condition checking:

<pre class="con">
<span class="psu">$</span> <span class="cmd">i=1 ; while [[ $i -lt 11 ]] ; do echo $i ; (( i++ )) ; done</span>
1
[...]
10
</pre>

### 5.2.1 Iterating Through Files Line-by-Line

A `for` loop is useful for iterating over **words**, but when working with **full lines from a text file**, a `while` loop is preferred.

Example: Processing a text file **line by line**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat textfile</span>
Hello
World!

<span class="psu">$</span> <span class="cmd">while read LINE ; do echo "I say $LINE" ; done < textfile</span>
I say Hello
I say World!
</pre>

<div class="warn">
<p><code>LINE</code> is just a standard variable name, but it is commonly used by system administrators for readability.</p>
</div>

### 5.2.2 Infinite Loops with `while`

A `while` loop can be used to **continuously** execute commands, useful for **monitoring tasks**.

#### Example 1: Continuous Monitoring

An **infinite loop** (terminates only when manually stopped with **Ctrl+C**):

<pre class="con">
<span class="psu">$</span> <span class="cmd">while true ; do date ; ps -ef | grep [n]srexecd ; echo ----- ; sleep 5 ; done</span>
Thu May 20 15:27:20 CEST 2021
root     3105389       1  0 Mar24 ?        00:05:05 /usr/sbin/nsrexecd
-----
Thu May 20 15:27:25 CEST 2021
root     3105389       1  0 Mar24 ?        00:05:05 /usr/sbin/nsrexecd
-----
^C
</pre>

#### Example 2: Delayed First Execution

This variation ensures **nothing happens for 5 seconds initially**, then starts execution:

<pre class="con">
<span class="psu">$</span> <span class="cmd">while sleep 5 ; do date ; ps -ef | grep [n]srexecd ; echo ----- ; done</span>
Thu May 20 15:28:04 CEST 2021
root     3105389       1  0 Mar24 ?        00:05:05 /usr/sbin/nsrexecd
-----
Thu May 20 15:28:10 CEST 2021
root     3105389       1  0 Mar24 ?        00:05:05 /usr/sbin/nsrexecd
-----
^C
</pre>

#### Example 3: Generating Multiple Random Files

A **real-world use case** using a `while` loop in **KSH** to create **multiple random files** (⚠️ **Be cautious! This uses `dd`**):

<pre class="con">
<span class="psu">$</span> <span class="cmd">i=1 ; while (( i < 614 )) ; do dd if=/dev/random of=random_${i} bs=512k count=200 ; (( i++ )) ; done</span>
</pre>

### 5.3 Some Real-Life Examples

#### Counting Files Recursively in Directories  

If filenames contain **spaces**, a `while` loop ensures proper handling:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ls -1 > /tmp/dirlist; while read -r LINE; do echo "'$LINE': $(find "$LINE" -type f | wc -l) files"; done < /tmp/dirlist && rm /tmp/dirlist</span>
</pre>

#### Processing a List of Files for Deletion  

If a **previously collected list** contains filenames with spaces, using `while read` ensures safe deletion:

<pre class="con">
<span class="psu">$</span> <span class="cmd">while read -r LINE ; do rm -f "$LINE" ; done < /tmp/old_backups_-_$(date '+%Y%m%d')</span>
</pre>

#### Iterating Over Variable Names

Using `for` to **loop over variable names** and dynamically retrieve their values:

<pre class="con">
IMAGE_NAME=imagename
IMAGE_TAG=imagetag

VARS=("IMAGE_NAME" "IMAGE_TAG")
for VAR in "${VARS[@]}"
  do
    VAL="${!VAR}"
    echo "$VAR=$VAL" >> build.env
  done
</pre>

### Summary: Choosing the Right Loop

- Use `for` loops when the number of iterations is **known**.
- Use `while` loops when the number of iterations is **unknown**.
- Use `while read` loops when processing **file contents** line by line.
- Use infinite `while true` loops for **continuous monitoring** or waiting for an event.

By understanding and applying loops effectively, you can **automate repetitive tasks**, **process files efficiently**, and **monitor systems in real time**.


<a name="6"></a>
## 6. Functions

Functions are a powerful feature in any programming languages, allowing for better code reuse, organization, and readability. In shell programming, functions provide a way to encapsulate code into reusable blocks, so you can define a sequence of commands that can be executed whenever needed, without repeating the code.

Both Bash and KSH support functions, but there are some slight differences in their behavior.

### 6.1 Defining a Function

In both Bash and KSH, functions can be defined using the `function` keyword or simply with the function name followed by parentheses. Here's a basic function definition:

The next function, `greet`, takes a single argument (`$1`) and prints a greeting message:

```
# Define the function
greet() {
  echo "Hello, $1!"
}

# Call the function
greet "World"
```

This next function, `check_file`, checks if a file exists and reports the result. It demonstrates how functions can make repetitive tasks more concise and manageable: 

check_file() {
  if [ -f "$1" ]; then
    echo "File $1 exists."
  else
    echo "File $1 does not exist."
  fi
}

check_file "example.txt"

### 6.2 Return Values from Functions

In both Bash and KSH, you can return values from functions using the `return` statement, but note that `return` only accepts numeric values (exit status). For returning more complex data, you can use `echo` or other means of output.

```
add_numbers() {
  local sum=$(( $1 + $2 ))
  echo $sum
}

result=$(add_numbers 3 5)
echo "Sum is: $result"
```

In this example, the `add_numbers` function calculates the sum of two numbers and prints the result.

### 6.3 Differences Between Bash and KSH Functions

While the basic functionality of functions is the same in Bash and KSH, there are some differences:

- In **KSH**, variables defined inside a function are by default global, whereas in **Bash**, variables inside functions are local unless declared as `global` or with the `local` keyword.
- **Bash** supports `declare -A` for associative arrays inside functions, whereas **KSH** has more limited support for complex data types.
- **Bash** allows function definitions with or without parentheses, while **KSH** expects parentheses when defining functions.

### 6.4 Practical Example: Config Check on SUSE or RHEL-like Linux Systems 

It's always useful to take a system overview both before and after a reboot using widely accepted configuration check scripts, such as the following **gigantic** one-liner.  
The tasks assume that the user has full `sudo` rights with `NOPASSWD`.  

The function `s` first prints the command in brackets, informing the user about the command being executed:

<pre class="con">
<span class="psu">$</span> <span class="cmd">s () { comm=$1 ; echo "[ ${comm} ]" ; $comm ; echo ----- ; } ; \
     s "hostname" ; \
     s "date" ; \
     s "stat /" ; \
     s "uptime" ; \
     s "uname -a" ; \
     s "cat /proc/cmdline" ; \
     s "ip -4 a" ; \
     s "ip r" ; \
     s "nmcli c" ; \
     s "nmcli d" ; \
     BOND_IF=$(nmcli -t -f TYPE,DEVICE c show | awk -F: '$1 == "bond" {print $2}') ; \
     TEAM_IF=$(nmcli -t -f TYPE,DEVICE c show | awk -F: '$1 == "team" {print $2}') ; \
     if [ -n "$BOND_IF" ] ; then s "cat /proc/net/bonding/$BOND_IF" ; fi ; \
     if [ -n "$TEAM_IF" ] ; then s "sudo teamdctl $TEAM_IF state" ; fi ; \
     s "df -hPT" ; \
     s "lsblk" ; \
     s "cat /etc/fstab" ; \
     s "sudo pvs" ; \
     s "sudo vgs" ; \
     IS_SUSE=$(grep '^ID_LIKE=.*suse' /etc/os-release) ; \
     IS_RHEL=$(grep '^ID_LIKE=.*fedora' /etc/os-release) ; \
     if [ -n "$IS_SUSE" ] ; \
     then \
       s "cat /etc/issue.net" ; \
       s "sudo zypper repos" ; \
     fi ; \
     if [ -n "$IS_RHEL" ] ; \
     then \
       s "cat /etc/redhat-release" ; \
       s "sudo dnf repolist" ; \
     fi ; \
     s "systemctl list-unit-files --type=service --no-page" ; \
     s "systemctl list-timers -all"</span>
</pre>

...and its output is like this:

<pre class="con">
[ hostname ]
ABCDHOST
-----
[ date ]
Thu Feb 20 02:07:03 PM EET 2025
-----
[ stat / ]
  File: /
  Size: 4096      	Blocks: 8          IO Block: 4096   directory
Device: 254,1	Inode: 2           Links: 18
Access: (0755/drwxr-xr-x)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2025-02-19 13:14:13.437417740 +0200
Modify: 2025-01-26 21:45:18.846000579 +0200
Change: 2025-01-26 21:45:18.846000579 +0200
 Birth: 2022-02-08 21:16:10.000000000 +0200
-----
[ uptime ]
 14:07:03  up   4:51,  2 users,  load average: 1.22, 0.89, 0.74
-----
[ uname -a ]
Linux ABCDHOST 6.13.0-1-default #1 SMP PREEMPT_DYNAMIC Mon Jan 20 07:26:08 UTC 2025 (1513344) x86_64 x86_64 x86_64 GNU/Linux
[...]
</pre>


<a name="7"></a>
## 7. Essential Text Processing and Search Tools for Scripting

**Text processing and searching** are fundamental tasks in shell scripting, and programs like `grep`, `sed`, `awk`, and `find` provide powerful ways to manipulate and filter data efficiently. 

`grep` is ideal for pattern matching in text files, `sed` excels in stream editing and text substitution, `awk` offers advanced text processing with built-in scripting capabilities, and `find` helps locate files and directories based on various criteria. 

Mastering these tools enhances the ability to **automate tasks**, **extract relevant information**, and efficiently **process large amounts of data** in scripts.

### 7.1 `grep`

`grep` is one of the most powerful and widely used tools in Unix-like operating systems for searching and pattern matching in text. It stands for "**global regular expression print**", and allows users to search for specific patterns in files or streams of text.

The program is an essential tool for anyone working with shell scripts or performing system administration tasks. Understanding its features, such as **regular expressions** and combining it with other tools like `find`, can significantly improve your efficiency in searching through files and processes.

This chapter will cover the essential uses and variations of `grep`, focusing on practical examples and advanced use cases.

#### 7.1.1 `grep` Usage Basics

The simplest usage of `grep` involves searching for a specific string within a file. For example, you can search for the word `error` in a log file:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep "error" /var/log/syslog</span>
</pre>

This command will print all lines from the `syslog` file containing the word "error." By default, `grep` is case-sensitive. To make it case-insensitive, you can use the `-i` flag:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep -i "error" /var/log/syslog</span>
</pre>

#### 7.1.2 Using `grep` in Process Searching

When using `grep` to search the output of commands, such as `ps`, you might encounter situations where `grep` itself shows up in the results. To avoid this, you can use a trick in process searching. Instead of using `ps -ef | grep sshd | grep -v grep`, you can use:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ps -ef | grep [s]shd</span>
</pre>

This technique works because `[s]shd` matches `sshd` without matching the `grep` process itself, thus giving you a cleaner result.

#### 7.1.3 Using Extended Regular Expressions (ERE)

The `egrep` command is the same as `grep -E` and allows you to use **extended regular expressions** (**ERE**). Extended regular expressions support more complex pattern matching, including things like **alternation** (`|`) and **grouping**.

<pre class="con">
<span class="psu">$</span> <span class="cmd">egrep "error|warning" /var/log/syslog</span>
</pre>

This command will match lines that contain either "error" or "warning". Using `grep -E` gives the same result:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep -E "error|warning" /var/log/syslog</span>
</pre>

#### 7.1.4 Searching in Directories with `find` and `grep -l`

Combining `grep` with `find` is an effective way to search for a pattern across multiple files within a directory. The `grep -l` option is useful here, as it only prints the filenames of files that match the given pattern, making it easier to identify which files contain the search term:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find /path/to/directory -type f -exec grep -l "search_string" {} +</span>
</pre>

This command searches through all files in `/path/to/directory` and prints the names of the files containing "`search_string`."

<div class="info">
<p>The traditional ending of the above <code>find</code> one-liner is <code>{} \;</code> instead of <code>+</code>, but that acts slower with this usage.</p>
</div>

### 7.1.5 Using Escape Characters

When searching for special characters or patterns that might be interpreted by `grep` or regular expressions, you can use escape characters to match them literally. For example, to search for a pattern like `/ui` or `./index` and exclude it from your results, use:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep -v -E "\/ui|\.\/index" file.txt</span>
</pre>

This command excludes lines containing either `/ui` or `./index` from the results. The backslashes are used to escape characters like `/` and `.` that have special meanings in regular expressions.

### 7.1.6 Differences Between GNU `grep` and AIX `grep`

While `grep` is fairly consistent across platforms, there are some differences in functionality. One of the notable differences is the **AIX version of `grep`**, which includes the `-p` option for paragraph-based searching. This is particularly useful when working with configuration files in AIX that use the **stanza** format.

The `grep -p <pattern>` will match entire **paragraphs** (or stanzas) in AIX configuration files, where each paragraph is typically separated by a blank line. This is a feature that is unique to AIX `grep` and can be super helpful when working with configuration files that require context-based searching.

For instance, you can use search the whole block of user `smith` in the AIX-standard `/etc/security/passwd` file:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo grep -p smith /etc/security/passwd</span>
smith:
  password = MGURSj.F056Dj
  lastupdate = 623078865
  flags = ADMIN,NOCHECK 
</pre>

### 7.2 `sed`

The command `sed` stands for "**stream editor**", and it is generally used for *"filtering and transforming text"*. In practice, it is usually used for very quick pattern replacements in text outputs. Sometimes these tasks can be done also either by `awk` or `cut`, but `sed` is often turns out to be the best and fastest option. 

#### 7.2.1 Simple replacements

Replacing all the **first** characters in each line...

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat testfile | sed 's/^/B/g'</span>
Balma
Bdió
Bmogyoró
</pre>

...and doing the same with all the **last** ones: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat testfile | sed 's/$/B/g'</span>
almaB
dióB
mogyoróB
</pre>

#### 7.2.2 Using inline commands in `sed` command

It requires using double quotes: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat testfile | sed "s/^/$(uname -s) | /g"</span>
Linux | alma
Linux | dió
Linux | mogyoró

<span class="psu">$</span> <span class="cmd">DATE=$(date '+%Y-%m-%d %H:%M:%S')</span>

<span class="psu">$</span> <span class="cmd">cat testfile | sed "s/^/$DATE | /g"</span>
2021-05-20 14:14:07 | alma
2021-05-20 14:14:07 | dió
2021-05-20 14:14:07 | mogyoró
</pre>

#### 7.2.3 Boolean operations within `sed` regex

Example for not only using `or` operation but also using ignoring the special meaning for the next character (`[` and `]`):

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo $IP</span>
[192.168.1.11]

<span class="psu">$</span> <span class="cmd">echo $IP | sed -Ee 's/\[|\]//g'</span>
192.168.1.11
</pre>

#### 7.2.4 The "in-place" parameter of `sed`

`sed` can also apply your desired changes directly into the file with the `-i` (or `--in-place`) parameter. 

The manual of `sed` advises to use the `SUFFIX` parameter too, so the program will keep a backup of the file, just in case it didn't well for you:

```
-i[SUFFIX], --in-place[=SUFFIX]

              edit files in place (makes backup if SUFFIX supplied)
```

Of course, if you tested your replacements in advance by printing them out to standard output first, then that can be sufficient. Here's an example for finding the patterns in any `index.html` files under a directory first, then finding and replacing the pattern by another one by the "in-place" feature: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -name 'index.html' -exec grep -l showdown.min.js {} \;</span>
./it/sysadmin/hyperv/index.html
./it/sysadmin/vmwwp/index.html
./it/sysadmin/commvault/index.html
./it/sysadmin/kvm/index.html
[...]

<span class="psu">$</span> <span class="cmd">find . -name 'index.html' -exec sed -i 's/showdown.min.js/showdown.js/g' {} \;
</pre>

#### 7.2.5 Special operations

Deleting empty lines from a text file is sometimes useful. 

- POSIX format, so it works fine also on AIX:  `sed '/^[[:space:]]*$/d'`
- GNU sed's format: `sed '/^$/d'`

E.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat tuned.conf | grep -v ^# | sed '/^$/d'</span>
[main]
summary=Broadly applicable tuning that provides excellent performance across a variety of common server workloads
[variables]
thunderx_cpuinfo_regex=CPU part\s+:\s+(0x0?516)|(0x0?af)|(0x0?a[0-3])|(0x0?b8)\b
amd_cpuinfo_regex=model name\s+:.*\bAMD\b
[cpu]
governor=performance
energy_perf_bias=performance
[...]
</pre>

### 7.3 `awk`

AWK is a versatile and powerful text processing tool that is commonly used in shell scripting to manipulate and analyze text data. It operates on a per-line basis, processing data in fields (columns) separated by whitespace or custom delimiters. AWK can be employed for everything from simple data extraction to complex reporting, making it an essential tool in any shell programmer's toolkit.

The name "AWK" is abbreviated from the names of the original developers – **Aho, Weinberger, and Kernighan**.

This chapter covers the most important aspects of AWK, along with practical examples to help you get started with using it in your scripts.

#### 7.3.1 AWK Basics: Field Separator

One of the core features of `awk` is its ability to treat whitespace as the default field separator. This allows you to easily extract and manipulate specific fields in structured text such as the output of commands like `df` or `ps`.

For example, to display specific columns from the `df` command output:

<pre class="con">
<span class="psu">$</span> <span class="cmd">df /home | awk '{print $1, $6}'</span>
Filesystem Mounted
/dev/nvme0n1p1 /
</pre>

In this case, `$1` represents the first column (`Filesystem`), and `$6` represents the sixth column (`Mounted`). AWK automatically splits each line into fields, and you can reference them using `$1`, `$2`, and so on.

#### 7.3.2 Searching with AWK

AWK is also useful for searching patterns in text, similar to `grep`. It allows you to match specific text patterns and print matching lines. A common use case is filtering process lists with `ps`.

For example, to search for all instances of the `sssd` service, you can use:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ps -ef | awk '/sssd/'</span>
root     14519     1  0 Oct17 ?        00:00:36 /usr/sbin/sssd -i --logger=files
root     14520 14519  0 Oct17 ?        01:08:11 /usr/libexec/sssd/sssd_be --domain ipa.mycompany.com --uid 0 --gid 0 --logger=files
[...]
</pre>

#### 7.3.3 Logical Operators in AWK

AWK supports logical operators such as `||` (OR) and `&&` (AND), making it easy to combine multiple conditions for matching data.

For example, to display lines that contain either "xfs" or "reiserfs" in the output of `df -hPT`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">df -hPT | awk '/xfs/ || /reiserfs/'</span>
/dev/nvme0n1p1 xfs 150G 125G 26G 84% /
</pre>

This allows for more advanced filtering with multiple conditions.

Another example for applying multiple logical operators into a complex regex search:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo sysctl -a | awk '(/net.ipv4/) && (/\.forward/ || /.ip_forward/)'</span>
net.ipv4.conf.all.forwarding = 1
net.ipv4.conf.default.forwarding = 1
net.ipv4.conf.eth0.forwarding = 1
net.ipv4.conf.eth1.forwarding = 1
net.ipv4.conf.lo.forwarding = 1
net.ipv4.conf.virbr-dev.forwarding = 1
net.ipv4.conf.vnet0.forwarding = 1
net.ipv4.ip_forward = 1
net.ipv4.ip_forward_update_priority = 1
net.ipv4.ip_forward_use_pmtu = 0
</pre>

#### 7.3.4 `BEGIN` and `END` Blocks

AWK allows you to define `BEGIN` and `END` blocks that are executed **before** any input is processed or **after** all input has been processed, respectively. The `BEGIN` block is typically used to set up initial conditions or variables.

For example, to set custom field separators before processing the data:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat /etc/passwd | awk 'BEGIN{FS=":"} {print $5}'</span>
Gipsz Jakab
</pre>

This example uses the `BEGIN` block to set the field separator (`FS`) to `:`. Then, the `print $5` command outputs the fifth field (the user's full name) from the `/etc/passwd` file.

##### 7.3.4.1 Parameters for `BEGIN` block

Here's the list of them, along with a brief explanation and an example of each:

- `FS` - **Field Separator**: defines the character or regular expression that awk uses to separate fields in each input line
    - Default: Whitespace (spaces or tabs)
    - E.g.: `awk 'BEGIN { FS=":" } { print $1 }' /etc/passwd`
- `OFS` - **Output Field Separator**: specifies the character to be used to separate fields in the output
    - Default: A single space
    - E.g.: `awk 'BEGIN { OFS="," } { print $1, $2 }' file.txt`
- `RS` - **Record Separator**: defines the character or regular expression that separates records (lines) in the input
    - Default: A newline (`\n`)
    - E.g.: `awk 'BEGIN { RS=";" } { print $1 }' file.txt`
- `ORS` - **Output Record Separator**: specifies the string to be used at the end of each output record (line)
    - Default: A newline character (`\n`)
    - E.g.: `awk 'BEGIN { ORS="; " } { print $1 }' file.txt`
- `NF` - **Number of Fields**: represents the number of fields in the current record. It is automatically set by awk when processing a line
    - E.g.: `awk 'BEGIN { FS="," } { print NF }' file.csv`
- `NR` - **Number of Records**: keeps track of the total number of records (lines) processed so far
    - E.g.: `awk 'BEGIN { print "Processing started"; } { print NR, $0 }' file.txt`
- `FILENAME`: Holds the name of the current file being processed
    - E.g.: `awk 'BEGIN { print "Processing file:", FILENAME }' file.txt`
- `FPAT` - **Field Pattern**: specifies the regular expression pattern used for field splitting in awk. This is more advanced and is useful for complex field parsing scenarios.
    - E.g.: `awk 'BEGIN { FPAT="[^,]+" } { print $1 }' file.txt`
- `CONVFMT`: Controls the format used for converting numbers to strings when printing
    - Default: `%.6g`
    - E.g.: `awk 'BEGIN { CONVFMT="%.2f" } { print $1 }' file.txt`
- `FIELDWIDTHS`: Allows awk to treat the input file as fixed-width fields rather than using delimiters. You specify the field widths as a space-separated list.
    - E.g.: `awk 'BEGIN { FIELDWIDTHS="10 10 10" } { print $1, $2, $3 }' file.txt`
- `IGNORECASE`: When set to `1`, it makes AWK case-insensitive in pattern matching.
    - Default: `0` (case-sensitive).
    - E.g.: `awk 'BEGIN { IGNORECASE=1 } { if ($1 == "hello") print $0 }' file.txt`
- `ENVIRON`: This is an associative array that holds the values of the environment variables. You can access environment variables like `$PATH`, `$HOME`, etc., within AWK scripts.
    - E.g.: `awk 'BEGIN { print ENVIRON["HOME"] }'`

##### 7.3.4.2 Practical use of `END` block

Generally, all the above listed parameters could be also accessed in the `END` block, however, the primary use of the `END` block is **to perform actions once all lines of input have been processed**, such as printing out summaries or calculations based on all the data processed.

E.g. let's replace `wc -l`'s functionality by an AWK one-liner:

<pre class="con">
<span class="psu">$</span> <span class="cmd">awk 'BEGIN { count = 0 } { count++ } END { print "Total Lines:", count }' /etc/passwd</span>
Total Lines: 49
</pre>

#### 7.3.5 Importing Variables into AWK

You can pass external shell variables into AWK using the `-v` option. This is particularly useful when you want to perform searches or calculations using shell variables.

For example, to replace a string in a file with a shell variable:

<pre class="con">
<span class="psu">$</span> <span class="cmd">FROMSTR="data"</span>

<span class="psu">$</span> <span class="cmd">TOSTR="opt"</span>

<span class="psu">$</span> <span class="cmd">head -3 /tmp/xyz_backups_to_save_20230320 | awk -v srch="/$FROMSTR/" -v repl="/$TOSTR/" '{ sub(srch,repl,$0); print $0 }'</span>
/opt/xyz-backups/xyz-abc-backup/2022/2022-10-11/2022-10-11_18-23-26.327.1665512606327
/opt/xyz-backups/xyz-abc-backup/2022/2022-10-06/2022-10-06_18-24-53.320.1665080693320
/opt/xyz-backups/xyz-abc-backup/2022/2022-10-07/2022-10-07_18-22-08.994.1665166928994
</pre>

In this example, the `awk` command uses shell variables `FROMSTR` and `TOSTR` to perform a search and replace within the output of a command.

#### 7.3.6 AWK for Mathematical Operations

AWK has built-in support for arithmetic operations, making it a handy tool for calculations. It can handle operations such as addition, subtraction, multiplication, division, and modulus.

For example, to calculate the sum of disk usage in GiB:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo du -akx -d 1 / 2>/dev/null</span>
129540	/root
22636	/etc
5417428	/var
[...]

<span class="psu">$</span> <span class="cmd">sudo du -akx -d 1 / 2>/dev/null | awk '{sum +=$1} END {print sum/1024/1024" GiB"}'</span>
39.4566 GiB
</pre>

Here's the most simple example of using division in AWK:

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo | awk '{print 100 / 4}'</span>
25
</pre>

A more practical example for the same which can be re-used for our scripts:

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo | awk '{num1=100; num2=4; print num1 / num2}'</span>
25
</pre>

#### 7.3.7 Comparing Values with AWK

AWK also allows for comparisons. You can use it to compare strings, numbers, or even dates.

For example, comparing version numbers:

<pre class="con">
<span class="psu">$</span> <span class="cmd">X=7.3.0.5-663138</span>

<span class="psu">$</span> <span class="cmd">echo "$(echo $X | awk 'BEGIN{FS=".";OFS="."}{print $1,$2}') > 7.2" | bc</span>
1
</pre>

This checks whether the version stored in `$X` is greater than `7.2` and prints `1` if it is true.

#### 7.3.8 Metacharacters in AWK

AWK supports regular expressions and metacharacters, which allow you to perform advanced pattern matching.

- `.` - any single character
- `\` - escape character, used to match special characters literally
- `^` - beginning of the line
- `$` - end of the line
- `[]` - matches any one of the characters in the brackets
- `*` - zero or more occurrences
- `+` - one or more occurrences
- `{n}` - exactly `n` occurrences
- `()` - grouping, used for capturing and repeating patterns

For example, to match a string starting with `ab` and ending with `c`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">echo "abc abbbbc ac" | awk '/ab*c/'</span>
abc abbbbc ac
</pre>

#### 7.3.9 Loops in AWK

AWK supports looping structures, making it a powerful tool for iterating over **fields, rows, or structured text patterns**. Loops in AWK are especially useful when dealing with **complex, structured data formats** that require parsing and transformation.

Key benefits of using AWK's loop:

- Unlike simple `grep` or `sed` commands, AWK can **iterate through structured data** and apply logic dynamically.
- Loops allow **pattern-based extraction** instead of relying on fixed field positions.
- This method is **flexible** and works even if additional fields exist in the structured list.

##### 7.3.9.1 Example: Iterating Through a Simple List

If you're lucky, the query's original output is relatively well-structured already. 

Here's the original output of our example command:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -g -t -f ipv4.routes c show bond-con.2504</span>
10.201.0.0/16 10.25.5.251, 10.224.0.0/12 10.25.5.251, 
</pre>

So, you just need to go through on the fields after the field separator is specified:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -g -t -f ipv4.routes c show bond-con.2504 | awk 'BEGIN{FS=", "}{for (i=1; i<=NF; i++) print $i}'</span>
10.201.0.0/16 10.25.5.251
10.224.0.0/12 10.25.5.251
[...]
</pre>

##### Explanation:

- `BEGIN{FS=", "}` (can be also defined as `-F', '`)
    - Sets the field separator to `, ` (comma followed by space).
- `{for (i=1; i<=NF; i++) print $i}`
    - Loops through each field, by defining an "index" variable, leveraging the `NF` (number of fields) operator, then prints each field on a new line.

AWK is fine having its program in multiple lines, so the above can be even issued like this, to enhance readability and expandability:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -g -t -f ipv4.routes c show bond-con.2504 | awk '
BEGIN{FS=", "}
{
  for (i=1; i<=NF; i++)
  print $i
}'</span>
10.201.0.0/16 10.25.5.251
10.224.0.0/12 10.25.5.251
[...]
</pre>

##### 7.3.9.2 Advanced Example: Iterating Through a Structured IP <-> Next Hop Assignment List

The following example demonstrates how to **extract** the same IP address and Next Hop (NH) values from a structured output generated by `nmcli`, even without its built-in parameters to make the output more processable.

When running `nmcli` to display IPv4 routes for a specific connection without extra options, the result is a **structured list** where each **IP and Next Hop** pair is enclosed in curly braces `{}` and separated by commas:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -f ipv4.routes c show bond-con.2504 | tee</span>
ipv4.routes:     { ip = 10.201.0.0/16, nh = 10.25.5.251 }; { ip = 10.224.0.0/12, nh = 10.25.5.251 }; [...]
</pre>

If processed **naively**, extracting this information would be challenging due to the **nested structure and irregular spacing**.

The following AWK script **iterates through the fields** of each line, using a `for` loop to **detect and extract** `ip` and `nh` values while stripping unnecessary spaces.

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -f ipv4.routes c show bond-con.2504 | awk '
BEGIN {FS="[{}=,]"}
{
  ip = nh = ""
  for (i = 1; i <= NF; i++) {
    if ($i ~ /ip/) ip = $(i+1)
    if ($i ~ /nh/) {
      nh = $(i+1)
      gsub(/^ *| *$/, "", ip)
      gsub(/^ *| *$/, "", nh)
      print ip, nh
    }
  }
}'</span>
10.201.0.0/16 10.25.5.251
10.224.0.0/12 10.25.5.251
[...]
</pre>

##### Detailed explanation:

1. **Field Separator (`FS`) is Defined:**  
    - The `FS="[{}=,]"` defines a custom field separator, splitting the input on **curly braces (`{}`), equal signs (`=`), and commas (`,`)**.
    - This ensures that `ip = 10.201.0.0/16` and `nh = 10.25.5.251` are parsed as separate fields.
2. **Looping Through Fields:**  
    - The script initializes `ip` and `nh` as empty variables.  
    - It then iterates through all fields (`NF` = number of fields) using a `for` loop.  
    - If the field contains **"ip"**, the next field is stored in `ip`.  
    - If the field contains **"nh"**, the next field is stored in `nh`, and both values are printed.
3. **Whitespace Cleanup (`gsub`)**:  
    - `gsub(/^ *| *$/, "", ip)` removes leading/trailing spaces from `ip`.  
    - `gsub(/^ *| *$/, "", nh)` does the same for `nh`.  
    - This ensures clean output without unnecessary spaces.

### 7.4 `find`

`find` is a command-line utility used for searching files and directories in a directory hierarchy. It is highly versatile, allowing users to search based on various criteria such as name, size, type, modification time, and permissions. The `find` command is an essential tool for system administrators, as it helps locate files for backup, file permission management, log rotation, and other administrative tasks. 

In this chapter, we will explore the most common and useful ways to utilize `find` in a practical context.

#### 7.4.1 Basic Usage of `find`

At its simplest, the `find` command can be used to search for files by name, location, or type. For example, to find all files named `myfile.txt` starting from the current directory:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -name "myfile.txt"</span>
</pre>

This command searches for `myfile.txt` in the current directory and all its subdirectories. You can use wildcards (like `*` or `?`) for more flexible search patterns. For example, to find all `.log` files:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -name "*.log"</span>
</pre>

`find` also allows you to search for files of specific types, such as directories (`-type d`), regular files (`-type f`), or symbolic links (`-type l`).

#### 7.4.2 Searching by File Permissions and Types

The `find` command allows you to search files based on permissions, making it useful for security tasks. For instance, to find files with write permissions for the group, you can use:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find /path/to/directory -type f -perm -g=w</span>
</pre>

This searches for regular files (`-type f`) that have write permission for the group (`-perm -g=w`).

You can also combine file type and permissions to refine your search. For example, to find directories that are readable and executable by others:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find /path/to/directory -type d -perm -o=rx</span>
</pre>

This example searches for directories (`-type d`) that have read and execute permissions for others (`-perm -o=rx`).

#### 7.4.3 Using `-exec` to Modify Files

One powerful feature of `find` is its ability to execute commands on the files it locates, using the `-exec` option. This is useful for performing actions like modifying file permissions.

For example, to find all files in a directory and subdirectories and set their permissions to `644` (read and write for owner, read-only for group and others), you can use:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find /path/to/directory -type f -exec chmod 644 {} \;</span>
</pre>

Here, `{}` is a placeholder for the file that `find` locates, and `\;` marks the end of the command to be executed on each file. 

#### 7.4.4 Using Redirection with `find`

`find` can be used in combination with redirection to create useful outputs. For example, to list all files and then sort them, you can redirect the output to a file:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -type f -exec ls -1 {} \; | sort > /tmp/filelist_A</span>
</pre>

This command lists all files (`-type f`) and passes them to `sort`, which then writes the sorted list to `/tmp/filelist_A`.

You can also use `find` with `xargs` for more efficient processing. For example, to search for files and count the number of lines in each:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find . -type f -print0 | xargs -0 wc -l</span>
</pre>

In this case, `-print0` and `-0` ensure that filenames with spaces are handled correctly. 

Another example, to search for files and copy them to a backup directory:

<pre class="con">
<span class="psu">$</span> <span class="cmd">find /path/to/source -type f -exec cp {} /path/to/backup \;</span>
</pre>

This will copy all files from `/path/to/source` to `/path/to/backup`.

#### 7.4.5 Using `find` in Log Rotation with `crontab`

A common use case for `find` is in log rotation, where it is used to find and archive log files based on certain criteria, such as age or size. For example, an administrator may use `find` to find log files older than 7 days and compress them as part of a daily log rotation task:

```
0 0 * * * find /var/log -type f -name "*.log" -mtime +7 -exec gzip {} \;
```

This `crontab` entry runs daily at midnight (`0 0 * * *`) and uses `find` to search for log files (`*.log`) that have not been modified in the last 7 days (`-mtime +7`), then compresses them with `gzip`.


<a name="8"></a>
## 8. Environment Configuration

Environment configuration is an essential part of working with Bash, enabling users to control how the shell behaves and interacts with the system. It involves managing **global** and **local** variables, customizing settings, and efficiently applying changes. This chapter covers everything from understanding key environment variables to setting up and maintaining a personalized configuration.

### 8.1 Global Environmental Variables

Global environmental variables are system-wide settings that influence the behavior of the shell and applications. Common examples include:

- `HOST`: The hostname of the machine.
- `TZ`: The timezone setting, which determines the time displayed by the system.
- `MACHTYPE`: The type of machine or architecture (e.g., `x86_64-suse-linux`).
- `USER`: The name of the logged-in user.
- `HOME`: The user's home directory path.
- `PATH`: The directories where the system looks for executable files.
- `PS1`: The primary shell prompt format, which can be customized for better visibility or information.
- `SHELL`: The current shell's type.
- `LANG`: The system's language.
- `EDITOR`: The default text editor.

These variables are typically defined in system-wide configuration files like `/etc/profile` or `/etc/environment` and are inherited by all user sessions.

You can show all the current environmental variables by entering either `env` or `set` commands.

### 8.2 Local Variables, Effective for Current Session

Local variables are specific to the current session or script and do not persist across new sessions. These are set using a simple assignment syntax:

```
VARIABLE_NAME="value"
```

Unlike global variables, local variables are not inherited by child processes. To make a local variable available to child processes, you must export it:

```
export VARIABLE_NAME="value"
```

Using local variables is ideal for temporary configurations, testing, or scripting without affecting the global environment.

### 8.3 Customizing Your Environment Setup

Personalizing your Bash environment allows for improved efficiency and a tailored experience. Common customizations include:

- Modifying the `PS1` prompt for better readability and context.
- Extending the `PATH` variable to include directories for custom scripts:  
```
export PATH="$PATH:/custom/directory"
```
- Defining aliases for frequently used commands:  
```
alias ll="ls -l --color=auto"
```
- Setting environment-specific variables, such as `EDITOR` or `LANG`, to suit your workflow.

These customizations can be added to user-specific files like `~/.bashrc` or `~/.bash_profile`, ensuring they persist across sessions.

When managing environment configurations, it is crucial to avoid unintended consequences. Overwriting critical variables like `PATH` without appending the original value can break your system. For example, instead of this:

```
export PATH="/custom/directory"
```

Always append the original value:

```
export PATH="$PATH:/custom/directory"
```

Additionally, ensure scripts and configuration files do not have syntax errors, as they may prevent your shell from starting properly. Testing changes in a separate terminal or session before applying them system-wide can help prevent disruptions.

Here's an example for setting up a `PS1` automatically by opening any new sessions (or when "sourced": see below).  
It uses... 
- different text colors for the username, the short hostname, the current working directory
- it uses two rows, so the second row is the actual prompt where the user can type
- it even deals with current Git repository if it "finds itself" in such directory. 

This example `PS1` setup is stored in the user's `~/.bashrc`:

```
# Define colors for the prompt
# RED="\[\033[0;31m\]"   # not used here, but good to have
GREEN="\[\033[0;32m\]"
# BLUE="\[\033[0;34m\]"  # not used here, but good to have
YELLOW="\[\033[0;33m\]"
MAGENTA="\[\033[0;35m\]"
CYAN="\[\033[0;36m\]"
PURPLE="\[\033[1;35m\]"
RESET="\[\033[0m\]"

# Function to show the current Git branch
git_branch() {
  branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
  [[ -n "$branch" ]] && echo "(${branch})"
}

# Build the PS1
PS1="\n${GREEN}\u${RESET}@${CYAN}\h${RESET}:${YELLOW}\w${RESET} ${MAGENTA}\$(git_branch)${RESET}\n${RESET}${PURPLE}\$${RESET} "
```

### 8.4 Using `source` and `.`

The `source` command (or its shorthand, `.`) is used to apply changes from a script or configuration file to the current shell session without starting a new one. For example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">source ~/.bashrc</span>
</pre>

This reloads your `.bashrc` file, applying any recent changes without requiring you to log out and back in. It is especially useful when testing new configurations or applying updates to environment variables.


<a name="9"></a>
## 9. Scripting Practicalities

Shell scripting is not just about writing code — it’s about making your scripts efficient, user-friendly, and flexible.

This chapter covers some additional and final tips, tricks, and examples that are essential for everyday shell programming. Whether you are automating tasks, processing data, or managing system configurations, these tips will help you write better, more efficient scripts. From error handling and file manipulation to creating user-friendly interactive scripts, hopefully you will find actionable knowledge here.

### 9.1 Useful Shell One-Liners

Shell one-liners are compact, yet powerful commands that can help automate tasks quickly. They are useful for system administrators, developers, and anyone who needs quick solutions. Here's an example of how to capture the output of a `ping` command, process it, and repeatedly execute the command with a time delay:

<pre class="con">
<span class="psu">$</span> <span class="cmd">while true ; do echo "`date`: `ping -c 2 myserver.example.com 2>/dev/null | grep transmit`" ; sleep 10 ; done</span>
Thu Dec 14 12:14:04 EET 2023: 2 packets transmitted, 2 received, 0% packet loss, time 5031ms
Thu Dec 14 12:14:19 EET 2023: 2 packets transmitted, 2 received, 0% packet loss, time 5036ms
^C
</pre>

This continuously pings a server and outputs the date with packet transmission statistics. The error output (`2>/dev/null`) ensures that the `ping` command does not display unnecessary information.

### 9.2 General Bash Tips and Tricks

#### 9.2.1 Re-run a command using part of its history

A common need in the shell is to re-run the last command that included a specific keyword. This is done using `!` followed by a part of the command:

<pre class="con">
<span class="psu">$</span> <span class="cmd">!?source</span>
source gjakab/venvs/experiment/bin/activate
</pre>

#### 9.2.2 String Manipulation: Trimming and Removing Whitespaces

String manipulation is essential in many shell scripts. For trimming or shortening strings, you can use shell parameter expansion:

<pre class="con">
<span class="psu">$</span> <span class="cmd">var1="Árvíztűrő tükörfúrógép"</span>

<span class="psu">$</span> <span class="cmd">var2=${var1::-4}</span>

<span class="psu">$</span> <span class="cmd">echo $var2</span>
Árvíztűrő tükörfúr
</pre>

This removes the last four characters from the string, trimming it to a desired length.

To remove leading whitespaces from a string, you can use one of two methods:

<pre class="con">
<span class="psu">$</span> <span class="cmd">var1="                Árvíztűrő tükörfúrógép"</span>

<span class="psu">$</span> <span class="cmd">echo "something: ${var1##*( )}"</span>
something: Árvíztűrő tükörfúrógép

<span class="psu">$</span> <span class="cmd">echo "this: $var1" | xargs</span>
this: Árvíztűrő tükörfúrógép
</pre>

The first method uses shell parameter expansion to strip leading spaces, while the second method uses `xargs` to trim whitespaces, making it a versatile option.

### 9.3 Sorting Data Using `sort`

Sorting is a frequent task in shell scripting, whether it’s sorting logs, IP addresses, or any other data. Here are some practical examples:

#### 9.3.1 Sorting by numerical value in a specific column

To sort a list of environment variables based on the third column:

<pre class="con">
<span class="psu">$</span> <span class="cmd">xy_envs 2>/dev/null | grep training | sort -t '-' -n -k3</span>
940991  connect-training-2                    Xms31G      Xmx31G
986399  connect-training-4                    Xms31G      Xmx31G
937300  connect-training-6                    Xms31G      Xmx31G
992254  connect-training-14                   Xms31G      Xmx31G
970705  connect-training-15                   Xms31G      Xmx31G
</pre>

#### 9.3.2 Sorting IP addresses by their octets

Sorting IP addresses in ascending order is done by specifying multiple keys for the octets:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat ip_list.txt | sort -t '.' -n -k 1,1 -k 2,2 -k 3,3 -k 4,4</span>
10.10.5.251
10.10.5.252
10.13.4.8
10.13.4.16
10.13.4.17
[...]
</pre>

This sorts the IPs based on each octet, from left to right.

### 9.4 Making Your Shell Program User-Friendly

Shell scripts that interact with users should be clear and easy to use. Here are some techniques for improving the usability of your scripts.

#### 9.4.1 Going Back One Line

Sometimes, it's necessary to overwrite the previous output line to update the user on the script’s progress:

```
[...]
echo "Cleaning up..."
rm -f $FILENAME
echo -e "\e[1ACleaning up... done"
```

This example overwrites the line "Cleaning up..." with "Cleaning up... done" to create a cleaner, more professional output.

#### 9.4.2 Advanced Parameter List with `getopts`

For scripts that need user inputs for configuration, using `getopts` for command-line argument parsing is a great option:

```
while getopts s:c:w: PARAM
do
  case "${PARAM}" in
    s) BLOCK_SIZE=${OPTARG};;
    c) BLOCK_COUNT=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    *) echo "Usage: $0 [-s block-size] [-c block-count] [-w workdir]" && exit 1
  esac
done
```

This code snippet parses command-line options `-s`, `-c`, and `-w` to set the values of `BLOCK_SIZE`, `BLOCK_COUNT`, and `WORKDIR` in your script.

### 9.5 Background Jobs and Job Control

In shell scripting, background jobs allow you to run multiple tasks concurrently. The `&` operator is used to run a process in the background:

<pre class="con">
<span class="psu">$</span> <span class="cmd">my_script.sh &</span>
</pre>

You can also bring a background job to the foreground using `fg`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">fg</span>
</pre>

Job control is useful when you need to run multiple processes simultaneously and manage them efficiently.

### 9.6 Handling Errors Gracefully

Error handling is a crucial aspect of writing reliable shell scripts. The `trap` command is often used to clean up or take specific actions when a script exits or encounters an error:

<pre class="con">
<span class="psu">$</span> <span class="cmd">trap 'echo "Cleaning up..."; rm -f $TEMP_FILE' EXIT</span>
</pre>

This ensures that a temporary file is deleted when the script exits.

### 9.7 Using Shell Functions for Code Reusability

Shell functions allow you to reuse code and make your scripts more modular. Here’s a simple example of using a function to calculate disk usage:

```
disk_usage() {
  df -h | grep "^/dev"
}
```

This function outputs disk usage information for mounted filesystems, which can be invoked anywhere in the script.

---

Shell scripting offers countless ways to improve productivity and automate tasks. By following these practicalities and employing these techniques, you can create more efficient, reliable, and user-friendly shell scripts.
