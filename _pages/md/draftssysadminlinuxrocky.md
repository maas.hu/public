# ![alt text](/_ui/img/icons/rocky_m.svg) Rocky Linux

<div style="text-align: right;"><h7>Posted: 2021-10-25 17:41</h7></div>

###### .

>Under development...

## Tartalomjegyzék

1. [Introduction](#1)
2. [Installation, Re-installation](#2)
3. [Environment Configuration Basics](#3)

<a name="1"></a>
## 1. Introduction

...

<a name="2"></a>
## 2. Installation, Re-installation

The below is a rough demonstration of any Red Hat or its forks (Fedora, CentOS, Rocky, Alma etc.), using their Anaconda installer. In this case, it's installed on a new empty disk (under VirtualBox) but it can be used also for overwriting an existing systems. 

### 2.1 Downloading the Install Media

We can download the classic DVD installer, bootdisk only or a "minimal" version from [rockylinux.org/download](https://rockylinux.org/download). In the below example, we work with the "minimal" version, but the finished system can have anything we additionally might need, separately. 

It worth comparing one of the Checksums too once the image is downloaded. 

### 2.2 Preparing a bootable pendrive

Windows alatt a [Rufust](https://rufus.ie), Linux alatt egyszerűen a `dd` parancsot tudom ajánlani bootolható pendrive készítéséhez. Ez utóbbinak ismertetése az [általános linuxos témakörök](/learning/it/sysadmin/linux/general/) kategóriában található. 

Ezután már indíthatjuk is újra a laptopunkat/PC-nket, az első másodpercekben nyomkodjuk a boot menüjét előhozó billentyűt (pl. F12-t), majd indulhat is a telepítés. 

### 2.3 The installation

Once the installer image booted up, choose the **Install Rocky Linux <version>** option in GRUB's menu: 

![alt_text](rl9-ins-01.png)

...

![alt_text](rl9-ins-02.png)

...

![alt_text](rl9-ins-03.png)

...

![alt_text](rl9-ins-04.png)

...

![alt_text](rl9-ins-05.png)

...

![alt_text](rl9-ins-06.png)

...

![alt_text](rl9-ins-07.png)

...

![alt_text](rl9-ins-08.png)

...

![alt_text](rl9-ins-09.png)

...

![alt_text](rl9-ins-10.png)

...

![alt_text](rl9-ins-11.png)

...

![alt_text](rl9-ins-12.png)

...

![alt_text](rl9-ins-13.png)

...

![alt_text](rl9-ins-14.png)

...

![alt_text](rl9-ins-15.png)

...

![alt_text](rl9-ins-16.png)

...

![alt_text](rl9-ins-17.png)

...

![alt_text](rl9-ins-18.png)

...

![alt_text](rl9-ins-19.png)

...

![alt_text](rl9-ins-20.png)

...

![alt_text](rl9-ins-21.png)

...

![alt_text](rl9-ins-22.png)

...

### Notes upon fresh "Minimal Install", and first `dnf update`

Packages to have:

```
# dnf install -y \
    bind-utils \
    bzip2 \
    git-core \
    nmap-ncat \
    tar \
    unzip \
    wget
```

General first time preparation in case you want to save your VM's disk image as a "template" for further ones:

```ksh
# dnf clean all
0 files removed

# dnf check-update
Rocky Linux 9 - BaseOS
[...]

# dnf update -y
[...]

# dnf install git-core bzip2 tar unzip bind-utils nmap-ncat
[...]
Is this ok [y/N]: y
[...]

# cd /var/log

# for i in $(find . -name '*log') ; do cat /dev/null > $i ; done

# >messages

# >wtmp

# shutdown -h now
```

Install the EPEL (Extra Packages for Enterprise Linux) repository if necessary: 

```ksh
# dnf install epel-release
[...]
```


<a name="3"></a>
## 3. Environment Configuration Basics

...

```ksh
$ localectl list-keymaps
ANSI-dvorak
al
al-plisi
amiga-de
amiga-us
[...]

$ localectl status
VC Keymap: us

# localectl set-keymap fi-nodeadkeys
```
