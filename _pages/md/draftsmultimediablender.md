# ![alt text](/_ui/img/icons/blender_m.svg) Blender

<div style="text-align: right;"><h7>Posted: 2023-10-15 22:55</h7></div>

###### .

## Table of Contents

1. [Introduction](#1)
2. [The User Interface](#2)
3. [Basic Operations on Meshes](#3)
4. [Curves](#4)
5. [Complex Operations on Meshes](#5)
6. [...](#6)


<a name="1"></a>
## 1. Introduction

![alt_text](blender_-_default_ui_with_monkey.png)

Blender is a free and open-source 3D computer graphics software tool set used for creating 3D-printed models, animated films, visual effects, art, motion graphics, video editing, interactive 3D applications, virtual reality, and even video games.

It's actively developed and improved by a huge community. The latest version can be always downloaded from [blender.org](https://www.blender.org/download/), but even all the most popular Linux distributions' general repositories offer the tool in a single package format. 


<a name="2"></a>
## 2. The User Interface

...

### 2.1 Viewpoints
By default, you get a **cube** object already already, in the center of your default **Viewpoint**, called as **User Perspective**.  
Apart from accessing them in View → Viewpoint menu, you can switch between different Viewpoints by your numpad: 
- `1` is **Front** Orthographic, so you can grab (`G`) and reposition your object easily in **X and Z locations**
- `3` is **Right** Ortographic, so you can do the same in **Y and Z locations**
- `7` is **Top** Ortographic, so you can do the same in **X and Y locations**

Grabbing the Viewpoint by holding the **middle mouse button** brings your Viewpoint back to a new User Perspective immediately.

### 2.2 The Modes

Your default Mode is **Object Mode** where you can manipulate **whole objects**. The other important mode is **Edit Mode**, where you can select and manipulate (e.g. grab and move) **vertices, edges and faces**. 

...

### 2.3 General Key Commands

There are even much more than this list, but using and practicing at least the following ones is essential when working with Blender: 

- **scroll up and down**: zoom in and out
- **drag by middle mouse button (MMB)**: switching to a user perspective to look around the object in the center
- `Shift` **+ drag by MMB**: moving the view (holding by the little hand icon does the same but it's much quicker)
- to set the **Camera** for your current **User Perspective**, press `[Ctrl+Alt+0]`.

### 2.4 Scene Collections

Blender names our objects automatically, but you can quickly end up having dozens of them. Apart from just easily renaming them, you can also organize them in **Collections**. This panel is located in the top right corner by default. 

Here's an example when objects were organized as such, then used the collection for a boolean operation, to be done not only against a single object only but a set of them, within the same collection: 

![alt_text](blender_-_scene_collections.png)

### 2.5 Custom UI




<a name="3"></a>
## 3. Basic Operations on any Objects

...

### 3.1 Adding a new Mesh

Usually, you work with meshes. If you want to work with more detailed object from the beginning, with higher number of verticles, then add your initial mesh with using the `[Shift+A]` shortcut. This gives you the possibility to set the initial parameters of the new mesh instead of using the factory default ones, e.g.:

![alt_text](blender_-_parameters_for_new_mesh.png)

### 3.2 Moving, Scaling and Rotating

The most essential operations are these

- `G`: **grab and move object** by viewpoint. Press `X`, `Y` or `Z` to limit the movement only to one direction and hold `Ctrl` to precise transform operation. You can see the transformation's value in the top left corner.
- `S`: **scale object** by viewpoint. Similarly to grabbing, you can limit the transformation's direction and precisity.
- `R`: **rotate object** by viewpoint. Similarly to grabbing, you can limit the transformation's direction and precisity.

#### 3.2.1 Real size of objects

Knowing the real size of a mesh or, generally speaking, any objects in Blender, is essential e.g. when you want to 3D print your art. 



#### 3.2.2 The Measure tool

Apart from using the grid, the `Ctrl` key and checking the top left corner constantly for fine positioning, sometimes the **Measure tool** can be very useful too. Example for using it as a protractor: 

![alt_text](blender_-_measure.png)

### 3.3 Object Properties

If you already have several, similar objects, then even though you selected them all, an object property change is valid only for the first selected one. To get it effective for all the others too, you can click with `RMB` and choose the **Copy to Selected** function from the menu, so all the selected objects will get that particular parameter changed:

![alt_text](blender_-_copy_to_selected.png)

### 3.4 Joining Meshes

Even though the boolean operation, see next subchapter, the Union might sound the obvious solution for merging two objects into one, that one does not always give a result and takes incredible computing power sometimes. The simple solution for it is the so called **Join** operation: **Object** menu --> **Join** or `[Ctrl+J]`

It's always good to ensure the following conditionals match: 
- at least two objects must be selected
- both objects are meshes or converted to meshes
- you're OK with deleting all the selected objects but the selected one that remains

E.g. here, the `Cube.001` object was selected secondary, so that's the active object, and that will stay upon join operation: 

![alt_text](blender_-_join.png)

### 3.5 Boolean Modifiers

These are very handy tools, and if you've worked with any vector graphics software before, you will know the concept instantly. If you're patient enough, almost any shapes can be created using boolean modifiers only.

First, you need to prepare your **primary object** that you want to keep but be modified by either...

- **cutting** the secondary object from it (which means using the **Difference** Boolean modifier)
- **merging** the secondary object with it into one (**Union**)
- keeping the only **intersection** that's common with the secondary object (**Intersect**)

Once the secondary object is also prepared...
1. select the primary one, and go to **Modifiers** → **Add Modifier** → **Boolean**
2. select either **Intersect**, **Union** or **Difference** (this last one is the most frequently used one anyway)
3. select the secondary object for the **Object** field
4. when you're satisfied with the plan of your modifier setup, you can apply it. The function is a little bit hidden: it's in the menu of the Modifier's menu, or you can just use `[Ctrl+A]`: 

![alt_text](blender_-_modifier_-_apply.png)

<div class="info">
<p><b>Before applying</b>, it's especially useful if you switch your secondary object as a wireframed one only, so you will see the edge creation process while you are moving your objects around. It can be set in the <b>Object</b> tab → <b>Viewport Display</b>, where you just need to enable <b>Wireframe</b> and set the <b>Display As</b> to <b>Wire</b>.
</div>

![alt_text](blender_-_modifier_-_secondary_as_wire.png)

You can read more on Boolean Modifiers at the official [Blender Documentation](https://docs.blender.org/manual/en/latest/modeling/modifiers/generate/booleans.html) site.

### 3.6 Filling

Press `[Alt+F]` for filling an empty hole with a new face/geometry like this: 

![alt_text](blender_-_fill.png)

### 3.7 Smooth Shading

It doesn't work for any meshes, especially when we created our own ones with low number of verticles. 

In Object Mode, select the object, then click with your right mouse button to get the Object Context Menu. Select Shade Auto Smooth from the menu, so it will "look like" smooth (but the object is actually still not smooth).

The shortcuts between `[Ctrl+1]` and `[Ctrl+5]` instantly sets the **Subdivision** modifier with the **Level of Viewport** between 1 and 5.

### 3.8 Material

By default, the objects get a grayish color and no material. If you want to have some colors for it, you need to add **Materials**. 


<a name="4"></a>
## 4. Curves

In object mode, go to **Add** → **Bezier** (for open curve) or **Circle** (for closed curve).

Let's stick to **Bezier Curve** for now. By default, it's a laying object with two control points only. You can add more by selecting any of them, then press `E` (**Extrude**). 

### 4.1 Converting a Curve to a 3D Object

1. Go to **Data** tab and make sure it's a **3D** under the **Shape** section, so you can rotate the control points by any locations in **Edit Mode**. 
![alt_text](blender_-_curves_-_convert_to_3d_-_1.png)
2. Under the **Geometry** section, increase the **Extrude** value to some small values, so it becomes a stripe.  
![alt_text](blender_-_curves_-_convert_to_3d_-_2.png)
3. Under the **Bevel** subsection of **Geometry**, increase the **Depth** value, so it becomes something like a pipe.  
![alt_text](blender_-_curves_-_convert_to_3d_-_3.png)
4. The **Fill Caps** ensures the object has its beginning and ending sections filled.  

You can also convert this new object into a mesh in **Object Mode** in **Object** menu → **Convert** → **Mesh**. The generated mesh is, however, sometimes gets too high resolution to work comfortable afterwards, so maybe it's a good idea to lower the **Resolution** of the **Bevel** first from the default `4` to e.g. `2`. 

...


<a name="5"></a>
## 5. Complex Operations on Meshes

Of course, this chapter never can be final, but we will go through on some extra operations that might be useful for you.

### 5.1 Cutting Meshes

It's often necessary to cut even a compound mesh into two separate ones. Let's demonstrate it through a simple cube mesh: 

1. Select the object in **Object Mode**, go to **Edit Mode**, then select the **Bisect** tool. Click and hold on **Knife** tool to access it:  
![alt_text](blender_-_bisect_-_01.png)
2. Draw the line where you want to split:  
![alt_text](blender_-_bisect_-_02.png)
3. Press `[V]` to clarify the splitting direction, then right click or `[Esc]` when done:  
![alt_text](blender_-_bisect_-_03.png)
4. Choose the Select Loop Inner-Region function under Select --> Select Loops menu:  
![alt_text](blender_-_bisect_-_04.png)
5. Press `[P]` as a final step for separation:  
![alt_text](blender_-_bisect_-_05.png)
6. Going back to **Object Mode**, you can simply grab (`[G]`) and move the new mesh:  
![alt_text](blender_-_bisect_-_06.png)
7. Now that both of the new meshes have an unfilled face, select one of them, go to **Edit Mode**, and click on one of the edges while holding `[Alt]`, so all the edges in the same face will be seelected:  
![alt_text](blender_-_bisect_-_07.png)
8. Press `[F]` to fill the face:  
![alt_text](blender_-_bisect_-_08.png)


<a name="6"></a>
## 6. ...
