# ![alt text](/_ui/img/icons/babytux_m.svg) Linux Storage

<div style="text-align: right;"><h7>Posted: 2021-10-25 17:41</h7></div>

###### .

![alt_text](linux_-_storage_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Partitions](#2)
3. [Filesystems](#3)
4. [LVM](#4)
5. [RAID](#5)
6. [Disk Performance Testing and Tuning](#6)
7. [LUKS Encryption](#7)


<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Partitions

...

The `gparted` tool is also possible to use if you prefer doing everything at once.

...


<a name="3"></a>
## 3. Filesystems

...


<a name="4"></a>
## 4. LVM

...

### 4.1 LVM and RAID support

LVM can be also considered as **a software RAID solution** with three possible RAID configurations:

- **RAID 0**, Striping: when the PV spreads over on multiple partitions/disks
- **RAID 1**, Mirroring (up to 5 copies in case of LVM2): when the same data block is copied over to multiple disks
- **RAID 10** or 1 + 0, Striping + Mirroring

See next chapter for more details about RAID levels.


<a name="5"></a>
## 5. RAID

It's a huge topic but we will go through on most commonly known solutions (at least the ones that I treat as such). 

### 5.1 Standard RAID levels

Trying not to just repeat the [Wikipedia](https://en.wikipedia.org/wiki/Standard_RAID_levels) that explains it very vell, in summary, these are the ones that have at least a common sense:

- **RAID 0** (*stripes*): when the data is across two or more disks but without parity information, redundancy or fault tolerance.
  - increased performance due to parallel disk access
  - loosing any of the disks causes total disaster
- **RAID 1** (*mirror*): it is when the data blocks are simply copied over to other disk(s) too
  - data remains intact if that's still accessible on at least one of the disks involved
  - overall write performance is equal to the slowest disk's speed
- **RAID 2** (*error correction*): also striping but on bit level instead of blocks with combining it *"Hamming code"* for error correction
  - very rarely implemented in practice
- **RAID 3** (*byte-level striping with dedicated parity disk*):
  - cannot service multiple requests simultaneously
  - very rarely implemented in practice
- **RAID 4** (*block-level striping with dedicated parity disk*):
  - it can be quickly extended online
  - good performance of random reads but slow writes due to all parity data goes to the same disk
- **RAID 5** (*block-level striping with distributed parity*): it creates array parity information so that the data can be reconstructed if a disk in the array fails. Provides better capacity than RAID level 10 but possibly lower performance
- **RAID 6** (*RAID 5 ("P") plus "Q" parity*): better data redundancy than RAID 5 but with slightly lower capacity and possibly lower performance. Provides better capacity than RAID level 10 but still possibly lower performance
- **RAID 10** (*striping of mirrors*): also referred as RAID 01; it's the combination of RAID 0 and RAID 1. Provides generally better performance than RAID 5 or 6, but has lower capacity.

### 5.2 Software RAID

On Linux, it's mostly done via the `mdadm` utility. The utility supports various RAID levels:

- RAID 0
- RAID 1
- RAID 4
- RAID 5
- RAID 6 (however, there is a known risk of data loss with md/raid6 arrays running on Linux since 2.6.32 in some specific conditions)
- RAID 10

...


### 5.3 Hardware RAID

If you manage an enterprise system and looking for any RAID Controllers that the system use, it is possible either by `lsscsi` as a summary or by `lshw` with the most important details. 

The below example shows a Dell PowerEdge system's setup, which has both **PERC** (Dell PowerEdge RAID Controller) and **BOSS** (Boot Optimized Storage Solution): 

```ksh
$ lsscsi
[0:2:0:0]    disk    DELL     PERC H745 Frnt   5.16  /dev/sdb 
[1:0:0:0]    disk    ATA      DELLBOSS VD      00-0  /dev/sda 
[3:0:0:0]    process Marvell  Console          1.01  -        

$ sudo lshw -class disk
  *-disk                    
       description: SCSI Disk
       product: PERC H745 Frnt
       vendor: DELL
       physical id: 2.0.0
       bus info: scsi@0:2.0.0
       logical name: /dev/sdb
       version: 5.16
       serial: 00f76c9ea9a1c3742d002b531d20a7c2
       size: 15TiB (16TB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=683ad01e-7642-0c49-8d7a-e94fd00f1576 logicalsectorsize=512 sectorsize=512
  *-disk
       description: ATA Disk
       product: DELLBOSS VD
       physical id: 0
       bus info: scsi@1:0.0.0
       logical name: /dev/sda
       version: 00-0
       serial: 1be5306b4b6e0010
       size: 447GiB (480GB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=04ab02c8-f203-42b8-8ceb-1e2795c23f35 logicalsectorsize=512 sectorsize=4096
```

Query for kernel module as RAID Controller driver and its version:

```ksh
$ lsmod | grep raid
megaraid_sas          176128  1

$ modinfo megaraid_sas | head -6
filename:       /lib/modules/4.18.0-513.11.1.el8_9.x86_64/kernel/drivers/scsi/megaraid/megaraid_sas.ko.xz
description:    Broadcom MegaRAID SAS Driver
author:         megaraidlinux.pdl@broadcom.com
version:        07.725.01.00-rc1
license:        GPL
rhelversion:    8.9
```

...or...

```ksh
$ dmesg | grep sas
[    7.264085] megasas: 07.725.01.00-rc1
[    7.264987] megaraid_sas 0000:01:00.0: BAR:0x0  BAR's base_addr(phys):0x00000000a6b00000  mapped virt_addr:0x00000000feb0085d
[    7.264997] megaraid_sas 0000:01:00.0: FW now in Ready state
[...]
```

...


<a name="6"></a>
## 6. Disk Performance Testing and Tuning

There are different terms when measuring the performance and capabilities of a disk device. 

The most common one is measuring **throughput**, which is about measuring **the capacity of the system or network to handle data processing**. In simpler terms, throughput is **the number of transactions a system can handle in a given time period**.

### 6.1 Simple disk I/O Tests with `dd`

...

```ksh
# dd if=/dev/zero of=testfile bs=1G count=100 oflag=dsync
100+0 records in
100+0 records out
107374182400 bytes (107 GB, 100 GiB) copied, 106.462 s, 1.0 GB/s
```


#### SD Card Tests

- "pendrive" style USB 3.0 adapter:  
<pre>
# dd if=/dev/zero of=/run/media/kmihaly/SanDisk128G/testfile bs=1G count=10 oflag=dsync
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 243.385 s, 44.1 MB/s
</pre>
- Ugreen 2-in-1 USB 3.0 adapter:  
<pre>
# dd if=/dev/zero of=/run/media/kmihaly/SanDisk128G/testfile bs=1G count=10 oflag=dsync
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 243.96 s, 44.0 MB/s
</pre>

##### Kodak UHS-I U3 V30 A1, 256 GB

- Factory default formatting (VFAT) with Ugreen:
<pre>
# dd if=/dev/zero of=/run/media/kmihaly/0123-4567/testfile bs=1G count=10 oflag=dsync
[...] ~ 30 MB/s [...]
</pre>
- Re-formatted as NTFS:
<pre>
# mkfs.ntfs -L Kodak256G -Q /dev/sdb1
Cluster size has been automatically set to 4096 bytes.
Creating NTFS volume structures.
mkntfs completed successfully. Have a nice day.

# dd if=/dev/zero of=/run/media/kmihaly/0123-4567/testfile bs=1G count=10 oflag=dsync
[...] ~ 30 MB/s [...]
</pre>
- Stress test with 10 * 10 GB files (so the further video files transferred over must be also in sync):
<pre>
# for i in {01..10} ; do echo $i ; dd if=/dev/zero of=/run/media/kmihaly/Kodak256G/testfile-${i} bs=1G count=10 oflag=dsync ; sleep 60 ; done
01
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 150.987 s, 71.1 MB/s
02
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 151.547 s, 70.9 MB/s
03
[...]
</pre>









### 6.2 Disk I/O Tests with `fio`

`fio` stands for **Flexible I/O Tester** and is a tool to measure and visualize the I/O performance of storage devices on Linux systems. It can simulate various I/O workloads to test the performance of hard drives, SSDs, and other storage devices.

It worth going through on its `man` page, which can be accessed also e.g. [here](https://linux.die.net/man/1/fio).

#### 6.2.1 Basic tests with `fio`

As suggested by [this article](https://webhostinggeeks.com/howto/use-fio-measure-speed-data-reads-writes-linux/), this can be considered as the "basic test" with `fio`: 

```ksh
# fio --name=test --rw=randwrite --ioengine=sync --bs=4k --numjobs=1 --size=1G --runtime=10m --time_based
[...]
```

Some explanation on the parameters used: 

- `--name=<name_of_test>`: This names the job "test".
- `--rw=<I/O_pattern>`: This sets the I/O pattern to random writes. Other options:
    - `read`: Sequential reads.
    - `write`: Sequential writes.
    - `randread`: Random reads.
    - `randwrite`: Random writes.
    - `rw` or `readwrite`: Mixed sequential reads and writes.
    - `randrw`: Mixed random reads and writes.
- `--ioengine=<ioengine>`: This specifies the I/O engine to be used. Explanation of most common engines:
    - `sync`: Basic read or write I/O. fseek is used to position the I/O location.
    - `libaio`: Linux native asynchronous I/O. (Looks like the most common one.)
- `--bs=<blocksize>`: This sets the block size to 4kB. (4kB is the default but might differ on some exotic systems, so it's better to define even 4k).
- `--numjobs=<number_of_jobs_per_thread>`: This runs one job/thread.
- `--size=<jobsize>`: This sets the total size of I/O for the job to 1GB.
- `--runtime=<runtime>`: This limits the job to run for 10 minutes. The default scale is seconds, but it also supports `m` (minutes)
- `--time_based`: This makes the job run for the specified time, regardless of the amount of I/O.

A non time-based simple write test with `fio`:

```ksh
# fio --name=test --rw=write --ioengine=libaio --bs=1G --numjobs=1 --size=100G --sync=1
[...]
```

Red Hat Support suggests this `fio` command as time-based equivalent of `dd`'s `dd if=/dev/zero of=testfile bs=1G count=100 oflag=dsync` (mentioned above in this article). This time, let's see its full output: 

```ksh
# fio --name=throughput-test-job1 --rw=randrw --ioengine=sync --iodepth=64 --bs=64k --size=100M --numjobs=4 --fdatasync=1 --runtime=120 --time_based --group_reporting
throughput-test-job1: (g=0): rw=randrw, bs=(R) 64.0KiB-64.0KiB, (W) 64.0KiB-64.0KiB, (T) 64.0KiB-64.0KiB, ioengine=sync, iodepth=64
...
fio-3.19
Starting 4 processes
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
Jobs: 4 (f=4): [m(4)][100.0%][r=935MiB/s,w=927MiB/s][r=14.0k,w=14.8k IOPS][eta 00m:00s] 
throughput-test-job1: (groupid=0, jobs=4): err= 0: pid=3564162: Wed May 29 18:50:27 2024
  read: IOPS=16.1k, BW=1005MiB/s (1054MB/s)(118GiB/120001msec)
    clat (usec): min=61, max=11397, avg=92.45, stdev=53.94
     lat (usec): min=61, max=11397, avg=92.52, stdev=53.95
    clat percentiles (usec):
     |  1.00th=[   69],  5.00th=[   73], 10.00th=[   74], 20.00th=[   77],
     | 30.00th=[   80], 40.00th=[   83], 50.00th=[   86], 60.00th=[   90],
     | 70.00th=[   96], 80.00th=[  104], 90.00th=[  119], 95.00th=[  135],
     | 99.00th=[  172], 99.50th=[  192], 99.90th=[  318], 99.95th=[  449],
     | 99.99th=[  930]
   bw (  KiB/s): min=702519, max=1253921, per=100.00%, avg=1030187.22, stdev=31843.47, samples=956
   iops        : min=10975, max=19592, avg=16095.29, stdev=497.68, samples=956
  write: IOPS=16.1k, BW=1005MiB/s (1054MB/s)(118GiB/120001msec); 0 zone resets
    clat (usec): min=30, max=5032, avg=43.62, stdev=18.24
     lat (usec): min=30, max=5033, avg=44.42, stdev=18.63
    clat percentiles (usec):
     |  1.00th=[   34],  5.00th=[   35], 10.00th=[   36], 20.00th=[   37],
     | 30.00th=[   38], 40.00th=[   39], 50.00th=[   41], 60.00th=[   42],
     | 70.00th=[   44], 80.00th=[   49], 90.00th=[   57], 95.00th=[   63],
     | 99.00th=[   80], 99.50th=[   91], 99.90th=[  212], 99.95th=[  265],
     | 99.99th=[  553]
   bw (  KiB/s): min=694902, max=1239468, per=100.00%, avg=1030151.76, stdev=32044.64, samples=956
   iops        : min=10856, max=19366, avg=16094.68, stdev=500.81, samples=956
  lat (usec)   : 50=41.39%, 100=46.50%, 250=11.97%, 500=0.12%, 750=0.02%
  lat (usec)   : 1000=0.01%
  lat (msec)   : 2=0.01%, 4=0.01%, 10=0.01%, 20=0.01%
  fsync/fdatasync/sync_file_range:
    sync (nsec): min=1062, max=32316k, avg=39035.23, stdev=58659.33
    sync percentiles (nsec):
     |  1.00th=[  1128],  5.00th=[  1160], 10.00th=[  1176], 20.00th=[  1208],
     | 30.00th=[  1288], 40.00th=[  1496], 50.00th=[ 55552], 60.00th=[ 66048],
     | 70.00th=[ 70144], 80.00th=[ 75264], 90.00th=[ 83456], 95.00th=[ 91648],
     | 99.00th=[109056], 99.50th=[119296], 99.90th=[195584], 99.95th=[261120],
     | 99.99th=[473088]
  cpu          : usr=2.18%, sys=57.67%, ctx=4287471, majf=0, minf=422
  IO depths    : 1=200.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=1929133,1929115,0,0 short=3858242,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=64

Run status group 0 (all jobs):
   READ: bw=1005MiB/s (1054MB/s), 1005MiB/s-1005MiB/s (1054MB/s-1054MB/s), io=118GiB (126GB), run=120001-120001msec
  WRITE: bw=1005MiB/s (1054MB/s), 1005MiB/s-1005MiB/s (1054MB/s-1054MB/s), io=118GiB (126GB), run=120001-120001msec

Disk stats (read/write):
    dm-3: ios=1929034/1931269, merge=0/0, ticks=82783/87736, in_queue=170519, util=100.00%, aggrios=1929133/1930406, aggrmerge=0/951, aggrticks=86989/105076, aggrin_queue=192065, aggrutil=100.00%
  sdb: ios=1929133/1930406, merge=0/951, ticks=86989/105076, in_queue=192065, util=100.00%
```

The next sets of tests are all based on [docs.oracle.com suggestions](https://docs.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm), but their article mentions some parameters that do not exist anymore.

Additional parameters that we will also use below: 

- `iodepth=int`: Number of I/O units to keep in flight against the file. Note that increasing `iodepth` beyond `1` will not affect synchronous ioengines (except for small degress when `verify_async` is in use). Even async engines my impose OS restrictions causing the desired depth not to be achieved. This may happen on Linux when using `libaio` and not setting `direct=1`, since buffered IO is not async on that OS. Keep an eye on the IO depth distribution in the fio output to verify that the achieved depth is as expected. Default: `1`.
- `direct=bool`: If true, use non-buffered I/O (usually `O_DIRECT`). Default: false (`0`).
- `group_reporting`: If set, display per-group reports instead of per-job when `numjobs` is specified.

<div class="warn">
<p>Do not run <code>fio</code> tests with a write workload (<code>readwrite</code>, <code>randrw</code>, <code>write</code>, <code>trimwrite</code>) directly against a device that is in use!</p>
</div>

#### 6.2.2 IOPS Performance Tests with `fio`

The key here is to have high `iodepth`: 

- Test random read/writes: `fio --name=iops-test-job --rw=randrw --ioengine=libaio --iodepth=256 --bs=4k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting`
- Test random reads (requires existing files): `fio --name=iops-test-job --rw=randread --ioengine=libaio --iodepth=256 --bs=4k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`
- Test sequential reads (requires existing files): `fio --name=iops-test-job --rw=read --ioengine=libaio --iodepth=256 --bs=4k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`

#### 6.2.3 Throughput Performance Tests with `fio`

Lower `iodepth` (specifically with `64`, apparently): 

- Test random read/writes with blocksize of 64k: `fio --name=throughput-test-job --rw=randrw --ioengine=libaio --iodepth=64 --bs=64k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting`
- Test random read/writes with blocksize of 256k: `fio --name=throughput-test-job --rw=randrw --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting`
- Test sequential reads (requires existing files): `fio --name=throughput-test-job --rw=read --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`
- Test random reads (requires existing files): `fio --name=throughput-test-job --rw=randread --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`

#### 6.2.4 Latency Performance Tests with `fio`

- Test random read/writes for latency: `fio --name=rwlatency-test-job --rw=randrw --ioengine=libaio --iodepth=1 --bs=4k --size=100G --numjobs=1 --direct=1 --runtime=120 --time_based --group_reporting`
- Test random reads for latency (requires existing files): `fio --name=readlatency-test-job --rw=randread --ioengine=libaio --iodepth=1 --bs=4k --size=100G --numjobs=1 --direct=1 --runtime=120 --time_based --group_reporting --readonly`

#### 6.2.5 Job File for `fio`

The tool supports having a "job file" too, e.g. you can save it to any file, like a `fioread.fio`:

```
[global]
bs=256K
iodepth=64
direct=1
ioengine=libaio
group_reporting
time_based
runtime=120
numjobs=4
name=raw-read
rw=read
							
[job1]
filename=device name
```

...then just run the job, invoking the file as `fio`'s only parameter: `fio read.fio`

#### 6.2.6 Scripting `fio` Tests

In reality, you might want to issue multiple tests with different blocksizes etc. It's always a good idea not to just issue long commands and wait but do them at once, half-automatically. 

Here's a possible script for excessive tests with all the six possibilities with five different blocksizes for measuring throughput performance from every aspects: 

```
#!/bin/sh

NAME="throughput-test-job"

if [ -d /data ] && [ $(df -m /data | awk '!/Filesystem/{print $4}') -gt 400 ]
  then cd /data
  else
    echo "There's no /data! Exiting."
    exit 1
fi
if [ ! -f /usr/bin/fio ]
  then
    echo "There's no fio! You should install it first. Exiting."
    exit 1
fi

echo "Throughput Performance Tests with iodepth of 64 and runtime of 120 seconds per job"

for RW in write read rw randwrite randread randrw
  do
    for BS in 4k 64k 256k 1M 1G
      do
        echo "Testing in \"$RW\" I/O pattern with $BS blocksize..."
        if [ $RW = "read" ] || [ $RW = "randread" ]
          then
            RO="--readonly"
            echo "Read-only mode requires existing files to test against. Skipping cleanup."
          else
            RO=""
            echo "Cleaning up..."
            rm -f $NAME* ; sync
            echo -e "\e[1ACleaning up... done"
        fi
        fio \
          --name=$NAME \
          --rw=$RW \
          --ioengine=libaio \
          --iodepth=64 \
          --bs=$BS \
          --numjobs=4 \
          --size=100G \
          --direct=1 \
          --runtime=120 \
          --time_based \
          --group_reporting \
          $RO  # you may consider adding " | grep IOPS" here if you want final results only
        echo "-----"
        sleep 5
      done
  done

rm -f $NAME*
echo "Done."
```

### 6.3 `iostat`

The utility is for reporting CPU statistics and I/O statistics for devices and partitions, and it is part of the `sysstat` package:

```ksh
# dnf whatprovides iostat
Last metadata expiration check: 0:00:10 ago on Sat 29 Jun 2024 01:28:39 PM EEST.
sysstat-12.5.4-7.el9.x86_64 : Collection of performance monitoring tools for Linux
Repo        : appstream
Matched from:
Filename    : /usr/bin/iostat
```

Synopsis:

```
iostat [ -c ] [ -d ] [ -h ] [ -k | -m ] [ -N ] [ -s ] [ -t ] [ -V ] [ -x ] [ -y ] [ -z ] \
[ --dec={ 0 | 1 | 2 } ] [ { -f | +f } directory ] [ -j { ID | LABEL | PATH | UUID | ... } ] \
[ -o JSON ] [ [ -H ] -g group_name ] [ --human ] [ --pretty ] [ -p [ device[,...] | ALL ] ] \
[ device [...] | ALL ] [ interval [ count  ] ]
```

Example usage for getting the statistics refresh interval in seconds:

```ksh
# iostat -d -k 1 sdb | awk '/sdb/ {printf "Read: "$3", Write: "$4"\n"}'
Read: 2368.89, Write: 428.41
Read: 0.00, Write: 0.00
Read: 0.00, Write: 128.00
Read: 0.00, Write: 7.92
Read: 0.00, Write: 4.00
Read: 0.00, Write: 0.00
Read: 0.00, Write: 0.00
Read: 0.00, Write: 0.00
^C
```

### 6.4 The `hdparm` and `sdparm` utilities

Let's see two example disk devices for checking them further. Both of them are Virtual Disk in RAID configuration in Dell PowerEdge servers. We can also check several features of them using `dmesg` tool, e.g. their kernel level defaults on "Write cache" feature: 

```ksh
$ lsscsi
[0:2:0:0]    disk    DELL     PERC H745 Frnt   5.16  /dev/sdb 
[1:0:0:0]    disk    ATA      DELLBOSS VD      00-0  /dev/sda 
[3:0:0:0]    process Marvell  Console          1.01  -        

$ sudo dmesg -Tx | grep "Write cache"
kern  :notice: [Fri Apr 26 16:12:43 2024] sd 1:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
kern  :notice: [Fri Apr 26 16:12:45 2024] sd 0:2:0:0: [sdb] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
```

In the above example, `sda` is a single RAID-1 while `sdb` is a RAID-6 with a special controller, so `hdparm` cannot be used for that one, only for `sda`: 

```ksh
$ sudo hdparm -i /dev/sda

/dev/sda:

 Model=DELLBOSS VD, FwRev=MV.R00-0, SerialNo=1be5306b4b6e0010
 Config={ Fixed }
 RawCHS=16383/16/63, TrkSize=34902, SectSize=512, ECCbytes=4
 BuffType=DualPortCache, BuffSize=8192kB, MaxMultSect=16, MultSect=off
 CurCHS=16383/16/3, CurSects=7864440, LBA=yes, LBAsects=937571968
 IORDY=yes, tPIO={min:120,w/IORDY:0}, tDMA={min:120,rec:120}
 PIO modes:  pio0 pio1 pio2 pio3 pio4 
 DMA modes:  mdma0 mdma1 mdma2 
 UDMA modes: udma0 udma1 udma2 udma3 udma4 udma5 udma6 
 AdvancedPM=no WriteCache=enabled
 Drive conforms to: unknown:  ATA/ATAPI-1,2,3,4,5,6,7

 * signifies the current active mode

$ sudo hdparm -i /dev/sdb

/dev/sdb:
SG_IO: bad/missing sense data, sb[]:  70 00 05 00 00 00 00 0d 00 00 00 00 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
 HDIO_GET_IDENTITY failed: Invalid argument
```

So, for SSD devices, you need the `sdparm` utility: 

```ksh
$ sudo sdparm -i /dev/sdb
    /dev/sdb: DELL      PERC H745 Frnt    5.16
Device identification VPD page:
  Addressed logical unit:
    designator type: NAA,  code set: Binary
      0x6ec2a7201d532b002da2ac410375b313

$ sudo sdparm -a /dev/sdb
    /dev/sdb: DELL      PERC H745 Frnt    5.16
Caching (SBC) mode page:
  IC            0  [cha: n, def:  0, sav:  0]
[...]

$ sudo sdparm --get WCE /dev/sdb
    /dev/sdb: DELL      PERC H745 Frnt    5.16
WCE           0  [cha: n, def:  0, sav:  0]
```

The "mode page" part also indicates that it cannot be saved with `sdparm`'s `--save` parameter, so e.g. when we attempt to set the above checked Write Cache parameter, then we can be sure it's not possible (my guess is that it's controlled in HW RAID Controller level, as set up in iDRAC), but here's what we get if we try to modify it: 

```ksh
$ sudo sdparm --set WCE=1 /dev/sdb
    /dev/sdb: DELL      PERC H745 Frnt    5.16
change_mode_page: failed setting page: Caching (SBC)
```

<a name="7"></a>
## 7. LUKS Encryption

...

Prerequisite is the `cryptsetup` program/package, so install it first. E.g.:

```ksh
# dnf install cryptsetup
[...]
Installed size: 500 k
Is this ok [y/N]: y
[...]
Installed:
  cryptsetup-2.3.7-7.el8.x86_64

Complete!
```

If the device was mounted as a regular FS before, make sure it's umounted, and also removed from `/etc/fstab`:

```ksh
# vi /etc/fstab

# systemctl daemon-reload
```

In case it was involved to encryption before, close this device, e.g.:

```ksh
# cryptsetup close /dev/mapper/lv_datak8s
```

1. Generate a passphrase for yourself first, and store it in a vault. E.g. a 32 character long passphrase, having upper-case, lower-case letters, digits and special characters (!, $, %, &, ...) is surely a strong one.

2. Encrypt the device, which is an LV in this example:

```ksh
# cryptsetup luksFormat /dev/mapper/vg_data-lv_datak8s 
WARNING: Device /dev/mapper/vg_data-lv_datak8s already contains a 'xfs' superblock signature.

WARNING!
========
This will overwrite data on /dev/mapper/vg_data-lv_datak8s irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for /dev/mapper/vg_data-lv_datak8s: 
Verify passphrase: 
```

3. Open (decrypt) the device:

```ksh
# cryptsetup open --type luks /dev/mapper/vg_data-lv_datak8s lv_datak8s_encr
Enter passphrase for /dev/mapper/vg_data-lv_datak8s: 
```

4. Format it as you wish, e.g.:

```ksh
# mkfs.xfs /dev/mapper/lv_datak8s_encr
[...]
```

5. Mount it:

```ksh
# mount /dev/mapper/lv_datak8s_encr /data/k8s
```

6. Get an overview of the disk as block device, e.g.:

```ksh
# lsblk /dev/sda
NAME                   MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda                      8:0    0 893.3G  0 disk  
└─sda1                   8:1    0 893.3G  0 part  
  ├─vg_data-lv_datak8s 253:7    0 625.3G  0 lvm   
  │ └─lv_datak8s_encr  253:9    0 625.3G  0 crypt /data/k8s
  └─vg_data-lv_datak0s 253:8    0   268G  0 lvm   /data/k0s
```

