# ![alt text](/_ui/img/icons/suse_m.svg) openSUSE

###### .

>Szerkesztés alatt...

## Table of Contents

1. [Introduction](#1)
2. [Install](#2)
3. [Package Management](#3)
4. [Multimedia](#4)
5. [Storage](#5)

<a name="1"></a>
## 1. Introduction

...

openSUSE images for for ARM in "JeOS" release can be found [here](https://en.opensuse.org/Portal:Arm#Release_supported_platforms).

...

<a name="2"></a>
## 2. Install

Fontos, hogy bár az openSUSE csapata továbbra is legyártja a különféle **Live image**-eket, azokat nem ajánlja ezzel a szöveggel: 

>Please be aware of the following limitations of the live images:

>&nbsp;

>\- They should not be used to install or upgrade Tumbleweed. Please use the Tumbleweed media instead

>\- They have a limited package and driver selection, so cannot be considered an accurate reflection as to whether Tumbleweed will work on your hardware or not

Az alábbiakban az **openSUSE Tumbleweed GNOME Live ISO** épp aktuális változatát töltjük le, majd telepítjük egy már létező rendszert felülírva. Ez nagyon hasonló a Tumbleweed "media"-ból ("**Offline Image**", kb. 4.4 GiB) való telepítéshez, de említésre fog kerülni, ahol lényeges különbségek vannak. 

### 2.1 Downloading the Install Image

Az "Offline Image" tehát az ajánlott, de 4.4 GiB-os és nem kapunk egy azonnal használható GNOME GUI-t, míg a "GNOME Live" mindössze kb. 900 MiB-os, egyből egy barátságos GUI töltődik be, amiről akár telepíthetünk is, viszont ez hivatalosan kevésbé támogatott. 

Linkek a telepítési médiákhoz: 

- Offline Image (x86\_64): [openSUSE-Tumbleweed-DVD-x86_64-Current.iso](https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-DVD-x86_64-Current.iso)
- GNOME Live (x86\_64): [openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso](https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso)

Az `.iso`-t letölthetjük egyszerűen böngészőben is, torrentről, vagy itt most csak a link ismeretében a `wget` paranccsal (a példában erősen limitálva a sebességet, hogy közben maradjon másoknak is internet): 

```ksh
$ wget https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso --limit-rate=200k
--2021-04-29 16:19:15--  https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso
Resolving download.opensuse.org (download.opensuse.org)... 195.135.221.134, 2001:67c:2178:8::13
Connecting to download.opensuse.org (download.opensuse.org)|195.135.221.134|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso [following]
--2021-04-29 16:19:16--  https://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso
Reusing existing connection to download.opensuse.org:443.
HTTP request sent, awaiting response... 302 Found
Location: http://quantum-mirror.hu/mirrors/pub/opensuse/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso [following]
--2021-04-29 16:19:16--  http://quantum-mirror.hu/mirrors/pub/opensuse/tumbleweed/iso/openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso
Resolving quantum-mirror.hu (quantum-mirror.hu)... 195.38.126.147, 78.131.56.189
Connecting to quantum-mirror.hu (quantum-mirror.hu)|195.38.126.147|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 909115392 (867M) [application/octet-stream]
Saving to: ‘openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso’

d-GNOME-Live-x86_64-Current.iso       60%[============================>                   ] 527.64M   200KB/s    eta 28m 58s
```

Töltsük le az SHA256 Checksum fájlt is, és ellenőrizzük, hogy a letöltött fájl biztosan hibátlan (a két hash megegyezik): 

```ksh
$ ls -la
total 1117224
drwxr-xr-x  3 gjakab users      4096 ápr   29 17.42 .
drwxr-xr-x 31 gjakab users      4096 ápr   18 09.55 ..
-rw-r--r--  1 gjakab users 909115392 ápr   28 08.40 openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso
-rw-r--r--  1 gjakab users       131 ápr   29 17.42 openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso.sha256

$ sha256sum openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso 
7e04e5cf1362d2e2ff77671d5017bf4147d2ef29770df9c7ccd0ab01584fd01a  openSUSE-Tumbleweed-GNOME-Live-x86_64-Current.iso

$ cat openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso.sha256
7e04e5cf1362d2e2ff77671d5017bf4147d2ef29770df9c7ccd0ab01584fd01a  openSUSE-Tumbleweed-GNOME-Live-x86_64-Snapshot20210427-Media.iso
```

### 2.2 Bootolható pendrive előkészítése

Windows alatt a [Rufust](https://rufus.ie), Linux alatt egyszerűen a `dd` parancsot tudom ajánlani bootolható pendrive készítéséhez. Ez utóbbinak ismertetése az [általános linuxos témakörök](/learning/it/sysadmin/linux/basics/#5) kategóriában található. 

Ezután már indíthatjuk is újra a laptopunkat/PC-nket, az első másodpercekben nyomkodjuk a boot menüjét előhozó billentyűt (pl. F12-t), majd indulhat is a telepítés. 

### 2.3 A telepítés

A Live image esetében gyorsan bebootol egy minimális környezet, míg az Offline Image-ből való telepítés esetében már a boot során választható az "Installation" menüpont.

#### 2.3.1 A telepítés indítása

Live rendszer esetében nyomjunk egy [Win] billentyűt, majd gépeljük be: "install". A megjelenő listából válasszuk az "Installation"-t: 

![alt_text](ost-ins-01.png)

#### 2.3.2 Nyelv, billentyűzet és licensz

![alt_text](ost-ins-02.png)

Ennél a pontnál az Offline Image-ből induló telepítő rákérdez a **hálózat beállítására**. Pl. itt praktikusan a WIFI-t állítjuk be, majd léphetünk is tovább. 

#### 2.3.3 Online Repositories

A telepítés során bekapcsolhatjuk az online repository-k használatát. Ezáltal a telepítés rettentő lassú lesz, viszont biztosan mindenből a legújabb verzió kerül telepítésre. Az alábbi példában bekapcsoltam: 

![alt_text](ost-ins-03.png)

Az Offline Image esetében nem szükséges ennek bekapcsolása. Sőt, sokkal gyorsabb, ha inkább nem most vacakolunk ezzel. 

Amennyiben bekapcsolásra került az **Online Repositories**, rá is kérdez a telepítő, hogy melyikeket aktiválnád még az alapértelmezetteken kívül: 

![alt_text](ost-ins-04.png)

#### 2.3.4 A rendszer szerepe

A telepítés során később még egyes alkomponenseket ki-be kapcsolgathatunk a végső **Install** gomb megnyomása előtt. 

![alt_text](ost-ins-05.png)

#### 2.3.5 Partícionálás

A telepítő felkínál egy javasolt szétosztást, de mi ügyesek vagyunk és tudunk jobbat. 

![alt_text](ost-ins-06.png)

Itt egy példát láthatunk, amikor is egy 120 GB-os SSD-re hagyományos partícionálással korábban már létrehoztunk...

- egy EFI Boot partíciót
- egy `/` partíciót
- egy swap területet
- minden maradékot a `/home`-nak, amit még titkosítottunk is

![alt_text](ost-ins-07.png)

A formázandó partícióknak is meg kell adnunk, hogy hová, hogyan csatolódjanak fel. 

A legelső, pici partíció tehát egy EFI Boot partíció:

![alt_text](ost-ins-08.png)

Ennek egy FAT típusú FS-nek és a `/boot/efi` alá csatolva kell léteznie: 

![alt_text](ost-ins-09.png)

A `/` már lehet a hagyományos Ext4: 

![alt_text](ost-ins-10.png)

A swap: 

![alt_text](ost-ins-11.png)

A `/home`-ot jelen esetben nem formázzuk, csak a mount pointját határozzuk meg: 

![alt_text](ost-ins-12.png)

Az összesítést alaposan nézzük át: 

![alt_text](ost-ins-13.png)

Jöhet egy ilyen figyelmeztetés, de általában ez ignorálható; rendszerünk képes lesz bebootolni: 

![alt_text](ost-ins-14.png)

Még egy összesítés: 

![alt_text](ost-ins-15.png)

Egyébként a fentieknél is jobb, modernebb, ha LVM-et használunk (annál is titkosíthatunk csak egy LV-t is) egy apró, 8 MB-os "BIOS Boot" partícióval: 

Diszk nézet: 

![alt_text](partitions_disk.png)

LVM nézet: 

![alt_text](partitions_lvm.png)

#### 2.3.6 Dátum és időzóna

![alt_text](ost-ins-16.jpg "")

#### 2.3.7 User lértehozása

Ezt én nem szoktam itt elvégezni, mivel a user nevével ellátott group is létrejön így. Inkább jobb szeretem, ha a telepítés után utólag, a root user alól készítem el a useremet a `users` primary groupba és a `wheels` SUDO-s groupba téve. 

![alt_text](ost-ins-17.png)

#### 2.3.8 A root jelszava

![alt_text](ost-ins-18.png)

#### 2.3.9 Végső összesítés

Itt – egyebek mellett – még ki/be kapcsolhatjuk az esetleges extra programcsoportokat (ha pl. nem kellenek játékok) vagy engedélyezhetjük már most pl. az SSHD-t: 

![alt_text](ost-ins-19.png)

És mégegy rákérdezés: 

![alt_text](ost-ins-20.png)

#### 2.3.10 A telepítés befejezése

Itt minden megy automatikusan. Közben még próbálkozhat az internetre csatlakozni, ha esetleg nem állítottunk be hálózatot. 

![alt_text](ost-ins-21.png)

### 2.4 Telepítés utáni teendők

#### 2.4.1 Userek létrehozása, beállítása

```ksh
# df -hPT
Filesystem            Type      Size  Used Avail Use% Mounted on
devtmpfs              devtmpfs  3.8G     0  3.8G   0% /dev
tmpfs                 tmpfs     3.8G     0  3.8G   0% /dev/shm
tmpfs                 tmpfs     1.5G   10M  1.5G   1% /run
/dev/sda2             ext4       20G  4.8G   14G  26% /
tmpfs                 tmpfs     3.8G   16K  3.8G   1% /tmp
/dev/sda1             vfat      8.0M     0  8.0M   0% /boot/efi
/dev/mapper/cr-auto-1 ext4       90G   76G  9.1G  90% /home
tmpfs                 tmpfs     764M   40K  764M   1% /run/user/0
tmpfs                 tmpfs     764M   80K  764M   1% /run/user/1000
tmpfs                 tmpfs     764M   56K  764M   1% /run/user/458

# cd /home

# ls -la
total 32
drwxr-xr-x  5 root     root   4096 Apr 20 00:56 .
drwxr-xr-x 21 root     root   4096 Apr 29 18:32 ..
drwx------  2 root     root  16384 Aug 17  2018 lost+found
drwxr-xr-x 19 1001     users  4096 Mar 29 19:08 gizi
drwxr-xr-x 31 1000     users  4096 Apr 29 17:54 gjakab

# useradd -g users -G wheel -u 1000 gjakab

# useradd -g users -u 1001 gizi

# ls -la
total 32
drwxr-xr-x  5 root     root   4096 Apr 20 00:56 .
drwxr-xr-x 21 root     root   4096 Apr 29 18:32 ..
drwx------  2 root     root  16384 Aug 17  2018 lost+found
drwxr-xr-x 19 gizi     users  4096 Mar 29 19:08 gizi
drwxr-xr-x 31 gjakab   users  4096 Apr 29 17:54 gjakab

# passwd gjakab
New password: 
Retype new password: 
passwd: password updated successfully

# passwd gizi
New password: 
Retype new password: 
passwd: password updated successfully

# visudo
...
%wheel ALL=(ALL) NOPASSWD: ALL
...
```

#### 2.4.2 Hostname

Hasznos lehet, ha a gép Serialját állítjuk be hostname-nek: 

```ksh
# dmidecode | grep "Serial Number"
...
	Serial Number: PB4XXYZ
...

# hostnamectl hostname PB4XXYZ
```

<a name="3"></a>
## 3. Package Management

A SUSE már az ősidők óta egy RPM alapú disztribúció és már 2006 óta a stabil és kiforrott **ZYpp** csomagkezelőt használja. Az elnevezés a *"Zen / YaST Packages Patches Patterns Products"* rövidítésből ered, a fő parancsa a `zypper`. 

### 3.1 A `zypper`

Elvileg gyorsabb és kényelmesebb is, mint az ugyanerre való, a Red Hatban és forkjaiban elterjedt `yum` vagy a `dnf`. 

#### 3.1.1 Searching with `zypper`

Külön öröm, hogy már alapértelmezetten kiírja a `search`-nél is a csomag státuszát, pl.:

```ksh
# zypper search mc
...
S  | Name                                               | Summary                                                                      | Type
---+----------------------------------------------------+------------------------------------------------------------------------------+-----------
...
i+ | mc                                                 | Midnight Commander                                                           | package
...
```

Listing packages installed from a particular Repository:

```ksh
# zypper pa -ir packman
Loading repository data...
Reading installed packages...
S  | Repository | Name                      | Version                                 | Arch
---+------------+---------------------------+-----------------------------------------+-------
i  | packman    | audacious-plugins         | 4.3-1699.9.pm.60                        | x86_64
v  | packman    | audacious-plugins         | 4.3-1699.9.pm.59                        | i586
i  | packman    | audacious-plugins-extra   | 4.3-1699.9.pm.60                        | x86_64
v  | packman    | audacious-plugins-extra   | 4.3-1699.9.pm.59                        | i586
i+ | packman    | avidemux3                 | 2.8.1-1699.2.pm.64                      | x86_64
[...]
```


#### 3.1.2 Installing with `zypper`

Repo-ból, pl.: 

```ksh
# zypper search ksh
...
S | Name                          | Summary                                             | Type
--+-------------------------------+-----------------------------------------------------+--------
  | ksh                           | Korn Shell                                          | package
...

# zypper install ksh
...
The following NEW package is going to be installed:
  ksh
...
Continue? [y/n/v/...? shows all options] (y): y
...
[done]

# zypper search ksh
...
S  | Name                          | Summary                                             | Type
---+-------------------------------+-----------------------------------------------------+--------
i+ | ksh                           | Korn Shell                                          | package
...
```

Külön beszerzett vagy saját `.rpm`-ből: 

```ksh
# zypper install libvpx-1.6.1-29.2.x86_64.rpm
...
warning: /var/tmp/zypp.x7ybuz/zypper/_tmpRPMcache_/%CLI%/libvpx-1.6.1-29.2.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 6722fe32: NOKEY
Looking for gpg key ID 6722FE32 in cache /var/cache/zypp/pubkeys.
Repository Plain RPM files cache does not define additional 'gpgkey=' URLs.
libvpx-1.6.1-29.2.x86_64 (Plain RPM files cache): Signature verification failed [4-Signatures public key is not available]
Abort, retry, ignore? [a/r/i] (a): i
...
[done]
Additional rpm output:
warning: /var/cache/zypper/RPMS/libvpx-1.6.1-29.2.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 6722fe32: NOKEY

#
```

#### 3.1.3 System update with `zypper`

A **Leap** kiadás egy állandó dolog, jelentős verzióugrások nincsenek, az "OS release" marad az eredeti, így annak frissítését "**Update**"-nek tekinthetjük, a **Tumbleweed** viszont a rolling update szemléletben készült, ezért annak minden több csomagot érintő frissítése egy "**Distribution Update**". Ennek megfelelően a Leap frissítése a `zypper up`-al, a Tumbleweed-é a `zypper dup`-al ajánlott. Méghozzá erősen ajánlott. 

The Tumbleweed release always needs distribution upgrade `zypper dup` instead of update (`zypper up`)!

Az "alapos" frissítés ebben a sorrendben ajánlott: 

1. Repo-k kiürítése (esetlegesen addig letöltött `.rpm`-ek eltávolítása)
2. Update csomagok listázása
3. Update / Distribution Update
4. Reboot

Példa ezekre a lépésekre egy Tumbleweed-en: 

```ksh
# zypper clean
All repositories have been cleaned up.

# zypper lu
Loading repository data...
Reading installed packages...
S | Repository              | Name                                     | Current Version                | Available Version              | Arch
--+-------------------------+------------------------------------------+--------------------------------+--------------------------------+-------
v | openSUSE-Tumbleweed-Oss | AppStream                                | 0.15.1-1.1                     | 0.15.1-1.2                     | x86_64
v | openSUSE-Tumbleweed-Oss | ImageMagick                              | 7.1.0.17-1.2                   | 7.1.0.19-1.1                   | x86_64
v | openSUSE-Tumbleweed-Oss | ImageMagick-config-7-SUSE                | 7.1.0.17-1.2                   | 7.1.0.19-1.1                   | x86_64
v | openSUSE-Tumbleweed-Oss | Mesa                                     | 21.3.1-295.4                   | 21.3.3-297.1                   | x86_64
...

# zypper dup
...
Warning: You are about to do a distribution upgrade with all enabled repositories. Make sure these repositories are compatible before you continue. See 'man zypper' for more information about this command.
Computing distribution upgrade...
...
The following 1324 packages are going to be upgraded:
...
The following product is going to be upgraded:
  openSUSE Tumbleweed  20220103-0 -> 20220113-0
...
1324 packages to upgrade, 9 new, 2 to remove.
Overall download size: 1.22 GiB. Already cached: 0 B. After the operation, additional 374.3 MiB will be used.
...
    Note: System reboot required.
Continue? [y/n/v/...? shows all options] (y): y
...
Reboot is suggested to ensure that your system benefits from these updates.

# sync

# reboot
```

### 3.2 The PackageKit

Frissen elindított rendszer esetében elég nagy rá az esély, hogy még jóideig nem tudjuk a `zypper`-t használni, mert a **PackageKit** nézelődik a lehetséges online update-ek után, letölt, és amit csak lehet, telepít. Amennyiben manuálisan szeretnénk valamit telepíteni vagy csak akár `zypper search`-öt futtatni valamilyen csomag után keresve, az alábbi hiba és kérdés fogad: 

```ksh
...
PackageKit is blocking zypper. This happens if you have an updater applet or other software
management application using PackageKit running.
We can ask PackageKit to interrupt the current action as soon as possible, but it depends on
PackageKit how fast it will respond to this request.
Ask PackageKit to quit? [yes/no] (no): no
...
```

#### 3.2.1 Disabling PackageKit

Amennyiben téged is bosszant a fenti dolog és te is jobb szereted CLI-ből frissíteni a rendszeredet, erősen megfontolandó, hogy kikapcsold és le is tiltsd a PackageKit-et. 

Deaktiválás és ellenőrzés: 

```ksh
# systemctl disable --now packagekit

# systemctl status packagekit
○ packagekit.service - PackageKit Daemon
     Loaded: loaded (/usr/lib/systemd/system/packagekit.service; static)
     Active: inactive (dead)
...
Jan 09 15:43:48 munch systemd[1]: Stopping PackageKit Daemon...
Jan 09 15:43:48 munch systemd[1]: packagekit.service: Deactivated successfully.
Jan 09 15:43:48 munch systemd[1]: Stopped PackageKit Daemon.
Jan 09 15:43:48 munch systemd[1]: packagekit.service: Consumed 54.303s CPU time.
```

További, online update-el kapcsolatos beállításokat a YAST2-ben is átnézhetünk ill. ki-/bekapcsolhatunk a **YAST** &rarr; **Software** &rarr; **Online Update** &rarr; **Configuration** menüpont &rarr; **Online Update** menüpontjában, amihez egy plusz csomagot is valószínűleg telepítenünk kell: 

![alt_text](ost-yast2_online_upd_conf.png)

<a name="4"></a>
## 4. Multimedia

A gyári telepítés nem tartalmazza az FFMpeg, Xvid stb. codec-eket licenszbeli kételyek miatt. Ezt a a hiányosságot a Packman repo hozzáadásával, néhány gyári csomag lecserélésével és továbbiak telepítésével oldhatjuk fel. 

A legelső parancs esetében a rendszerünk Tumbleweed, de Leap esetében csak az `openSUSE_Tumbleweed` részt kell `openSUSE_Leap_$releasever`-re cserélnünk:

```ksh
# zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/' packman
Adding repository 'packman' ...............................................................................[done]
Repository 'packman' successfully added
...

 # zypper refresh
Repository 'openSUSE-20220102-0' is up to date.
...
Do you want to reject the key, trust temporarily, or trust always? [r/t/a/?] (r): a
...

# zypper dist-upgrade --from packman --allow-vendor-change
Loading repository data...
...
The following 35 packages are going to change vendor:
  gstreamer-plugin-pipewire     openSUSE -> http://packman.links2linux.de
  gstreamer-plugins-bad         openSUSE -> http://packman.links2linux.de
  gstreamer-plugins-libav       openSUSE -> http://packman.links2linux.de
  gstreamer-plugins-ugly        openSUSE -> http://packman.links2linux.de
  libavcodec58_134              openSUSE -> http://packman.links2linux.de
...
35 packages to upgrade, 7 new, 35  to change vendor.
Overall download size: 18.6 MiB. Already cached: 0 B. After the operation, additional 25.9 MiB will be used.
Continue? [y/n/v/...? shows all options] (y): y
...
```

<a name="5"></a>
## 5. Storage

### 5.1 Munting an Android Phone's FS

Ehhez telepítenünk kell a Media Transfer Protocol (MTP) támogatását, amit legegyszerűbben az MTPFS csomag telepítésével oldhatunk meg: 

```ksh
# zypper search package:mtpfs
...
S  | Name              | Summary                                   | Type
---+-------------------+-------------------------------------------+--------
   | mtpfs             | FUSE filesystem that supports MTP devices | package
   | mtpfs-debuginfo   | Debug information for package mtpfs       | package
   | mtpfs-debugsource | Debug sources for package mtpfs           | package
...

# zypper install mtpfs
...
```

