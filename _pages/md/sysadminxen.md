# ![alt text](/_ui/img/icons/xen_m.svg) Xen Project

<div style="text-align: right;"><h7>Posted: 2021-05-27 19:41</h7></div>

###### .

![alt_text](xen_bg.jpg)


## Table of contents

1. [Introduction](#1)
2. [The Xen Architecture](#2)
3. [Installation, Preparation](#3)
4. [Managing VMs with `libvirt`](#4)
5. [Managing VMs with `xen-tools`](#5)
6. [Further Storage Considerations](#6)
7. [Further Network Considerations](#7)
8. [The XCP-ng Xen Virtualization Management Platform](#8)


<a name="1"></a>
## 1. Introduction

The hypervisor of **Xen Project** was the first open source solution of its kind in 2005, that provided a **Type-1** virtualization, such as **Bare-Metal**, such as **Full** Virtualization. In Xen's terms, it supports **HVM guests** since then. 

In short, in case of Type-1, the Virtual Machine has a chance to access the machine's resources through the **hypervisor** directly. Unless in case of Type-2, there is no another, additional layer, represented by the Operating System and its drivers. Of course, accessing everything as Type-1 is sometimes not entirely ideal or optimal, that's why later on Xen introduced further features to improve performance whenever is possible and required. 

Even though the set of solutions that Xen provides are mostly referenced as Type-1, using the Xen Project gives more options. If the Type-1 design cannot be created or we don't want it to be created, so we don't want to dedicate a whole physical machine for it, then we can choose using it without full hardware virtualization in a paravirtualized design (PV or PVH). Using it, even such crazy ideas can be done like virtualizing further VMs under a VirtualBox VM. 

It is also important to note that Xen is not the same as **XenServer**. XenServer is the whole platform made by **Citrix**, and the source of the fork that gave **XCP-ng**. The Xen Project is a true **GPLv2** open source project, whith contribution from Citrix, Arm, SUSE, AWS and so on. Also, Citrix renamed XenServer to **Citrix Hypervisor** in 2018, but that's the exact same thing. According to all of these, the two "product" lines are being developed mostly independently. 


<a name="2"></a>
## 2. The Xen Architecture

In Xen's terminology, **both the VMs and the host systems are so called domains**: 
- **Dom0** or **Domain-0** is an abbreviation for **domain zero**, the management or control domain with privileged access to the hardware and device drivers. **It starts first and manages the DomU unprivileged domains.** The Xen hypervisor is not usable without Dom0. This is essentially the "host" operating system.  
- **DomU** is an unprivileged domain with no direct access to the hardware or device drivers. These types of domains can access anything only through the **Xen Virtual Machine Monitor** (VMM)

![alt_text](xen_-_architecture_-_dom0-domu.png)

## 2.1 PV, HVM and PVH

The modes of Xen virtualization is shown by the below picture (which is from the official page of Xen Project): 

![alt_text](xen_-_guestmodes.png)

On **ARM** hosts, there is only one guest type (see below in figure). On **x86** hosts, the hypervisor supports its three main modes **PV**, **HVM** and **PVH**, and some mixtures of these are also possible. 

### 2.1.1 PV

PV means **Paravirtualized**. This is the mode that the Xen Project originally introduced in 2003, and later adopted by other virtualization platforms. PV **does not require virtualization extensions** from the host CPU, but requires Xen-aware guest operating systems (which has been true for all the Linux distros out there for decades). PV guests are primarily of use for legacy HW and legacy guest images and in special scenarios, e.g. special guest types, special workloads (e.g. Unikernels), running Xen within another hypervisor without using nested hardware virtualization support. This is the mode when you can even achieve hosting a VM, under another VM for some wild experiments. 

### 2.1.2 HVM

It stands for **Hardware-assisted Virtual Machine**, introduced in late 2005. Besides you need to have the **Intel VT-x** or **AMD-V** hardware extensions enabled, the Xen Project software uses **QEMU** device models to emulate PC hardware, including BIOS, IDE disk controller, VGA graphic adapter, USB controller, network adapter, etc. HVM Guests use **PV interfaces and drivers** when they are available in the guest. For Windows guests, drivers are available to download at Xen Project's website. When available, HVM will use Hardware and Software Acceleration, such as Local APIC, Posted Interrupts, Viridian (Hyper-V) enlightenments and make use of guest PV interfaces where they are faster. Typically **HVM is the best performing option** for Linux, Windows, *BSDs. 

### 2.1.3 PVH, and its mixture modes: HVM with PV drivers and PVHVM

**PVH** guests are **lightweight HVM-like** guests that use virtualization extensions from the host CPU to virtualize guests. Unlike HVM guests, **PVH guests do not require QEMU** to emulate devices, but use PV drivers for I/O and native operating system interfaces for virtualized timers, virtualized interrupt and boot. PVH guests require PVH enabled guest operating system. This approach is similar to how Xen virtualizes ARM guests, with the exception that ARM CPUs provide hardware support for virtualized timers and interrupts. As PVH is the latest refinement of PV mode, and it combines the best advantages of Xen Project's PV mode with full utiliation of HW support. 

The **HVM with PV drivers** on the guest, and the HVM with **PVHVM Capable Guest** are both require the HVM operating mode. 

## 2.1.4 The operation modes in summary

![alt_text](xen_-_guestmodes_-_summary.png)

Some more references: 

- [docs.opensuse.org: xen](https://doc.opensuse.org/documentation/leap/virtualization/html/book-virtualization/part-virt-xen.html)
- [brendangregg.com: Xen feature detection](https://www.brendangregg.com/blog/2014-05-09/xen-feature-detection.html)
- [xenproject.org: the Virtualization Spectrum](https://wiki.xenproject.org/wiki/Understanding_the_Virtualization_Spectrum)


<a name="3"></a>
## 3. Installation, Preparation

Let's assume you have an x86_64 PC which can be also any laptop, miniPC, whatever. Ideally, it should have at least 8 GB of RAM, a CPU with 2 cores (4 threads), and also storage on it with some tens of GB of space. 

### 3.1 Prerequisites

#### 3.1.1 BIOS Settings

To achieve the full HVM on x86 platform, you need to have the **hardware virtualization feature** enabled in BIOS. It means **VT-x** on Intel and **AMD-V** on AMD systems. There are many ways to check that, this example is one of them to ensure it on an Intel Core i5 based machine (it will show the same line for each thread): 

```ksh
$ grep -E "svm|vmx" /proc/cpuinfo
flags		: [...] dtes64 monitor ds_cpl vmx smx est tm2 ssse3 [...]
```

More details in BIOS configuration requirements for Xen HVM can be found e.g. [here](https://vidigest.com/2020/01/23/bios-configuration-requirements-for-xen-project-hvm/).

#### 3.1.2 Choosing a Linux distro

Regarding the host OS, theoretically almost any Linux distros can be used, but openSUSE is one of the main contributors, so you will surely find all the necessary tools in their latest version in its standard repository.

Here is what an openSUSE Tumbleweed offers (as of September, 2022) when you search for the pattern `xen` (Leap has probably a very similar list): 

```ksh
# zypper search xen
Loading repository data...
Reading installed packages...

S | Name                           | Summary                                                    | Type
--+--------------------------------+------------------------------------------------------------+-----------
  | grub2-i386-xen                 | Bootloader with support for Linux, Multiboot and more      | package
  | grub2-x86_64-xen               | Bootloader with support for Linux, Multiboot and more      | package
[...]
  | libvirt-daemon-xen             | Server side daemon & driver required to run XEN guests     | package
[...]
  | patterns-server-xen_server     | Xen Virtual Machine Host Server                            | package
  | patterns-server-xen_tools      | XEN Virtualization Host and tools                          | package
[...]
  | xen-devel                      | Xen Virtualization: Headers and libraries for development  | package
  | xen-doc-html                   | Xen Virtualization: HTML documentation                     | package
i | xen-libs                       | Xen Virtualization: Libraries                              | package
  | xen-libs-32bit                 | Xen Virtualization: Libraries                              | package
  | xen-tools                      | Xen Virtualization: Control tools for domain 0             | package
  | xen-tools-domU                 | Xen Virtualization: Control tools for domain U             | package
  | xen-tools-xendomains-wait-disk | Adds a new xendomains-wait-disks.service                   | package
  | xen_server                     | Xen Virtual Machine Host Server                            | pattern
  | xen_tools                      | XEN Virtualization Host and tools                          | pattern
[...]
```

#### 3.1.3 Disk Volumes

It's important to know that both **Logical Volumes (LVs)** (*"Block Based"*) and different **image file types** (*"File Based"*) are supported techniques by Xen Project to use as VM storage. Using LVM and LVs directly may lead to smaller I/O overhead, because it eliminates a layer against the I/O operations. LVM, therefore, is considered as **Thick provisioning**, while using image files (also called as *"EXT"*) is **Thin provisioning**. 

As a consequence, if you think about designing your Virtual Environment for long term, you need prepare your host system also with some free disk space, maybe with separated Volume Group on a dedicated Datastore. 

### 3.2 Installation

There are two main ways to create and manage Xen VMs: 

- Through `libvirt`: it provides GUI tools and at least the `virsh` CLI tool for these activities. If you choose this way, the `libvirt-daemon-xen` and `virt-install`, `virt-manager` packages are needed too. 
- Through the standard `xl` CLI tool: no extra management layer is used in this case. 

Example for installing `xen-tools` (it has lots of other package requirements, like QEMU), and also the above mentioned `libvirt` toolset: 

```ksh
# zypper install xen-tools libvirt-daemon-xen virt-install virt-manager
Loading repository data...
Reading installed packages...
[...]
Executing %posttrans scripts .......................................................................[done]
[...]
```

Alternatively, it is also possible to just install everything automatically, from a GUI interface, using the **YaST2 - virtualization** tool. It can be accessed by just typing *"xen"* in the start screen or typing `yast2 virtualization` in the terminal: 

![alt_text](yast_-_virtualization_install_-_01.png)

Any way you chose, you will have the extra option in GRUB to boot from: 

![alt_text](opensuse_-_boot_-_xen_hypervisor.png)

The Linux kernel itself remains the same after rebooting into Xen mode, but `/boot/grub2/grub.cfg` shows that unnecessary kernel modules are skipped and the Xen kernel loads with `multiboot` feature (which is `multiboot2` in this example, for EFI extensions, and native EFI64) before loading the normal `vmlinuz*` kernel image: 

```
[...]
menuentry 'openSUSE Tumbleweed' [...] {
	load_video
	set gfxpayload=keep
	insmod gzio
	insmod part_gpt
	insmod lvm
	insmod ext2
[...]
	echo	'Loading Linux 5.19.7-1-default ...'
	linux	/boot/vmlinuz-5.19.7-1-default root=/dev/mapper/rootvg-rootlv  splash=silent resume=/dev/rootvg/swaplv mitigations=auto quiet lsm=apparmor
	echo	'Loading initial ramdisk ...'
	initrd	/boot/initrd-5.19.7-1-default
}
[...]
menuentry 'openSUSE Tumbleweed, with Xen hypervisor' [...] {
	insmod part_gpt
	insmod lvm
	insmod ext2
[...]
	echo	'Loading Xen 4.16.2_04-2 ...'
        if [ "$grub_platform" = "pc" -o "$grub_platform" = "" ]; then
            xen_rm_opts=
        else
            xen_rm_opts="no-real-mode edd=off"
        fi
	multiboot2	/boot/xen-4.16.2_04-2.gz placeholder dom0_max_vcpus=3 vga=gfx-1024x768x16 ${xen_rm_opts}
	echo	'Loading Linux 5.19.7-1-default ...'
	module2	/boot/vmlinuz-5.19.7-1-default placeholder root=/dev/mapper/rootvg-rootlv  splash=silent resume=/dev/rootvg/swaplv mitigations=auto quiet lsm=apparmor
	echo	'Loading initial ramdisk ...'
	module2	--nounzip   /boot/initrd-5.19.7-1-default
}
[...]
```

Once the packages are installed, you can ensure once more that the host is **HVM-ready**: 

```ksh
# xl dmesg | grep -i vmx
(XEN) VMX: Supported advanced features:
(XEN) VMX: Disabling executable EPT superpages due to CVE-2018-12207
(XEN) HVM: VMX enabled
```

`Domain-0` is ready already after reboot: 

```ksh
# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1801     4     r-----     185.8
Xenstore                                     1    31     1     -b----       0.0
```

Detect the virtualization type of the host OS: 

```ksh
# dmesg | grep -i xen
[...]
[    0.000000] Hypervisor detected: Xen PV
[    0.046474] Kernel/User page tables isolation: disabled on XEN PV.
[    0.278370] Setting APIC routing to Xen PV.
[    0.589982] Booting kernel on Xen
[    0.589983] Xen version: 4.16.2_04-2 (preserve-AD)
[    0.597375] xen: PV spinlocks enabled
[...]
```

Your host OS accesses the Hypervisor in a privileged PV mode, that's why `dmesg` still reports `Xen PV` is detected (at least, in my interpretation it is the case, which can be wrong). 

### 3.3 `virt-manager` GUI or `xl` commands?

The convenient way to manage Xen domains, so create, control, modify, migrate, or delete them, is using `virt-manager`, which means using `libvirt`. This toolset is a relatively new one as the first release, which was considered as stable, was released in 2014. By now, this tool became a unified way of managing VMs, as it can be used to manage not only Xen, but also KVM/QEMU, VMware ESXi, or even LXC containers are supported.

📌 Even though you have the powerful `xl` tool as the successor of the good old `xm` one (since 2014), **once you use the `libvirt` tool to create a VM, you cannot then use `xl` to either make adjustments or open `xl console` to that VM**!  
Watch the nice video about it: ["XM to XL: A short, but necessary journey"](https://www.youtube.com/watch?v=qdJi18VekEY)

When you manage VMs with `libvirt`, sometimes you need to use `virsh` too, as the command-line interface. It works by connecting to the `libvirt` API that connects to the hypervisor software. 

As a summary, here is the comparison from software footprint point of view (on an openSUSE distro): 

- `libvirt` (or `virt-manager`):
    - requires `libvirt`, `virt-install` YaST module, `libvirt-client` (includes `virsh`), comes with `virt-viewer`, still requires `xen` and `xen-libs`, and also requires `qemu` base packages to emulate HW for HVM guests, and some extra `qemu` packages 
    - it is roughly about 255 MB (OK, it's still not so much)
- `xl` (or `xen-tools`): 
    - requires only its own `xen` and `xen-libs` packages, and `qemu` base packages to emulate HW for HVM guests  
    - it is about 132 MB all together

### 3.4 Further kernel parameters

You can also fine-tune your Domain-0, optionally. E.g.:

```ksh
# vi /etc/default/grub
[...]
GRUB_CMDLINE_XEN="dom0_max_vcpus=2"
[...]

# grub2-mkconfig -o /boot/grub2/grub.cfg
[...]
```

You will see the previously specified number of cores in `/proc/cpuinfo` after the reboot. 


<a name="4"></a>
## 4. Managing VMs with `libvirt`

When `virt-manager` is started locally as normal user, it immediately asks for `root` password. 

### 4.1 Creating VMs remotely

In the following example, just for fun, we'll use `virt-manager` remotely, from another computer. It is also a good practice at the same time for how to forward X through `ssh`. 

#### 4.1.1 SSH Connection with forwarded X, directly with `root` user

This is the short, but unsafe way. 

You need to just enable the `root` login for `sshd.service`: 

1. Add the `PermitRootLogin yes` parameter in a new `/etc/ssh/sshd_config.d/rootlogin.conf` (on openSUSE),
2. Restart `sshd.service`, then
3. Login to this host with `ssh -X root@...` directly.
4. Start `virt-manager`

It's not so safe, but still an option e.g. for home labs.

#### 4.1.2 SSH Connection with forwarded X, with `xauth` key setup

In the below example, `PF0237F2` is the host with the Xen virtual environment: 

```ksh
gjakab@H92VFK3:~> ssh -X gjakab@192.168.101.102
Last login: Fri Sep 16 20:29:53 EEST 2022 from 192.168.101.100 on pts/5
Have a lot of fun...

gjakab@PF0237F2:~> xclock

gjakab@PF0237F2:~> echo $DISPLAY
localhost:10.0

gjakab@PF0237F2:~> xauth list
PF0237F2/unix:10  MIT-MAGIC-COOKIE-1  b2c73fa190012343659ea840dee03ad2

gjakab@PF0237F2:~> sudo su -

PF0237F2:~ # export DISPLAY=localhost:10.0

PF0237F2:~ # xauth add unix:10  MIT-MAGIC-COOKIE-1  b2c73fa190012343659ea840dee03ad2
xauth:  file /root/.Xauthority does not exist

PF0237F2:~ # xclock

PF0237F2:~ # virt-manager
```

### 4.2 Creating a VM with `virt-manager`

First, the hypervisor is not connected but it can be accessed by double clicking on it: 

![alt_text](virt-manager_-_01.png)

When we have a running VM, here is how it looks like: 

![alt_text](virt-manager_-_02.png)

With the **New VM** button on the left, you can start creating a VM: 

1. Along with choosing that you want to use your already downloaded `.iso` image, you can also decide if you want to use HVM (**fullvirt**) or PV (**paravirt**) for it at **Architecture options** section, then in **Step 2 of 5**, you need to browse your `.iso` and also specify the OS you are installing, and specify the dedicated RAM and CPUs in **Step 3 of 5**: 

![alt_text](virt-manager_-_new_vm_-_01-02-03.png)

2. Specify the disk image in **Step 4 of 5** (it will be in `qcow2` format under `/var/lib/libvirt/images` if you don't choose otherwise), then finally, **Name** your VM (also the name of your image), select **Network**, then click on **Finish**: 

![alt_text](virt-manager_-_new_vm_-_04-05.png)

3. After finishing, the **New VM** window closes at this phase, but you can access your real VM in the main window of **Virtual Machine Manager** with the second, **Open** icon. You can finalize the installation there, as usual: 

![alt_text](virt-manager_-_new_vm_-_06.png)

### 4.2.1 Creating a VM with `libvirt` on CLI

This is only an old example; it worked once when all the conditions existed: 

```ksh
# virt-install -n xen-01 -r 256 -f /var/lib/xen/images/xen-01.dsk -s 3 --vnc -p -l http://192.168.88.2/centos/5/os/i386/
```

### 4.3 Configuring a VM

In `virt-manager`'s main window, go to **Edit** &rarr; **Virtual Machine Details** &rarr; **Show Virtual Hardware Details** icon (small lightbulb): 

![alt_text](virt-manager_-_vm_virtual_hardware_details.png)

### 4.4 Global Network and Storage details

Both the basic **Network** and the **Storage** details can be configured in **Edit** &rarr; **Connection Details**. 

#### 4.4.1 Network

A `virbr0` virtual interface was automatically created to connect with the host. 

It should be started either manually in `virt-manager` or it can be also activated during boot: 

![alt_text](virt-manager_-_connection_details.png)

##### 4.4.1.1 Accessing the VM through SSH

Once you prepared the VM (`root` login is enabled, `sshd.service` is running and no firewall is blocking it), let's take a quick overview of the `virbr0` interface on the host machine, then login to the VM with `ssh`, e.g.: 

```ksh
$ ip -4 a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
3: wlp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    inet 192.168.101.102/24 brd 192.168.101.255 scope global dynamic noprefixroute wlp3s0
       valid_lft 37371sec preferred_lft 37371sec
6: virbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever

$ ssh root@192.168.122.175
root@192.168.122.175's password: 

[root@rockysrv1 ~]# ip -4 a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: enX0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    inet 192.168.122.175/24 brd 192.168.122.255 scope global dynamic noprefixroute enX0
       valid_lft 3311sec preferred_lft 3311sec

[root@rockysrv1 ~]# dmesg | grep -i virt
[    0.037759] Booting paravirtualized kernel on Xen HVM
[    2.122734] systemd[1]: Detected virtualization xen.
[    2.455329] systemd[1]: Starting Setup Virtual Console...
[    3.709136] xen_netfront: Initialising Xen virtual ethernet driver
[    6.046769] systemd[1]: Detected virtualization xen.
[    8.148467] input: Xen Virtual Keyboard as /devices/virtual/input/input7
[    8.151424] input: Xen Virtual Pointer as /devices/virtual/input/input8
```

<a name="5"></a>
## 5. Managing VMs with `xen-tools`

The `xl` command is the new tool for managing (creating, configuring, destroying, migrating and connecting) Xen objects.  
As mentioned above already, when a domain was created with `libvirt`, then `xl` can only list and check that domain, and not even `xl console` works. To do that, the guest needs to be fully controlled by `xl`. 

### 5.1 Create a VM

The `xl create ...` is the tool for this activity: 

```ksh
# xl create --help
Usage: xl [-vtTfN] create <ConfigFile> [options] [vars]

Create a domain from config file <filename>.

Options:

-h                      Print this help.
-p                      Leave the domain paused after it is created.
-c                      Connect to the console after the domain is created.
-f FILE, --defconfig=FILE
                     Use the given configuration file.
-q, --quiet             Quiet.
-n, --dryrun            Dry run - prints the resulting configuration
                         (deprecated in favour of global -N option).
-d                      Enable debug messages.
-F                      Run in foreground until death of the domain.
-e                      Do not wait in the background for the death of the domain.
-V, --vncviewer         Connect to the VNC display after the domain is created.
-A, --vncviewer-autopass
                        Pass VNC password to viewer via stdin.
--ignore-global-affinity-masks Ignore global masks in xl.conf.
```

Steps to create e.g. a Linux VM, running in HVM mode: 

1. Create the disk image: 

```ksh
# cd /var/lib/xen/images

# qemu-img create -f raw vm-rocky9-01-hvm.img 13G
[...]
```

2. Ensure `virbr0` interface is present or configure `xenbr0`

3. Prepare the config, e.g. a `vm-rocky9-01-hvm.cfg` (example for a HVM guest, with file disk image): 

```
name = "vm-rocky9-01-hvm"
type='hvm'
vcpus=1
memory = 1024
disk = [ 'file:/var/lib/xen/images/vm-rocky9-01-hvm.img,hda,w','file:/var/lib/xen/images/Rocky-9.0-20220805.0-x86_64-minimal.iso,hdc:cdrom,r' ]
vif = ['bridge=virbr0']
vnc=1
vnclisten="0.0.0.0"
vncpasswd=""
```

4. Create the VM and start it with one single command. It takes some minutes: 

```ksh
# xl create vm-rocky9-01-hvm.cfg
[...]
```

If you also used the `-c` parameter for the previous command, you might be surprised that you logged in to the installer as a Live OS: 

```ksh
[anaconda root@localhost /]# lsblk
NAME             MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0              7:0    0 660.9M  1 loop 
loop1              7:1    0     3G  1 loop 
├─live-rw        253:0    0     3G  0 dm   /
└─live-base      253:1    0     3G  1 dm   
loop2              7:2    0    32G  0 loop 
└─live-rw        253:0    0     3G  0 dm   /
sr0               11:0    1   1.4G  0 rom  /run/install/sources/mount-0000-cdrom
                                           /run/install/repo
xvda             202:0    0    13G  0 disk 
├─xvda1          202:1    0   512M  0 part /mnt/sysroot/boot
│                                          /mnt/sysimage/boot
└─xvda2          202:2    0  12.5G  0 part 
  ├─rootvg-swaplv
  │              253:2    0     2G  0 lvm  [SWAP]
  ├─rootvg-rootlv
  │              253:3    0    10G  0 lvm  /mnt/sysroot
  │                                        /mnt/sysimage
  └─rootvg-homelv
                 253:4    0   504M  0 lvm  /mnt/sysroot/home
                                           /mnt/sysimage/home
zram0            252:0    0   952M  0 disk [SWAP]
```

5. In a new terminal, connect to the guest graphically, for installation (also example for connecting via SSH, with X forwarding): 

```ksh
$ ssh -X gjakab@192.168.101.102
Last login: Sat Sep 24 14:41:35 EEST 2022 from 192.168.101.100 on pts/4
Have a lot of fun...

$ vncviewer localhost
TigerVNC Viewer 64-bit v1.12.0
Copyright (C) 1999-2021 TigerVNC Team and many others (see README.rst)
[...]
```

6. Proceed with the installation: 

![alt_text](vncviewer_-_new_vm.png)

7. When the installation finished, do not reboot the system, but close the VNC window and destroy the VM: 

```ksh
# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1801     1     r-----     639.3
Xenstore                                     1    31     1     -b----       0.2
vm-rocky9-01-hvm                             3  1024     1     -b----       0.2

# xl destroy 3
```

8. Modify the installer a bit (remove the `.iso` image): 

```ksh
# cp -p vm-rocky9-01-hvm.cfg vm-rocky9-01-hvm.cfg_install

# vi vm-rocky9-01-hvm.cfg
[...]
disk = [ 'file:/var/lib/xen/images/vm-rocky9-01-hvm.img,hda,w' ]
[...]
```

9. Then start the VM again (also connecting to it with Xen console): 

```ksh
# xl create -c vm-rocky9-01-hvm.cfg
Parsing config from vm-rocky9-01-hvm.cfg
libxl: warning: libxl_dm.c:1220:libxl__suse_node_to_path: replacing '/var/lib/xen/images/vm-rocky9-01-hvm.img' with '/dev/loop0' from /local/domain/0/backend/vbd/6/768/node, just for qemu-xen
[...]
Rocky Linux 9.0 (Blue Onyx)
Kernel 5.14.0-70.13.1.el9_0.x86_64 on an x86_64
[...]
localhost login: root
Password: 
[root@localhost ~]# who am i
root     hvc0         2022-09-24 16:58
```

10. You can also check the IP, create a user or enable root login and connect with SSH.

### 5.2 Using `xen-tools` and `libvirt` VMs simultaneously

It's, of course, possible. Both can run at the same time and both can connect to each other etc.

Overview on such scenario: 

```ksh
# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1801     1     r-----     639.3
Xenstore                                     1    31     1     -b----       0.2
vm-rocky9-01-hvm                             6  1024     1     -b----      23.7
vm-rocky9-02-hvm-libvirt                     8  1536     2     -b----      22.9
```

GUI connections for both of them: 

![alt_text](vncviewer_-_virt-manager_-_consoles.png)

### 5.3 Controlling domains with `xen-tools`

You had some examples of `xl list` output by now. Let's see what letter in **State** column means what: 

- `r` - running: The domain is currently running on a CPU.
- `b` - blocked: The domain is blocked, and not running or runnable. This can be because the domain is waiting on IO (a traditional wait state) or has gone to sleep because there was nothing else for it to do.
- `p` - paused: The domain has been paused, usually occurring through the administrator running xl pause. When in a paused state the domain will still consume allocated resources (like memory), but will not be eligible for scheduling by the Xen hypervisor.
- `s` - shutdown: The guest OS has shut down (SCHEDOP_shutdown has been called) but the domain is not dying yet.
- `c` - crashed: The domain has crashed, which is always a violent ending. Usually this state only occurs if the domain has been configured not to restart on a crash. See xl.cfg(5) for more info.
- `d` - dying: The domain is in the process of dying, but hasn't completely shut down or crashed.

E.g., a just shutting down domain shows `ps` for some moments. 

List of the commonly used options for `xl` command (each of them requires a `<domain ID>`): 

- `console` - Attach to the console of a domain specified by its domain ID (`Ctrl+]` or `Ctrl+u` to detach, theoretically)
- `pause` - Pause a domain. When in a paused state the domain will still consume allocated resources
- `reboot` - Reboot a domain. This acts just as if the domain had the reboot command run from the console.
- `destroy` - Immediately terminate the domain specified by its domain ID. This doesn't give the domain OS any chance to react, and is the equivalent of ripping the power cord out on a physical machine. In most cases you will want to use the shutdown command instead.
- `shutdown` - Gracefully shuts down a domain. This coordinates with the domain OS to perform graceful shutdown, so there is no guarantee that it will succeed. 

### 5.4 Auto-starting Xen guests

The prerequisite for this is of course ensuring the Xen kernel is maybe also boots automatically like this: 

```ksh
# grep menuentry /boot/grub2/grub.cfg | awk -F'--class' '{print $1}'
[...]
menuentry 'openSUSE Tumbleweed'  
[...]
menuentry 'openSUSE Tumbleweed, with Xen hypervisor' 
[...]

# grub2-editenv list
saved_entry=openSUSE Tumbleweed

# grep GRUB_DEFAULT /etc/default/grub
GRUB_DEFAULT=saved

# grub2-set-default "openSUSE Tumbleweed, with Xen hypervisor"

# grub2-editenv list
saved_entry=openSUSE Tumbleweed, with Xen hypervisor
```

#### 5.4.1 Auto-starting Xen guests - the obsolete way

Theoretically, only two steps are left: 

1. Deactivate VM restore (without this, the VM will be started, then destroyed): 

```ksh
# ls -l /etc/default/xendomains
ls: cannot access '/etc/default/xendomains': No such file or directory

# echo "XENDOMAINS_RESTORE=false & save" > /etc/default/xendomains

# cat /etc/default/xendomains
XENDOMAINS_RESTORE=false & save
```

2. Create softlink(s) of your VM config file(s) under `/etc/xen/auto`, e.g.: 

```ksh
# cd /etc/xen/auto
PF0237F2:/etc/xen/auto # ls -la
total 8
drwxr-xr-x 2 root root 4096 Sep  3 03:15 .
drwx------ 6 root root 4096 Sep 19 22:57 ..

# ln -s /root/Xen/Configs/vm-rocky9-01-hvm.cfg

# ls -la
total 8
drwxr-xr-x 2 root root 4096 Sep 24 16:06 .
drwx------ 6 root root 4096 Sep 19 22:57 ..
lrwxrwxrwx 1 root root   38 Sep 24 16:06 vm-rocky9-01-hvm.cfg -> /root/Xen/Configs/vm-rocky9-01-hvm.cfg
```

#### 5.4.2 Auto-starting Xen geusts with `systemd`

There is already a `xendomains.service` for v4.16 on openSUSE, which is disabled and, for some reason, cannot be set up to start automatically, but we may just use as a source:

```ksh
# /usr/lib/xen/bin/xendomains start
Restoring Xen domains: vm-rocky9-01-hvm
  [done] 

# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1801     4     r-----     133.0
Xenstore                                     1    31     1     -b----       0.0
vm-rocky9-01-hvm                             2  1024     1     -b----       0.1
```

Notice, that at least `xencommons.service` is enabled and also running properly, so that should be a prerequisite for sure: 

```ksh
# systemctl list-unit-files --type=service --no-page
UNIT FILE                                      STATE           PRESET  
[...]
xen-dom0-modules.service                       disabled        disabled
xen-init-dom0.service                          disabled        disabled
xen-qemu-dom0-disk-backend.service             disabled        disabled
xen-watchdog.service                           disabled        disabled
xencommons.service                             enabled         enabled 
xenconsoled.service                            disabled        disabled
xendomains.service                             disabled        disabled
xenstored.service                              disabled        disabled
[...]
```

1. Then, let's create a new `/usr/lib/systemd/system/xenautostart.service` file with this contents: 

```
[Unit]
Description=Xenautostart
Requires=xencommons.service
After=xencommons.service

[Service]
Type=oneshot
RemainAfterExit=true
ExecStart=/usr/lib/xen/bin/xendomains start
ExecStop=/usr/lib/xen/bin/xendomains stop
ExecReload=/usr/lib/xen/bin/xendomains restart

[Install]
WantedBy=multi-user.target
```

2. Enable, and start it: 

```ksh
# systemctl enable xenautostart.service
Created symlink /etc/systemd/system/multi-user.target.wants/xenautostart.service → /usr/lib/systemd/system/xenautostart.service.

# systemctl restart xenautostart.service

# systemctl status xenautostart.service
● xenautostart.service - Xenautostart
     Loaded: loaded (/usr/lib/systemd/system/xenautostart.service; enabled; preset: disabled)
     Active: active (exited) since Sat 2022-09-24 19:23:58 EEST; 3s ago
    Process: 3881 ExecStart=/usr/lib/xen/bin/xendomains start (code=exited, status=0/SUCCESS)
   Main PID: 3881 (code=exited, status=0/SUCCESS)
        CPU: 24ms
[...]

# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1801     4     r-----     163.6
Xenstore                                     1    31     1     -b----       0.1
vm-rocky9-01-hvm                             3  1024     1     -b----       0.2
```

3. Reboot and check the outcome again by `xl list` and `systemctl status xenautostart.service`. You may also check how did the auto start go in the correspondent `.log` files under `/var/log/xen`. 


<a name="6"></a>
## 6. Further Storage Considerations

In `virt-manager`'s Connection Details window, there are no so much options to set, it is rather an overview only.

### 6.1 The disk images

When you use `libvirt`, the default way of storing the disk images is the `.qcow` format, and the image creation, conversion, and management activities are part of the `QEMU` layer. 

#### 6.1.1 The default path of disk images

Still remaining at the `libvirt` "way", if you want to change the default location of the big image files of VMs, you need to edit the `/etc/libvirt/storage/default.xml` file through the recommended `virsh pool-edit default` command (which opens a `vim` editor anyway). 

The default path is `/var/lib/libvirt/images`: 

```ksh
# cat storage/default.xml
[...]
  <target>
    <path>/var/lib/libvirt/images</path>
  </target>
[...]
```

The other option is to simply symlink this folder to somewhere else, where there is far enough disk space. 

In case of `xen-tools`, the default place for such images is also under `/var/lib`, under `/var/lib/xen/images`, but in reality, that actually used path is stored in the domain configuration file, which can be anywhere. For simplicity, it is recommended to follow this standard. 


<a name="7"></a>
## 7. Further Network Considerations

In the above examples, the `virbr0` was already created by `libvirt`. But what if we want to use `xen-tools` only, natively? And what if we want to reach the VMs remotely on the network and also want the VMs to reach the internet?

### 7.1 Administering the virtual bridge network

Articles on the subject: 

- [Xen Project's Wiki](https://wiki.xenproject.org/wiki/Network_Configuration_Examples_(Xen_4.1%2B)) about `xenbr0` interface creation
- [XEN, KVM, Libvirt and IPTables](https://cooker.wbitt.com/index.php/XEN,_KVM,_Libvirt_and_IPTables)


### 7.2 Reaching not only the Xen host, but also the VMs remotely

This is might be needed to reach the VMs from another computer but within the same home network. 

IPs and networks: 

- The remote host's IP, connecting to WiFi: `192.168.101.100`
- Xen host's primary IP, connecting to the same WiFi: `192.168.101.102`
- Xen host's `virbr0` IP, (also the gateway): `192.168.122.1`
- The VM's only IP on `enX0` interface: `192.168.122.128`

#### 7.2.1 Static routes

1. On the **remote host**, e.g.: 

```ksh
# ip route add 192.168.122.128 via 192.168.101.100
```

2. On the **VM**, e.g.:

```ksh
# ip route add 192.168.101.0/24 via 192.168.122.1

# ip route
default via 192.168.122.1 dev enX0 proto dhcp metric 100 
192.168.101.0/24 via 192.168.122.1 dev enX0 
192.168.122.0/24 dev enX0 proto kernel scope link src 192.168.122.128 metric 100 
```

3. **Test** on the remote host: 

```ksh
$ ping 192.168.122.128
PING 192.168.122.128 (192.168.122.128) 56(84) bytes of data.
64 bytes from 192.168.122.128: icmp_seq=1 ttl=63 time=3.01 ms
64 bytes from 192.168.122.128: icmp_seq=2 ttl=63 time=2.47 ms
^C
[...]
```

4. If the above was successful as in the example, you may configure it also **permanently** with `nmcli`, e.g.:

```ksh
# nmcli c
NAME  UUID                                  TYPE      DEVICE 
enX0  bb702f81-ea50-3178-9033-5e8252da8e7b  ethernet  enX0   

# nmcli con edit enX0
[...]
nmcli> set ipv4.routes 192.168.101.0/24 192.168.122.1
nmcli> save persistent
Connection 'enX0' (bb702f81-ea50-3178-9033-5e8252da8e7b) successfully updated.
nmcli> quit
```

#### 7.2.2 `iptables` adjustment

The Xen host gets several `iptables` forward rules from `libvirt`'s setup, so the VMs can access the internet and you can access the VMs from the host too e.g. through SSH. 

The typical Home Lab setup, however, requires further adjustments in `iptables` to get the above connection still working. 

>For now, I just accepted that when I want the VMs to be connected to the internet, then I leave `firewalld.service` running on the Dom0 and when it's not needed, and I want to access it in the internal network, then I stop it. 


<a name="8"></a>
## 8. The XCP-ng Xen Virtualization Management Platform

The `libvirt` and `virt-manager` together is already a representation of a useful GUI toolset for Xen, but with limited capabilities and still letting too much configuration work for the administrator. 

**XCP-ng** is much more than a GUI: it is a Linux distribution, shipped with all the packages, tools and preconfigurations that are needed to build an enterprise-level, Xen Project-based virtualization solution, with a Web GUI, represented by **Xen Orchestra**. 

### 8.1 History of XCP-ng, shortly

<img src="xcp-ng_-_logo.png" class="right">

**XCP** itself stands for the **Xen Cloud Platform**, which was a *"Turnkey Server Virtualization"* solution that supplied out-of-the box virtualization and cloud computing until **2013**. This was the year, when Citrix announced that XenServer will be open sourced, so continuing with XCP became pointless and discontinued. On December **2017**, Citrix announced that they would remove important features of XenServer Free edition and make them only available on paid tiers. In response, Olivier Lambert, the original founder of Xen Orchestra announced that he would revive the XCP project as XCP-ng (same abbreviation, but also "**next generation**"). 

The Xen Orchestra GUI can be basically considered as a cheaper alternative to administer a Citrix Hypervisor (XenServer) environment. Originally, XCP's XCP-XAPI Packages could be also installed via standard `.deb` packages on Debian and Ubuntu distributions or could be downloaded simply as an `.iso` file, a custom Linux distribution, as a preconfigured, complete solution. XCP-ng followed this complete solution approach in 2017. Also, XCP-ng uses the exact same API as XCP used, and also uses pretty much the same Xen Orchestra. 
More info on XCP's origins is in [Xen Project's Wiki](https://wiki.xenproject.org/wiki/XCP_Overview).

As of September 2022, the current version of XCP-ng is 8.2.1, and its base OS is a CentOS 7 with Xen 4.13. 

📌 **Using Xen Orchestra is free but it has limitations. For comparison and pricing, check their comparison page at [xen-orchestra.com](https://xen-orchestra.com/#!/xo-pricing).**

### 8.2 Installation, accessing the Web GUI

📌 **Please note that this is not a complete manual either for XCP-ng or Xen Orchestra. It only demonstrates some basic features and guides you through on some basic tasks.**

After you downloaded the current installation `.iso` image from XCP-ng's website, you just need to create a bootable USB flashdrive. E.g.:

```ksh
# dd if=xcp-ng-8.2.1.iso of=/dev/sda bs=4M conv=fdatasync
[...]
```

The installer first welcomes you, then guides you through a wizard, with some simple questions. This step, however, about storage usage can be tricky. This time, I chose LVM, but it's up to you: 

![alt_text](xcp-ng_-_install_-_01.jpg)

After basic network configuration and setting up the `root` password, the installation finishes in some more minutes: 

![alt_text](xcp-ng_-_install_-_02.jpg)

After reboot, it (the Dom0) boots up to a CLI menu with some general details and low level settings: 

![alt_text](xcp-ng_-_install_-_03.jpg)

The IP that you can see on this CLI dashboard can be already used as a `https://` address. Connect to it from another device and click on **Quick Deploy**. 

### 8.3 Xen Orchestra deployment

Fist, it asks for your `root` password that you set during the installation: 

![alt_text](xcp-ng_-_xen_orchestra_-_deploy_-_01.png)

If you would just continue using DHCP, you can leave everything as is, except you need to specify the Admin account for XOA. In this case, it is `xoadmin`: 

![alt_text](xcp-ng_-_xen_orchestra_-_deploy_-_02-03-04.png)

After you logged in to XOA, the main page welcomes you, where you can notice that a VM is already running, directly for hosting this Web GUI: 

![alt_text](xcp-ng_-_xen_orchestra_-_deploy_-_05.png)

### 8.4 Prepare a Storage for installers

You can transfer your `.iso` installer image(s) simply by `scp`. 

The already prepared Storage for similar images is "XCP-ng Tools", and its path is `/opt/xensource/packages/iso`. 

As it is only under `/` by default, you can also create a new Storage yourself, e.g. when you created a bigger LV and an FS under it, and also mounted it: 

![alt_text](xcp-ng_-_xen_orchestra_-_new_storage_-_01.png)

In this example, the path of this storage is `/isostore`: 

![alt_text](xcp-ng_-_xen_orchestra_-_new_storage_-_02.png)

Once you uploaded your `.iso`, it will be detected (or you can also use the **Refresh** button): 

![alt_text](xcp-ng_-_xen_orchestra_-_new_storage_-_03.png)

### 8.5 Prepare Network

By default, there are two network types available: 

- **Pool-wide network associated with eth0**: it means, your VM will get into your home network, like you simply connected another device at home. The VM's IP will be available remotely, from e.g. another laptop too, directly. It means better reachability but less security, and Xen offers this type by default for new VMs. 
- **Host internal management network**: when it is chosen for a VM, a `xenapi` virtual interface will be created on the Dom0 with an internal IP (typically `169.254.0.1/16`, which is the GW too), and the VM will get an IP from that range. It means better security but limited reachability (additional `ip route` and/or firewall config is needed if you want to reach such VM remotely). 

Xen uses **Open vSwitch** by default. It is a production quality, multilayer virtual switch licensed under the open source Apache 2.0 license. It is designed to enable massive network automation through programmatic extension, while still supporting standard management interfaces and protocols. 

The other mode is the **Bridge** one. You can check which one is in use: 

```ksh
# cat /etc/xensource/network.conf
openvswitch
```

It is possible to switch the whole network management to **Bridge** mode, but that requires rebooting the host (taking care of VMs is also recommended): 

```ksh
# xe-switch-network-backend bridge
Disabling openvswitch daemon
Removed symlink /etc/systemd/system/multi-user.target.wants/openvswitch-xapi-sync.service.
Removed symlink /etc/systemd/system/multi-user.target.wants/openvswitch.service.
Configure system for bridge networking
You *MUST* now reboot your system
```

### 8.6 Create and install a new VM

1. On **VMs** page (**Home** --> **VMs**), click on **New VM** button in the top right corner --> then **Select pool** (you will have only one in the beginning): 

![alt_text](xcp-ng_-_xen_orchestra_-_new_vm_-_01.png)

2. Select a **Template**, that will prefill everything, but you may make your adjustments as you like, e.g.:

![alt_text](xcp-ng_-_xen_orchestra_-_new_vm_-_02.png)

📌 It's good to note that even though you can set lower values for vCPU and RAM, but **there are minimum values** that are not visible here. For such cases, it's maybe better to select another template or not use template at all. 

3. Maybe it's better to switch off the boot from Hard Drive for now at the **Boot order** section: 

![alt_text](xcp-ng_-_xen_orchestra_-_new_vm_-_03.png)

4. Proceed with the installation as usual: 

![alt_text](xcp-ng_-_xen_orchestra_-_new_vm_-_04.png)

5. After successful installation, notice that your VM's virtualization mode switched to PVHVM as that's supported by the VM's OS: 

![alt_text](xcp-ng_-_xen_orchestra_-_new_vm_-_05.png)

And here is the **Dashboard**, when you have 4 VMs already: 

![alt_text](xcp-ng_-_xen_orchestra_-_dashboard.png)

Each of them got a new LV too: 

```ksh
# lvs
  LV                                       VG                                                 Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  MGT                                      VG_XenStorage-738ce638-6511-caff-fae4-0bffe7954055 -wi-a-----   4.00m                                                    
  VHD-310f22c7-a818-4fa3-a241-f3773ef8cad2 VG_XenStorage-738ce638-6511-caff-fae4-0bffe7954055 -wi------- <10.03g                                                    
  VHD-bf546867-04ae-475e-81bd-d2dd77da384c VG_XenStorage-738ce638-6511-caff-fae4-0bffe7954055 -wi-ao---- <20.05g                                                    
  VHD-d1583a4c-29d3-48ef-b70c-98ec3984e50d VG_XenStorage-738ce638-6511-caff-fae4-0bffe7954055 -wi-ao----  13.03g                                                    
  VHD-f4366a86-2a2c-48f1-a060-5d30ef7e168a VG_XenStorage-738ce638-6511-caff-fae4-0bffe7954055 -wi------- <10.03g
```

### 8.7 Cloning VMs

Should you want to clone an existing VM, you can either use the **Copy VM** feature: 

![alt_text](xcp-ng_-_xen_orchestra_-_copy_vm.png)

...or you can use the **Fast clone** button which basically does the same, and puts a `-clone` at the end of the VMs name. 

You can check the progress in the **Tasks** page.

### 8.8 Maintenance and reboot of host

You can either do maintenance or reboot your host. When it is rebooted, the `XOA` VM **won't start automatically by default** (it can be modified), so you need to start it manually first. 

It can be done...

- either by `xe` command, in an `ssh` session: 

```ksh
# xe vm-start vm="XOA"

# xl list
Name                                        ID   Mem VCPUs	State	Time(s)
Domain-0                                     0  1424     4     r-----     100.5
XOA                                          1  2048     1     r-----       3.7
```

- or you can do it directly on your XCP-ng console: 

![alt_text](xcp-ng_-_vms.jpg)

Once started, you can reach the Xen Orchestra Web GUI again. 

Finally, it's also a safe method to shut the VMs down first when you reboot or shutdown the server. 

Again, the above was just a quick walkthrough, not a complete manual. We just scratched the surface. You can browse the whole [XCP-ng documentation](https://xcp-ng.org/docs/) for details, even for guidelines. 