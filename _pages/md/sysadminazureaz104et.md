# ![alt text](/_ui/img/icons/azure_m.svg) Azure AZ-104 Practice Questions - ExamTopics

<div style="text-align: right;"><h7>Posted: 2024-10-26 13:53</h7></div>

###### .

These contents are extracted from the unofficial [ExamTopics's AZ-104 directory](https://www.examtopics.com/exams/microsoft/az-104/).


## Sorted list of questions mainly focusing on...

1. [Azure Identities and Governance, including ARM templates](#1)
2. [Azure Networking](#2)
3. [Azure Storage](#3)
4. [Azure Virtual Machines](#4)
5. [Azure PaaS Compute Options](#5)
6. [Azure Backup and Data Protection](#6)
7. [Azure Monitoring and Logging](#7)


<a name="1"></a>
## 1. Azure Identities and Governance, including ARM templates

---

<div>
  <p><span class="k">1.</span>Your company has an Azure Active Directory (Azure AD) subscription.</p>
  <p>You want to implement an Azure AD conditional access policy.</p>
  <p>The policy must be configured to require members of the Global Administrators group to use Multi-Factor Authentication and an Azure AD-joined device when they connect to Azure AD from untrusted locations.</p>
  <p><b>Solution</b>: You access the multi-factor authentication page to alter <b>the user settings</b>.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>Your company has an Azure Active Directory (Azure AD) subscription.</p>
  <p>You want to implement an Azure AD conditional access policy.</p>
  <p>The policy must be configured to require members of the Global Administrators group to use Multi-Factor Authentication and an Azure AD-joined device when they connect to Azure AD from untrusted locations.</p>
  <p><b>Solution</b>: You access the Azure portal to alter <b>the session control of the Azure AD conditional access policy</b>.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (88%)</p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>Your company has an Azure Active Directory (Azure AD) subscription.</p>
  <p>You want to implement an Azure AD conditional access policy.</p>
  <p>The policy must be configured to require members of the Global Administrators group to use Multi-Factor Authentication and an Azure AD-joined device when they connect to Azure AD from untrusted locations.</p>
  <p><b>Solution</b>: You access the Azure portal to alter <b>the grant control of the Azure AD conditional access policy</b>.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"A"</b> (93%)</p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>Your company makes use of Multi-Factor Authentication for when users are not in the office. The Per Authentication option has been configured as the usage model.</p>
  <p>After the acquisition of a smaller business and the addition of the new staff to Azure Active Directory (Azure AD) obtains a different company and adding the new employees to Azure Active Directory (Azure AD), you are informed that these employees should also make use of Multi-Factor Authentication.</p>
  <p>To achieve this, the Per Enabled User setting must be set for the usage model.</p>
  <p><b>Solution</b>: You reconfigure the existing usage model via the Azure portal.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (95%)</p>
    <p><i>Since it is not possible to change the usage model of an existing provider as it is right now, you have to create a new one and reactivate your existing server with activation credentials from the new provider.</i></p>
    <p><i>Reference: https://365lab.net/2015/04/11/switch-usage-model-in-azure-multi-factor-authentication-server/</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>Your company's Azure solution makes use of Multi-Factor Authentication for when users are not in the office. The Per Authentication option has been configured as the usage model.</p>
  <p>After the acquisition of a smaller business and the addition of the new staff to Azure Active Directory (Azure AD) obtains a different company and adding the new employees to Azure Active Directory (Azure AD), you are informed that these employees should also make use of Multi-Factor Authentication.</p>
  <p>To achieve this, the Per Enabled User setting must be set for the usage model.</p>
  <p><b>Solution</b>: You reconfigure the existing usage model via the Azure CLI.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (82%)</p>
    <p><i>Since it is not possible to change the usage model of an existing provider as it is right now, you have to create a new one and reactivate your existing server with activation credentials from the new provider.</i></p>
    <p><i>Reference: https://365lab.net/2015/04/11/switch-usage-model-in-azure-multi-factor-authentication-server/</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>Your company's Azure solution makes use of Multi-Factor Authentication for when users are not in the office. The Per Authentication option has been configured as the usage model.</p>
  <p>After the acquisition of a smaller business and the addition of the new staff to Azure Active Directory (Azure AD) obtains a different company and adding the new employees to Azure Active Directory (Azure AD), you are informed that these employees should also make use of Multi-Factor Authentication.</p>
  <p>To achieve this, the Per Enabled User setting must be set for the usage model.</p>
  <p><b>Solution</b>: You create a new Multi-Factor Authentication provider with a backup from the existing Multi-Factor Authentication provider data.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (85%)</p>
    <p><i>Creating a new Multi-Factor Authentication provider with a backup from the existing provider data does not change the usage model from Per Authentication to Per Enabled User. You would need to specifically change the Multi-Factor Authentication settings to the Per Enabled User model to meet the requirements.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>Your company has an Azure Active Directory (Azure AD) tenant named weyland.com that is configured for hybrid coexistence with the on-premises Active
Directory domain.</p>
  <p>You have a server named DirSync1 that is configured as a DirSync server.</p>
  <p>You create a new user account in the on-premise Active Directory. You now need to replicate the user information to Azure AD immediately.</p>
  <p><b>Solution</b>: You run the <code>Start-ADSyncSyncCycle -PolicyType Initial</code> PowerShell cmdlet.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (72%)</p>
    <p><i><code>Initial</code> will perform a full sync and add the user account created but it will take time, 
<code>Delta</code>, will kick off a delta sync and bring only the last change, so it will be "immediately" and will fulfill the requirements.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span>Your company has an Azure Active Directory (Azure AD) tenant named weyland.com that is configured for hybrid coexistence with the on-premises Active
Directory domain.</p>
  <p>You have a server named DirSync1 that is configured as a DirSync server.</p>
  <p>You create a new user account in the on-premise Active Directory. You now need to replicate the user information to Azure AD immediately.</p>
  <p><b>Solution</b>: You use Active Directory Sites and Services to force replication of the Global Catalog on a domain controller.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
    <p><i>On a server with Azure AD Connect installed, navigate to the Start menu and select AD Connect, then Synchronization Service. Then go to CONNECTORS tab, and then select RUN on the ACTIONS pane.<br>
    You can also use the <code>Start-ADSyncSyncCycle -PolicyType Delta</code> PowerShell cmdlet to achieve the same.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">9.</span>Your company has an Azure Active Directory (Azure AD) tenant named weyland.com that is configured for hybrid coexistence with the on-premises Active
Directory domain.</p>
  <p>You have a server named DirSync1 that is configured as a DirSync server.</p>
  <p>You create a new user account in the on-premise Active Directory. You now need to replicate the user information to Azure AD immediately.</p>
  <p><b>Solution</b>: You restart the NetLogon service on a domain controller.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
    <p><i>On a server with Azure AD Connect installed, navigate to the Start menu and select AD Connect, then Synchronization Service. Then go to CONNECTORS tab, and then select RUN on the ACTIONS pane.<br>
    You can also use the <code>Start-ADSyncSyncCycle -PolicyType Delta</code> PowerShell cmdlet to achieve the same.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">10.</span>You have downloaded an Azure Resource Manager (ARM) template to deploy numerous virtual machines (VMs). The ARM template is based on a current VM, but must be adapted to reference an administrative password.</p>
  <p>You need to make sure that the password cannot be stored in plain text.</p>
  <p>You are preparing to create the necessary components to achieve your goal.</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Which of the following should you create to achieve your goal?</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>An Azure Key Vault</li>
    <li>An Azure Storage Account</li>
    <li>Azure Active Directory (AD) Identity Protection</li>
    <li>An Access Policy</li>
    <li>An Azure Policy</li>
    <li>A Backup Policy</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"A" and "D"</b> (100%)</p>
    <p><i>You can use a template that allows you to deploy a simple Windows VM by retrieving the password that is stored in a Key Vault. Therefore, the password is never put in plain text in the template parameter file. (Note: now the access policy is considered a legacy way to provide access to the key vault. Now you can use RBAC.)</i></p>
  </div>
</div>

---




<a name="2"></a>
## 2. Azure Networking



<a name="3"></a>
## 3. Azure Storage

---

<div>
  <p><span class="k">1.</span>Your company has a Microsoft Azure subscription.</p>
  <p>The company has datacenters in Los Angeles and New York.</p>
  <p>You are configuring the two datacenters as geo-clustered sites for site resiliency.</p>
  <p>You need to recommend an Azure storage redundancy option.</p>
  <p>You have the following data storage requirements:</p>
  <ul>
    <li>Data must be stored on multiple nodes.</li>
    <li>Data must be stored on nodes in separate geographic locations.</li>
    <li>Data can be read from the secondary location as well as from the primary location.</li>
  </ul>
  <p><b>Which of the following Azure stored redundancy options should you recommend?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Geo-redundant storage</li>
    <li>Read-only geo-redundant storage</li>
    <li>Zone-redundant storage</li>
    <li>Locally redundant storage</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (88%)</p>
    <p><i>RA-GRS is based on the GRS, but it also provides an option to read from the secondary region, regardless of whether Microsoft initiates a failover from the primary to the secondary region.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>Your company has an azure subscription that includes a storage account, a resource group, a blob container and a file share.</p>
  <p>A colleague named Jon Ross makes use of a solitary Azure Resource Manager (ARM) template to deploy a virtual machine and an additional Azure Storage account.</p>
  <p>You want to review the ARM template that was used by Jon Ross.</p>
  <p><b>Solution</b>: You access the Virtual Machine blade.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>Your company has an azure subscription that includes a storage account, a resource group, a blob container and a file share.</p>
  <p>A colleague named Jon Ross makes use of a solitary Azure Resource Manager (ARM) template to deploy a virtual machine and an additional Azure Storage account.</p>
  <p>You want to review the ARM template that was used by Jon Ross.</p>
  <p><b>Solution</b>: You access the Resource Group blade.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"A"</b> (100%)</p>
    <p><i>In the Resource Group blade, you can select the resource group where the virtual machine and additional storage account were deployed, and then click on the "Deployments" tab. This will display a list of all deployments made to the resource group, including the ARM template used for the deployment.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>Your company has an azure subscription that includes a storage account, a resource group, a blob container and a file share.</p>
  <p>A colleague named Jon Ross makes use of a solitary Azure Resource Manager (ARM) template to deploy a virtual machine and an additional Azure Storage account.</p>
  <p>You want to review the ARM template that was used by Jon Ross.</p>
  <p><b>Solution</b>: You access the Container blade.</p>
  <p><b>Does the solution meet the goal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Yes</li>
    <li>No</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
  </div>
</div>

---



<a name="4"></a>
## 4. Azure Virtual Machines

---

<div>
  <p><span class="k">1.</span>Your company has serval departments. Each department has a number of virtual machines (VMs).</p>
  <p>The company has an Azure subscription that contains a resource group named RG1.</p>
  <p>All VMs are located in RG1.</p>
  <p>You want to associate each VM with its respective department.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Create Azure Management Groups for each department.</li>
    <li>Create a resource group for each department.</li>
    <li>Assign tags to the virtual machines.</li>
    <li>Modify the settings of the virtual machines.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"C"</b> (100%)</p>
    <p><i>Reference: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-using-tags</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You are planning to deploy an Ubuntu Server virtual machine to your company's Azure subscription.</p>
  <p>You are required to implement a custom deployment that includes adding a particular trusted root certification authority (CA).</p>
  <p><b>Which of the following should you use to create the virtual machine?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>The <code>New-AzureRmVm</code> cmdlet.</li>
    <li>The <code>New-AzVM</code> cmdlet.</li>
    <li>The <code>Create-AzVM</code> cmdlet.</li>
    <li>The <code>az vm create</code> command.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"D"</b> (99%)</p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>Your company has three virtual machines (VMs) that are included in an availability set.</p>
  <p>You try to resize one of the VMs, which returns an allocation failure message.</p>
  <p>It is imperative that the VM is resized.</p>
  <p><b>Which of the following actions should you take?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>You should only stop one of the VMs.</li>
    <li>You should stop two of the VMs.</li>
    <li>You should stop all three VMs.</li>
    <li>You should remove the necessary VM from the availability set.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"C"</b> (86%)</p>
    <p><i>If your VM(s) are deployed using the Resource Manager (ARM) deployment model and you need to change to a size which requires different hardware then you can resize VMs by first stopping your VM, selecting a new VM size and then restarting the VM. If the VM you wish to resize is part of an availability set, then you must stop all VMs in the availability set before changing the size of any VM in the availability set. The reason all VMs in the availability set must be stopped before performing the resize operation to a size that requires different hardware is that all running VMs in the availability set must be using the same physical hardware cluster. Therefore, if a change of physical hardware cluster is required to change the VM size then all VMs must be first stopped and then restarted one-by-one to a different physical hardware clusters.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span> You have an Azure virtual machine (VM) that has a single data disk. You have been tasked with attaching this data disk to another Azure VM.</p>
  <p>You need to make sure that your strategy allows for the virtual machines to be offline for the least amount of time possible.</p>
  <p><b>Which of the following is the action you should take FIRST?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Stop the VM that includes the data disk.</li>
    <li>Stop the VM that the data disk must be attached to.</li>
    <li>Detach the data disk.</li>
    <li>Delete the VM that includes the data disk.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"C"</b> (81%)</p>
    <p><i>You can simply detach a data disk from one VM and attach it to the other VM without stopping either of the VMs.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span> Your company has an Azure subscription.</p>
  <p>You need to deploy a number of Azure virtual machines (VMs) using Azure Resource Manager (ARM) templates. You have been informed that the VMs will be included in a single availability set.</p>
  <p>You are required to make sure that the ARM template you configure allows for as many VMs as possible to remain accessible in the event of fabric failure or maintenance.</p>
  <p><b>Which of the following is the value that you should configure for the <code>platformFaultDomainCount</code> property?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>10</li>
    <li>30</li>
    <li>Min Value</li>
    <li>Max Value</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"D"</b> (100%)</p>
    <p><i>2 or 3 is max for a region so answer should be Max.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>Your company has an Azure subscription.</p>
  <p>You need to deploy a number of Azure virtual machines (VMs) using Azure Resource Manager (ARM) templates. You have been informed that the VMs will be included in a single availability set.</p>
  <p>You are required to make sure that the ARM template you configure allows for as many VMs as possible to remain accessible in the event of fabric failure or maintenance.</p>
  <p><b>Which of the following is the value that you should configure for the <code>platformUpdateDomainCount</code> property?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>10</li>
    <li>20</li>
    <li>30</li>
    <li>40</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (100%)</p>
    <p><i>Each virtual machine in your availability set is assigned an update domain and a fault domain by the underlying Azure platform. Each availability set can be configured with up to three fault domains and twenty update domains.<br>
    Ref: https://docs.microsoft.com/en-us/azure/virtual-machines/availability-set-overview</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>Your company has an Azure Active Directory (Azure AD) tenant that is configured for hybrid coexistence with the on-premises Active Directory domain.</p>
  <p>The on-premise virtual environment consists of virtual machines (VMs) running on Windows Server 2012 R2 Hyper-V host servers.</p>
  <p>You have created some PowerShell scripts to automate the configuration of newly created VMs. You plan to create several new VMs.</p>
  <p>You need a solution that ensures the scripts are run on the new VMs.</p>
  <p><b>Which of the following is the best solution?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Configure a <code>SetupComplete.cmd</code> batch file in the <code>%windir%\setup\scripts</code> directory.</li>
    <li>Configure a Group Policy Object (GPO) to run the scripts as logon scripts.</li>
    <li>Configure a Group Policy Object (GPO) to run the scripts as startup scripts.</li>
    <li>Place the scripts in a new virtual hard disk (VHD).</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"A"</b> (52%)</p>
    <p><i>After Windows is installed but before the logon screen appears, Windows Setup searches for the <code>SetupComplete.cmd</code> file in the <code>%WINDIR%\Setup\Scripts\</code> directory.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span> Your company has an Azure Active Directory (Azure AD) tenant that is configured for hybrid coexistence with the on-premises Active Directory domain.</p>
  <p>You plan to deploy several new virtual machines (VMs) in Azure. The VMs will have the same operating system and custom software requirements.</p>
  <p>You configure a reference VM in the on-premise virtual environment. You then generalize the VM to create an image.</p>
  <p>You need to upload the image to Azure to ensure that it is available for selection when you create the new Azure VMs.</p>
  <p><b>Which PowerShell cmdlets should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li><code>Add-AzVM</code></li>
    <li><code>Add-AzVhd</code></li>
    <li><code>Add-AzImage</code></li>
    <li><code>Add-AzImageDataDisk</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"B"</b> (92%)</p>
    <p><i><code>New-AzVM</code> is for creating new VMs, not uploading images.<br>
    <code>Add-AzImage</code> does not exist. the correct command is "New-AzImage".<br>
    <code>Add-AzImageDataDisk</code> Adds a data disk to an image object.<br>
    <code>Add-AzVhd</code> seems to be the correct option, since the it "Uploads a virtual hard disk from an on-premises machine to Azure (managed disk or blob)."<br>
    https://docs.microsoft.com/en-us/powershell/module/az.compute/add-azvhd?view=azps-8.3.0</i></p>
  </div>
</div>

---



<a name="5"></a>
## 5. Azure PaaS Compute Options



<a name="6"></a>
## 6. Azure Backup and Data Protection



<a name="7"></a>
## 7. Azure Monitoring and Logging




## Unorganized / Empty template


<!-- Question #23 (page 3) is next! -->

<!--

  <p>Select only one answer.</p>

  <p>Select all answers that apply.</p>

-->

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">NUM.</span>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p>asdf</p>
  <p><b>Solution:</b>: optional_solution_offering</p>
  <p><b>question_in_bold</b></p>
  <p>way_to_select_solution</p>
  <ol type="A">
    <li>AAAA</li>
    <li>BBBB</li>
    <li>CCCC</li>
    <li>DDDD</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="solution">
    <p>The answer is <b>"LETTER"</b> (PERCENT%)</p>
    <p><i>explanation_if_any</i></p>
  </div>
</div>

---

