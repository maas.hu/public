# ![alt text](/_ui/img/flags/fin_m.svg) Finnish Verbs, Types, Conjugation of them

<div style="text-align: right;"><h7>Posted: 2022-11-04 20:10</h7></div>

###### .

<div class="info">
<p>The below list is constantly growing, and it may contain mistakes/typos. Don't take it as a source of truth!.</p>
</div>

<!-- https://fi.bab.la/taivutus/suomi/haluta -->

## Legend

- (`[1..6]`): **Verbityyppi** - the type of the verb
- `P`: **Preesens** - present tense, declarative case
- `PN`: **Preesens, negatiivinen** - present tense, negation
- `K`: **Kysymys** - forming question, directly from the verb
- `I`: **Imperfekti** - past tense
- `NI`: **Imperfekti, negatiivinen** - past tense, negation; same as the conjugation for **Perfekti** and its negation, **Pluskvamperfekti** and its negation, **Potentiaali in perfekti** and its negation, **Konditionaali in perfekti** and its negation
- `PA`: **Passiivi** - the 4 main passive forms of the verb (`P`, `PN`, `I`, `NI`)

## Table of Contents

1. [A to E](#a-e)
2. [F to I](#f-i)
3. [J to K](#j-k)
4. [L to N](#l-n)
5. [O to R](#o-r)
6. [S to Ö](#s-ö)


<a name="a-e"></a>
## A to E

- **ajaa** (`1`) - vezetni, irányítani (*to drive*, *to lead*)  
  `P`: ajan, ajat, ajaa, ajamme, ajatte, ajavat  
  `PN`: en aja, et aja...  
  `K`: Ajanko? Ajatko?...  
  `I`: ajoin, ajoit, ajoi, ajoimme, ajoitte, ajoivat  
  `NI`: en/et/ei ajanut, emme/ette/eivät ajaneet  
  `PA`: ajetaan, ei ajeta, ajettiin, ei ajettu  
- **ajattella** (`3`) - gondolni (*to think*)  
  `P`: ajattelen, ajattelet, ajattelee, ajattelemme, ajattelette, ajattelevat  
  `PN`: en ajattele, et ajattele...  
  `K`: Ajattelenko? Ajatteletko?...  
  `I`: ajattelin, ajattelit, ajatteli, ajattelimme, ajattelitte, ajattelivat  
  `NI`: en/et/ei ajatellut, emme/ette/eivät ajatelleet  
  `PA`: ajatellaan, ei ajatella, ajateltiin, ei ajateltu  
- **alkaa** (`1`) - kezdődni (*to begin*)  
  `P`: alan, alat, alkaa, alamme, alatte, alkavat  
  `PN`: en ala, et ala...  
  `K`: Alanko? Alatko?...  
  `I`: aloin, aloit, alkoi, aloimme, aloitte, alkoivat  
  `NI`: en/et/ei alkanut, emme/ette/eivät alkaneet  
  `PA`: aletaan, ei aleta, alettiin, ei alettu
- **aloittaa** (`1`) - kezdeni (*to start, to initiate*)  
  `P`: aloitan, aloitat, aloittaa, aloitamme, aloitatte, aloittavat  
  `PN`: en aloita, et aloita...  
  `K`: Aloitanko? Aloitatko?...  
  `I`: aloitin, aloitit, aloitti, aloitimme, aloititte, aloittivat  
  `NI`: en/et/ei aloittanut, emme/ette/eivät aloittaneet  
  `PA`: aloitetaan, ei aloiteta, aloitettiin, ei aloitettu  
- **aloittua** (`1`) - elkezdődni (*to begin, to start itself*)  
  `P`: aloituu, aloittuvat  
  `PN`: ei aloitu, eivät aloitu...  
  `K`: Aloittuuko? Aloittuvatko?...  
  `I`: alkoi, alkoivat  
  `NI`: ei/ eivät alkanut  
  `PA`: aloitetaan, ei aloiteta, aloitettiin, ei aloitettu
- **ammua** (`1`) - bőgni (*to moo, to low (cattle)*)  
  `P`: ammun, ammuu, ammumme, ammutte, ammuavat  
  `PN`: en ammu, et ammu...  
  `K`: Ammunko? Ammutko?...  
  `I`: ammuin, ammuimme, ammuivat  
  `NI`: en/et/ei ammunut, emme/ette/eivät ammunneet  
  `PA`: ammutaan, ei ammuta, ammuttiin, ei ammuttu
- **ampua** (`1`) - lőni (*to shoot*)  
  `P`: ammun, ammut, ampuu, ammumme, ammutte, ampuvat  
  `PN`: en ammu, et ammu...  
  `K`: Ammunko? Ammutko?...  
  `I`: ammuin, ammuit, ampui, ammuimme, ammuitte, ampuivat  
  `NI`: en/et/ei ampunut, emme/ette/eivät ampuneet  
  `PA`: ammutaan, ei ammuta, ammuttiin, ei ammuttu  
- **antaa** (`1`) - adni (*to give*)  
  `P`: annan, annat, antaa, annamme, annatte, antavat  
  `PN`: en anna, et anna...  
  `K`: Annanko? Annatko?...  
  `I`: annoin, annoit, antoi, annoimme, annoitte, antoivat  
  `NI`: en/et/ei antanut, emme/ette/eivät antaneet  
  `PA`: annetaan, ei anneta, annettiin, ei annettu  
- **antaa anteeksi** (`1`) - megbocsátani (*to forgive*)  
  `P`: annan anteeksi, antaa anteeksi, annamme anteeksi...  
  `PN`: en anna anteeksi, et anna anteeksi...  
  `K`: Annanko anteeksi? Annatteko anteeksi?...  
  `I`: annoin anteeksi, antoi anteeksi, annoimme anteeksi...  
  `NI`: en/et/ei antanut anteeksi, emme/ette/eivät antaneet anteeksi  
  `PA`: annetaan anteeksi, ei anneta anteeksi, annettiin anteeksi, ei annettu anteeksi
- **arvata** (`4`) - kitalálni (*to guess*)  
  `P`: arvaan, arvaat, arvaa, arvaamme, arvaatte, arvaavat  
  `PN`: en arvaa, et arvaa...  
  `K`: Arvaanko? Arvaatko?...  
  `I`: arvasin, arvasit, arvasi, arvasimme, arvasitte, arvasivat  
  `NI`: en/et/ei arvannut, emme/ette/eivät arvanneet  
  `PA`: arvataan, ei arvata, arvattiin, ei arvattu  
- **arvioida** (`2`) - becsülni, értékelni (*to estimate, to evaluate*)  
  `P`: arvioin, arvioit, arvioi, arvioimme, arvioitte, arvioivat  
  `PN`: en arvioi, et arvioi...  
  `K`: Arvioinko? Arvioitteko?...  
  `I`: arvioin, arvioit, arvioi, arvioimme, arvioivat  
  `NI`: en/et/ei arvioinut, emme/ette/eivät arvioineet  
  `PA`: arvioidaan, ei arvioida, arvioitiin, ei arvioitu
- **arvostaa** (`1`) - értékelni, tisztelni (*to appreciate, to respect*)  
  `P`: arvostan, arvostat, arvostaa, arvostamme, arvostatte, arvostavat  
  `PN`: en arvosta, et arvosta...  
  `K`: Arvostanko? Arvostatko?...  
  `I`: arvostin, arvostit, arvosti, arvostimme, arvostitte, arvostivat  
  `NI`: en/et/ei arvostanut, emme/ette/eivät arvostaneet  
  `PA`: arvostetaan, ei arvosteta, arvostettiin, ei arvostettu
- **astua** (`1`) - lépni (*to step*)  
  `P`: astun, astut, astuu, astumme, astutte, astuvat  
  `PN`: en astu, et astu...  
  `K`: Astunko? Astutko?...  
  `I`: astuin, astuit, astui, astuimme, astuitte, astuivat  
  `NI`: en/et/ei astunut, emme/ette/eivät astuneet  
  `PA`: astutaan, ei astuta, astuttiin, ei astuttu
- **asua** (`1`) - lakni (*to live somewhere*)  
  `P`: asun, asut, asuu, asumme, asutte, asuvat  
  `PN`: en asu, et asu...  
  `K`: Asunko? Asutko?...  
  `I`: asuin, asuit, asui, asuimme, asuitte, asuivat  
  `NI`: en/et/ei asunut, emme/ette/eivät asuneet  
  `PA`: asutaan, ei asuta, asuttiin, ei asuttu  
- **auttaa** (`1`) - segíteni (*to help*)  
  `P`: autan, autat, auttaa, autamme, autatte, auttavat  
  `PN`: en auta, et auta...  
  `K`: Autanko? Autatko?...  
  `I`: autoin, autoit, auttoi, autoimme, autoitte, auttoivat  
  `NI`: en/et/ei auttanut, emme/ette/eivät auttaneet  
  `PA`: autetaan, ei auteta, autettiin, ei autettu  
- **avata** (`4`) - kinyitni (*to open*)  
  `P`: avaan, avaat, avaa, avaamme, avaatte, avaavat  
  `PN`: en avaa, et avaa...  
  `K`: Avaanko? Avaatko?...  
  `I`: avasin, avasit, avasi, avasimme, avasitte, avasivat  
  `NI`: en/et/ei avannut, emme/ette/eivät avanneet  
  `PA`: avataan, ei avata, avattiin, ei avattu  
- **douppata** (`4`) - doppingolni (*to dope*)  
  `P`: douppaan, douppaat, douppaa, douppaamme, douppaatte, douppaavat  
  `PN`: en douppaa, et douppaa...  
  `K`: Douppaanko? Douppaatko?...  
  `I`: douppasin, douppasit, douppasi, douppasimme, douppasitte, douppasivat  
  `NI`: en/et/ei douppanut, emme/ette/eivät douppaneet  
  `PA`: douppataan, ei douppata, douppattiin, ei douppattu  
- **edesauttaa** (`1`) - elősegíteni (*to promote, to facilitate*)  
  `P`: edesautan, edesautat, edesauttaa, edesautamme, edesautatte, edesauttavat  
  `PN`: en edesauta, et edesauta...  
  `K`: Edesautanko? Edesautatko?...  
  `I`: edesautoin, edesautoit, edesautoi, edesautoimme, edesautoitte, edesauttoivat  
  `NI`: en/et/ei edesauttanut, emme/ette/eivät edesauttaneet  
  `PA`: edesautetaan, ei edesauteta, edesautettiin, ei edesautettu  
- **ehdottaa** (`1`) - javasolni (*to suggest*)  
  `P`: ehdotan, ehdotat, ehdottaa, ehdotamme, ehdotatte, ehdottavat  
  `PN`: en ehdota, et ehdota...  
  `K`: Ehdotanko? Ehdotatko?...  
  `I`: ehdotin, ehdotit, ehdotti, ehdotimme, ehdotitte, ehdottivat  
  `NI`: en/et/ei ehdottanut, emme/ette/eivät ehdottaneet  
  `PA`: ehdotetaan, ei ehdoteta, ehdotettiin, ei ehdotettu  
- **ehdyttää** (`1`) - kimeríteni (*to exhaust, to drain*)  
  `P`: ehdytän, ehdytät, ehdytää, ehdytämme, ehdytätte, ehdytävät  
  `PN`: en ehdytä, et ehdytä...  
  `K`: Ehdytänkö? Ehdytätkö?...  
  `I`: ehdytin, ehdytit, ehdytti, ehdytimme, ehdytitte, ehdyttivät  
  `NI`: en/et/ei ehdyttänyt, emme/ette/eivät ehdyttäneet  
  `PA`: ehdytetään, ei ehdytetä, ehdytettiin, ei ehdytetty  
- **ehtiä** (`1`) - elérni, ráérni (*to have time, to reach in time*)  
  `P`: ehdin, ehdit, ehtii, ehdimme, ehditte, ehtivät  
  `PN`: en ehdi, et ehdi...  
  `K`: Ehdinkö? Ehditkö?...  
  `I`: ehdin, ehdit, ehti, ehdimme, ehditte, ehtivät  
  `NI`: en/et/ei ehtinyt, emme/ette/eivät ehtineet  
  `PA`: ehditään, ei ehditä, ehdittiin, ei ehditty
- **eksyä** (`1`) - eltévedni (*to get lost*)  
  `P`: eksyn, eksyt, eksyy, eksymme, eksytte, eksyvät  
  `PN`: en eksy, et eksy...  
  `K`: Eksynkö? Eksytkö?...  
  `I`: eksyin, eksyit, eksyi, eksyimme, eksyitte, eksyivät  
  `NI`: en/et/ei eksynyt, emme/ette/eivät eksyneet  
  `PA`: eksytään, ei eksytä, eksyttiin, ei eksytty  
- **elää** (`1`) - élni (*to live*)  
  `P`: elän, elät, elää, elämme, elätte, elävät  
  `PN`: en elä, et elä...  
  `K`: Elänkö? Elätkö?...  
  `I`: elin, elit, eli, elimme, elitte, elivät  
  `NI`: en/et/ei elänyt, emme/ette/eivät eläneet  
  `PA`: eletään, ei eletä, elettiin, ei eletty  
- **ennustaa** (`1`) - megjósolni (*to predict, to forecast*)  
  `P`: ennustan, ennustaa, ennustamme, ennustatte, ennustavat  
  `PN`: en ennusta, et ennusta...  
  `K`: Ennustanko? Ennustatko?...  
  `I`: ennustin, ennustit, ennusti, ennustimme, ennustitte, ennustivat  
  `NI`: en/et/ei ennustanut, emme/ette/eivät ennustaneet  
  `PA`: ennustetaan, ei ennusteta, ennustettiin, ei ennustettu
- **epäillä** (`3`) - kételkedni (*to doubt, to suspect*)  
  `P`: epäilen, epäilet, epäilee, epäilemme, epäilette, epäilevät  
  `PN`: en epäile, et epäile...  
  `K`: Epäilenkö? Epäiletkö?...  
  `I`: epäilin, epäilit, epäili, epäilimme, epäilitte, epäilivät  
  `NI`: en/et/ei epäillyt, emme/ette/eivät epäilleet  
  `PA`: epäillään, ei epäillä, epäiltiin, ei epäilty
- **erehtyä** (`1`) - tévedni (*to make a mistake*)  
  `P`: erehdyn, erehdyt, erehtyy, erehdymme, erehdyitte, erehtyvät  
  `PN`: en erehdy, et erehdy...  
  `K`: Erehdynkö? Erehdytkö?...  
  `I`: erehdyin, erehdyit, erehtyi, erehdyimme, erehdyitte, erehtyivät  
  `NI`: en/et/ei erehtynyt, emme/ette/eivät erehtyneet  
  `PA`: erehdytään, ei erehdytä, erehdyttiin, ei erehdytty
- **erota** (`4`) - elválni (*to separate, to divorce, to resign*)  
  `P`: eroan, eroat, eroaa, eroamme, eroatte, eroavat  
  `PN`: en eroa, et eroa...  
  `K`: Eroanko? Eroatko?...  
  `I`: erosin, erosit, erosi, erosimme, erositte, erosivat  
  `NI`: en/et/ei eronnut, emme/ette/eivät eronneet  
  `PA`: erotaan, ei erota, erottiin, ei erottu  
- **esitellä** (`3`) - bemutatni (*to introduce, to present*)  
  `P`: esittelen, esittelet, esittelee, esittelemme, esittelette, esittelevät  
  `PN`: en esittele, et esittele...  
  `K`: Esittelenkö? Esitteletkö?...  
  `I`: esittelin, esittelit, esitteli, esittelimme, esittelitte, esittelivät  
  `NI`: en/et/ei esitellyt, emme/ette/eivät esitelleet  
  `PA`: esitellään, ei esitellä, esiteltiin, ei esitelty  
- **etsiä** (`1`) - keresni (*to search, to look for*)  
  `P`: etsin, etsit, etsii, etsimme, etsitte, etsivät  
  `PN`: en etsi, et etsi...  
  `K`: Etsinkö? Etsitkö?...  
  `I`: etsin, etsit, etsi, etsimme, etsitte, etsivät  
  `NI`: en/et/ei etsinyt, emme/ette/eivät etsineet  
  `PA`: etsitään, ei etsitä, etsittiin, ei etsitty  


<a name="f-i"></a>
## F to I

- **feikata** (`4`) - megjátszani, tettetni (*to fake, to pretend*)  
  `P`: feikkaan, feikkaat, feikkaa, feikkaamme, feikkaatte, feikkaavat  
  `PN`: en feikkaa, et feikkaa...  
  `K`: Feikkaanko? Feikkaatko?...  
  `I`: feikkasin, feikkasit, feikkasi, feikkasimme, feikkasitte, feikkasivat  
  `NI`: en/et/ei feikannut, emme/ette/eivät feikanneet  
  `PA`: feikataan, ei feikata, feikattiin, ei feikattu
- **haaveilla** (`3`) - álmodozni (*to dream, to fantasize*)  
  `P`: haaveilen, haaveilet, haaveilee, haaveilemme, haaveilette, haaveilevat  
  `PN`: en haaveile, et haaveile...  
  `K`: Haaveilenko? Haaveiletko?...  
  `I`: haaveilin, haaveilit, haaveili, haaveilimme, haaveilitte, haaveilivat  
  `NI`: en/et/ei haaveillut, emme/ette/eivät haaveilleet  
  `PA`: haaveillaan, ei haaveilla, haaveiltiin, ei haaveiltu
- **haista** (`3`) - szagolni, szagot árasztani (*to smell, to stink*)  
  `P`: haisen, haiset, haisee, haisemme, haisette, haisevat  
  `PN`: en haise, et haise...  
  `K`: Haisenko? Haisetko?...  
  `I`: haisin, haisit, haisi, haisimme, haisitte, haisivat  
  `NI`: en/et/ei haissut, emme/ette/eivät haisseet  
  `PA`: haistaan, ei haista, haistiin, ei haistu  
- **hajota** (`4`) - széttörni (*to break down, to disintegrate*)  
  `P`: hajoan, hajoaa, hajoamme, hajoatte, hajoavat  
  `PN`: en hajoa, et hajoa...  
  `K`: Hajoanko? Hajoatteko?...  
  `I`: hajosin, hajosi, hajosimme, hajosivat  
  `NI`: en/et/ei hajonnut, emme/ette/eivät hajonneet  
  `PA`: hajotaan, ei hajota, hajottiin, ei hajottu
- **hakata** (`4`) - ütni, vágni (*to hit, to chop, to hack*)  
  `P`: hakkaan, hakkaat, hakkaa, hakkaamme, hakkaatte, hakkaavat  
  `PN`: en hakkaa, et hakkaa...  
  `K`: Hakkaanko? Hakkaatteko?...  
  `I`: hakkasin, hakkasi, hakkasimme, hakkasivat  
  `NI`: en/et/ei hakannut, emme/ette/eivät hakanneet  
  `PA`: hakataan, ei hakata, hakattiin, ei hakattu
- **halata** (`4`) - megölelni (*to hug*)  
  `P`: halaan, halaat, halaa, halaamme, halaatte, halaavat  
  `PN`: en halaa, et halaa...  
  `K`: Halaanko? Halaatko?...  
  `I`: halasin, halasit, halasi, halasimme, halasitte, halasivat  
  `NI`: en/et/ei halannut, emme/ette/eivät halanneet  
  `PA`: halataan, ei halata, halattiin, ei halattu  
- **haljeta** (`6`) - szétrepedni, hasadni (*to burst, to split*)  
  `P`: halkean, halkeat, halkeaa, halkeamme, halkeatte, halkeavat  
  `PN`: en halkea, et halkea...  
  `K`: Halkeanko? Halkeatko?...  
  `I`: halkesin, halkesit, halkesi, halkesimme, halkesitte, halkesivat  
  `NI`: en/et/ei haljennut, emme/ette/eivät haljenneet  
  `PA`: haljetaan, ei haljeta, haljettiin, ei haljettu
- **haluta** (`4`) - akarni (*to want*)  
  `P`: haluan, haluat, haluaa, haluamme, haluatte, haluavat  
  `PN`: en halua, et halua...  
  `K`: Haluanko? Haluatko?...  
  `I`: halusin, halusit, halusi, halusimme, halusitte, halusivat  
  `NI`: en/et/ei halunnut, emme/ette/eivät halunneet  
  `PA`: halutaan, ei haluta, haluttiin, ei haluttu  
- **harjoittaa** (`1`) - gyakorolni (*to practice, to train*)  
  `P`: harjoitan, harjoitat, harjoittaa, harjoitamme, harjoitatte, harjoittavat  
  `PN`: en harjoita, et harjoita...  
  `K`: Harjoitanko? Harjoitatko?...  
  `I`: harjoitin, harjoitit, harjoitti, harjoitimme, harjoititte, harjoittivat  
  `NI`: en/et/ei harjoittanut, emme/ette/eivät harjoittaneet  
  `PA`: harjoitetaan, ei harjoiteta, harjoitettiin, ei harjoitettu  
- **harjoitella** (`3`) - edzeni, gyakorolni (*to practice*)  
  `P`: harjoittelen, harjoittelet, harjoittelee, harjoittelemme, harjoittelette, harjoittelevat  
  `PN`: en harjoittele, et harjoittele...  
  `K`: Harjoittelenko? Harjoitteletko?...  
  `I`: harjoittelin, harjoittelit, harjoitteli, harjoittelimme, harjoittelitte, harjoittelivat  
  `NI`: en/et/ei harjoitellut, emme/ette/eivät harjoitelleet  
  `PA`: harjoitellaan, ei harjoitella, harjoiteltiin, ei harjoiteltu  
- **haudata** (`4`) - eltemetni (*to bury*)  
  `P`: hautaan, hautaat, hautaa, hautaamme, hautaatte, hautaavat  
  `PN`: en hautaa, et hautaa...  
  `K`: Hautaanko? Hautaatko?...  
  `I`: hautasin, hautasit, hautasi, hautasimme, hautasitte, hautasivat  
  `NI`: en/et/ei haudannut, emme/ette/eivät haudanneet  
  `PA`: haudataan, ei haudata, haudattiin, ei haudattu
- **heittää** (`1`) - dobni (*to throw*)  
  `P`: heitän, heität, heittää, heitämme, heitätte, heittävät  
  `PN`: en heitä, et heitä...  
  `K`: Heitänkö? Heitätkö?...  
  `I`: heitin, heitit, heitti, heitimme, heititte, heittivät  
  `NI`: en/et/ei heittänyt, emme/ette/eivät heittäneet  
  `PA`: heitetään, ei heitetä, heitettiin, ei heitetty
- **herätä** (`4`) - felébredni (*to wake up*)  
  `P`: herään, heräät, herää, heräämme, heräätte, heräävät  
  `PN`: en herää, et herää...  
  `K`: Heräänkö? Heräätkö?...  
  `I`: heräsin, heräsit, heräsi, heräsimme, heräsitte, heräsivät  
  `NI`: en/et/ei herännyt, emme/ette/eivät heränneet  
  `PA`: herätään, ei herätä, herättiin, ei herätty  
- **hiihtää** (`1`) - síelni (*to ski*)  
  `P`: hiihdän, hiihdät, hiihtää, hiihdämme, hiihdätte, hiihtävät  
  `PN`: en hiihdä, et hiihdä...  
  `K`: Hiihdänkö? Hiihdätkö?...  
  `I`: hiihdin, hiihdit, hiihti, hiihdimme, hiihditte, hiihtivät  
  `NI`: en/et/ei hiihtänyt, emme/ette/eivät hiihtäneet  
  `PA`: hiihdetään, ei hiihdetä, hiihdettiin, ei hiihdetty  
- **huolestua** (`1`) - aggódni (*to become worried*)  
  `P`: huolestun, huolestut, huolestuu, huolestumme, huolestutte, huolestuvat  
  `PN`: en huolestu, et huolestu...  
  `K`: Huolestunko? Huolestutko?...  
  `I`: huolestuin, huolestuit, huolestui, huolestuimme, huolestuitte, huolestuivat  
  `NI`: en/et/ei huolestunut, emme/ette/eivät huolestuneet  
  `PA`: huolestutaan, ei huolestuta, huolestuttiin, ei huolestuttu
- **huomata** (`4`) - észrevenni (*to notice*)  
  `P`: huomaan, huomaat, huomaa, huomaamme, huomaatte, huomaavat  
  `PN`: en huomaa, et huomaa...  
  `K`: Huomaanko? Huomaatko?...  
  `I`: huomasin, huomasit, huomasi, huomasimme, huomasitte, huomasivat  
  `NI`: en/et/ei huomannut, emme/ette/eivät huomanneet  
  `PA`: huomataan, ei huomata, huomattiin, ei huomattu  
- **huutaa** (`1`) - kiáltani, kiabálni (*to shout*)  
  `P`: huudan, huudat, huutaa, huudamme, huudatte, huutavat  
  `PN`: en huuda, et huuda...  
  `K`: Huudanko? Huudatko?...  
  `I`: huusin, huusit, huusi, huusimme, huusitte, huusivat  
  `NI`: en/et/ei huutanut, emme/ette/eivät huutaneet  
  `PA`: huudetaan, ei huudeta, huudettiin, ei huudettu
- **hylätä** (`4`) - elhagyni, visszautasítani (*to abandon, to reject*)  
  `P`: hylkään, hylkäät, hylkää, hylkäämme, hylkäätte, hylkäävät  
  `PN`: en hylkää, et hylkää...  
  `K`: Hylkäänkö? Hylkäätkö?...  
  `I`: hylkäsin, hylkäsit, hylkäsi, hylkäsimme, hylkäsitte, hylkäsivät  
  `NI`: en/et/ei hylännyt, emme/ette/eivät hylänneet  
  `PA`: hylätään, ei hylätä, hylättiin, ei hylätty
- **hymyillä** (`3`) - mosolyogni (*to smile*)  
  `P`: hymyilen, hymyilet, hymyilee, hymyilemme, hymyilette, hymyilevät  
  `PN`: en hymyile, et hymyile...  
  `K`: Hymyilenkö? Hymyiletkö?...  
  `I`: hymyilin, hymyilit, hymyili, hymyilimme, hymyilitte, hymyilivät  
  `NI`: en/et/ei hymyillyt, emme/ette/eivät hymyilleet  
  `PA`: hymyillään, ei hymyillä, hymyiltiin, ei hymyilty  
- **hyväksyä** (`1`) - elfogadni (*to accept, to approve*)  
  `P`: hyväksyn, hyväksyt, hyväksyy, hyväksymme, hyväksytte, hyväksyvät  
  `PN`: en hyväksy, et hyväksy...  
  `K`: Hyväksynkö? Hyväksytkö?...  
  `I`: hyväksyin, hyväksyit, hyväksyi, hyväksyimme, hyväksyitte, hyväksyivät  
  `NI`: en/et/ei hyväksynyt, emme/ette/eivät hyväksyneet  
  `PA`: hyväksytään, ei hyväksytä, hyväksyttiin, ei hyväksytty  
- **häiritä** (`5`) - zavarni (*to disturb, to bother*)  
  `P`: häiritsen, häiritset, häiritsee, häiritsemme, häiritsette, häiritsevät  
  `PN`: en häiritse, et häiritse...  
  `K`: Häiritsenkö? Häiritsetkö?...  
  `I`: häiritsin, häiritsit, häiritsi, häiritsimme, häiritsitte, häiritsivät  
  `NI`: en/et/ei häirinnyt, emme/ette/eivät häirinneet  
  `PA`: häiritään, ei häiritä, häirittiin, ei häiritty  
- **ihailla** (`3`) - csodálni (*to admire*)  
  `P`: ihailen, ihailet, ihailee, ihailemme, ihailette, ihailevat  
  `PN`: en ihaile, et ihaile...  
  `K`: Ihailenko? Ihailetko?...  
  `I`: ihailin, ihailit, ihaili, ihailimme, ihailitte, ihailivat  
  `NI`: en/et/ei ihaillut, emme/ette/eivät ihailleet  
  `PA`: ihaillaan, ei ihailla, ihailtiin, ei ihailtu  
- **ihastua** (`1`) - belehabarodni (*to become fond of, to fall for*)  
  `P`: ihastun, ihastut, ihastuu, ihastumme, ihastutte, ihastuvat  
  `PN`: en ihastu, et ihastu...  
  `K`: Ihastunko? Ihastutko?...  
  `I`: ihastuin, ihastuit, ihastui, ihastuimme, ihastuitte, ihastuivat  
  `NI`: en/et/ei ihastunut, emme/ette/eivät ihastuneet  
  `PA`: ihastutaan, ei ihastuta, ihastuttiin, ei ihastuttu  
- **ihmetellä** (`3`) - csodálkozni (*to wonder, to be amazed*)  
  `P`: ihmettelen, ihmettelet, ihmettelee, ihmettelemme, ihmettelette, ihmettelevat  
  `PN`: en ihmettele, et ihmettele...  
  `K`: Ihmettelenko? Ihmetteletko?...  
  `I`: ihmettelin, ihmettelit, ihmetteli, ihmettelimme, ihmettelitte, ihmettelivät  
  `NI`: en/et/ei ihmetellyt, emme/ette/eivät ihmetelleet  
  `PA`: ihmetellään, ei ihmetellä, ihmeteltiin, ei ihmetelty  
- **ilmetä** (`6`) - kiderülni, megnyilvánulni (*to emerge, to appear*)  
  `P`: ilmenee, ilmenet, ilmenee, ilmenevät  
  `PN`: ei ilmene...  
  `K`: Ilmenenkö? Ilmenetkö?...  
  `I`: ilmenin, ilmenit, ilmeni, ilmenimme, ilmenitte, ilmenivät  
  `NI`: en/et/ei ilmennyt, emme/ette/eivät ilmenneet  
  `PA`: ilmetään, ei ilmetä, ilmettiin, ei ilmetty
- **ilmoittaa** (`1`) - jelenteni (*to announce, to report*)  
  `P`: ilmoitan, ilmoitat, ilmoittaa, ilmoitamme, ilmoitatte, ilmoittavat  
  `PN`: en ilmoita, et ilmoita...  
  `K`: Ilmoitanko? Ilmoitatko?...  
  `I`: ilmoitin, ilmoitit, ilmoitti, ilmoitimme, ilmoititte, ilmoittivat  
  `NI`: en/et/ei ilmoittanut, emme/ette/eivät ilmoittaneet  
  `PA`: ilmoitetaan, ei ilmoiteta, ilmoitettiin, ei ilmoitettu  
- **ilostua** (`1`) - megörülni (*to become happy*)  
  `P`: ilostun, ilostut, ilostuu, ilostumme, ilostutte, ilostuvat  
  `PN`: en ilostu, et ilostu...  
  `K`: Ilostunko? Ilostutko?...  
  `I`: ilostuin, ilostuit, ilostui, ilostuimme, ilostuitte, ilostuivat  
  `NI`: en/et/ei ilostunut, emme/ette/eivät ilostuneet  
  `PA`: ilostutaan, ei ilostuta, ilostuttiin, ei ilostuttu
- **imuroida** (`2`) - porszívózni (*to vacuum*)  
  `P`: imuroin, imuroit, imuroi, imuroimme, imuroitte, imuroivat  
  `PN`: en imuroi, et imuroi...  
  `K`: Imuroinko? Imuroitko?...  
  `I`: imuroin, imuroit, imuroi, imuroimme, imuroitte, imuroivat  
  `NI`: en/et/ei imuroinut, emme/ette/eivät imuroineet  
  `PA`: imuroidaan, ei imuroida, imuroitiin, ei imuroitu
- **innostaa** (`1`) - lelkesíteni (*to inspire, to excite*)  
  `P`: innostan, innostat, innostaa, innostamme, innostatte, innostavat  
  `PN`: en innosta, et innosta...  
  `K`: Innostanko? Innostatko?...  
  `I`: innostin, innostit, innosti, innostimme, innostitte, innostivat  
  `NI`: en/et/ei innostanut, emme/ette/eivät innostaneet  
  `PA`: innostetaan, ei innosteta, innostettiin, ei innostettu
- **innostua** (`1`) - lelkesedni (*to get excited, to become enthusiastic*)  
  `P`: innostun, innostut, innostuu, innostumme, innostutte, innostuvat  
  `PN`: en innostu, et innostu...  
  `K`: Innostunko? Innostutko?...  
  `I`: innostuin, innostuit, innostui, innostuimme, innostuitte, innostuivat  
  `NI`: en/et/ei innostunut, emme/ette/eivät innostuneet  
  `PA`: innostutaan, ei innostuta, innostuttiin, ei innostuttu
- **istua** (`1`) - ülni (*to sit*)  
  `P`: istun, istut, istuu, istumme, istutte, istuvat  
  `PN`: en istu, et istu...  
  `K`: Istunko? Istutko?...  
  `I`: istuin, istuit, istui, istuimme, istuitte, istuivat  
  `NI`: en/et/ei istunut, emme/ette/eivät istuneet  
  `PA`: istutaan, ei istuta, istuttiin, ei istuttu
- **itkeä** (`1`) - sírni (*to cry*)  
  `P`: itken, itket, itkee, itkemme, itkette, itkevät  
  `PN`: en itke, et itke...  
  `K`: Itkenkö? Itketkö?...  
  `I`: itkin, itkit, itki, itkimme, itkette, itkivät  
  `NI`: en/et/ei itkenyt, emme/ette/eivät itkeneet  
  `PA`: itketään, ei itketä, itkettiin, ei itketty


<a name="j-k"></a>
## J to K

<img src="/_ui/img/imgfx/conversation.png" title="" align="right" class="right">

- **jakaa** (`1`) - megosztani (*to share, to divide*)  
  `P`: jaan, jaat, jakaa, jaamme, jaatte, jakavat  
  `PN`: en jaa, et jaa...  
  `K`: Jaanko? Jaatko?...  
  `I`: jaoin, jaoit, jakoi, jaoimme, jaoitte, jakoivat  
  `NI`: en/et/ei jakanut, emme/ette/eivät jakaneet  
  `PA`: jaetaan, ei jaeta, jaettiin, ei jaettu
- **jatkaa** (`1`) - folytatni (*to continue*)  
  `P`: jatkan, jatkat, jatkaa, jatkamme, jatkatte, jatkavat  
  `PN`: en jatka, et jatka...  
  `K`: Jatkanko? Jatkatko?...  
  `I`: jatkoin, jatkoit, jatkoi, jatkoimme, jatkoitte, jatkoivat  
  `NI`: en/et/ei jatkanut, emme/ette/eivät jatkaneet  
  `PA`: jatketaan, ei jatketa, jatkettiin, ei jatkettu
- **järjestää** (`1`) - szervezni, rendezni (*to organize*)  
  `P`: järjestän, järjestät, järjestää, järjestämme, järjestätte, järjestävät  
  `PN`: en järjestä, et järjestä...  
  `K`: Järjestänkö? Järjestätkö?...  
  `I`: järjestin, järjestit, järjesti, järjestimme, järjestitte, järjestivät  
  `NI`: en/et/ei järjestänyt, emme/ette/eivät järjestäneet  
  `PA`: järjestetään, ei järjestetä, järjestettiin, ei järjestetty
- **juhlia** (`1`) - ünnepelni (*to celebrate*)  
  `P`: juhlin, juhlit, juhlii, juhlimme, juhlitte, juhlivat  
  `PN`: en juhli, et juhli...  
  `K`: Juhlinko? Juhlitko?...  
  `I`: juhlin, juhlit, juhli, juhlimme, juhlitte, juhlivat  
  `NI`: en/et/ei juhlinut, emme/ette/eivät juhlineet  
  `PA`: juhlitaan, ei juhlita, juhlittiin, ei juhlittu
- **juoda** (`2`) - inni (*to drink*)  
  `P`: juon, juot, juo, juomme, juotte, juovat  
  `PN`: en juo, et juo...  
  `K`: Juonko? Juotko?...  
  `I`: join, joit, joi, joimme, joitte, joivat  
  `NI`: en/et/ei juonut, emme/ette/eivät juoneet  
  `PA`: juodaan, ei juoda, juotiin, ei juotu
- **juoksuttaa** (`1`) - futtatni (*to make someone run*)  
  `P`: juoksutan, juoksutat, juoksuttaa, juoksutamme, juoksutatte, juoksuttavat  
  `PN`: en juoksuta, et juoksuta...  
  `K`: Juoksutanko? Juoksutatko?...  
  `I`: juoksutin, juoksutit, juoksutti, juoksutimme, juoksutitte, juoksuttivat  
  `NI`: en/et/ei juoksuttanut, emme/ette/eivät juoksuttaneet  
  `PA`: juoksutetaan, ei juoksuteta, juoksutettiin, ei juoksutettu
- **juoruta** (`4`) - pletykálni (*to gossip*)  
  `P`: juoruan, juoruat, juoruaa, juoruamme, juoruatte, juoruavat  
  `PN`: en juorua, et juorua...  
  `K`: Juoruanko? Juoruatko?...  
  `I`: juorusin, juorusit, juorusi, juorusimme, juorusitte, juorusivat  
  `NI`: en/et/ei juorunnut, emme/ette/eivät juorunneet  
  `PA`: juorutaan, ei juoruta, juoruttiin, ei juoruttu
- **juosta** (`3`) - futni (*to run*)  
  `P`: juoksen, juokset, juoksee, juoksemme, juoksette, juoksevat  
  `PN`: en juokse, et juokse...  
  `K`: Juoksenko? Juoksetko?...  
  `I`: juoksin, juoksit, juoksi, juoksimme, juoksitte, juoksivat  
  `NI`: en/et/ei juossut, emme/ette/eivät juosseet  
  `PA`: juostaan, ei juosta, juostiin, ei juostu
- **juottaa** (`1`) - itatni (*to make sb. drink*)  
  `P`: juotan, juotat, juottaa, juotamme, juotatte, juottavat  
  `PN`: en juota, et juota...  
  `K`: Juotanko? Juotatko?...  
  `I`: juotin, juotit, juotti, juotimme, juotitte, juottivat  
  `NI`: en/et/ei juottanut, emme/ette/eivät juottaneet  
  `PA`: juotetaan, ei juoteta, juotettiin, ei juotettu
- **jutella** (`3`) - beszélgetni (*to chat, to talk casually*)  
  `P`: juttelen, juttelet, juttelee, juttelemme, juttelette, juttelevat  
  `PN`: en juttele, et juttele...  
  `K`: Juttelenko? Jutteletko?...  
  `I`: juttelin, juttelit, jutteli, juttelimme, juttelitte, juttelivat  
  `NI`: en/et/ei jutellut, emme/ette/eivät jutelleet  
  `PA`: jutellaan, ei jutella, juteltiin, ei juteltu
- **jutustella** (`3`) - csevegni (*to have a chat*)  
  `P`: jutustelen, jutustelet, jutustelee, jutustelemme, jutustelette, jutustelevat  
  `PN`: en jutustele, et jutustele...  
  `K`: Jutustelenko? Jutusteletko?...  
  `I`: jutustelin, jutustelit, jutusteli, jutustelimme, jutustelitte, jutustelivat  
  `NI`: en/et/ei jutustellut, emme/ette/eivät jutustelleet  
  `PA`: jutustellaan, ei jutustella, jutusteltiin, ei jutusteltu
- **kadehtia** (`1`) - irigykedni (*to envy*)  
  `P`: kadehdin, kadehdit, kadehtii, kadehdimme, kadehditte, kadehtivat  
  `PN`: en kadehdi, et kadehdi...  
  `K`: Kadehdinko? Kadehditko?...  
  `I`: kadehdin, kadehdit, kadehti, kadehdimme, kadehditte, kadehtivat  
  `NI`: en/et/ei kadehtinut, emme/ette/eivät kadehtineet  
  `PA`: kadehditaan, ei kadehdita, kadehdittiin, ei kadehdittu
- **kadottaa** (`1`) - elveszíteni (*to lose something*)  
  `P`: kadotan, kadotat, kadottaa, kadotamme, kadotatte, kadottavat  
  `PN`: en kadota, et kadota...  
  `K`: Kadotanko? Kadotatko?...  
  `I`: kadotin, kadotit, kadotti, kadotimme, kadotitte, kadottivat  
  `NI`: en/et/ei kadottanut, emme/ette/eivät kadottaneet  
  `PA`: kadotetaan, ei kadoteta, kadotettiin, ei kadotettu
- **kehittää** (`1`) - fejleszteni (*to develop*)  
  `P`: kehitän, kehität, kehittää, kehitämme, kehitätte, kehittävät  
  `PN`: en kehitä, et kehitä...  
  `K`: Kehitänkö? Kehitätkö?...  
  `I`: kehitin, kehiti, kehitti, kehitimme, kehittivät  
  `NI`: en/et/ei kehittänyt, emme/ette/eivät kehittäneet  
  `PA`: kehitetään, ei kehitetä, kehitettiin, ei kehitetty
- **kaivata** (`4`) - hiányolni, vágyakozni (*to miss, to long for*)  
  `P`: kaipaan, kaipaat, kaipaa, kaipaamme, kaipaatte, kaipaavat  
  `PN`: en kaipaa, et kaipaa...  
  `K`: Kaipaanko? Kaipaatko?...  
  `I`: kaipasin, kaipasit, kaipasi, kaipasimme, kaipasitte, kaipasivat  
  `NI`: en/et/ei kaivannut, emme/ette/eivät kaivanneet  
  `PA`: kaivataan, ei kaivata, kaivattiin, ei kaivattu
- **kalastaa** (`1`) - horgászni, halászni (*to fish*)  
  `P`: kalastan, kalastat, kalastaa, kalastamme, kalastatte, kalastavat  
  `PN`: en kalasta, et kalasta...  
  `K`: Kalastanko? Kalastatko?...  
  `I`: kalastin, kalastit, kalasti, kalastimme, kalastitte, kalastivat  
  `NI`: en/et/ei kalastanut, emme/ette/eivät kalastaneet  
  `PA`: kalastetaan, ei kalasteta, kalastettiin, ei kalastettu
- **karata** (`4`) - megszökni (*to escape, to run away*)  
  `P`: karkaan, karkaat, karkaa, karkaamme, karkaatte, karkaavat  
  `PN`: en karkaa, et karkaa...  
  `K`: Karkaanko? Karkaatko?...  
  `I`: karkasin, karkasit, karkasi, karkasimme, karkasitte, karkasivat  
  `NI`: en/et/ei karannut, emme/ette/eivät karanneet  
  `PA`: karataan, ei karata, karattiin, ei karattu
- **katketa** (`6`) - eltörni (*to snap, to break*)  
  `P`: katkean, katkeaa, katkeamme, katkeatte, katkeavat  
  `PN`: en katkea, et katkea...  
  `K`: Katkeanko? Katkeatteko?...  
  `I`: katkesi, katkesimme, katkesivat  
  `NI`: en/et/ei katkennut, emme/ette/eivät katkenneet  
  `PA`: katketaan, ei katketa, katkesiin, ei katketettu
- **katsella** (`3`) - nézegetni (*to look at*)  
  `P`: katselen, katselet, katselee, katselemme, katselette, katselevat  
  `PN`: en katsele, et katsele...  
  `K`: Katselenko? Katseletko?...  
  `I`: katselin, katselit, katseli, katselimme, katselitte, katselivat  
  `NI`: en/et/ei katsellut, emme/ette/eivät katselleet  
  `PA`: katsellaan, ei katsella, katseltiin, ei katseltu
- **katsoa** (`1`) - nézni (*to watch, to look at*)  
  `P`: katson, katsot, katsoo, katsomme, katsotte, katsovat  
  `PN`: en katso, et katso...  
  `K`: Katsonko? Katsotko?...  
  `I`: katsoin, katsoit, katsoi, katsoimme, katsoitte, katsoivat  
  `NI`: en/et/ei katsonut, emme/ette/eivät katsoneet  
  `PA`: katsotaan, ei katsota, katsottiin, ei katsottu
- **kehuskella** (`3`) - dicsekedni (*to boast*)  
  `P`: kehuskelen, kehustelet, kehustelee, kehustelemme, kehustelette, kehustelevat  
  `PN`: en kehustele, et kehustele...  
  `K`: Kehustelenko? Kehusteletko?...  
  `I`: kehustelin, kehustelit, kehusteli, kehustelimme, kehustelitte, kehustelivat  
  `NI`: en/et/ei kehuskellut, emme/ette/eivät kehuskelleet  
  `PA`: kehuskellaan, ei kehuskella, kehusteltiin, ei kehusteltu
- **keskustella** (`3`) - beszélgetni (*to discuss, to converse*)  
  `P`: keskustelen, keskustelet, keskustelee, keskustelemme, keskustelette, keskustelevat  
  `PN`: en keskustele, et keskustele...  
  `K`: Keskustelenko? Keskusteletko?...  
  `I`: keskustelin, keskustelit, keskusteli, keskustelimme, keskustelitte, keskustelivat  
  `NI`: en/et/ei keskustellut, emme/ette/eivät keskustelleet  
  `PA`: keskustellaan, ei keskustella, keskusteltiin, ei keskusteltu
- **kerrata** (`4`) - ismételni (*to revise, to repeat*)  
  `P`: kertaan, kertaat, kertaa, kertaamme, kertaatte, kertaavat  
  `PN`: en kertaa, et kertaa...  
  `K`: Kertaanko? Kertaatko?...  
  `I`: kertasin, kertasit, kertasi, kertasimme, kertasitte, kertasivat  
  `NI`: en/et/ei kerrannut, emme/ette/eivät kerranneet  
  `PA`: kerrataan, ei kerrata, kerrattiin, ei kerrattu
- **kertoa** (`1`) - mesélni, mondani (*to tell, to narrate*)  
  `P`: kerron, kerrot, kertoo, kerromme, kerrotte, kertovat  
  `PN`: en kerro, et kerro...  
  `K`: Kerronko? Kerrotko?...  
  `I`: kerroin, kerroit, kertoi, kerroimme, kerroitte, kertoivat  
  `NI`: en/et/ei kertonut, emme/ette/eivät kertoneet  
  `PA`: kerrotaan, ei kerrota, kerrottiin, ei kerrottu
- **keräillä** (`3`) - gyűjtögetni (*to collect*)  
  `P`: keräilen, keräilet, keräilee, keräilemme, keräilette, keräilevät  
  `PN`: en keräile, et keräile...  
  `K`: Keräilenkö? Keräiletkö?...  
  `I`: keräilin, keräilit, keräili, keräilimme, keräilitte, keräilivät  
  `NI`: en/et/ei keräillyt, emme/ette/eivät keräilleet  
  `PA`: keräillään, ei keräillä, keräiltiin, ei keräilty
- **kieltää** (`1`) - megtiltani (*to forbid*)  
  `P`: kiellän, kiellät, kieltää, kiellämme, kiellätte, kieltävät  
  `PN`: en kiellä, et kiellä...  
  `K`: Kiellänkö? Kiellätkö?...  
  `I`: kielsin, kielsit, kielsi, kielsimme, kielsitte, kielsivät  
  `NI`: en/et/ei kieltänyt, emme/ette/eivät kieltäneet  
  `PA`: kielletään, ei kielletä, kiellettiin, ei kielletty
- **kiivetä** (`4`) - mászni (*to climb*)  
  `P`: kiipeän, kiipeät, kiipeää, kiipeämme, kiipeätte, kiipeävät  
  `PN`: en kiipeä, et kiipeä...  
  `K`: Kiipeänkö? Kiipeätkö?...  
  `I`: kiipesin, kiipesit, kiipesi, kiipesimme, kiipesitte, kiipesivät  
  `NI`: en/et/ei kiivennyt, emme/ette/eivät kiivenneet  
  `PA`: kiivetään, ei kiivetä, kiivettiin, ei kiivetty
- **kirjoittaa** (`1`) - írni (*to write*)  
  `P`: kirjoitan, kirjoitat, kirjoittaa, kirjoitamme, kirjoitatte, kirjoittavat  
  `PN`: en kirjoita, et kirjoita...  
  `K`: Kirjoitanko? Kirjoitatko?...  
  `I`: kirjoitin, kirjoitit, kirjoitti, kirjoitimme, kirjoititte, kirjoittivat  
  `NI`: en/et/ei kirjoittanut, emme/ette/eivät kirjoittaneet
  `PA`: kirjoitetaan, ei kirjoiteta, kirjoitettiin, ei kirjoitettu
- **korjata** (`4`) - javítani (*to repair, to fix*)  
  `P`: korjaan, korjaat, korjaa, korjaamme, korjaatte, korjaavat  
  `PN`: en korjaa, et korjaa...  
  `K`: Korjaanko? Korjaatko?...  
  `I`: korjasin, korjasit, korjasi, korjasimme, korjasitte, korjasivat  
  `NI`: en/et/ei korjannut, emme/ette/eivät korjanneet  
  `PA`: korjataan, ei korjata, korjattiin, ei korjattu
- **kuivata** (`4`) - szárítani (*to dry*)  
  `P`: kuivaan, kuivaat, kuivaa, kuivaamme, kuivaatte, kuivaavat  
  `PN`: en kuivaa, et kuivaa...  
  `K`: Kuivanko? Kuivaatko?...  
  `I`: kuivasin, kuivasit, kuivasi, kuivasimme, kuivasitte, kuivasivat  
  `NI`: en/et/ei kuivannut, emme/ette/eivät kuivanneet  
  `PA`: kuivataan, ei kuivata, kuivattiin, ei kuivattu
- **kumartaa** (`1`) - meghajolni (*to bow*)  
  `P`: kumarran, kumarrat, kumartaa, kumarramme, kumarratte, kumartavat  
  `PN`: en kumarra, et kumarra...  
  `K`: Kumarranko? Kumarratko?...  
  `I`: kumarsin, kumarsit, kumarsi, kumarsimme, kumarsitte, kumarsivat  
  `NI`: en/et/ei kumartanut, emme/ette/eivät kumartaneet  
  `PA`: kumarretaan, ei kumarreta, kumarrettiin, ei kumarrettu
- **kutsua** (`1`) - hívni, meghívni (*to call, to invite*)  
  `P`: kutsun, kutsut, kutsuu, kutsumme, kutsutte, kutsuvat  
  `PN`: en kutsu, et kutsu...  
  `K`: Kutsunko? Kutsutko?...  
  `I`: kutsuin, kutsuit, kutsui, kutsuimme, kutsuitte, kutsuivat  
  `NI`: en/et/ei kutsunut, emme/ette/eivät kutsuneet  
  `PA`: kutsutaan, ei kutsuta, kutsuttiin, ei kutsuttu
- **kuulla** (`3`) - hallani (*to hear*)  
  `P`: kuulen, kuulet, kuulee, kuulemme, kuulette, kuulevat  
  `PN`: en kuule, et kuule...  
  `K`: Kuulenko? Kuuletko?...  
  `I`: kuulin, kuulit, kuuli, kuulimme, kuulitte, kuulivat  
  `NI`: en/et/ei kuullut, emme/ette/eivät kuulleet  
  `PA`: kuullaan, ei kuulla, kuultiin, ei kuultu
- **kuunnella** (`3`) - hallgatni (*to listen*)  
  `P`: kuuntelen, kuuntelet, kuuntelee, kuuntelemme, kuuntelette, kuuntelevat  
  `PN`: en kuuntele, et kuuntele...  
  `K`: Kuuntelenko? Kuunteletko?...  
  `I`: kuuntelin, kuuntelit, kuunteli, kuuntelimme, kuuntelitte, kuuntelivat  
  `NI`: en/et/ei kuunnellut, emme/ette/eivät kuunnelleet  
  `PA`: kuunnellaan, ei kuunnella, kuunneltiin, ei kuunneltu
- **kylpeä** (`1`) - fürdeni (*to bathe*)  
  `P`: kylven, kylvet, kylpee, kylvemme, kylvette, kylpevät  
  `PN`: en kylve, et kylve...  
  `K`: Kylvenkö? Kylvetkö?...  
  `I`: kylvin, kylvit, kylpi, kylvimme, kylvitte, kylpivät  
  `NI`: en/et/ei kylpenyt, emme/ette/eivät kylpeneet  
  `PA`: kylvetään, ei kylvetä, kylvettiin, ei kylvetty
- **kävellä** (`3`) - sétálni (*to walk*)  
  `P`: kävelen, kävelet, kävelee, kävelemme, kävelette, kävelevät  
  `PN`: en kävele, et kävele...  
  `K`: Kävelenkö? Käveletkö?...  
  `I`: kävelin, kävelit, käveli, kävelimme, kävelitte, kävelivät  
  `NI`: en/et/ei kävellyt, emme/ette/eivät kävelleet  
  `PA`: kävellään, ei kävellä, käveltiin, ei kävelty
- **kääntää** (`1`) - fordítani (*to translate*)  
  `P`: käännän, käännät, kääntää, käännämme, käännätte, kääntävät  
  `PN`: en käännä, et käännä...  
  `K`: Käännänkö? Käännätkö?...  
  `I`: käänsin, käänsit, käänsi, käänsimme, käänsitte, käänsivät  
  `NI`: en/et/ei kääntänyt, emme/ette/eivät kääntäneet  
  `PA`: käännetään, ei käännetä, käännettiin, ei käännetty
- **käydä** (`2`) - járni, meglátogatni (*to visit, to go to*)  
  `P`: käyn, käyt, käy, käymme, käytte, käyvät  
  `PN`: en käy, et käy...  
  `K`: Käynkö? Käytkö?...  
  `I`: kävin, kävit, kävi, kävimme, kävitte, kävivät  
  `NI`: en/et/ei käynyt, emme/ette/eivät käyneet  
  `PA`: käydään, ei käydä, käytiin, ei käyty
- **käyttää** (`1`) - használni (*to use*)  
  `P`: käytän, käytät, käyttää, käytämme, käytätte, käyttävät  
  `PN`: en käytä, et käytä...  
  `K`: Käytänkö? Käytätkö?...  
  `I`: käytin, käytit, käytti, käytimme, käytitte, käyttivät  
  `NI`: en/et/ei käyttänyt, emme/ette/eivät käyttäneet  
  `PA`: käytetään, ei käytetä, käytettiin, ei käytetty


<a name="l-n"></a>
## L to N

- **laimentaa** (`1`) - hígítani (*to dilute*)  
  `P`: laimennan, laimennat, laimentaa, laimennamme, laimennatte, laimentavat  
  `PN`: en laimenna, et laimenna...  
  `K`: Laimennanko? Laimennatko?...  
  `I`: laimensin, laimensit, laimensi, laimensimme, laimensitte, laimensivat  
  `NI`: en/et/ei laimentanut, emme/ette/eivät laimentaneet  
  `PA`: laimennetaan, ei laimenneta, laimennettiin, ei laimennettu
- **lainata** (`4`) - kölcsönadni, kölcsönvenni (*to lend, to borrow*)  
  `P`: lainaan, lainaat, lainaa, lainaamme, lainaatte, lainaavat  
  `PN`: en lainaa, et lainaa...  
  `K`: Lainanko? Lainaatko?...  
  `I`: lainasin, lainasit, lainasi, lainasimme, lainasitte, lainasivat  
  `NI`: en/et/ei lainannut, emme/ette/eivät lainanneet  
  `PA`: lainataan, ei lainata, lainattiin, ei lainattu  
- **laiskotella** (`3`) - lustálkodni (*to laze around*)  
  `P`: laiskottelen, laiskottelet, laiskottelee, laiskottelemme, laiskottelette, laiskottelevat  
  `PN`: en laiskottele, et laiskottele...  
  `K`: Laiskottelenko? Laiskotteletko?...  
  `I`: laiskottelin, laiskottelit, laiskotteli, laiskottelimme, laiskottelitte, laiskottelivat  
  `NI`: en/et/ei laiskotellut, emme/ette/eivät laiskotelleet  
  `PA`: laiskotellaan, ei laiskotella, laiskoteltiin, ei laiskoteltu
- **laskea** (`1`) - számolni, leengedni (*to count, to lower*)  
  `P`: lasken, lasket, laskee, laskemme, laskette, laskevat  
  `PN`: en laske, et laske...  
  `K`: Laskenko? Lasketko?...  
  `I`: laskin, laskit, laski, laskimme, laskitte, laskivat  
  `NI`: en/et/ei laskenut, emme/ette/eivät laskeneet  
  `PA`: lasketaan, ei lasketa, laskettiin, ei laskettu
- **laskea leikkiä** (`1`) - viccelődni (*to joke*)  
  `P`: lasken leikkiä, lasket leikkiä, laskee leikkiä, laskemme leikkiä, laskette leikkiä, laskevat leikkiä  
  `PN`: en laske leikkiä, et laske leikkiä...  
  `K`: Laskenko leikkiä? Lasketko leikkiä?...  
  `I`: laskin leikkiä, laskit leikkiä, laski leikkiä, laskimme leikkiä, laskitte leikkiä, laskivat leikkiä  
  `NI`: en/et/ei laskenut leikkiä, emme/ette/eivät laskeneet leikkiä  
  `PA`: lasketaan leikkiä, ei lasketa leikkiä, laskettiin leikkiä, ei laskettu leikkiä
- **laskeutua** (`1`) - leszállni (*to descend, to land*)  
  `P`: laskeudun, laskeudut, laskeutuu, laskeudumme, laskeudutte, laskeutuvat  
  `PN`: en laskeudu, et laskeudu...  
  `K`: Laskeudunko? Laskeudutko?...  
  `I`: laskeuduin, laskeuduit, laskeutui, laskeuduimme, laskeuduitte, laskeutuivat  
  `NI`: en/et/ei laskeutunut, emme/ette/eivät laskeutuneet  
  `PA`: laskeudutaan, ei laskeuduta, laskeuduttiin, ei laskeuduttu
- **laulaa** (`1`) - énekelni (*to sing*)  
  `P`: laulan, laulat, laulaa, laulamme, laulatte, laulavat  
  `PN`: en laula, et laula...  
  `K`: Laulanko? Laulatko?...  
  `I`: lauloin, lauloit, lauloi, lauloimme, lauloitte, lauloivat  
  `NI`: en/et/ei laulanut, emme/ette/eivät laulaneet  
  `PA`: lauletaan, ei lauleta, laulettiin, ei laulettu
- **leikata** (`4`) - vágni (*to cut*)  
  `P`: leikkaan, leikkaat, leikkaa, leikkaamme, leikkaatte, leikkaavat  
  `PN`: en leikkaa, et leikkaa...  
  `K`: Leikkaanko? Leikkaatko?...  
  `I`: leikkasin, leikkasit, leikkasi, leikkasimme, leikkasitte, leikkasivat  
  `NI`: en/et/ei leikannut, emme/ette/eivät leikanneet  
  `PA`: leikataan, ei leikata, leikattiin, ei leikattu
- **leikkiä** (`1`) - játszani (*to play*)  
  `P`: leikin, leikit, leikkii, leikimme, leikitte, leikkivät  
  `PN`: en leiki, et leiki...  
  `K`: Leikinkö? Leikitkö?...  
  `I`: leikin, leikit, leikki, leikimme, leikitte, leikkivät  
  `NI`: en/et/ei leikkinyt, emme/ette/eivät leikkineet  
  `PA`: leikitään, ei leikitä, leikittiin, ei leikitty
- **leipoa** (`1`) - sütni (*to bake*)  
  `P`: leivon, leivot, leipoo, leivomme, leivotte, leipovat  
  `PN`: en leivo, et leivo...  
  `K`: Leivonko? Leivotko?...  
  `I`: leivoin, leivoit, leipoi, leivoimme, leivoitte, leipoivat  
  `NI`: en/et/ei leiponut, emme/ette/eivät leiponeet  
  `PA`: leivotaan, ei leivota, leivottiin, ei leivottu
- **lentää** (`1`) - repülni (*to fly*)  
  `P`: lennän, lennät, lentää, lennämme, lennätte, lentävät  
  `PN`: en lennä, et lennä...  
  `K`: Lennänkö? Lennätkö?...  
  `I`: lensin, lensit, lensi, lensimme, lensitte, lensivät  
  `NI`: en/et/ei lentänyt, emme/ette/eivät lentäneet  
  `PA`: lennetään, ei lennetä, lennettiin, ei lennetty
- **lennättää** (`1`) - reptetni (*to make something fly*)  
  `P`: lennätän, lennätät, lennättää, lennätämme, lennätätte, lennättävät  
  `PN`: en lennätä, et lennätä...  
  `K`: Lennätänkö? Lennätätkö?...  
  `I`: lennätin, lennätit, lennätti, lennätimme, lennätitte, lennättivät  
  `NI`: en/et/ei lennättänyt, emme/ette/eivät lennättäneet  
  `PA`: lennätetään, ei lennätetä, lennätettiin, ei lennätetty
- **levätä** (`4`) - pihenni (*to rest*)  
  `P`: lepään, lepäät, lepää, lepäämme, lepäätte, lepäävät  
  `PN`: en lepää, et lepää...  
  `K`: Lepäänkö? Lepäätkö?...  
  `I`: lepäsin, lepäisit, lepäsi, lepäsimme, lepäsite, lepäsivät  
  `NI`: en/et/ei levännyt, emme/ette/eivät levänneet  
  `PA`: levätään, ei levätä, levättiin, ei levätty
- **lopettaa** (`1`) - abbahagyni (*to stop*)  
  `P`: lopetan, lopetat, lopettaa, lopetamme, lopetatte, lopettavat  
  `PN`: en lopeta, et lopeta...  
  `K`: Lopetanko? Lopetatko?...  
  `I`: lopetin, lopetit, lopetti, lopetimme, lopetitte, lopettivat  
  `NI`: en/et/ei lopettanut, emme/ette/eivät lopettaneet  
  `PA`: lopetetaan, ei lopeteta, lopetettiin, ei lopetettu
- **luistella** (`3`) - korcsolyázni (*to ice skate*)  
  `P`: luistelen, luistelet, luistelee, luistelemme, luistelette, luistelevat  
  `PN`: en luistele, et luistele...  
  `K`: Luistelenko? Luisteletko?...  
  `I`: luistelin, luistelit, luisteli, luistelimme, luistelitte, luistelivat  
  `NI`: en/et/ei luistellut, emme/ette/eivät luistelleet  
  `PA`: luistellaan, ei luistella, luisteltiin, ei luisteltu
- **lukea** (`1`) - olvasni (*to read*)  
  `P`: luen, luet, lukee, luemme, luette, lukevat  
  `PN`: en lue, et lue...  
  `K`: Luenko? Luetko?...  
  `I`: luin, luit, luki, luimme, luitte, lukivat  
  `NI`: en/et/ei lukenut, emme/ette/eivät lukeneet  
  `PA`: luetaan, ei lueta, luettiin, ei luettu
- **luottaa** (`1`) - bízni (*to trust, to rely on*)  
  `P`: luotan, luotat, luottaa, luotamme, luotatte, luottavat  
  `PN`: en luota, et luota...  
  `K`: Luotanko? Luotatko?...  
  `I`: luotin, luotit, luotti, luotimme, luotitte, luottivat  
  `NI`: en/et/ei luottanut, emme/ette/eivät luottaneet  
  `PA`: luotetaan, ei luoteta, luotettiin, ei luotettu
- **lupata** (`4`) - ígérni (*to promise*)  
  `P`: lupaan, lupaat, lupaa, lupaamme, lupaatte, lupaavat  
  `PN`: en lupaa, et lupaa...  
  `K`: Lupaanko? Lupaatko?...  
  `I`: lupasin, lupasit, lupasi, lupasimme, lupasitte, lupasivat  
  `NI`: en/et/ei luvannut, emme/ette/eivät luvanneet  
  `PA`: luvataan, ei luvata, luvattiin, ei luvattu
- **luulla** (`3`) - hinni, gondolni (*to think, to suppose*)  
  `P`: luulen, luulet, luulee, luulemme, luulette, luulevat  
  `PN`: en luule, et luule...  
  `K`: Luulenko? Luuletko?...  
  `I`: luulin, luulit, luuli, luulimme, luulitte, luulivat  
  `NI`: en/et/ei luullut, emme/ette/eivät luulleet  
  `PA`: luullaan, ei luulla, luultiin, ei luultu
- **lyödä** (`2`) - ütni (*to hit, to strike*)  
  `P`: lyön, lyöt, lyö, lyömme, lyötte, lyövät  
  `PN`: en lyö, et lyö...  
  `K`: Lyönkö? Lyötkö?...  
  `I`: löin, löit, löi, löimme, löitte, löivät  
  `NI`: en/et/ei lyönyt, emme/ette/eivät lyöneet  
  `PA`: lyödään, ei lyödä, lyötiin, ei lyöty
- **lähteä** (`1`) - indulni (*to leave, to depart*)  
  `P`: lähden, lähdet, lähtee, lähdemme, lähdette, lähtevät  
  `PN`: en lähde, et lähde...  
  `K`: Lähdenkö? Lähdetkö?...  
  `I`: lähdin, lähdit, lähti, lähdimme, lähditte, lähtivät  
  `NI`: en/et/ei lähtenyt, emme/ette/eivät lähteneet  
  `PA`: lähdetään, ei lähdetä, lähdettiin, ei lähdetty
- **löytää** (`1`) - találni (*to find*)  
  `P`: löydän, löydät, löytää, löydämme, löydätte, löytävät  
  `PN`: en löydä, et löydä...  
  `K`: Löydänkö? Löydätkö?...  
  `I`: löysin, löysit, löysi, löysimme, löysitte, löysivät  
  `NI`: en/et/ei löytänyt, emme/ette/eivät löytäneet  
  `PA`: löydetään, ei löydetä, löydettiin, ei löydetty
- **maata** (`4`) - feküdni (*to lay*)  
  `P`: makaan, makaat, makaa, makaamme, makaatte, makaavat  
  `PN`: en makaa, et makaa...  
  `K`: Makaanko? Makaatko?...  
  `I`: makasin, makasit, makasi, makasimme, makasitte, makasivat  
  `NI`: en/et/ei maannut, emme/ette/eivät maanneet  
  `PA`: maataan, ei maata, maattiin, ei maattu
- **maistua** (`1`) - ízleni (*to taste (like something)*)  
  `P`: maistun, maistut, maistuu, maistumme, maistutte, maistuvat  
  `PN`: en maistu, et maistu...  
  `K`: Maistunko? Maistutko?...  
  `I`: maistuin, maistuit, maistui, maistuimme, maistuitte, maistuivat  
  `NI`: en/et/ei maistunut, emme/ette/eivät maistuneet  
  `PA`: maistutaan, ei maistuta, maistuttiin, ei maistuttu
- **maksaa** (`1`) - fizetni (*to pay*)  
  `P`: maksan, maksat, maksaa, maksamme, maksatte, maksavat  
  `PN`: en maksa, et maksa...  
  `K`: Maksanko? Maksatko?...  
  `I`: maksoin, maksoit, maksoi, maksoimme, maksoitte, maksoivat  
  `NI`: en/et/ei maksanut, emme/ette/eivät maksaneet  
  `PA`: maksetaan, ei makseta, maksettiin, ei maksettu
- **maksattaa** (`1`) - megfizettetni (*to make someone pay*)  
  `P`: maksatatan, maksatat, maksattaa, maksatamme, maksatatte, maksattavat  
  `PN`: en makseta, et makseta...  
  `K`: Maksatanko? Maksatatko?...  
  `I`: maksetin, maksetit, maksetti, maksetimme, maksetitte, maksettivat  
  `NI`: en/et/ei maksattanut, emme/ette/eivät maksattaneet  
  `PA`: maksatetaan, ei maksateta, maksatettiin, ei maksatettu
- **matkustaa** (`1`) - utazni (*to travel*)  
  `P`: matkustan, matkustat, matkustaa, matkustamme, matkustatte, matkustavat  
  `PN`: en matkusta, et matkusta...  
  `K`: Matkustanko? Matkustatko?...  
  `I`: matkustin, matkustit, matkusti, matkustimme, matkustitte, matkustivat  
  `NI`: en/et/ei matkustanut, emme/ette/eivät matkustaneet  
  `PA`: matkustetaan, ei matkusteta, matkustettiin, ei matkustettu
- **mennä** (`3`) - menni (*to go*)  
  `P`: menen, menet, menee, menemme, menette, menevät  
  `PN`: en mene, et mene...  
  `K`: Menenkö? Menetkö?...  
  `I`: menin, menit, meni, menimme, menitte, menivät  
  `NI`: en/et/ei mennyt, emme/ette/eivät menneet  
  `PA`: mennään, ei mennä, mentiin, ei menty
- **moittia** (`1`) - megszidni (*to scold, to criticize*)  
  `P`: moitin, moitit, moittii, moitimme, moititte, moittivat  
  `PN`: en moiti, et moiti...  
  `K`: Moitinko? Moititko?...  
  `I`: moitin, moitit, moitti, moitimme, moititte, moittivat  
  `NI`: en/et/ei moittinut, emme/ette/eivät moittineet  
  `PA`: moititaan, ei moitita, moitittiin, ei moitittu
- **murista** (`3`) - morogni (*to growl, to grumble*)  
  `P`: murisen, muriset, murisee, murisemme, murisette, murisevat  
  `PN`: en murise, et murise...  
  `K`: Murisenko? Murisetko?...  
  `I`: murisin, murisit, murisi, murisimme, murisitte, murisivat  
  `NI`: en/et/ei murissut, emme/ette/eivät murisseet  
  `PA`: muristaan, ei murista, muristiin, ei muristu
- **myydä** (`2`) - eladni (*to sell*)  
  `P`: myyn, myyt, myy, myymme, myytte, myyvät  
  `PN`: en myy, et myy...  
  `K`: Myynkö? Myytkö?...  
  `I`: myin, myit, myi, myimme, myitte, myivät  
  `NI`: en/et/ei myynyt, emme/ette/eivät myyneet  
  `PA`: myydään, ei myydä, myytiin, ei myyty
- **nauraa** (`1`) - nevetni (*to laugh*)  
  `P`: nauran, naurat, nauraa, nauramme, nauratte, nauravat  
  `PN`: en naura, et naura...  
  `K`: Nauranko? Nauratko?...  
  `I`: nauroin, nauroit, nauroi, nauroimme, nauroitte, nauroivat  
  `NI`: en/et/ei nauranut, emme/ette/eivät nauraneet  
  `PA`: nauretaan, ei naureta, naurettiin, ei naurettu
- **nousta** (`3`) - felkelni, felemelkedni (*to rise, to get up*)  
  `P`: nousen, nouset, nousee, nousemme, nousette, nousevat  
  `PN`: en nouse, et nouse...  
  `K`: Nousenko? Nousetko?...  
  `I`: nousin, nousit, nousi, nousimme, nousitte, nousivat  
  `NI`: en/et/ei noussut, emme/ette/eivät nousseet  
  `PA`: noustaan, ei nousta, noustiin, ei noustu
- **nukkua** (`1`) - aludni (*to sleep*)  
  `P`: nukun, nukut, nukkuu, nukumme, nukutte, nukkuvat  
  `PN`: en nuku, et nuku...  
  `K`: Nukunko? Nukutko?...  
  `I`: nukuin, nukuit, nukkui, nukuimme, nukuitte, nukkuivat  
  `NI`: en/et/ei nukkunut, emme/ette/eivät nukkuneet  
  `PA`: nukutaan, ei nukuta, nukuttiin, ei nukuttu
- **nähdä** (`2`) - látni (*to see*)  
  `P`: näen, näet, näkee, näemme, näette, näkevät  
  `PN`: en näe, et näe...  
  `K`: Näenkö? Näetkö?...  
  `I`: näin, näit, näki, näimme, näitte, näkivät  
  `NI`: en/et/ei nähnyt, emme/ette/eivät nähneet  
  `PA`: nähdään, ei nähdä, nähtiin, ei nähty


<a name="o-r"></a>
## O to R

- **odottaa** (`1`) - várni (*to wait*)  
  `P`: odotan, odotat, odottaa, odotamme, odotatte, odottavat  
  `PN`: en odota, et odota...  
  `K`: Odotanko? Odotatko?...  
  `I`: odotin, odotit, odotti, odotimme, odotitte, odottivat  
  `NI`: en/et/ei odottanut, emme/ette/eivät odottaneet  
  `PA`: odotetaan, ei odoteta, odotettiin, ei odotettu
- **oksentaa** (`1`) - hányni (*to vomit*)  
  `P`: oksennan, oksennat, oksentaa, oksennamme, oksennatte, oksentavat  
  `PN`: en oksenna, et oksenna...  
  `K`: Oksennanko? Oksennatko?...  
  `I`: oksensin, oksensit, oksensi, oksensimme, oksensitte, oksensivat  
  `NI`: en/et/ei oksentanut, emme/ette/eivät oksentaneet  
  `PA`: oksennetaan, ei oksenneta, oksennettiin, ei oksennettu
- **olla** (`3`) - lenni (*to be*)  
  `P`: olen, olet, on, olemme, olette, ovat  
  `PN`: en ole, et ole...  
  `K`: Olenko? Oletko?...  
  `I`: olin, olit, oli, olimme, olitte, olivat  
  `NI`: en/et/ei ollut, emme/ette/eivät olleet  
  `PA`: ollaan, ei olla, oltiin, ei oltu
- **opettaa** (`1`) - tanítani (*to teach*)  
  `P`: opetan, opetat, opettaa, opetamme, opetatte, opettavat  
  `PN`: en opeta, et opeta...  
  `K`: Opetanko? Opetatko?...  
  `I`: opetin, opetit, opetti, opetimme, opetitte, opettivat  
  `NI`: en/et/ei opettanut, emme/ette/eivät opettaneet  
  `PA`: opetetaan, ei opeteta, opetettiin, ei opetettu
- **opiskella** (`3`) - tanulni (*to study*)  
  `P`: opiskelen, opiskelet, opiskelee, opiskelemme, opiskelette, opiskelevat  
  `PN`: en opiskele, et opiskele...  
  `K`: Opiskelenko? Opiskeletko?...  
  `I`: opiskelin, opiskelit, opiskeli, opiskelimme, opiskelit, opiskeli  
  `NI`: en/et/ei opiskellut, emme/ette/eivät opiskelleet  
  `PA`: opiskellaan, ei opiskella, opiskeltiin, ei opiskeltu
- **oppia** (`1`) - megtanulni (*to learn*)  
  `P`: opin, opit, oppii, opimme, opitte, oppivat  
  `PN`: en opi, et opi...  
  `K`: Opinko? Opitko?...  
  `I`: opin, opit, oppi, opimme, opitte, oppivat  
  `NI`: en/et/ei oppinut, emme/ette/eivät oppineet  
  `PA`: opitaan, ei opita, opittiin, ei opittu
- **ostaa** (`1`) - vásárolni (*to buy*)  
  `P`: ostan, ostat, ostaa, ostamme, ostatte, ostavat  
  `PN`: en osta, et osta...  
  `K`: Ostanko? Ostatko?...  
  `I`: ostin, ostit, osti, ostimme, ostitte, ostivat  
  `NI`: en/et/ei ostanut, emme/ette/eivät ostaneet  
  `PA`: ostetaan, ei osteta, ostettiin, ei ostettu
- **ottaa** (`1`) - venni (*to take*)  
  `P`: otan, otat, ottaa, otamme, otatte, ottavat  
  `PN`: en ota, et ota...  
  `K`: Otanko? Otatko?...  
  `I`: otin, otit, otti, otimme, otitte, ottivat  
  `NI`: en/et/ei ottanut, emme/ette/eivät ottaneet  
  `PA`: otetaan, ei oteta, otettiin, ei otettu  
- **paskata** (`4`) - szarni (*to (take a) shit*)  
  `P`: paskannan, paskannat, paskantaa, paskannamme, paskannatte, paskantavat  
  `PN`: en paskanna, et paskanna...  
  `K`: Paskannanko? Paskannatko?...  
  `I`: paskansin, paskansit, paskansi, paskansimme, paskansitte, paskansivat  
  `NI`: en/et/ei paskantanut, emme/ette/eivät paskantaneet  
  `PA`: paskannetaan, ei paskanneta, paskannettiin, ei paskannettu
- **pehmetä** (`6`) - meglágyulni (*to soften*)  
  `P`: pehmenen, pehmenet, pehmenee, pehmenemme, pehmenette, pehmenevät  
  `PN`: en pehmene, et pehmene...  
  `K`: Pehmenenkö? Pehmenetkö?...  
  `I`: pehmenin, pehmenit, pehmeni, pehmenimme, pehmenitte, pehmenivät  
  `NI`: en/et/ei pehmennyt, emme/ette/eivät pehmenneet  
  `PA`: pehmennään, ei pehmennetä, pehmennettiin, ei pehmennetty
- **pelata** (`4`) - játszani (*to play [sports/games]*)  
  `P`: pelaan, pelaat, pelaa, pelaamme, pelaatte, pelaavat  
  `PN`: en pelaa, et pelaa...  
  `K`: Pelaanko? Pelaatko?...  
  `I`: pelasin, pelasit, pelasi, pelasimme, pelasitte, pelasivat  
  `NI`: en/et/ei pelannut, emme/ette/eivät pelanneet  
  `PA`: pelataan, ei pelata, pelattiin, ei pelattu
- **pelätä** (`4`) - félni (*to fear, to be afraid*)  
  `P`: pelkään, pelkäät, pelkää, pelkäämme, pelkäätte, pelkäävät  
  `PN`: en pelkää, et pelkää...  
  `K`: Pelkäänkö? Pelkäätkö?...  
  `I`: pelkäsin, pelkäsit, pelkäsi, pelkäsimme, pelkäsitte, pelkäsivät  
  `NI`: en/et/ei pelännyt, emme/ette/eivät pelänneet  
  `PA`: pelätään, ei pelätä, pelättiin, ei pelätty
- **pestä** (`3`) - mosni (*to wash*)  
  `P`: pesen, peset, pesee, pesemme, pesette, pesevät  
  `PN`: en pese, et pese...  
  `K`: Pesenkö? Pesetkö?...  
  `I`: pesin, pesit, pesi, pesimme, pesitte, pesivät  
  `NI`: en/et/ei pessyt, emme/ette/eivät pessyt  
  `PA`: pestään, ei pestä, pestiin, ei pesty
- **piirtää** (`1`) - rajzolni (*to draw*)  
  `P`: piirrän, piirrät, piirtää, piirrämme, piirrätte, piirtävät  
  `PN`: en piirrä, et piirrä...  
  `K`: Piirränkö? Piirrätkö?...  
  `I`: piirsin, piirsit, piirsi, piirsimme, piirsitte, piirsivät  
  `NI`: en/et/ei piirtänyt, emme/ette/eivät piirtäneet  
  `PA`: piirretään, ei piirretä, piirrettiin, ei piirretty
- **pitää** (`1`) - szeretni, tartani (*to like, to hold*)  
  `P`: pidän, pidät, pitää, pidämme, pidätte, pitävät  
  `PN`: en pidä, et pidä...  
  `K`: Pidänkö? Pidätkö?...  
  `I`: pidin, pidit, piti, pidimme, piditte, pitivät  
  `NI`: en/et/ei pitänyt, emme/ette/eivät pitäneet  
  `PA`: pidetään, ei pidetä, pidettiin, ei pidetty
- **potkaista** (`2`) - rúgni (*to kick*)  
  `P`: potkaisen, potkaiset, potkaisee, potkaisemme, potkaisette, potkaisevat  
  `PN`: en potkaise, et potkaise...  
  `K`: Potkaisenko? Potkaisetko?...  
  `I`: potkaisin, potkaisit, potkaisi, potkaisimme, potkaisitte, potkaisivat  
  `NI`: en/et/ei potkaissut, emme/ette/eivät potkaisseet  
  `PA`: potkaistaan, ei potkaista, potkaistiin, ei potkaistu
- **puhua** (`1`) - beszélni (*to speak, to talk*)  
  `P`: puhun, puhut, puhuu, puhumme, puhutte, puhuvat  
  `PN`: en puhu, et puhu...  
  `K`: Puhunko? Puhutko?...  
  `I`: puhuin, puhuit, puhui, puhuimme, puhuitte, puhuivat  
  `NI`: en/et/ei puhunut, emme/ette/eivät puhuneet  
  `PA`: puhutaan, ei puhuta, puhuttiin, ei puhuttu
- **pukea** (`1`) - öltözni (*to dress, to put on clothes*)  
  `P`: puen, puet, pukee, puemme, puette, pukevat  
  `PN`: en pue, et pue...  
  `K`: Puenko? Puetko?...  
  `I`: puin, puit, puki, puimme, puitte, pukivat  
  `NI`: en/et/ei pukenut, emme/ette/eivät pukeneet  
  `PA`: puetaan, ei pueta, puettiin, ei puettu
- **puraista** (`2`) - harapni (*to bite once*)  
  `P`: puraisen, puraiset, puraisee, puraisemme, puraisette, puraisevat  
  `PN`: en puraise, et puraise...  
  `K`: Puraisenko? Puraisetko?...  
  `I`: puraisin, puraisit, puraisi, puraisimme, puraisitte, puraisivat  
  `NI`: en/et/ei puraissut, emme/ette/eivät puraiseet  
  `PA`: puraistaan, ei puraista, puraistiin, ei puraistu
- **purra** (`3`) - harapni (*to bite (continuously)*)  
  `P`: puren, puret, puree, puremme, purette, purevat  
  `PN`: en pure, et pure...  
  `K`: Purenko? Puretko?...  
  `I`: purin, purit, puri, purimme, puritte, purivat  
  `NI`: en/et/ei purrut, emme/ette/eivät purreet  
  `PA`: purraan, ei purra, purtiin, ei purrtu
- **pyöräillä** (`3`) - biciklizni (*to cycle, to ride a bike*)  
  `P`: pyöräilen, pyöräilet, pyöräilee, pyöräilemme, pyöräilette, pyöräilevät  
  `PN`: en pyöräile, et pyöräile...  
  `K`: Pyöräilenkö? Pyöräiletkö?...  
  `I`: pyöräilin, pyöräilit, pyöräili, pyöräilimme, pyöräilitte, pyöräilivät  
  `NI`: en/et/ei pyöräillyt, emme/ette/eivät pyöräilleet  
  `PA`: pyöräillään, ei pyöräillä, pyöräiltiin, ei pyöräilty
- **rakastaa** (`1`) - szeretni (*to love*)  
  `P`: rakastan, rakastat, rakastaa, rakastamme, rakastatte, rakastavat  
  `PN`: en rakasta, et rakasta...  
  `K`: Rakastanko? Rakastatko?...  
  `I`: rakastin, rakastit, rakasti, rakastimme, rakastitte, rakastivat  
  `NI`: en/et/ei rakastanut, emme/ette/eivät rakastaneet  
  `PA`: rakastetaan, ei rakasteta, rakastettiin, ei rakastettu
- **rakastella** (`3`) - szeretkezni (*to make love*)  
  `P`: rakastelen, rakastelet, rakastelee, rakastelemme, rakastelette, rakastelevat  
  `PN`: en rakastele, et rakastele...  
  `K`: Rakastelenko? Rakasteletko?...  
  `I`: rakastelin, rakastelit, rakasteli, rakastelimme, rakastelitte, rakastelivat  
  `NI`: en/et/ei rakastellut, emme/ette/eivät rakastelleet  
  `PA`: rakastellaan, ei rakastella, rakasteltiin, ei rakasteltu
- **rakastua** (`1`) - beleszeretni (*to fall in love*)  
  `P`: rakastun, rakastut, rakastuu, rakastumme, rakastutte, rakastuvat  
  `PN`: en rakastu, et rakastu...  
  `K`: Rakastunko? Rakastutko?...  
  `I`: rakastuin, rakastuit, rakastui, rakastuimme, rakastuitte, rakastuivat  
  `NI`: en/et/ei rakastunut, emme/ette/eivät rakastuneet  
  `PA`: rakastutaan, ei rakastuta, rakastuttiin, ei rakastuttu
- **rakentaa** (`1`) - építeni (*to build*)  
  `P`: rakennan, rakennat, rakentaa, rakennamme, rakennatte, rakentavat  
  `PN`: en rakenna, et rakenna...  
  `K`: Rakennanko? Rakennatko?...  
  `I`: rakensin, rakensit, rakensi, rakensimme, rakensitte, rakensivat  
  `NI`: en/et/ei rakentanut, emme/ette/eivät rakentaneet  
  `PA`: rakennetaan, ei rakenneta, rakennettiin, ei rakennettu
- **rauhoittua** (`1`) - megnyugodni (*to calm down*)  
  `P`: rauhoitun, rauhoitut, rauhoittuu, rauhoitumme, rauhoitutte, rauhoittuvat  
  `PN`: en rauhoitu, et rauhoitu...  
  `K`: Rauhoitunko? Rauhoitutko?...  
  `I`: rauhoituin, rauhoituit, rauhoittui, rauhoituimme, rauhoituitte, rauhoittuivat  
  `NI`: en/et/ei rauhoittunut, emme/ette/eivät rauhoittuneet  
  `PA`: rauhoitutaan, ei rauhoituta, rauhoituttiin, ei rauhoituttu
- **riisua** (`1`) - levenni, levetkőzni (*to undress, to take off clothes*)  
  `P`: riisun, riisut, riisuu, riisumme, riisutte, riisuvat  
  `PN`: en riisu, et riisu...  
  `K`: Riisunko? Riisutko?...  
  `I`: riisuin, riisuit, riisui, riisuimme, riisuitte, riisuivat  
  `NI`: en/et/ei riisunut, emme/ette/eivät riisuneet  
  `PA`: riisutaan, ei riisuta, riisuttiin, ei riisuttu
- **rikkoa** (`1`) - eltörni, megszegni (*to break, to violate*)  
  `P`: rikon, rikot, rikkoo, rikomme, rikotte, rikkovat  
  `PN`: en riko, et riko...  
  `K`: Rikonko? Rikotko?...  
  `I`: rikoin, rikoit, rikkoi, rikoimme, rikoitte, rikkoivat  
  `NI`: en/et/ei rikkonut, emme/ette/eivät rikkoneet  
  `PA`: rikotaan, ei rikota, rikottiin, ei rikottu
- **ruokkia** (`1`) - etetni (*to feed*)  
  `P`: ruokin, ruokit, ruokkii, ruokimme, ruokitte, ruokkivat  
  `PN`: en ruoki, et ruoki...  
  `K`: Ruokinko? Ruokitko?...  
  `I`: ruokin, ruokit, ruokki, ruokimme, ruokitte, ruokkivat  
  `NI`: en/et/ei ruokkinut, emme/ette/eivät ruokkineet  
  `PA`: ruokitaan, ei ruokita, ruokittiin, ei ruokittu
- **ruveta** (`4`) - elkezdeni (*to start, to begin doing something*)  
  `P`: rupean, rupeat, rupeaa, rupeamme, rupeatte, rupeavat  
  `PN`: en rupea, et rupea...  
  `K`: Rupeanko? Rupeatko?...  
  `I`: rupesin, rupesit, rupesi, rupesimme, rupesitte, rupesivat  
  `NI`: en/et/ei ruvennut, emme/ette/eivät ruvenneet  
  `PA`: ruvetaan, ei ruveta, ruvettiin, ei ruvettu


<a name="s-ö"></a>
## S to Ö

<img src="/_ui/img/imgfx/logistics.png" title="" align="right" class="right">

- **saada** (`2`) - kapni (*to get, to receive*)  
  `P`: saan, saat, saa, saamme, saatte, saavat  
  `PN`: en saa, et saa...  
  `K`: Saanko? Saatko?...  
  `I`: sain, sait, sai, saimme, saitte, saivat  
  `NI`: en/et/ei saanut, emme/ette/eivät saaneet  
  `PA`: saadaan, ei saada, saatiin, ei saatu
- **saattaa** (`1`) - elkísérni (*to accompany, to escort*)  
  `P`: saatan, saatat, saattaa, saatamme, saatatte, saattavat  
  `PN`: en saata, et saata...  
  `K`: Saatanko? Saatatko?...  
  `I`: saatoin, saatoit, saattoi, saatoimme, saatoitte, saattoivat  
  `NI`: en/et/ei saattanut, emme/ette/eivät saattaneet  
  `PA`: saatetaan, ei saateta, saatettiin, ei saatettu
- **sanoa** (`1`) - mondani (*to say*)  
  `P`: sanon, sanot, sanoo, sanomme, sanotte, sanovat  
  `PN`: en sano, et sano...  
  `K`: Sanonko? Sanotko?...  
  `I`: sanoin, sanoit, sanoi, sanoimme, sanoitte, sanoivat  
  `NI`: en/et/ei sanonut, emme/ette/eivät sanoneet  
  `PA`: sanotaan, ei sanota, sanottiin, ei sanottu
- **saunoa** (`1`) - szaunázni (*to sauna*)  
  `P`: saunon, saunot, saunoo, saunomme, saunotte, saunovat  
  `PN`: en sauno, et sauno...  
  `K`: Saunonko? Saunotko?...  
  `I`: saunoin, saunoit, saunoi, saunoimme, saunoitte, saunoivat  
  `NI`: en/et/ei saunonut, emme/ette/eivät saunoneet  
  `PA`: saunotaan, ei saunota, saunottiin, ei saunottu
- **seisoa** (`1`) - állni (*to stand*)  
  `P`: seison, seisot, seisoo, seisomme, seisotte, seisovat  
  `PN`: en seiso, et seiso...  
  `K`: Seisonko? Seisotko?...  
  `I`: seisoin, seisoit, seisoi, seisoimme, seisoitte, seisoivat  
  `NI`: en/et/ei seisonut, emme/ette/eivät seisoneet  
  `PA`: seistään, ei seistä, seistiin, ei seisty
- **siivota** (`4`) - takarítani (*to clean*)  
  `P`: siivoan, siivoat, siivoaa, siivoamme, siivoatte, siivoavat  
  `PN`: en siivoa, et siivoa...  
  `K`: Siivoanko? Siivoatko?...  
  `I`: siivosin, siivosit, siivosi, siivosimme, siivositte, siivosivat  
  `NI`: en/et/ei siivonnut, emme/ette/eivät siivonneet  
  `PA`: siivotaan, ei siivota, siivottiin, ei siivottu
- **sijaita** (`5`) - elhelyezkedni (*to be located*)  
  `P`: sijaitsen, sijaitset, sijaitsee, sijaitsemme, sijaitsette, sijaitsevat  
  `PN`: en sijaitse, et sijaitse...  
  `K`: Sijaitsenko? Sijaitsetko?...  
  `I`: sijaitsin, sijaitsit, sijaitsi, sijaitsimme, sijaitsitte, sijaitsivat  
  `NI`: en/et/ei sijainnut, emme/ette/eivät sijainneet  
  `PA`: sijaitaan, ei sijaita, sijaittiin, ei sijaittu
- **soida** (`2`) - szólni, csörögni (*to ring, to sound*)  
  `P`: soin, soit, soi, soimme, soitte, soivat  
  `PN`: en soi, et soi...  
  `K`: Soinko? Soitko?...  
  `I`: soin, soit, soi, soimme, soitte, soivat  
  `NI`: en/et/ei soinut, emme/ette/eivät soineet  
  `PA`: soidaan, ei soida, soitiin, ei soitu
- **soittaa** (`1`) - játszani (hangszeren), hívni (*to play (instrument), to call*)  
  `P`: soitan, soitat, soittaa, soitamme, soitatte, soittavat  
  `PN`: en soita, et soita...  
  `K`: Soitanko? Soitatko?...  
  `I`: soitin, soitit, soitti, soitimme, soititte, soittivat  
  `NI`: en/et/ei soittanut, emme/ette/eivät soittaneet  
  `PA`: soitetaan, ei soiteta, soitettiin, ei soitettu
- **surra** (`3`) - gyászolni (*to grieve, to mourn*)  
  `P`: suren, suret, suree, suremme, surette, surevat  
  `PN`: en sure, et sure...  
  `K`: Surenko? Suretko?...  
  `I`: surin, surit, suri, surimme, suritte, surivat  
  `NI`: en/et/ei surrut, emme/ette/eivät surreet  
  `PA`: surraan, ei surra, surtiin, ei surru
- **syödä** (`2`) - enni (*to eat*)  
  `P`: syön, syöt, syö, syömme, syötte, syövät  
  `PN`: en syö, et syö...  
  `K`: Syönkö? Syötkö?...  
  `I`: söin, söit, söi, söimme, söitte, söivät  
  `NI`: en/et/ei syönyt, emme/ette/eivät syöneet  
  `PA`: syödään, ei syödä, syötiin, ei syöty
- **tanssia** (`1`) - táncolni (*to dance*)  
  `P`: tanssin, tanssit, tanssii, tanssimme, tanssitte, tanssivat  
  `PN`: en tanssi, et tanssi...  
  `K`: Tanssinko? Tanssitko?...  
  `I`: tanssin, tanssit, tanssi, tanssimme, tanssitte, tanssivat  
  `NI`: en/et/ei tanssinut, emme/ette/eivät tanssineet  
  `PA`: tanssitaan, ei tanssita, tanssittiin, ei tanssittu
- **tarvita** (`5`) - szüksége van valamire (*to need*)  
  `P`: tarvitsen, tarvitset, tarvitsee, tarvitsemme, tarvitsette, tarvitsevat  
  `PN`: en tarvitse, et tarvitse...  
  `K`: Tarvitsenko? Tarvitsetko?...  
  `I`: tarvitsin, tarvitsit, tarvitsi, tarvitsimme, tarvitsitte, tarvitsivat  
  `NI`: en/et/ei tarvinnut, emme/ette/eivät tarvinneet  
  `PA`: tarvitaan, ei tarvita, tarvittiin, ei tarvittu
- **tavata** (`4`) - találkozni (*to meet*)  
  `P`: tapaan, tapaat, tapaa, tapaamme, tapaatte, tapaavat  
  `PN`: en tapaa, et tapaa...  
  `K`: Tapaanko? Tapaatko?...  
  `I`: tapasin, tapasit, tapasi, tapasimme, tapasitte, tapasivat  
  `NI`: en/et/ei tavannut, emme/ette/eivät tavanneet  
  `PA`: tavataan, ei tavata, tavattiin, ei tavattu
- **tehdä** (`2`) - csinálni (*to do, to make*)  
  `P`: teen, teet, tekee, teemme, teette, tekevät  
  `PN`: en tee, et tee...  
  `K`: Teenkö? Teetkö?...  
  `I`: tein, teit, teki, teimme, teitte, tekivät  
  `NI`: en/et/ei tehnyt, emme/ette/eivät tehneet  
  `PA`: tehdään, ei tehdä, tehtiin, ei tehty
- **tilata** (`4`) - rendelni (*to order, to subscribe*)  
  `P`: tilaan, tilaat, tilaa, tilaamme, tilaatte, tilaavat  
  `PN`: en tilaa, et tilaa...  
  `K`: Tilanko? Tilaatko?...  
  `I`: tilasin, tilasit, tilasi, tilasimme, tilasitte, tilasivat  
  `NI`: en/et/ei tilannut, emme/ette/eivät tilanneet  
  `PA`: tilataan, ei tilata, tilattiin, ei tilattu
- **tiskata** (`4`) - mosogatni (*to wash the dishes*)  
  `P`: tiskaan, tiskaat, tiskaa, tiskaamme, tiskaatte, tiskaavat  
  `PN`: en tiskaa, et tiskaa...  
  `K`: Tiskaanko? Tiskaatko?...  
  `I`: tiskasin, tiskasit, tiskasi, tiskasimme, tiskasitte, tiskasivat  
  `NI`: en/et/ei tiskannut, emme/ette/eivät tiskanneet  
  `PA`: tiskataan, ei tiskata, tiskattiin, ei tiskattu
- **tulla** (`3`) - jönni (*to come*)  
  `P`: tulen, tulet, tulee, tulemme, tulette, tulevat  
  `PN`: en tule, et tule...  
  `K`: Tulenko? Tuletko?...  
  `I`: tulin, tulit, tuli, tulimme, tulitte, tulivat  
  `NI`: en/et/ei tullut, emme/ette/eivät tulleet  
  `PA`: tullaan, ei tulla, tultiin, ei tultu
- **tupakoida** (`2`) - dohányozni (*to smoke*)  
  `P`: tupakoin, tupakoit, tupakoi, tupakoimme, tupakoitte, tupakoivat  
  `PN`: en tupakoi, et tupakoi...  
  `K`: Tupakoinko? Tupakoitko?...  
  `I`: tupakoin, tupakoit, tupakoi, tupakoimme, tupakoitte, tupakoivat  
  `NI`: en/et/ei tupakoinut, emme/ette/eivät tupakoineet  
  `PA`: tupakoidaan, ei tupakoida, tupakoitiin, ei tupakoitu
- **tutustua** (`1`) - megismerkedni (*to get to know*)  
  `P`: tutustun, tutustut, tutustuu, tutustumme, tutustutte, tutustuvat  
  `PN`: en tutustu, et tutustu...  
  `K`: Tutustunko? Tutustutko?...  
  `I`: tutustuin, tutustuit, tutustui, tutustuimme, tutustuitte, tutustuivat  
  `NI`: en/et/ei tutustunut, emme/ette/eivät tutustuneet  
  `PA`: tutustutaan, ei tutustuta, tutustuttiin, ei tutustuttu
- **tyhjentää** (`1`) - kiüríteni (*to empty, to clear*)  
  `P`: tyhjennän, tyhjennät, tyhjentää, tyhjennämme, tyhjennätte, tyhjentävät  
  `PN`: en tyhjennä, et tyhjennä...  
  `K`: Tyhjennänkö? Tyhjennätkö?...  
  `I`: tyhjensin, tyhjensit, tyhjensi, tyhjensimme, tyhjensitte, tyhjensivät  
  `NI`: en/et/ei tyhjentänyt, emme/ette/eivät tyhjentäneet  
  `PA`: tyhjennetään, ei tyhjennetä, tyhjennettiin, ei tyhjennetty
- **työskennellä** (`3`) - dolgozni (*to work*)  
  `P`: työskentelen, työskentelet, työskentelee, työskentelemme, työskentelette, työskentelevat  
  `PN`: en työskentele, et työskentele...  
  `K`: Työskentelenkö? Työskenteletkö?...  
  `I`: työskentelin, työskentelit, työskenteli, työskentelimme, työskentelitte, työskentelivät  
  `NI`: en/et/ei työskennellyt, emme/ette/eivät työskennelleet  
  `PA`: työskennellään, ei työskennellä, työskenneltiin, ei työskennelty  
- **täyttää** (`1`) - betölteni (*to fill, to fulfill*)  
  `P`: täytän, täytät, täyttää, täytämme, täytätte, täyttävät  
  `PN`: en täytä, et täytä...  
  `K`: Täytänkö? Täytätkö?...  
  `I`: täytin, täytit, täytti, täytimme, täytitte, täyttivät  
  `NI`: en/et/ei täyttänyt, emme/ette/eivät täyttäneet  
  `PA`: täytetään, ei täytetä, täytettiin, ei täytetty
- **uida** (`2`) - úszni (*to swim*)  
  `P`: uin, uit, ui, uimme, uitte, uivat  
  `PN`: en ui, et ui...  
  `K`: Uinko? Uitko?...  
  `I`: uin, uit, ui, uimme, uitte, uivat  
  `NI`: en/et/ei uinut, emme/ette/eivät uineet  
  `PA`: uidaan, ei uida, uitiin, ei uitu
- **unohtaa** (`1`) - elfelejteni (*to forget*)  
  `P`: unohdan, unohdat, unohtaa, unohdamme, unohdatte, unohtavat  
  `PN`: en unohda, et unohda...  
  `K`: Unohdanko? Unohdatko?...  
  `I`: unohdin, unohdit, unohti, unohdimme, unohditte, unohtivat  
  `NI`: en/et/ei unohtanut, emme/ette/eivät unohtaneet  
  `PA`: unohdetaan, ei unohdeta, unohdettiin, ei unohdettu
- **urheilla** (`3`) - sportolni (*to exercise, to do sports*)  
  `P`: urheilen, urheilet, urheilee, urheilemme, urheilette, urheilevat  
  `PN`: en urheile, et urheile...  
  `K`: Urheilenko? Urheiletko?...  
  `I`: urheilin, urheilit, urheili, urheilimme, urheilitte, urheilivat  
  `NI`: en/et/ei urheillut, emme/ette/eivät urheilleet  
  `PA`: urheillaan, ei urheilla, urheiltiin, ei urheiltu
- **uskoa** (`1`) - hinni, gondolni (*to believe, to think*)  
  `P`: uskon, uskot, uskoo, uskomme, uskotte, uskovat  
  `PN`: en usko, et usko...  
  `K`: Uskonko? Uskotko?...  
  `I`: uskoin, uskoit, uskoi, uskoimme, uskoitte, uskoivat  
  `NI`: en/et/ei uskonut, emme/ette/eivät uskoneet  
  `PA`: uskotaan, ei uskota, uskottiin, ei uskottu
- **valita** (`5`) - választani (*to choose*)  
  `P`: valitsen, valitset, valitsee, valitsemme, valitsette, valitsevat  
  `PN`: en valitse, et valitse...  
  `K`: Valitsenko? Valitsetko?...  
  `I`: valitsin, valitsit, valitsi, valitsimme, valitsitte, valitsivat  
  `NI`: en/et/ei valinnut, emme/ette/eivät valinneet  
  `PA`: valitaan, ei valita, valittiin, ei valittu
- **valmistautua** (`1`) - felkészülni (*to prepare oneself*)  
  `P`: valmistaudun, valmistaudut, valmistautuu, valmistaudumme, valmistaudutte, valmistautuvat  
  `PN`: en valmistaudu, et valmistaudu...  
  `K`: Valmistaudunko? Valmistaudutko?...  
  `I`: valmistauduin, valmistauduit, valmistautui, valmistauduimme, valmistauduitte, valmistautuivat  
  `NI`: en/et/ei valmistautunut, emme/ette/eivät valmistautuneet  
  `PA`: valmistaudutaan, ei valmistauduta, valmistauduttiin, ei valmistauduttu
- **vanheta** (`6`) - öregedni (*to grow old*)  
  `P`: vanhenen, vanhenet, vanhenee, vanhenemme, vanhenette, vanhenevat  
  `PN`: en vanhene, et vanhene...  
  `K`: Vanhenenko? Vanhenetko?...  
  `I`: vanhenin, vanhenit, vanheni, vanhenimme, vanhenitte, vanhenivat  
  `NI`: en/et/ei vanhennut, emme/ette/eivät vanhenneet  
  `PA`: vanhetaan, ei vanheta, vanhettiin, ei vanhettu
- **viedä** (`2`) - vinni (*to take (away)*)  
  `P`: vien, viet, vie, viemme, viette, vievät  
  `PN`: en vie, et vie...  
  `K`: Vienkö? Vietkö?...  
  `I`: vein, veit, vei, veimme, veitte, veivät  
  `NI`: en/et/ei vienyt, emme/ette/eivät vieneet  
  `PA`: viedään, ei viedä, vietiin, ei viety
- **viettää** (`1`) - tölteni, ünnepelni (*to spend (time), to celebrate*)  
  `P`: vietän, vietät, viettää, vietämme, vietätte, viettävät  
  `PN`: en vietä, et vietä...  
  `K`: Vietänkö? Vietätkö?...  
  `I`: vietin, vietit, vietti, vietimme, vietitte, viettivät  
  `NI`: en/et/ei viettänyt, emme/ette/eivät viettäneet  
  `PA`: vietetään, ei vietetä, vietettiin, ei vietetty
- **voida** (`2`) - tudni, képesnek lenni (*to be able to, can*)  
  `P`: voin, voit, voi, voimme, voitte, voivat  
  `PN`: en voi, et voi...  
  `K`: Voinko? Voitko?...  
  `I`: voin, voit, voi, voimme, voitte, voivat  
  `NI`: en/et/ei voinut, emme/ette/eivät voineet  
  `PA`: voidaan, ei voida, voitiin, ei voitu
- **yrittää** (`1`) - próbálni (*to try, to attempt*)  
  `P`: yritän, yrität, yrittää, yritämme, yritätte, yrittävät  
  `PN`: en yritä, et yritä...  
  `K`: Yritänkö? Yritätkö?...  
  `I`: yritin, yritit, yritti, yritimme, yrititte, yrittivät  
  `NI`: en/et/ei yrittänyt, emme/ette/eivät yrittäneet  
  `PA`: yritetään, ei yritetä, yritettiin, ei yritetty
- **ymmärtää** (`1`) - érteni (*to understand*)  
  `P`: ymmärrän, ymmärrät, ymmärtää, ymmärrämme, ymmärrätte, ymmärtävät  
  `PN`: en ymmärrä, et ymmärrä...  
  `K`: Ymmärränkö? Ymmärrätkö?...  
  `I`: ymmärsin, ymmärsit, ymmärsi, ymmärsimme, ymmärsitte, ymmärsivät  
  `NI`: en/et/ei ymmärtänyt, emme/ette/eivät ymmärtäneet  
  `PA`: ymmärretään, ei ymmärretä, ymmärrettiin, ei ymmärretty  
