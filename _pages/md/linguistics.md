# 🙊 Linguistics

![alt_text](linguistics_-_bg.jpg)

## Articles on Linguistics

- ![alt text](/_ui/img/flags/fin_s.svg) [Finnish](/linguistics/finnish)
- ![alt text](/_ui/img/flags/rusyn_s.svg) [Rusyn](/linguistics/rusyn)
