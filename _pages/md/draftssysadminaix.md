# ![alt text](/_ui/img/icons/aix_m.svg) AIX

<div style="text-align: right;"><h7>Posted: 2022-09-21 17:21</h7></div>

###### .

![alt_text](aix_-_bg.jpg)


## Table of Contents

<img src="p770_bg.jpg" width="400px" title="" align="right" class="right">

1. [Introduction](#1)
2. [Basic AIX Administration Tasks](#2)
3. [Standard AIX Installation](#3)
4. [AIX LVM and Filesystems](#4)
5. [AIX Networking](#5)
6. [Backup](#6)
7. [NIM](#7)


## Preface

This subpage tries to deal with, and focus on **AIX** itself, as an OS, rather than seriously touching the **IBM Power Virtualization** area. There is [another subpage](/learning/it/sysadmin/powervm) for that. 

The other, very close subject is [IBM PowerHA SystemMirror](/learning/it/sysadmin/powerha). 

Most of the chapters below are not meant for total beginner AIX admins (e.g. probably the most fundamental `lsuser` and `lsvg` commands won't be detailed), while they are not complete sets of manuals/instructions either. Mostly, they reflect to my happy years of experiense with AIX administration. Please, keep these in mind when reading. 

>The contents are just being figured out, that's why you can see many `...` (empty) sections. 


<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Basic AIX Administration Tasks

...

### 2.1 User Administration

...

#### 2.1.1 Some useful commands

- `lsuser -a registry SYSTEM ALL` - listing two specific attributes (`registry` and `SYSTEM`) of ALL the users (following the system default registry as source)
- `lsuser -C -a id groups home gecos ALL | grep -v ^#name:` - quick user list, with `:` separated fields
- `chgrpmem -m + user1,user2 group1` - adding `user1` and `user2` into `group1`
- `pwdadm -c user1` - get rid of the `ADMCHG` flag from `user1` in `/etc/security/passwd` after its password was recently changed by `root`

#### 2.1.2 Checking and modifying the default attributes

Basics: 

```ksh
# lssec -f /etc/security/user -s default -a minlen
default minlen=20

# chsec -f /etc/security/user -s default -a "minlen=8"

# lssec -f /etc/security/user -s default -a minlen
default minlen=8
```

Password hashing algorithm (further details about it can be found [here](https://www.ibm.com/support/pages/node/670029)): 

```ksh
# lssec -f /etc/security/login.cfg -s usw -a pwd_algorithm
usw pwd_algorithm=ssha1

# lssec -f /etc/security/pwdalg.cfg -s ssha1 -a ALL
ssha1 lpa_module=/usr/lib/security/ssha lpa_options=algorithm=sha1
```

### 2.2 Fileset (package) management

...

#### 2.2.1 Some useful commands

- `bffcreate -c -d .` - converting the recently downloaded "image name" packages into "package name" (more self-explanatory) format
- `installp -u X11.fnt.ucs.ttf` - removing a fileset
- `installp -a -XYd . X11.fnt.ucs.ttf` - installing a particular fileset under a directory, if that's a base level fileset
- `installp -c all` - committing any possible updates
- `installp -a -F -XYd . X11.Dt.ToolTalk` - forcingly reinstalling a particular fileset
- `install_all_updates -Ypd .` - applying all the possible update packages under a directory, in **preview** mode
- `install_all_updates -Yd .` - applying all the possible update packages under a directory without any confirmation

...


### 2.3 System boot and reboot

...

#### 2.3.1 Reboot

Before doing any reboots on a real server, a preliminary *config check* is always a good idea. Here is a oneliner, which lists includes also checking PowerHA and VIOS components if any: 

```ksh
# s () { comm=$1 ; echo "[ ${comm} ]" ; $comm ; echo ----- ; } ; s "hostname" ; s "date" ; s "lparstat -i" ; s "oslevel -s" ; s "lppchk -v" ; s "bootlist -m normal -o" ; s "lsdev" ; s "lssrc -a" ; s "ifconfig -a" ; s "netstat -rn" ; s "lsattr -El inet0" ; s "lspv" ; s "df -m" ; which cltopinfo >/dev/null 2>&1 ; RC=$? ; if [ $RC = 0 ] ; then s "halevel -s" ; echo "[ cluster filesets ]" ; lslpp -L | grep " cluster\." ; s "cltopinfo" ; s "cllsserv" ; s "cllsappmon" ; s "cllsvg" ; s "cllsfs" ; s "cllistlogs" ; else echo "No PowerHA" ; echo ----- ; fi ; ls -l /usr/ios/cli/ioscli > /dev/null 2>&1 ; RC=$? ; if [ $RC = 0 ] ; then s "/usr/ios/cli/ioscli ioslevel" ; s "/usr/ios/cli/ioscli lsrep" ; s "/usr/ios/cli/ioscli lsmap -all" ; s "/usr/ios/cli/ioscli lsmap -all -npiv" ; else echo "Not a VIOS" ; echo ----- ; fi ; s "ps -ef"
[ hostname ]
aixsrv1
-----
[ date ]
Tue Aug 24 13:37:24 CEST 2021
-----
[ lparstat -i ]
Node Name                                  : aixsrv1
Partition Name                             : aixsrv1
Partition Number                           : 58
...
```

Final bootlist check, then reboot: 

```ksh
# bootlist -m normal -o ; ls -l /dev/r$(bootlist -m normal -o | grep hdisk | head -1 | awk '{print $1}') /dev/ipldevice
hdisk0 blv=hd5 pathid=4
hdisk0 blv=hd5 pathid=5
hdisk0 blv=hd5 pathid=6
hdisk0 blv=hd5 pathid=7
crw-------    1 root     system       18,  0 Jun 03 15:49 /dev/rhdisk0
crw-------    2 root     system       18,  0 Jul 06 2017  /dev/ipldevice

# shutdown -Fr
...
```


...

<a name="3"></a>
## 3. Standard AIX Installation

There are several methods to get an AIX system freshly installed. In case you possess an official AIX Installer media in `.iso` format, and you would do its standard "next-next-finish" procedure, you already have several possibilities. If we consider the different types of `mksysb` restore solutions, the selection of possibilities are even way wider. 

[IBM summarizes](https://www.ibm.com/docs/en/aix/7.2?topic=installing-scenarios-aix) most of the common installation methods on their site.

First, let's take a quick overview on some possible, quite typical scenarios for standard AIX Installation.

### 3.1 Standard AIX Installation Scenarios

These are only the concepts, not the detailed set of instructions. 

#### 3.1.1 Using a VIOS Virtual Media Repository

It obviously requires an **IBM PowerVM environment** with at least one VIOS (a pair of them is always preferred) and a target LPAR. 

1. Copy the ISO image to a VIOS
2. Put the ISO image to a Virtual Media Repository
3. Create or ensure the VIOS Client, the target LPAR has a `vhost` virtual adapter defined on the VIOS
4. Map (`loadopt`) the Virtual Media to the `vhost` virtual adapter
5. Boot the target host into SMS menu, select the `SCSI CD-ROM` as boot device, and proceed with the installation

#### 3.1.2 Using a NIM Server

Apart from having also an IBM PowerVM environment, you need to have another LPAR which has the **NIM Server filesets** installed. 

1. Copy the ISO image to a NIM Server and extract it or mount it as a loop device.
2. Create the `lpp_source` and SPOT resources, using the installer
3. Ensure the NIM network is properly prepared
4. Boot the target host into SMS menu, select `Setup Remote IPL` --> BOOTP, IP Parameters, do a Ping Test, select Network from boot options, and proceed with the installation

#### 3.1.3 Installing AIX on Linux QEMU

OK, this is a really extreme case, but I believe, it's not that difficult, and worth the effort. If you are **lacking an available IBM Power machine at hand**, then [AIX on Linux QEMU](/learning/it/sysadmin/qemu#2) can be a good alternative to set this up, then continuing **practicing** on it. 

1. Copy the ISO image to your selected x86_64 or aarch64 host
2. Install the prerequsite QEMU packages
3. Create a virtual disk with at least 10 GB "virtual" space on it
4. Compose the command to start the VM, and proceed with the installation by
  - emulating an eligible hardware
  - specifying the virtual disk and the virtual CD-ROM
  - defining booting from the virtual CD-ROM

Of course, the performance and the range of enterprise features are far not the same as working on a real Power Systems environment, but at least somewhat close. 

### 3.2 The Installation Procedure

The AIX 7.2 Installer offers two installation methods depending on your goal: 

- **New and Complete Overwrite**: Overwrites **everything** on the disk selected for installation.  
  Only use this method if the disk is totally empty or if there is nothing on the disk you want to preserve.
- **Preservation Install**: Preserves **some** of the existing data on the disk selected for installation.  
  This method overwrites the usr (`/usr`), variable (`/var`), temporary (`/tmp`), and root (`/`) file systems. Other product (applications) files and configuration data will be destroyed too.
- **Migration**: This method upgrades from earlier versions of the AIX BOS to the current one.  
  This method preserves most of the file systems, including `rootvg`, logical volumes, and system configuration files and overwrites `/tmp`. 

The following "screens" are from a **New and Complete Overwrite**, but the outputs are mostly the same for all the three of them. During **Migration**, you may get some tricky questions during that, but normally that also goes smoothly. 

1. Whatever was your method to get the install media booted, you will get the next prompt first. Start the actual installation by pressing `1` and Enter first to select the **System Console**: 

```ksh
[...]
 ******* Please define the System Console. *******

Type a 1 and press Enter to use this terminal as the
  system console.
Pour definir ce terminal comme console systeme, appuyez
  sur 1 puis sur Entree.
Taste 1 und anschliessend die Eingabetaste druecken, um
  diese Datenstation als Systemkonsole zu verwenden.
Premere il tasto 1 ed Invio per usare questo terminal
  come console.
Escriba 1 y pulse Intro para utilizar esta terminal como
  consola del sistema.
Escriviu 1 1 i premeu Intro per utilitzar aquest
  terminal com a consola del sistema.
Digite um 1 e pressione Enter para utilizar este terminal
  como console do sistema.
[...]
```

2. Select **language**: 

```ksh
>>>  1 Type 1 and press Enter to have English during install.
     2 Entreu 2 i premeu Intro per veure la instal�laci� en catal�.
     3 Entrez 3 pour effectuer l'installation en fran�ais.
     4 F�r Installation in deutscher Sprache 4 eingeben
        und die Eingabetaste dr�cken.
     5 Immettere 5 e premere Invio per l'installazione in Italiano.
     6 Digite 6 e pressione Enter para usar Portugu�s na instala��o.
     7 Escriba 7 y pulse Intro para la instalaci�n en espa�ol.

    88  Help ?



>>> Choice [1]: 1
```

3. It is always important to at least review the **Installation Settings**, so select `2` here: 

```ksh
                      Welcome to Base Operating System
                      Installation and Maintenance

Type the number of your choice and press Enter.  Choice is indicated by >>>.

>>> 1 Start Install Now with Default Settings

    2 Change/Show Installation Settings and Install

    3 Start Maintenance Mode for System Recovery

    4 Make Additional Disks Available

    5 Select Storage Adapters



    88  Help ?
    99  Previous Menu

>>> Choice [1]: 2
```

4. Here is the main menu of the installer where you may want to select the **Method**, the **Disk**, the **Edition** and the **Software Install Options**: 

```ksh
                          Installation and Settings

Either type 0 and press Enter to install with current settings, or type the
number of the setting you want to change and press Enter.

    1  System Settings:
         Method of Installation.............New and Complete Overwrite
         Disk Where You Want to Install.....hdisk0

    2  Primary Language Environment Settings (AFTER Install):
         Cultural Convention................English (United States)
         Language ..........................English (United States)
         Keyboard ..........................English (United States)
         Keyboard Type......................Default
    3  Security Model.......................Default                   
    4  More Options  (Software install options)
    5  Select Edition.......................standard
>>> 0  Install with the current settings listed above.

                       +-----------------------------------------------------
    88  Help ?         |    WARNING: Base Operating System Installation will
    99  Previous Menu  |    destroy or impair recovery of ALL data on the
                       |    destination disk hdisk0.
>>> Choice [0]: 4
```

5. The **Software Install Options**: 

You may want to install **OpenSSH Server** right from here, under the `4 More Options` menu. Otherwise, you need to do it later, when the system is ready (by `mount -v cdrfs -o ro /dev/cd0 /mnt`, and so on). 

By default, the following fileset groups are set for an AIX 7.2 TL5 SP4 installer: 

```ksh
                             Install Options

 1.  Graphics Software................................................ Yes
 2.  System Management Client Software................................ Yes
 3.  OpenSSH Client Software.......................................... No
 4.  OpenSSH Server Software.......................................... No
 5.  Enable System Backups to install any system...................... Yes
     (Installs all devices)



>>> 6.  Install More Software

    0  Install with the current settings listed above.

    88  Help ?
    99  Previous Menu

>>> Choice [6]: 0
```

For more info on these options, see the IBM Documentation about [BOS Installation options](https://www.ibm.com/docs/en/aix/7.2?topic=system-bos-installation-options). 

- if you select all the 5 groups above, then it means ~`767` filesets for a Standard edition (depending on the platform/device drivers)
- having only OpenSSH Client and Server groups selected (like below), which is a bare minimum, the result is ~`217` filesets only (depending on the platform/device drivers)

```ksh
                             Install Options

 1.  Graphics Software................................................ No 
 2.  System Management Client Software................................ No 
 3.  OpenSSH Client Software.......................................... Yes
 4.  OpenSSH Server Software.......................................... Yes
 5.  Enable System Backups to install any system...................... No 
     (Installs all devices)



>>> 6.  Install More Software

    0  Install with the current settings listed above.

    88  Help ?
    99  Previous Menu

>>> Choice [6]: 0
```

When you are done, select `0` to proceed with the previously specified settings. 

6. A final **Overwrite Installation Summary** for confirmation before continuing with the actual installation: 

```ksh
                            Overwrite Installation Summary

Disks:  hdisk0
Cultural Convention:  en_US
Language:  en_US
Keyboard:  en_US
Graphics Software:  No 
System Management Client Software:  No 
OpenSSH Client Software:  Yes
OpenSSH Server Software:  Yes
Enable System Backups to install any system:  No 
Selected Edition:  standard

Optional Software being installed:  



>>> 1   Continue with Install
                       +-----------------------------------------------------
    88  Help ?         |    WARNING: Base Operating System Installation will
    99  Previous Menu  |    destroy or impair recovery of ALL data on the
                       |    destination disk hdisk0.
>>> Choice [1]: 1
```

7. The install process **might take even more than 2 hours**, depending on your choices above.  
  When it finally finishes, you can **Set Terminal Type**, which I think is traditionally either `vt100` or `xterm`: 

```ksh
[...]
                            Set Terminal Type 
The terminal is not properly initialized.  Please enter a terminal type 
and press Enter.  Some terminal types are not supported in 
non-English languages. 

     ibm3101          tvi912          vt330           aixterm
     ibm3151          tvi920          vt340           dtterm
     ibm3161          tvi925          wyse30          xterm
     ibm3162          tvi950          wyse50          lft
     ibm3163          vs100           wyse60          sun
     ibm3164          vt100           wyse100 
     ibmpc            vt320           wyse350 

                      +-----------------------Messages----------------------- 
                      | ERROR:  Undefined terminal type.  Please try again. 
    88  Help ?        | 
                      | If the next screen is unreadable, press Break (Ctrl-c) 
                      | to return to this screen. 
>>> Choice []: xterm
[...]
```

8. Select `Accept License Agreements` in the appearning, restricted `smitty` menu, then press `Esc+0` (`F10` emulation)

9. Select `Accept Software Maintenance Terms and Conditions` and exit

10. Next, the **Installation Assistant** welcomes you. In this menu, do not exit with `F10` after one single setting, but always go back with `F3` to the main menu. As a minimum, you can...  
- set **Date and Time** (including **Time Zone**),  
- set `root` pw and  
- optionally, install additional SW if you want, then select `Exit to Login`: 

```ksh
                             Installation Assistant

Move cursor to desired item and press Enter.

  Set Date and Time
  Set root Password
  Configure Network Communications
  Install Software Applications
  System Workload Partition Software Maintenance
  Using SMIT (information only)
  Tasks Completed - Exit to Login
```

Even if you missed setting up the `root` password, you can login with just simply entering `root` as user name, and you will get the prompt, but then you may immediately change the password. 

11. Login to the system: 

```ksh
[...]
AIX Version 7
Copyright IBM Corporation, 1982, 2022.
Console login: root
root's Password: 
```

This concludes the standard Installation Procedure. Your next steps are probably setting up the **hostname** and basic **networking** with `smitty mktcpip`. E.g.:

```ksh
                        Minimum Configuration & Startup

 To Delete existing configuration data, please use Further Configuration menus

Type or select values in entry fields.
Press Enter AFTER making all desired changes.

[TOP]                                                   [Entry Fields]
* HOSTNAME                                           [aix72srv1]
* Internet ADDRESS (dotted decimal)                  [192.168.101.103]
  Network MASK (dotted decimal)                      [255.255.255.0]
* Network INTERFACE                                   en0
  NAMESERVER
           Internet ADDRESS (dotted decimal)         []
           DOMAIN Name                               []
  Default Gateway
       Address (dotted decimal or symbolic name)     [192.168.101.1]           
       Cost                                          []                       #
       Do Active Dead Gateway Detection?              no                     + 
  Your CABLE Type                                     N/A                    + 
  START Now                                           yes                    +
[BOTTOM]

F1=Help             F2=Refresh          F3=Cancel           F4=List
Esc+5=Reset         F6=Command          F7=Edit             F8=Image          
F9=Shell            F10=Exit            Enter=Do
```

Expected results of for the previous task: 

```ksh
                                 COMMAND STATUS

Command: OK            stdout: yes           stderr: no

Before command completion, additional instructions may appear below.

[TOP]
en0
aix72srv1
inet0 changed
en0 changed
inet0 changed
Checking for srcmstr active...complete
Starting tcpip daemons:
0513-029 The syslogd Subsystem is already active.
Multiple instances are not supported.
0513-029 The sendmail Subsystem is already active.
Multiple instances are not supported.
0513-029 The inetd Subsystem is already active.
[MORE...11]

F1=Help             F2=Refresh          F3=Cancel           F6=Command
F8=Image            F9=Shell            F10=Exit            /=Find 
n=Find Next
```


<a name="4"></a>
## 4. AIX LVM and Filesystems

...

### 4.1 Introduction to LVM

...

### 4.2 Useful commands

#### 4.2.1 Cleaning up PVs for reuse

Preparing disks in case they belonged to other VG before. The other option is using `-f` (force) for `mkvg` command, but this set of commands and steps, or at least parts of them, can be useful for other cases too: 

```ksh
# lspv | grep -E "hdisk5|hdisk6"
hdisk5          00c59a07bcd4c060                    None
hdisk6          00c59a07bccbbdf9                    None

# for i in hdisk5 hdisk6 ; do echo "$i: $(getconf DISK_SIZE /dev/$i)" ; done
hdisk5: 5120
hdisk6: 5120

# ls -l /dev/hdisk5 /dev/rhdisk5 /dev/hdisk6 /dev/rhdisk6
brw-------    1 root     system       24,  7 May 20 17:38 /dev/hdisk5
brw-------    1 root     system       24,  8 May 20 17:38 /dev/hdisk6
crw-------    1 root     system       24,  7 May 20 17:38 /dev/rhdisk5
crw-------    1 root     system       24,  8 May 20 17:38 /dev/rhdisk6

# dd if=/dev/zero of=/dev/rhdisk5 bs=1M count=100
100+0 records in.
100+0 records out.

# dd if=/dev/zero of=/dev/rhdisk6 bs=1M count=100
100+0 records in.
100+0 records out.

# lspv | grep -E "hdisk5|hdisk6"
hdisk5          00c59a07bcd4c060                    None
hdisk6          00c59a07bccbbdf9                    None

# rmdev -dl hdisk5
hdisk5 deleted
# rmdev -dl hdisk6
hdisk6 deleted

# lspv | grep -E "hdisk5|hdisk6"
#

# cfgmgr

# lspv | grep -E "hdisk5|hdisk6"
hdisk5          none                                None
hdisk6          none                                None
```

#### 4.2.2 Creating VG, LV, FS, quickly

Example for creating a Scalable type VG, then a mirrored LV, then FS with Inline Log, and finally mounting and checking it: 

```ksh
# mkvg -s 128 -S -y testvg hdisk5 hdisk6
0516-1254 mkvg: Changing the PVID in the ODM.
0516-1254 mkvg: Changing the PVID in the ODM.
testvg

# lsvg testvg
VOLUME GROUP:       testvg                   VG IDENTIFIER:  00fbe31000004c00000001798a717d1c
VG STATE:           active                   PP SIZE:        128 megabyte(s)
VG PERMISSION:      read/write               TOTAL PPs:      78 (9984 megabytes)
MAX LVs:            256                      FREE PPs:       78 (9984 megabytes)
LVs:                0                        USED PPs:       0 (0 megabytes)
OPEN LVs:           0                        QUORUM:         2 (Enabled)
TOTAL PVs:          2                        VG DESCRIPTORS: 3
STALE PVs:          0                        STALE PPs:      0
ACTIVE PVs:         2                        AUTO ON:        yes
MAX PPs per VG:     32768                    MAX PVs:        1024
LTG size (Dynamic): 512 kilobyte(s)          AUTO SYNC:      no
HOT SPARE:          no                       BB POLICY:      relocatable
MIRROR POOL STRICT: off
PV RESTRICTION:     none                     INFINITE RETRY: no
DISK BLOCK SIZE:    512                      CRITICAL VG:    no
FS SYNC OPTION:     no

# lsvg -p testvg
testvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk5            active            39          39          08..08..07..08..08
hdisk6            active            39          39          08..08..07..08..08

# mklv -t jfs2 -y testlv -c 2 testvg 32
testlv

# ls -la /testfs
ls: 0653-341 The file /testfs does not exist.

# crfs -v jfs2 -m /testfs -d testlv -A yes -a logname=INLINE
File system created successfully.
4177588 kilobytes total disk space.
New File System size is 8388608

# mount /testfs

# df -m /testfs
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/testlv     4096.00   4079.05    1%        4     1% /testfs

# ls -la /testfs
total 8
drwxr-xr-x    3 root     system          256 May 20 17:45 .
drwxr-xr-x   46 root     system         4096 May 20 17:45 ..
drwxr-xr-x    2 root     system          256 May 20 17:45 lost+found

# lsvg -p testvg
testvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk5            active            39          7           00..00..00..00..07
hdisk6            active            39          7           00..00..00..00..07

# lsvg -l testvg
testvg:
LV NAME             TYPE       LPs     PPs     PVs  LV STATE      MOUNT POINT
testlv              jfs2       32      64      2    open/syncd    /testfs

# lslv -m testlv
testlv:/testfs
LP    PP1  PV1               PP2  PV2               PP3  PV3
0001  0001 hdisk5            0001 hdisk6
0002  0002 hdisk5            0002 hdisk6
...
0031  0031 hdisk5            0031 hdisk6
0032  0032 hdisk5            0032 hdisk6
```

#### 4.2.3 Removing FS, LV, then VG, quickly

```ksh
# umount /testfs

# df -m /testfs
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/hd4        2048.00    869.06   58%    20762     9% /

# rmlv testlv
Warning, all data contained on logical volume testlv will be destroyed.
rmlv: Do you wish to continue? y(es) n(o)? y
rmlv: Logical volume testlv is removed.

# lsvg -l testvg
testvg:
LV NAME             TYPE       LPs     PPs     PVs  LV STATE      MOUNT POINT

# lsvg -p testvg
testvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk5            active            39          39          08..08..07..08..08
hdisk6            active            39          39          08..08..07..08..08

# exportvg testvg
0516-764 exportvg: The volume group must be varied off
        before exporting.

# varyoffvg testvg

# exportvg testvg

# lspv | grep -E "hdisk5|hdisk6"
hdisk5          00fbe3108a717c7f                    None
hdisk6          00fbe3108a717cec                    None
```

#### 4.2.4 Queries for quick LUN listings

Example for a complete `unique_id`: 

```ksh
# lsattr -El hdisk0 -a unique_id
unique_id 33213600507680C8100128800000000000B6F04214503IBMfcp Unique device identifier False
```

Based on the previous query, the following query can be useful only for **IBM SVC** LUNs, because it works with the long character description of `unique_id`, that may differ for other technologies: 

```ksh
# for i in $(lspv | awk '{print $1}') ; do echo "$i -> $(lsattr -El $i -a unique_id | cut -c 16-47)" ; done
hdisk0 -> 600507680C8100128800000000000B6F
hdisk1 -> 600507680C8100128800000000000027
hdisk2 -> 600507680C8100128800000000000028
```

Extending the above `lspv` list query with LUN IDs, VG names, statuses and sizes: 


```ksh
# echo "PVname PVID VGname Status Serial Size [MB]" | awk '{printf "%-9s%-18s%-15s%-12s%-34s%-5s%-2s\n", $1,$2,$3,$4,$5,$6,$7}' ; lspv | while read PV ; do if [ $(echo $PV | grep -E 'active|concurrent|locked' | wc -l) -gt 0 ]; then echo "$PV $(lsattr -El $(echo $PV | awk '{print $1}') -a unique_id | cut -c 16-47) $(getconf DISK_SIZE /dev/$(echo $PV | awk '{print $1}'))" | awk '{printf "%-9s%-18s%-15s%-12s%-34s%-5s\n", $1,$2,$3,$4,$5,$6}' ; else echo "$PV $(lsattr -El $(echo $PV | awk '{print $1}') -a unique_id | cut -c 16-47) $(getconf DISK_SIZE /dev/$(echo $PV | awk '{print $1}'))" | awk '{printf "%-9s%-18s%-15s%-12s%-34s%-5s\n", $1,$2,$3,"N/A",$4,$5}' ; fi ; done
PVname   PVID              VGname         Status      Serial                            Size [MB]
hdisk1   00c59a0737fdbf1b  oraprodvg      concurrent  600507680C8004C68800000000000081  1048576
hdisk3   00c59a0737fdbf6d  oraprodvg      concurrent  600507680C8004C68800000000000083  1048576
hdisk2   00c59a0737fdbe33  oraprodvg      concurrent  600507680C8004C68800000000000082  1048576
hdisk7   00c59a07be39db69  nfsexpvg       concurrent  600507680C8004C68800000000000063  1044480
hdisk4   00c59a0737fdbebf  oraprodvg      concurrent  600507680C8004C68800000000000084  1048576
hdisk16  00c59a07d786984d  HB1vg          N/A         600507680C8004C68800000000000046  1024
hdisk19  00c59a07d786998f  rman_vg        concurrent  600507680C8004C68800000000000041  2457600
hdisk23  00c59a07d7869b36  HB2vg          N/A         600507680C8004C68800000000000049  1024
hdisk24  00c60f37d7529023  rootvg         active      600507680C8004C6880000000000003E  149504
hdisk0   00fbe3101c25a8c4  oraprod2vg     concurrent  600507680C8004C688000000000000CE  3194880
```

**Total size** of all LUNs locally:

```ksh
# for PV in $(lspv | awk '{print $1}') ; do getconf DISK_SIZE /dev/$PV ; done | awk '{sum +=$1} END {print sum}' | awk '{print ($1/1024)" GiB"}'
10784 GiB
```

Putting the same into a `for` loop, issued on a jump server: 

```ksh
# for LPAR in lpar01 lpar02 lpar03 ; do echo "$LPAR: $(for PV in $(ssh $LPAR lspv | awk '{print $1}') ; do ssh $LPAR getconf DISK_SIZE /dev/$PV ; done | awk '{sum +=$1} END {print sum}' | awk '{print ($1/1024)" GiB"}')" ; done
lpar01: 1140 GiB
lpar02: 180 GiB
lpar03: 646 GiB
```

...


<a name="5"></a>
## 5. AIX Networking

...

Listing EtherChannel configuration in a more human readable, more trackable way: 

```ksh
# for EC in $(lsdev | awk '/EtherChannel/{print $1}') ; do echo "$EC -->" ; entstat -d $EC | grep "Port VLAN ID" | head -1 ; lsattr -El $EC | awk '/adapter_names|backup_adapter|netaddr/ {print $1": "$2}' | sed 's/netaddr/IP to ping/g' ; echo "EtherChannel device's IP and netmask: $(lsattr -El $(echo $EC | sed s/ent/en/g) | echo $(awk '/netaddr |netmask/ {print $2}'))" ; echo ----- ; done
ent0 -->
Port VLAN ID:   100
adapter_names: ent10
backup_adapter: ent11
IP to ping: 10.161.138.254
EtherChannel device's IP and netmask: 10.161.138.212 255.255.255.192
-----
ent1 -->
Port VLAN ID:   200
adapter_names: ent20
backup_adapter: ent21
IP to ping: 10.163.111.254
EtherChannel device's IP and netmask: 192.168.10.11 255.255.254.0
-----
```

...


<a name="6"></a>
## 6. Backup

...


<a name="7"></a>
## 7. NIM

...
