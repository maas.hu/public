# ![alt text](/_ui/img/icons/php_m.svg) PHP

###### .

Az alábbi leírásban a **PHP** alapjairól fogom a saját tapasztalataimat megosztani olyan tematika szerint, ahogy véleményem szerint átlátható és elsajátítható.

## Tartalomjegyzék

1. [Bevezetés](#1)
2. [A fejlesztői környezet kialakítása](#2)
3. [Alapfüggvények, nyelvi konstrukciók](#3)
		
<a name="1"></a>
## 1. Bevezetés

A PHP egy szkriptnyelv, tehát a programkód meg kell maradjon szöveges változatában, és a futtatásához egy értelmezőprogramra van szükség.
A PHP rövidítés eredetileg a Personal Home Page szavakból ered, mely elnevezés az első megjelenéskor, még 1994-ben azt hivatott szemléltetni, hogy segítségével a személyes weboldalainkat alakíthatjuk egységesebbé egy makrókészlet alkalmazásával. Később egyre fejlettebb képességeket kapott, és rövidesen egy önálló programozási nyelv vált belőle. A vele való fejlesztéshez mindenképpen szükségünk van a **HTML/CSS** ismeretére is.

Mind a statikus, mind a dinamikus weboldalak esetében egyaránt a webes dokumentumok, képek és egyéb állományok a webszerver tárhelyén találhatóak. A hagyományos, többnyire statikus HTML oldalak esetében, amikor a kliens a webböngészője segítségével egy adott oldalra, vagy HTML dokumentura hivatkozik, a webszervernek nincs más dolga, mint továbbítani a dokumentumot és az abban hivatkozott képeket/egyéb állományokat. Minden további feldolgozás a kliens számítógépén történik. Amikor egy ilyen statikus weboldalon egy másik linkre, vagy menüpontra kattintunk, akkor a webszervernek a megjelenítéshez szükséges, és még nem cache-elt új fájlokat ismét el kell küldenie a kliens irányába.
Annak érdekében, hogy ne mindig az egész weboldal töltődjön be újra és újra egy-egy menüpont kiválasztásakor, kezdetben a HTML frame-ek voltak hivatottak ezt bizonyos szinten könnyíteni, ám viszonylag ritkán alkalmazott megoldás volt ez. Többnyire az "egészoldalas" weboldalak voltak továbbra is népszerűbbek. Jelentős javulás érhető el, ha oldalainkat féldinamikussá vagy dinamikussá tesszük: az oldal nem, vagy kevésbé változó részeit már előre odageneráltatjuk valamelyik támogatott módszerrel, és csak az új részek megszerkesztésére kell fordítanunk energiánkat és időnket.
Alapvetően ezt, de ezen túl még rengeteg más, feldolgozással járó műveletet végez el a PHP.

Röviden összefoglalva a folyamatot: a többnyire Linuxot futtató szerverre beérkező kérésről az azon futó webszerver elsőként eldönti, hogy a PHP-nek kell-e továbbítani. Ha igen, továbbítja, azt a PHP feldolgozza, majd az eredményt továbbítja még mindig helyben a webszervernek. Ez ezután továbbítja azt a kliens webböngészőjének, ahol végül megjelenik az összerakott weboldal. Mindennek köszönhetően egyrészt a bonyolultabb feldolgozásokból adódó terhelés is a webszerver oldalán marad, másrészt garantálja, hogy minden kliens ugyanazt, vagy legalábbis pontosan a rászabott eredményt kapja a feldolgozás után.

![alt_text](php_01.png "PHP")

<a name="2"></a>
## 2. A fejlesztői környezet kialakítása

A fentiek alapján talán könnyen kitalálható: a HTML-el ellentétben a PHP saját gépen történő használatához egy webszerver szoftver telepítésére és futtatására is szükség van. Ez mellé a PHP értelmezőt is telepítenünk kell, aminek jelenlétét a webszervernek is tudtára kell adnunk. Tehát semmiképpen sem egyszerűen a kész .php fájlokat fogjuk a böngészőnkben megnyitni, hanem helyette a webszervert kell helyesen beállítanunk, és onnantól kezdve a megfelelő könyvtárakban elhelyezett .php fájlokra fogunk hivatkozni. A .html fájl megnyitása helyett a "localhost" URL-t fogjuk megnyitni, mintha az egy hagyományos URL lenne. Ez a módszer természetesen statikus HTML oldalaknál is egy választható megoldás, de PHP esetében mindenképpen így kell eljárnunk.
Létezik egy másik módszer is, amikor a fejlesztői környezetet már készen kapjuk, és abban tesztelhetjük a kódjainkat, ám lehetnek jelentős funkcionalitásbeli különbségek, amikor a késznek gondolt weboldalt feltöltjük a tárhelyszolgáltató FTP címére, így mindenképpen a valódi webszerveres környezetet kialakító, teljesértékű webszerver és PHP értelmező telepítése, majd beállítása ajánlott.

### 2.1 Letölteni valók Windows alá

Windows alatt akár a MS saját webszerverével, az IIS-el is összepárosítható a PHP, de mivel a tárhelyszolgáltatók legalább 99%-a az Apache-ot, vagy annak valamelyik fork-ját alkalmazza, így jobb, ha mi is maradunk annál a megoldásnál.

1. Apache:

Elsődleges lelőhelye a [httpd.apache.org](https://httpd.apache.org/download.cgi), ahol a windows-os változat címe:
[https://www.apachehaus.com/cgi-bin/download.plx](https://www.apachehaus.com/cgi-bin/download.plx). 
Az oldalon a leírás pillanatában az "Apache 2.4.39 x64 with OpenSSL 1.1.1c"-t választottam; a fájl neve: `httpd-2.4.39-o111c-x64-vc15.zip`. 

2. PHP:

A Windows-os változat lelőhelye a [windows.php.net](http://windows.php.net/download/). Itt a "VC15 x64 Thread Safe (2019-Jul-03 17:49:17)" változatot választottam; a fájl neve: `php-7.3.7-Win32-VC15-x64.zip`. 

### 2.2 Konfigurálás Windows alatt

A `D:\bin` alatt (de lehet bárhol máshol is elhelyezni, pl. `C:\Program Files`, vagy bármi; ez majd a konfiguráláskor lesz érdekes) csináltam egy `HTTPD-PHP` mappát, abba bemásoltam az első `.zip` fájlból az Apache24 tartalmát (tehát a bin, `cgi-bin` stb. könyvtárakat, még a `.txt` fájlokat is). 

A HTTPD-PHP könyvtáron belül csináltam egy "php" nevű mappát, oda került minden a második `.zip` fájlból.

A `conf\httpd.conf` fájl szerkesztése:

`SRVROOT` cseréje erről:

```
Define SRVROOT "/Apache24"
```

erre:

```
Define SRVROOT "D:/bin/HTTPD-PHP"
```

A többi `LoadModule`-os rész alá/fölé beszúrni új sorként:

```
LoadModule php7_module php/php7apache2_4.dll
```

A `DocumentRoot` átírása erről:

```
DocumentRoot "${SRVROOT}/htdocs"
<Directory "${SRVROOT}/htdocs">
```

erre:

```
DocumentRoot "D:/Documents/HTML/www"
<Directory "D:/Documents/HTML/www">
```

A `DirectoryIndex` kiegészítése ilyenről:

```
DirectoryIndex index.html
```

ilyenre:

```
DirectoryIndex index.html index.php
```

A `IfModule mime_module` blokk kiegészítése, még a `/IfModule` előtt:

```
AddType application/x-httpd-php .php
PHPIniDir "D:/bin/HTTPD-PHP/php"
```

A `conf/extra/httpd-ahssl.conf` letiltása:

```
Include conf/extra/httpd-ahssl.conf
```

...itt egy hashtag beszúrásával:

```
#Include conf/extra/httpd-ahssl.conf
```

`php\php.ini-production` átmásolása `php\php.ini` néven.
Valószínűleg kelleni fog egy extra `.dll` beszerzése is, a `vcruntime140.dll` (a MS Visual C++ része, így ha az telepítve van, erre a `.dll`-re sincs szükség), ami letölthető pl. a [dll-files.com](https://www.dll-files.com/vcruntime140.dll.html) oldaláról is akár.

Adj teljes hozzáférési jogokat a Users-nek (Felhasználók) a `logs` könyvtárhoz!

### 2.3 Tesztelés
Indíts el egy parancssort, menj a `HTTPD-PHP` könyvtárba és indítsd el a `bin/httpd.exe`-t (ha valami még nem OK, azt ki fogja írni)! Hagyd futni, teszteld a böngészőben a "localhost" meghívásával!

Sokkal jobb teszt, ha a webszervernek beállított `DocumentRoot`-ban (a fenti példánál maradva a `D:/Documents/HTML/www`-ben) elhelyezünk egy `phpinfo.php` fájlt az alábbi tartalommal:

```
<?php
phpinfo();
?>
```

...majd a böngészőben megnyitjuk a [http://localhost/phpinfo.php](http://localhost/phpinfo.php) oldalt.

Innentől kezdve nincs más dolgunk, mint a fejlesztés első lépéseként elindítjuk a `httpd.exe`-t, azt hagyjuk az üres, fekete képernyőjében futni, majd a kódoláskor az ellőrzést a böngészőben a megszokott módon, oldalfrissítéssel végezzük. A PHP kód szerkesztőjének ugyanazt az editort használhatjuk, ami HTML szerkesztéshez is bevált: *Notedpad++*, *Kate*, *Geany*, *GEdit* stb.

<a name="3"></a>
## 3. Alapfüggvények, nyelvi konstrukciók

### 3.1 A `print` és az `echo`

Erre a két nagyon alapvető parancsra gyakran függvényként hivatkoznak, de működésüket tekintve nem azok, csupán "nyelvi konstrukciók". Nem szükséges ugyanis, hogy zárójelek közé tegyük argumentumlistájukat, továbbá a visszatérési értékük sem függ a lefutásuk sikerességétől.

Mindkét parancs az utánuk megadott szöveget jelenítteti meg a böngészővel. A különbség kettejük között az, hogy az `echo` több argumentumot is elfogad, és nem ad visszatérési értéket, míg a `print` csak egyetlen argumentummal rendelkezhet, és mindig "1"-el tér vissza a lefutása után.

Néhány példa használatukra:

```
echo 'Hello Világ!';
echo "Hello Világ!";
echo ("Hello világ!");
print 'Hello Világ!';
print "Hello Világ!";
print ("Hello világ!");
```

Egy egyszerű HTML tartalom kiíratása az `echo`-val:

```
echo '<!DOCTYPE html>
<html>
  <head>
    <title>Címsor</title>
  </head>
  <body>
    <p>Tartalom</p>
  </body>
</html>
';
```

>Folytatás következik...
