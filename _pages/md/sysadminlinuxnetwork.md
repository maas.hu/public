# ![alt text](/_ui/img/icons/babytux_m.svg) Linux Network and Security Basics

<div style="text-align: right;"><h7>Posted: 2021-10-25 17:41</h7></div>

###### .

![alt_text](linux_-_network_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Linux IP Networking Basics](#2)
3. [Understanding Traditional Linux Networking](#3)
4. [NetworkManager](#4)
5. [Secure Shell](#5)
6. [NFS and Samba](#6)
7. [Managing On-Demand Network Services with xinetd](#7)
8. [Firewall Solutions](#8)
9. [Troubleshooting Network](#9)


<a name="1"></a>
## 1. Introduction

Linux networking and security are foundational topics for any advanced system administrator. 

This article provides a deep dive into key aspects, starting with networking basics to establish a solid foundation. It explores advanced configurations using `iproute` for granular control, alongside **NetworkManager** for streamlined management of complex setups. **Secure Shell** (**SSH**) is covered as a vital tool for secure remote access, with emphasis on configuration best practices. Practical troubleshooting techniques are included to address common network issues efficiently.


<a name="2"></a>
## 2. Linux IP Networking Basics

Networking is a fundamental component of any Linux system, whether it's a desktop, a server, or an embedded system.

Linux provides powerful networking capabilities, allowing users to configure and manage network **interfaces**, **routes**, and **services**. Understanding the basics of Linux networking is essential for system administrators, developers, and anyone working with networked devices. This chapter covers key networking concepts, including IP addressing, hostnames, DHCP, NAT, and bridged networks.

### 2.1 IPv4, IPv6

**IPv4 and IPv6** are the two primary versions of the **Internet Protocol** (**IP**).

**IPv4**, the older and still widely used protocol, uses 32-bit addresses, allowing for approximately 4.3 billion unique addresses. Due to address exhaustion, **IPv6** was introduced with 128-bit addresses, providing an almost unlimited address space. 

IPv6 eliminates the need for **NAT** (**Network Address Translation**) in many cases, as it allows every device to have a globally unique address. A notable difference is that IPv6 uses **link-local addresses** (starting with `fe80::`) for communication within a network without requiring **DHCP**. 

While IPv4 heavily relies on manual configuration or DHCP for address assignment, IPv6 introduces **stateless address autoconfiguration (SLAAC)**, allowing devices to configure themselves automatically.

### 2.2 Network Interface Cards (NICs)

A **Network Interface Card** (**NIC**) is the hardware or virtual device that connects a system to a network. Linux recognizes these interfaces and assigns them names like `eth0`, `ens33`, or `wlan0`, depending on the interface type and naming conventions used by the system.

There are different types of network interfaces in Linux:

- **Physical interfaces** – Ethernet (`eth0`, `ens4f0`), Wi-Fi (`wlan0`), or fiber (`enp2s0f0`).
- **Virtual interfaces** – VLANs (`eth0.100`), bridges (`br0`), tunnels (`tun0`, `tap0`), and bonded interfaces (`bond0`).
- **Loopback interface** (`lo`) – A special interface used for internal communication within the system (127.0.0.1).

To list all network interfaces on a most recent Linux system:

<pre class="con">
<span class="psr">$</span> <span class="cmd">ip link show</span>
1: lo: &lt;LOOPBACK,UP,LOWER_UP&gt; mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s3: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:8f:a3:ed brd ff:ff:ff:ff:ff:ff
3: enp0s8: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:f2:4a:8f brd ff:ff:ff:ff:ff:ff
</pre>

- The loopback (`lo`) interface is always present and used for internal communication.
- The `enp0s3` and `enp0s8` interfaces are a standard Ethernet NICs, showing their MAC address and status (`UP`).

To bring an interface up or down, use:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip link set enp0s3 up</span>

<span class="psr">#</span> <span class="cmd">ip link set enp0s3 down</span>
</pre>

Network interfaces form the foundation of Linux networking, as all IP configuration, routing, and firewall rules depend on them.

### 2.3 Hostname

A hostname is the unique identifier assigned to a system on a network. It is primarily used for easy identification rather than relying on numerical IP addresses. On Linux, the hostname is typically stored in `/etc/hostname`, but modifying this file alone may not be sufficient for proper hostname resolution. To ensure correct name resolution, the hostname should also be listed in `/etc/hosts`, especially on servers. The recommended method to manage hostnames is using the `hostnamectl` command, which interacts with **systemd-hostnamed** to set static, transient, and pretty hostnames.

Example of checking the hostname:

<pre class="con">
<span class="psr">#</span> <span class="cmd">hostnamectl</span>
   Static hostname: rockysrv1
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 69f007ebb8e24dba853b62fd7ffe56fb
           Boot ID: 5a8744ccecd841aaac9aab3c8da01405
    Virtualization: oracle
  Operating System: Rocky Linux 8.4 (Green Obsidian)
       CPE OS Name: cpe:/o:rocky:rocky:8.4:GA
            Kernel: Linux 4.18.0-305.19.1.el8_4.x86_64
      Architecture: x86-64
</pre>

To change the hostname permanently:

<pre class="con">
<span class="psr">#</span> <span class="cmd">hostnamectl set-hostname rockysrv2</span>

<span class="psr">#</span> <span class="cmd">reboot</span>
[...]
</pre>

### 2.4 IP Routes

A **route** in networking determines how packets travel from a source to a destination. Linux maintains a **routing table**, which defines where to send traffic based on the destination IP address. The kernel consults this table whenever an outbound packet is generated.

Routes can be classified into three main types:

- **Directly connected routes** – A device can communicate directly with other devices in the same subnet.
- **Static routes** – Manually configured routes define specific paths for certain destinations.
- **Dynamic routes** – Routes learned via routing protocols like OSPF or BGP, often used in complex network environments.

On a most recent Linux, the routing table can be viewed with `ip route show` (or shortly, with `ip r`). Here's a simple example:

<pre class="con">
<span class="psr">$</span> <span class="cmd">ip route show</span>
default via 192.168.1.1 dev eth0 proto dhcp metric 100
192.168.1.0/24 dev eth0 proto kernel scope link src 192.168.1.100
</pre>

- The **default** route (`default via 192.168.1.1`) tells the system to send traffic for unknown destinations through 192.168.1.1 (gateway).
- The second line indicates that traffic for the 192.168.1.0/24 subnet is directly reachable via `eth0`.

To manually add a static route:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip route add 10.0.0.0/24 via 192.168.1.254 dev eth0</span>
</pre>

This directs packets for 10.0.0.0/24 through 192.168.1.254.

Static routes are useful for custom network paths, but they must be reconfigured manually if the network topology changes.

### 2.5 DHCP

**Dynamic Host Configuration Protocol (DHCP)** is used to automatically assign IP addresses, subnet masks, gateways, and other network parameters to devices on a network. It eliminates the need for manually configuring each device with a static IP.

A **DHCP client** (such as `dhclient` or `systemd-networkd` on modern Linux systems) sends a request to a **DHCP server**, which responds with an available **IP lease**. The lease has an **expiration time**, after which the client **must renew** the address. DHCP is widely used in enterprise networks, home networks, and cloud environments. 

In addition to IPv4, DHCPv6 exists for IPv6 networks, but **many IPv6 setups prefer SLAAC** over DHCPv6 for address assignment.

### 2.6 NAT

**Network Address Translation (NAT)** allows multiple devices to share a single public IP address by modifying packet headers as they pass through a router. This is crucial in IPv4 networks due to address exhaustion. 

The most common NAT type is **masquerading**, where outbound traffic from private IPs (e.g., `192.168.1.0/24`) is translated to a public IP. When responses return, the router correctly maps them back to the originating internal device. 

While NAT is essential for IPv4, it is **largely unnecessary in IPv6** because IPv6 provides sufficient public addresses for all devices. However, NAT66 (IPv6 NAT) can still be used in specific scenarios like privacy and policy enforcement.

### 2.7 Bridged Networks

A **bridged network** connects multiple interfaces at the **data link layer** (**Layer 2**), allowing systems to communicate as if they were on the same physical network. It is **commonly used in virtualization** (e.g., **KVM**, **VirtualBox**, **Docker**) to allow VMs or containers to appear as independent devices on the network. 

Linux supports bridging via the `bridge` kernel module and tools like `brctl` or `nmcli`. A bridge acts as a virtual switch, forwarding packets between connected interfaces without modifying them. 

Unlike NAT-based networking, bridged networks provide **full bidirectional access** between guests and the external network, making them ideal for scenarios where virtual machines or containers need to behave like regular physical hosts.


<a name="3"></a>
## 3. Understanding Traditional Linux Networking

Before modern network management tools like `NetworkManager` became widely adopted, Linux networking was primarily managed using low-level utilities and configuration scripts. Early tools such as `ifconfig` and `netstat` were part of the `net-tools` package, while later, the more advanced `iproute` utilities replaced them. These command-line tools provided fine-grained control over network interfaces, routing tables, and other network settings.

In this chapter, we explore the traditional ways of managing networking in Linux before `NetworkManager` was introduced. This includes basic network interface configuration, IP and route management, and persistent network settings. While these methods are largely deprecated in favor of `ip` commands and automated management, understanding them is still valuable, particularly when working with minimal Linux environments, embedded systems, or troubleshooting.

### 3.1 Early Networking Tools (`net-tools`)

Historically, network management on Linux relied on the `net-tools` package, which provided a set of commands for interface configuration, routing, and network statistics. These tools were commonly used before being replaced by more modern alternatives.

The following are key utilities included in `net-tools` on an RHEL 8-based system:

<pre class="con">
<span class="psu">$</span> <span class="cmd">rpm -ql net-tools | grep bin/</span>
/usr/bin/netstat
/usr/sbin/arp
/usr/sbin/ether-wake
/usr/sbin/ifconfig
/usr/sbin/ipmaddr
/usr/sbin/iptunnel
/usr/sbin/mii-diag
/usr/sbin/mii-tool
/usr/sbin/nameif
/usr/sbin/plipconfig
/usr/sbin/route
/usr/sbin/slattach
</pre>

The most commonly used commands from `net-tools` were:

- **`ifconfig`**: Configure and display network interfaces.
- **`route`**: Manage the system's routing table.
- **`netstat`**: Display network statistics, open sockets, and routing information.
- **`arp`**: Manage the system's ARP cache.

However, these tools have been deprecated in favor of the `iproute` package.

### 3.2 Transition to `iproute`

To provide more advanced network control and performance, the `iproute` package was introduced. This set of tools replaced `net-tools` and became the standard for network configuration in Linux.

Key commands provided by `iproute` include:

<pre class="con">
<span class="psu">$</span> <span class="cmd">rpm -ql iproute | grep bin/</span>
/usr/sbin/arpd
/usr/sbin/bridge
/usr/sbin/ifcfg
/usr/sbin/ip
/usr/sbin/rdma
/usr/sbin/routef
/usr/sbin/routel
/usr/sbin/ss
</pre>

The most commonly used tools in `iproute`:

- **`ip`**: A versatile replacement for `ifconfig`, `route`, and `arp`.
- **`ss`**: A more powerful alternative to `netstat` for socket statistics.
- **`bridge`**: Manage bridge devices.
- **`tc`**: Traffic control for bandwidth management.

For example, checking an interface's IP address using `ip`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip addr show dev enp0s3</span>
</pre>

Viewing the routing table:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip route show</span>
</pre>

These commands are now the recommended way to manage Linux networking.

### 3.3 Basic Network Configuration with Traditional Tools

Before dynamic networking services like `NetworkManager`, network configurations were manually handled using commands like `ifconfig`, `route`, and configuration files.

#### 3.3.1 Checking Interface and IP Configuration

Using `ifconfig`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ifconfig -a</span>
</pre>

Using `ip`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip addr show dev enp0s3</span>
</pre>

#### 3.3.2 Managing Routes

Displaying routes with `netstat`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">netstat -rn</span>
</pre>

Displaying routes with `ip`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip route show</span>
</pre>

#### 3.3.3 Adding IP Addresses and Routes

Setting an IP address with `ifconfig`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ifconfig enp0s3 192.168.1.14/24</span>
</pre>

Using `ip` instead:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip addr add 192.168.1.14/24 dev enp0s3</span>
</pre>

Adding a default gateway traditionally:

<pre class="con">
<span class="psr">#</span> <span class="cmd">route add default gw 192.168.0.1</span>
</pre>

Using `ip` for the same task:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip route add default via 192.168.0.1</span>
</pre>

### 3.4 Configuring Persistent Network Settings

To ensure networking settings persist after reboots, they must be stored in system configuration files. Traditionally, Red Hat-based systems used `ifcfg` configuration scripts located in `/etc/sysconfig/network-scripts/`.

Listing network scripts:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ls -la /etc/sysconfig/network-scripts/</span>
</pre>

Example of a DHCP-based configuration:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cat /etc/sysconfig/network-scripts/ifcfg-enp0s3</span>
TYPE=Ethernet
BOOTPROTO=dhcp
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
</pre>

For static configuration:

<pre class="con">
<span class="psr">#</span> <span class="cmd">vi /etc/sysconfig/network-scripts/ifcfg-enp0s3</span>
</pre>

Editing it to:

<pre class="con">
BOOTPROTO=none
IPADDR=10.0.2.15
NETMASK=255.255.255.0
GATEWAY=10.0.2.2
DNS1=192.168.1.1
</pre>

Applying changes:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ifdown enp0s3 && ifup enp0s3</span>
</pre>

Checking the new settings:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip addr show dev enp0s3</span>
</pre>

### 3.5 Summary and Key Takeaways

- **Early networking tools (`net-tools`)**: `ifconfig`, `netstat`, `route`, and `arp` were used before being deprecated.
- **Modern networking (`iproute`)**: The `ip` command replaces many legacy commands, providing more flexibility.
- **Basic networking tasks**: Assigning IPs, setting routes, and viewing configuration historically relied on manual commands.
- **Persistent configuration**: Network settings were stored in `/etc/sysconfig/network-scripts/` before `NetworkManager` became the standard.

Although `NetworkManager` has largely replaced these traditional methods, understanding them remains useful for system administrators working in minimal environments or troubleshooting complex networking issues.


<a name="4"></a>
## 4. NetworkManager

**NetworkManager** is the default networking management tool on most modern Linux distributions. It simplifies network configuration and management by providing a unified interface to handle both wired and wireless connections, VPNs, bridges, and more. Designed for both desktop and server environments, NetworkManager ensures seamless connectivity by automatically handling network interfaces, maintaining active connections, and integrating with system services.

At the system level, when networking is managed by NetworkManager, the `NetworkManager.service` systemd service must be running. This service provides the core functionality for managing network devices and configurations dynamically.

To interact with NetworkManager, users have multiple tools at their disposal, depending on their preferences and system environment:

- `nm-connection-editor`: a graphical user interface (GUI) tool, ideal for desktop users who prefer a visual approach to managing network settings.
- `nmcli`: a command-line tool that offers full control over NetworkManager. It can be used interactively within its built-in command interpreter or executed with direct, structured command-line options in scripts and automation.
- `nmtui`: a text-based user interface (TUI) alternative that provides a structured way to manage network settings, closely resembling the GUI layout of `nm-connection-editor`.

Even during the installation of an RHEL-based system, the **Anaconda installer** utilizes `nm-connection-editor` for configuring network settings. This ensures consistency in network configuration from the installation phase onward.

NetworkManager even takes care of fixing the traditional network scripts under `/etc/sysconfig/network-scripts/` when it applies, so e.g. on RHEL8-based systems.

Among these tools, `nmcli` is the most powerful and versatile, making it the preferred choice for system administrators and automation scripts. It provides fine-grained control over all aspects of networking, from simple Wi-Fi connections to complex VLAN and bridge configurations.

### 4.1 NetworkManager's "connections" and "devices"

NetworkManager distinguishes between **connections** and **devices**, which are often confused. 

A device represents **the physical or virtual network interface**, such as `eth0` or `wlan0`, that exists on the system.  
A connection is **a configuration profile** that defines how a device should operate, including its IP settings, VLANs, bonding, or Wi-Fi credentials.

While a device can exist without an active connection, NetworkManager requires a connection profile to manage network settings persistently across reboots.

### 4.2 General Usage of `nmcli`

Without any options, the command shows all the possible details about all the interfaces: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli</span>
enp0s3: connected to enp0s3
        "Intel 82540EM"
        ethernet (e1000), 08:00:27:72:B6:D3, hw, mtu 1500
        ip4 default
        inet4 10.0.2.15/24
        route4 0.0.0.0/0
        route4 10.0.2.0/24
        inet6 fe80::a00:27ff:fe72:b6d3/64
        route6 fe80::/64
        route6 ff00::/8
[...]
</pre>

`nmcli` offers **command aliasing**, so it allows users to type shortened versions of commands, like `nmcli connection`, `nmcli con`, or `nmcli c` while still executing the same operation.

Listing our currently known networks and the devices that can be managed by NetworkManager: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli c</span>
NAME                UUID                                  TYPE      DEVICE
enp0s3              98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8

<span class="psr">#</span> <span class="cmd">nmcli d</span>
DEVICE  TYPE      STATE      CONNECTION
enp0s3  ethernet  connected  enp0s3
enp0s8  ethernet  connected  Wired connection 1
lo      loopback  unmanaged  --
</pre>

Here, the `c` stands for `connection`, while `d` is for `device`.

The tool's configuration files can be found under `/etc/NetworkManager`. We can set those devices in `/etc/NetworkManager/NetworkManager.conf` which we don't want to allow for NetworkManager to manage.

To show **all the attributes** of a connection, use the `show` parameter, e.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli c show bond-con.104 | tee</span>
connection.id:                          bond-con.104
connection.uuid:                        4a1f19f0-92be-437f-8217-e8ffa8985b48
connection.stable-id:                   --
connection.type:                        vlan
connection.interface-name:              bond-dev.104
connection.autoconnect:                 yes
connection.autoconnect-priority:        0
[...]
</pre>

Querying for a single field, e.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli -f ipv4.routes c show bond-con.104</span>
ipv4.routes:   { ip = 10.31.120.0/24, nh = 10.31.107.251 }; { ip = 10.37.120.0/24, nh = 10.31.107.251 }
</pre>

You can even set the **hostname** via `nmcli`: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli general hostname --help</span>
Usage: nmcli general hostname { ARGUMENTS | help }
[...]
</pre>

### 4.3 Using `nmcli` Editor

`nmcli` is not a simple CLI tool to manipulate network resources via parameters and values, but also offers an internal editor, an internal command interpreter that you can invoke by the `edit` option. 

Next we go through on example for configuring a connection in `nmcli`'s editor. 

First, bringing the connection down (in case it doesn't block our actual session, it must be safe to do):

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli c</span>
NAME                UUID                                  TYPE      DEVICE
enp0s3              98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8

<span class="psr">#</span> <span class="cmd">nmcli c down enp0s3</span>
Connection 'enp0s3' successfully deactivated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/8)
</pre>

Opening up the connection for editing it:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli c edit enp0s3</span>

===| nmcli interactive connection editor |===

Editing existing '802-3-ethernet' connection: 'enp0s3'

Type 'help' or '?' for available commands.
Type 'print' to show all the connection properties.
Type 'describe [&lt;setting&gt;.&lt;prop&gt;]' for detailed property description.

You may edit the following settings: connection, 802-3-ethernet (ethernet), 802-1x, dcb, sriov, ethtool, match, ipv4, ipv6, hostname, tc, proxy
<span class="psr">nmcli></span> <span class="cmd">print connection</span>
['connection' setting values]
connection.id:                          enp0s3
connection.uuid:                        98013505-7e46-4bf7-b485-940fd16b97d8
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              enp0s3
[...]
<span class="psr">nmcli></span> <span class="cmd">set connection.id NAT Adapter</span>
<span class="psr">nmcli></span> <span class="cmd">print ipv4</span>
['ipv4' setting values]
ipv4.method:                            auto
ipv4.dns:                               --
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         --
ipv4.gateway:                           --
ipv4.routes:                            --
[...]
<span class="psr">nmcli></span> <span class="cmd">set ipv4.method manual</span>
<span class="psr">nmcli></span> <span class="cmd">set ipv4.addresses 10.0.2.15/24</span>
<span class="psr">nmcli></span> <span class="cmd">set ipv4.gateway 10.0.2.2</span>
<span class="psr">nmcli></span> <span class="cmd">set ipv4.dns 192.168.1.1</span>
<span class="psr">nmcli></span> <span class="cmd">print ipv4</span>
['ipv4' setting values]
ipv4.method:                            manual
ipv4.dns:                               192.168.1.1
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         10.0.2.15/24
ipv4.gateway:                           10.0.2.2
ipv4.routes:                            --
[...]
<span class="psr">nmcli></span> <span class="cmd">save</span>
Connection 'NAT Adapter' (98013505-7e46-4bf7-b485-940fd16b97d8) successfully updated.
<span class="psr">nmcli></span> <span class="cmd">quit</span>
</pre>

Bringing the connection back, and checking it:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli c</span>
NAME                UUID                                  TYPE      DEVICE
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8
NAT Adapter         98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  --

<span class="psr">#</span> <span class="cmd">nmcli c up "NAT Adapter"</span>
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/9)

<span class="psr">#</span> <span class="cmd">nmcli c</span>
NAME                UUID                                  TYPE      DEVICE
NAT Adapter         98013505-7e46-4bf7-b485-940fd16b97d8  ethernet  enp0s3
Wired connection 1  831afe5b-55fb-3d3e-b2af-a1ab8f481e75  ethernet  enp0s8
</pre>

### 4.4 Static Routes

Example for setting up a static route permanently, under `nmcli` editor: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli c</span>
NAME  UUID                                  TYPE      DEVICE 
enX0  bb702f81-ea50-3178-9033-5e8252da8e7b  ethernet  enX0   

<span class="psr">#</span> <span class="cmd">nmcli con edit enX0</span>
[...]
<span class="psr">nmcli></span> <span class="cmd">set ipv4.routes 192.168.101.0/24 192.168.122.1</span>
<span class="psr">nmcli></span> <span class="cmd">save persistent</span>
Connection 'enX0' (bb702f81-ea50-3178-9033-5e8252da8e7b) successfully updated.
<span class="psr">nmcli></span> <span class="cmd">quit</span>
</pre>

Adding routes directly in command line, e.g.:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli con mod enX0 +ipv4.routes "192.168.101.0/24 192.168.122.1"
</pre>

Cleaning up the NM-managed route list, e.g.:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli con mod enX0 ipv4.routes ""
</pre>

A simple `NetworkManager.service` restart is not enough to take it into effect; the routes will stay in `ip r`'s output. Taking the connection completely down, then up is needed for this:

<pre class="con">
<span class="psr">#</span> <span class="cmd">sudo bash -c "nmcli c down enX0 ; sleep 5 ; nmcli c up enX0" &
</pre>

### 4.5 Manipulating the Wi-Fi Connections

NetworkManager provides robust tools for managing **Wi-Fi connections**, including enabling/disabling **radios**, scanning for available networks, and connecting to both **open and secured access points**.

#### 4.5.1 Enabling and Disabling Wi-Fi

To **check the status** of Wi-Fi and WWAN radios:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli radio</span>
WIFI-HW  WIFI     WWAN-HW  WWAN
enabled  enabled  enabled  enabled
</pre>

To **disable** Wi-Fi:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli radio wifi off</span>
</pre>

**Verifying** the status after disabling:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli radio</span>
WIFI-HW  WIFI      WWAN-HW  WWAN
enabled  disabled  enabled  enabled
</pre>

**Re-enabling** Wi-Fi:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli radio wifi on</span>
</pre>

#### 4.5.2 Scanning for Available Wi-Fi Networks

To **list** all available Wi-Fi networks:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli device wifi list</span>
IN-USE  BSSID              SSID                MODE   CHAN  RATE        SIGNAL  BARS  SECURITY  
        48:AB:CD:98:76:04  DNA-WIFI-CCFE       Infra  11    130 Mbit/s  90      ▂▄▆█  WPA1 WPA2 
        48:AB:CD:98:76:09  DNA-WIFI-5Ghz-CCFE  Infra  36    270 Mbit/s  84      ▂▄▆█  WPA1 WPA2 
*       48:AB:CD:98:76:08  DNA-WIFI-CCFE       Infra  36    270 Mbit/s  70      ▂▄▆_  WPA1 WPA2 
        34:AB:CD:98:76:A8  Nipa                Infra  6     135 Mbit/s  52      ▂▄__  WPA1
</pre>

#### 4.5.3 Connecting to a Wi-Fi Network

**Connecting** to an Open Network:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli device wifi connect OpenHotspot</span>
</pre>

Connecting to a **Secured Network** with Password:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli device wifi connect MyWiFi password "my_secure_password"</span>
</pre>

If the connection is successful, it will automatically be saved for future use.

Connecting using a predefined Connection Profile, if you've **already connected before** and the profile exists:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli connection up MyWiFi</span>
</pre>

#### 4.5.4 Listing Saved Wi-Fi Connections

To view all saved Wi-Fi connections, e.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli connection show</span>
NAME       UUID                                  TYPE      DEVICE  
MyWiFi     a1b2c3d4-1234-5678-9abc-def012345678  wifi      wlp2s0  
GuestWiFi  f0e1d2c3-4567-89ab-cdef-9876543210ff  wifi      --      
Ethernet   12345678-90ab-cdef-1234-567890abcdef  ethernet  ens5    
</pre>

The `DEVICE` column shows (with green color) whether the connection is currently active.

#### 4.5.5 Forgetting a Wi-Fi Network

To **remove** a saved Wi-Fi connection:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli connection delete MyWiFi</span>
</pre>

#### 4.5.6 Troubleshooting Wi-Fi Issues

If Wi-Fi is not working properly, you can try to **restart NetworkManager**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">systemctl restart NetworkManager</span>
</pre>

To **check** whether the Wi-Fi adapter is detected, e.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli device status</span>
DEVICE  TYPE      STATE         CONNECTION
wlp2s0  wifi      connected     MyWiFi
ens5    ethernet  connected     Wired
lo      loopback  unmanaged     --
</pre>

If `wlp2s0` appears as `unavailable` or `disconnected`, try re-enabling it:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli device set wlp2s0 managed yes</span>
</pre>

If a connection attempt fails, **checking logs** may help:

<pre class="con">
<span class="psr">#</span> <span class="cmd">journalctl -u NetworkManager --no-pager | tail -n 20</span>
</pre>

As a last resort, **disabling and re-enabling** the Wi-Fi adapter might help:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli device disconnect wlp2s0</span>

<span class="psr">#</span> <span class="cmd">nmcli device connect wlp2s0</span>
</pre>

### 4.6 Teaming and Bonding

Teaming is deprecated in RHEL9, but it used to be the superior for several decades due to its greater features. Now bonding is emerged, and it uses kernel-close system calls, so it means significantly lower overhead as of today.

As a quick comparison, here are the main differences between teaming and bonding:

<table class="tan">
  <tr>
    <th>Feature</th><th>Teaming (RHEL 8)</th><th>Bonding (RHEL 9)</th>
  </tr>
  <tr>
    <td>Config Tool</td><td><p><code>teamd</code> (userspace)</p></td><td>Kernel bonding driver</td>
  </tr>
  <tr>
    <td>Monitoring</td><td><p><code>ethtool</code></p></td><td><p><code>miimon</code></p></td>
  </tr>
  <tr>
    <td>Config Method</td><td><p><code>team0</code> JSON config</p></td><td><p><code>bond.options</code> key-value</p></td>
  </tr>
  <tr>
    <td>Default Behavior</td><td>Needs explicit config</td><td>Bonding mode applied directly</td>
  </tr>
  <tr>
    <td>Performance</td><td>Higher flexibility</td><td>Lower overhead</td>
  </tr>
</table>

The **ports**, also known as "**slaves**", must include **at least two or more interfaces**. When naming them, you can use space characters, but in such cases, you need to enclose the names in quotes.

Both teaming and bonding can be configured during installation via **Anaconda** or later. It's also possible to set them up entirely using `nmcli` commands. Let's look at some examples.

#### 4.6.1 Setting up teaming via `nmcli`

Assuming we ensured these NIC ports are actually connected properly, we have the following, initial state of interfaces in this example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli d</span>
DEVICE     TYPE      STATE         CONNECTION    
ens4f0     ethernet  disconnected  --
ens5f0     ethernet  disconnected  --
</pre>

1. **Create the Team Interface (`team0`)**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type team con-name team0 \
                              ifname team0 \
                              mtu 9000 \
                              config '{"runner": {"name": "lacp", "tx_hash": ["eth", "l3", "l4"], "active": true, "fast_rate": true, "agg_select_policy": "lacp_prio"}, "link_watch": [{"name": "ethtool"}]}'</span>
    </pre>
    This sets up LACP (802.3ad) as the link aggregation mode, and also:
      - makes `team0` actively negotiate LACP
      - ensures faster failover detection
      - improves traffic distribution across aggregated links
      - esures priority-based aggregation selection
      - improves link failure detection

2. **Create the VLAN Interface (`team0.104`)**:
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type vlan con-name team0.104 \
                              ifname team0.104 \
                              dev team0 \
                              id 104 \
                              mtu 9000 \
                              ip4 10.25.104.99/22 \
                              gw4 10.25.107.251</span>

    <span class="psr">#</span> <span class="cmd">nmcli con mod team0.104 ipv4.dns "127.0.0.1,8.8.8.8"</span>

    <span class="psr">#</span> <span class="cmd">nmcli con mod team0.104 ipv4.dns-search "example.com"</span>
    </pre>

3. **Create Team Slave Connections**:
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type team-slave con-name team0_port_1 ifname ens4f0 master team0</span>

    <span class="psr">#</span> <span class="cmd">nmcli con add type team-slave con-name team0_port_2 ifname ens5f0 master team0</span>
    </pre>

The results must be similar to this:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli c</span>
NAME           UUID                                  TYPE      DEVICE
team0.104      4e9f34ca-e4f7-4360-8147-f43fe8f7474a  vlan      team0.104
team0          f4771208-adad-4a03-a114-36cdd2ad294e  team      team0
team0_port_1   02b7506c-fdaf-424c-a117-d26fb9a07b54  ethernet  ens4f0
team0_port_2   aecfed5f-454e-45cc-b427-cf4d10de53e3  ethernet  ens5f0    

<span class="psu">$</span> <span class="cmd">nmcli d</span>
DEVICE     TYPE      STATE        CONNECTION
team0.104  vlan      connected    team0.104
ens4f0     ethernet  connected    team0_port_1
ens5f0     ethernet  connected    team0_port_2
team0      team      connected    team0
</pre>

It's also important to ensure the "Aggregator ID" is the same. This is how you can do it when teaming is used:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo teamdctl team0 state</span>
setup:
  runner: lacp
ports:
  ens4f0
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 2
    runner:
      aggregator ID: 8, Selected
      selected: yes
      state: current
  ens5f0
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 2
    runner:
      aggregator ID: 8, Selected
      selected: yes
      state: current
runner:
  active: yes
  fast rate: yes
</pre>

#### 4.6.2 Setting up bonding via `nmcli`

Similarly as above, assuming we ensured these NIC ports are actually connected properly, we have the following, initial state of interfaces in this example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli d</span>
DEVICE     TYPE      STATE        CONNECTION    
ens4f0     ethernet  disconnected  --
ens5f0     ethernet  disconnected  --
</pre>

1. Create the Bond Interface (`bond`):
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type bond con-name bond-con \
                              ifname bond-dev \
                              mtu 9000 \
                              ipv4.method "disabled" \
                              ipv6.method "disabled" \
                              bond.options "mode=802.3ad,miimon=1,updelay=0,downdelay=0"</span>
    </pre>

2. Create the VLAN Interface (`bond.104`):
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type vlan con-name bond-con.104 \
                              ifname bond-dev.104 \
                              dev bond-dev \
                              id 104 \
                              mtu 9000 \
                              ip4 10.25.104.99/22 \
                              gw4 10.25.107.251</span>

    <span class="psr">#</span> <span class="cmd">nmcli con mod bond-con.204 ipv4.dns "8.8.8.8,1.1.1.1"</span>

    <span class="psr">#</span> <span class="cmd">nmcli con mod bond-con.204 ipv4.dns-search "example.com"</span>
    </pre>

3. Add Bond Slave Interfaces (`ens4f0`, `ens5f0`):
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nmcli con add type bond-slave con-name bond-port-1 ifname ens4f0 master bond-dev</span>

    <span class="psr">#</span> <span class="cmd">nmcli con add type bond-slave con-name bond-port-2 ifname ens5f0 master bond-dev</span>
    </pre>

The results must be similar to this:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmcli c</span>
NAME          UUID                                  TYPE      DEVICE       
bond-con.104  b4897223-ffd7-4ce2-bacd-d962cf5e048d  vlan      bond-dev.104
bond-con      292e0d10-5de6-48b3-b909-99661e595928  bond      bond-dev
bond-port-1   e5fddb9b-a889-41b3-a689-9cf24d52ddd7  ethernet  ens4f0
bond-port-2   816e8f07-29c0-43e8-a729-bc5424f6bd90  ethernet  ens5f0
lo            f163214d-51eb-45e9-8ab9-98db87ffe37a  loopback  lo

<span class="psu">$</span> <span class="cmd">nmcli d</span>
DEVICE        TYPE      STATE                   CONNECTION
bond-dev.104  vlan      connected               bond-con.104
bond-dev      bond      connected               bond-con
ens4f0        ethernet  connected               bond-port-1
ens5f0        ethernet  connected               bond-port-2
lo            loopback  connected (externally)  lo

<span class="psu">$</span> <span class="cmd">ip -4 a</span>
1: lo: &lt;LOOPBACK,UP,LOWER_UP&gt; mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
11: bond-dev.104@bond-dev: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 9000 qdisc noqueue state UP group default qlen 1000
    inet 10.25.104.99/22 brd 10.25.107.255 scope global noprefixroute bond-dev.104
       valid_lft forever preferred_lft forever
</pre>

Similarly to teaming, "Aggregator ID" must be the same for these bonded ports:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat /proc/net/bonding/bond</span>
Ethernet Channel Bonding Driver: v5.14.0-503.22.1.el9_5.x86_64

Bonding Mode: IEEE 802.3ad Dynamic link aggregation
Transmit Hash Policy: layer2 (0)
MII Status: up
MII Polling Interval (ms): 1
Up Delay (ms): 0
Down Delay (ms): 0
Peer Notification Delay (ms): 0

802.3ad info
LACP active: on
LACP rate: slow
Min links: 0
Aggregator selection policy (ad_select): stable

Slave Interface: ens4f0
[...]
Aggregator ID: 1
[...]

Slave Interface: ens5f0
[...]
Aggregator ID: 1
[...]
</pre>


<a name="5"></a>
## 5. Secure Shell (SSH)

**Secure Shell** (**SSH**) is a protocol that provides a secure way to access and manage remote systems over an encrypted network connection. SSH replaces older insecure methods such as **Telnet**, **RSH**, and **FTP**, offering **authentication, encryption, and data integrity**.

Beyond its primary purpose of remote login, SSH also enables **secure file transfers**, **port forwarding**, **proxy connections**, and even **encrypted filesystems via SSHFS**. This chapter explores SSH's various features, including authentication, configuration, tunneling, and advanced use cases.

### 5.1 General Usage of SSH

SSH can be used for logging into remote servers securely. Here are some common login methods:

- Login as the current user or as defined in the SSH client config:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh jumpsrv1</span>
    [...]
    <span class="psu">[john.doe@jumpsrv1 ~]$</span>
    </pre>  
- Login with an explicitly specified username:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh john.doe@jumpsrv1</span>
    [...]
    <span class="psu">[john.doe@jumpsrv1 ~]$</span>
    </pre>
- Alternative syntax using `-l`:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh -l john.doe jumpsrv1</span>
    [...]
    <span class="psu">[john.doe@jumpsrv1 ~]$</span>
    </pre>
- Logging in to a server via a jumphost:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh -J jumpsrv1 websrv1.prv.example.com</span>
    [...]
    <span class="psu">[john.doe@websrv1 ~]$</span>
    </pre>
- Using additional options with `-o`:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh -o StrictHostKeyChecking=no -o ConnectTimeout=5 websrv1.prv.example.com</span>
    [...]
    <span class="psu">[john.doe@websrv1 ~]$</span>
    </pre>

### 5.2 Configuring SSH

SSH configuration is managed globally in `/etc/ssh/`, which contains:

- **SSH client configuration** (`ssh_config`) for outbound connections.
- **SSH server configuration** (`sshd_config`) for incoming connections.
- **Host keys**, which verify the authenticity of remote systems.

A centralized location for user authentication keys can be set up in `sshd_config`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo grep authorized_keys /etc/ssh/sshd_config</span>
# Centralized location for authorized_keys
AuthorizedKeysFile /etc/ssh/authorized_keys/%u
</pre>

Users can define personal SSH client configurations in `~/.ssh/config`:

```
Host jumpsrv1 jumpsrv2 jumpsrv3 jumpsrv4
  AddKeysToAgent yes
  User john.doe
  ForwardAgent yes
  ServerAliveInterval 60
  Hostname %h.mgmt.example.com
  IdentityFile ~/.ssh/id_rsa

Host *.prv *.prv.example.com
  AddKeysToAgent yes
  User john.doe
  ProxyJump jumpsrv2
  ForwardAgent yes
  IdentityFile ~/.ssh/id_rsa
  ServerAliveInterval 60
```

To print effective SSH server settings:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo sshd -T</span>
</pre>

### 5.3 SSH Authentication Keys

SSH uses two types of keys:

- **Authentication keys**: Used for passwordless login.
- **Host keys**: Verify the identity of SSH servers.

Common SSH key types:

- **RSA** (2048-bit or 4096-bit recommended)
- **ED25519** (modern, secure, and fast)
- **ECDSA** (256-bit or 384-bit)
- **DSA** (deprecated)

Generating a key:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh-keygen -t ed25519</span>
</pre>

Copying the public key to a remote server:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh-copy-id -i ~/.ssh/id_ed25519.pub user@server</span>
</pre>

### 5.4 SSH Tunneling and Port Forwarding

SSH allows encrypting connections between local and remote hosts via port forwarding.

#### 5.4.1 Local Port Forwarding

Local port forwarding tunnels traffic from a local port to a remote service.

Example: Forward local port `8080` to a remote web server on port `80`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh -L 8080:remote-server:80 user@gateway</span>
</pre>

#### 5.4.2 Remote Port Forwarding

Remote forwarding exposes a local service to a remote machine.

Example: Make local port `22` available on `gateway` as port `2222`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh -R 2222:localhost:22 user@gateway</span>
</pre>

#### 5.4.3 Dynamic Port Forwarding (SOCKS Proxy)

Using SSH as a SOCKS proxy to anonymize traffic:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh -D 1080 user@gateway</span>
</pre>

Configure applications to use `localhost:1080` as a SOCKS proxy.

### 5.5 SSHFS: Mounting Remote Filesystems

SSHFS (SSH Filesystem) allows mounting remote directories over SSH, serving as a lightweight alternative to NFS.

**Advantages over NFS:**
- Works over SSH, avoiding firewall issues.
- Requires no additional server setup beyond SSH.
- Encrypts data transmission.

**Mounting a remote directory:**

<pre class="con">
<span class="psu">$</span> <span class="cmd">sshfs user@server:/remote/path /local/mountpoint</span>
</pre>

**Unmounting:**

<pre class="con">
<span class="psu">$</span> <span class="cmd">fusermount -u /local/mountpoint</span>
</pre>

### 5.6 SSH Host Keys

Check local host keys:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ls -1 /etc/ssh/ssh_host_*pub</span>
</pre>

Retrieve remote host keys:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh-keyscan server</span>
</pre>

### 5.7 SSH Agent

Load a private key into SSH agent:

<pre class="con">
<span class="psu">$</span> <span class="cmd">eval $(ssh-agent -s)</span>

<span class="psu">$</span> <span class="cmd">ssh-add ~/.ssh/id_rsa</span>
</pre>

List loaded keys:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh-add -L</span>
</pre>

### 5.8 CA Key-Based Authentication

**SSH CA Key-Based Authentication** allows issuing short-lived SSH certificates for users instead of distributing static public keys. This provides centralized control, reducing key sprawl and enhancing security. Only the **SSH server side** requires preparation, while users can request and receive certificates dynamically.

Using SSH CA authentication offers significant advantages:

✅ **Centralized control** – No need to distribute static public keys.  
✅ **Short-lived certificates** – Reduces the risk of key leaks.  
✅ **Flexible access management** – Easy revocation of compromised credentials.  
✅ **Simplified automation** – No need to pre-distribute keys across multiple servers.

By implementing CA-based authentication, SSH security is greatly enhanced while **simplifying user management**.

### 5.8.1 Setting Up the SSH Certificate Authority (CA)

The first step is to establish an SSH **Certificate Authority (CA)** that will sign users' public keys.

1. **Generate the CA Keypair**  
On a secure host, generate an RSA key pair to act as the CA, e.g.:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mkdir -p ~/cakeys && ssh-keygen -t rsa -b 4096 -f ~/cakeys/ca_user_key</span>
    Generating public/private rsa key pair.
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again:
    Your identification has been saved in /root/cakeys/ca_user_key
    Your public key has been saved in /root/cakeys/ca_user_key.pub
    </pre>

   **Important Notes**:

    - The **private key** (`ca_user_key`) should be stored securely, as it is used to sign certificates.
    - The **passphrase** must be stored in a secure vault.
    - The **public key** (`ca_user_key.pub`) will be distributed to SSH servers.

2. **Copy the Public CA Key to SSH Servers**  
On every server that should trust the CA, add the public key to `/etc/ssh/`:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">cp ~/cakeys/ca_user_key.pub /etc/ssh/ca_user_key.pub</span>
    </pre>
Modify the **SSH server configuration** to trust this CA:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">echo "TrustedUserCAKeys /etc/ssh/ca_user_key.pub" >> /etc/ssh/sshd_config</span>
    </pre>
Restart the SSH daemon to apply changes:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl restart sshd</span>
    </pre>

### 5.8.2 Preparing the User's Key for Signing

Before a user can authenticate, they must generate an SSH key and submit it for signing.

1. **Generate a User Keypair**  
On the client machine, the user generates an **Ed25519** keypair (recommended for security and performance):
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519</span>
    Generating public/private ed25519 key pair.
    </pre>
2. **Submit the Public Key for Signing**  
The user sends the generated `.pub` file (`id_ed25519.pub`) to the administrator for signing.  
3. **Prepare the SSH Configuration File**  
While waiting for the signed certificate, the user can preconfigure their SSH client:  
    <pre>
    Host *
    User infra
    IdentityFile ~/.ssh/id_ed25519
    CertificateFile ~/.ssh/id_ed25519-cert.pub
    ServerAliveInterval 60
    </pre>
The `CertificateFile` should point to the signed certificate once received.

### 5.8.3 Signing the User’s Key

Once the administrator has received the **user's public key**, they sign it with the **CA private key**.

1. **Ensure the CA Key is Secure**  
Set correct permissions before signing:
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">chmod 600 ~/cakeys/ca_user_key</span>
    </pre>
2. **Sign the User’s Key with an Expiry Time**  
The following command signs the user’s public key (`id_ed25519.pub`) with the **CA private key**, issuing a certificate valid for **2 weeks**:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ssh-keygen -s ~/cakeys/ca_user_key -I Visitor -n infra -V +2w ~/.ssh/id_ed25519.pub</span>
    Enter passphrase: 
    Signed user key id_ed25519-cert.pub: id "Visitor" serial 0 for infra valid from 2024-09-13T16:52:00 to 2024-09-27T16:53:43
    </pre>
   **Explanation of options:**  

    - `-s ~/cakeys/ca_user_key` → Uses the CA private key to sign.
    - `-I Visitor` → Identifier for the certificate.
    - `-n infra` → Allows authentication as the `infra` user.
    - `-V +2w` → Valid for 2 weeks.

3. **Deliver the Signed Certificate to the User**  
The signed certificate (`id_ed25519-cert.pub`) is sent back to the user.

### 5.8.4 Logging in with a Signed Certificate

Once the user has received their **signed certificate**, they place it in `~/.ssh/` and attempt to log in:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh rocky94srv1.example.com</span>
The authenticity of host 'rocky94srv1.example.com (192.168.56.101)' can't be established.
ED25519 key fingerprint is SHA256:97+NnQ/gU+VQciTyPTxu7uiJKSu0cCgzr3NUriL7WwY.
This key is not known by any other names
<span class="psu">Are you sure you want to continue connecting (yes/no/[fingerprint])?</span> <span class="cmd">yes</span>
Warning: Permanently added 'rocky94srv1.example.com' (ED25519) to the list of known hosts.
</pre>

If authentication succeeds, it confirms that the **CA-based authentication** is functioning correctly.

### 5.8.5 Verifying the Signed Certificate

Users or administrators can inspect the details of a signed certificate at any time:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ssh-keygen -Lf ~/.ssh/id_ed25519-cert.pub</span>
.ssh/id_ed25519-cert.pub:
        Type: ssh-ed25519-cert-v01@openssh.com user certificate
        Public key: ED25519-CERT SHA256:Lf5dz7O30J0nvm6ZpBv77+Yfqpk/oEBORfoIJLbuCmA
        Signing CA: RSA SHA256:gY+/EEL8RuUN7cMX6XT3Ge8DCR9Pa2NSOuPLsggGEno (using rsa-sha2-512)
        Key ID: "Visitor"
        Serial: 0
        Valid: from 2024-09-13T16:52:00 to 2024-09-27T16:53:43
        Principals: 
                infra
        Critical Options: (none)
        Extensions: 
                permit-X11-forwarding
                permit-agent-forwarding
                permit-port-forwarding
                permit-pty
                permit-user-rc
</pre>

This output confirms:

- **The certificate type** (user certificate).
- **The CA signature** used to sign the key.
- **The allowed login user (`infra`)**.
- **The validity period** of the certificate.

### 5.9 Revoking SSH Keys

Traditionally, SSH key management relied on users removing old or untrusted public keys from their `~/.ssh/authorized_keys` file manually. However, since OpenSSH **5.4/5.4p1** (released in 2010), SSH servers can now centrally **revoke keys**, ensuring that even if a key remains in `authorized_keys`, it will be denied.

To revoke an SSH key, OpenSSH allows defining a **revoked keys file**, which must be referenced in the SSH daemon configuration (`sshd_config`). Keys can be revoked using:

- **Plain text lists**: A simple text-based format listing revoked public keys.
- **Key Revocation Lists (KRLs)**: A more efficient **binary format** that scales better for larger deployments.

### 5.9.1 Using a Plain Text Revoked Keys List

For simple revocation, a **text-based revoked keys list** can be stored at `/etc/ssh/revoked_keys`, containing one revoked key per line.

1. Create the file and add the **public key** to revoke:  
    <pre>
    ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAr4Bd3[...]2gx admin@oldhost
    </pre>
2. Configure `sshd` to reference this file:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">echo "RevokedKeys /etc/ssh/revoked_keys" >> /etc/ssh/sshd_config</span>
    </pre>
3. Restart the SSH daemon:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl restart sshd</span>
    </pre>

This approach is simple but **not scalable** for large environments, as the file is read line by line.

### 5.9.2 Using Key Revocation Lists (KRLs)

For **larger-scale key management**, OpenSSH provides **Key Revocation Lists (KRLs)**, a **binary format** that allows revoking **specific keys, key hashes, or entire certificate authorities (CAs)**.

**Creating and Using a KRL File**:

1. **Generate an Empty KRL File**  
First, create an empty KRL file:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ssh-keygen -kf /etc/ssh/revoked_keys.krl</span>
    </pre>
2. **Add a Public Key to the KRL**  
To revoke a specific key:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ssh-keygen -kuf /etc/ssh/revoked_keys.krl ~/.ssh/id_ed25519.pub</span>
    </pre>
3. **Verify the Contents of the KRL File**  
You can check which keys are included:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ssh-keygen -Qlf /etc/ssh/revoked_keys.krl</span>
    # KRL version 0
    # Generated at 20250124T112128
    hash: SHA256:730/0NwOZI4NPEJ3tR8OeOZzRShuIxDZgJjrdamwxcY # ssh-ed25519
    </pre>
4. **Reference the KRL File in `sshd_config`**  
Modify SSH daemon settings:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">echo "RevokedKeys /etc/ssh/revoked_keys.krl" >> /etc/ssh/sshd_config</span>
    </pre>
5. **Restart `sshd` to Apply Changes**  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl restart sshd</span>
    </pre>

### 5.9.3 Checking if a Key is Revoked

Once a KRL is in place, compare it against existing keys:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ssh-keygen -Qf /etc/ssh/revoked_keys.krl ~/.ssh/*pub</span>
/home/user/.ssh/id_ecdsa.pub: ok
/home/user/.ssh/id_ed25519.pub: ok
/home/user/.ssh/id_ed25519_tf.pub: REVOKED
/home/user/.ssh/id_rsa.pub: ok
</pre>

### 5.9.4 Choosing Between Plain Text Revocation and KRLs

- **Use a plain text revoked keys file** (`RevokedKeys /etc/ssh/revoked_keys`) if:
    - You have a **small number** of revoked keys.
    - You need a **quick and manual** way to revoke keys.
    - You are not managing SSH at scale.
- **Use a Key Revocation List (KRL)** (`RevokedKeys /etc/ssh/revoked_keys.krl`) if:
    - You need **better performance** when revoking many keys.
    - You want to revoke **entire certificate authorities (CAs)**.
    - You need a **secure, compressed format** that is not human-readable.

By implementing **SSH key revocation**, administrators can prevent unauthorized access **centrally**, without relying on users to manually clean up old or compromised keys.


<a name="6"></a>
## 6. NFS and Samba

**Network file sharing** is essential for seamless data access across multiple systems in a network. Linux provides two primary solutions for sharing files over a network:

- **NFS (Network File System)**: Designed for Unix/Linux environments, allowing transparent file access across machines.
- **Samba (SMB/CIFS)**: Provides file and printer sharing, primarily for interoperability with Windows systems.

This chapter covers the configuration, usage, security best practices, and troubleshooting of both NFS and Samba.

### 6.1 NFS (Network File System)

NFS allows remote systems to **mount and access directories as if they were local**. It is commonly used in Linux/Unix environments for centralized storage and shared directories.

#### 6.1.1 Installing NFS

To use NFS, install the required packages:

For RHEL-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install -y nfs-utils</span>
</pre>

For Debian-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">apt install -y nfs-kernel-server</span>
</pre>

#### 6.1.2 Configuring an NFS Server

1. Define shared directories in `/etc/exports`:  
    <pre>
    /srv/nfs/share  192.168.1.0/24(rw,sync,no_root_squash)
    </pre>
    Explanation:
    - `rw` → Read/write access.
    - `sync` → Ensures data is written immediately (safer but slower).
    - `no_root_squash` → Allows root users from clients to retain root access.
2. Export the shares:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">exportfs -rav</span>
    </pre>
3. Start and enable the NFS service:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl enable --now nfs-server</span>
    </pre>

#### 6.1.3 Configuring an NFS Client

To mount an NFS share:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mount -t nfs 192.168.1.100:/srv/nfs/share /mnt</span>
</pre>

To make it persistent, add the following entry to `/etc/fstab`, e.g.:

```
192.168.1.100:/srv/nfs/share /mnt nfs defaults 0 0
```

#### 6.1.4 NFS Security Considerations

- Use `root_squash` to prevent root users on clients from having full privileges.
- Restrict access to specific subnets or hosts.
- Enable NFSv4 and Kerberos authentication for secure communication.

### 6.2 Samba (SMB/CIFS)

Samba allows Linux systems to share files and printers with **Windows and other SMB-compatible devices**.

#### 6.2.1 Installing Samba

For RHEL-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install -y samba samba-client</span>
</pre>

For Debian-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">apt install -y samba</span>
</pre>

#### 6.2.2 Configuring a Samba Server

1. Edit the main configuration file `/etc/samba/smb.conf`:  
    <pre>
    [shared]
        path = /srv/samba/share
        read only = no
        browsable = yes
        guest ok = no
        valid users = @smbgroup
    </pre>
2. Create the shared directory:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mkdir -p /srv/samba/share</span>

    <span class="psr">#</span> <span class="cmd">chown -R root:smbgroup /srv/samba/share</span>

    <span class="psr">#</span> <span class="cmd">chmod 2775 /srv/samba/share</span>
    </pre>
3. Add a Samba user:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">smbpasswd -a username</span>
    </pre>
4. Restart the Samba service:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl restart smb nmb</span>
    </pre>

#### 6.2.3 Connecting to a Samba Share from a Client

From a Linux system:

<pre class="con">
<span class="psu">$</span> <span class="cmd">smbclient -U username -L //server-ip</span>
</pre>

To mount the share:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mount -t cifs -o username=username //server-ip/shared /mnt</span>
</pre>

For a persistent mount, add to `/etc/fstab`:

```
//server-ip/shared /mnt cifs username=username,password=yourpassword 0 0
```

From Windows, access it via `\\\server-ip\shared` in File Explorer.

#### 6.2.4 Samba Security Considerations

- Use strong passwords for Samba users.
- Restrict access by IP range.
- Disable guest access unless explicitly needed.
- Configure firewalls to allow only necessary traffic.

### 6.3 Troubleshooting NFS and Samba

#### 6.3.1 Troubleshooting NFS

- Verify exported directories:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">exportfs -v</span>
    </pre>
- Check if the NFS service is running:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl status nfs-server</span>
    </pre>
- Check firewall rules (NFS uses ports **2049** and **111**):  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --list-services</span>
    </pre>
- Debug mount issues:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">showmount -e 192.168.1.100</span>
    </pre>

#### 6.3.2 Troubleshooting Samba

- Check Samba configuration syntax:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">testparm</span>
    </pre>
- Verify running services:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl status smb nmb</span>
    </pre>
- List active Samba connections:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">smbstatus</span>
    </pre>
- Test authentication:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">smbclient -U username //server-ip/shared</span>
    </pre>

### 6.4 Choosing Between NFS and Samba

<table class="tan">
  <tr>
    <th>Feature</th>
    <th>NFS</th>
    <th>Samba (SMB/CIFS)</th>
  </tr>
  <tr>
    <td><strong>Best for</strong></td>
    <td>Linux-to-Linux file sharing</td>
    <td>Windows/Linux/macOS file sharing</td>
  </tr>
  <tr>
    <td><strong>Performance</strong></td>
    <td>Generally faster in Unix/Linux environments</td>
    <td>Slightly slower but better for mixed OS environments</td>
  </tr>
  <tr>
    <td><strong>Security</strong></td>
    <td>Supports Kerberos for authentication</td>
    <td>User authentication via Samba passwords</td>
  </tr>
  <tr>
    <td><strong>Firewall considerations</strong></td>
    <td>Uses ports 2049, 111</td>
    <td>Uses ports 445, 139</td>
  </tr>
</table>


For Linux-only environments, **NFS** is preferred due to performance and simplicity. For mixed environments (Windows, Linux, macOS), **Samba** provides better interoperability.


<a name="7"></a>
## 7. Managing On-Demand Network Services with xinetd

**xinetd** (*eXtended Internet Services Daemon*) is a replacement for the traditional `inetd` super-server. It provides a **centralized** and **efficient** way to manage network-based services that start only when needed, rather than running continuously in the background.

While `xinetd` was historically used for services like **FTP**, **Telnet**, **TFTP**, and **time-based services**, its usage has declined with the rise of more secure alternatives (like SSH replacing Telnet). However, it is still used for managing custom TCP/IP-based services efficiently.

Key advantages of `xinetd`:
- **On-demand service management**: Services are started only when a connection request is received.
- **Security controls**: Supports access control, logging, and resource limits.
- **Load management**: Prevents overloading the system by limiting the number of simultaneous connections.

### 7.1 Installing and Enabling xinetd

Most modern distributions still provide `xinetd`, even though it's less commonly used.

#### 7.1.1 Installing xinetd

For RHEL-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install -y xinetd</span>
</pre>

For Debian-based systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">apt install -y xinetd</span>
</pre>

#### 7.1.2 Enabling and Starting xinetd

After installation, enable and start the service:

<pre class="con">
<span class="psr">#</span> <span class="cmd">systemctl enable --now xinetd</span>
</pre>

Verify that `xinetd` is running:

<pre class="con">
<span class="psr">#</span> <span class="cmd">systemctl status xinetd</span>
</pre>

### 7.2 Understanding xinetd Configuration

The main configuration file is `/etc/xinetd.conf`, which defines global settings for all services. Individual service configurations are stored in `/etc/xinetd.d/`.

#### 7.2.1 Global Configuration (`/etc/xinetd.conf`)

Here is an example of the global configuration file:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cat /etc/xinetd.conf | grep -v ^# | sed '/^[[:space:]]*$/d'</span>
defaults
{
    log_type        = SYSLOG daemon info
    log_on_failure  = HOST
    log_on_success  = PID HOST DURATION EXIT
    cps             = 50 10
    instances       = 50
    per_source      = 10
    v6only          = no
    groups          = yes
    umask           = 002
}
includedir /etc/xinetd.d
</pre>

- `log_type` → Logs activities to **syslog**.
- `cps 50 10` → Limits incoming connections to **50 per second**, pausing for **10 seconds** if exceeded.
- `instances 50` → Limits concurrent instances of `xinetd`-managed services.
- `per_source 10` → Restricts each source IP to **10 simultaneous connections**.
- `includedir /etc/xinetd.d` → Loads service-specific configurations from this directory.

### 7.3 Managing xinetd Services

Each service managed by `xinetd` has its own configuration file under `/etc/xinetd.d/`. Below is an example service definition.

#### 7.3.1 Example: Enabling a Simple TCP Service

Example: Running a **custom TCP service** on port **9999**.

1. Create the configuration file `/etc/xinetd.d/custom_service`:  
    <pre>
    service custom_service
    {
        socket_type = stream
        protocol = tcp
        wait = no
        user = root
        server = /usr/local/bin/custom_server
        port = 9999
        disable = no
    }
    </pre>
2. Reload `xinetd`:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl restart xinetd</span>
    </pre>
3. Verify that the service is now available:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">ss -tulpn | grep 9999</span>
    </pre>

### 7.4 Security Considerations for xinetd

Since `xinetd` is often used for managing older and potentially insecure services, security hardening is critical.

#### 7.4.1 Restricting Access by IP Address

To allow access **only from specific IPs**, modify the service configuration:

```
service telnet
{
    disable = no
    socket_type = stream
    protocol = tcp
    user = root
    server = /usr/sbin/in.telnetd
    only_from = 192.168.1.0/24
    no_access = ALL
}
```

#### 7.4.2 Limiting Connection Rates

To prevent **Denial of Service (DoS) attacks**, set connection limits:

```
cps = 25 5   # Allows 25 connections per second, then pauses for 5 seconds
per_source = 5  # Max 5 connections per IP
instances = 50  # Max 50 instances of the service
```

#### 7.4.3 Enabling TCP Wrappers

Many `xinetd` services still support **TCP Wrappers** (`/etc/hosts.allow` and `/etc/hosts.deny`).

Example: Allow only specific hosts:

```
# /etc/hosts.allow
in.telnetd: 192.168.1.0/24
```

Block all others:

```
# /etc/hosts.deny
in.telnetd: ALL
```

### 7.5 Troubleshooting xinetd Services

#### 7.5.1 Checking xinetd Logs

Since `xinetd` logs to **syslog**, check logs with:

<pre class="con">
<span class="psr">#</span> <span class="cmd">journalctl -u xinetd --no-pager</span>
</pre>

#### 7.5.2 Verifying Service Configuration

To check syntax and reload the service:

<pre class="con">
<span class="psr">#</span> <span class="cmd">systemctl restart xinetd</span>
</pre>

If a service isn’t working, ensure it’s enabled:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cat /etc/xinetd.d/<service> | grep disable</span>
disable = no
</pre>

#### 7.5.3 Checking Open Ports

Verify if `xinetd` is listening on the expected port:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ss -tulpn | grep xinetd</span>
</pre>

#### 7.5.4 Testing Service Accessibility

Use `telnet` or `nc` to test service accessibility:

<pre class="con">
<span class="psu">$</span> <span class="cmd">telnet localhost 9999</span>
</pre>

Or with `ncat`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ncat -zv localhost 9999</span>
</pre>

### 7.6 Summary: Should You Use xinetd?

While `xinetd` offers flexible service management, **modern systemd-based socket activation** has largely replaced it. However, it remains useful for:
- **Legacy services** that require on-demand activation.
- **Rate-limited** or **restricted-access** network services.
- **Custom TCP/IP services** requiring centralized control.

For most new deployments, **systemd socket activation** (`systemd-socket-activate`) is the preferred alternative.


<a name="8"></a>
## 8. Firewall Solutions

Linux provides multiple firewall solutions, each with different levels of flexibility, usability, and automation. The three most common firewall implementations are:

- **IPTables**: A traditional rule-based firewall system, still widely used in legacy environments.
- **nftables**: The modern replacement for IPTables, designed to be more efficient and flexible.
- **firewalld**: A dynamic firewall management tool that simplifies firewall configuration, primarily used on RHEL-based distributions.

Additionally, **FERM (For Easy Rule Making)** is often used as an abstraction layer to make IPTables rulesets easier to manage.

This chapter covers these firewall solutions, their usage, and key configuration examples.

### 8.1 IPTables

**IPTables** is one of the oldest and most powerful firewall solutions on Linux. It operates using a set of rules organized into chains and tables, determining how packets should be handled. While still in use, it is gradually being replaced by **nftables** and **firewalld** on modern systems.

#### 8.1.1 Listing Current Rules

Example: Listing rules in a system handling many SFTP connections:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo iptables -L -n -w | grep sftpcustomera</span>
sshd       all  --  194.21.20.20      0.0.0.0/0            /* sftpcustomera */
sshd       all  --  194.21.20.19      0.0.0.0/0            /* sftpcustomera */
sshd       all  --  194.21.20.18      0.0.0.0/0            /* sftpcustomera */
sshd       all  --  194.21.20.17      0.0.0.0/0            /* sftpcustomera */
[...]
</pre>

#### 8.1.2 Blocking and Unblocking an Outgoing Port

Example: Blocking and later allowing outgoing connections on port 443:

1. Verify that the connection works:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ssh -o ConnectTimeout=3 index.com -p 443</span>
    kex_exchange_identification: Connection closed by remote host
    </pre>
2. Block outgoing connections on port 443:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">iptables -A OUTPUT -p tcp --dport 443 -j DROP</span>
    </pre>
3. Verify that connections are now blocked:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">iptables -L</span>
    [...]
    Chain OUTPUT (policy ACCEPT)
    target     prot opt source               destination         
    DROP       tcp  --  anywhere             anywhere             tcp dpt:https

    <span class="psr">#</span> <span class="cmd">ssh -o ConnectTimeout=3 index.com -p 443</span>
    ssh: connect to host index.com port 443: Connection timed out
    </pre>
4. Remove the blocking rule:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">iptables -D OUTPUT -p tcp --dport 443 -j DROP</span>
    </pre>
5. Verify that the connection works again:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ssh -o ConnectTimeout=3 index.com -p 443</span>
    kex_exchange_identification: Connection closed by remote host
    </pre>

### 8.2 IPTables with FERM

**FERM (For Easy Rule Making)** is a wrapper for IPTables that allows administrators to write more structured and readable firewall rules. This makes managing large or complex rulesets easier, especially when automating with configuration management tools like Ansible.

Example of a **FERM configuration file** (`/etc/ferm/ferm.conf`):

```
table filter {
    chain INPUT {
        policy DROP;
        proto tcp dport ssh ACCEPT;
        proto icmp ACCEPT;
    }
    chain OUTPUT {
        policy ACCEPT;
    }
}
```

To apply the rules:

<pre class="con">
<span class="psr">#</span> <span class="cmd">ferm /etc/ferm/ferm.conf</span>
</pre>

FERM improves firewall management but still relies on **IPTables**, meaning its limitations remain.

### 8.3 `nftables`

**nftables** is the modern replacement for IPTables, offering better performance, easier syntax, and greater flexibility. It allows complex rule sets to be managed programmatically, making it ideal for **automation** and **templating**.

#### 8.3.1 Configuring nftables

Example: An Ansible template to allow Cockpit access only from a company VPN:

```
# {{ ansible_managed }}

# Allow Cockpit from Company VPN only
table inet filter {
    chain input {
        ip saddr { {{ company_vpn_ips | replace (" ", ",") }} } counter jump cockpit
    }
    chain cockpit {
        tcp dport { websm } counter accept
    }
}
```

#### 8.3.2 Enabling NAT for Virtual Machines

This `nftables` configuration allows **internet access** for nested virtual machines:

```
table ip nat {
    chain postrouting {
        type nat hook postrouting priority srcnat; policy accept;
        ip daddr 10.0.0.0/8 accept;
        ip daddr 172.16.0.0/12 accept;
        ip daddr 192.168.0.0/16 accept;
        oif "virbr-dev" masquerade;
    }
}
```

#### 8.3.3 Applying and Persisting nftables Rules

1. Apply rules manually:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">nft -f /etc/nftables.conf</span>
    </pre>
2. Ensure rules persist after reboot:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">systemctl enable nftables</span>
    </pre>

### 8.4 `firewalld`

**firewalld** is a higher-level firewall management tool designed for **dynamic** rule changes. It uses **zones** instead of raw rules, making it easier to configure firewall settings without manually handling chains and tables.

#### 8.4.1 Basic firewalld Usage

1. Check the firewall status:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --state</span>
    </pre>
2. List active rules:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --list-all</span>
    </pre>
3. Allow SSH traffic permanently:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --permanent --add-service=ssh</span>

    <span class="psr">#</span> <span class="cmd">firewall-cmd --reload</span>
    </pre>

#### 8.4.2 Configuring Zones

firewalld uses **zones** to define different levels of trust for network interfaces.

1. List available zones:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --get-zones</span>
    </pre>
2. Assign an interface to a zone:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --permanent --zone=trusted --add-interface=eth1</span>
    </pre>
3. Reload the firewall to apply changes:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">firewall-cmd --reload</span>
    </pre>

### 8.5 Choosing the Right Firewall Solution

<table class="tan">
  <tr>
    <th>Feature</th>
    <th>IPTables</th>
    <th>nftables</th>
    <th>firewalld</th>
  </tr>
  <tr>
    <td><strong>Complexity</strong></td>
    <td>High</td>
    <td>Moderate</td>
    <td>Low</td>
  </tr>
  <tr>
    <td><strong>Automation</strong></td>
    <td>Limited</td>
    <td>Excellent</td>
    <td>Good</td>
  </tr>
  <tr>
    <td><strong>Performance</strong></td>
    <td>Moderate</td>
    <td>High</td>
    <td>Moderate</td>
  </tr>
  <tr>
    <td><strong>Ease of Use</strong></td>
    <td>Difficult</td>
    <td>Moderate</td>
    <td>Easy</td>
  </tr>
</table>

For **legacy** or highly customized systems, **IPTables** or **FERM** may still be used. However, **nftables** is the recommended replacement for modern deployments, and **firewalld** is ideal for dynamic rule management.

This chapter provides a structured comparison of Linux firewall solutions, helping you choose the right one for your environment.


<a name="9"></a>
## 9. Troubleshooting Network

Network troubleshooting is an essential skill for diagnosing connectivity issues, service failures, packet loss, and misconfigurations. This chapter covers key Linux tools and techniques to analyze and debug network problems.

### 9.1 Checking Interface and Packet Statistics with `ip`

The `ip` command provides detailed interface statistics, which can help diagnose packet drops, transmission errors, and connectivity issues.

Example: Checking packet statistics for the `ens5f0` interface:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ip -s -h l show dev ens5f0</span>
4: ens5f0: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 9000 qdisc mq master team0 state UP mode DEFAULT group default qlen 1000
    link/ether 94:6d:ae:70:c7:e2 brd ff:ff:ff:ff:ff:ff
    RX:  bytes packets errors dropped  missed   mcast           
         82.5M    143k      0       0       0   18.7k 
    TX:  bytes packets errors dropped carrier collsns           
         47.0M    116k      0       0       0       0 
    altname enp129s0f0
</pre>

Key indicators to check:
- **Errors**: Hardware failures, incorrect MTU settings, or driver issues.
- **Dropped packets**: Potential network congestion or misconfigured QoS.
- **Missed packets**: May indicate buffer overflows or insufficient queue length.

### 9.2 Checking Open Ports and Listening Services with `ss`

The `ss` (socket statistics) command replaces `netstat` for checking open sockets, active connections, and services listening on specific ports.

Example: Checking if a service (e.g., `zabbix_agent2`) is listening on port **10050**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo ss -tulpn | grep 10050</span>
tcp    LISTEN     0      128    [::]:10050              [::]:*                   users:(("zabbix_agent2",pid=243599,fd=6))
</pre>

Options used:
- `-t` → Show TCP sockets.
- `-u` → Show UDP sockets.
- `-l` → Show only listening sockets.
- `-p` → Show process names using the sockets.
- `-n` → Show numerical addresses instead of resolving names.

### 9.3 Testing Connectivity with `ncat`

`ncat` (from the `nmap` suite) is useful for testing open ports and verifying whether a service is reachable.

Example: Checking if SSH (port **22**) is accessible:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ncat -zv my-server.example.com 22</span>
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 10.25.4.163:22.
Ncat: 0 bytes sent, 0 bytes received in 0.04 seconds.
</pre>

Example: Checking if Zabbix Agent (port **10050**) is reachable:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ncat -zv my-server.example.com 10050</span>
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection timed out.
</pre>

If a connection times out, the service may not be running, firewall rules may be blocking access, or network routing may be misconfigured.

### 9.4 Scanning Networks with `nmap`

`nmap` is a powerful tool for network exploration, security auditing, and port scanning.

#### 9.4.1 Listing IPs in a Subnet

Example: Listing all IP addresses in the `10.20.0.0/16` network:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmap -sL -n 10.20.0.0/16</span>
[...]
Nmap scan report for 10.20.0.0
Nmap scan report for 10.20.0.1
[...]
Nmap scan report for 10.20.255.255
Nmap done: 65536 IP addresses (0 hosts up) scanned in 1.20 seconds.
</pre>

#### 9.4.2 Scanning for Open Ports

Example: Checking if **port 22 (SSH)** is open on a host:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmap -p 22 10.20.4.8</span>
[...]
PORT   STATE SERVICE
22/tcp open  ssh
[...]
</pre>

#### 9.4.3 Scanning Multiple Hosts

Example: Scanning port 22 on multiple hosts using a range:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmap -v -p 22 10.20.4.8-11</span>
</pre>

#### 9.4.4 Checking Active Hosts in a Subnet

Example: Finding hosts with an open SSH port (22) in `10.20.0.0/16`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">nmap -v -Pn -p 22 10.20.0.0/16 --open | awk '/open port 22/{print $6}'</span>
</pre>

Options explained:
- `-v` → Verbose output.
- `-Pn` → Skip ping checks (useful if ICMP is blocked).
- `-p <port>` → Specify the port to scan.
- `--open` → Show only hosts with open ports.

### 9.5 Capturing Packets with `tcpdump`

`tcpdump` allows capturing and analyzing network traffic, which is useful for diagnosing connectivity issues and debugging low-level network problems.

#### 9.5.1 Capturing Traffic for a Specific Host

Example: Listening for traffic between `team0.204` and `10.26.204.1`:

Originally, the communication was failing:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo tcpdump -nni team0.204 host 10.26.204.1</span>
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on team0.204, link-type EN10MB (Ethernet), capture size 262144 bytes
[...]
14:49:40.073440 IP 10.26.204.1.34724 > 10.24.204.3.2181: Flags [S], seq 2381228630, win 26880, options [mss 8960,nop,nop,sackOK,nop,wscale 7], length 0
14:49:41.093244 IP 10.26.204.1.34724 > 10.24.204.3.2181: Flags [S], seq 2381228630, win 26880, options [mss 8960,nop,nop,sackOK,nop,wscale 7], length 0
</pre>

Successfully capturing **ICMP** (ping) and **TCP handshake** traffic when the network team has fixed an issue:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo tcpdump -nni team0.204 host 10.26.204.1</span>
15:49:06.050743 IP 10.26.204.1 > 10.24.204.3: ICMP echo request, id 123, seq 1, length 64
15:49:06.050773 IP 10.24.204.3 > 10.26.204.1: ICMP echo reply, id 123, seq 1, length 64
15:49:12.510293 IP 10.26.204.1.39890 > 10.24.204.3.2181: Flags [S], seq 1656045964, win 26880, options [mss 8960,nop,nop,sackOK,nop,wscale 7], length 0
</pre>

This confirms that ICMP and TCP communication are now functioning correctly.

### 9.6 Querying DNS with `dig`

The `dig` (**Domain Information Groper**) command is a powerful tool for querying **DNS records** and debugging domain name resolution issues. It is widely used because it provides **detailed output**, supports **customized queries**, and is more flexible than **`nslookup`** or **`host`** commands.

#### Why Use `dig` Over `nslookup` or `host`?

- **More Detailed Output**: `dig` provides comprehensive query results, including response times, authority, and additional records.
- **Better Scripting Compatibility**: Unlike `nslookup`, which has an interactive mode, `dig` outputs structured data suitable for automation.
- **Supports Various DNS Record Types**: Easily queries A, AAAA, MX, CNAME, TXT, NS, and other record types.
- **Allows Querying Specific Nameservers**: Useful for troubleshooting propagation issues by checking multiple DNS servers.

#### 9.6.1 Basic Usage of `dig`

The simplest way to use `dig` is by querying a domain without additional options, e.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dig example.com</span>
; <<>> DiG 9.11.3-1ubuntu1.17-Ubuntu <<>> example.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 12345
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; QUESTION SECTION:
;example.com.           IN      A

;; ANSWER SECTION:
example.com.    300     IN      A       93.184.216.34

;; Query time: 15 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Thu Feb 15 12:34:56 UTC 2025
;; MSG SIZE  rcvd: 55
</pre>

Explanation:

- **`QUESTION SECTION`**: Shows the requested domain (`example.com`) and record type (`A`).
- **`ANSWER SECTION`**: Displays the resolved IP address (`93.184.216.34`).
- **`Query time`**: Indicates how long the request took.
- **`SERVER`**: The DNS server that handled the query (Google Public DNS in this case: `8.8.8.8`).

#### 9.6.2 Querying a Specific DNS Record

By default, `dig` retrieves **A records** (IPv4 addresses). You can specify other record types:

- **Query AAAA (IPv6) record:**  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">dig AAAA example.com</span>
    </pre>
- **Query CNAME (Canonical Name) record:**  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">dig CNAME www.example.com</span>
    </pre>
- **Query NS (Name Server) records for a domain:**  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">dig NS example.com</span>
    </pre>
- **Query TXT records (e.g., for SPF, DKIM, or verification keys):**  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">dig TXT example.com</span>
    </pre>

#### 9.6.3 Checking Mail Exchange (MX) Records

`MX` (Mail Exchange) records define mail servers responsible for receiving emails for a domain. If email delivery issues occur, checking `MX` records is crucial. E.g.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dig MX example.com</span>
;; ANSWER SECTION:
example.com.    3600    IN      MX      10 mail1.example.com.
example.com.    3600    IN      MX      20 mail2.example.com.
</pre>

Explanation:

- The **lower priority number** (10) is preferred.
- `mail1.example.com.` is the **primary** mail server.
- `mail2.example.com.` is a **backup mail server** (higher priority number means lower preference).

#### 9.6.4 Using a Specific DNS Server for Queries

By default, `dig` queries the system's configured DNS resolver. To test DNS propagation or check records on a different nameserver, specify one manually:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dig @8.8.8.8 example.com</span>
</pre>

- `@8.8.8.8` forces the query to use **Google Public DNS**.
- Useful for checking if DNS records have propagated across different providers.

#### 9.6.5 Getting a Short, Concise Answer

For automation or quick lookups, use the `+short` option:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dig example.com +short</span>
93.184.216.34
</pre>

Similarly, to get only MX records in a compact form:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dig MX example.com +short</span>
10 mail1.example.com.
20 mail2.example.com.
</pre>

### 9.6.6 Summary: When to Use `dig`

- **Basic lookups:** `dig example.com`
- **Query specific record types:** `dig MX/TXT/CNAME/NS example.com`
- **Check DNS propagation:** `dig @8.8.8.8 example.com`
- **Debug mail delivery issues:** `dig MX example.com`
- **Retrieve only relevant data:** `dig +short example.com`

Using `dig` effectively provides deeper insights into **domain resolution**, **DNS health**, and **mail server configurations**, making it an essential tool for network troubleshooting.
