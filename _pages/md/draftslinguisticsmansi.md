# ![alt text](/_ui/img/flags/mansi_m.svg) Manysi nyelv / Мāньси лāтыӈ

<div style="text-align: right;"><h7>Posted: 2020-04-29 20:48</h7></div>

###### .

<div class="info">
<p>Ez a bejegyzés még rendkívül hiányos, szerkesztés alatt áll.</p>
</div>

<a href="mansi_people.jpg" target="_blank" title="">
  <img src="mansi_people.gif" title="" align="right" class="right"></a>

## Tartalomjegyzék

1. [Bevezető - ](#1)
2. [Az ábécé - ](#2)
3. [Alapmondatok, kifejezések - ](#3)
4. [Főnév - ](#4)
5. [Melléknév - ](#5)
6. [Számnév - ](#6)
7. [Névmás - ](#7)
8. [Ige - ](#8)
9. [Határozószó - ](#9)
10. [Névelő - ](#10)
11. [Elöljárószók - ](#11)
12. [Kötőszavak, módosítószók és egyéb határozószók - ](#12)
13. [Állítás és tagadás - ](#13)
14. [Rövid dialógusok - ](#14)
15. [Mesék, versek - ](#15)
16. [Kiadványok, linkek](#16)

<a name="1"></a>
## 1. Bevezető

A finnugor nyelvek az ősi uráli nyelvből fejlődtek ki, és egyes vélekedések szerint a napjainkban még élő nyelvek közül a finn áll a legközelebb az ősi urálihoz. A tudomány jelen állása szerint az uráli alapnyelvet nagyjából 6000 évvel ezelőtt beszélték, majd ahogy vándoroltak a népek, évszázadok alatt vált szét az alábbi ábrán is feltűntetett Ugor alapnyelvre és a Balti-finn nyelvek őseire, amelyeket 3000 évvel ezelőtt használhattak. 

![alt_text](uralic_lang.png "Az uráli nyelvek egymáshoz való viszonya és a ma élő finnugor nyelvek")

A **manysi** egy nagyjából 900 fő által anyanyelvként beszélt nyelv, amely az *Uráli* nyelvcsalád *Finnugor* nagycsoportjához, azon belül az *Obi-ugor* alcsoportba tartozik. Legközelebbi rokona a szintén Obi-ugor **hanti** és az *Ugor* **magyar**. Mind az ősi szavakat, a nyelvek logikáját és hangzásait tekintve a manysi tekinthető a modern magyar nyelv legközelebbi rokonának. Beszélői többnyire az oroszországi Hanti- és Manysiföldön élnek az *Ob folyó* alsó folyásánál, illetve az Ob egyes mellékfolyóinak körzetében. 

![alt_text](ugric_lang.png "Ugor és Obi-ugor nyelvjárások")

A manysi közösségek egymástól nem ritkán több száz km-es távolságban alakultak ki, ezért alapvetően 4 fő dialektus különböztethető meg, amelyek olyan eltéréseket mutatnak (mutattak), hogy egyik-másik nyelvjárás között még a kölcsönös megérhetőség is nehézkes. A 4 nyelvjárás: 

- az **északi** (a Szoszva, a Szigva, a Ljapin, a Lozva felső folyásánál és az Ob mentén), 
- a **keleti** (a Konda felső, középső és alsó részén illetve a Yukonda partján), 
- a napjainkra már kihalt **nyugati** (a Pelim, a Lozve alsó és középső részén és a Szoszva déli területein)
- a szintén kihalt **déli** (a Tavda folyó mentén)

A **manysi irodalmi nyelv** a szoszvai, azaz az **északi** dialektust használja. 

A hanti és manysi népeknek, történelmük során rengeteg viszontagságot kellett átvészelniük. Mivel a hatalmas Oroszországi Föderáció területének belsejében élnek, önállóságot soha sem kaphattak igazán, és a politikai lépések többnyire számukra kifejezetten kedvezőtlen kimenetelűek voltak. A manysik 50-60 éve még egyáltalán, vagy csak nagyon gyengén beszélték az orosz nyelvet, mára azonban egyre inkább eloroszosodtak. Emiatt még a nyelv kismértékű szabványosítása és a saját írásbeliség elterjedésének ellenére is különösen veszélyeztetett nyelvnek minősül. 

<a name="2"></a>
## 2. Az ábécé

A manysi nyelv első írott formája 1868-ban jelent meg a Máté evangéliumának fordításában, de akkor még latin betűket használva.  
A jelenleg használatos cirill ábécét 1937-ben kezdték bevezetni, majd 1939-ben vált általános szabvánnyá. 

### 2.1 A manysi ábécé betűi

<table class="tan">
  <tr>
    <td><b>А a</b></td><td><b>Ā ā</b></td><td><b>Б б</b></td><td><b>В в</b></td><td><b>Г г</b></td><td><b>Д д</b></td><td><b>Е е</b></td><td><b>Ē ē</b></td>
  </tr>
  <tr>
    <td><b>Ё ё</b></td><td><b>Ë̄ ё̄</b></td><td><b>Ж ж</b></td><td><b>З з</b></td><td><b>И и</b></td><td><b>Ӣ ӣ</b></td><td><b>Й й</b></td><td><b>К к</b></td>
  </tr>
  <tr>
    <td><b>Л л</b></td><td><b>М м</b></td><td><b>Н н</b></td><td><b>Ӈ ӈ</b></td><td><b>О о</b></td><td><b>Ō ō</b></td><td><b>П п</b></td><td><b>Р р</b></td>
  </tr>
  <tr>
    <td><b>С с</b></td><td><b>Т т</b></td><td><b>У у</b></td><td><b>Ӯ ӯ</b></td><td><b>Ф ф</b></td><td><b>Х х</b></td><td><b>Ц ц</b></td><td><b>Ч ч</b></td>
  </tr>
  <tr>
    <td><b>Ш ш</b></td><td><b>Щ щ</b></td><td><b>Ъ ъ</b></td><td><b>Ы ы</b></td><td><b>Ы̄ ы̄</b></td><td><b>Ь ь</b></td><td><b>Э э</b></td><td><b>Э̄ э̄</b></td>
  </tr>
  <tr>
    <td><b>Ю ю</b></td><td><b>Ю̄ ю̄</b></td><td><b>Я я</b></td><td><b>Я̄ я̄</b></td><td colspan="4"></td>
  </tr>
</table>

### 2.2 Kiejtés

A **betűk kiejtése** a Nemzetközi Fonetikai Ábécé (*International Phonetic Alphabet, IPA*) jelölése alapján, valamint a magyarban megtalálható hangokkal összehasonlítva: 

**a** [ a ] - Röviden ejtett magyar "á". Csak palóc nyelvjárásban és egyes erdélyi szociolektusokban fordul elő.  
**ā** [ aː ] - "á"  
**б** [ b ] - "b"  
**в** [ ◌ʷ ] - "v"  
**г** [ ɣ ], [ ɡ ] - nagyon lágy, zöngés *veláris* (hátul képzett) réshangként, de jövevényszavaknál simán csak "g" hangként ejtik  
**д** [ d ] - "d"  
**е** [ ʲe ] - "je", illetve lehet "e" is, de az előtte lévő mássalhangzó lágyításával  
**ē** [ ʲe: ] - Hosszú "je" ("jee"), illetve lehet "ee" is, de az előtte lévő mássalhangzó lágyításával  
**ё** [ ʲo ] - "jo"  
**ё̄** [ ʲo: ] - "jó"  
**ж** [ ʒ ] - "zs"  
**з** [ z ] - "z"  
**и** [ i ] - "i"  
**ӣ** [ i: ] - "í"  
**й** [ j ] - "j"  
**к** [ k ] - "k"  
**л** [ l ], [ ʎ ] - "l", de előfordulhat közelebb a [j]-hez mint az [l]-hez. Mint az angol: *million*. A magyar "ly" régies ejtése.  
**м** [ m ] - "m"  
**н** [ n ], [ ɲ ] - "n" vagy "ny"  
**ӈ** [ ŋ ] - (*velar nasal*) "n"-el indul, de hátsó orrhangként elfolytódik. Pont ilyen hangunk nincs, de pl. angolban ilyen a *sing*.  
**о** [ o ] - "o"  
**ō** [ o: ] - "ó"  
**п** [ p ] - "p"  
**р** [ r ] - "r"  
**с** [ s ] - "sz"  
**т** [ t ] - "t"  
**у** [ u ] - "u"  
**ӯ** [ uː ] - "ú"  
**ф** [ f ] - "f"  
**х** [ χ ] - Erőteljes "h", mint a magyarban a *technika*, *technikum*  
**ц** [ t͡s ] - "c"  
**ч** [ t͡ʃʲ ] - "cs"  
**ш** [ ʃ ] - "s"  
**щ** [ ʃʲtʃʲ ] - "scsá"  
**ъ** [ - ] - keményjel  
**ы** [ ɪ ] - Az "i" és az "ü" közötti, "Elöl-középen képzett, középső-felső nyelvállású, ajakréses magánhangzó" (yeri)  
**ы̄** [ ɪ: ] - Az előbbi "i" és az "ü" közötti hang, de hosszan  
**ь** [ ◌ʲ ] - lágyságjel  
**э** [ ə~ɤ ] - "e"  
**э̄** [ ə~ɤ: ] - Hosszú "e" ("ee")  
**ю** [ ʲu ] - "ju"  
**ю̄** [ ʲu: ] - "jú"  
**я** [ ʲa ] - "j" + röviden ejtett "palóc" "á"  
**я̄** [ ʲa: ] - "já"

<a name="3"></a>
## 3. Alapmondatok, kifejezések - 

A továbbiakban a magyarral nagyon erős hasonlóságot mutató szavakat és kifejezéseket pirossal jelöltem. 

**а-а** - igen  ↔
**ати** - nem  

**ōлы** - <r>él</r>, létezik, van  
**āтюм** - nincs

**Пāся!** / **Пāся ōлэн!** - Szia! / Szervusz!  
**Ёмас алпыл!** - Jó reggelt!  
**Ёмас хотал!** - Jó napot!  
**Ёмас этипала!** - Jó estét!  
**Нāн хуму?** - Hogy vagy?  
**Пуссын, ёмас!** - Köszönöm, jól!  

<a name="4"></a>
## 4. Főnév - 

A manysiban sincsenek a főneveknek nemei. 

...

#### Tárgyak

**нāй** - tűz  
**вит** - <r>víz</r>  
**хōтал** - nap  
**я̄** - folyó  
**тӯр** - tó  
**щāращ** - tenger  

**хори** - láp, mocsár  
**ня̄р** - láp, mocsár  
**кирп** - sár (kiszáradt sár)  

**ня̄нь** - kenyér  
**вой** - <r>vaj</r>  
**мāй** - méz  
**тэ̄п** - <r>táp</r>, takarmány  
**кол** - ház  
**вēтра** - vödör

#### Állatok

**хул** - <r>hal</r>  
**лув** - <r>ló</r>  
**āмп** - eb  
**кати** - macska  
**хāр кати** - kandúr macska  
**пищ-пищ** - egér  
**лāпанты** - <r>lepke</r>  
**хулах** - <r>holló</r>  
**хотаӈ** - <r>hattyú</r>  

#### Emberek

**хум** - férfi, <r>hím</r>  
**нэ̄** - <r>nő</r>  
**пыгрись** - fiú  
**пыг** - fiúgyermek  
**агирись** - lány  
**аги** - leánygyermek  
**āща** (vagy **āщ**) - apa  
**щāнь** - anya  

**маньси** - manysi  
**ханты** - hanti  
**венгр** - magyar  
**русь** - orosz  
**ёрн** - nyenyec  

**сам** - <r>szem</r>  
**ӯлум** - <r>álom</r>  

#### Az idő számlálása

**хōтал** - nap (úgy is, mint égitest és úgy is, mint 24 óra)  
**сāт** - hét (úgy is, mint számnév, úgy is, mint 7 nap)  
**э̄тпос** - hónap  
**тāл** - év  

**хӯрмит хōталт** - tegnapelőtt ("a harmadik napon")  
**мōлхōтал** - tegnap  
**тыхōтал** - ma  
**хōталпалыт** - egész nap  
**холыт** - holnap  
**мōтхōтал** - holnapután  

**сāт ōвыл хōтал** - hétfő ("a hét első napja")  
**сāт китыт хōтал** - kedd  
**сāт хӯрмит хōтал** - szerda  
**сāт нилыт хōтал** - csütörtök  
**сāт атыт хōтал** - péntek  
**сāт хотит хōтал** - szombat  
**ӯщлахтын хōтал** - vasárnap ("a pihenés napja")

**тӯя** - tavasz  
**тув** - nyár  
**таквыс**  - ősz  
**тāл** - <r>tél</r>  

**Тāл котиль э̄тпос** - január  
**Рēтыӈ ю̄свой э̄тпос** - február  
**Мāньполь э̄тпос** - március  
**Яныгпōль э̄тпос** - április  
**Я̄ӈк нāтнэ э̄тпос** - május  
**Лӯпта э̄тпос** - június  
**Ōйттур э̄тпос** - július  
**Вōртур э̄тпос** - augusztus  
**Сӯкыр э̄тпос** - szeptember  
**Мāнь таквс э̄тпос** - október  
**Яныг таквс э̄тпос** - november  
**Вāт сāграпнал э̄тпос** - december  

#### Időjárás

**хōталкāт** - napsütés  
**вōт** - szél  
**ракв** - eső  
**тӯйт** - hó  
**я̄ӈк** - <r>jég</r>

#### Égtájak

**лӯйи мā** - észak  
**мōртым мā** - dél, "ismeretlen föld"  
**хōтал нэ̄йлын ēр** - kelet ("nap kelésének oldala")  
**хōтал ӯнтнэ ēр** - nyugat ("nap nyugvásának oldala")  

<a name="5"></a>
## 5. Melléknév

**ёмас** - <r>jó</r>  
**люль** - rossz  

**хоса** - <r>hosszú</r>  
**вāти** - rövid  

**вуйкан** - fehér, fényes  
**сэ̄мыл** vagy **шэ̄мыл** - fekete  
**выйыр** - <r>vörös</r>, piros  
**атырхарпа** - kék, kékes  

<a name="6"></a>
## 6. Számnév - 

#### Tőszámnevek egytől tízig: 

**аква** - egy  
**китыr** - <r>kettő</r>  
**хӯрум** - <r>három</r>  
**нила** - <r>négy</r>  
**ат** - <r>öt</r>  
**хот** - <r>hat</r>  
**сат** - hét  
**нёллов**  - <r>nyolc</r>  
**онтеллов** - kilenc  
**лов** - tíz

Az 1-nek és 2-nek jelzői formája is használatos: **акв** és **кит** , akárcsak a magyarban a *"két"* szó esetében, ami az ómagyarban *"kit"* volt.

#### Tizen-valahányak: 

**аквхуйплов** - tizenegy  
**китхуйплов** - tizenkettő  
**хурумхуйплов** - tizenhárom  
**нилахуйплов** - tizennégy  
**атхуйплов** - tizenöt  
**хотхуйплов** - tizenhat  
**сатхуйплов** - tizenhét  
**нёлоловхуйплов** - tizennyolc  
**онтоловхуйплов** - tizenkilenc

#### Tízesével százig: 

**хус** - <r>húsz</r>  
**ват** - harminc  
**налиман** - <r>negyven</r>  
**атпан** - <r>ötven</r>  
**хотпан** - <r>hatvan</r>  
**сатлов** - hetven  
**нёлсат** - <r>nyolcvan</r>  
**онтырсат** - kilencven  
**сат** - <r>száz</r>

#### A húsz feletti számok összefűzése rendkívül egyedi: 

**ват нупыл аква** - 21 (*harminc felé egy*)  
**ват нупыл китыг** - 22 (*harminc felé kettő*)  
**налиман нупыл хурум** - 33 (*negyven felé három*)  
**атпан нупыл хот** - 46 (*ötven felé hat*)  
**хотпан нупыл сат** - 57 (*hatvan felé hét*)

#### Ezrek: 

**сотэр** *(szoter)* - ezer

...

<a name="7"></a>
## 7. Névmás - 

### Személyes névmások

A manysiban a többes szám kiegészül még azzal az esettel, amikor csak duális mennyiségről van szó. Így pl. a *mi* is lehet *mi ketten* ill. *mi mindannyian*. 

**ам** - én  
**наӈ** - te  
**тав** - ő  
**мēн** - mi *(te és én)*  
**мāн** - mi *(mi, többen (mindannyian))*  
**нэ̄н** (vagy **нэ̄нки**) - ti *(ti, ketten)*  
**нāн** - ti *(ti, többen)*  
**тэ̄н** - ők *(ők, ketten)*  
**тāн** (vagy **тāнки**) - ők *(ők, többen)*

### Mutató névmások

**ты** - ez  
**тыя** - ezt  
**та** - az  
**тайи** - azt  

### Kérdő névmások

**мāныр** - mi  
**хотьют** - ki, kié (**хотьют ут** - valakié)  
**хōт** - hol  
**хоталь** - hová, valahová  

**Наӈ манарын тот атынтэ̄йн?** - Te mit szimatolsz itt?  
**Ты кeнт хотьют ут?** - Kié lesz az a sapka?  
**Хотьют ут тыт та хультум.** - Valakinek a holmija itt maradt.  

<a name="8"></a>
## 8. Ige - 

**мини** - menni  
**али** - ölni  
**алисьли** - vadászni  
**юнти** - varrni, ölteni  
**ловиньти** - olvasni, gondolni  
**поели** - rajzolni  
**рупити** - dolgozni  
**хайти** - futni (hajtani (?))  
**тиламли** - repülni  
**учли** - ülni, állni (?)  
**люли** - állni  

...

**Ты тув пил āтюм** - E nyáron bogyó nincs (nem termett).  
**Ам хōтал ляльт сунсэ̄юм.** - Nézek a napba.  
**Атыт э̄лмхōлас ты урыл āнумн ты лāвыс.** - Már az ötödik ember mondja ezt nekem.  
**Ам пумась книга ловиньтэгум (ловиньтасум).** - Én érdekes könyvet olvasok (olvastam).  
**Ам ти пумась книга ловиньтилум (ловиньтаслум).** - Én ezt az érdekes könyvet olvasom (olvastam).  
**Над няврам эруптэгын.** - Te szereted (szeretted) a gyerekeket.  
**Мōтхōтал тав лāвхатас юв-ёхтуӈкв.** - Holnaputánra ő azt ígérte visszatér.  

**Тав аньмунтнув минас** - Ő nemrég távozott.  
**Амккем ке олсум, ам мот ман миннувм алыщлаӈкв** - Ha egyedül lennék, más helyekre mennék vadászni.  

...


<a name="9"></a>
## 9. Határozószó - 

Munkácsi Bernát: <a href="https://adtplus.arcanum.hu/hu/view/UgorFuzetek_11_1894/?pg=0&layout=s" target="_blank">Ugor füzetek</a> c. kötetéből egy részlet: 

![alt_text](ugor_fuzetek_-_manysi_helyhatarozoszok.png)


<a name="10"></a>
## 10. Névelő - 

<a name="11"></a>
## 11. Elöljárószók - 

<a name="12"></a>
## 12. Kötőszavak, módosítószók és egyéb határozószók - 

<a name="13"></a>
## 13. Állítás és tagadás - 

<a name="14"></a>
## 14. Rövid dialógusok - 

<a name="15"></a>
## 15. Mesék, versek - 

Ка̄тпаттамн а̄ӈкватэ̄гум –  
Тӯрыт, я̄т ам ка̄салэ̄гум,  
О̄лнэ колум тот сусхаты,  
Хо̄тал нё̄р тапа̄лн тӯйтхаты.  
Са̄лы лё̄ӈх хосыт сӯсэ̄гум,  
Са̄лы сун ам тот хо̄нтэ̄гум,  
Морах пилкве татем са̄в,  
Во̄ркве, пе̄с йис о̄лнэ ма̄в.  

Ка̄тум сым ляпан пинтлы̄лум –  
Ща̄ньтем э̄рыг ам хӯлылум,  
Ха̄птем йинэ суе о̄с,  
Тувыл суйты посыӈ А̄с.  
О̄пам ла̄тӈе сумылла̄лы,  
Аквтоп а̄ращ посымла̄лы,  
Ра̄тнэ ко̄йп суй хӯнтамласум,  
Ма̄-вит э̄рге ка̄саласум.  

Ка̄тпаттан наӈ а̄ӈквата̄лэн...  
Сымын ка̄тын пинумта̄лэ̄лн...  

>Ránézek a tenyeremre –  
>Tavat, folyót látok benne.  
>Lakóházam ott látszik,  
>Az Urálon túl a nap játszik.  
>Rénszarvas nyomát, ha látom,  
>Szánt ott biztosan találok.  
>Oly sok ott a mocsári málna,  
>Erdőcske, régi korunk világa.    
<br>
>Kezemet szívemre szorítom –  
>Anyácskám énekét hallom,  
>Csónakocskám közeledik,  
>Csobog a fényes Obon.  
>Apám hangja hallatszik,  
>A tűzhely lángja kialszik,  
<br>
>Hallgatom a dob ritmusát,  
>Benne hallom a világ dalát.  

>Nézz bele a tenyeredbe...  
>S tedd a kezed a szívedre...  

<a href="luima_seripos_no_21_2019.jpg" target="_blank" title="">
  <img src="luima_seripos_no_21_2019.gif" title="" align="right" class="right"></a>

<a name="16"></a>
## 16. Kiadványok, linkek - 

- Kiváló összefoglaló a manysik életéről, történelméről, sorsáról és kultúrájáról: <a href="http://fu.nytud.hu/hivmny.htm" target="_blank">Rokonszenv</a>
- A **Луима сэрипос** című *(Északi hajnal)* újság összes eddigi száma: <a href="http://www.khanty-yasang.ru/luima-seripos/no-1-1011" target="_blank">khanty-yasang.ru</a>
- Az *Orosz Alkalmazott Kutatások és Fejlesztések Obi-ugor Intézményének* weboldala, tele ingyenesen letölthető könyvekkel, szótárakkal .pdf formátumban: <a href="https://ouipiir.ru/node/287" target="_blank">ouipiir.ru</a>
    - <a href="https://ouipiir.ru/sites/default/files/docs/1124-2085.pdf" target="_blank">Manysi-Orosz Szótár</a>
- [Könyvek, szintén az Ob-Ugor Intézet weboldalról](https://ouipiir.ru/education-publications?field_sel_ethnos_value=mansi&field_authors_tid=All)
- a Helsinki Egyetem Észak Manysi nyelvkurzus anyaga: [Pohjoismansin peruskurssi](https://helda.helsinki.fi/items/6026e854-0058-4f5d-991c-54416d620af1)
