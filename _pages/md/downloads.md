# 📥 Downloads, Links

###### .

## Appendix

A *GTK+ programozás Free Pascalban* c. könyv melléklete, példaprogramok: <a class="zip" href="src-gtkpas.zip" target="_blank">src-gtkpas.zip (74.2 kB)</a>

## Short Scripts

<a class="zip" href="node_vscode_packager.sh.zip" target="_blank">node_vscode_packager.sh.zip (1.5 kB)</a> 

## Raku Binaries

<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.12-aix64.tar.gz" target="_blank">raku-6.d-2021.12-aix64.tar.gz (14.1 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.10-aix64.tar.gz" target="_blank">raku-6.d-2021.10-aix64.tar.gz (14.1 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.09-aix64.tar.gz" target="_blank">raku-6.d-2021.09-aix64.tar.gz (14.1 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.08-aix64.tar.gz" target="_blank">raku-6.d-2021.08-aix64.tar.gz (14.0 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.07-aix64.tar.gz" target="_blank">raku-6.d-2021.07-aix64.tar.gz (14.0 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.06-aix64.tar.gz" target="_blank">raku-6.d-2021.06-aix64.tar.gz (14.0 MB)</a>  
<a class="zip" href="https://filedn.com/lAVcBV9KnpUbSC1YiyioRPy/Raku/raku-6.d-2021.05-aix64.tar.gz" target="_blank">raku-6.d-2021.05-aix64.tar.gz (13.9 MB)</a>

## Links

- Ingyenes PowerPoint template-ek: <a href="http://www.slidescarnival.com/" target="_blank">slidescarnival.com</a>
- Google Analytics: <a href="https://analytics.google.com/analytics/web" target="_blank">analytics.google.com</a>

...


