# ![alt text](/_ui/img/icons/python_m.svg) Python

<div style="text-align: right;"><h7>Posted: 2023-02-27 22:28</h7></div>

###### .


## Table of Contents

1. [Introduction](#1)
2. [Environment Setup](#2)
3. [Python Scripting Basics](#3)
4. [Common Data Types](#4)
5. [Control Flow](#5)
6. [Functions](#6)
7. [Interacting with Files](#7)
8. [Standard Library Packages](#8)
9. [Handling Program Exceptions](#9)
10. [Classes](#10)
11. [PIP and VirtualEnv](#11)
12. [Structure of a Project](#12)
13. [Test-driven Development](#13)


<a name="1"></a>
## 1. Introduction

![alt_text](python_-_bg.jpg)

As you probably know, Python consistently ranks as one of the most popular programming languages. It appeared in the **Top 10 in 2002 Q2**, emerged to the **Top 3 since 2014 Q1**, and it's being the actual **Most Popular programming language since 2018 Q4**, at least as of February 2023. [Here](https://www.youtube.com/watch?v=qQXXI5QFUfw) is one of the references to that.

<img src="guido.jpg" class="right">

The language itself was originally created by the Duch programmer **Guido van Rossum**. Being as a successor to the **ABC programming language**, the first `0.9.0` version of Python was released in **20 February 1991**. 

Python3 is an **Object Oriented Scripting Language**. "Scripting", so it's an **Interpreter**: it doesn't generate binary code after it processed the source code, but it executes it immediately. Python3 supports both **Dynamic** and **Strong Type System**, it covers all the **Functional Concepts** such as *map*, *reduce*, *filter* etc. and its syntax is a **pseudo-code like** one, delimited by **whitespace**.

Its popularity increased so rapidly, probably because...

- even though its primary set of platforms are still represented by **Unix-like systems**, mostly Linux distros, it is an **open source** and **platform and processor architecture independent** development environment,
- as it has a pseudo-code like syntax, the programmer is enforced to write "pretty", **properly indented** code, therefore...
- it's a good choice for **teaching** programming (it was originally **designed for learning**, just like Pascal),
- several "cool" and well-paid IT areas and jobs require and use Python, such as **Data Science, DevOps and Web Development**, because of its efficiency and great flexibility

### 1.1 Legacy vs. Present vs. Future

As of today, it's still relatively usual when a computer has multiple Python versions installed, pushing the user into confusion sometimes, e.g.:

```ksh
$ ls -l $(which python python2 python3 python3.{1..99} 2>/dev/null)
-rwxr-xr-x 1 root root 28643224 Sep 20 09:56 /opt/Python3/bin/python3.12
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python -> python2.7
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python2 -> python2.7
lrwxrwxrwx 1 root root       10 Dec  4 23:40 /usr/bin/python3 -> python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 23:23 /usr/bin/python3.10
-rwxr-xr-x 1 root root    14448 Dec  4 23:40 /usr/bin/python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 21:51 /usr/bin/python3.9
```

Shortly, Python 2.x is **legacy**, while Python 3.x is the **present and future** of the language. There are still huge amount of code out there that relies on Python 2.x interpreter, so the last release of it from **April 20, 2020**, the Python `2.7.18` is still maintained and supported, it still gets security updates sometimes (e.g. the Jan 2023 release of it has the `2.7.18-28.1` patch release), but it won't be updated anymore from features point of view. 

The document [What’s New In Python 3.0](https://docs.python.org/3/whatsnew/3.0.html) perfectly describes what were the significant changes.  
It's interesting to note too, that since `3.8.0`, these new minor versions change in every year's October.


<a name="2"></a>
## 2. Environment Setup

Python hasn't been considered as an exotic component for decades, so at least two or more main versions of it must be accessible in your favorite Linux distribution's official package repository. There are also Python packages and their dependencies officially available for several other platforms. 

To prepare a host for everyday use of Python3 and other development tools, you can follow multiple scenarios. The most common ones: 

1. **install everything** using the Linux distro's package repositories
2. install **prerequisites and tools only**, then compile Python3 itself from its **source code**
3. same as the previous one but use an existing, precompiled, **Python3 binary archive**

The following subchapters cover all these scenarios with some overlaps, as e.g. **user-level preparations** and **PIP Environment's initial configuration** are needed for all of them.

### 2.1 Package Installations on a Linux distro

To follow along all the excercises in this article, these binaries and tools will be needed: 

- `git`
- `lsof`
- `tar`
- `wget`
- `which`
- `words` (`/usr/share/dict/words`)
- a **text editor**: optional, but having e.g. a `vim` is always useful
- and finally, the **Python3 itself**. It is **optional** if you compile it from source (see below) or you have already an archive for "installing" it. Otherwise, try to get the latest version of it (e.g. RHEL8.7 and Rocky 8.7 provides the version `3.9`).

On a freshly installed, "Minimal Install" **Rocky 8.7**, there is no `python` command as is, only a minimal environment, e.g. for `dnf`: 

```ksh
# which python python3
/usr/bin/which: no python in (/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin)
/usr/bin/which: no python3 in (/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin)

# rpm -qa | grep -i python | wc -l
28
```

In the scenario of this subchapter, **we don't install Python3 from its source**, so there's no need to install all the possible dependencies for compiling it. Therefore, **the following should be enough** to proceed: 

```ksh
# dnf search python3
[...]
python39.x86_64 : Version 3.9 of the Python interpreter
[...]

# dnf install -y git lsof tar wget which words python39

# python3 --version
Python 3.9.13
```

Again, if you use a custom, prepared archive for "installing" Python3 or you would go for compiling the latest version from source, **you can skip installing the package** `python3` in the previous command.

### 2.2 User-level Preparations

Using source control is essential nowadays, so let's do its initial configuration, e.g.: 

```ksh
$ git config --global user.name "Jakab Gipsz"

$ git config --global user.email "jakab.gipsz@example.com"
```

#### 2.2.1 Optional configurations

These are optional color and other formatting settings for Bash and VIM, recommended on [Linux Academy GitHub repo](https://github.com/linuxacademy/content-python-for-sys-admins). 

- a useful `.bashrc` config (also reload `bash`), and an example for how it also features showing the branch name of your Git repo in your `PS1` (command prompt): 

```ksh
$ curl https://raw.githubusercontent.com/linuxacademy/content-python-for-sys-admins/master/helpers/bashrc -o ~/.bashrc

$ exec $SHELL

~ $

~ $ mkdir sample ; cd sample

sample $ touch file.txt

sample $ git init
[...]
Initialized empty Git repository in /home/gjakab/sample/.git/

sample $ git add --all .

sample $ git status
On branch master
[...]

sample $ git commit -m "Added new file"
[master (root-commit) 1911477] Added new file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file.txt

sample[master] $ 
```

- a useful `.vimrc` config: 

```ksh
~ $ curl https://raw.githubusercontent.com/linuxacademy/content-python-for-sys-admins/master/helpers/vimrc -o  ~/.vimrc
```

### 2.3 Compiling the Latest Python3 version from Source

![alt_text](python_-_src_download.jpg)

It might be needed, mainly when the distro's official repo locks the minor version of Python3 but you want to use the latest, greatest version. 

#### 2.3.1 Prerequisites

Still remaining at Red Hat or its direct relatives like Rocky, compiling requires the "Development Tools" package group to be installed, most importantly, `gcc` and `make` and many other tools and libraries:

```ksh
# dnf groupinstall -y "development tools"
[...]
```

Further packages, required for compiling Python3 from source: 

```ksh
# dnf install -y bzip2-devel expat-devel gdbm-devel libffi-devel ncurses-devel openssl-devel readline-devel sqlite-devel tk-devel xz-devel zlib-devel
[...]
```

#### 2.3.2 Get the Source Code

The source code can be downloaded from [python.org](https://www.python.org/downloads/source/). As of February 2023, the latest version is 3.11.2. 

Copy the link of "Gzipped source tarball" and use `wget` to download it to your test server: 

```ksh
# cd /usr/src

# wget https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tgz

# tar -xzf Python-3.11.2.tgz

# cd Python-3.11.2
```

#### 2.3.3 Configuration and compilation

You can probably find hundreds of examples for the first method, using `altinstall`, such as populating everything under `/usr/local`, because it's easier to do. However it's not necessarily prettier. 

By the second potential method, you can ensure everything is under your desired directory, but in the other hand, that requires improving the `PATH` global variable (or linking). 

##### 2.3.3.1 Using `altinstall`

The `altinstall` makes sure everything will be installed under `/usr/local`, letting your original Python3-related files, that were installed through `rpm`/`dnf`, intact. 

```ksh
# ./configure --enable-optimizations
[...]
config.status: creating pyconfig.h
configure: creating Modules/Setup.local
configure: creating Makefile

# make altinstall
[...]

# python3.11 --version
Python 3.11.2
```

##### 2.3.3.2 Using custom `--prefix` directory

The other option is specifying `--prefix` for `configure`, e.g. `--prefix /opt/Python3`, but this is still something to be tested more.

```ksh
# ./configure --enable-optimizations --prefix /opt/Python3
[...]
config.status: creating pyconfig.h
configure: creating Modules/Setup.local
configure: creating Makefile

# make
[...]

# make install
[...]

# du -sh /opt/Python3
422M	/opt/Python3

# /opt/Python3/bin/python3 --version
Python 3.11.2
```

Once you are done, make sure this custom `/opt/Python3` is added to your regular user's `PATH` variable, e.g.:

```ksh
~ $ grep PATH .bashrc
export PATH=$HOME/bin:$PATH:/opt/Python3/bin
```

#### 2.3.4 Configure PIP environment for your regular user and test

1. **Improve the `secure_path`** for `sudo` to include either `/usr/local/bin` too: 

```ksh
~ $ sudo vim /etc/sudoers
[...]
Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
[...]
```

...or your custom directory: 

```ksh
~ $ sudo vim /etc/sudoers
[...]
Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/opt/Python3/bin
[...]
```

2. **Refresh PIP** (it should work the same way either you used `altinstall` or `prefix`):

```ksh
~ $ sudo pip3.11 install --upgrade pip
Requirement already satisfied: pip in /usr/local/lib/python3.11/site-packages (22.3.1)
Collecting pip
  Downloading pip-23.0.1-py3-none-any.whl (2.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 2.1/2.1 MB 17.0 MB/s eta 0:00:00
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 22.3.1
    Uninstalling pip-22.3.1:
      Successfully uninstalled pip-22.3.1
Successfully installed pip-23.0.1
```

Now you're done. 

<div class="warn">
<p>If you used <code>--prefix</code> for compiling, <b>this is the phase when you should consider packaging the whole installation</b>, e.g. the <code>Python3</code> folder under your <code>/opt</code>, to a nicely named <code>.tar.gz</code> archive.</p>
</div>


<a name="3"></a>
## 3. Python Scripting Basics

### 3.1 REPL for Rapid Experimentation

**REPL** stands for **Read, Evaluate, Print, Loop** (same as for other scripting languages anyway, like for Raku).

```ksh
~ $ python3
Python 3.11.2 (main, Feb 28 2023, 18:30:49) [GCC 8.5.0 20210514 (Red Hat 8.5.0-16)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Some initial tests, including the mandatory "Hello World", then `exit()` from REPL: 

```ksh
>>> 1 + 1
2
>>> None
>>> print("Hello World!")
Hello World!
>>> 
>>> exit()
~ $ 
```

You can use the `quit()` function too for exit or `Ctrl+D`.

<div class="warn">
<p>Notice that in REPL, simply entering any string, number or variable is enough to print it on the screen, but you need <code>print()</code> function in "real" source codes.</p>
</div>

The other, useful key command in Python's REPL is `Ctrl+L` to clear the screen. 

### 3.2 Python as an Invoked One-Liner

Just like in Perl's and Raku's case, Python can be invoked e.g. as a piece of shell command, e.g.:

```ksh
~ $ python3 -c 'print("Hello World!")'
Hello World!

~ $ python3 -c 'print(1+1)'
2
```

### 3.3 Source Files as Python Programs

The real world Python programs and overall, anything more complex than the above, simple cases require storing our Python code in source code files. 

As for any other shell scripts, if the interpreter is not specified by the shebang (after `#!`), you need to invoke your source file by invoking the interpreter, so `python3` first, e.g.:

```ksh
~ $ vim hello.py

~ $ cat hello.py
print("Hello World!")

~ $ python3 hello.py 
Hello World!
```

The common way for creating Python source files is **using a proper shebang**. It can be either the full path of the `python3` binary or to a link for it (e.g. `/usr/bin/python3`), or, which is the best choice, just let the `env` program decide the location of the usable interpreter: 

```
#!/usr/bin/env python3

print("Hello World!")
```

To use this file, now you need to make it executable too: 

```ksh
~ $ chmod +x hello.py

~ $ ./hello.py
Hello World!
```

Of course, you can remove the `.py` extension too, as this point your "program" can be considered as an individual software. 

<div class="warn">
<p>When our learning journey involves conditionals and loops, it's better and cleaner to start using this "normal" way, using regular Python3 source files and invoking them directly in a built-in terminal, e.g. in <b>Visual Studio Code</b>.</p>
</div>

### 3.4 Comments in our Source Code

Python simply uses the same `#` for commenting as any Unix shell does. 

Python doesn't have block comments, but there are many occasions out there when multi-line strings, that are not assigned to any variables, are considered as block comments. It's ugly and it allocates memory already, but it might work as block comment sometimes. Try to avoid doing it. E.g.:

```
# This is a proper comment
print("Hello World!")

"""
This is an unassigned multi-line string,
which is not so nice thing to use as block comment.
Don't do this!
"""
```

For now, this `print()` was the only function we used. 

<div class="warn">
<p>To get a full list and details about all the built-in functions in Python3, you can browse the official documentation's <a href="https://docs.python.org/3/library/functions.html">Built-in Functions</a> page.</p>
</div>

<a name="4"></a>
## 4. Common Data Types

![alt_text](python_-_data_types.svg)

There are several interpretations how to differentiate, compare and sort Python3's data types, but here is one of them. The official and, most importantly, [updated documentation](https://docs.python.org/3/tutorial/datastructures.html#) on it might give more detailed explanation and further good ideas and examples. 

### 4.1 Strings

Python doesn't distinguish when using single and double quote pairs, so `"Hello World!"` is processed the same as `'Hello World!'`, and it's true even when applying the multi-line string. You only need to be sure you closed with the same type of quote as you opened it.

#### 4.1.1 Combining strings

Using it again in REPL: 

```ksh
>>> 'pass' + 'word'
'password'
```

#### 4.1.2 Multiplying strings

It's a nice feature by Python: 

```ksh
>>> 'Ha' * 4
'HaHaHaHa'
```

#### 4.1.3 Methods of strings

As everything in Python, even a string is an **object**, and it has a **state** and **methods**. The `find()` function is one of these methods, which gives the index number of the pattern found. If that's not found, it gives a negative number: 

```ksh
>>> 'parakeet'.find('r')
2

>>> 'parakeet'.find('s')
-1
```

A similar, simple method is the function `lower()` to generate the lowercase version of all the possible characters of the string: 

```ksh
>>> 'PaRaKeEt'.lower()
'parakeet'
```

#### 4.1.4 Control characters

Special, such as **line ending** characters can be used freely, e.g.:

```ksh
>>> print('Hello,\nWorld!')
Hello,
World!
```

...or the nice **tabulator** delimitered one (using multiple of them in a source file this time): 

```
#!/usr/bin/env python3

print('Name\t\tPosition')
print('Jakab Gipsz\tDirector')
print('John Doe\tOperator')
```

```ksh
$ hello
Name        Position
Jakab Gipsz Director
John Doe    Operator
```

As in any shell scripts, you can, and sometimes you **need to** use **escape** (`\`) characters, e.g.:

```ksh
>>> print('Your\'s is better')
Your's is better
```

### 4.2 Numbers (int and float)

#### 4.2.1 Math operators

When using basic math operations, notice that: 

- it's OK to use or leave white spaces around the operator
- as always, integers and float numbers can be negative or positive numbers
- adding decimal places adds the same for the result too

```ksh
>>> 2+2
4

>>> 2 + 2
4

>>> 2 * -5
-10

>>> 2.3 + 2.7
5.0

>>> 2.35 + 2.65
5.0
```

Summary of **math operators**: 

- **addition**: `+`
- **subtraction**: `-`
- **multiplication**: `*`
- **division**: `/`
- **floor division**: `//` (to get the whole number of the division)
- **modulus**: `%` (to get the remainder of the division)
- **exponent** (power) operation: `**`

Further examples for the above list: 

```ksh
>>> 10 - 3
7

>>> 4 * 7
28

>>> 9 / 2
4.5

>>> 9 // 2
4

>>> 9 % 2
1

>>> 2 ** 3
8
```

#### 4.2.2 Converting Between String and Number Data Types

There are the following **basic function**s that are related to this conversion: 

- `str()`: converting the given value to a String
- `int()`: converting the given value to an Integer
- `int()`: doing the same to a Float
- `type()`: querying for the data type of the given value

Examples for these functions: 

```ksh
>>> type(3)
<class 'int'>

>>> type("parakeet")
<class 'str'>

>>> int
<class 'int'>

>>> int("1")
1

>>> int(1.0)
1

>>> float("1.1")
1.1

>>> float(1)
1.0

>>> str(1.1)
'1.1'
```

After the conversion, the new data can be used accordingly, e.g.:

```ksh
>>> 1.1 * 3
3.3000000000000003

>>> str(1.1) * 3
'1.11.11.1'
```

### 4.3 Booleans and `None`

#### 4.3.1 Booleans

In Python, the Boolean data type is `bool` which can have the values `True` or `False`: 

```ksh
>>> True
True

>>> False
False

>>> type(True)
<class 'bool'>
```

The `bool()` function simply decides whether the value to be converted is anything but not empty (`True`) or empty (`False`): 

```ksh
>>> bool()
False

>>> bool("")
False

>>> bool([])
False

>>> bool({})
False

>>> bool("anything")
True

>>> bool([1])
True
```

#### 4.3.1 The `NoneType`

The Python's `None` is the same as `null` or `nil` in other languages, it's the "complete" nothingness. It has the unique type `NoneType`. It's so "nothing", not even the REPL displays it as a result: 

```ksh
>>> type(None)
<class 'NoneType'>

>>> None
```

### 4.4 Variables and Constants

Variables are **placeholders** to any values, other functions, for comparisons etc. As everything in Python, they have their own state and they might have their own methods. The procedure when a variable gets its first or new value assigned is called assignment. Either the programmer in the code or other functions are allowed to perform these assignments and re-assignments. 

#### 4.4.1 Assignments

You need to use the **assignment** operator (`=`) to assign a value for your variable: 

```ksh
>>> myVar = "Hello World!"

>>> myVar
'Hello World!'

>>> print(myVar)
Hello World!
```

**Reassigning** is anytime possible, e.g.:

```ksh
>>> myVar = "Hello World!"

>>> print(myVar)
Hello World!

>>> myVar = 1

>>> print(myVar)
1
```

Assigning a **variable to another** is necessary sometimes, and it's effective at the **moment of the assignment**, no matter the referenced variable changed later, e.g.:

```ksh
>>> myVar = 123

>>> myVar
123

>>> myInt = myVar

>>> myVar = "Hello"

>>> myInt
123
```

As you can see, it doesn't matter that the referenced `myVar` changed from `123` to `"Hello"` during the program: the final value of `myInt` still remains the same as it had at the moment of assignment. 

#### 4.4.2 Mixing strings and variables for the `print()` function

Python supports inserting variables both by separating them from the string parts with the `,` character and injecting them with `{}` characters. E.g., these two solutions have the same results: 

```
[...]
    print(name, 'is', age, 'years old.')
[...]
    print(f'{name} is {age} years old.')
[...]
```

#### 4.4.3 Extra Operations with Variables

The `+=` puts additional value to existing one, which is the same as combining the regular assignment (`=`) with the combining/addition operator (`+`): 

```ksh
>>> myVar = "Hello World!"

>>> print(myVar)
Hello World!

>>> myVar += " Bye!"

>>> print(myVar)
Hello World! Bye!

>>> myVar = myVar + " Bye!"

>>> print(myVar)
Hello World! Bye! Bye!

>>> myVar = 1

>>> print(myVar)
1

>>> myVar += 1

>>> print(myVar)
2
```

For numbers, as you may imagine by now, the same logic works with other combined math operators, e.g.:

```ksh
>>> myVar = 123

>>> myVar *= 123

>>> myVar
15129

>>> myVar -= 123

>>> myVar
15006

>>> myVar /= 123

>>> myVar
122.0
```

#### 4.4.4 Constants

A **constant** acts the same as the variable but its assigned value **cannot be changed**. That must be permanent in the memory during the run of the program. It was never obvious throughout the history of programming languages whether constants are surely needed or not, that's why some languages simply don't support it, or at least not easily, while others do. 

Some of the benefits of using constants anyway: 

- They protect the originally assigned value not to be changed
- They clearly communicate to the programmer or others that the value is permanent, which improves readability as it's a common practice to name constants with full capital letters (e.g. `PI`)
- They may reduce debugging needs and lowers the risk of errors

Python doesn't offer using constants by default, only variables, and it doesn't have a dedicated syntax for defining constants. It's only a common agreement in the Python community that by using capital letters only, you're communicating that the current name is intended to be treated as a constant. And nothing more, so they are not "true" constants, they're not protected from overwriting/re-assignment. 

Python, however, has several [built-in constants](https://docs.python.org/3/library/constants.html), such as the the above mentioned `False`, `True` or an interesting "literal", the `Ellipsis`. 

In practice, it's usually advised to simply create a separate file as Python **module**, then `import` it in the beginning of the main program like in the following example: 

- to a `const.py` file under the same directory: 

```
# declare constants 
PI = 3.14
```

- very simple example of contents of the main program: 

```
#!/usr/bin/env python3

import const

print("The value of Pi is", const.PI)
```

- and the results: 

```ksh
$ hello
The value of Pi is 3.14
```

Another helpful strategy to handle your constants is to define them already on OS/shell level, as environment variables, importing the `os` module, e.g., using a standard environment variable, coming from the shell:

```ksh
>>> import os

>>> os.getenv('USER')
'gjakab'
```

### 4.5 Lists

A **list** is the most common sequenced type in Python. It has multiple elements, enclosed within `[]` characters and separated by `,` characters. 

These are basically **arrays**, as known by other languages, but in Python, it's not enforced to have only identical type of elements in this data type, that's why it's referenced mostly as list. 

#### 4.5.1 Basic Usage of Lists

Example for creating a simple list and referencing to it as a whole:

```ksh
>>> myList = [1, 2, 3, 4, 5]

>>> myList
[1, 2, 3, 4, 5]
```

It becomes interesting when we work with elements only of the array. The elements are indexed, so they start from `0`, so, based on the above declaration, this `myList` with 5 elements has the last indexed element as `4`. The `len` function helps us to inspect the lenght. Also, it's possible to reference to an element by a negative index number: 

```ksh
>>> myList[0]
1

>>> len(myList)
5

>>> myList[4]
5

>>> myList[-1]
5

>>> myList[-3]
3
```

#### 4.5.2 Using Slice in a List

Slices are used to step through items in a list. E.g.:

```ksh
>>> myList[0:3]
[1, 2, 3]

>>> myList[2:]
[3, 4, 5]

>>> myList[:3]
[1, 2, 3]

>>> myList[0:-1:2]
[1, 3]

>>> myList[0::2]
[1, 3, 5]
```

#### 4.5.3 Re-assigning Value of an Element in a List

It's very simple and flexible; it works the same as it worked for variables: 

```ksh
>>> myList[0] = "a"

>>> myList
['a', 2, 3, 4, 5]
```

It can be done even by referencing slices: 

```ksh
>>> myList[1:3] = ["b", "c"]

>>> myList
['a', 'b', 'c', 4, 5, 6, 7, 8, 9, 10]
```

If you assign more elements than referenced by the slice, then you will just get a longer list, with more element(s).

#### 4.5.4 Appending and Reducing Elements in a List

The `append()` method is used for adding one more element: 

```ksh
>>> myList.append(6)

>>> myList.append(7)

>>> myList
['a', 2, 3, 4, 5, 6, 7]
```

Or you can use the **concatenation** too for adding further elements: 

```ksh
>>> myList += [8, 9, 10]

>>> myList
['a', 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

If you want to place your new element(s) **somewhere else than the end** of the list, e.g. to the beginning, you can use the same logic, but a bit more trickier way: 

```ksh
>>> myList
[2, 3, 4, 5]

>>> myList[:0] += [1]

>>> myList
[1, 2, 3, 4, 5]
```

The `pop()` method is used for removing elements from the end of the list by default, while it also **displays** the removed value: 

```ksh
>>> myList
[1, 2, 3, 4, 5]

>>> myList.pop()
5

>>> myList
[1, 2, 3, 4]
```

The `pop()` also can be used for removing any other indexed element from the list (as long as it's not out of range), e.g.:

```ksh
>>> myList
[1, 2, 3, 4]

>>> myList.pop(0)
1

>>> myList
[2, 3, 4]
```

The `remove()` method is used for removing elements by their values. If that cannot be found, then we get a `ValueError`: 

```ksh
>>> myList
[1, 2, 3, 4, 5]

>>> myList.remove(3)

>>> myList
[1, 2, 4, 5]

>>> myList.remove(6)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: list.remove(x): x not in list
```

Reducing the list of elements is also possible e.g. by assigning *empty* list to a slice of the original list, e.g.:

```ksh
>>> myList
['a', 'b', 'c', 4, 5, 6, 7, 8, 9, 10]

>>> myList[3:] = []

>>> myList
['a', 'b', 'c']
```

#### 4.5.5 Advanced list comprehensions

The [official documentations describes](https://docs.python.org/3/tutorial/datastructures.html?highlight=data%20structures#list-comprehensions) how easy to fill an empty list object not only with a separate `for` loop, but also by an included logic, within the list. That's called as **list comprehension**. 

The general syntax for it is:

```
[item for item in my_list]
```

Let's check them both with the official example: 

- Creating a list of squares, the separated way: 

```
squares = []

for x in range(10):
    squares.append(x**2)

print(squares)
```
```ksh
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

- And the "included" way: 

```
squares = [x**2 for x in range(10)]

print(squares)
```

#### 4.5.6 Sorting elements of lists

The built-in `sorted()` function can be used both for sorting list elements alphabetically and for numeric values: 

```
fruits = ['nut', 'apple', 'plum']
id = [3, 8, 1]

print(sorted(fruits))
print(sorted(id))
print(sorted(id, reverse=True))
```
```ksh
['apple', 'nut', 'plum']
[1, 3, 8]
[8, 3, 1]
```

### 4.6 Tuples

Tuple is an **immutable sequence type**. It is assigned similarily as a list, but it doesn't have any methods for modifying its elements.

They can be used to declare another tuple or its elements can be instantly assigned to multiple variables which is called **unpacking the tuple**: 

```ksh
>>> point = (2.0, 3.0)

>>> point
(2.0, 3.0)

>>> point_3d = point + (4.0,)

>>> point_3d
(2.0, 3.0, 4.0)

>>> point
(2.0, 3.0)

>>> x, y, z = point_3d

>>> x
2.0

>>> y
3.0

>>> z
4.0
```

Most commonly use of tuples is rather used as a format requirement only. E.g. like here, where exactly two any words are required in the specified position: 

```ksh
>>> print ("My name is: %s %s" % ("Jakab", "Gipsz"))
My name is: Jakab Gipsz

>>> print ("My first name, last name and weight are: %s %s, %f kg" % ("Jakab", "Gipsz", 77))
My first name, last name and weight are: Jakab Gipsz, 77.000000 kg
```

### 4.7 Sets

Python also includes a data type for sets. A set is an **unordered collection** with no duplicate elements. Basic uses include membership testing and eliminating duplicate entries. Set objects also support mathematical operations like union, intersection, difference, and symmetric difference.

The `{}` curly braces or the `set()` function can be used to create sets. 

Notice that the duplicated elements are simply ignored during creation: 

```ksh
>>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}

>>> basket
{'orange', 'banana', 'pear', 'apple'}
```

Otherwise, they can be used as lists. 

### 4.8 Dictionaries (dicts)

It's another sequencable type, but it is used for assigning **key and value pairs**, where the keys must be unique. (It is basically the same as the standard JSON format.)

What differs between Python2 and 3, is that Python3 keeps the original order of the key/value pairs. 

#### 4.8.1 Basic Usage of Dictionaries

Also showing the dangerous `del` function how it can be used relatively safely and how to use the previously already introduced `pop()` method for the same: 

```ksh
>>> ages = { 'Michael': 43, 'George': 40, 'John': 37 }

>>> ages
{'Michael': 43, 'George': 40, 'John': 37}

>>> ages['George']
40

>>> ages['Kevin']
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'Kevin'

>>> ages['Kevin'] = 34

>>> ages
{'Michael': 43, 'George': 40, 'John': 37, 'Kevin': 34}

>>> del ages['Kevin']

>>> ages
{'Michael': 43, 'George': 40, 'John': 37}

>>> ages.pop('John')
37

>>> ages
{'Michael': 43, 'George': 40}
```

#### 4.8.2 Advanced Queries on Elements of a Dictionary

Both the `keys()` method and the general `list` function can be used for querying only either the keys or the values: 

```ksh
>>> ages.keys()
dict_keys(['Michael', 'George'])

>>> list(ages.keys())
['Michael', 'George']

>>> ages.values()
dict_values([43, 40])

>>> list(ages.values())
[43, 40]
```

#### 4.8.3 Using the `dict` Function

A basic usage of it: 

```ksh
>>> weights = dict(Michael=77, George=78, John=79)

>>> weights
{'Michael': 77, 'George': 78, 'John': 79}
```

An advanced version of the same (for storing another list of data), which can be more easily iterated in real world programs: 

```ksh
>>> colors = dict([('Michael', 'blue'), ('George', 'green'), ('John', 'brown')])

>>> colors
{'Michael': 'blue', 'George': 'green', 'John': 'brown'}
```

Lists are mutable because you can add or remove items from them. Therefore you cannot use a list (e.g. `[1, 2, 3]`) as a dictionary key.


<a name="5"></a>
## 5. Control Flow

![alt_text](python_-_control_flows.svg)

When only some actions happen after each other **in a sequence, in a batch**, no matter what, then we can call it a **sequential** program. It's still can be useful sometimes if this batch must follow a strict order (*read this file --> print that --> run this other program --> finish*). 

Usually, we have at least one value that needs to be **evaluated, compared** with something or used as an input list for some **iterations**. Let's see what are the basics for these.

### 5.1 Comparison Operators

Python has the following, quite **standard comparison operators**: 

- `==` - equal
- `!=` - not equal
- `>` - greater than
- `<` - less than
- `>=` - greater than or equal to
- `<=` - less than or equal to

Some examples, demonstrated still in REPL (note that Python doesn't have the `===` as JavaScript does, so it always does the **conversion** too between `int` and `float` if necessary): 

```ksh
>>> 1 < 2
True

>>> 1 > 2
False

>>> 1 == 2
False

>>> 1 == 1.000
True

>>> 1 === 1.000
  File "<stdin>", line 1
    1 === 1.000
        ^
SyntaxError: invalid syntax

>>> 1 != 2
True

>>> "Hello" == "hello"
False

>>> "Hello" == "Hello"
True

>>> "a" < "b"
True

>>> 'abc' < 'b'
True
```

#### 5.1.1 The `in` and `not in`

They can inspect a list whether the searched value is in the list or not, e.g.:

```ksh
>>> 2 in [1, 2, 3]
True

>>> 2 not in [1, 2, 3]
False

>>> 4 not in [1, 2, 3]
True
```

### 5.2 The `if` Conditional

The `if` statement returns `True` if the evaluated equasion is true, and `False` if not. 

This is when we first need to deal with Python's strict syntax, the **indentation requirement**. When working or just experimenting with REPL, entering after the `:` character the user gets a new line, starting with `...`, where you need to use the required four spaces long indentation as associated code block. If you hit one more enter, REPL knows you finished with specifying the code block. E.g.:

```ksh
>>> if 1 == 1:
...     print('This is true.')
... 
This is true.
```

<div class="warn">
<p>From now on, only the <i>true</i> Python3 code will be shown, not the REPL output, for the sake of simplicity and readibility. Also, the shebang line will be skipped from the top of the code.</p>
</div>

So, the same in a Python script: 

```
#!/usr/bin/env python3

if 1 == 1:
    print('This is true.')
```
```ksh
$ ./test.py
This is true.
```

#### 5.2.1 Involving the `else` Branch

This is probably the most simple `if`, `else` statement for demonstrating it. In this case, it prints `This was false` as the evaluated equasion is an obvious `False`: 

```
if False:
    print('This was true.')
else:
    print('This was false')
```

#### 5.2.2 Further sub-evaluations with `elif`

The following program executes the block that was evaluated as `True` or executes `else` if none of them was such. In this case, the given string had 6 characters, so it prints out `This name has 6 characters.`: 

```
name = 'George'

if len(name) >= 7:
    print('The name is long.')
elif len(name) == 6:
    print('This name has 6 characters.')
elif len(name) >= 5:
    print('This name has 5 or more characters.')
else:
    print('The name is short.')
```

### 5.3 The `match` and `case` keywords

The Python3 version `3.10` intruduced this solution to simplify overcomplicated set of `if`/`elif`/`else` code blocks. The `case` can evaluate equasions but not comparisons, so this is a bit dumb demonstration of it but works:

```
name = 'George'

match len(name):
    case 6:
        print('This name has 6 characters.')
    case 5:
        print('This name has 5 characters.')
    case _:
        print('The name is either too long or too short.')
```

Notice it has a **default** case, working as a final `else` statement.

### 5.4 The `for` loop

The `for` loop is typically useful when you want to loop through on a list, on every items of it.

Its template syntax is `for TEMP_VAR in SEQUENCE`, e.g.:

```
for color in 'red', 'green', 'blue':
    print(color)

print('The last element was', color)
```
```ksh
red
green
blue
The last element was blue
```

Of course, any further loops or conditionals can be nested inside the loop. In this example, we print out the value of the temporary, index variable only if that matches the string "green" (also involving a variable for the list): 

```
colors = ['red', 'green', 'blue']

for color in colors:
    if color == 'green':
        print(color)
```
```ksh
green
```

Looping through **tuples** works the same: 

```
point = (2.1, 3.2, 7.5)

for value in point:
    print(value)
```

Tuples within a list, and iterating through the pairs: 

```
list_of_points = [(1, 2), (3, 4), (4, 5)]

for x, y in list_of_points:
    print(f'x: {x}, y: {y}')
```
```ksh
x: 1, y: 2
x: 3, y: 4
x: 4, y: 5
```

And finally, looping through a **dictionary** can be a bit more interesting. 

It can be done either like this, with `keys()` function: 

```
ages = { 'Michael': 43, 'George': 40, 'John': 37 }

for key in ages.keys():
    print(key, 'is', ages[key], 'years old.')
```

...or like this, using the `items()` function:

```
ages = { 'Michael': 43, 'George': 40, 'John': 37 }

for name, age in ages.items():
    print(name, 'is', age, 'years old.')
```

The result in both cases: 

```ksh
Michael is 43 years old.
George is 40 years old.
John is 37 years old.
```

### 5.5 The `while` loop

The `while` loop executes its set of statements as long as the given condition is `True`. It means, it's typically useful if we're not sure the number of iterations, the number of elements looped through. 

In this simple example, we iterate until the value of the variable `count` is less than `5`: 

```
count = 1

while count < 5:
    print('Hello', count)
    count += 1
```
```ksh
Hello 1
Hello 2
Hello 3
Hello 4
```

It's important to make sure somehow the evaluated equasion changes, in this case the indexing number is always increased by one more inside the loop. Otherwise, you will get an **infinite loop**. 

The easiest way to create an infinite `while` loop is to specify the evaluation as `while True`. It can be useful e.g. when monitoring system resources. 

Of course, any further loops or conditionals can be nested inside the loop. In this example, we print out every odd numbers between 1 and 10: 

```
count = 1

while count < 10:
    if count % 2 != 0:
        print(count, 'is an odd number.')
    count += 1
```
```ksh
1 is an odd number.
3 is an odd number.
5 is an odd number.
7 is an odd number.
9 is an odd number.
```

When using `break` inside the `if` statement's block, it stops at the first hit (the first odd number), then exists the program safely: 

```
count = 1

while count < 10:
    if count % 2 != 0:
        print(count, 'is an odd number.')
        break
    count += 1
```

The opposite of this `break` is `continue`, which forces remaining in that block in this case, so it again creates an infinite loop. Another solution for the same result (printing the lines 1, 3, 5, 7, 9), but using `continue` inside `if`'s block: 

```
count = 1

while count < 10:
    if count % 2 == 0:
        count += 1
        continue
    print(count, 'is an odd number.')
    count += 1
```

### 5.6 Logic operations

These are **compound comparisons** which can be used similarily as we did above by conditionals but these make the code significantly shorter. A very simple example for using such operation is the...

#### 5.6.1 `not`

```
name = ""

print(not name)
```
```ksh
True
```

As the `name` variable was empty, it's "nothing", so it contains a `False` value. Therefore, the `not` operator gives a `True` answer evaluating it.

Using it in a more reasonable evaluation: 

```
name = ""

if not name:
    print('No name given.')
```
```ksh
No name given.
```

A similarily simple example, but giving "something" for `not`:

```
name = "Kevin"

if not name:
    print('No name given.')
else:
    print('Name given.')
```

#### 5.6.2 `or`

It evaluates two inputs, where if any of them are `True`, then the result is also `True`: 

```
first = ""
last = "Smith"

if first or last:
    print("The user has either a first or a last name.")
```

Using `or` in a very flexible situation, which works as **setting defaults**: 

```
last = ""

last_name = last or "Doe"
print(last_name)
```
```ksh
Doe
```

#### 5.6.3 `and`

It also evaluates two inputs, but its result is `True` only if both sides were `True`: 

```
first = "George"
last = ""

if first and last:
    print(f'Full name: {first} {last}')
elif first:
    print(f'First name: {first}')
elif last:
    print(f'Last name: {last}')
```
```ksh
First name: George
```

As soon as `and` evaluates anything as `False` from left to right, it gives a `False` result. 

So the first evaluation results printing the given string too, while the second one results printing the result of the evaluation only (yes, in this case, REPL would be better to demonstrate it): 

```
print('1st evaluation:', (1 == 1) and print("Something"))
print('2nd evaluation:', (1 == 2) and print("Something"))
```
```ksh
Something
1st evaluation: None
2nd evaluation: False
```

### 5.7 Reading User Input

Asking the user of our program for input during its run time is also closely related to the Control Flow, so this topic should be part of this chapter. 

The function `input()` is the primary solution for asking input for any CLI programs. E.g.:

```
name = input("What is your name? ")
birthdate = input("What is your birthdate? ")
age = int(input("How old are you? "))

print(f"{name} was born on {birthdate}.")
print(f"Half of your age is {age / 2}")
```
```ksh
What is your name? Jakab Gipsz
What is your birthdate? 1990-01-01
How old are you? 33
Jakab Gipsz was born on 1990-01-01.
Half of your age is 16.5
```


<a name="6"></a>
## 6. Functions

Functions are used for putting all the related instructions together into an **independently usable and invokable** code block. (There are probably better descriptions out there for function, but this is mine, so far.)

### 6.1 Function Basics

Defining functions uses the `def` keyword, followed by a custom name or it. This name can be anything which is not a reserved word, and it can start with a lower case letter or with `_`. The expected indentation within the function's code block should be the same as for a conditional or a loop. 

Defining and using an extremely simple function in Python3: 

```
def hello_world():
    print("Hello World!")

hello_world()
```
```ksh
Hello World!
```

As you can see, there's nothing inside `()` while normally our functions have something to be processed against. 

Here's an example where we use a **parameter**: 

```
def print_name(name):
    print(f"The name is {name}.")

print_name("George")
```
```ksh
The name is George.
```

This function, however, does not know what we actually want from it to **return** with. That is why we usually need to use the `return` statement inside our function, E.g.:

```
def add_two(num):
    return num + 2

print(add_two(3))
```
```ksh
5
```

### 6.2 Using Functions in Complex Scripts

In "real world" scripts, functions are defined in advance in the code, then invoked later, when and where needed. Let's see an example for this. 

#### 6.2.1 A BMI Calculator

The following example calculates your Body Mass Index (BMI), just like the official formula suggests calculating it: 

- if the values are in the metric system, `BMI = weight / (height ** 2)`
- if the values are in the US/UK ("imperial") one, `BMI = 703 * weight / (height ** 2)`

The set of technical theories behind this program: 

- The `gather_info()` function is defined first to get the values to be calculated with. The given values can be either in the metric system or in the US/UK ("imperial") one, which makes the program more smart, but the user needs to define it. This definition can start either with lower or upper case letters.
- The `calculate_bmi` function performs the calculation, based on the given values. The `metric` is the default one, so if that 3rd value is not specified calling this function, then that will be the effective one. Also, the function has a prepared multi-line string, a `docstring` for potential documentation, which is the standard way to prepare it.
- The final `while` loop is the one that asks for user input until processable values were given. If the value for `system` doesn't start either with `i` or `m`, it asks again. Otherwise, the processing continues, based on that and stops (thanks to `break`), when the final printout is done. 

The program and its example output for how it's been progressed: 

```
def gather_info():
    height = float(input("What is your height? (inches or meters) "))
    weight = float(input("What is your weight? (punds or kilograms) "))
    system = input("Are your measurements in metric or imperial units? ").lower().strip()
    return (height, weight, system)

def calculate_bmi(weight, height, system='metric'):
    """
    Return the Body Mass Index (BMI) for the
    given weight, height and measurement system.
    """
    if system == 'metric':
        bmi = (weight / (height ** 2))
    else:
        bmi = 703 * (weight / (height ** 2))
    return bmi

while True:
    height, weight, system = gather_info()
    if system.startswith('i'):
        bmi = calculate_bmi(weight, system=system, height=height)
        print(f"Your BMI is {bmi}")
        break
    elif system.startswith('m'):
        bmi = calculate_bmi(weight, height)
        print(f"Your BMI is {bmi}")
        break
    else:
        print("Error: Unknown measurement system. Please use imperial or metric.")
```
```ksh
What is your height? (inches or meters) 1.73
What is your weight? (punds or kilograms) 77
Are your measurements in metric or imperial units? metric
Your BMI is 25.727555214006482
```


<a name="7"></a>
## 7. Interacting with Files

Python has its own way for interacting with files, which makes it platform-independent. On the other hand, even opening an existing text file looks complicated first. 

To begin this interaction, we need to use the built-in function `open()`. According to the [documentation](https://docs.python.org/3/library/functions.html#open), it has the following syntax: 

```
open(file, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True, opener=None)
```

Where the `mode` can be one of the following: 
- `r`: open for reading (default)
- `w`: open for writing, truncating the file first
- `x`: open for exclusive creation, failing if the file already exists
- `a`: open for writing, appending to the end of file if it exists
- `b`: binary mode
- `t`: text mode (default)
- `+`: open for updating (reading and writing)

You can combine some of these flags. The default mode is `r`, but it's the same as (a synonym of) `rt`. Modes `w+` and `w+b` open and truncate the file. Modes `r+` and `r+b` open the file with no truncation.

### 7.1 Reading a Text File

First of all, let's create a new text file, called `names.txt`, under the same directory as our Python script is located: 
```
George
Rosalinda
Samantha
```

When we don't specify anything, we get a `_io.TextIOWrapper` object reading that file: 
```
names = open('names.txt', 'r')

print(names)
```
```ksh
<_io.TextIOWrapper name='names.txt' mode='r' encoding='UTF-8'>
```

This object has some methods, provided by the `io` [module](https://docs.python.org/3/library/io.html#id1) (which I don't understand yet why importing this module is not needed). The obvious one in our case is `read()`: 

```
names = open('names.txt', 'r')

print(names.read())
```
```ksh
George
Rosalinda
Samantha
```

As files are **data streams**, and once we entered that stream, the "cursor position" ends up at its final character, so attempting to `read()` it again gives us an empty line only: 

```
[...]
print("Attempting to read it again:")
print(names.read())
```
```ksh
Attempting to read it again:

```

In case it's needed, and in order to go back to any cursor position, you need to use the `seek()` method: 

```
[...]
names.seek(0)
print("Attempting to read it again:")
print(names.read())
```
```ksh
Attempting to read it again:
George
Rosalinda
Samantha
```

When you finished reading the stream of the file, it's highly recommended to close it with the `close()` method: 

```
[...]
names.close()
```

### 7.2 Looping Through File's Contents

This is a common way to go through on elements of a file (with replacing the new line characters by an empty string): 

```
names = open('names.txt', 'r')

names.seek(0)
for line in names:
    print(line, end="")

names.close()
```
```ksh
George
Rosalinda
Samantha
```

A bit more advanced mode is simply using the `with` statement along with `readlines()` function for the same.  
An example for them, also using `argparse` library for program argument parsing (see details about it later), and the `lower()` function to convert the given parameter to have lowercase letters only: 

```
import argparse

parser = argparse.ArgumentParser(description='Search for words including partial word')
parser.add_argument('snippet', help='partial (or complete) string to search for in words')

arts = parser.parse_args()
snippet = arts.snippet.lower()

with open('/usr/share/dict/words') as f:
    words = f.readlines()

matches = []

for word in words:
    if snippet in word.lower():
        matches.append(word)

print(matches)
```
```ksh
$ ./test.py
usage: test.py [-h] snippet
test.py: error: the following arguments are required: snippet

$ ./test.py David
['David\n', 'davidia\n', 'davidias\n', 'Davidic\n', "David's\n", 'Davidson\n', "Davidson's\n", 'Davidsonville\n', "Davidsonville's\n", 'Davidsville\n', "Davidsville's\n", 'McDavid\n', "McDavid's\n"]
```

The same, with a shorter way of looping through the above `words` object with advanced list comprehensions, replacing the whole, separate `for` block: 

```
[...]
matches = [word for word in words if snippet in word.lower()]
[...]
```

And finally, the same with removing the unnecessary whitespace/line ending characters: 

```
[...]
matches = [word.strip() for word in words if snippet in word.lower()]
[...]
```
```ksh
$ ./test.py David
['David', 'davidia', 'davidias', 'Davidic', "David's", 'Davidson', "Davidson's", 'Davidsonville', "Davidsonville's", 'Davidsville', "Davidsville's", 'McDavid', "McDavid's"]
```

Here, we still kept the `print(matches)` function at the end, but it can be even simplified by not using the `matches` variable at all.

### 7.3 Writing into Files

As we saw the flags of `open()` function, the `w` and `a` are potential ones for opening a file for writing. Comparing the two, `w` completely **overwrites** the file, while `a` **appends** the new contents to the end after the existing one. 

#### 7.3.1 Writing contents to a new file

Still having our above example `txt` file with names, here is a simple way to open it, then open a new file for writing and copying the contents of the existing file to a new one. It doesn't give us any output if it was successful, apart from that the `names_new.txt` will be created: 

```
names = open('names.txt', 'r')
names_new = open('names_new.txt', 'w')

names_new.write(names.read())
```

If we want to continue with also reading the new file, it should be closed, then re-opened again with the reading flag: 

```
names = open('names.txt', 'r')
names_new = open('names_new.txt', 'w')

names_new.write(names.read())
names_new.close()

names_new = open(names_new.name, 'r')
print(names_new.read())

names_new.close()
```
```ksh
George
Rosalinda
Samantha
```

Otherwise, we would get an `io.UnsupportedOperation: not readable` error. 


#### 7.3.2 Opening files for both reading and writing

Luckily, Python offers opening the file both reading and writing too. In that case, we just need to make sure the `seek()` rewinds the cursor back to starting position once that's written: 

```
names = open('names.txt', 'r')
names_new = open('names_new.txt', 'w+')

names_new.write(names.read())

names_new.seek(0)
print(names_new.read())

names_new.close()
```
```ksh
George
Rosalinda
Samantha
```

#### 7.3.3 Appending contents to file with automatically closing it

A common way to work with file is using the `with` statement. 

Here, we not simply append (with `a` flag) to the end of the existing contents but we also seek through the file, add one more new line, then automatically close it: 

```
with open('names_new.txt', 'a') as f:
    f.write("\nOliver")
```

To make it more clear: 

```
f = open('names_new.txt', 'a')

with f:
    f.write("\nOliver")
```



<a name="8"></a>
## 8. Standard Library Packages

Python3 offers further useful functionalities out-of-the box. These are called **modules**, but usually also referred as **library packages**. These modules are members of the **Standard Library**, and they need to be **imported** in the beginning of the code with the `import` keyword.

The official [docs.python.org](https://docs.python.org/3/library/) page lists and describes all the packages that the standard installation of Python includes but not loaded by default. According to the official definition as of now:

>The library contains built-in modules (written in C) that provide access to system functionality such as file I/O that would otherwise be inaccessible to Python programmers, as well as modules written in Python that provide standardized solutions for many problems that occur in everyday programming. 

### 8.1 The `import` and `from` Keywords

For the sake of simplicity, let's import and use the `time` library (or package) as is, and then use its `localtime()` function: 

```
import time

now = time.localtime()
print(now)
```
```ksh
time.struct_time(tm_year=2023, tm_mon=3, tm_mday=7, tm_hour=15, tm_min=57, tm_sec=41, tm_wday=1, tm_yday=66, tm_isdst=0)
```

It returns with the `time.struct_time` type of object with all of its attributes, so we can use e.g. `print(now.tm_hour)` only, which returns with `15`. 

The above `import` imports the whole module, and if its functions are used multiple times, each case requires explicit `time.<function>` definitions (like above, with `time.localtime`). If we know in advance which functions will be used throughout our code, it's much cleaner to use `import` along with `from`, with listing these functions (separated by commas), e.g.:

```
from time import localtime

now = localtime()
print(now)
```

### 8.2 The `time` Module - Building a Timer Program

Still using the `time` module, and digging for more details in the [official documentation of it](https://docs.python.org/3/library/time.html), we can find that the `%X` directive stands for *"Locale’s appropriate time representation."*, it can be used for properly formatted time, in `<24hour:minutes:seconds>` format. Let's use it to create a timer: 

```
from time import localtime, mktime, strftime

start_time = localtime()
print(f"Timer started at {strftime('%X', start_time)}")

input("Press 'Enter' to stop timer ")

stop_time = localtime()
difference = mktime(stop_time) - mktime(start_time)

print(f"Timer stopped at {strftime('%X', stop_time)}")
print(f"Total time: {difference} seconds")
```
```ksh
Timer started at 17:23:28
Press 'Enter' to stop timer 
Timer stopped at 17:23:32
Total time: 4.0 seconds
```

See `localtime`, `mktime` and `strftime` functions in the [documentation](https://docs.python.org/3/library/time.html) for further details. 

### 8.3 The `os` Module - Working with Environment Variables

[Here](https://docs.python.org/3/library/os.html) is the official documentation of its latest version.

The obvious choice from this library package is the `os.environ` function, which returns with the environment variables, coming from the invoking shell. A simple example for using it: 

```
from os import environ

print(environ["USER"])
```
```ksh
gjakab
```

The `os.getenv` function might be a better choice sometimes because if offers defining a default value if the specified environment variable is undefined. E.g.:

```
from os import getenv

stage = getenv("STAGE", default="dev").upper()

output = f"We're running in {stage}"

if stage.startswith("PROD"):
    output = "DANGER!!! - " + output

print(output)
```
```ksh
$ STAGE=test ./test.py 
We're running in TEST

$ STAGE=production ./test.py 
DANGER!!! - We're running in PRODUCTION

$ ./test.py 
We're running in DEV
```

### 8.4 The `subprocess` Module - Invoking OS Commands with Arguments

The [`subprocess` library](https://docs.python.org/3/library/subprocess.html) is mostly useful when we need to invoke standard Linux commands. E.g. this runs a regular `ls -l` command under the directory where our Python script is located:

```
import subprocess

proc = subprocess.run(["ls", "-l"])
```

It even invoked when it was simply assigned to the above `proc` variable. Some more, short possibilities to handle the variable: 

```
proc = subprocess.run(["ls", "-l"])

print(proc)
print(proc.returncode)
print(proc.args)
```
```ksh
$ ./test.py
[...]
CompletedProcess(args=['ls', '-l'], returncode=0)
0
['ls', '-l']
```

Handling the standard output and standard error parts of the invoked command requires using the `.PIPE` special value: 

```
import subprocess

proc = subprocess.run(
    ["ls", "-l"],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
)

```
```ksh
$ ./test.py 
b'total 12\n-rw-r--r-- 1 gjakab users   26 Apr 17 15:29 names.txt\n-rwxr-xr-x 1 gjakab users  164 Apr 19 16:59 test.py\ndrwxr-xr-x 2 gjakab users 4096 Mar  9 14:10 Videos\n'
```

It resulted printing the standard output only but in a byte format, so without rendering e.g. the `\n` characters for breaking the lines. The solution for this is using the `decode()` function for decoding bytes (by default): 

```
[...]
print(proc.stdout.decode())
```

Preparing a `subprocess.run` invocation for exception handling: 

```
proc = subprocess.run(['cat', 'nonexistingfile'], check=True)
```

As an alternative, we can also still use the old `subprocess.call()` function. 

### 8.5 The `random` Module - Generating "Test" Data

As its [official documentation](https://docs.python.org/3/library/random.html) introduces it, the module primarily implements pseudo-random number generators for various distributions, but it can be used for many other sorts of useful tasks concerning random data, when we combine it with any existing, even mass list of any data types. 

Its `random()` takes no arguments, so using it is very easy. Let's try it out and only that particular one first: 

```
from random import random

print(random())
```
```ksh
0.2459010625687873
```

It's also worth mentioning the module's `uniform()` function which returns the **next random floating point number in the given range** and also the `choice()` function, which **selects one item from the given choices**. Example for the latter: 

```
import random

print(random.choice(['red', 'green', 'blue', 'yellow']))
```
```ksh
$ ./test.py 
yellow

$ ./test.py 
blue
```

### 8.6 The `json` Module - The JSON Encoder and Decoder

Working with nowadays' favorite data format is not difficult in Python3 with [this module](https://docs.python.org/3/library/json.html). 

Let's see the module's `dump()` function how it works in the following example, where we generate either the predefined or the default `100` JSON files with random values, using the `/usr/share/dict/words` dictionary: 

```
#!/usr/bin/env python3.11

import json
import random
import os

count = int(os.getenv("FILE_COUNT") or 100)
words = [word.strip() for word in open('/usr/share/dict/words').readlines()]

for indentifier in range(count):
    amount = random.uniform(1.0, 1000)
    content = {
        'topic': random.choice(words),
        'value': "%.2f" % amount
    }
    with open(f'./new/receipt-{indentifier}.json', 'w') as f:
        json.dump(content, f)
```

Create the required `new` directory first, then run the script first without any parameters. After that, check the results, then run it with specifying the `FILE_COUNT` OS variable for the script ahead of invoking it: 


```ksh
$ mkdir new

$ ./test.py
$ ls -la new/ | tail -5
-rw-r--r-- 1 gjakab users   42 Apr 20 14:40 receipt-96.json
-rw-r--r-- 1 gjakab users   42 Apr 20 14:40 receipt-97.json
-rw-r--r-- 1 gjakab users   40 Apr 20 14:40 receipt-98.json
-rw-r--r-- 1 gjakab users   44 Apr 20 14:40 receipt-99.json
-rw-r--r-- 1 gjakab users   40 Apr 20 14:40 receipt-9.json

$ cat new/receipt-99.json
{"topic": "copulatively", "value": "512.84"}$ 

$ ls -la new/receipt-* | wc -l
100

$ rm -f new/*

$ FILE_COUNT=10 ./test.py 

$ ls -la new/receipt-* | wc -l
10
```


### 8.7 The `shutil` Module - High-level File Operations

This [module](https://docs.python.org/3/library/shutil.html) offers a number of high-level operations on files and collections of files. 

In particular, functions are provided which support file copying and removal. A quite regular example for using it is its `move()` function which recursively moves a file or directory (`src`) to another location (`dst`), and returns with the destination. 

```
shutil.move(src, dst, copy_function=copy2)
```

Using it "in action" in details will be demonstrated in the next subchapter, along with another another module. 

### 8.8 The `glob` Module - Performing OS Globing Operations in Python Code

"**Globing**" is a user request, when only **a set of elements of a bigger group** is specified. On Linux CLI, this is a good example for that: 

```ksh
$ ls -1 new/receipt-[0-2].json
new/receipt-0.json
new/receipt-1.json
new/receipt-2.json
```

Python3's `glob` module is a Unix style pathname pattern expansion to achieve the same: 

```
import glob

print(glob.glob('new/receipt-[0-2].json'))
```
```ksh
$ ./test.py
['new/receipt-1.json', 'new/receipt-2.json', 'new/receipt-0.json']
```

Example for putting the previous 4 modules together by a script that:
- creates a `processed` directory if that doesn't exist (also introducint exception handling a little)
- deals with files only as defined with `glob()` function
- fetches the file names without their relative path with `split()` function
- reads the files and moves them as JSON content with `load()` function to the destination
- we even measure the amount of data processed

```
#!/usr/bin/env python3.11

import os
import glob
import json
import shutil

try:
    os.mkdir('./processed')
except OSError:
    print("'processed' directory already exists")

receipts = glob.glob('new/receipt-[0-9]*.json')
subtotal = 0.0

for path in receipts:
    with open(path) as f:
        content = json.load(f)
        subtotal += float(content['value'])
    name = path.split("/")[-1]
    destination = f"./processed/{name}"
    shutil.move(path, destination)
    print(f"moved '{path}' to '{destination}'")

print("Receipt subtotal: $%.2f" % subtotal)
```
```ksh
$ ./test.py 
'processed' directory already exists
moved 'new/receipt-3.json' to './processed/receipt-3.json'
moved 'new/receipt-6.json' to './processed/receipt-6.json'
moved 'new/receipt-5.json' to './processed/receipt-5.json'
moved 'new/receipt-1.json' to './processed/receipt-1.json'
moved 'new/receipt-2.json' to './processed/receipt-2.json'
moved 'new/receipt-8.json' to './processed/receipt-8.json'
moved 'new/receipt-9.json' to './processed/receipt-9.json'
moved 'new/receipt-7.json' to './processed/receipt-7.json'
moved 'new/receipt-4.json' to './processed/receipt-4.json'
moved 'new/receipt-0.json' to './processed/receipt-0.json'
Receipt subtotal: $4309.90

$ ./test.py 
'processed' directory already exists
Receipt subtotal: $0.00
```

This section can be simplified by directly using the desired path (`processed`) of our files for these moves: 

```
[...]
    # name = path.split("/")[-1]
    # destination = f"./processed/{name}"

    destination = path.replace('new', 'processed')
[...]
```


The module's `iglob()` function is the **iterator** version of `glob()` to store only one element in the memory at a time. 

### 8.9 The `re` Module - Regular Expression Operations

This [module](https://docs.python.org/3/library/re.html) provides regular expression matching operations similar to those found in **Perl**.

The three essential functions in this library: 
- `re.match()` checks for a match only at the beginning of the string
- `re.search()` checks for a match anywhere in the string (this is what Perl does by default)
- `re.fullmatch()` checks for entire string to be a match

E.g.:

```
re.match("c", "abcdef")         # No match
re.search("c", "abcdef")        # Match
re.fullmatch("p.*n", "python")  # Match
re.fullmatch("r.*n", "python")  # No match

re.search("^c", "abcdef")  # No match
re.search("^a", "abcdef")  # Match

re.match("X", "A\nB\nX", re.MULTILINE)    # No match
re.search("^X", "A\nB\nX", re.MULTILINE)  # Match
```

### 8.10 The `math` Module - Mathematical functions

This [module](https://docs.python.org/3/library/math.html) provides access to the mathematical functions defined by the C standard.

E.g.:

```
import math

print(sum([.1, .1, .1, .1, .1, .1, .1, .1, .1, .1]))        # 0.9999999999999999
print(math.fsum([.1, .1, .1, .1, .1, .1, .1, .1, .1, .1]))  # 1.0
```

For **rounding** float numbers to integer that's close to it, we can use either the `ceil()` or `floor()` functions: 
- `math.ceil(x)`: Return the ceiling of `x`, the **smallest integer greater than or equal to** `x`. If `x` is not a float, delegates to `x.__ceil__`, which should return an Integral value.
- `math.floor(x)`: Return the floor of `x`, the **largest integer less than or equal to** `x`. If `x` is not a float, delegates to `x.__floor__`, which should return an Integral value.

However, the built-in `round(<value>, <digits>)` function does the very simiar feature which is sometimes more straightforward. 

### 8.11 The `sys` Module - System-specific Parameters and Functions

This [module](https://docs.python.org/3/library/sys.html) provides access to some variables used or maintained **by the interpreter and to functions that interact strongly with the interpreter**. 

First, let's see the standard `sys.argv` function out of the module's wide range of functions. Of course, the function follows the POSIX standards, so if we query for the 0th argument of our program, that's always simply the program's name (if it's in the path, then the full path of it will be returned). The further IDs of the arguments are real arguments: 

```
import sys

print(f"The zeroth argument is the script's name: {sys.argv[0]}")
print(f"All the script arguments in a slice: {sys.argv[1:]}")
print(f"The first argument: {sys.argv[1]}")
```

It gives error for the 3rd `print` call if no arguments are specified for our script, but runs already fine if anything is provided: 

```ksh
$ ./test.py
The zeroth argument is the script's name: ./test.py
All the script arguments in a slice: []
Traceback (most recent call last):
  File "/home/[...]./test.py", line 7, in <module>
    print(f"The first argument: {sys.argv[1]}")
                                 ~~~~~~~~^^^
IndexError: list index out of range

$ ./test.py hello
The zeroth argument is the script's name: ./test.py
All the script arguments in a slice: ['hello']
The first argument: hello
```

It handles correctly already, if complex arguments are specified: 

```ksh
$ ./test.py hello szia "good night"
The zeroth argument is the script's name: ./test.py
All the script arguments in a slice: ['hello', 'szia', 'good night']
The first argument: hello
```

### 8.12 The `argparse` Module - Tool for Writing User-friendly Programs

The [module](https://docs.python.org/3/library/argparse.html) provides several additional features above the lower level `sys.argv`, such as: 

- types of arguments
- documentation for arguments
- optional or required arguments

Using this module, it's much easier to write **user-friendly command-line interfaces**. The program defines what arguments it requires, and `argparse` will figure out how to parse those out of `sys.argv`.

A simple example for invoking and starting using the library: 

```
import argparse

# build the parser
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='the file to read')

args = parser.parse_args()
print(args)
```
```ksh
$ ./test.py
usage: test.py [-h] filename
test.py: error: the following arguments are required: filename

$./test.py hello
Namespace(filename='hello')
```

It provides a complete, human readable `help` already if the argument is missing: 

```ksh
$ ./test.py --help
usage: test.py [-h] filename

positional arguments:
  filename    the file to read

options:
  -h, --help  show this help message and exit
```

Using the built-in `version` flag for **versioning** our program: 

```
parser.add_argument('--version', '-v', action='version', version='%(prog)s 1.0')
```

In the below example, we invoke the `readlines()` function to go through on the lines of the textfile. 

Our complete program so far and how it works: 

```
#!/usr/bin/env python3.11

import argparse

# build the parser
parser = argparse.ArgumentParser(description='Read a file in reverse')
parser.add_argument('filename', help='the file to read')
parser.add_argument('--limit', '-l', type=int, help='the number of lines to read')
parser.add_argument('--version', '-v', action='version', version='%(prog)s 1.0')

# parse the arguments
args = parser.parse_args()

# read the file, reverse the contents and print
with open(args.filename) as f:
    lines = f.readlines()
    lines.reverse()

    if args.limit:
        lines = lines[:args.limit]
    
    for line in lines:
        print(line.strip()[::-1])
```
```ksh
$ cat names.txt 
George
Rosalinda
Samantha

$ ./test.py names.txt
ahtnamaS
adnilasoR
egroeG

$ ./test.py names.txt -l 2
ahtnamaS
adnilasoR
```

### 8.13 The `tempfile` Module - Generate temporary files and directories

[This module](https://docs.python.org/3/library/tempfile.html) can be useful sometimes for testing or for storing some data temporarily as easy as possible as besides some lower-level functions, it also offers high-level interfaces for automatic cleanup. 

The official example implementation of it: 

```
import tempfile

# create a temporary file and write some data to it
fp = tempfile.TemporaryFile()
fp.write(b'Hello world!')

# read data from file
fp.seek(0)
fp.read()

# close the file, it will be removed
fp.close()

# create a temporary file using a context manager
with tempfile.TemporaryFile() as fp:
    fp.write(b'Hello world!')
    fp.seek(0)
    fp.read()
# file is now closed and removed

# create a temporary directory using the context manager
with tempfile.TemporaryDirectory() as tmpdirname:
    print('created temporary directory', tmpdirname)

# directory and contents have been removed
```

Creating a `NamedTemporaryFile` (which is not simply a file-like object as in case of `TemporaryFile` but true file) without deleting it at the end: 

```
import tempfile

with tempfile.NamedTemporaryFile(delete=False) as fp:
    fp.write(b'Hello world!')
    fp.seek(0)
    print(f"file {fp.name} created with contents:")
    print(fp.read())
```
```ksh
file /tmp/tmp4mhxgnqy created with contents:
b'Hello world!'
```


<a name="9"></a>
## 9. Handling Program Exceptions

A well written program is prepared for circumstances when not everything goes as expected. The most simple way for that is **preparing the possible parameters** of our CLI program in the first place. E.g., if a mandatory parameter is missing, then let's provide the user some help. 

Going a bit further, our program must handle the possible runtime errors and exceptions too, therefore, it's part of this kind of preparation. 

### 9.1 Handling Exceptions with `try [...] except`

Python is a modern programming language, therefore it supports the standard way, known in many other languages, to handle exceptions during runtime. 

This subchapter is about the usual practices around `try`, `except`, `else`, `finally` statements. The documentation about them is [here](https://docs.python.org/3/tutorial/errors.html#handling-exceptions).

**Generally**, and using all the possible options, the syntax looks like this:

```
try:
    # tasks to attempt
except:
    # letting the user know what error happened if any
else:
    # tasks to run if try block was fully successful
finally: 
    # tasks to run regardless anything above was successful or not
```

**In details**, the `try` statement works as follows (mostly, it's simply copied from the documentation out):

- First, the `try` clause (the statement(s) between the `try` and `except` keywords) is executed.
- If **no exception occurs**, the `except` clause is skipped and execution of the `try` statement is finished.
- If **an exception occurs** during execution of the `try` clause, the rest of the clause is **skipped**. Then, if its type matches the exception named after the `except` keyword, the `except` clause is executed, and then execution continues after the `try [...] except` block.
- If an exception occurs which **does not match the exception** named in the `except` clause, it is passed on to outer `try` statements; if no handler is found, it is an unhandled exception and execution stops with a message as shown above.
- The `try [...] except` statement has an optional `else` clause, which, when present, must follow all `except` clauses. It is useful for code that must be executed if the `try` clause does not raise an exception. 
- The use of the `else` clause is better than adding additional code to the `try` clause because it avoids accidentally catching an exception that wasn’t raised by the code being protected by the `try [...] except` statement.
- If a `finally` clause is present, then it will execute as the last task before the `try` statement completes. The `finally` clause runs **whether or not** the `try` statement produces an exception.

### 9.2 Exception Handling in Practice

Applying the grasp of these futures in a very simple script, which attempts to create a directory if that doesn't exist: 

```
import os

try:
    os.mkdir('./processed')
except OSError:
    print("'processed' directory already exists")
```

It's successful first, then it faces against the predefined exception: 

```ksh
$ ./test.py

$ ./test.py
'processed' directory already exists
```

Let's see another example by applying the features to our above, letter-replacing program to handle the error when the specified file to be processed cannot be found: 

```
[...]
try:
    f = open(args.filename)
    limit = args.limit
except FileNotFoundError as err:
    print(f"Error: {err}")
else:
    with f:
        lines = f.readlines()
        lines.reverse()

        if args.limit:
            lines = lines[:limit]
        
        for line in lines:
            print(line.strip()[::-1])
```
```ksh
 ./test.py names.txt -l 2
ahtnamaS
adnilasoR

 ./test.py namesss.txt
Error: [Errno 2] No such file or directory: 'namesss.txt'
```

Note that iterating the lines with the `with` statement was already initialized in the `try` block, so that part becomes simpler within the `else` block. 

If we want to handle anything else, not only the `FileNotFoundError` error, we can simplify the `except` block: 

```
[...]
except:
    print(f"An error occured.")
[...]
```

### 9.3 Exit Statuses

If a command completes successfully, its traditional exit status is `0`. If something went wrong, it can be usually `1`, but basically anything else is possible. 

We have the "power" to override these exit statuses. The function responsible for this is `sys.exit()`, its documentation can be found [here](https://docs.python.org/3/library/sys.html#sys.exit).

Improving our above program to give proper (non-zero) exit code, when an exception happened:

```
[...]
import argparse
import sys
[...]
except:
    print(f"An error occured.")
    sys.exit(1)
[...]
```
```ksh
$ ./test.py valami
An error occured.
Finally

$ echo $?
1
```


<a name="10"></a>
### 10. Classes

...

>A `class` is template for creating user-defined objects. Class definitions normally contain method definitions which operate on instances of the class.

...

### 10.1 Decorators

The [decorator](https://docs.python.org/3/glossary.html#term-decorator) is a function returning another function, using the `@wrapper` syntax. 

>The decorator syntax is merely syntactic sugar, the following two function definitions are semantically equivalent:

```
def f(arg):
    ...
f = staticmethod(f)

@staticmethod
def f(arg):
    ...
```

...


<a name="11"></a>
## 11. PIP and VirtualEnv

Python has its own ecosystem to access a wide variety of packages besides the factory-default Standard Library ones. There are hundreds of thousands of them, made by the huge Python developer community. 

### 11.1 The Base Packages

The Python3 version we installed always gets a respective `pip` executable too. In our case, it was `python3.11`, so we should be able to use `pip3.11`. 

Getting a simple list with it: 

```ksh
$ pip3.11 list
Package    Version
---------- -------
pip        23.1
setuptools 65.5.0
```

If your output still uses the legacy format instead of `columns`, you can set it up in `~/.config/pip/pip.conf`: 

```
[list]
format=columns
```

The two default packages that come with base installation: 
- `pip`: it is the `pip` itself that you manage, and it's just a console script
- `setuptools` is a tool that specifies how the packages are installed.

Besides them, `wheel` is a "built distribution" of Python code that can be installed by pip. The `wheel` type is the newest version and will be used moving forward. The `egg` is a "built distribution" of Python code that can be installed by pip; however, the `wheel` is the newest version.

### 11.2 Upgrading Packages

- **globally**, as `root`, e.g. `sudo /opt/Python3/bin/pip3 install --upgrade pip`  
  the result can be seen under the installation's `.../lib/python3.11/site-packages` directory
- **locally**, for your own user, e.g. `/opt/Python3/bin/pip3 install --upgrade pip`  
  the results are under your `~/.local/lib/python3.11/site-packages` directory

### 11.3 Installing Packages

As an example, installing a popular package (and its dependencies) for interacting with Azure Web Services. Let's do it only *locally*, for the current user only:

```ksh
$ pip3.11 list
Package    Version
---------- -------
pip        23.1.1
setuptools 65.5.0

$ pip3.11 install boto3
[...]
Successfully installed boto3-1.26.118 botocore-1.29.118 jmespath-1.0.1 python-dateutil-2.8.2 s3transfer-0.6.0 six-1.16.0 urllib3-1.26.15

$ pip3.11 list
Package         Version
--------------- --------
boto3           1.26.118
botocore        1.29.118
jmespath        1.0.1
pip             23.1.1
python-dateutil 2.8.2
s3transfer      0.6.0
setuptools      65.5.0
six             1.16.0
urllib3         1.26.15
```

### 11.4 Working with Requirements

With `pip`'s `freeze` parameter, you can get an output of the installed packages in **requirements format**. It is useful if you have a project that requires exact versions of a package. 

Traditionally, this list in requirements format is saved to `requirements.txt` file. Let's see an example for using it: 

```ksh
$ pip3.11 freeze
boto3==1.26.118
botocore==1.29.118
jmespath==1.0.1
python-dateutil==2.8.2
s3transfer==0.6.0
six==1.16.0
urllib3==1.26.15

$ pip3.11 freeze > requirements.txt

$ pip3.11 uninstall -y -r requirements.txt 
Found existing installation: boto3 1.26.118
[...]
  Successfully uninstalled urllib3-1.26.15
```

Installing is now simple with the `requirements.txt` file: 

```ksh
$ pip3.11 install -r requirements.txt 
[...]
Successfully installed [...]
```

### 11.5 VirtualEnv

Python offers a way to keep all your project related packages, the specific versions of them in a separated environment directory. It is called **VirtualEnv**.

In general, it performs two important actions: 
1. Creates a directory to the specified location where it generates all the subdirectories and symlinks to proper Python binaries and libraries
2. When activated, it modifies the user's `$PATH` `$PS1` environmental variables, so the VirtualEnv's ones will be effective

#### 11.5.1 Preparation

At first, let's just create a `venvs` directory, then also print `python3`'s help about how to use the VirtualEnv feature: 

```ksh
$ mkdir venvs

$ python3.11 -m venv --help
usage: venv [-h] [--system-site-packages] [--symlinks | --copies] [--clear] [--upgrade] [--without-pip] [--prompt PROMPT] [--upgrade-deps] ENV_DIR [ENV_DIR ...]
[...]
Once an environment has been created, you may wish to activate it, e.g. by sourcing an activate script in its bin directory.
```

#### 11.5.2 Creation

Creating a new VirtualEnv called `experiment` and checking its results: 

```ksh
$ python3.11 -m venv venvs/experiment

$ ls -l venvs/experiment/ | awk '{print $1,$9,$10,$11}'
total   
drwxr-xr-x bin  
drwxr-xr-x include  
drwxr-xr-x lib  
lrwxrwxrwx lib64 -> lib
-rw-r--r-- pyvenv.cfg  

$ ls -l venvs/experiment/bin | awk '{print $1,$9,$10,$11}'
total   
-rw-r--r-- activate  
-rw-r--r-- activate.csh  
-rw-r--r-- activate.fish  
-rw-r--r-- Activate.ps1  
-rwxr-xr-x pip  
-rwxr-xr-x pip3  
-rwxr-xr-x pip3.11  
lrwxrwxrwx python -> python3.11
lrwxrwxrwx python3 -> python3.11
lrwxrwxrwx python3.11 -> /opt/Python3/bin/python3.11
```

#### 11.5.3 Activation and Deactivation

The environment is deactivated by default, meaning, the `$PATH` and `$PS1` global variables are the original ones, but the program's prepared one can be activated by the generated `.../bin/activate` file. 

The **activation** is very simple: 

```ksh
$ source venvs/experiment/bin/activate

(experiment) $ which python
/home/gjakab/[...]/venvs/experiment/bin/python

(experiment) $ python --version
Python 3.11.3
```

When you're done, the **deactivation** is even more simple as activation defines an alias for it: 

```ksh
$ deactivate
```

#### 11.5.4 PIP in VirtualEnv

Of course, the new environment creates a factory default PIP environment too, so once the environment is activated, you can first upgrade the `pip` package, then you can install any other packages, e.g.:

```ksh
(experiment) $ pip list
Package    Version
---------- -------
pip        22.3.1
setuptools 65.5.0
[...]
[notice] A new release of pip available: 22.3.1 -> 23.1.1
[notice] To update, run: pip install --upgrade pip

(experiment) $ pip install --upgrade pip
Requirement already satisfied: pip in ./venvs/experiment/lib/python3.11/site-packages (22.3.1)
Collecting pip
  Using cached pip-23.1.1-py3-none-any.whl (2.1 MB)
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 22.3.1
    Uninstalling pip-22.3.1:
      Successfully uninstalled pip-22.3.1
Successfully installed pip-23.1.1

(experiment) $ pip list
Package    Version
---------- -------
pip        23.1.1
setuptools 65.5.0

(experiment) $ pip install requests
Collecting requests
  Downloading requests-2.28.2-py3-none-any.whl (62 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 62.8/62.8 kB 7.0 MB/s eta 0:00:00
Collecting charset-normalizer<4,>=2 (from requests)
  Downloading charset_normalizer-3.1.0-cp311-cp311-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (197 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 197.3/197.3 kB 17.5 MB/s eta 0:00:00
Collecting idna<4,>=2.5 (from requests)
  Downloading idna-3.4-py3-none-any.whl (61 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.5/61.5 kB 11.2 MB/s eta 0:00:00
Collecting urllib3<1.27,>=1.21.1 (from requests)
  Using cached urllib3-1.26.15-py2.py3-none-any.whl (140 kB)
Collecting certifi>=2017.4.17 (from requests)
  Downloading certifi-2022.12.7-py3-none-any.whl (155 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 155.3/155.3 kB 19.3 MB/s eta 0:00:00
Installing collected packages: urllib3, idna, charset-normalizer, certifi, requests
Successfully installed certifi-2022.12.7 charset-normalizer-3.1.0 idna-3.4 requests-2.28.2 urllib3-1.26.15
```

Also, you can install your requirements with `pip install -r requirements.txt` if you've prepared for it. Then, your environment is **ready**!

The resulting `site_packages` directories/files will be found under the VirtualEnv's own `.../lib` directory. 

### 11.6 Using 3rd Party Packages under VirtualEnv

As already shown above, we installed the `requests` 3rd party package under our VirtualEnv. 

Here is an example using it (with the popular, random dog picture generator API): 

```
#!/usr/bin/env python3.11

import os
import sys
import requests

from argparse import ArgumentParser

parser = ArgumentParser(description='provides a random dog picture as defined by the "BREED" OS variable')

args = parser.parse_args()

breed = os.getenv("BREED")

if not breed:
    print("Error: no 'BREED' provided")
    sys.exit(1)

url = f"https://dog.ceo/api/breed/{breed}/images/random"

res = requests.get(url)

if res.status_code != 200:
    print(f"Error talking to dog picture provider: {res.status_code}")
    sys.exit(1)

print(res.json())
```
```ksh
(experiment) $ ./test.py
Error: no 'BREED' provided

(experiment) $ ./test.py --help
usage: test.py [-h]
[...]
provides a random dog picture as defined by the "BREED" OS variable
[...]
options:
  -h, --help  show this help message and exit

(experiment) $ BREED=hound ./test.py
{'message': 'https://images.dog.ceo/breeds/hound-ibizan/n02091244_5400.jpg', 'status': 'success'}

(experiment) $ BREED=retriever ./test.py
{'message': 'https://images.dog.ceo/breeds/retriever-chesapeake/n02099849_1212.jpg', 'status': 'success'}

(experiment) $ BREED=mudi ./test.py
Error talking to dog picture provider: 404
```

As you can see, trying out a rare dog breed resulted a HTTP error code `404` as it doesn't exist in the DB (yet).

<div class="warn">
For restricting the program to the specific VirtualEnv only, you may consider chaning the shebang to e.g.:<br>
<code>#!/home/user/venvs/experiment/bin/python</code>
</div>

When installing 3rd party packages, PIP also ensures all the installed modules' Python scripts get the proper shebang, directly referencing the main binary as the interpreter, e.g.:

```ksh
$ pip list | grep ansible
ansible      7.3.0
ansible-core 2.14.3

$ which ansible
~/.local/bin/ansible

$ head -1 ~/.local/bin/ansible
#!/opt/Python3/bin/python3.11
```

### 11.7 The Pipenv Tool

This [tool](https://docs.pipenv.org/) is used for automatically creating and managing VirtualEnv for our projects, as well it adds/removes packages from `Pipfile` as you install/uninstall packages. It also generates the ever-important `Pipfile.lock`, which is used to produce deterministic builds.

```ksh
$ mkdir pgbackup

$ cd pgbackup/

$ pipenv --help
Usage: pipenv [OPTIONS] COMMAND [ARGS]...
[...]

$ pipenv --python $(which python3.11)
Creating a virtualenv for this project...
Pipfile: /home/gjakab/Repositories/jakab.gipsz/pgbackup/Pipfile
Using /opt/Python3/bin/python3.11 (3.11.3) to create virtualenv...
⠋ Creating virtual environment...created virtual environment CPython3.11.3.final.0-64 in 67ms
  creator CPython3Posix(dest=/home/gjakab/.local/share/virtualenvs/pgbackup-iqXhjI26, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/gjakab/.local/share/virtualenv)
    added seed packages: pip==23.1, setuptools==67.6.1, wheel==0.40.0
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment!
Virtualenv location: /home/gjakab/.local/share/virtualenvs/pgbackup-iqXhjI26
Creating a Pipfile for this project...
$ ls -la
total 16
drwxr-xr-x 3 gjakab users 4096 Apr 24 15:29 .
drwxr-xr-x 4 gjakab users 4096 Apr 24 15:28 ..
drwxr-xr-x 7 gjakab users 4096 Apr 24 15:28 .git
-rw-r--r-- 1 gjakab users  170 Apr 24 15:29 Pipfile

$ cat Pipfile 
[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"

[packages]

[dev-packages]

[requires]
python_version = "3.11"
python_full_version = "3.11.3"
```


<a name="12"></a>
## 12. Structure of a Project

When you're creating your own module or package, you need to follow a folder structure. 

An example list of tasks for a possible project: 

1. Initialize a PostgreSQL DB server, setting up a sample DB with 1000 random eimployee data
2. Install and set up the PostgreSQL DB client on another server
2. Check the DB connection, e.g. `psql postgres://demo:password@10.20.30.11:80/sample -c "SELECT count(id) FROM employees;"`
3. Set up the above `pipenv`
4. Set up the `.gitignore` file
5. Create a `README` (it can be either `.md` or `.rst`. However, an old course suggested using `.rst` since *"Many Python tools know how to parse and display reStructuredText. By using this as the markup for your documentation, you can leverage other tools to display your documentation in a presentable way."*)
6. Set up the VirtualEnv for using simple `make` or `pipenv run make` commands to run tests

`pgbackup` examples: 
- `pgbackup postgres://bob@example.com:5432/db_one --driver s3 backups`
- `pgbackup postgres://bob@example.com:5432/db_one --driver s3 local /var/local/db_one/backups/dump.sql`

### 12.1 Initial Project Structure

Here is an example for setting up an initial project structure from directory point of view: 

```ksh
$ mkdir -p src/pgbackup

$ touch src/pgbackup/__init__.py

$ mkdir tests

$ touch tests/.keep
```

The `__init__.py` file lets Python know that we're building our own 3rd party package and where to start searching for packages and even submodules. In this case, this `pgbackup` is going to be the name of the new package. 

It's also worth doing everything with source control, so `git init` might be needed, and even creating a `.gitignore` file can be useful for letting `git` know what to ignore. 

### 12.2 Configuration for `setuptools`

The documentation for the tool is [here](https://setuptools.pypa.io/en/latest/). The Basic Use can be checked out from [here](https://setuptools.pypa.io/en/latest/userguide/quickstart.html#basic-use).

Create a `setup.py`, assuming we have the above directory structure:
```
from setuptools import setup, find_packages

with open('README.md', encoding='UTF-8') as f:
    readme = r.read()

setup(
    name='pgbackup',
    version='0.1.0',
    description='Database backups locally or to AWS S3.',
    long_description=readme,
    author='Jakab',
    author_email='jakab.gipsz@example.com',
    install_requires=[],
    packages=find_packages('src'),
    package_dir={'': 'src'}
)
```

After that, initiating `pipenv` with `shell` parameter instantly puts the user to the activated environment with adjusted `PS1`, so then `pip install -e .` can be run: 

```ksh
$ pipenv shell
Creating a virtualenv for this project...
Pipfile: /home/gjakab/Downloads/Pipfile
Using /opt/Python3/bin/python3 (3.11.3) to create virtualenv...
⠙ Creating virtual environment...created virtual environment CPython3.11.3.final.0-64 in 96ms
  creator CPython3Posix(dest=/home/gjakab/.local/share/virtualenvs/Downloads-9GJblHFI, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/gjakab/.local/share/virtualenv)
    added seed packages: pip==23.1, setuptools==67.6.1, wheel==0.40.0
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator
[...]
✔ Successfully created virtual environment!
Virtualenv location: /home/gjakab/.local/share/virtualenvs/Downloads-9GJblHFI
Launching subshell in virtual environment...
[...]
$ . /home/gjakab/.local/share/virtualenvs/Downloads-9GJblHFI/bin/activate

(Downloads) $ 
(Downloads) $ pip install -e .
Obtaining file:///home/gjakab/Downloads
  Preparing metadata (setup.py) ... done
Installing collected packages: pgbackup
  Running setup.py develop for pgbackup
Successfully installed pgbackup-0.1.0
```

This package is already listed in `pip list`'s output. Our package is not ready yet, so we can also uninstall it in the usual way (but it removes the files only in the VirtualEnv): 

```ksh
(Downloads) $ pip list
Package    Version Editable project location
---------- ------- ---------------------------
pgbackup   0.1.0   /home/gjakab/Downloads/src
pip        23.1
setuptools 67.6.1
wheel      0.40.0
(Downloads) $ pip uninstall pgbackup
Found existing installation: pgbackup 0.1.0
Uninstalling pgbackup-0.1.0:
  Would remove:
    /home/gjakab/.local/share/virtualenvs/Downloads-9GJblHFI/lib/python3.11/site-packages/pgbackup.egg-link
Proceed (Y/n)? Y
  Successfully uninstalled pgbackup-0.1.0
```

### 12.3 Creating a `Makefile`

This is an example for creating a standard `Makefile` for our project, e.g.:

```
.PHONY: install test

default: test

install:
    pipenv install --dev --skip-lock

test:
    PYTHONPATH=./src pytest
```

And try it out:

```ksh
$ make install
pipenv install --dev --skip-lock
Installing dependencies from Pipfile...
Installing dependencies from Pipfile...
```

### 12.4 Distributing Our Project

- `wheel` is used for this
- it requires filling up a `setup.cfg` file defining at least the minimum Python version
- run `python setup.py bdisk_wheel` under your VirtualEnv
- it generates a `dist` directory and a `.whl` file, depending on the name and version of your project
- it can be used for PIP installation, e.g. `pip install dist/pgbackup-0.1.0-py311-none-any.whl` locally or e.g. `pip install --user https://some-remote-storage/pgbackup-0.1.0-py311-none-any.whl` from a remote storage. 


<a name="13"></a>
## 13. Test-driven Development

Test-driven Development or TDD is a **software development process** relying on software requirements being converted to test cases before software is fully developed, and tracking all software development by repeatedly testing the software against all test cases. This is as opposed to software being developed first and test cases created later. 

This approach lets the developer to proceed with "just enough" development at a time instead of focusing on too much features. 

These are multiple possible frameworks to perform TDD. Two of them are: 
- Unit Test
- `pytest`

### 13.1 `pytest`

In this chapter, we will proceed with the `pytest` framework. [Here](https://docs.pytest.org/en/latest) is its homepage for additional documentation. 

#### 13.1.1 Setting up `pytest`

To initiate the TDD work, you can create the environment for that with a single command (the example was issued under `Downloads` directory, that's why it shows that): 

```ksh
$ pipenv install --dev pytest
Installing pytest...
Resolving pytest...
Installing...
Adding pytest to Pipfile's [dev-packages] ...
✔ Installation Succeeded
Pipfile.lock not found, creating...
Locking [packages] dependencies...
Locking [dev-packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success!
Updated Pipfile.lock (a5ac1099adee41bf86860f5b04c13af694ad5cce9958c352def4b3e80ccf9a0e)!
Installing dependencies from Pipfile.lock (cf9a0e)...
Installing dependencies from Pipfile.lock (cf9a0e)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.

$ pipenv shell
Launching subshell in virtual environment...
$  . /home/gjakab/.local/share/virtualenvs/Downloads-9GJblHFI/bin/activate

(Downloads) $ pip list
Package    Version
---------- -------
iniconfig  2.0.0
packaging  23.1
pip        23.1
pluggy     1.0.0
pytest     7.3.1
setuptools 67.6.1
wheel      0.40.0
(Downloads) $ 
```

The procedure added a line under the `[dev-packages]` section in `Pipfile`: 

```
[...]
[dev-packages]
pytest = "*"
[...]
```

The testing Python program source codes should start with `test_` and the testing Python functions inside of it also should follow this, starting with `test_`, because most Python test runners will gather files having names starting with `test_` to run the tests found in the files.

#### 13.1.2 `pytest` assertions

Python3 has the `assert` [statement](https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement) built-in and `pytest` heavily relies on that. 

Creating an example test program called `test_cli.py` which uses a non-existing module inside of our new package called `pgbackup`: 

```
import pytest

from pgbackup import cli

url = "postgres://bob@example.com:5432/db_one"

def test_parser_without_driver():
    """
    Without a specified driver the parser will exit
    """
    with pytest.raises(SystemExit):
        parser = cli.create_parser()
        parser.parse_args([url])

def test_parser_with_driver():
    """
    The parser will exit if it receives a driver without a destination
    """
    parser = cli.create_parser()
    with pytest.raises(SystemExit):
        parser.parse_args([url, '--driver', 'local'])

def test_parser_with_driver_and_destination():
    """
    The parser will not exit if it receives a driver and destination
    """
    parser = cli.create_parser()

    args = parser.parse_args([url, '--driver', 'local', '/some/path'])

    assert args.url == url
    assert args.driver == 'local'
    assert args.destination == '/some/path'
```

If we set our `Makefile` up as described above (with the `test` block as default), issuing a `make` command will provide enough hints already what's next. First, the imported `cli` module will be definitely missing, so creating an empty `cli.py` file under the `pgbackup` directory will make `make` listing more specific errors. 

Initial version of the `cli` module in `cli.py` file: 

```
from argparse import Action, ArgumentParser

class DriverAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        driver, destination = values
        namespace.driver = driver.lower()
        namespace.destination = destination

def create_parser():
    parser = ArgumentParser()
    parser.add_argument('url', help="URL of the PostgreSQL DB to backup")
    parser.add_argument('--driver',
            help="how and where to store the backup",
            nargs=2,
            action=DriverAction,
            required=True)
    return parser
```

This will result successful test at this phase.

...

The `self` is a part that makes a method a method through the `__call__` definition for the `class`. The `self` is the object that the method is called on. 

...

#### 13.1.3 `pytest` fixtures

Lazily copypasted from the official documentation: 

>The [test fixtures](https://docs.pytest.org/en/6.2.x/fixture.html) initialize test functions. They provide a fixed baseline so that tests execute reliably and produce consistent, repeatable, results. Initialization may setup services, state, or other operating environments.

...

That's all for now. The article is still subject to improve. 