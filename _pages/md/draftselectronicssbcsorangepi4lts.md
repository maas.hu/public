# 🍊 Orange Pi 4 LTS

<div style="text-align: right;"><h7>Posted: 2022-10-19 20:09</h7></div>

###### .

## Table of contents

1. [Introduction](#1)
2. [Dimensions](#2)
3. [Temperature / Stress Tests with Passive Heat Sinks](#3)
4. [OS Install](#4)


![alt_text](orangepi4lts_-_top.png)


<a name="1"></a>
## 1. Introduction

...

![alt_text](first_start_20220927.png)

...


<a name="2"></a>
## 2. Dimensions

The official datasheet can be downloaded from [here](https://www.rockchip.fr/RK3399%20datasheet%20V1.8.pdf). 

My measurement results, concerning chip **packaging sizes**: 

- RK3399: 
    - outside: 21x21 mm
    - inside (useful zone for potential passive heatsink): 16.4x16.4 mm
- RAM: 14.5x10 mm x2
- eMMC: 12x10 mm
- Wifi: 8x8x5 mm

The official RPi4 heat sink kit contains one 14x15x5 mm and two 8x8x5 mm heat sinks.


<a name="3"></a>
## 3. Temperature / Stress Tests with Passive Heat Sinks

Shortly after I bought this SBC, and before applying any heat sinks (and used the original OS only), it reached even **85 °C** each time any similar test was running for at least 1 minute, so I decided to improve it a bit and take some tests. 

Active heat sink on these ARM Cortex based chips always sounded weird to me as they are basically simply the same as we had inside of a regular smartphone some years ago. And they had also only well designed, heavy, but passive heat sinks. 

I didn't use the "usual" YouTube tests or other, standard benchmarks. It was only a simple calculation (`raku -e 'my @doubles = (1...2_000_000).hyper.map(* * 2);'`), issued **3 times** per setup, that takes around **4 minutes** of heavy, multi-threaded CPU load for an RK3399. 

I also took **10 minutes of resting** time after each test to examine how the particular setup cools the chip down. Collecting the data was always done automatically via simple shell scripts. 

The setups: 
- the **1st** setup is with a completely "naked" RK3399 (but small heat sinks already applied on eMMC and WiFi)
- the **2nd** setup has the "official" Raspberry Pi 4 B heat sink set's piece, meant for its main CPU (the set contains one **14x15x5** mm and two 8x8x5 mm heat sinks)
- the **3rd** setup was just a joke of course: I put a small spanner on the the Raspberry Pi 4 B heat sink, and actually it meant a lot (as you can see above)
- the **4th** one is obvously the most efficient: that heat sink is almost a "tower": 19x19x24 mm (bougth on Aliexpress just recently)

### 3.1 Preparations and testing

I didn't use the "usual" YouTube tests or other, standard benchmarks, just a simple calculation that takes around 4 minutes of heavy, mostly multi-threaded CPU load for an RK3399: 

```ksh
$ raku -v
Welcome to Rakudo™ v2022.07.
Implementing the Raku® Programming Language v6.d.
Built on MoarVM version 2022.07.

$ raku -e 'my @doubles = (1...2_000_000).hyper.map(* * 2);'
```

#### Script for gathering temperature data

Let's monitor all the "zones". Orange Pi 4 LTS has two of them: 

```ksh
$ screen -S temp

$ while true ; do for i in /sys/class/thermal/thermal_zone*/temp ; do echo "$(date +'%H:%M:%S'),$(echo $i | awk 'BEGIN{FS="/"}{print $5}'),$(awk '{print $1/(1000)"°C"}' $i)" ; done ; sleep 10 ; done > test-temp_$(date '+%Y%m%d-%H%M%S')

^A^D
```

#### Script for 3 stress tests with 10 minutes of resting between them

In output of `time`, the `real` is the "wall clock time", so we take only that one into consideration: 

```ksh
$ screen -S perf

$ for i in {1..3} ; do echo "Test #${i},$(date +'%H:%M:%S'),$(s () { time raku -e 'my @doubles = (1...2_000_000).hyper.map(* * 2);' ; } ; s 2>time.txt ; sync ; cat time.txt | awk '/real/{print $2}')" ; sleep 600 ; done > test-perf_$(date '+%Y%m%d-%H%M%S') ; rm time.txt

^A^D
```


### 3.2 The results


![alt_text](heat_sink_tests_-_results.png)

### 3.3 Additional notes

With totally "naked" RK3399 setup out of the 4 setups (when there were small heat sinks on eMMC and WiFi chips already in each case), it decided to use the 4 weak cores (core 1-4, the Cortex-A53 ones) during the calculations for some reason, and the cores 5 and 6 (the two Cortex-A72) were just... resting.

Normally, this is what should be noticed during such load: 

![alt_text](heat_sink_tests_-_htop.png)


<a name="4"></a>
## 4. OS Install

...

![alt_text](nand_flash_install_-_1.png)

...

![alt_text](nand_flash_install_-_2.png)

...

![alt_text](nand_flash_install_-_3.png)

...

![alt_text](nand_flash_install_-_4.png)

...

![alt_text](nand_flash_install_-_5.png)

...

![alt_text](nand_flash_install_-_6.png)

...