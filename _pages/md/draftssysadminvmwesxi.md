# ![alt text](/_ui/img/icons/vmware_m.svg) VMware ESXi

###### .

>A VMware-ről már végképp csak a "teljesség igénye" miatt készült el az oldal. Semmi specifikusra (egyelőre) ne számíts. Inkább csak képnézegetés. Ugye, milyen szép? Na ugye!

## Tartalomjegyzék

1. [Bevezető](#1)
2. [Alap ellenőrzések](#2)

<a name="1"></a>
## 1. Bevezető

...

![alt_text](vsphere-6.7-censured-transformed.jpg)

<a name="2"></a>
## 1. Alap ellenőrzések

Csak betekintésre alkalmas user esetében nagyjából az alábbi három dolog miatt kell, hogy legyen bejárásunk. 

### 1.1 VM IP-i

![alt_text](vm-ips.png)

### 1.2 VM kapacitása

![alt_text](vm-capacity.png)

### 1.3 VM snapshot-jai

![alt_text](vm-snapshots-1.png)

![alt_text](vm-snapshots-2.png)