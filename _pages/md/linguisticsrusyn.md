#  ![alt text](/_ui/img/flags/rusyn_m.svg) Ruszin nyelv / Русиньска бесїда

<div style="text-align: right;"><h7>Posted: 2022-08-27 21:44</h7></div>

###### .

![alt_text](rusyn_-_bg.jpg)


## Tartalomjegyzék

1. [Bevezető - Увод](#1)
2. [Az ábécé - Алфавіт](#2)
3. [Alapszavak, kifejezések - ](#3)


<a name="1"></a>
## 1. Bevezető - Увод

A **ruszin nyelv** az ukrán nyelvhez közel álló keleti szláv nyelv, amelyet földrajzilag két csoportba oszthatunk: *pannon ruszin* (ismert még *vajdasági* vagy *bácsi* ruszin nyelvként is) és a *kárpátaljai ruszin* nyelvekre. Hivatalosan Ukrajnában máig nem ismerik el, mint önálló nyelvet, a ruszinokat pedig, mint önálló népcsoportot. Hivatalos nyelvként csupán a Vajdaságban használják a pannon ruszint, amelynek külön irodalmi normája van.  

A hivatalos adatok szerint az összes ruszin dialektust együttesen 600.000-nél is többen beszélik annak ellenére, hogy a különböző országokban élő ruszin kisebbségek együttvéve csupán 70.000 főt tesznek ki. Kárpátalján (*Закарпатська область*) rengetegen, még az ott élő magyarok és azok leszármazottai is többnyire hétköznapjaik során a *kárpátaljai ruszin nyelvet*, vagy ahogy általában említik: a *kárpátaljai nyelvet* beszélik, miközben nemzetiségüket tekintve csak nagyon kevesen tekintik magukat ruszinnak. 

A továbbiakban a **kárpátaljai ruszin** nyelvet vesszük alapul.  

![alt_text](karpatska_rus.png "A kárpáti ruszin nép elterjedése az első világháború előtt")  
###### A kárpáti ruszin nép elterjedése az első világháború előtt


<a name="2"></a>
## 2. Az ábécé - Алфавіт

<table class="tan">
  <tr>
    <td><b>А a</b></td><td><b>Б б</b></td><td><b>В в</b></td><td><b>Г г</b></td><td><b>Ґ ґ</b></td><td><b>Д д</b></td><td><b>Е е</b></td><td><b>Є є</b></td>
  </tr>
  <tr>
    <td><b>Ё ё</b></td><td><b>Ж ж</b></td><td><b>З з</b></td><td><b>І і</b></td><td><b>Ї ї</b></td><td><b>И и</b></td><td><b>Ы ы</b></td><td><b>Ӣ ӣ</b></td>
  </tr>
  <tr>
    <td><b>К к</b></td><td><b>Л л</b></td><td><b>М м</b></td><td><b>Н н</b></td><td><b>О о</b></td><td><b>П п</b></td><td><b>Р р</b></td><td><b>С с</b></td>
  </tr>
  <tr>
    <td><b>Т т</b></td><td><b>У у</b></td><td><b>Ф ф</b></td><td><b>Х х</b></td><td><b>Ц ц</b></td><td><b>Ч ч</b></td><td><b>Ш ш</b></td><td><b>Щ щ</b></td>
  </tr>
  <tr>
    <td><b>Ю ю</b></td><td><b>Я я</b></td><td><b>Ь ь</b></td><td><b>Ъ ъ</b></td><td colspan="4"></td>
  </tr>
</table>



<a name="3"></a>
## 3. Alapszavak, kifejezések - 

### 3.1 Magyarral rokon szavak

**бiцiглi** <span class="s" onclick="speak('0001')">💬</span> - bicikli  
**бесїда** <span class="s" onclick="speak('0002')">💬</span> - beszéd, nyelv (de más szláv nyelvekben is előfordul)  
**блодер** <span class="s" onclick="speak('0003')">💬</span> - sütő (K-Magyarországon szintén *"blóder"*)  
**боут** <span class="s" onclick="speak('0004')">💬</span> - bolt (régiesen ejtve magyarul is: *"bóút"*)  
**варош** <span class="s" onclick="speak('0005')">💬</span> - város  
**ґазда** <span class="s" onclick="speak('0025')">💬</span> - gazda, házigazda  
**ґатi** <span class="s" onclick="speak('0026')">💬</span> - nadrág (gatya)  
**дараб** <span class="s" onclick="speak('0006')">💬</span> - darab  
**жеб** <span class="s" onclick="speak('0007')">💬</span> - zseb  
**інаш** - inas  
**калач** - kalács  
**канчув** - kancsó  
**капу** - kapu  
**керт** <span class="s" onclick="speak('0008')">💬</span> - kert  
**киста** <span class="s" onclick="speak('0009')">💬</span> - tészta (sütemény értelemben, de K-Magyarországon a süteményt is *"tésztának"* hívják)  
**комино** - kémény  
**кошарка** <span class="s" onclick="speak('0010')">💬</span> - kosár (kosárka)  
**крумплi** <span class="s" onclick="speak('0011')">💬</span> - krumpli  
**леквар** <span class="s" onclick="speak('0012')">💬</span> - lekvár  
**лӱтра** - létra  
**мачка** <span class="s" onclick="speak('0013')">💬</span> - macska (de más szláv nyelvekben is előfordul)  
**морков** <span class="s" onclick="speak('0014')">💬</span> - sárgarépa (K-Magyarországon szintén *"murkó"*)  
**мусай** <span class="s" onclick="speak('0015')">💬</span> - muszáj  
**оцет** <span class="s" onclick="speak('0016')">💬</span> - ecet (De ukránul is!)  
**палачiнтовка** <span class="s" onclick="speak('0017')">💬</span> - palacsinta  
**парадичка** <span class="s" onclick="speak('0018')">💬</span> - paradicsom  
**пасуля** <span class="s" onclick="speak('0019')">💬</span> - bab (paszuly)  
**пiцiко** - picike  
**погар** <span class="s" onclick="speak('0020')">💬</span> - pohár  
**пулька** - pulyka  
**серсама** <span class="s" onclick="speak('0021')">💬</span> - szerszám  
**танир** - tányér  
**тапочкi** <span class="s" onclick="speak('0022')">💬</span> - papucs(ok) (De ukránul is!)  
**ташка** <span class="s" onclick="speak('0023')">💬</span> - táska  
**тинта** - tinta  
**уброся** - abrosz  
**федивка** - fedő  
**цимбор** <span class="s" onclick="speak('0024')">💬</span> - cimbora  
**шетемен** - sütemény  
**ширинка, убрус** - kendő  

...


### 3.2 Néhány további szó

**возир** - ablak  
**ґарадичї** <span class="s" onclick="speak('0029')">💬</span> - lépcső  
**горниц** - fazék  
**дичка** <span class="s" onclick="speak('0032')">💬</span> - apró szemű körte  
**нуж** - kés  
**фiрханга** <span class="s" onclick="speak('0028')">💬</span> - függöny  
**финджа** <span class="s" onclick="speak('0030')">💬</span> - bögre  
**хежа** <span class="s" onclick="speak('0027')">💬</span> - ház  
**штрiмфлi** <span class="s" onclick="speak('0031')">💬</span> - zokni(k)  

**моти** <span class="s" onclick="speak('0033')">💬</span> - mosni  

...

### 3.3 Néhány alapmondat

**Поздравы!** <span class="s" onclick="speak('0034')">💬</span> / **Поклоны!** <span class="s" onclick="speak('0035')">💬</span> - Szia! *("Üdvözöllek!")*  
**Агой!** / **Привiт!** - Hello! (a legáltalánosabb köszönés ismerős felé)  
**Здоров!** <span class="s" onclick="speak('0036')">💬</span> - Szervusz! (csak férfinak mondják, másik jelentése: *"kézfogás"*)  
**Слава Iсусу Христу!** <span class="s" onclick="speak('0037')">💬</span> - *kb.* Üdvözlöm! *(kb. "Dícséret Krisztusnak!")*  
**Навiкы Богу Слава!** <span class="s" onclick="speak('0038')">💬</span> - *kb.* Üdvözlet önnek is! *(válasz az előbbire, kb. "Istennek szintén!")*  

**Добрый рано!** <span class="s" onclick="speak('0039')">💬</span> - Jó reggelt!  
**Добрый динь!** <span class="s" onclick="speak('0040')">💬</span> - Jó napot!  
**Добрый вичур!** <span class="s" onclick="speak('0041')">💬</span> - Jó estét!  

**Доброго здоровлїчка!** <span class="s" onclick="speak('0042')">💬</span> / **Доброго здоровля!** <span class="s" onclick="speak('0043')">💬</span> - Jó egészséget kívánok! (általános köszöntés egy hallgatósághoz, pl. *"Köszöntök mindenkit!"*)  

**Май ся!** <span class="s" onclick="speak('0045')">💬</span> - Szevasz! (baráti elköszönés, jelentése: *Légy jó!*; további változatai: **Май ся фест!** / **Май ся добрї!** / **Мая ся шумнi!**)  
**Майте ся!** - Viszlát! / Szevasztok! (baráti elköszönés, jelentése: *Legyen jó ön!* / *Legyetek jók!*)  
**Ходи здоровый!** <span class="s" onclick="speak('0046')">💬</span> - Szia! (elköszönés, ha a másik fél távozik egy haverjától pl., jelentése: *Menj egészséggel!*)  
**Ходїт здоровi** <span class="s" onclick="speak('0047')">💬</span> - Viszlát! / Sziasztok! (elköszönés, ha egy idősebb, vagy mások többen távoznak, jelentése: *Menjen egészséggel!* / *Menjetek egészséggel!*)  
**Будь здоровый!** <span class="s" onclick="speak('0048')">💬</span> - Szia! (elköszönés, ha az elköszönő távozik egy haverjától pl., jelentése: *Menj egészséggel!*)  
**Будьте здоровi!** <span class="s" onclick="speak('0049')">💬</span> - Viszlát! / Sziasztok! (elköszönés, ha az elköszönő távozik egy idősebbtől vagy másoktól, jelentése: *Menjen egészséggel!* / *Menjetek egészséggel!*)  

**Пой на фриштик!** <span class="s" onclick="speak('0050')">💬</span> - Gyere reggelizni!  
**Пой на пив децу!** <span class="s" onclick="speak('0051')">💬</span> - Gyere egy felesre!

...

>A jegyzet még rendkívül hiányos, begyűjtés alatt áll...
