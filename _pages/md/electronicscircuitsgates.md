# 🧮 Prototype Logic Gates

<div style="text-align: right;"><h7>Posted: 2022-11-18 22:33</h7></div>

###### .

![alt_text](gates_bg.jpg)


## Table of contents

1. [Introduction](#1)
2. [Logic Gates with Mechanic Switches](#2)
3. [Logic Gates with Diodes](#3)
4. [Resistor-Transistor Logic](#4)
5. [Diode-Transistor Logic](#5)
6. [Transistor-Transistor Logic](#6)
7. [Logic Gates with JFETs](#7)
8. [CMOS Logic](#8)
9. [NMOS Logic](#9)
10. [Bi-CMOS Logic](#10)


<a name="1"></a>
## 1. Introduction

I was always curious whether the different logic gates, the very important building blocks of today's high tech devices, can be reproduced by **discrete components**. Even if the exact reproduction is usually almost impossible, I thought, it would be great to build something that works as similar as possible, following the same idea, using the same types of active components. 

The below is not a thorough walkthrough on **Digital Electronics Basics**, however, it deals with quite many actual implementation of historical logic gate families. It's also important to note that inside of today's digital ICs, the real implementations are, of course, sometimes **much more advanced**, even if I call them as *XYZ Logic* here. 

Even though that was not tested, all the gates with two inputs below are capable to be improved to three, four or more input ones, but this can be deduced from the design relatively easily. 


<a name="2"></a>
## 2. Logic Gates with Mechanic Switches

Only for the sake of completeness, we need to mention the originally expected behavior of these logic gates through traditional, mechanic switches, or buttons. Even though these basics are not really complicated, these also help us to understand the requirements and the truth tables. 

In Digital Electronics, we deal with **zeros** and **ones**. These can be also considered, and also often marked as **Low (L)**, representing 0, and **High (H)**, representing 1, logic levels. 

### 2.1 The `AND` Gate

The mechanic **switch implementation** of the `AND` gate: 

![alt_text](switch_-_and.svg)

The **truth table** of this `AND` gate: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>1</td>
  </tr>
</table>

Shortly, the `Out` (e.g. a lightbulb) gets the voltage for lightning **only** if both `A` and `B` switches are pushed (pushed, meaning they both get the `1`, such as High (`H`) level). 

### 2.2 The `OR` Gate

The mechanic **switch implementation** of the `OR` gate: 

![alt_text](switch_-_or.svg)

The **truth table** of an `OR` gate: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>1</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>1</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>1</td>
  </tr>
</table>

Shortly, the `Out` (e.g. a lightbulb) gets the proper voltage for lightning if **any** of `A` or `B` or both switches are pushed. 

### 2.3 The `NOT` Gate or Inverter

The mechanic **switch implementation** of the Inverter, with a **default-on** switch: 

![alt_text](switch_-_not.svg)

This single-input logic gate type does nothing but inverts its input to the output, so its **truth table** is extremely simple: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
  </tr>
</table>

There is a fourth fundamental logic gate, the **Buffer**, which is even simpler, but also very important. Instead of wasting of each other's time with that, let's see an actually really frequently used module of any prototype logic gate testing environment, the **Gate Input**. 

### 2.4 The Gate Input Circuit

Normally, we want to have a default `L` (`0`) on each gate input, and send `H` (`1`) to it only when the button is pressed. This can be also considered as the implementation of a **mechanical Buffer**: 

![alt_text](switch_-_input.svg)

- the first circuit with the **10k** pull-down resistor is fine with any **CMOS, HCMOS etc.** logic families
- the second circuit with the **1k** resistor is recommended for **TTL, S TTL, LS TTL** logic families, especially for the old ones, when **at least 1.6mA** is the minimal requirement on gate inputs

Voltage levels on the output, when using the 1st gate input circuit with 10k pull-down resistor, also assuming that the `V+` is **4.97..5V**:

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>4.90V</td>
  </tr>
</table>

<div class="warn">
In this article, the V+, +V<sub>CC</sub> and +V<sub>DD</sub> are always this <b>4.97..5V</b>. Also, I took the <b>10k</b> pull-down resistor as "default" when building these prototype logic gates. If something else needs to be used, then that is mentioned separately.
</div>

### 2.5 Logic Gates' Tolerance Limits

In case of real logic gates, the logic `H` and `L` levels have tolerance limits. As only these two levels exist (forgetting the fact for now that there are even 3-state driver gates), we need to be relatively strict when determining what voltage levels means what. 

Traditionally, these are the **tolerance levels** of TTL gates (Input Signal Levels are the expected voltages on input connectors, while the Output Signal Levels are the ones that we can expect to appear on any TTL-compatible gate after logic "processing"): 

![alt_text](acceptable_logic_levels.svg)

The logic gates are also good to *clarify* the signals, appearing on their input connectors, so e.g. a TTL gate is able to **detect and amplify** a 2.5V level signal as an obvious `H` logic level. Also, anything between 0.8V and 2V are considered as uncertain, so we should avoid such signals as much as possible. 

<div class="warn">
It's also worth mentioning that several logic gate families don't tolerate if they receive higher input level voltage than their main supply voltage!
</div>


<a name="3"></a>
## 3. Logic Gates with Diodes

Possible implementations of simple resistor-diode-based logic gates: 

![alt_text](rdl.svg)

The chosen diode can be any general, multi-purpose one, but at least I had a lot of **1N4004**, so I chose ones. All the other diodes below are tested with this type. 

The expected logical levels of `AND` and `OR` gates are known, now let's see the actual voltage levels on the above implementations. 

Measured, real levels of the above illustrated `AND` gate, **assuming `V+` is 5V**: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.37V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.42V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.42V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>4.46V</td>
  </tr>
</table>

And the same for the `OR` gate: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.45V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.45V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>4.46V</td>
  </tr>
</table>

Involving resistors and diodes only, it's not possible to build inverter, therefore `NAND` and `NOR` gates are not possible either. 


<a name="4"></a>
## 4. Resistor-Transistor Logic

Resistor-Transistor Logic (**RTL**) circuits were first constructed with discrete components, as only number of transistors were needed for them. At the time, resistors were cheaper and transistors were still hard to make. 

This approach became the **first digital logic family** to be produced **in 1961** as a monolithic integrated circuit. These logic gates were even used in the [Apollo Guidance Computer](https://en.wikipedia.org/wiki/Resistor%E2%80%93transistor_logic#Multi-transistor_RTL_NOR_gate), which was built using ICs, each of them having 3-input RTL-based NOR gates.

A bit more advanced logic IC family was Texas Instruments' `AAxxx` [series](https://archive.org/details/radio_electronics_1980-01/page/n41/mode/2up) (page 42), also from the 60's, where at least `AAAL4` had basic gates, and `AAFL1` contained flip-flops (even the naming of the IC suggested it nicely).

The more widely available IC family was Motorola's `MCxxxP`. E.g., `MC789P` was a hex inverter. This family is thoroughly described in Don Lancaster's [RTL Cookbook](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjGobK4n-D7AhVvk4sKHYIyB6QQFnoECA4QAQ&url=https%3A%2F%2Fworldradiohistory.com%2FArchive-Don-Lancaster%2Frtlcb.pdf&usg=AOvVaw3rQvAgMku4Im2J_6dCDNl9).

Let's see the actually tried, and working solutions with NPN transistors first. 

### 4.1 RTL with NPN BJTs

These are the classic solutions as NPN transistors are more common. Also, many books and articles mention **10k** sized resistors for all of them, which mostly works, at least with ~5V supply voltage, but sometimes we can get more stable signals only when something else as pull-up or pull-down resistors are applied. 

The chosen NPN transistors are the same, but I marked them for these circuits to be sure. I used especially **BC547B**, but any similar, multi-purpose NPN BJTs are fine. 

Regarding their power supply, traditionally, `Vcc` means **Common Supply Voltage**, which is the IEEE 255 doubled-subscript notation for supply voltage at the collector.

#### 4.1.1 RTL NPN Buffer

![alt_text](rtl_-_npn_-_buffer.svg)

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>4.24V</td>
  </tr>
</table>

#### 4.1.2 RTL NPN Inverter

![alt_text](rtl_-_npn_-_not.svg)

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>4.96V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0.10V</td>
  </tr>
</table>

#### 4.1.2 RTL NPN `AND` Gate

Notice that the pull-down resistor was rather selected as a 1k one. 

![alt_text](rtl_-_npn_-_and.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.38V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>3.90V</td>
  </tr>
</table>

The results are still not perfect, but when 10k resistor was used as R3, the output level for `A=0; B=1` was even worse, 2.10V. 

#### 4.1.2 RTL NPN `NAND` Gate

![alt_text](rtl_-_npn_-_nand.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.96V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.89V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.90V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.24V</td>
  </tr>
</table>

#### 4.1.2 RTL NPN `OR` Gate


![alt_text](rtl_-_npn_-_or.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.24V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.24V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>4.26V</td>
  </tr>
</table>

#### 4.1.2 RTL NPN `NOR` Gate

![alt_text](rtl_-_npn_-_nor.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.96V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.10V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.10V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.07V</td>
  </tr>
</table>


### 4.2 RTL with PNP BJTs

The idea is pretty much the same as previously, in this case, with reversed polarity, but that's not the only main difference. The arrangement of the components are also swapped between `AND`, `NAND` and `OR`, `NOR` gates, resulting more reliable output signals for PNP `AND`, where NPN `AND` gate was poor, but also resulting poorer output on PNP `OR`, where NPN `OR` gate was a bit more stable. 

As the supply voltage is still +5V for them, let's keep marking it with +V<sub>CC</sub> (even though it always arrives to emitters in these designs). 

#### 4.2.1 RTL PNP Buffer

The resistor before the collector has an unordenary resistor to get valuable results: 

![alt_text](rtl_-_pnp_-_buffer.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0.50V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>4.47V</td>
  </tr>
</table>

#### 4.2.2 RTL PNP Inverter

This gate perfectly follows the standards, and gives solid output signals: 

![alt_text](rtl_-_pnp_-_not.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>4.88V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

#### 4.1.2 RTL PNP `AND` Gate

The gate follows the Buffer's values: 

![alt_text](rtl_-_pnp_-_and.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.49V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.50V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.50V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>4.45V</td>
  </tr>
</table>

#### 4.1.2 RTL PNP `NAND` Gate

This gate follows the inverter's values, and so as its solid output signals too: 

![alt_text](rtl_-_pnp_-_nand.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.88V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.88V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.88V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.02V</td>
  </tr>
</table>


#### 4.1.2 RTL PNP `OR` Gate

After several trials, I ended up with this **7.8k** resistor as R3 to get these relatively acceptable output levels, but this construction is probably the worst performing one in my RTL designs: 

![alt_text](rtl_-_pnp_-_or.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.75V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>3.71V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>4.90V</td>
  </tr>
</table>

#### 4.1.2 RTL PNP `NOR` Gate

And finally, one of the best constructions that we can achieve with BJTs: 

![alt_text](rtl_-_pnp_-_nor.svg)

The measured stability of this solution is quite impressive: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.87V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

And that's it! Further ideas, even mixing NPN and PNP transistors in gates (without exact signal measurements): 

- [RTL with both NPN and PNP](http://codeperspectives.com/computer-design/npn-pnp-logic-gates/)


<a name="5"></a>
## 5. Diode-Transistor Logic

The idea of DTL logic gates were used in commercial IC families, starting in 1962, and they were considered as a more successful logic family than RTL, before TTL became easier to be manufactured, and that forced DTL out of the market. At least, there were `NTE9xxx` named (by NTE), `FCHxxx` (by Philips), and `MIC9xx` (by Motorola) DTL ICs were widely available. NTE's ICs are still not impossible to buy in shops as of today. 

The logic gates that the actual ICs contain are obviously a bit more sophisticated than the below prototype ones, but the idea behind them is the same. Let's see and test them through. 

### 5.1 DTL `AND` Gate

![alt_text](dtl_-_and.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.02V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.12V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.12V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>3.78V</td>
  </tr>
</table>

### 5.1 DTL `NAND` Gate

![alt_text](dtl_-_nand.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.92V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.15V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.19V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.04V</td>
  </tr>
</table>

### 5.1 DTL `OR` Gate

For DTL `OR` and `NOR` gates, the same idea, such as combining the single, diode-resistor gate with RTL buffer/inverter, but replacing the resistor before the base by a forward biased diode doesn't really work. In that case, the circuit would take huge power. Instead, we can just apply a reduced preliminary diode gate, combined with a standard RTL buffer and inverter. 

![alt_text](dtl_-_or.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>3.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>3.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>3.92V</td>
  </tr>
</table>

### 5.4 DTL `NOR` Gate

![alt_text](dtl_-_nor.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.96V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.10V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.10V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.10V</td>
  </tr>
</table>


### 5.5 DTL as an actual logic family

Below you can find the classic implementation of a NAND gate from the DTL family, as it was actually used in `MIC9xx` IC family.

![alt_text](dtl_-_nand_-_mic9xx.svg)

The [series](https://www.datasheetarchive.com/pdf/download.php?id=ab779cf5a406e1f2a138c6a21d4d7d62cffdab&type=O&term=930%2520series%2520dtl) started with `MIC930`, which was a *"Dual 4-input NAND gate with fan-in expander nodes"*.


<a name="6"></a>
## 6. Transistor-Transistor Logic

The TTL family is the greatest success of all times, even as of today. It shares several basic ideas with DTL family, but the input diodes were replaced by transistors, where the input signals always arrive to NPN emitters. The standard gates were designed in 1961, and their mass production as the well known `74xx` series ICs began in the 2nd half of the 60's by Texas Instruments. This `74xx` family was so successful, that it became an industry standard, and other manufacturers mostly started to use the same naming. Even if the naming was not the same (e.g. in the Soviet Union), the devices themselves were fully compatible with other manufacturers' products. 

### 6.1 The Fundamental TTL `NAND` Gate

The [basic TTL circuit](https://en.wikipedia.org/wiki/Transistor%E2%80%93transistor_logic#Fundamental_TTL_gate) is always a **multi-emitter** `NAND` gate. If we want to reproduce the circuit with discrete components, any multi-emitter transistor is not a usual part on the market, but luckily, it's easy to substitute with multiple BJTs, with their **base and collector pins connected** together. The chosen transistors are still the same as above: **BC547B** or any similar, multi-purpose NPN BJTs. 

![alt_text](ttl_-_nand_-_basic.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.98V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.92V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.92V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.27V</td>
  </tr>
</table>

### 6.2 Advanced TTL `NAND` Gate with Totem-Pole Output

This adjustment makes it possible to get rid of high output resistance, to make the gate more stable. 

![alt_text](ttl_-_nand_-_tp.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.24V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.18V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.18V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.01V</td>
  </tr>
</table>

The results are really convincing, and this circuit design is really similar to the actually implemented TTL `NAND` gates in ICs, that are manufactured even today. 

### 6.3 Modern TTL Implementations

You're probably aware of the internet is full with circuit diagrams (sometimes, they differ a bit) of the standard TTL NAND gate, as it's implemented 4 times in 7400 IC and it's "clones". Now let's see bit more rare circuits about, how the bit more modern `S` (**Schottky**) and `LS` (**Low-Power Schottky**) circuits are designed in ICs, also as `NAND` gates: 

- `NAND` gate in 74S00: 

![alt_text](ttl_-_nand_-_s.svg)

- `NAND` gate in 74LS00: 

![alt_text](ttl_-_nand_-_ls.svg)

⚠️ It's important to note (again) that TTL ICs' gates **require an input-low to sink 1.6 mA** at the low voltage of 0.8V. (Newer products may not need 1.6 mA, but older TTL did.) It means, **the 10kΩ pull-down resistor for manual input signals would be too large**. We need e.g. **1kΩ**, and it will work fine. 

These are the results when an actual SN7400's `NAND` gate is tested (with 1kΩ pull-down resistor for the input pins): 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>3.81V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>3.79V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>3.79V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.05V</td>
  </tr>
</table>


<a name="7"></a>
## 7. Logic Gates with JFETs

...

This chapter still exists in theory only. At least this article suggests it's possible: 

- [gates with N-Channel JFETs](https://www.techbriefs.com/component/content/article/tb/pub/briefs/semiconductors-and-ics/3415)

...

#### 7.1 N-JFET Buffer

...

A poor design from me so far: e

![alt_text](njfet_-_buffer.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0.44V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>3.96V</td>
  </tr>
</table>

...



<a name="8"></a>
## 8. CMOS Logic

We call a wiring diagram **complementary** when it contains semiconductors in pairs, from the opposite sex but with same or very similar electric characteristics, e.g. same *"Gate to Source Cutoff voltage"* (V<sub>GS(off)</sub>) but in the other polarity. 

It's not a coincidence the **Complementary MOSFET (CMOS)** gates became really popular right after they appeared on the "market". They provide a very solid signal with almost zero current consumption. However, even the basic TTL gates can provide significantly higher switching speed than these, the integrability rate of MOSFET transistors (especially with the absence of the extra resistors between them) and the very low power consumption made them quickly extremely successful. 

The below prototype gates are constructed and tested both with **BS107**/**BS208** and **BS170**/**BS250** (in TO-92 package) MOSFET pairs. 

### 8.1 CMOS Inverter

We start with this design right away, because basically nobody ever constructs the Buffer gate and its enhanced ones out of CMOS transistors. Instead, everybody (so as the manufacturers) put an extra Inverter, when Buffer, `AND` or `OR` are created.

![alt_text](cmos_-_not.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>4.94V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

### 8.2 CMOS `NAND`

![alt_text](cmos_-_nand.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.94V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

### 8.3 CMOS `NOR`

![alt_text](cmos_-_nor.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.94V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

### 8.4 CMOS with Protected Output

There can be moments, when all the output MOSFETs conduct, and **a high current can flow across them**. If these gates are built of really small signal complementary components like **BS107**/**BS208** or **BS170**/**BS250** (in TO-92 package) or even "worse", **BSS84**/**BSS138** (SOT23), it's possible that they would be thermally destroyed in time, especially if the input of the logic gate is left *floating* or the rise or fall time of the input signal is very slow. (The source and more info on this theory can be found at [mynor.org](http://mynor.org/downloads/TraNOR-Transistor-Logic.pdf).)

To protect them a little bit, an additional resistor can be useful, while it doesn't slow the switching speed. It can be placed right before the output to the +V<sub>DD</sub> direction in all the above gates like this: 

![alt_text](cmos_-_not_-_protected.svg)

If the transistor pairs are more *robust* ones, like **2SK2231**/**2SJ377** (TO-252), this extra resistor is unnecessary.


<a name="9"></a>
## 9. NMOS Logic

NMOS gates are slower and consume more power than CMOS, but the manufacturers were able to achieve the higher integration rate for a while, so this family also definitely deserves a chapter. At least the [Signetics 2650](https://en.wikipedia.org/wiki/Signetics_2650) CPU was built of NMOS gates in the '70s. 

In theory, we can construct the Inverter, `NAND` and `NOR` gates like this, using the depletion mode NMOS (T1 in each case) simply as a resistor. All of these work fine, tested, and got solid outputs (4.90 - 4.94 V): 

![alt_text](nmos_-_not-nand-nor_-_theory.svg)

Now let's try constructing even the buffering gates too, using only Enhancement-mode NMOS transistors. The applied discrete component is **BS107**, **BS170** or similar in each case below. 

#### 9.1 NMOS Buffer

Having a 10k resistor already provides acceptable logic levels, but 1M is much better: 

![alt_text](nmos_-_buffer.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>3.96V</td>
  </tr>
</table>

#### 9.2 NMOS Inverter

In case of pull-up resistor, 10k turned out to be better: 

![alt_text](nmos_-_not.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">In</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>4.98V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

### 9.3 NMOS `AND` Gate

![alt_text](nmos_-_and.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>3.98V</td>
  </tr>
</table>

### 9.4 NMOS `NAND` Gate

![alt_text](nmos_-_nand.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.97V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>4.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>4.91V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>

### 9.5 NMOS `OR` Gate

![alt_text](nmos_-_or.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>3.96V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>3.93V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>3.97V</td>
  </tr>
</table>

### 9.6 NMOS `NOR` Gate

![alt_text](nmos_-_nor.svg)

The measured voltages on the output: 

<table class="tan">
  <tr>
    <th width="40px">A</th>
    <th width="40px">B</th>
    <th width="50px">Out</th>
  </tr>
  <tr>
    <td>0</td>
    <td>0</td>
    <td>4.97V</td>
  </tr>
  <tr>
    <td>0</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>0</td>
    <td>0.00V</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>0.00V</td>
  </tr>
</table>


<a name="10"></a>
## 10. Bi-CMOS Logic

As mentioned above, the BJT-based families are significantly faster than the MOSFET ones. But how much faster? According to Texas Instument's "Logic Guide", the so called "Propagation Delay" (t<sub>PD</sub>) is **120 ns** for the standard CMOS `NAND` gate (CD4011) versus the TTL's **5 ns** (SN7400). As the technology improved, the "**High-Speed CMOS**" (74HCxxx) appeared with **22-68 ns**, then the "**Advanced High-Speed CMOS**" (74AHCxxx) came out with **7-14 ns**. It is enough for most needs but it can be improved further. 

And then the engineers started to construct families with leveraging the advantages from both branches. These gates are basically **CMOS gates in their core with TTL drivers at their output**. The result is reaching even 2 ns tPD in case of "**Advanced Low-Voltage BiCMOS**" (74ALBxxxxx) at 3.3 V. 

...
