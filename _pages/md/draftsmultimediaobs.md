# ![alt text](/_ui/img/icons/obs_m.svg) OBS Studio

<div style="text-align: right;"><h7>Posted: 2022-01-27 21:44</h7></div>

###### .


1. [Introduction](#1)
2. [Installation](#2)
3. [General Settings](#3)
4. [Content Recording](#4)
5. [Streaming](#5)
6. [Customized Capturing Scenarios](#6)


<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Installation

...

```ksh
# rpm -qa | grep -i -E "obs-studio|libqt5-qtwayland"
libqt5-qtwayland-5.15.2+kde37-1.5.x86_64
obs-studio-27.1.3-1.26.x86_64
```


<a name="3"></a>
## 3. General Settings

...

![alt_text](output_simple.png "")

...

![alt_text](output_advanced.png "")

...


<a name="4"></a>
## 4. Content Recording

...

![alt_text](camera_video_format.png "")

...

![alt_text](resize_output.png "")

...

![alt_text](audio_mixer.png "")

...


<a name="5"></a>
## 5. Streaming

...

![alt_text](twitch_stream_key.png)

...

![alt_text](stream_twitch.png)

...

![alt_text](twitch_download.png)

...


<a name="6"></a>
## 6. Customized Capturing Scenarios

### 6.1 Custom Area of a window

If you want to capture only an area either from a screen or a window, like this:

![alt_text](obs_-_record_area_-_widescreen_-_full_picture.jpg)

...then you need to adjust the **Position** and **Size**, the **Bounding Box Size**, and also set the **Bounding Box Type** to **Scale to inner bounds**, e.g.: 

![alt_text](obs_-_record_area_-_widescreen.png)

However, you should want to scrictly capture an area of your screen, no matter what's there, so let's see the easier way with Screen Capture.

### 6.2 Custom Area of the screen

1. Make sure the "**Screen Capture (PipeWire)**" is the only one in your sources
2. Measure what's **the size** of the desired area e.g. in GIMP. In this example, it's 1721x1359
3. Set this size **Controls** --> **Settings** --> **Video**, both for **Base (Canvas) Resolution** and for **Output (Scaled) Resolution**
4. Click with `[RMB]` on your preview area --> **Transform** --> **Edit Transform**
5. Set the **Position**, e.g. -32.0000 px for horizontal one. You can leave everything else as default:  
  ![alt_text](obs_-_record_area_-_screen_capture.png)

Notes: 

- Always look for the screen sharing indicator (probably an orange one) on the top right corner (in case you use GNOME)!
- It worth checking the preview sometimes to ensure whether that's still recording the video!
- Unmute/Mute microphone on demand!

