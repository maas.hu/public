# ![alt text](/_ui/img/icons/azure_m.svg) Azure

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_bg.png)


## Muistiinpanoja näistä Azure-artikkeleista

<div class="info">
<p>Nämä artikkelit auttavat minua oppimaan suomea samalla, kun tutkin Azuren käyttöä. Haluan jakaa hyödyllisiä ajatuksia Azuren ominaisuuksista. Kirjoitan selkosuomeksi, ja toivon, että nautit lukemisesta yhtä paljon kuin minä nautin oppimisesta!</p>
</div>

Teknisten otsikoiden ja erikoistermien, kuten *"Azure Identities and Governance"*, kohdalla käytetään englanninkielisiä termejä, koska ne ovat alalla yleisiä. Siksi kaikki nämä termit pidetään englanniksi artikkeleissa.

Tarkoituksena on kerätä ajatuksia, ei kirjoittaa täydellistä opasta. Älä ota näitä liian vakavasti! Käytä aina Microsoftin virallista dokumentaatiota. Aloita [täältä](https://learn.microsoft.com/en-us/azure)!

Mitä tulee kaavioiden ikoneihin, tässä on Microsoftin lausunto: *"Microsoft sallii näiden ikonien käytön kaavioissa, koulutusmateriaaleissa tai dokumentaatiossa. Saat kopioida, jakaa ja näyttää ikoneja vain sallittuun käyttöön. Muut oikeudet pidätetään."*


## Johdanto Azureen

**Microsoft Azure** on ollut tuotteen nimi 25. maaliskuuta 2014 lähtien, mutta alusta esiteltiin ensimmäisen kerran **27. lokakuuta 2008** nimellä *"Project Red Dog"*. Kirjoitushetkellä se on yksi suurimmista pilvipalvelualustoista. Azure tarjoaa **SaaS**, **PaaS** ja **IaaS** -ratkaisuja.


## Miksi Azure?

Azure on vahva erityisesti Microsoftin ekosysteemin integroinnissa, kuten **Active Directoryn** ja **Windows Server** -palveluiden kanssa. Se tarjoaa monipuolisia hybridipilviratkaisuja, jotka toimivat saumattomasti sekä pilvessä että paikallisissa ympäristöissä. 

Azuren palvelut, kuten **Azure DevOps** ja **Logic Apps**, helpottavat automatisointia ja jatkuvaa integraatiota. Lisäksi se on hyvä valinta organisaatioille, jotka tarvitsevat laajaa globaalien datakeskusten verkkoa. Lopuksi, Azuren tietoturvaominaisuudet, kuten **MFA** ja **ehdollinen käyttöoikeus**, ovat erittäin kehittyneitä.


## Artikkelit Azuresta

- ![alt text](/_ui/img/icons/azure_identity_s.svg) [Azure Identities and Governance](/sysadmin/azure/governance)
- ![alt text](/_ui/img/icons/azure_network_s.svg) [Azure Networking](/sysadmin/azure/network)
- ![alt text](/_ui/img/icons/azure_storage_s.svg) [Azure Storage](/sysadmin/azure/storage)
- ![alt text](/_ui/img/icons/azure_vm_s.svg) [Azure Virtual Machines](/sysadmin/azure/vms)
- ![alt text](/_ui/img/icons/azure_compute_s.svg) [Azure PaaS Compute Options](/sysadmin/azure/paas)
- ![alt text](/_ui/img/icons/azure_backup_s.svg) [Azure Backup and Data Protection](/sysadmin/azure/backup)
- ![alt text](/_ui/img/icons/azure_monitoring_s.svg) [Azure Monitoring and Logging](/sysadmin/azure/monitoring)


## Tenttiharjoituksia englanniksi

- ![alt text](/_ui/img/icons/azure_s.svg) [AZ-104 - Microsoft ESI Portal Practice Assessment](/sysadmin/azure/az104ms)
- ![alt text](/_ui/img/icons/azure_s.svg) [AZ-104 - ExamTopics](/sysadmin/azure/az104et)
