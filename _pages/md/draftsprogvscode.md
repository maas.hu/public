# ![alt text](/_ui/img/icons/vscode_m.svg) Visual Studio Code

###### .

![alt_text](vscode_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [General Settings](#2)
3. [Key Commands](#3)
4. [Source Control Support](#4)
5. [Extensions](#5)


<a name="1"></a>
## 1. Introduction

Either for learning or actively coding an any desktop platforms, one of the best tools to use is Microsoft's Visual Studio Code that you can just download from [code.visualstudio.com](https://code.visualstudio.com/download).

All the three major desktop platforms are actively maintained, and you may use any of them, it's surely fast, convenient and easy to get used to it. It's been improved and optimised over the years based on real developer feedback. It also supports hundreds of **extensions**, that you can just install and use quickly and also conveniently. What I especially like is that the program is available **in single packages** too, apart from any package managers or installers, so you can have the best control over it, allowing to maintain its version changes on your own. 

Among all the useful extensions, the **Live Server** is worth mentioning for sure. With the help of it, you can start a "live" webserver by one single click, which opens the local website on your favorite browser, and it refreshes each time you change and save something in the code. 

It's built-in source control features are also very efficient for everyday coding work which you can also enhance by furether extensions. 


<a name="2"></a>
## 2. General Settings

The Settings are available by clicking on the gears icon in the right bottom corner or by pressing the `[Ctrl+,]` key combination. 

### 2.1 Identations

In case you find it disturbing to have the thin vertical lines to show the identations, you can turn them off. You can find this setting by searching for `"editor.renderIndentGuides"`

You can even customize the identation detecting feature which switches to shorter or longer ones when detected by `"editor.detectIndentation": false,`

Controlling the default length of the identation or tabulator size can be useful too. It can be set by the `"editor.tabSize"` parameter. By default, it's `4`, but you might prefer having `2`. 

However, it's the default nowadays, but you can also control whether the tabulator identations should be real tabulator characters or the substituting spaces. I definitely prefer spaces, so let's check if the setting `"editor.insertSpaces"` is `true`.

### 2.2 Custom or Own Font

You might prefer using your favorite console type font instead of the default one. You can find for this setting by searching for `"editor.fontFamily"`. 

Here is a possible setting for it: `'Liberation Mono', Consolas, 'Courier New', monospace`

### 2.3 Your Own Color Theme

Even though VSCode offers some built-in themes and there are hundreds of free themes available on the internet, you can custom your own one. Here is how, through an example: 

1. Set the *"Tomorrow Night Blue"* theme (*"Color Theme"*)

2. Search for the `workbench.colorCustomizations` setting, then click on *"Edit in settings.json"* link

3. Edit the `settings.json` file.  
   This file exists under `c:\Users\%USERNAME%\AppData\Roaming\Code\User\` on Windows and under `.config/Code/User/` on Linux, so if you want to keep a copy of the original, you can do that there.) 

Here is an example for the relevant block in `settings.json`. The file already starts with your other custom settings but you can do your color customizations in `workbench.colorCustomizations` block:

```
[...]
    "workbench.colorCustomizations": {
        "activityBar.background": "#12518c",
        "editor.background": "#1a5994",
        "editor.foreground": "#e7edf4",
        "editor.lineHighlightBackground": "#12518c",
        "editor.selectionBackground": "#0a457e",
        "editor.selectionHighlightBackground": "#02274a",
        "editorGutter.background": "#12518c",
        "editorLineNumber.activeForeground": "#cbdae8",
        "editorLineNumber.foreground": "#9eb8d1",
        "editorWidget.background": "#12518c",
        "panel.background": "#12518c",
        "scrollbarSlider.activeBackground": "#e7edf4",
        "scrollbarSlider.background": "#3a77b0",
        "scrollbarSlider.hoverBackground": "#cbdae8",
        "sideBar.background": "#02274a",
        "statusBar.background": "#12518c",
        "tab.activeBackground": "#1a5994",
        "tab.inactiveBackground": "#02274a",
        "terminal.ansiBrightBlack": "#ad3e9f",
        "terminal.background": "#152d18",
        "terminal.foreground": "#8ddc97"
    },
[...]
```

You can read the official documentation for further [Theme Color](https://code.visualstudio.com/api/references/theme-color) customizations in a well detailed format. 

### 2.4 Further Useful Settings

It happened only once to me somewhere between the version `1.46.1` and `1.49.0`, when a Windows Update caused some troubles with hardware acceleration, when the UI had annoyingly strange colors. It was solved by just disabling hardware acceleration by setting `true` for `"disable-hardware-acceleration"`. It also requires restarting your VSCode. 

A very general thing to disable too is copy-pasting with your color scheme included. Being a programmer, you probably just need the raw text, nothing else, so turn this off by `editor.copyWithSyntaxHighlighting`.


<a name="3"></a>
## 3. Key Commands

Here you can find the most useful, general key bindings that can be used, of course, both on Windows and Linux. 

### 3.1 Quick Commenting

- inserting line commenting characters (e.g. `#` for Shell Script or `//` for JavaScript) to the beginning of one or multiple, selected lines: `[Ctrl]+[K]`, then `[Ctrl]+[C]`
- uncommenting: `[Ctrl]+[K]`, then `[Ctrl]+[U]`
- turning block comments on and off: `[Shift]+[Alt]+[A]` (e.g. `/* this is a JavaScript block comment */`)

### 3.2 Adjusting Identations

In case you have the "formatter" installed for the particular programming language you are just working with, you might often use the `[Alt]+[Shift]+[F]` key combination. This formatter is built-in in case of JavaScript. 


<a name="4"></a>
## 4. Source Control Support

VSCode has very strong Source Control Support by default, without any extensions/add-ons. In practice, it means, you can maintain your work in Git repositories, clone, create new branches, stage, commit and push changes to Git server with some clicks only instead of "exhausting" `git` commands. See the article about GitLab for more details around that: all the notes in that subject is true for both GitHub and GitLab. 

A useful setting in this field is the **Smart Commit** feature (see the settings for it below). Without it, it's expected to click on "+" sign in **Source Control** tab file-by-file or all together by the **Stage All Changes** button, which is the same as `git add .`. (Apparently, this is the expected/default behavior now.)

![alt_text](vscode_smart_commit.png)


<a name="5"></a>
## 5. Extensions

There are hundreds of very useful and tons of questionably useful extensions available that you can just directly install for VSCode. They will be installed under your home directory; under `.vscode/extensions/` in case of Linux. 

Here are my all time recommendations (as of today): 

![alt_text](vscode_extensions.png)
