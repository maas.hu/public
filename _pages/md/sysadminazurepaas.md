# ![alt text](/_ui/img/icons/azure_compute_m.svg) Azure PaaS Compute Options

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_paas_-_bg.jpg)


## Table of Contents

1. [Johdanto](#1)
2. [Azure App Service](#2)
3. [Azure Functions](#3)
4. [Azure Batch](#4)
5. [Azure Kubernetes Service (AKS)](#5)
6. [Azure Container Instances (ACI)](#6)


<a name="1"></a>
## 1. Johdanto

Azure tarjoaa useita **PaaS (Platform as a Service)** -palveluita, jotka helpottavat sovellusten hallintaa ja käyttöönottoa ilman tarvetta hallita taustalla olevaa käyttöjärjestelmää tai laitteistoa. PaaS Compute -palvelut keskittyvät erityisesti laskentakapasiteetin tarjoamiseen sovelluksille, jolloin yritykset voivat skaalata ja optimoida resurssejaan helpommin. 

Tässä artikkelissa käsitellään yleisiä huomioita yleisimmistä PaaS Compute -vaihtoehdoista, kuten **Azure App Service**, **Azure Functions**, **Azure Batch**, **Azure Kubernetes Service** ja **Azure Container Instances**. Näiden palveluiden ominaisuudet ja mahdollisuudet kehittyvät ja laajenevat nopeasti, joten ajantasaiset tiedot ja lisätiedot löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/architecture/guide/technology-choices/compute-decision-tree).


<a name="2"></a>
## 2. Azure App Service

**Azure App Service** on hallinnoitu PaaS Compute -palvelu, joka mahdollistaa web-sovellusten, mobiilisovellusten ja API:en kehittämisen ja isännöinnin. App Service hoitaa automaattisesti infrastruktuurin ylläpidon, kuten palvelimien päivitykset ja skaalauksen, ja mahdollistaa keskittymisen sovelluksen kehittämiseen.

### 2.1 App Service Plans

App Service -sovellukset ajetaan **App Service Plan** -ympäristössä, joka määrittää käytettävät resurssit ja hintatason. Suunnitelmat voidaan skaalata sekä ylös- että alaspäin käyttötarpeen mukaan. Tarjolla on useita hintatasoja, kuten **Free**, **Basic**, **Standard**, **Premium**, ja **Isolated**. Nämä määrittävät resurssien määrän ja skaalautumisominaisuudet.

### 2.2 Skaalaus

App Service Planin skaalautuvuus mahdollistaa sovellusten suorituskyvyn parantamisen. Skaalauksessa on kaksi vaihtoehtoa:

- **Skaalaus ylöspäin**: Lisätään laskentatehoa ja ominaisuuksia muuttamalla hintatasoa.
- **Skaalaus ulospäin**: Lisätään VM-instansseja manuaalisesti tai automaattisesti.

### 2.3 Hallinta ja turvallisuus

App Service -ympäristöön kuuluu myös turvallisuusominaisuuksia, kuten SSL-sertifikaattien lisääminen ja pääsynhallinta **Azure Active Directoryn** avulla. Sovellukset voidaan suojata myös **Identity Provider** -ratkaisuilla.


<a name="3"></a>
## 3. Azure Functions

**Azure Functions** on tapahtumapohjainen PaaS Compute -palvelu, jonka avulla voidaan ajaa koodia ilman, että on tarpeen hallita palvelimia. Azure Functions on erityisen hyödyllinen lyhytaikaisille, tapahtumapohjaisille prosesseille.

### 3.1 Tapahtumapohjaisuus

Azure Functions käynnistyy määritellyn tapahtuman, kuten HTTP-pyynnön tai aikataulutetun ajon perusteella. Tämä mahdollistaa sovellusten osien ajamisen vain tarvittaessa, mikä voi merkittävästi alentaa kustannuksia.

### 3.2 Skaalautuvuus

Azure Functions skaalautuu automaattisesti kuormituksen perusteella. Tämä tarkoittaa, että kuorman kasvaessa uusia instansseja luodaan automaattisesti ilman manuaalista väliintuloa.

### 3.3 Soveltuvuus ja käyttötapaukset

Azure Functions on ihanteellinen valinta esimerkiksi taustatyön ajamiseen, tiedostojen käsittelyyn tai mikroprosesseille, joissa palvelimien jatkuva ylläpito ei ole tarpeellista.


<a name="4"></a>
## 4. Azure Batch

**Azure Batch** on PaaS Compute -palvelu, joka on suunniteltu erityisesti suurien laskentatyökuormien käsittelyyn. Se on ihanteellinen valinta korkean suorituskyvyn laskentaympäristöihin (HPC), kuten tieteelliseen tutkimukseen tai data-analyysiin.

### 4.1 Automatisointi ja ajastaminen

Azure Batch mahdollistaa laskentatehtävien automaattisen ajastamisen ja jakamisen monien virtuaalikoneiden kesken. Tämä mahdollistaa suurien prosessointityömäärien jakamisen ja ajamisen rinnakkain.

### 4.2 Skaalautuvuus

Azure Batch tukee suurten laskentakuormien skaalausta ja jakaa tehtävät automaattisesti käytettävissä olevien VM-instanssien kesken, mikä varmistaa tehokkaan prosessoinnin ja resurssien käytön.


<a name="5"></a>
## 5. Azure Kubernetes Service (AKS)

**Azure Kubernetes Service (AKS)** on hallinnoitu Kubernetes-orkestrointipalvelu, jonka avulla voit ajaa ja hallita konttisovelluksia laajamittaisesti. Kubernetes on markkinoiden johtava standardi konttien hallintaan ja orkestrointiin, ja AKS tuo tämän alustan helposti hallittavaksi Azure-ympäristöön.

### 5.1 Kubernetesin hallinta

AKS poistaa Kubernetes-klusterien hallinnan monimutkaisuuden, sillä Azure hoitaa taustalla olevaa infrastruktuuria ja mahdollistaa keskittymisen konttisovellusten orkestrointiin. AKS tukee myös **Azure Container Registry** -integraatiota, mikä helpottaa konttien hallintaa ja käyttöönottoa.

### 5.2 Skaalautuvuus

AKS-klusterit voidaan skaalata ylös- ja alaspäin sovelluksen kuormituksen mukaan. AKS tukee myös **autoskaalausta**, mikä tarkoittaa, että klusterin resurssit kasvavat ja pienenevät automaattisesti käyttöasteen mukaan.

### 5.3 Orkestrointi ja käyttötapaukset

AKS on ihanteellinen valinta monimutkaisten mikropalveluarkkitehtuurien hallintaan, joissa vaaditaan konttien tehokasta orkestrointia ja skaalautuvuutta.


<a name="6"></a>
## 6. Azure Container Instances (ACI)

**Azure Container Instances (ACI)** on PaaS Compute -palvelu, jonka avulla voit ajaa kontteja ilman monimutkaista orkestrointia tai palvelininfrastruktuurin hallintaa. ACI tarjoaa nopean ja yksinkertaisen tavan käynnistää ja hallita kontteja pilviympäristössä.

### 6.1 Container Groupit

ACI tukee **Container Groupien** luomista, mikä mahdollistaa useiden konttien ajamisen rinnakkain ja niiden välisten riippuvuuksien hallinnan. Tämä tekee ACI:sta joustavan valinnan monimutkaisten sovellusten jakamiseen kontteihin.

### 6.2 Yhdistettävyys ja käyttötapaukset

ACI mahdollistaa sovellusten suoran yhdistämisen muihin Azure-palveluihin, kuten tietokantoihin tai App Service -palveluihin. Tämä tekee siitä kätevän valinnan pienille ja keskisuurille mikropalveluarkkitehtuureille.
