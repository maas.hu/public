# ![alt text](/_ui/img/icons/python_m.svg) Python

<div style="text-align: right;"><h7>Posted: 2023-02-27 22:28</h7></div>

###### .

![alt_text](python_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Environment Setup](#2)


<a name="1"></a>
## 1. Introduction

Python is consistently ranked as one of the most popular programming languages. It first entered the **Top 10 in Q2 2002**, climbed into the **Top 3 by Q1 2014**, and has held the position of **Most Popular Programming Language since Q4 2018**, at least as of March 2025. [Here](https://www.youtube.com/watch?v=u8FgXP2z_KE) is one reference supporting this.

<img src="guido.jpg" class="right">

The language was originally created by the Dutch programmer **Guido van Rossum**. As a successor to the **ABC programming language**, the first `0.9.0` version of Python was released on **February 20, 1991**.

This article focuses on **Python 3**, commonly referred to as **Python3**.

Python3 is an **object-oriented scripting language**. As a **scripting language**, it uses an **interpreter**, meaning it executes code **immediately** rather than compiling it into binary form. Python3 supports **both dynamic and strong typing**, includes functional programming concepts such as *map*, *reduce*, and *filter*, and features a **clean, pseudo-code-like syntax**, where **whitespace is used for code blocks** instead of braces or keywords.

Its rapid rise in popularity can be attributed to several factors:

- While its primary platform remains **Unix-like systems** (especially Linux), Python is **open-source**, **cross-platform**, and **architecture-independent**.
- Its **readable, pseudo-code-like syntax** enforces proper **indentation**, making it easy to write and maintain.
- It is an excellent **teaching language**, originally designed for learning—similar to Pascal.
- It is widely used in **high-demand fields** such as **Data Science, DevOps, and Web Development**, thanks to its efficiency and flexibility.

### 1.1 Legacy vs. Present vs. Future

It is common for a system to have multiple Python versions installed, which can sometimes be confusing for users. For example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ls -l $(which python python2 python3 python3.{1..99} 2>/dev/null)</span>
-rwxr-xr-x 1 root root 28643224 Sep 20 09:56 /opt/Python3/bin/python3.12
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python -> python2.7
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python2 -> python2.7
lrwxrwxrwx 1 root root       10 Dec  4 23:40 /usr/bin/python3 -> python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 23:23 /usr/bin/python3.10
-rwxr-xr-x 1 root root    14448 Dec  4 23:40 /usr/bin/python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 21:51 /usr/bin/python3.9
</pre>

In short, **Python 2.x is legacy**, while **Python 3.x is the standard and future of the language**. Many legacy applications still rely on Python 2.x, which is why its final release, **Python 2.7.18 (April 20, 2020)**, continues to receive **occasional security updates** (e.g., `2.7.18-28.1` in January 2023). However, no new features will be added.

For details on the transition from Python 2 to Python 3, refer to the official documentation: [What’s New In Python 3.0](https://docs.python.org/3/whatsnew/3.0.html). It's also worth noting that since Python **3.8.0**, new **minor versions** are released every **October**.

### 1.2 Leaving the System-Default Python Alone

Python can be installed in various ways. The most straightforward method is to use the official installers provided by the Python developers or, in the case of Linux distributions, install it via the package manager.

On Linux, a **default system-wide Python installation** is always present, as many core system components **depend on it**. For example, on **RHEL 9**, the `dnf` package manager (written in Python) has a **hardcoded shebang** pointing to `/usr/bin/python3.9`.

As documented in [Red Hat’s official guide](https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/installing_and_using_dynamic_programming_languages/assembly_introduction-to-python_installing-and-using-dynamic-programming-languages#assembly_introduction-to-python_installing-and-using-dynamic-programming-languages):

>*Python 3.9 is the default Python implementation in RHEL 9. Python 3.9 is distributed in a non-modular `python3` RPM package in the BaseOS repository and is usually installed by default. Python 3.9 will be supported for the whole life cycle of RHEL 9.*

>*(...)*

>*In RHEL 9, the **unversioned** form of the `python` command points to the default Python 3.9 version and it is an equivalent to the `python3` and `python3.9` commands. In RHEL 9, you cannot configure the unversioned command to point to a different version than Python 3.9.*

Fortunately, major Linux distributions provide **multiple minor versions** of Python in their repositories, ensuring users have access to different versions. On RHEL 9-based systems, for example, users can install additional Python **3.9 components** as well as newer versions such as **Python 3.11 and Python 3.12**.


<a name="2"></a>
## 2. Environment Setup

Installing a relatively up-to-date version of Python3 is straightforward using the package manager. For example, on **openSUSE**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo zypper install python312</span>
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following 4 recommended packages were automatically selected:
  python312 python312-curses python312-dbm python312-pip

The following 12 NEW packages are going to be installed:
  libpython3_12-1_0 libpython3_12-1_0-x86-64-v3 python312 python312-base python312-base-x86-64-v3 python312-curses python312-dbm python312-gobject python312-gobject-cairo
  python312-pip python312-pycairo python312-x86-64-v3

12 new packages to install.

Package download size:    18.5 MiB

Package install size change:
              |      70.5 MiB  required by packages that will be installed
    70.5 MiB  |  -      0 B    released by packages that will be removed

Backend:  classic_rpmtrans
<span class="psu">Continue? [y/n/v/...? shows all options] (y): </span> <span class="cmd">y</span>
[...]
</pre>

To provide **more flexibility**, this article will guide you through **compiling the latest Python3 version from source**.

### 2.1 Installing Prerequisites for Compiling Python3 from Source

When compiling Python3 on **Red Hat** or its derivatives like **Rocky Linux**, you need to install the **"Development Tools"** package group. This includes essential build tools such as `gcc` and `make`, along with other necessary dependencies.

Below is the size of this package group on **Rocky Linux 9.5**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf groupinstall -y "development tools"</span>
[...]
Install  364 Packages
Upgrade    3 Packages

Total download size: 274 M
[...]
Complete!
</pre>

In addition to the core development tools, the following **libraries and headers** are required for compiling Python3:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install -y bzip2-devel expat-devel libffi-devel ncurses-devel openssl-devel readline-devel sqlite-devel tk-devel atk-devel xz-devel zlib-devel</span>
[...]
Install  44 Packages
Upgrade   4 Packages

Total download size: 21 M
[...]
Complete!
</pre>

### 2.2 Downloading the Source Code

The official Python source code can be downloaded from [python.org](https://www.python.org). As of **March 2025**, the latest release is **Python 3.13.2**.

![alt_text](python_-_src_download.jpg)

Navigate to **[Downloads → Source Code](https://www.python.org/downloads/source/)**, **copy the link** of the *"Gzipped source tarball"*, and use `wget` to download it to your system:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cd /usr/src</span>

<span class="psr">#</span> <span class="cmd">wget https://www.python.org/ftp/python/3.13.2/Python-3.13.2.tgz</span>

<span class="psr">#</span> <span class="cmd">tar -xzf Python-3.13.2.tgz</span>

<span class="psr">#</span> <span class="cmd">cd Python-3.13.2</span>
</pre>

### 2.4 Configuring and Compiling Python3

There are multiple ways to install Python3 from source. The most common approach is using `altinstall`, which installs Python under `/usr/local`. However, this method does not allow precise control over the installation path.

A **more structured approach** is to specify a custom **installation prefix** (`--prefix`), ensuring that Python is installed in a dedicated directory like `/opt/Python3`. This approach provides **portability** and **does not interfere** with system-wide Python installations.

#### 2.4.1 Using a Custom `--prefix` Directory (Preferred)

This method installs Python into `/opt/Python3`, creating a **standalone Python environment**.

1. **Ensure `/opt/Python3` is free to use** (rename an existing instance if necessary). Below, `/opt` is empty:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ls -la /opt</span>
    total 8
    drwxr-xr-x.  2 root root 4096 Nov  3 03:29 .
    dr-xr-xr-x. 19 root root 4096 Jan 30 16:26 ..
    </pre>
2. **Run the configuration script** with optimizations enabled and specify `/opt/Python3` as the target directory:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">./configure --enable-optimizations --prefix /opt/Python3</span>
    [...]
    config.status: creating pyconfig.h
    configure: creating Modules/Setup.local
    configure: creating Makefile
    </pre>
3. **Compile the source code** (this step can take several minutes):  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">make</span>
    Running code to generate profile data (this can take a while):
    [...]
    Checked 112 modules (33 built-in, 75 shared, 1 n/a on linux-x86_64, 0 disabled, 3 missing, 0 failed on import)
    make[1]: Leaving directory '/usr/src/Python-3.13.2'
    </pre>
4. **Install Python into `/opt/Python3`**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">make install</span>
    Creating directory /opt/Python3/bin
    Creating directory /opt/Python3/lib
    [...]
    Successfully installed pip-24.3.1
    </pre>
5. **Verify the installation**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">du -sh /opt/Python3</span>
    390M	/opt/Python3

    <span class="psr">#</span> <span class="cmd">/opt/Python3/bin/python3 --version</span>
    Python 3.13.2
    </pre>

To integrate `/opt/Python3` into your environment, add it to your `PATH`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep PATH .bashrc</span>
export PATH=$HOME/bin:$PATH:/opt/Python3/bin
</pre>

#### 2.4.2 Using `altinstall` (Simpler Approach)

If you prefer a simpler method, `altinstall` ensures that Python is installed under `/usr/local`, keeping system-installed Python versions untouched.

<pre class="con">
<span class="psr">#</span> <span class="cmd">./configure --enable-optimizations</span>
[...]
config.status: creating pyconfig.h
configure: creating Modules/Setup.local
configure: creating Makefile

<span class="psr">#</span> <span class="cmd">make altinstall</span>
[...]

<span class="psr">#</span> <span class="cmd">python3.13 --version</span>
Python 3.13.2
</pre>

### 2.5 Configuring PIP for Your Regular User

To ensure PIP works properly, update your system's **secure_path** for `sudo` to include either your custom directory (`/opt/Python3/bin`) or `/usr/local/bin` (if you used `altinstall`).

1. **Modify `secure_path` in `/etc/sudoers`** (example for `/opt/Python3`):  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">vi /etc/sudoers</span>
    [...]
    Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/opt/Python3/bin
    [...]
    </pre>
2. **Upgrade PIP globally**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">pip3.13 install --upgrade pip</span>
    Requirement already satisfied: pip in /opt/Python3/lib/python3.13/site-packages (24.3.1)
    Collecting pip
      Downloading pip-25.0.1-py3-none-any.whl.metadata (3.7 kB)
    Downloading pip-25.0.1-py3-none-any.whl (1.8 MB)
      ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.8/1.8 MB 9.9 MB/s eta 0:00:00
    Installing collected packages: pip
    Successfully installed pip-25.0.1
    </pre>

### 2.6 Packaging for Portability

If you used `--prefix`, you might want to **package the entire installation** (`/opt/Python3`) into a `.tar.gz` archive, making it portable across different systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">PKG="python-$(/opt/Python3/bin/python3 --version | awk '{print $2}')-lin-x64" ; \
  tar -cf $PKG.tar Python3 ; \
  sync ; \
  gzip $PKG.tar ; \
  du -h $PKG* | awk '{print $2" is ready, "$1"B"}'</span>
python-3.13.2-lin-x64.tar.gz is ready, 113MB
</pre>

...
