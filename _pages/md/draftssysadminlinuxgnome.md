# ![alt text](/_ui/img/icons/gnome_m.svg) GNOME

###### .

>Szerkesztés alatt...

## Tartalomjegyzék

1. [Bevezető](#1)
2. [Extension-ök és tweak-ek](#2)
3. [Hasznos GNOME toolok](#3)
4. [`gsettings` és `dconf-editor`](#4)

...

<a name="1"></a>
## 1. Bevezető

A **GNOME** elnevezés egy betűszó; eredetileg a ***GNU Network Object Model Environment***-et jelentette, ám mára ennek pontossága vitatott, így sokszor már csak sima névként, Gnome-ként találkozhatunk vele. A GNOME az egyik legelterjedtebb nyílt forrású grafikus felhasználói felület Linuxhoz és egyéb Unix-szerű operációs rendszerekhez. A **GNU Project** *("GNU's Not Unix")* hivatalos és számos Linux-disztribúció alapértelmezett felülete (pl. a Fedoráé is). 

![alt_text](desktop-202201.jpg "")

<a name="2"></a>
## 2. Extension-ök és tweak-ek

Korábban az Extension-ök is a Gnome Tweaks tool egyik menüjében helyezkedtek el; jelenleg (2022. január) nem. Tehát mostanra e két dolog különvált egymástól. 

![alt_text](extension_-_extensions-tweaks.png "")

### 2.1 Tweak-ek

Az alapvető megjelenésbeli és funkcióbeli GNOME beállítások tartoznak ide.

Régen ezt a toolt (`gnome-tweaks`) külön kellett telepíteni, de már évek óta minden disztribúció alapértelmezés szerint tartalmazza, hiszen kb. nem lehet élni nélküle. Indítása a fent ismertetett módon, a [Super] gombra való nyomás után a "tweaks" begépelésével behozható. 

### 2.1.1 Ablakok címsorának gombjai

Az első és legfontosabb &ndash; szerintem &ndash;, hogy visszakapcsoljuk az ablakok címsorához a "minimize" és "maximize" gombokat is: 

![alt_text](tweak_-_windows_titlebars.png "")

### 2.1.2 A Top Bar

Érdemes lehet (tehát ez csak ajánlott) a naptárban a hetek számozását is bekapcsolni, de az akksi pontos százalékos megjelenése is itt van: 

![alt_text](tweak_-_top_bar.png "")

### 2.1.3 Témák

Ez talán a használatbavételkor leginkább látogatott rész, hiszen ezekkel a külön telepíthető témákkal tehetjük egyedivé az ablakkezelőnket igazán. 

A [gnome-look.org](http://gnome-look.org) oldalról tölthetjük le a csak a címsort, gombokat stb.-t érintő **GTK3/4** témákat vagy a mégalaposabb **Gnome Shell** témákat.

Néhány szerintem elfogadható GTK3/4 téma, amikre érdemes itt keresni és kipróbálni: "**Ultimate Maia**", "**Fluent**", "**Yaru**" (ezekből is a zöld/cián változatokkal szoktam próbálkozni csak). 

A letöltött csomagok tartalmából a nekünk kívánatos könyvtárát az `/usr/share/themes` alá kell kibontanunk root-ként (jogosultságok ellenőrzésével): 

```ksh
# cd /usr/share/themes

# ls -la
total 52
dr-xr-xr-x  11 root root  4096 Jan 27 00:33 .
dr-xr-xr-x 270 root root 12288 Jan 26 23:52 ..
drwxr-xr-x   4 root root  4096 Jan  5 18:07 Adwaita
drwxr-xr-x   4 root root  4096 Jan  5 18:07 Adwaita-dark
drwxr-xr-x   4 root root  4096 Jan 15 22:47 Default
drwxr-xr-x   4 root root  4096 Jan 15 22:47 Emacs
drwxr-xr-x  10 root root  4096 Jan 15 04:19 Fluent-Green
drwxr-xr-x   4 root root  4096 Jan  5 18:07 HighContrast
drwxr-xr-x   3 root root  4096 Dec 31 12:53 Raleigh
drwxr-xr-x   5 root root  4096 May 11  2020 Ultimate-Maia-Green
drwxr-xr-x   8 root root  4096 May  2  2021 Yaru-Green
```

A saját home-unk alá is meg lehet ezt oldani természetesen ((talán) a `.local/share/` alatt). 

![alt_text](tweak_-_appearance.png "")

Hasznos lehet még, hogy itt tudjuk ki-/bekapcsolni a bejelentkezéskor automatikusan elinduló dolgokat is (pl. a Skype is beteszi magát ide telepítés után): 

Egy kicsit kapcsolódik: a GNOME Terminal témáját, színeit sem árt személyre szabni. Pl. ezekkel a színekkel: 

- BG: `#1F3623`
- FG: `#C5F1BE`

### 2.1.4 Automatikusan elinduló alkalmazások

Ez a felhasználó `.config/autostart` könyvtára alatti `.desktop` fájlok szintjén is konfigurálható (pl. egyszerűen átnevezni ott a `.desktop` fájlt `.desktop.disabled`-re), de itt kényelmesebb.

![alt_text](tweak_-_startup_applications.png "")

### 2.2 Extension-ök

Ezekre egy nagyon kényelmes megoldást is kínál az [extensions.gnome.org](https://extensions.gnome.org/) weboldal: A "**GNOME Shell Integration**" Firefox-os extension telepísével magán a weboldalon kapcsolhatjuk ki-/be és konfigurálhatjuk ezeket. Egy ilyen vezérlő lesz így látható és használható minden extension mellett a weboldalon: 

![alt_text](extension_-_web_button.png "")

Az így telepített extension-ök a felhasználó `.local/share/gnome-shell/extensions/` könyvtára alá kerülnek. Ezeket root-ként át is pakolhatjuk a gyári extension-ök mellé, ha nagyon beválnak, de alaposan győződjünk meg arról, hogy a jogosultságok ott rendben vannak és a könyvtár neve is az extension hivatalos neve: 

Példa a gyári extension-ös könyvtár tartalmára: 

```ksh
# cd /usr/share/gnome-shell/extensions/

# ls -la
total 36
drwxr-xr-x 9 root root 4096 Jan 26 23:17 .
drwxr-xr-x 6 root root 4096 Jan 23 14:44 ..
drwxr-xr-x 2 root root 4096 Jan  5 18:08 apps-menu@gnome-shell-extensions.gcampax.github.com
drwxr-xr-x 5 root root 4096 Jan 26 23:10 dash-to-dock@micxgx.gmail.com
drwxr-xr-x 4 root root 4096 Jan 26 22:49 desktopicons-neo@darkdemon
drwxr-xr-x 2 root root 4096 Jan  5 18:08 launch-new-instance@gnome-shell-extensions.gcampax.github.com
drwxr-xr-x 2 root root 4096 Jan  5 18:08 places-menu@gnome-shell-extensions.gcampax.github.com
drwxr-xr-x 2 root root 4096 Jan  5 18:08 window-list@gnome-shell-extensions.gcampax.github.com
drwxr-xr-x 2 root root 4096 Jan  5 18:08 workspace-indicator@gnome-shell-extensions.gcampax.github.com
```

Az egyik ilyen "kívülről" érkező extension nálam a "dash-to-dock" volt. Ennek a pontos neve mindig az adott könyvtár `metadata.json` fájljában, az "uuid" mezőben van. Továbbá a `.js` fájloknak is futtathatóknak kell lenniük (egy `find` paranccsal pillanatok alatt be tudjuk ezt állítani): 

```ksh
# grep uuid dash-to-dock@micxgx.gmail.com/metadata.json 
  "uuid": "dash-to-dock@micxgx.gmail.com",

# ls -l dash-to-dock@micxgx.gmail.com/*js
-rwxr-xr-x 1 root root 36256 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/appIconIndicators.js
-rwxr-xr-x 1 root root 54222 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/appIcons.js
-rwxr-xr-x 1 root root 40039 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/dash.js
-rwxr-xr-x 1 root root 11118 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/dbusmenuUtils.js
-rwxr-xr-x 1 root root 88616 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/docking.js
-rwxr-xr-x 1 root root   502 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/extension.js
-rwxr-xr-x 1 root root  5087 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/fileManager1API.js
-rwxr-xr-x 1 root root 10754 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/intellihide.js
-rwxr-xr-x 1 root root  9438 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/launcherAPI.js
-rwxr-xr-x 1 root root 23119 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/locations.js
-rwxr-xr-x 1 root root 42510 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/prefs.js
-rwxr-xr-x 1 root root 20778 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/theming.js
-rwxr-xr-x 1 root root 12527 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/utils.js
-rwxr-xr-x 1 root root 20972 Oct 24 22:20 dash-to-dock@micxgx.gmail.com/windowPreview.js
```

Sajnos sokszor az Extensions tool nem látja azonnal ezeket, így vagy újra kell indítani a GNOME Shell-t vagy az egész gépet.

A fenti lista alapján ez látszik a toolban (néhány gyári dolog is már be van állítva, pl. az "Applications Menu"): 

![alt_text](extension_-_built-in_extensions.png "")

A két kiemelt extra extension jelen esetben a "**Desktop Icons: Neo**" és a "**Dash to Dock**" (ez utóbbi a gyári "**Window List**" kiváltására). Az alábbiakat csinálják ezek: 

- Desktop Icons: Neo: 

![alt_text](extension_-_desktop_icons.png "")

- Dash to Dock: 

![alt_text](extension_-_dash_to_dock.png "")

...

<a name="3"></a>
## 3. Hasznos GNOME toolok

...

```ksh
$ gnome-screenshot -d 5 -p
```

...

To determine if Wayland is being used: 

```ksh
$ loginctl show-session $(loginctl | awk '/seat/{print $1}') -P Type
wayland
```


<a name="4"></a>
## 4. `gsettings` és `dconf-editor`

...

```ksh
~> gsettings list-recursively org.gnome.gnome-screenshot
org.gnome.gnome-screenshot default-file-type 'png'
org.gnome.gnome-screenshot include-pointer false
org.gnome.gnome-screenshot delay 0
org.gnome.gnome-screenshot take-window-shot false
org.gnome.gnome-screenshot last-save-directory ''
org.gnome.gnome-screenshot auto-save-directory ''
org.gnome.gnome-screenshot include-icc-profile true

~> gsettings set org.gnome.gnome-screenshot include-pointer true

~> gsettings set org.gnome.gnome-screenshot auto-save-directory "~/Pictures/Screenshots"

~> gsettings list-recursively org.gnome.gnome-screenshot
org.gnome.gnome-screenshot default-file-type 'png'
org.gnome.gnome-screenshot include-pointer true
org.gnome.gnome-screenshot delay 0
org.gnome.gnome-screenshot take-window-shot false
org.gnome.gnome-screenshot last-save-directory ''
org.gnome.gnome-screenshot auto-save-directory '~/Pictures/Screenshots'
org.gnome.gnome-screenshot include-icc-profile true
```

Alapértelmezés szerint a [PrtSc] billentyű nem a `gnome-screenshot` programot hívja meg (valószínűleg), emiatt ezt ki kell erőszakolni belőle. Ezt a GNOME Settings ablakában tehetjük meg: 

![alt_text](screenshot_button.png "")

...

Nem kapcsolódik (még) egyik fejezethez sem, de GNOME Wayland alatt ha az elavult alapokon nyugvó MS Teams helyett azt a Chrome alatt használjuk, képernyőmegosztáskor csak fekete képernyőt osztunk meg. Ennek elkerülésére a `webrtc-pipewire-capturer` rejtett feature-t kell engedélyeznünk: [chrome://flags/#enable-webrtc-pipewire-capturer](chrome://flags/#enable-webrtc-pipewire-capturer)
