# ![alt text](/_ui/img/icons/azure_backup_m.svg) Azure Backup and Data Protection

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_backup_-_bg.jpg)


## Table of Contents

1. [Johdanto](#1)
2. [Azure Backupin Yleiskatsaus](#2)
3. [Recovery Services -holvi](#3)
4. [Virtuaalikoneiden Varmuuskopiointi](#4)
5. [On-premises Varmuuskopiointi](#5)
6. [Azure Site Recovery](#6)


<a name="1"></a>
## 1. Johdanto

**Azure Backup** tarjoaa yksinkertaisen, turvallisen ja kustannustehokkaan ratkaisun tietojen varmuuskopiointiin ja palauttamiseen. Sen avulla voidaan suojata sekä pilvessä että paikallisympäristössä olevia resursseja, kuten virtuaalikoneita, tiedostojakoja ja sovelluksia. Lisäksi Azure Site Recovery mahdollistaa hätätilanteiden varalta työkuormien replikoinnin ja nopean toipumisen. 

Tämä artikkeli tarjoaa yleiskatsauksen Azure Backupin ja datan suojauksen avainominaisuuksiin. Lisätietoja ja ajantasaiset tiedot löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/backup/backup-overview).


<a name="2"></a>
## 2. Azure Backupin Yleiskatsaus

Azure Backup tukee useiden erilaisten työkuormien suojaamista ja tarjoaa tehokkaita varmuuskopiointiratkaisuja sekä pilvi- että paikallisiin ympäristöihin.

### 2.1 Varmuuskopiointipolitiikat ja tallennusvaihtoehdot

Varmuuskopiointipolitiikat määrittelevät varmuuskopioiden ottamisen tiheyden ja säilytysajan. Azure Backup tarjoaa seuraavat tallennusvaihtoehdot:
- **Locally-redundant Storage (LRS)**: Kolme kopiota yhdessä datakeskuksessa.
- **Zone-redundant Storage (ZRS)**: Kolme kopiota eri vyöhykkeissä samalla alueella.
- **Geo-redundant Storage (GRS)**: Kopiot kahdessa eri alueella, mikä lisää vikasietoisuutta.

### 2.2 Automaattinen tietojen suojaus

Azure Backupin merkittäviä etuja ovat:

- Automaattinen tallennuksen hallinta
- Rajoittamaton datan siirto
- Sovellustietoiset varmuuskopiot
- Pitkäaikainen säilytys ja joustava palautus
- Datan salaaminen sekä siirron aikana että levossa


<a name="3"></a>
## 3. Recovery Services -holvi

**Recovery Services -holvi** on keskeinen komponentti Azure Backup -palvelussa. Se hallinnoi varmuuskopioita ja tarjoaa erilaisia suojausominaisuuksia.

### 3.1 Holvin ominaisuudet

Kun luot uuden Recovery Services -holvin, voit valita sen redundanssiasetukset (LRS, ZRS tai GRS) ja ottaa käyttöön immutability-toiminnon, joka estää varmuuskopioiden muuttamisen.

### 3.2 Holvin hallinta

Voit hallita varmuuskopiointipolitiikkoja ja palautusprosesseja holvin kautta. Holvi tukee myös **soft delete** -ominaisuutta, joka estää tahattoman datan poistamisen säilyttämällä poistetut varmuuskopiot enintään 14 päivän ajan.


<a name="4"></a>
## 4. Virtuaalikoneiden Varmuuskopiointi

Azure Backup tukee virtuaalikoneiden kattavaa suojausta, mukaan lukien sovellustietoiset varmuuskopiot.

### 4.1 Varmuuskopioinnin hallinta

Voit luoda varmuuskopiointipolitiikan, joka määrittää kuinka usein varmuuskopiot otetaan ja kuinka kauan niitä säilytetään. Virtuaalikoneiden varmuuskopiointi voi sisältää koko koneen tai yksittäisten tiedostojen palautuksen.

### 4.2 Palautusvaihtoehdot

Virtuaalikoneiden palautuksessa voit valita, haluatko palauttaa koko koneen uuteen sijaintiin, korvata olemassa olevan koneen tai palauttaa yksittäisiä tiedostoja. Tämä tarjoaa joustavan ja nopean palautusratkaisun.

### 4.3 Soft delete -toiminto

Ennen kuin voit poistaa käyttämättömän **Recovery Services Vaultin**, sinun on ensin [poistettava käytöstä soft delete -suojaus](https://learn.microsoft.com/en-us/azure/backup/backup-azure-security-feature-cloud?tabs=azure-portal#permanently-deleting-soft-deleted-backup-item) holvista. Tämä sisältää seuraavat vaiheet:

1. Varmuuskopioinnin poistaminen käytöstä
2. Suojattujen kohteiden poistaminen
3. Holvin poistaminen

### 4.4 Restore Point Collection -resurssit

Kun virtuaalikoneen ensimmäinen varmuuskopiointi suoritetaan Azure Backupin avulla, Azure luo automaattisesti uuden resurssiryhmän nimeltä `AzureBackupRG_<alue>`. Tämä resurssiryhmä sisältää tiettyjä varmuuskopiointiin ja palautukseen liittyviä resursseja, kuten **Restore Point Collection** -resursseja. Nämä resurssit ovat olennaisia, sillä ne tallentavat varmuuskopion tilannevedoksia ja mahdollistavat tietojen palautuksen tarvittaessa.

*Restore Point Collection* -resurssit luodaan joka kerta, kun otetaan varmuuskopio virtuaalikoneesta. Ne pitävät kirjaa kaikista kyseisen virtuaalikoneen palautuspisteistä ja tallentavat tietoja esimerkiksi levyjen tilannevedoksista. Nämä palautuspisteet mahdollistavat joko koko virtuaalikoneen tai yksittäisten tiedostojen ja levyjen palauttamisen.

**Tärkeää huomioitavaa:**
- Restore Point Collection -resurssit eivät ole tarkoitettu suoraan hallittaviksi, eli niitä ei tulisi manuaalisesti muokata tai poistaa.
- Palautustoimenpiteet, kuten virtuaalikoneen tai yksittäisten levyjen palautus, tulee tehdä Azure Backup -palvelun kautta, ei suoraan Restore Point Collection -resursseista.

**Käyttö palautustilanteessa:**
- Restore Point Collection -resurssit toimivat taustalla osana Azure Backup -järjestelmää, ja ne ovat tärkeitä varmistamaan, että palautuspisteet säilyvät ja ovat käytettävissä.
- Palautuksen yhteydessä ei tarvitse suoraan käsitellä näitä resursseja, sillä kaikki palautustoimenpiteet suoritetaan **Recovery Services Vault** -palvelun kautta.

Voit tarkastella Restore Point Collection -resursseja käyttämällä **Azure CLI** -komentoa, esim.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az resource list --resource-group AzureBackupRG_westeurope_1 --resource-type Microsoft.Compute/restorePointCollections --output table</span>
Name                                               ResourceGroup               Location    Type                                       Status
-------------------------------------------------  --------------------------  ----------  -----------------------------------------  --------
AzureBackup_log-central-eu_vm_1843080556391456570  AzureBackupRG_westeurope_1  westeurope  Microsoft.Compute/restorePointCollections
AzureBackup_netbox-prod_vm_738873827354707         AzureBackupRG_westeurope_1  westeurope  Microsoft.Compute/restorePointCollections
</pre>

Tämä komento listaa kaikki kyseisen resurssiryhmän Restore Point Collection -resurssit ja tarjoaa yleiskuvan siitä, milloin varmuuskopiot ja palautuspisteet on luotu.


<a name="5"></a>
## 5. On-premises Varmuuskopiointi

Azure Backup tukee myös paikallisten tiedostojen varmuuskopiointia **Microsoft Azure Recovery Services (MARS) -agentin** avulla.

### 5.1 MARS-agentin ominaisuudet

**MARS-agentti** mahdollistaa tiedostojen ja kansioiden suojauksen suoraan Azureen ilman erillistä varmuuskopiointipalvelinta. Se on kevyt agentti, joka integroituu suoraan Azure Backup -ratkaisuun.

### 5.2 Rajoitukset

Vaikka MARS-agentti tarjoaa monia etuja, se ei ole sovellustietoinen ja tukee vain Windows-ympäristöjä. Lisäksi Linux-ympäristöissä tarvitaan muita ratkaisuja.


<a name="6"></a>
## 6. Azure Site Recovery

Azure Site Recovery mahdollistaa kriittisten palveluiden ja työkuormien replikoinnin ja palauttamisen hätätilanteissa.

### 6.1 Replikointi ja failover

Site Recovery mahdollistaa virtuaalikoneiden replikoinnin eri Azure-alueiden välillä tai paikallisesta ympäristöstä Azureen. Hätätilanteessa voit suorittaa failoverin toiseen sijaintiin ja jatkaa toimintoja mahdollisimman vähäisin seisokein.

### 6.2 Palautussuunnitelmat

Voit luoda palautussuunnitelmia, jotka automatisoivat failover-prosessin. Tämä vähentää seisokkiaikaa ja varmistaa liiketoiminnan jatkuvuuden. Azure Site Recovery on tärkeä osa kattavaa varmuuskopiointi- ja palautusstrategiaa.

Lisätietoja ja ajantasaiset ohjeet löydät osoitteesta learn.microsoft.com.
