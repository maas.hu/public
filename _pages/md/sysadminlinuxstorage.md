# ![alt text](/_ui/img/icons/babytux_m.svg) Linux Storage

<div style="text-align: right;"><h7>Posted: 2025-03-12 15:53</h7></div>

###### .

![alt_text](linux_-_storage_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Partitions](#2)
3. [Filesystems](#3)
4. [LVM](#4)
5. [RAID](#5)
6. [LUKS Encryption](#6)
7. [Disk Performance Testing and Tuning](#7)


<a name="1"></a>
## 1. Introduction

Linux storage management has evolved significantly since the early days of Unix-based systems. Initially, disk storage was managed with simple partitions and filesystem-based approaches. Over time, more sophisticated methods such as **Logical Volume Manager** (**LVM**), **RAID** configurations, and **network storage** solutions have been introduced, making Linux a flexible and powerful choice for handling a wide range of storage needs. 

Today, Linux is widely used in enterprise environments, cloud infrastructures, and personal computing, offering robust support for various storage technologies.

### 1.1 Linux Disk Management and the `/dev` Directory  

One of the core principles of Linux storage is that **everything is represented as a file**. This means that storage devices, partitions, and even virtual block devices appear under the `/dev` directory. The `/dev` directory contains special files that represent physical and logical devices, allowing users and system processes to interact with storage using standard file operations.

For example, typical block devices such as hard drives and SSDs appear in `/dev` with specific naming conventions. These device files allow system utilities like `fdisk`, `lsblk`, and `blkid` to query and manage storage devices directly.

### 1.2 Evolution of Disk Identifiers in Linux  

Disk identifiers in Linux have changed over time as storage technologies evolved:

- **IDE Hard Drives (Legacy, pre-SATA era)**  
    Traditionally, **IDE (Integrated Drive Electronics) disks** were named using `hda`, `hdb`, `hdc`, etc., representing primary and secondary drives on the system.
- **SCSI, SATA, and USB Storage Devices**  
    With the rise of **SCSI (Small Computer System Interface)** and **SATA (Serial ATA)** drives, as well as external storage devices such as USB-connected drives and SD cards, Linux standardized device names under the `sdX` format:
    - `/dev/sda` (first SCSI/SATA/USB disk)
    - `/dev/sdb` (second disk)
    - `/dev/sdc`, and so on.
- **NVMe SSDs (High-Speed PCIe Storage)**  
    With the introduction of **NVMe (Non-Volatile Memory Express)** SSDs, a new naming scheme was adopted:
    - `/dev/nvme0n1` (first NVMe drive)
    - `/dev/nvme0n1p1` (first partition on the first NVMe drive)
    - `/dev/nvme1n1` (second NVMe drive), etc.

These naming conventions help Linux efficiently manage storage devices regardless of their underlying technology, ensuring compatibility across different hardware types. Understanding these identifiers is crucial for **disk partitioning**, **filesystem management**, and **troubleshooting** storage issues.

<div class="warn">
<p>You must always be extremely cautious when working with block devices! A simple mistake, such as using the wrong device name (e.g., specifying <code>/dev/sda</code> instead of <code>/dev/sdb</code>), can result in irreversible data loss!</p>
</div>

<a name="2"></a>
## 2. Partitions

A partition is a **logically defined section** of a physical disk that acts as an **independent storage unit**. 

**Partitioning** is an essential step in disk management, as it enables better organization of data, multi-boot configurations, and system performance optimizations.

Historically, partition management tools date back to the DOS era, with tools like **PartitionMatic** making early attempts to simplify disk partitioning. Today, Linux offers a wide variety of powerful command-line and graphical tools for managing partitions efficiently.

One of the easiest ways to gain an overview of existing partitions is by using the `df` command. While `df -hPT` provides a user-friendly summary of disk usage and file systems, we will explore file systems in more detail later.

### 2.1 Partitioning and Disk Device Management Tools for Existing Systems

Linux provides multiple utilities to inspect, manage, and modify partitions. These tools range from simple listing utilities to powerful partition editors.

#### 2.1.1 `lsscsi`

The `lsscsi` command lists all **SCSI devices** and their associated block devices. It is primarily used for identifying attached **SCSI, SATA, and NVMe devices**, displaying their logical paths in `/dev/`.

For example, running `lsscsi` might produce the following output:

<pre class="con">
<span class="psu">$</span> <span class="cmd">lsscsi</span>
[0:0:0:0]    disk    ATA      VBOX HARDDISK    1.0   /dev/sda 
[1:0:0:0]    cd/dvd  VBOX     CD-ROM           1.0   /dev/sr0 
</pre>

Here:

- `/dev/sda` represents a virtual hard disk.
- `/dev/sr0` is a virtual CD/DVD-ROM drive.

This tool is particularly useful in environments where multiple disk devices are present, making it easier to distinguish between them.

#### 2.1.2 `lsblk`

The `lsblk` command provides a **tree-like representation** of storage devices and their partitions. Unlike `lsscsi`, it offers detailed information about **mount points, partition relationships, and logical volume mappings**.

Example output:

<pre class="con">
<span class="psu">$</span> <span class="cmd">lsblk</span>
NAME              MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                 8:0    0   30G  0 disk 
├─sda1              8:1    0   64M  0 part /boot/efi
├─sda2              8:2    0  512M  0 part /boot
└─sda3              8:3    0 29.4G  0 part 
  ├─rootvg-rootlv 253:0    0 25.4G  0 lvm  /
  └─rootvg-swaplv 253:1    0    4G  0 lvm  [SWAP]
sr0                11:0    1 1024M  0 rom  
</pre>

Key points:

- `sda` is a **physical disk** with three partitions.
- `sda1` and `sda2` are regular partitions, with `sda1` mounted as `/boot/efi` (EFI system partition).
- `sda3` is part of an **LVM (Logical Volume Manager)** setup, which contains logical volumes (`rootvg-rootlv` for root and `rootvg-swaplv` for swap space).

##### So many useful parameters for `lsblk`

The `lsblk` tool also provides powerful filtering and formatting options, making it especially useful for scripting. For example, in a `%pre` script within a Red Hat Kickstart file, we can efficiently determine the largest available **disk** device:

<pre class="con">
<span class="psu">$</span> <span class="cmd">lsblk -n -d -b -o SIZE,TYPE,NAME</span>
32212254720 disk sda
 1073741312 rom  sr0
</pre>

In this example:

- The `-n` option suppresses column headers for cleaner output.
- The `-d` option ensures that only **top-level** devices (not partitions or logical volumes) are listed.
- The `-b` option displays sizes in **bytes** for precise comparisons.
- The `-o SIZE,TYPE,NAME` option specifies which columns to output.

This approach allows scripts to reliably identify and use the **largest disk** available for installation or partitioning.

#### 2.1.3 `fdisk`

The `fdisk` utility is a widely used partitioning tool for managing **MBR (DOS)** and **GPT (GUID Partition Table)** disk labels. While `fdisk` is powerful, it is primarily text-based and may not be the best choice for beginners.

##### Key differences between MBR and GPT

- **MBR**
    - Supports a maximum of **4 primary partitions**.
    - Can create **extended partitions**, which allow additional logical partitions.
    - Limited to **2 TiB** maximum disk size.
- **GPT**
    - Supports **128 partitions** by default.
    - Does not require extended/logical partitions.
    - Supports disks **larger than 2 TiB**.
    - Required for **UEFI booting**.

Example output of `fdisk -l`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">fdisk -l</span>
Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 49146992-29DE-40B8-953E-9C800B2ABF00

Device       Start      End  Sectors  Size Type
/dev/sda1     2048   133119   131072   64M EFI System
/dev/sda2   133120  1181695  1048576  512M Linux filesystem
/dev/sda3  1181696 62908415 61726720 29.4G Linux LVM

[...]
</pre>

This output shows:

- A **GPT disk label** (`Disklabel type: gpt`).
- Three partitions: an **EFI system partition**, a **Linux filesystem partition**, and an **LVM partition**.

If managing **MBR** partitions, `fdisk` will enforce the **4 primary partition** limit unless extended partitions are used.

##### 2.1.3.1 Entering `fdisk`

The `fdisk` utility provides an interactive interface for listing partition details, modifying existing partitions, and creating new ones.  

At any point, the `m` option can be used to display a list of available commands.  

It's important to note that partition type codes are **not universal** across all systems. For example, the "Linux LVM" type might be represented by `8e` on one system but `31` on another.

##### 2.1.3.2 Example of Partition Management with `fdisk`

This example demonstrates how to prepare a **completely empty disk** by:  

1. Switching to a **GPT disk label**
2. Creating a **new partition** that spans the entire disk (from the first to the last usable sector)
3. Changing the partition type to **"Linux LVM"** (while also listing the available types)
4. Writing the changes to disk

<pre class="con">
<span class="psr">#</span> <span class="cmd">fdisk /dev/sdb</span>
Welcome to fdisk (util-linux 2.32.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

[...]

<span class="psr">Command (m for help):</span> <span class="cmd">g</span> <span class="cmt"><-- switching to GPT</span>

Created a new GPT disklabel (GUID: 2B325B6A-5D0E-8248-9A0D-755B79CA69DD).
The old dos signature will be removed by a write command.

[...]

<span class="psr">Command (m for help):</span> <span class="cmd">n</span> <span class="cmt"><-- "New"</span>
<span class="psr">Partition number (1-128, default 1):</span> <span class="cmt"><-- just press Enter here</span>
<span class="psr">First sector (2048-37497077726, default 2048):</span> <span class="cmt"><-- just press Enter here</span>
<span class="psr">Last sector, +sectors or +size{K,M,G,T,P} (2048-37497077726, default 37497077726):</span> <span class="cmt"><-- just press Enter here</span>

Created a new partition 1 of type 'Linux filesystem' and of size 17.5 TiB.

<span class="psr">Command (m for help):</span> <span class="cmd">p</span> <span class="cmt"><-- "Print" the results</span>
Disk /dev/sdb: 17.5 TiB, 19198503813120 bytes, 37497077760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 262144 bytes / 1048576 bytes
Disklabel type: gpt
Disk identifier: 2B325B6A-5D0E-8248-9A0D-755B79CA69DD

Device     Start         End     Sectors  Size Type
/dev/sdb1   2048 37497077726 37497075679 17.5T Linux filesystem

[...]

<span class="psr">Command (m for help):</span> <span class="cmd">t</span> <span class="cmt"><-- changing "Type"</span>
Selected partition 1
<span class="psr">Partition type (type L to list all types):</span> <span class="cmd">L</span> <span class="cmt"><-- "List" all type <-> serial number assignments</span>

[...]

<span class="psr">Partition type (type L to list all types):</span> <span class="cmd">31</span> <span class="cmt"><-- specifying the type by its ID (not the same on all systems!)</span>
Changed type of partition 'Linux filesystem' to 'Linux LVM'.

<span class="psr">Command (m for help):</span> <span class="cmd">p</span> <span class="cmt"><-- "Print" the results again</span>
Disk /dev/sdb: 17.5 TiB, 19198503813120 bytes, 37497077760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 262144 bytes / 1048576 bytes
Disklabel type: gpt
Disk identifier: 2B325B6A-5D0E-8248-9A0D-755B79CA69DD

Device     Start         End     Sectors  Size Type
/dev/sdb1   2048 37497077726 37497075679 17.5T Linux LVM

<span class="psr">Command (m for help):</span> <span class="cmd">w</span> <span class="cmt"><-- "Write changes and exit"</span>
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
</pre>

#### 2.1.4 The `blkid` Tool

The `blkid` utility, part of the `util-linux-core` package, scans all system-detected block devices and prints their attributes, such as **UUIDs**, **filesystem types**, and **partition labels**. It is particularly useful for identifying and working with storage devices.

Example output:

<pre class="con">
<span class="psr">#</span> <span class="cmd">blkid</span>
/dev/mapper/rootvg-swaplv: UUID="3b8564e2-2375-4faa-8d72-e70c04b3a956" TYPE="swap"
/dev/mapper/rootvg-rootlv: UUID="1e815777-217e-4ddf-bc0b-5973629abdee" TYPE="ext4"
/dev/sda2: UUID="b1b906d6-175e-4613-935a-9d1a038a796b" TYPE="ext4" PARTUUID="a71c7a8d-be8d-46d3-9843-97b5802e1bd1"
/dev/sda3: UUID="einDgV-dFed-8GFh-z8D3-1gdo-JPyP-N7VBTG" TYPE="LVM2_member" PARTUUID="4599e25b-76a5-455d-a14f-c1aeddde53e3"
/dev/sda1: SEC_TYPE="msdos" UUID="BCE5-7A18" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="b51ae1ea-9b3c-4067-b304-406391349ab0"
</pre>

The **UUID** (**Universally Unique Identifier**) in the output is especially important, as it allows devices to be referenced consistently, even if their device names (e.g., `/dev/sda1`, `/dev/sdb2`) change. Using **UUIDs** for mounting filesystems is a common best practice to avoid dependency on dynamic device names.

#### 2.1.5 `gparted` Tool

For users who prefer a graphical interface, **GParted (GNOME Partition Editor)** provides a visual way to manage partitions. It supports:

- Creating, resizing, and deleting partitions.
- Formatting partitions with various file systems.
- Moving partitions without data loss.

GParted is particularly useful for users unfamiliar with command-line partitioning tools.

### 2.2 Partitioning as Preparation for Installation

Most Linux distributions provide built-in partitioning tools during installation. However, partitions can be created **before installation** using any of the tools discussed earlier.

For example, Red Hat-based distributions like **RHEL, Fedora, and Rocky Linux** use the **Anaconda installer**, which provides:

- **Automatic partitioning** (recommended for beginners).
- **Custom partitioning** (for advanced users needing specific layouts).
- **LVM, RAID, and Btrfs support**.

An example, showing a manual partitioning via a Rocky Linux' Anaconda installer:

![alt_text](linux_-_storage_-_anaconda_partitioner.png)

When manually partitioning for an installation, a common layout might be:

1. **EFI system partition** (if UEFI boot is required, with at least 64 MB).
2. **`/boot` partition** (typically at least 512 MB for keeping old kernel OS updates).
3. **Root (`/`) partition** (holding the OS).
4. **Swap partition** (optional, unless using a swap file).


<a name="3"></a>
## 3. Filesystems

Linux supports **a vast range of filesystems**, both in terms of reading and writing existing ones when connected, and by **formatting** new storage devices. The ability to work with multiple filesystems makes Linux highly flexible and suitable for various use cases, from enterprise servers to embedded systems.

Traditionally, `mkfs` has been the standard utility for formatting volumes. However, its manual now states that **"This `mkfs` frontend is deprecated in favor of filesystem-specific `mkfs.<type>` utilities."**  
In the following example, simply typing `mkfs.` and pressing **TAB** reveals the variety of filesystem types that can be formatted on your system:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo mkfs.[TAB]</span>
mkfs.bfs     mkfs.cramfs  mkfs.ext3    mkfs.f2fs    mkfs.jfs     mkfs.msdos   mkfs.vfat
mkfs.btrfs   mkfs.ext2    mkfs.ext4    mkfs.fat     mkfs.minix   mkfs.ntfs    mkfs.xfs
</pre>

### 3.1 Overview of Filesystem Types

Linux supports various filesystems, each designed for different purposes. Below is a summary of the most commonly used types:

- **Ext2, Ext3, Ext4** – The **Extended Filesystem family**, with Ext4 being the most widely used in community-powered distributions. Ext4 supports journaling, large files, and performance improvements over its predecessors.
- **XFS** – A high-performance journaling filesystem, optimized for parallel I/O and scalability. It is the default filesystem for **Red Hat Enterprise Linux (RHEL)** and its forks like **Rocky**.
- **Btrfs** – A **copy-on-write** filesystem with built-in **snapshotting, checksumming, and RAID-like features**. It is primarily used in **SUSE Linux Enterprise** and **openSUSE**.
- **JFS** – IBM's **Journaled File System**, designed for large-scale storage and high throughput. It is available under Linux via the `jfsutils` package.
- **ReiserFS** – An older journaling filesystem, once popular due to its efficiency in handling small files, but is now largely obsolete.
- **FAT, FAT32, and exFAT** – These Microsoft-originated filesystems are widely used for **USB drives, SD cards, and external storage**. The `dosfstools` package provides utilities to format FAT filesystems.
- **NTFS** – A Microsoft filesystem still used on **Windows systems**. Linux supports it via the **NTFS-3G driver** (via `ntfsprogs` package).
- **MinixFS** – A simple filesystem primarily used in **Minix**, a predecessor to Linux. Historically, early Linux versions used MinixFS before Ext2 was developed.
- **CRAMFS** – A read-only compressed filesystem mainly used in **embedded devices**.
- **F2FS** – A filesystem designed for **flash storage** (e.g., SSDs, SD cards) to improve performance and lifespan.

#### Example: Formatting an SD Card as NTFS

NTFS can be a good choice for large-capacity storage that needs compatibility with both Linux and Windows, such as external hard drives or **smart TVs** like LG's:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mkfs.ntfs -L Kodak256G -Q /dev/sdb1</span>
Cluster size has been automatically set to 4096 bytes.
Creating NTFS volume structures.
mkntfs completed successfully. Have a nice day.
</pre>

#### Filesystems and Logical Volume Management (LVM)

Traditionally, filesystems are created directly on disk partitions. However, in **modern Linux servers and desktops**, filesystems are often created on **Logical Volume Management (LVM) devices** instead. This allows for greater flexibility, including **resizing, snapshots, and easier disk management** (covered in the next chapter).

### 3.2 Mounting Filesystems

When a device is attached to the system and contains a valid filesystem recognized by Linux, it still needs to be **mounted** before it can be accessed. 

Modern Linux distributions automatically mount external devices, such as USB drives, under `/run/media/` or `/mnt/`, making them immediately available to the user. However, **internal storage devices and manually created filesystems do not get auto-mounted** and require manual intervention.

The `mount` command, when run without arguments, displays all currently mounted filesystems, including system partitions. To focus on specific filesystems, it’s often useful to **filter the output**. For example, to check the mount status of boot-related partitions:

<pre class="con">
<span class="psu">$</span> <span class="cmd">mount | grep boot</span>
/dev/sda2 on /boot type ext4 (rw,relatime,seclabel)
/dev/sda1 on /boot/efi type vfat (rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=ascii,shortname=winnt,errors=remount-ro)
</pre>

While `mount` is often used for inspection, its primary function is **mounting filesystems manually**. Both the device and the mount point must exist before execution. Example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo mount /dev/mapper/datavg-datalv /data</span>

<span class="psu">$</span> <span class="cmd">df -hPT /data</span>
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/datavg-datalv xfs    16T  110G   16T   1% /data

<span class="psu">$</span> <span class="cmd">mount | grep data</span>
/dev/mapper/datavg-datalv on /data type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=256k,sunit=512,swidth=2048,noquota)
</pre>

#### 3.2.1 The `/etc/fstab` File

The `/etc/fstab` file is a simple text-based configuration file that **defines how and where filesystems should be automatically mounted**. It ensures persistent mounting across reboots.

Example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat /etc/fstab</span>
[...]
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rootvg-rootlv                  /          ext4  defaults                    1 1
UUID=b1b906d6-175e-4613-935a-9d1a038a796b  /boot      ext4  defaults                    1 2
UUID=BCE5-7A18                             /boot/efi  vfat  umask=0077,shortname=winnt  0 2
/dev/mapper/rootvg-swaplv                  none       swap  defaults                    0 0
</pre>

The `/etc/fstab` file consists of **six space-separated fields**, defining how a filesystem is mounted:

1. **Device identifier** – The filesystem’s block device (`/dev/sdX`), **UUID**, or **LABEL**.
2. **Mount point** – The directory where the filesystem will be mounted (or `none` for swap partitions).
3. **Filesystem type** – Examples: `ext4`, `xfs`, `btrfs`, `vfat`, `swap`, etc.
4. **Mount options** – Either `defaults` (which includes `rw`, `suid`, `dev`, `exec`, `auto`, `nouser`, `async`) or custom options like `noexec`, `nosuid`, etc.
5. **Dump frequency** – Used by `dump(8)` for backups (`0` disables it).
6. **Filesystem check order** – Defines the sequence in which `fsck` runs during boot (`1` for root filesystem, `2` for others, `0` to disable checking).

To apply changes after modifying `/etc/fstab`, run:

<pre class="con">
<span class="psr">#</span> <span class="cmd">systemctl daemon-reload</span>
</pre>

This ensures systemd acknowledges the updated mount configurations.

### 3.3 Using `tune2fs`

The `tune2fs` command is used to modify tunable parameters of any **Ext-based** filesystem (`ext2`, `ext3`, `ext4`). It allows system administrators to adjust various filesystem attributes, such as reserved blocks, error behavior, and journaling options.

One of its most frequently used options is `-l`, which **lists the contents of the filesystem superblock**, displaying the current values of all tunable parameters.

For example, checking the **reserved block count** (which defaults to 5% of the filesystem space) can be useful for monitoring storage allocation:

<pre class="con">
<span class="psr">#</span> <span class="cmd">tune2fs -l /dev/mapper/rootvg-rootlv | grep -i reserved</span>
Reserved block count:     333312
Reserved GDT blocks:      1024
Reserved blocks uid:      0 (user root)
Reserved blocks gid:      0 (group root)
</pre>

By default, 5% of the filesystem space is reserved for the root user. While this is useful for system partitions, it may not be necessary for **data partitions**, where reclaiming this space could be beneficial.

#### 3.3.1 Changing the Reserved Block Percentage

To **remove reserved space** (i.e., set it to `0%`), use the `-m` option. After applying the change, a reboot is required to reflect the updated available disk space in this case, since we're changing our `/` filesystem.

Before making the change:

<pre class="con">
<span class="psr">#</span> <span class="cmd">df -hPT /</span>
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv ext4   25G  3.4G   21G  15% /
</pre>

Setting reserved space to `0%`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">tune2fs -m 0 /dev/mapper/rootvg-rootlv</span>
tune2fs 1.46.5 (30-Dec-2021)
Setting reserved blocks percentage to 0% (0 blocks)
</pre>

Reboot the system to apply changes:

<pre class="con">
<span class="psr">#</span> <span class="cmd">reboot</span>
Connection to rocky95srv1.example.com closed by remote host.
</pre>

After reboot:

<pre class="con">
<span class="psr">#</span> <span class="cmd">df -hPT /</span>
Filesystem                Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv ext4   25G  3.4G   22G  14% /
</pre>

The reserved block percentage can **also be set during filesystem creation** using the `-m` option with `mkfs.ext*` commands:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mkfs.ext4 -m 0 /dev/sdb1</span>
</pre>

This ensures no space is reserved from the start, avoiding the need for manual adjustments later.


<a name="4"></a>
## 4. LVM

Linux' **Logical Volume Manager** (**LVM**), often referred to as **LVM2** due to its current stable version, is based on **IBM AIX's LVM** with various modifications—some improvements and some limitations—compared to the original.

The core idea behind LVM is that it introduces a **logical abstraction layer** between the filesystem and the underlying disk partition. This enables a **flexible storage management system**, where the **Logical Volume (LV)** can host **filesystems, swap space, or even raw storage** for specialized use cases. 

Unlike traditional partitions, **LVM allows dynamic resizing** of logical volumes using small storage units called **Physical Extents (PEs)** (known as **Physical Partitions (PPs)** in IBM AIX). This means an LV can be **expanded or shrunk** as long as the system and storage components support it.

### 4.1 Core Concepts of Linux LVM

LVM consists of **four main components**:

- **PV (Physical Volume)**: Represents a storage device or partition that LVM can manage. A PV **can belong to only one VG**.
- **VG (Volume Group)**: A storage pool that consists of one or more PVs.
- **PE (Physical Extent)**: The smallest allocation unit within a VG, used to construct LVs.
- **LV (Logical Volume)**: The "virtual partition" created from a VG, which is then formatted with a **filesystem (FS)** or used for other purposes like swap space.

The illustration below demonstrates how these components are interconnected:

![LVM Structure](linux_-_storage_-_lvm.svg)

In the diagram, **FS3** is smaller than its corresponding LV, which could indicate that the LV was expanded but the **filesystem was not resized accordingly**.

### 4.2 Benefits of Using LVM

LVM is highly useful because it **introduces powerful storage management features** without adding measurable overhead. Some of its advantages include:

- **Flexible resizing**: LVs can be **expanded or shrunk** dynamically.
- **Efficient storage pooling**: You can add or remove **additional PVs** to increase storage capacity.
- **Snapshot support**: Create **point-in-time snapshots** of an LV for backups or testing.

A **unique advantage of Linux LVM** compared to its **IBM AIX counterpart** is its **greater flexibility**. On AIX, **LVM is mandatory**, meaning disks **must** be added as LVM-managed devices from the start. This enforces a **strict, partitionless approach**. However, **Linux allows more freedom**, enabling **"non-standard" setups** where partitions can be used as PVs (e.g., `/dev/sdb2` in the diagram). This backward compatibility means **LVM can be used on top of traditional partitioned disks**, offering **hybrid setups** that are impossible in AIX.

### 4.3 LVM and RAID Support

LVM can also function as a **software RAID solution**, supporting three RAID configurations:

- **RAID 0 (Striping)**: Data is **spread across multiple disks** for increased performance.
- **RAID 1 (Mirroring)**: Data is **duplicated across multiple disks** for redundancy (up to **five copies** in LVM2).
- **RAID 10 (Striping + Mirroring)**: A combination of RAID 0 and RAID 1 for both performance and redundancy.

See the next chapter for **more details about RAID levels**.

### 4.4 Understanding the `/dev/mapper/` Hierarchy in Linux  

The `/dev/mapper/` directory is a **critical part of the Linux device-mapper subsystem**, used primarily for managing logical volumes, encrypted devices, and other mapped block devices.  

The reason why it even exists is that Linux uses **device mapping** to create **abstraction layers over physical block devices**. Instead of accessing raw disk partitions directly, the **device-mapper framework** allows flexible management of storage by creating **virtual devices** that point to physical storage locations.  

#### 4.4.1 What Uses `/dev/mapper/`

- **LVM (Logical Volume Manager)**:
    - LVM uses the **device mapper** to create virtual block devices for logical volumes.
    - Each **logical volume** (LV) appears under `/dev/mapper/` as a device file.
    - Example: `/dev/mapper/vg_name-lv_name`
    - A more traditional symlink also exists in `/dev/vg_name/lv_name`.
- **DM-Crypt (LUKS Encrypted Partitions)**:
    - Encrypted partitions use device-mapper to create **decrypted** virtual devices.
    - Example: `/dev/mapper/luks-encrypted-disk`
- **RAID and Multipath Devices:**  
    - **Multipath I/O (MPIO)** creates device-mapper nodes for redundant paths to storage.
    - Example: `/dev/mapper/mpathX`

#### 4.4.2 How the `/dev/mapper/` Hierarchy is Handled  

1. **Created by the `udev` system:**  
    - When a device-mapper entry is added, **udev** automatically creates the corresponding `/dev/mapper/` device file.
2. **Managed using `dmsetup`:**  
    - The `dmsetup` command provides direct interaction with device-mapper devices.
    - Example: `sudo dmsetup ls` 
3. **LVM Logical Volumes Appear Here:**  
    - The **LVM2 metadata** references the `/dev/mapper/` path as the primary location of logical volumes.
4. **Persistent Across Reboots:**  
    - LVM metadata, LUKS headers, or multipath configurations ensure that device-mapper nodes persist after a reboot.

##### Example: Viewing Device Mapper Entries  

<pre class="con">
<span class="psr">#</span> <span class="cmd">ls -l /dev/mapper/</span>
</pre>

Typical output for a system using **LVM and LUKS encryption**:  

<pre class="con">
lrwxrwxrwx 1 root root 7 Feb 14 10:20 my_vg-my_lv -> ../dm-0
lrwxrwxrwx 1 root root 7 Feb 14 10:20 luks-encrypted -> ../dm-1
</pre>

Each entry is a **symbolic link to a `/dev/dm-*` device**, which is the actual mapped block device.  

##### Summary  

- `/dev/mapper/` holds **virtual block devices** managed by **device-mapper**.  
- It is crucial for **LVM, encryption (LUKS), multipath I/O, and software RAID**.  
- Device-mapper ensures **flexibility**, allowing storage layers to be **dynamically managed**.  
- Tools like `lsblk`, `dmsetup`, and `lvdisplay` help inspect these mappings.  

This directory is essential for **modern Linux storage management**, ensuring that logical storage abstractions remain efficient and persistent.


<a name="5"></a>
## 5. RAID

RAID (**Redundant Array of Independent Disks**) is a broad and complex topic, but here, we'll focus on **the most commonly used RAID levels**, along with their implementations in both **software** and **hardware**.

### 5.1 Standard RAID Levels

Rather than repeating the [Wikipedia article](https://en.wikipedia.org/wiki/Standard_RAID_levels), which covers RAID levels extensively, here is a **concise summary** of the most relevant RAID configurations:

- **RAID 0** (*Striping*): Data is spread across **two or more disks** without any redundancy.
    - **Pros**: Increased performance due to **parallel disk access**.
    - **Cons**: If **any disk fails, all data is lost**.
- **RAID 1** (*Mirroring*): Each data block is **duplicated across multiple disks**.
    - **Pros**: Data remains intact as long as **at least one disk is operational**.
    - **Cons**: Write performance is limited by **the slowest disk**.
- **RAID 2** (*Bit-level striping with Hamming Code for error correction*):
    - **Rarely used in practice** due to inefficiency.
- **RAID 3** (*Byte-level striping with a dedicated parity disk*):
    - **Not widely implemented**, as it **cannot handle multiple requests efficiently**.
- **RAID 4** (*Block-level striping with a dedicated parity disk*):
    - **Pros**: Supports easy **online expansion**.
    - **Cons**: **Good read performance**, but **slower writes** since all parity data goes to a single disk.
- **RAID 5** (*Block-level striping with distributed parity*):
    - **Pros**: Can **rebuild lost data** if a single disk fails, **better storage efficiency** than RAID 10.
    - **Cons**: **Lower write performance** than RAID 10 due to parity calculations.
- **RAID 6** (*RAID 5 + double parity*):
    - **Pros**: Can **tolerate two simultaneous disk failures**.
    - **Cons**: **Lower capacity and performance** compared to RAID 10.
- **RAID 10** (*Mirroring + Striping*): A combination of RAID 0 and RAID 1.
    - **Pros**: **Best performance and redundancy**.
    - **Cons**: **Lower storage efficiency** than RAID 5 or 6.

### 5.2 Software RAID

On **Linux**, software RAID is primarily managed via the `mdadm` utility, which supports the following RAID levels:

- **RAID 0** (Striping)
- **RAID 1** (Mirroring)
- **RAID 4** (Block-level striping with dedicated parity)
- **RAID 5** (Block-level striping with distributed parity)
- **RAID 6** (Similar to RAID 5 but with additional parity for fault tolerance)
- **RAID 10** (Striped mirrors)

<div class="warn">
<p>Since Linux <strong>kernel 2.6.32</strong>, there have been <strong>known data loss risks</strong> in certain <code>md/raid6</code> arrays under specific conditions.</p>
</div>

#### 5.2.1 Creating a RAID10 Array with `mdadm`

The `mdadm` utility is used for managing **Linux Software RAID**. This example demonstrates how to create a **RAID10** array using two identical, empty disks: `/dev/sdb` and `/dev/sdc`.  

RAID10 (RAID 1+0) combines **mirroring (RAID1) and striping (RAID0)** for better redundancy and performance. Normally, RAID10 requires at least four disks, but **Linux allows RAID10 with only two disks** in a special layout.

##### Steps to Create the RAID10 Array

1. Ensure **`mdadm` is installed**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">dnf install -y mdadm</span>  <span class="cmt"># RHEL-based systems</span>
    [...]
    </pre>
2. **Verify** the disks are empty and unmounted:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">lsblk /dev/sdb /dev/sdc</span>
    [...]
    </pre>
3. Create the **RAID10 array**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mdadm --create --verbose /dev/md0 --level=10 --raid-devices=2 /dev/sdb /dev/sdc</span>
    [...]
    </pre>
4. **Monitor** the array building process:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">cat /proc/mdstat</span>
    [...]
    </pre>
5. Save the **RAID configuration**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mdadm --detail --scan >> /etc/mdadm.conf</span>
    [...]
    </pre>
6. **Create a filesystem** on the new RAID device:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mkfs.xfs /dev/md0</span>
    [...]
    </pre>
7. **Mount** the new RAID volume:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">mkdir -p /mnt/raid10</span>

    <span class="psr">#</span> <span class="cmd">mount /dev/md0 /mnt/raid10</span>
    </pre>
8. Make the mount **persistent** across reboots:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">echo "/dev/md0 /mnt/raid10 xfs defaults 0 2" | sudo tee -a /etc/fstab</span>
    </pre>

##### Verifying the RAID10 Array

You can check the RAID10 array details at any time using:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo mdadm --detail /dev/md0
</pre>

This setup ensures redundancy while maintaining good read/write performance, even with only two disks.

### 5.3 Hardware RAID

In **enterprise environments**, RAID is often handled by **dedicated RAID controllers** instead of software solutions. You can check for RAID controllers using `lsscsi` (for a summary) or `lshw` (for detailed hardware information).

Below is an example from a **Dell PowerEdge** system, which features both **PERC** (**PowerEdge RAID Controller**) and **BOSS** (**Boot Optimized Storage Solution**):

<pre class="con">
<span class="psu">$</span> <span class="cmd">lsscsi</span>
[0:2:0:0]    disk    DELL     PERC H745 Frnt   5.16  /dev/sdb 
[1:0:0:0]    disk    ATA      DELLBOSS VD      00-0  /dev/sda 
[3:0:0:0]    process Marvell  Console          1.01  -        

<span class="psu">$</span> <span class="cmd">sudo lshw -class disk</span>
  *-disk                    
       description: SCSI Disk
       product: PERC H745 Frnt
       vendor: DELL
       physical id: 2.0.0
       bus info: scsi@0:2.0.0
       logical name: /dev/sdb
       version: 5.16
       serial: 00f76c9ea9a1c3742d002b531d20a7c2
       size: 15TiB (16TB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=683ad01e-7642-0c49-8d7a-e94fd00f1576 logicalsectorsize=512 sectorsize=512
  *-disk
       description: ATA Disk
       product: DELLBOSS VD
       physical id: 0
       bus info: scsi@1:0.0.0
       logical name: /dev/sda
       version: 00-0
       serial: 1be5306b4b6e0010
       size: 447GiB (480GB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=04ab02c8-f203-42b8-8ceb-1e2795c23f35 logicalsectorsize=512 sectorsize=4096
</pre>

#### 5.3.1 Identifying RAID Controller Drivers

You can also check the **RAID controller driver** being used by querying the **kernel modules**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">lsmod | grep raid</span>
megaraid_sas          176128  1

<span class="psu">$</span> <span class="cmd">modinfo megaraid_sas | head -6</span>
filename:       /lib/modules/4.18.0-513.11.1.el8_9.x86_64/kernel/drivers/scsi/megaraid/megaraid_sas.ko.xz
description:    Broadcom MegaRAID SAS Driver
author:         megaraidlinux.pdl@broadcom.com
version:        07.725.01.00-rc1
license:        GPL
rhelversion:    8.9
</pre>

Alternatively, checking **kernel logs** can provide more details:

<pre class="con">
<span class="psu">$</span> <span class="cmd">dmesg | grep sas</span>
[    7.264085] megasas: 07.725.01.00-rc1
[    7.264987] megaraid_sas 0000:01:00.0: BAR:0x0  BAR's base_addr(phys):0x00000000a6b00000  mapped virt_addr:0x00000000feb0085d
[    7.264997] megaraid_sas 0000:01:00.0: FW now in Ready state
[...]
</pre>


<a name="6"></a>
## 6. LUKS Encryption

LUKS (**Linux Unified Key Setup**) is the **standard disk encryption method** for Linux. It provides **strong encryption**, represents a secure way to **protect data at rest**, supports **multiple key slots**, and integrates well with **modern Linux systems**.

### 6.1 Key Concepts

LUKS allows an entire block device to be encrypted, requiring a **passphrase or key** to unlock and access the data.

Below is an example of using **LUKS encryption** for a system’s `/home` filesystem, while leaving all other partitions traditionally formatted (without LUKS or even LVM):

![alt_text](linux_-_storage_-_luks.png)

LUKS is flexible, allowing encryption to be applied at various levels:

- **Full Disk Encryption (FDE):** Encrypting the entire disk, including the root filesystem.
- **LVM Volume Group Encryption:** Encrypting an entire **Volume Group** (`VG`) to protect all contained **Logical Volumes** (`LVs`).
- **Per-Volume Encryption:** Encrypting specific **Logical Volumes** while leaving others unencrypted.

### 6.2 Encrypting a Logical Volume

LUKS can be used to encrypt **existing logical volumes** or newly created ones.

In the following example, we will:

1. **Wipe an existing filesystem** (ensuring no important data remains).
2. **Apply LUKS encryption at the logical volume (LV) level.**

Before proceeding, ensure you have **backups** of any critical data, as the encryption process **will erase** the existing filesystem on the volume.  

#### 6.2.1 Installing Required Tools

The `cryptsetup` package is required for LUKS encryption. Install it first:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install cryptsetup</span>
[...]
Installed size: 500 k
Is this ok [y/N]: y
[...]
Installed:
  cryptsetup-2.3.7-7.el8.x86_64

Complete!
</pre>

#### 6.2.2 Preparing the Target Device

If the device **was previously used as a regular filesystem**, **unmount** it and remove its entry from `/etc/fstab` to prevent automatic mounting:

<pre class="con">
<span class="psr">#</span> <span class="cmd">vi /etc/fstab</span>

<span class="psr">#</span> <span class="cmd">systemctl daemon-reload</span>
</pre>

If the device **was previously encrypted**, close it before proceeding:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cryptsetup close /dev/mapper/datak8slv</span>
</pre>

#### 6.2.3 Encrypting the Device

1. **Generate a strong passphrase** and store it securely in a password vault.  
   A good practice is to use a **32-character-long passphrase** containing **uppercase and lowercase letters, digits, and special characters**.
2. **Encrypt the target device** (here, an LVM logical volume is used):

<pre class="con">
<span class="psr">#</span> <span class="cmd">cryptsetup luksFormat /dev/mapper/datavg-datak8slv</span>
WARNING: Device /dev/mapper/datavg-datak8slv already contains a 'xfs' superblock signature.

WARNING!
&#61;&#61;&#61;&#61;&#61;&#61;&#61;&#61;
This will overwrite data on /dev/mapper/datavg-datak8slv irrevocably.

<span class="psr">Are you sure? (Type 'YES' in capital letters):</span> <span class="cmd">YES</span>
Enter passphrase for /dev/mapper/datavg-datak8slv: 
Verify passphrase: 
</pre>

#### 6.2.4 Unlocking, Formatting, and Mounting the Encrypted Device

After encryption, **the device must be opened (decrypted) before use**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cryptsetup open --type luks /dev/mapper/datavg-datak8slv datak8slv_encr</span>
Enter passphrase for /dev/mapper/datavg-datak8slv: 
</pre>

This creates a **new device mapping** under `/dev/mapper/`, which can now be formatted and mounted.

Format the unlocked device with your **preferred filesystem**, e.g., XFS:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mkfs.xfs /dev/mapper/datak8slv_encr</span>
[...]
</pre>

Once formatted, mount it to your desired location:

<pre class="con">
<span class="psr">#</span> <span class="cmd">mount /dev/mapper/datak8slv_encr /data/k8s</span>
</pre>

To verify that the **encrypted volume is correctly mapped and mounted**, use `lsblk`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">lsblk /dev/sda</span>
NAME                   MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda                      8:0    0 893.3G  0 disk  
└─sda1                   8:1    0 893.3G  0 part  
  ├─datavg-datak8slv   253:7    0 625.3G  0 lvm   
  │ └─datak8slv_encr   253:9    0 625.3G  0 crypt /data/k8s
  └─datavg-datak0slv   253:8    0   268G  0 lvm   /data/k0s
</pre>

Here, you can see:

- The **physical device (`sda`)**.
- Its **partition (`sda1`)**.
- The **LVM logical volume (`datavg-datak8slv`)**.
- The **LUKS encrypted layer (`datak8slv_encr`)**.
- The **mounted filesystem (`/data/k8s`)**.


<a name="7"></a>
## 7. Disk Performance Testing and Tuning

Measuring disk performance is crucial for system optimization, ensuring hardware efficiency, and diagnosing potential bottlenecks. 

When testing storage devices, **several key performance metrics** are evaluated:

- **Throughput** – Measures the amount of data transferred per second (MB/s, GB/s).
- **IOPS (Input/Output Operations Per Second)** – Measures how many read/write operations can be handled per second.
- **Latency** – Measures the delay between a request and the corresponding response.

### 7.1 Simple Disk I/O Tests with `dd`

The `dd` command is widely used for **testing raw disk performance** by reading or writing large chunks of data. It is sometimes jokingly called:
- **"Disk Destroyer"** (due to its ability to overwrite disks if used incorrectly),
- **"Data Definition"** (historical meaning),
- **"Disk Duplicator"** (for cloning disks and partitions).

A basic sequential write speed test with `dd`:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dd if=/dev/zero of=testfile bs=1G count=10 oflag=dsync</span>
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 150.987 s, 71.1 MB/s
</pre>

- `if=/dev/zero`: Reads from `/dev/zero` (zero-filled pseudo-device).
- `of=testfile`: Writes data to `testfile`.
- `bs=1G`: Sets a block size of **1GB**.
- `count=10`: Writes **10 blocks**, resulting in **10GB** of total data.
- `oflag=dsync`: Ensures **synchronous writes**, measuring realistic disk speeds.

### 7.2 Disk I/O Tests with `fio`

`fio` (**Flexible I/O Tester**) is a powerful tool for **benchmarking storage devices** with highly configurable test patterns. It simulates **various real-world I/O workloads** and provides **detailed performance statistics**.

To understand `fio` options in detail, check its **manual page**:
- Local: `man fio`
- Online: [Fio man page](https://linux.die.net/man/1/fio)

#### 7.2.1 Basic Tests with `fio`

The following command performs **random write tests** with a **4K block size**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">fio --name=test --rw=randwrite --ioengine=sync --bs=4k --numjobs=1 --size=1G --runtime=10m --time_based</span>
[...]
</pre>

##### Explanation of Key Parameters:

- `--name=<test_name>`: Assigns a name to the test.
- `--rw=<pattern>`: Defines the **read/write** pattern.
  - `read`: Sequential reads.
  - `write`: Sequential writes.
  - `randread`: Random reads.
  - `randwrite`: Random writes.
  - `rw` or `readwrite`: Mixed sequential read/write.
  - `randrw`: Mixed random read/write.
- `--ioengine=<engine>`: Specifies the I/O processing engine.
  - `sync`: Standard read/write operations.
  - `libaio`: Asynchronous I/O (widely used for Linux testing).
- `--bs=<block_size>`: Defines the block size (e.g., `4k`, `64k`, `1M`).
- `--numjobs=<threads>`: Sets the number of parallel jobs/threads.
- `--size=<job_size>`: Defines total job size (e.g., `1G` = 1GB).
- `--runtime=<duration>`: Limits test duration (e.g., `10m` = 10 minutes).
- `--time_based`: Ensures the test runs for the **specified time** instead of stopping after reaching `size`.

##### A non-time-based sequential write test:

<pre class="con">
<span class="psr">#</span> <span class="cmd">fio --name=test --rw=write --ioengine=libaio --bs=1G --numjobs=1 --size=100G --sync=1</span>
[...]
</pre>

#### Fio test equivalent to `dd` (Red Hat Support recommended):

<pre class="con">
<span class="psr">#</span> <span class="cmd">fio --name=throughput-test-job1 --rw=randrw --ioengine=sync --iodepth=64 --bs=64k --size=100M --numjobs=4 --fdatasync=1 --runtime=120 --time_based --group_reporting</span>
throughput-test-job1: (g=0): rw=randrw, bs=(R) 64.0KiB-64.0KiB, (W) 64.0KiB-64.0KiB, (T) 64.0KiB-64.0KiB, ioengine=sync, iodepth=64
[...]
fio-3.19
Starting 4 processes
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
throughput-test-job1: Laying out IO file (1 file / 100MiB)
Jobs: 4 (f=4): [m(4)][100.0%][r=935MiB/s,w=927MiB/s][r=14.0k,w=14.8k IOPS][eta 00m:00s] 
throughput-test-job1: (groupid=0, jobs=4): err= 0: pid=3564162: Wed May 29 18:50:27 2024
  read: IOPS=16.1k, BW=1005MiB/s (1054MB/s)(118GiB/120001msec)
    clat (usec): min=61, max=11397, avg=92.45, stdev=53.94
     lat (usec): min=61, max=11397, avg=92.52, stdev=53.95
    clat percentiles (usec):
     |  1.00th=[   69],  5.00th=[   73], 10.00th=[   74], 20.00th=[   77],
     | 30.00th=[   80], 40.00th=[   83], 50.00th=[   86], 60.00th=[   90],
     | 70.00th=[   96], 80.00th=[  104], 90.00th=[  119], 95.00th=[  135],
     | 99.00th=[  172], 99.50th=[  192], 99.90th=[  318], 99.95th=[  449],
     | 99.99th=[  930]
   bw (  KiB/s): min=702519, max=1253921, per=100.00%, avg=1030187.22, stdev=31843.47, samples=956
   iops        : min=10975, max=19592, avg=16095.29, stdev=497.68, samples=956
  write: IOPS=16.1k, BW=1005MiB/s (1054MB/s)(118GiB/120001msec); 0 zone resets
    clat (usec): min=30, max=5032, avg=43.62, stdev=18.24
     lat (usec): min=30, max=5033, avg=44.42, stdev=18.63
    clat percentiles (usec):
     |  1.00th=[   34],  5.00th=[   35], 10.00th=[   36], 20.00th=[   37],
     | 30.00th=[   38], 40.00th=[   39], 50.00th=[   41], 60.00th=[   42],
     | 70.00th=[   44], 80.00th=[   49], 90.00th=[   57], 95.00th=[   63],
     | 99.00th=[   80], 99.50th=[   91], 99.90th=[  212], 99.95th=[  265],
     | 99.99th=[  553]
   bw (  KiB/s): min=694902, max=1239468, per=100.00%, avg=1030151.76, stdev=32044.64, samples=956
   iops        : min=10856, max=19366, avg=16094.68, stdev=500.81, samples=956
  lat (usec)   : 50=41.39%, 100=46.50%, 250=11.97%, 500=0.12%, 750=0.02%
  lat (usec)   : 1000=0.01%
  lat (msec)   : 2=0.01%, 4=0.01%, 10=0.01%, 20=0.01%
  fsync/fdatasync/sync_file_range:
    sync (nsec): min=1062, max=32316k, avg=39035.23, stdev=58659.33
    sync percentiles (nsec):
     |  1.00th=[  1128],  5.00th=[  1160], 10.00th=[  1176], 20.00th=[  1208],
     | 30.00th=[  1288], 40.00th=[  1496], 50.00th=[ 55552], 60.00th=[ 66048],
     | 70.00th=[ 70144], 80.00th=[ 75264], 90.00th=[ 83456], 95.00th=[ 91648],
     | 99.00th=[109056], 99.50th=[119296], 99.90th=[195584], 99.95th=[261120],
     | 99.99th=[473088]
  cpu          : usr=2.18%, sys=57.67%, ctx=4287471, majf=0, minf=422
  IO depths    : 1=200.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=1929133,1929115,0,0 short=3858242,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=64

Run status group 0 (all jobs):
   READ: bw=1005MiB/s (1054MB/s), 1005MiB/s-1005MiB/s (1054MB/s-1054MB/s), io=118GiB (126GB), run=120001-120001msec
  WRITE: bw=1005MiB/s (1054MB/s), 1005MiB/s-1005MiB/s (1054MB/s-1054MB/s), io=118GiB (126GB), run=120001-120001msec

Disk stats (read/write):
    dm-3: ios=1929034/1931269, merge=0/0, ticks=82783/87736, in_queue=170519, util=100.00%, aggrios=1929133/1930406, aggrmerge=0/951, aggrticks=86989/105076, aggrin_queue=192065, aggrutil=100.00%
  sdb: ios=1929133/1930406, merge=0/951, ticks=86989/105076, in_queue=192065, util=100.00%
</pre>

<div class="warn">
<p>Do not run <code>fio</code> tests with a write workload (<code>readwrite</code>, <code>randrw</code>, <code>write</code>, <code>trimwrite</code>) directly against a device that is in use!</p>
</div>

#### 7.2.2 Advanced IOPS, Throughput, and Latency Tests

- **IOPS tests** (focus on **high iodepth** values).
- **Throughput tests** (focus on **lower iodepth** values).
- **Latency tests** (focus on **iodepth=1**).

Additional parameters that we will also use below: 

- `iodepth=int`: Number of I/O units to keep in flight against the file. Note that increasing `iodepth` beyond `1` will not affect synchronous ioengines (except for small degress when `verify_async` is in use). Even async engines my impose OS restrictions causing the desired depth not to be achieved. This may happen on Linux when using `libaio` and not setting `direct=1`, since buffered IO is not async on that OS. Keep an eye on the IO depth distribution in the fio output to verify that the achieved depth is as expected. Default: `1`.
- `direct=bool`: If true, use non-buffered I/O (usually `O_DIRECT`). Default: false (`0`).
- `group_reporting`: If set, display per-group reports instead of per-job when `numjobs` is specified.

#### 7.2.3 Throughput Performance Tests with `fio`

Lower `iodepth` (specifically with `64`, apparently): 

- Test random read/writes with blocksize of 64k: `fio --name=throughput-test-job --rw=randrw --ioengine=libaio --iodepth=64 --bs=64k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting`
- Test random read/writes with blocksize of 256k: `fio --name=throughput-test-job --rw=randrw --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting`
- Test sequential reads (requires existing files): `fio --name=throughput-test-job --rw=read --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`
- Test random reads (requires existing files): `fio --name=throughput-test-job --rw=randread --ioengine=libaio --iodepth=64 --bs=256k --size=100G --numjobs=4 --direct=1 --runtime=120 --time_based --group_reporting --readonly`

#### 7.2.4 Latency Performance Tests with `fio`

- Test random read/writes for latency: `fio --name=rwlatency-test-job --rw=randrw --ioengine=libaio --iodepth=1 --bs=4k --size=100G --numjobs=1 --direct=1 --runtime=120 --time_based --group_reporting`
- Test random reads for latency (requires existing files): `fio --name=readlatency-test-job --rw=randread --ioengine=libaio --iodepth=1 --bs=4k --size=100G --numjobs=1 --direct=1 --runtime=120 --time_based --group_reporting --readonly`

#### 7.2.5 Job File for `fio`

The tool supports having a "job file" too, e.g. you can save it to any file, like a `fioread.fio`:

<pre>
[global]
bs=256K
iodepth=64
direct=1
ioengine=libaio
group_reporting
time_based
runtime=120
numjobs=4
name=raw-read
rw=read
							
[job1]
filename=device name
</pre>

...then just run the job, invoking the file as `fio`'s only parameter: `fio read.fio`

#### 7.2.6 Scripting `fio` Tests

In practice, you may need to run **multiple tests with different block sizes and parameters**. Instead of executing long commands one by one and waiting for each to finish, it's more efficient to automate the process with a script.

Below is a script that systematically runs **all six I/O patterns with five different block sizes** to measure throughput performance from every angle.

<pre>
#!/bin/sh

NAME="throughput-test-job"

if [ -d /data ] && [ $(df -m /data | awk '!/Filesystem/{print $4}') -gt 400 ]
  then cd /data
  else
    echo "There's no /data! Exiting."
    exit 1
fi
if [ ! -f /usr/bin/fio ]
  then
    echo "There's no fio! You should install it first. Exiting."
    exit 1
fi

echo "Throughput Performance Tests with iodepth of 64 and runtime of 120 seconds per job"

for RW in write read rw randwrite randread randrw
  do
    for BS in 4k 64k 256k 1M 1G
      do
        echo "Testing in \"$RW\" I/O pattern with $BS blocksize..."
        if [ $RW = "read" ] || [ $RW = "randread" ]
          then
            RO="--readonly"
            echo "Read-only mode requires existing files to test against. Skipping cleanup."
          else
            RO=""
            echo "Cleaning up..."
            rm -f $NAME* ; sync
            echo -e "\e[1ACleaning up... done"
        fi
        fio \
          --name=$NAME \
          --rw=$RW \
          --ioengine=libaio \
          --iodepth=64 \
          --bs=$BS \
          --numjobs=4 \
          --size=100G \
          --direct=1 \
          --runtime=120 \
          --time_based \
          --group_reporting \
          $RO  # you may consider adding " | grep IOPS" here if you want final results only
        echo "-----"
        sleep 5
      done
  done

rm -f $NAME*
echo "Done."
</pre>


### 7.3 `iostat` – Disk and CPU Usage Monitoring

`iostat` (part of `sysstat`) provides **real-time disk I/O statistics**, including **read/write speeds and CPU usage**.

Install `sysstat` if missing:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install sysstat</span>
</pre>

To monitor **real-time disk activity**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">iostat -d -k 1 sdb | awk '/sdb/ {printf "Read: "$3", Write: "$4"\n"}'</span>
Read: 2368.89, Write: 428.41
Read: 0.00, Write: 0.00
Read: 0.00, Write: 128.00
[...]
</pre>

### 7.4 Disk Feature Inspection with `hdparm` and `sdparm`

These tools are used to query and configure disk settings:

- `hdparm` – For traditional HDDs/SSDs.
- `sdparm` – For **SCSI/SAS/NVMe** devices.

#### Checking Drive Capabilities (`hdparm`):

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo hdparm -i /dev/sda</span>
 Model=DELLBOSS VD, FwRev=MV.R00-0, SerialNo=1be5306b4b6e0010
 Config={ Fixed }
 BuffType=DualPortCache, BuffSize=8192kB, MaxMultSect=16, MultSect=off
 LBA=yes, LBAsects=937571968
 WriteCache=enabled
</pre>

#### Querying SSD Features (`sdparm`):

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo sdparm -a /dev/sdb</span>
    /dev/sdb: DELL      PERC H745 Frnt    5.16
Caching (SBC) mode page:
  WCE  = 0  (Write Cache Disabled)
</pre>

To check if **Write Cache** can be modified:
<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo sdparm --set WCE=1 /dev/sdb</span>
change_mode_page: failed setting page: Caching (SBC)
</pre>

This confirms that **hardware RAID controllers** enforce write-cache policies, making `sdparm` ineffective for such changes.
