# 📗 About

<img src="me_caricature.png" title="" align="right" class="right">

## ...me

Hi, my name is **Mihály Kóródi**, and I like documenting things; that's basically my 4th hobby besides the others that I will list below. 

Even though I know it's never a good idea to start apologizing right in the beginning, but you might have noticed already that **I'm not a native English speaker**, so bear with me, please. 

## ...the site

Everything here is about **learning** things. Mostly, regarding **IT**, but **Language Learning** and **Electronics** also received my special attention during the decades, so they got their deserved subpages too. In case of IT, the scale is quite wide: Everything from classic **System Administration**, through **Programming**, to **Computer Graphics** can be found here. The subjects are already picked, but I consider only some of them as ready and well detailed. The rest still needs to be taken care of. Usually, if any of the subpages remain in their initial phase for more than two years, then I'm always a bit sad but I just remove those pages. 

**Technically**, the whole site is relying on **client-side JavaScript rendering**, using **ShowdownJS**, an amazing Markdown to HTML converter, and in my setup, it is **your browser** that makes the calculations, not a PHP or a Node.js instance on server side. Apart from a webserver, nothing more is needed. And even though the site **doesn't use backend**, I already consider it as a **dynamic website**. How cool is that, right? I think it is. 😌

## ...the domain name

This crazy domain for the site was simply picked by me in 2017, when everything was about **SaaS**, **PaaS**, **IaaS** and the countless (and annoying) buzzwords like **Cognitive**, **Cloud**, **Agile**, **AI**, "**Ninja**" already hit the IT industry. I can just promise that **none of them are used on these pages** in any contexts (apart from this one, as I've used all of them here already). 

**Enjoy browsing and reading!**

## 🙃 Weird emojis?

It's possible that some emojis don't show up properly in your browser. 

To fix it on Firefox, you need to: 

1. open a new tab, and type: `about:config`
2. accept the risk and continue
3. search for `font.name-list.emoji`
4. set the default `Segoe UI Emoji, Twemoji Mozilla` value to `Twemoji Mozilla, Segoe UI Emoji`
5. restart the browser
