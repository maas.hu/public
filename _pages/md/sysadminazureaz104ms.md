# ![alt text](/_ui/img/icons/azure_m.svg) Azure AZ-104 Practice Questions - Microsoft ESI Portal

<div style="text-align: right;"><h7>Posted: 2024-10-25 10:00</h7></div>

###### .

These are the official [Microsoft ESI Portal](https://esi.microsoft.com/)'s Practice Assessment questions.


## Sorted list of questions mainly focusing on...

1. [Azure Identities and Governance, including ARM templates](#1)
2. [Azure Networking](#2)
3. [Azure Storage](#3)
4. [Azure Virtual Machines](#4)
5. [Azure PaaS Compute Options](#5)
6. [Azure Backup and Data Protection](#6)
7. [Azure Monitoring and Logging](#7)


<a name="1"></a>
## 1. Azure Identities and Governance, including ARM templates

---

<div>
  <p><span class="k">1.</span>You plan to use the following two Azure Resource Manager (ARM) templates to provision virtual machines:</p>
  <p><code>Template.json</code>:</p>
  <pre>{ 
  "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#", 
  "contentVersion": "1.0.0.0", 
  "parameters": { 
    "adminUsername": { 
      "type": "string", 
      "metadata": { 
        "description": "User name for the Virtual Machine." 
      } 
    }, 
    "adminPassword": { 
      "type": "securestring", 
      "metadata": { 
        "description": "Password for the Virtual Machine." 
      } 
    }, 
    "dnsLabelPrefix": { 
      "type": "string", 
      "defaultValue": "[concat('vm-', uniqueString(resourceGroup().id))]", 
      "metadata": { 
        "description": "Unique DNS Name for the Public IP used to access the Virtual Machine." 
      } 
    }, 
[...]
{ 
      "apiVersion": "2019-12-01", 
      "type": "Microsoft.Compute/virtualMachines", 
      "name": "[variables('vmName')]", 
      "location": "[parameters('location')]", 
      "dependsOn": [ 
        "[variables('storageAccountName')]", 
        "[variables('nicName')]" 
      ], 
      "properties": { 
        "hardwareProfile": { 
          "vmSize": "[parameters('vmSize')]" 
        }, 
        "osProfile": { 
          "computerName": "[variables('vmName')]", 
          "adminUsername": "[parameters('adminUsername')]", 
          "adminPassword": "[parameters('adminPassword')]" 
        }, 

[...]</pre>
  <p><code>Template.parameters.json</code>:</p>
  <pre>{ 
  "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentParameters.json#", 
  "contentVersion": "1.0.0.0", 
  "parameters": { 
    "adminUsername": { 
      "value": "" 
    }, 
    "adminPassword": { 
             ... 
      } 
    } 
  } 
}</pre>
  <p><b>Which two resources should you provision to ensure that the password can be stored securely?</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">Access Policy</li>
    <li>an Azure compliance policy</li>
    <li>an Azure Storage account</li>
    <li class="q-good">Azure Key Vault</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
      <p><i>You must create a new key vault, create the password from there, and then specify the parameters. You must also create a Key Vault access policy to use in the template.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>Your company has a set of resources deployed to an Azure subscription. The resources are deployed to a resource group named app-grp1 by using Azure Resource Manager (ARM) templates.</p>
  <p>You need to verify the date and the time that the resources in app-grp1 were created.</p>
  <p><b>Which blade should you review for app-grp1 in the Azure portal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Deployments</li>
    <li>Diagnostics setting</li>
    <li>Metrics</li>
    <li>Policy</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Navigating to the Diagnostics settings blade provides the ability to diagnose errors or review warnings. Navigating to the Metrics blade provides metrics information (CPU, resources) to users. On the Deployments blade for the resource group (app-grp1), all the details related to a deployment, such as the name, status, date last modified, and duration, are visible. Navigating to the Policy blade only provides information related to the policies enforced on the resource group.</i><p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You have an Azure subscription that contains a resource group named RG1. RG1 contains an Azure virtual machine named VM1.</p>
  <p>You need to use VM1 as a template to create a new Azure virtual machine.</p>
  <p><b>Which three methods can you use to complete the task? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>From Azure Cloud Shell, run the Get-AzVM and New-AzVM cmdlets.</li>
    <li>From Azure Cloud Shell, run the Save-AzDeploymentScriptLog and New-AzResourceGroupDeployment cmdlets.</li>
    <li class="q-good">From Azure Cloud Shell, run the Save-AzDeploymentTemplate and New-AzResourceGroupDeployment cmdlets.</li>
    <li class="q-good">From RG1, select Export template, select Download, and then, from Azure Cloud Shell, run the New-AzResourceGroupDeployment cmdlet.</li>
    <li class="q-good">From VM1, select Export template, and then select Deploy.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>From RG1, selecting the Download option from the Export template page exports the Azure Resource Manager (ARM) template from the resource group properties. You can then deploy the ARM template by running the New-AzResourceGroupDeployment cmdlet.</i></p>
    <p><i>By using the Save-AzDeploymentTemplate cmdlet, you can save the resource ARM template. You can then deploy the ARM template by running the New-AzResourceGroupDeployment cmdlet.</i></p>
    <p><i>From VM1, selecting the Deploy option from the Export template page allows you to deploy a new Azure virtual machine and use the configuration of VM1 as the template.</i></p>
    <p><i>The Save-AzDeploymentScriptLog cmdlet is used to save the log of a deployment script execution.</i></p>
    <p><i>The Get-AzVM cmdlet generates a list of virtual machines that are created in the Azure subscription.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have an Azure subscription that contains a resource group named RG1.</p>
  <p>You have an Azure Resource Manager (ARM) template for an Azure virtual machine.</p>
  <p>You need to use PowerShell to provision a virtual machine in RG1 by using the template.</p>
  <p><b>Which PowerShell cmdlet should you run?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li><code>New-AzManagementGroupDeployment</code></li>
    <li class="q-good"><code>New-AzResourceGroupDeployment</code></li>
    <li><code>New-AzSubscriptionDeployment</code></li>
    <li><code>New-AzVM</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Virtual machines are deployed to resource groups, so you must run the <code>New-AzResourceGroupDeployment</code> cmdlet. You can deploy virtual machines to subscriptions or management groups directly, therefore, <code>New-AzManagementGroupDeployment</code> and <code>New-AzSubscriptionDeployment</code> cannot be used. <code>New-AzVM</code> can be used to provision a new virtual machine, but without using a template.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You plan to deploy an Azure virtual machine based on a basic template stored in the Azure Resource Manager (ARM) library.</p>
  <p><b>What can you configure during the deployment of the template?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>the disk assigned to virtual machine</li>
    <li>the operating system</li>
    <li class="q-good">the resource group</li>
    <li>the size of virtual machine</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>When you deploy a resource by using a template, you can mention the resource group for the deployment. The resource group is a container for Azure resources and makes it easier to manage the resources.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have a Microsoft Entra tenant named contoso.com. Microsoft Entra Connect is configured to sync users to the tenant.</p>
  <p>You need to assign licenses to the users based on Microsoft Entra ID attributes. The solution must minimize administrative effort.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">Assign the licenses to the dynamic groups.</li>
    <li>Assign the licenses to the security groups.</li>
    <li>Create an automatic assignment policy.</li>
    <li class="q-good">Create dynamic groups.</li>
    <li>Create security groups.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>To assign licenses to users based on Microsoft Entra ID attributes, you must create a dynamic security group and configure rules based on custom attributes. The dynamic group must be added to a license group for automatic synchronization. All users in the groups will get the license automatically. Microsoft Entra evaluates the users in the organization that are in scope for an assignment policy rule and creates assignments for the users who don't have assignments to an access package; automatic assignment policies are not used for licensing.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You have a Microsoft Entra tenant that uses Microsoft Entra Connect to sync with an Active Directory Domain Services (AD DS) domain.</p>
  <p>You need to ensure that users can reset their AD DS password from the Azure portal. The users must be able to use two methods to reset their password.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">From Password reset in the Azure portal, configure the Authentication methods settings.</li>
    <li>From Password reset in the Azure portal, configure the Notifications settings.</li>
    <li>From Password reset in the Azure portal, configure the Registration settings.</li>
    <li>Run Microsoft Entra Connect and select Device writeback.</li>
    <li class="q-good">Run Microsoft Entra Connect and select Password writeback.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must run the Microsoft Entra Connect Wizard to enable Password writeback. You must configure the authentication option to enable the two methods required to reset a password.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span>You have a Microsoft Entra tenant.</p>
  <p>You create a new user named User1.</p>
  <p>You need to assign a Microsoft 365 E5 license to User1.</p>
  <p><b>Which user attribute should be configured for User1 before you can assign the license?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>First name</li>
    <li>Last name</li>
    <li>Other email address</li>
    <li class="q-good">Usage location</li>
    <li>User type</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Not all Microsoft 365 services are available in all locations. Before a license can be assigned to a user, you must specify the Usage location. The attributes of First name, Last name, Other email address, and User type are not mandatory for license assignment.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">9.</span>You have an Azure subscription that contains multiple users and administrators.</p>
  <p>You are creating a new custom role by using the following JSON.</p>
  <pre>{ 
  "Name": "Custom Role", 
  "Id": null, 
  "IsCustom": true, 
  "Description": "Custom Role description", 
  "Actions": [ 
    "Microsoft.Compute/*/read", 
    “Microsoft.Compute/snapshots/write”, 
    “Microsoft.Compute/snapshots/read”, 
    "Microsoft.Support/*" 
  ], 
  "NotActions": [ 
  “Microsoft.Compute/snapshots/delete” 
  ], 
  "AssignableScopes": [ 
    "/subscriptions/00000000-0000-0000-0000-000000000000", 
    "/subscriptions/11111111-1111-1111-1111-111111111111" 
  ] 
}</pre>
  <p><b>Which three actions can be performed by a user that is assigned the custom role? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">Call Microsoft Support.</li>
    <li>Create and delete a snapshot.</li>
    <li class="q-good">Create and read a snapshot.</li>
    <li>Create virtual machines.</li>
    <li class="q-good">Read all virtual machine settings.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The role can read all compute resources, call Microsoft support roles, and allow the creation and reading of a snapshot.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">10.</span>You have the following resource groups, management groups, and Azure subscriptions:</p>
  <ul>
    <li>Two resource groups named RG1 and RG2 that are associated with a subscription named 111-222-333 and a management group named MG1</li>
    <li>Two resource groups named RG3 and RG4 that are associated with a subscription named 777-888-999 and a management group named MG1</li>
    <li>Two resource groups named RG5 and RG6 that are associated with a subscription named 444-555-666 and a management group named MG1</li>
    <li>Two resource group named RG10 and RG11 that are associated with a subscription named 222-333-444 and a management group named MG2</li>
    <li>Two resource group named RG11 and RG12 that are associated with a subscription named 555-666-888 and a management group named MG2</li>
  </ul>
  <p>You need to assign a role to a user to ensure the user can view all the resources in the subscriptions. The solution must use the principle of least privilege.</p>
  <p><b>Which role should you assign?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>the Billing Reader role for all the subscriptions</li>
    <li>the Billing Reader role for MG1 and MG2</li>
    <li>the Contributor role for MG1 and MG2</li>
    <li class="q-good">the Reader role for MG1 and MG2</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Assigning the Reader role for MG1 and MG2 is correct because the simplest way to give user access to all resources is to assign a role at the management group level.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">11.</span>You have an Azure subscription that contains several storage accounts.</p>
  <p>You need to provide a user with the ability to perform the following tasks:</p>
  <ul>
    <li>Manage containers within the storage accounts.</li>
    <li>View storage account access keys.</li>
  </ul>
  <p>The solution must use the principle of least privilege.</p>
  <p><b>Which role should you assign to the user?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Owner</li>
    <li>Reader</li>
    <li class="q-good">Storage Account Contributor</li>
    <li>Storage Blob Data Contributor</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Storage Account Contributor allows the management of storage accounts. It provides access to the account key, which can be used to access data via Shared Key authorization. Storage Blob Data Contributor grants permissions to read, write, and delete Azure Storage containers and blobs. Reader allows you to view all resources but does not allow you to make any changes. Owner grants full access to manage all resources, including the ability to assign roles in Azure RBAC.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">12.</span>You have several management groups and Azure subscriptions.</p>
  <p>You want to prevent the accidental deletion of resources.</p>
  <p><b>To which three resource types can you apply delete locks? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>management groups</li>
    <li class="q-good">resource groups</li>
    <li>storage account data</li>
    <li class="q-good">subscriptions</li>
    <li class="q-good">virtual machines</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can use delete locks to block the deletion of virtual machines, subscriptions, and resource groups. You cannot use delete locks on management groups or storage account data.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">13.</span>You have an Azure subscription.</p>
  <p>You plan to create an Azure Policy definition named Policy1.</p>
  <p>You need to include remediation information to indicate when users use Microsoft Defender for Cloud Regulatory and Compliance.</p>
  <p><b>To which definition section should you add remediation information for Policy1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">metadata</li>
    <li>mode</li>
    <li>parameters</li>
    <li>policyRule</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must use the RemediationDescription field in the metadata section from properties to specify a custom recommendation. The remaining options are Azure policies, but do not allow specific custom remediation information.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">14.</span>You have an Azure subscription.</p>
  <p>From PowerShell, you run the <code>Get-MgUser</code> cmdlet for a user and receive the following details:</p>
  <ul>
    <li>Id: 8755b347-3545-3876-3987-999999999999</li>
    <li>DisplayName: Ben Smith</li>
    <li>Mail: bsmith@contoso.com</li>
    <li>UserPrincipalName: bsmith_contoso.com#EXT#@fabrikam.com</li>
  </ul>
  <p><b>Which statement accurately describes the user?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>The user account is disabled.</li>
    <li class="q-good">The user is a guest in the tenant.</li>
    <li>The user is assigned an administrative role.</li>
    <li>The user is deleted.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>For guest users, the user principal name (UPN) will contain the email of the guest user (bsmith_contoso.com) followed by #EXT# followed by the domain name of the tenant (@fabrikam.com). Regular Microsoft Entra users appear in a format of user@fabrikam.com.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">15.</span>Your Microsoft Entra tenant and on-premises Active Directory domain contain multiple users.</p>
  <p>You need to configure self-service password reset (SSPR) password writeback functionality. The solution must minimize costs.</p>
  <p><b>Which Microsoft Entra ID edition should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Microsoft Entra ID Free</li>
    <li class="q-good">Microsoft Entra ID P1</li>
    <li>Microsoft Entra ID P2</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Only Microsoft Entra ID P1 and P2 support SSPR, but Microsoft Entra ID P1 is the lower cost option.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">16.</span>You have an Azure subscription.</p>
  <p>An administrator manages access to resources at the resource group level. The assignment process is automated by running the following PowerShell script nightly.</p>
  <pre>$rg = "RG1" 
$RoleName = "CustomRole1" 
$Role = Get-AzRoleDefinition -Name $RoleName 
New-AzRoleAssignment -SignInName user1@contoso.com `   
    -RoleDefinitionName $Role.Name `
    -ResourceGroupName $rg</pre>
  <p>User1 is unable to access the RG1 resource group. You discover that the script fails to complete for new users.</p>
  <p>You run Get-AzRoleDefinition | Format-Table -Property Name, Id and receive the following information:</p>
  <pre>Name: Custom Role 1, ID: 111-222-333
Name: Owner, ID: 222-333-444
Name: Contributor, ID: 333-444-555
Name: Reader, ID: 666-777-888</pre>
  <p>You need to modify the script to ensure that it does not fail in the future.</p>
  <p><b>What should you change in the script?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li><code>$Role = Add-AzRoleDefinition -Name $RoleName</code></li>
    <li><code>$Role = Get-AzRoleAssignment -Name $RoleName</code></li>
    <li><code>$Role = Set-AzRoleAssignment -Name $RoleName</code></li>
    <li class="q-good"><code>$RoleName = "111-222-333"</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You should use the ID of the role in case the role name was changed to prevent such a change from breaking the script.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">17.</span>You have an Azure subscription and a user named User1.</p>
  <p>You need to assign User1 a role that allows the user to create and manage all types of resources in the subscription. The solution must prevent User1 from assigning roles to other users.</p>
  <p><b>Which Azure role-based access control (RBAC) role should you assign to User1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>API Management Service Contributor</li>
    <li class="q-good">Contributor</li>
    <li>Owner</li>
    <li>Reader</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Users with the Contributor role can create and manage all types of resources but cannot delegate new access to other users. Users with the Reader role can view existing Azure resources but cannot perform any action against them. Users with the API Management Service Contributor role can only manage API Management services and APIs. Users with the Owner role provides full access to all resources, including the right to delegate access to others.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">18.</span>You have an Azure subscription that contains 200 virtual machines.</p>
  <p>You plan to use Azure Advisor to provide cost recommendations when underutilized virtual machines are detected.</p>
  <p>You need to ensure that all Azure admins are notified whenever an Advisor alert is generated. The solution must minimize administrative effort.</p>
  <p><b>What should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">an action group</li>
    <li>an application security group</li>
    <li>an Azure Automation account</li>
    <li>a capacity reservation group</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Whenever Azure Advisor detects a new recommendation for resources, an event is stored in the Azure Activity log. You can set up alerts for these events from Azure Advisor. You can select a subscription and optionally a resource group to specify the resources for which you want to receive alerts. You also need to create an action group that will contain all the users to be notified.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">19.</span>You have an Azure subscription that contains a tenant named contoso.com.</p>
  <p>All users in contoso.com are currently able to invite external users to B2B collaboration.</p>
  <p>You need to ensure that only members of the Guest Inviter, User Administrator, and Global Administrator roles can invite guest users.</p>
  <p><b>What should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Access reviews</li>
    <li>Conditional Access</li>
    <li>Cross-tenant access settings</li>
    <li class="q-good">External collaboration settings</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>External collaboration settings let you specify which roles in your organization can invite external users for B2B collaboration. These settings also include options for allowing or blocking specific domains and options for restricting which external guest users can see in your Microsoft Entra directory.</i></p>
    <p><i>Conditional Access allows you to apply rules to strengthen authentication and block access to resources from unknown locations.</i></p>
    <p><i>Cross-tenant access settings are used to configure collaboration with a specific Microsoft Entra organization.</i></p>
    <p><i>Access reviews are not used to control who can invite guest users.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">20.</span>You have an Azure subscription.</p>
  <p>You run the following command:</p>
  <pre>Get-AzRoleDefinition | Format-Table -Property Name, Id</pre>
  <p>The command output contains data that includes the following:</p>
  <pre>CustomRole1   111-222-333-444-555 
Owner         8e3af657-a8ff-443c-a75c-2fe8c4bcb635 
Contributor   b24988ac-6180-42a0-ab88-20f7382dd24c 
Reader        acdd72a7-3385-48ef-bd42-f606fba81ae7</pre>
  <p>You have a script that manages access to resources at the resource group level. The assignment process is automated by running the following PowerShell script nightly.</p>
  <pre>$rg = "RG1" 
$RoleName = "111-222-333-444-555" 
$Role = Get-AzRoleDefinition -Name $RoleName 
New-AzRoleAssignment -SignInName user1@contoso.com ` 
    -RoleDefinitionName $Role.Name ` 
    -ResourceGroupName $rg</pre>
  <p>User1 is unable to access the RG1 resource group. You discover that the script fails to complete for User1.</p>
  <p>You need to modify the script to ensure that it does not fail.</p>
  <p><b>What should you change in the script?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li><code>$Role = Add-AzRoleDefinition -Name $RoleName</code></li>
    <li><code>$Role = Get-AzRoleAssignment -Name $RoleName</code></li>
    <li><code>$Role = Set-AzRoleAssignment -Name $RoleName</code></li>
    <li class="q-good"><code>$RoleName = "CustomRole1"</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p>Missed explanation, but OK.</p>
  </div>
</div>

---

<div>
  <p><span class="k">21.</span>You have an Azure subscription that contains multiple virtual machines.</p>
  <p>You need to ensure that a user named User1 can view all the resources in a resource group named RG1. You must use the principle of least privilege.</p>
  <p><b>Which role should you assign to User1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Billing Reader</li>
    <li>Contributor</li>
    <li class="q-good">Reader</li>
    <li>Tag Contributor</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The Reader role allows you to view all the resources but does not allow you to make any changes. The Contributor role allows you to manage all the resources, the Billing Reader role provides read access only to billing data, and the Tag Contributor role allows you to manage entity tags without providing access to the entities themselves.</i></p>
  </div>
</div>

---


<a name="2"></a>
## 2. Azure Networking

---

<div>
  <p><span class="k">1.</span>You have an Azure virtual network that contains two subnets named Subnet1 and Subnet2. You have a virtual machine named VM1 that is connected to Subnet1. VM1 runs Windows Server.</p>
  <p>You need to ensure that VM1 is connected directly to both subnets.</p>
  <p><b>What should you do first?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">From the Azure portal, add a network interface.</li>
    <li>From the Azure portal, create an IP group.</li>
    <li>From the Azure portal, modify the IP configurations of an existing network interface.</li>
    <li>Sign in to Windows Server and create a network bridge.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>A network interface is used to connect a virtual machine to a subnet. Since VM1 is connected to Subnet1, VM1 already has a network interface attached that is connected to Subnet1. To connect VM1 directly to Subnet2, you must create a new network interface that is connected to Subnet2. Next, you must attach the new network interface to VM1.</i></p>
    <p><i>An IP group is a user-defined collection of static IP addresses, ranges, and subnets. A network bridge allows you to connect multiple existing network connection in Windows together. Changing the IP configurations of the existing network interface results in VM1 being connected to Subnet2 but not to Subnet1.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You have an Azure subscription that contains the following virtual networks:</p>
  <ul>
    <li>VNet1 has an IP address range of 192.168.0.0/24.</li>
    <li>VNet2 has an IP address range of 10.10.0.0/24.</li>
    <li>VNet3 has an IP address range of 192.168.0.0/16.</li>
  </ul>
  <p>You need configure virtual network peering.</p>
  <p><b>Which two peerings can you create? Each correct answer presents complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">VNet1 can be peered with VNet2.</li>
    <li>VNet1 can be peered with VNet3.</li>
    <li class="q-good">VNet2 can be peered with VNet3.</li>
    <li>VNet3 can be peered with VNet1.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>VNet1 and VNet2 have non-overlapping IP addresses. For virtual network peering, both virtual networks must have non-overlapping IP addresses.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You have an Azure subscription that contains two resource groups named RG1 and RG2.</p>
  <p>RG1 contains the following resources:</p>
  <ul>
    <li>A virtual network named VNet1 located in the East US Azure region</li>
    <li>A network security group (NSG) named NSG1 located in the West US Azure region</li>
  </ul>
  <p>RG2 contains the following resources:</p>
  <ul>
    <li>A virtual network named VNet2 located in the East US Azure region</li>
    <li>A virtual network named VNet3 located in the West US Azure region</li>
  </ul>
  <p>You need to apply NSG1.</p>
  <p><b>To which subnets can you apply NSG1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>the subnets of all the virtual networks</li>
    <li>the subnets of VNet1 only</li>
    <li>the subnets of VNet1 and VNet2</li>
    <li class="q-good">the subnets of VNet3 only</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can assign an NSG to the subnet of the virtual network in the same region as the NSG and NSG1 is in the West US region.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have an Azure subscription that contains a network security group (NSG) named NSG1.</p>
  <p>You plan to configure NSG1 to allow the following types of traffic:</p>
  <ul>
    <li>Remote Desktop Management</li>
    <li>Secured HTTPS</li>
  </ul>
  <p><b>Which two ports should you allow in NSG1? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>80</li>
    <li>25</li>
    <li class="q-good">443</li>
    <li>587</li>
    <li class="q-good">3389</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must open port 443 to secured HTTPS traffic, port 3389 for Remote Desktop, and 587 to send outbound email by using authenticated SMTP relay. Port 80 is used for unsecured traffic. Port 25 is used by mail traffic.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You have three network security groups (NSGs) named NSG1, NSG2, and NSG3. Port 80 is blocked in NSG3 and allowed in NSG1 and NSG2.</p>
  <p>You have four Azure virtual machines that have the following configurations:</p>
  <p>VM1:</p>
  <ul>
    <li>Subnet: Subnet1</li>
    <li>Network card: NIC1</li>
    <li>NIC1 is assigned to NSG2.</li>
  </ul>
  <p>VM2:</p>
  <ul>
    <li>Subnet: Subnet1</li>
    <li>Network card: NIC2</li>
    <li>NIC2 is assigned to NSG3.</li>
  </ul>
  <p>VM3:</p>
  <ul>
    <li>Subnet: Subnet3</li>
    <li>Network card: NIC3</li>
    <li>NIC3 is assigned to NSG3.</li>
  </ul>
  <p>VM4:</p>
  <ul>
    <li>Subnet: Subnet2</li>
  </ul>
  <p>You have the following subnets:</p>
  <ul>
    <li>Subnet1 is assigned to NSG1.</li>
    <li>Subnet2 is assigned to NSG3.</li>
    <li>Subnet 3 does not have an NSG assigned.</li>
  </ul>
  <p><b>Which virtual machine will allow traffic from the internet on port 80?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">VM1</li>
    <li>VM2</li>
    <li>VM3</li>
    <li>VM4</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>On VM1, both NSGs assigned to Subnet1 and the NIC1 card allow traffic on port 80. On VM2, NSG1 allows traffic, but NSG3 blocks traffic for the network interface. On VM3 and VM4, NSG3 blocks traffic.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have an Azure subscription that contains two virtual networks named VNet1 and VNet2.</p>
  <p>You need to ensure that the resources on both VNet1 and VNet2 can communicate seamlessly between both networks.</p>
  <p><b>What should you configure from the Azure portal?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>connected devices</li>
    <li>firewall</li>
    <li class="q-good">peerings</li>
    <li>service endpoints</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can connect virtual networks to each other with virtual network peering. Once the virtual networks are peered, the resources on both virtual networks can communicate with each other with the same latency and bandwidth as though the resources were on the same virtual network.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You have an Azure subscription that contains a virtual network named VNet1.</p>
  <p>You plan to deploy a virtual machine named VM1 to be used as a network inspection appliance.</p>
  <p>You need to ensure that all network traffic passes through VM1.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Configure a user-defined route.</li>
    <li>Create a virtual network gateway.</li>
    <li>Modify the default route.</li>
    <li>Modify the system route.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure automatically creates a route table for each subnet on an Azure virtual network and adds system default routes to the table. You can override some of the Azure system routes with custom user-defined routes and add more custom routes to route tables. Azure routes outbound traffic from a subnet based on the routes on a subnet's route table.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span>You have an Azure subscription that contains a virtual network named VNet1 and a virtual machine named VM1.</p>
  <p>VM1 can only be accessed from the internal network.</p>
  <p>An external contractor needs access to VM1. The solution must minimize administrative effort.</p>
  <p><b>What should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">a public IP address</li>
    <li>a second private IP address</li>
    <li>a Site-to-Site (S2S) VPN</li>
    <li>Azure Firewall</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>To share a virtual machine with an external user, you must add a public IP address to the virtual machine. An additional IP address or firewall configuration will not help in this case. Configuring a S2S VPN does not have minimal administrative effort.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">9.</span>You have an Azure subscription that contains a virtual network named VNet1.</p>
  <p>You plan to enable VNet1 connectivity to on-premises resources by using an encrypted connection.</p>
  <p><b>What should you configure for VNet1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>a private endpoint connection</li>
    <li>a public IP address</li>
    <li class="q-good">a virtual network gateway</li>
    <li>internet routing</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>A VPN gateway is a type of virtual network gateway that sends encrypted traffic between a virtual network and an on-premises location across a public connection. You can also use a VPN gateway to send traffic between virtual networks across the Azure backbone. A VPN gateway connection relies on the configuration of multiple resources, each of which contains configurable settings.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">10.</span>You have an Azure subscription that contains an Azure DNS zone named contoso.com.</p>
  <p>You add a new subdomain named test.contoso.com.</p>
  <p>You plan to delegate test.contoso.com to a different DNS server.</p>
  <p><b>How should you configure the domain delegation?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Add an A record for test.contoso.com.</li>
    <li class="q-good">Add an NS record set named test to the contoso.com zone.</li>
    <li>Create the SOA record for test.contoso.com.</li>
    <li>Modify the A record for contoso.com.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must create a DNS NS record set named test in the contoso.com zone. An NS zone must be created at the apex of the zone named contoso.com. You do not need to create the SOA record set in test.contoso.com. It must only be created in contoso.com. You do not need to create or modify the DNS A record.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">11.</span>You have an Azure subscription that contains four virtual machines. Each virtual machine is connected to a subnet on a different virtual network.</p>
  <p>You install the DNS Server role on a virtual machine named VM1.</p>
  <p>You configure each virtual network to use the IP address of VM1 as the DNS server.</p>
  <p>You need to ensure that all four virtual machines can resolve IP addresses by using VM1.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Configure a DNS server on all four virtual machines.</li>
    <li class="q-good">Configure network peering.</li>
    <li>Create and associate a route table to all four subnets.</li>
    <li>Create Site-to-Site (S2S) VPNs.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>By default, Azure virtual machines can communicate only with other virtual machines that are connected to the same virtual network. If you want a virtual machine to communicate with other virtual machines that are connected to other virtual networks, you must configure network peering.</i></p>
    <p><i>A route table controls how network traffic is routed. But without network peering, network traffic is still limited to single virtual network.</i></p>
    <p><i>Configuring a Site-to-Site (S2S) VPN is incorrect because you are not connecting on-premises virtual machines to the cloud.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">12.</span>You have an Azure virtual network named VNet1.</p>
  <p>You create an Azure Private DNS zone named contoso.com.</p>
  <p>You need to ensure that the virtual machines on VNet1 register in the contoso.com private DNS zone.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Add a virtual network link to contoso.com.</li>
    <li>Add Azure DNS Private Resolver to VNet1.</li>
    <li>Configure each virtual machine to use a custom DNS server.</li>
    <li>Configure VNet1 to use a custom DNS server.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>To associate a virtual network to a private DNS zone, you add the virtual network to the zone by creating a virtual network link.</i></p>
    <p><i>Azure DNS Private Resolver is used to proxy DNS queries between on-premises environments and Azure DNS.</i></p>
    <p><i>A custom DNS server will work if you deploy a DNS server as a virtual machine or an appliance, however, this configuration does not work with a private DNS zone.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">13.</span>You have an Azure subscription that contains a web app named App1.</p>
  <p>You configure App1 with a custom domain name of webapp1.contoso.com.</p>
  <p>You need to create a DNS record for App1. The solution must ensure that App1 remains accessible if the IP address changes.</p>
  <p><b>Which type of DNS record should you create?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>A</li>
    <li class="q-good">CNAME</li>
    <li>SOA</li>
    <li>SRV</li>
    <li>TXT</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>For web apps, you create either an A (Address) record or a CNAME (Canonical Name) record. An A record maps a domain name to an IP address. A CNAME record maps a domain name to another domain name. DNS uses the second name to look up the address. Users still see the first domain name in their browser. If the IP address changes, a CNAME entry is still valid, whereas an A record must be updated.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">14.</span>You have an Azure subscription that contains network security groups (NSGs).</p>
  <p><b>Which two resources can be associated with a NSG? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Azure Monitor</li>
    <li>Azure Network Watcher</li>
    <li class="q-good">network interfaces</li>
    <li class="q-good">subnets</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can use a network security group (NSG) to be assigned to a network interface. NSGs can be associated with subnets or individual virtual machine instances within that subnet. When an NSG is associated with a subnet, the access control list (ACL) rules apply to all virtual machine instances of that subnet.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">15.</span>You have an Azure virtual network that contains four subnets. Each subnet contains 10 virtual machines.</p>
  <p>You plan to configure a network security group (NSG) that will allow inbound traffic over TCP port 8080 to two virtual machines on each subnet. The NSG will be associated to each subnet.</p>
  <p>You need to recommend a solution to configure the inbound access by using the fewest number of NSG rules possible.</p>
  <p><b>What should you use as the destination in the NSG?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">an application security group</li>
    <li>a service tag</li>
    <li>the subnets of the virtual machines</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Application security groups allow you to group together the network interfaces from multiple virtual machines, and then use the group as the source or destination in an NSG rule. The network interfaces must be in the same virtual network.</i></p>
    <p><i>You can use the IP address of each virtual machine as the destination, but you must create a rule for each virtual machine.</i></p>
    <p><i>Using the subnets will require four rules and will also allow traffic to all the virtual machines on those subnets.</i></p>
    <p><i>Service tags are for specific Azure services, such as Azure App Service or Azure Backup.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">16.</span>You have a virtual machine named VM1 that is assigned to a network security group (NSG) named NSG1.</p>
  <p>NSG1 has the following outbound security rules:</p>
  <p>Rule1:</p>
  <ul>
    <li>Priority: 900</li>
    <li>Name: BlockInternet</li>
    <li>Port: 80</li>
    <li>Protocol: TCP</li>
    <li>Source: Any</li>
    <li>Destination: Any</li>
    <li>Action: Block</li>
  </ul>
  <p>asdf</p>
  <ul>
    <li>Priority: 1000</li>
    <li>Name: AllowInternet</li>
    <li>Port: 80</li>
    <li>Protocol: TCP</li>
    <li>Source: Any</li>
    <li>Destination: Any</li>
    <li>Action: Allow</li>
  </ul>
  <p>You need to ensure that internet access to VM1 on port 80 is allowed.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Change the action of Rule2.</li>
    <li>Change the name of Rule1.</li>
    <li class="q-good">Change the priority of Rule2.</li>
    <li>Change the source of Rule 2.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Rule1 has higher priority, so the action will be blocked. You can increase the priority of Rule2, decrease the priority of Rule1, or change the action of Rule1 to achieve the goal.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">17.</span>You have an Azure subscription that contains an ASP.NET application. The application is hosted on four Azure virtual machines that run Windows Server.</p>
  <p>You have a load balancer named LB1 to load balances requests to the virtual machines.</p>
  <p>You need to ensure that site users connect to the same web server for all requests made to the application.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Configure an inbound NAT rule.</li>
    <li class="q-good">Set Session persistence to Client IP.</li>
    <li>Set Session persistence to None.</li>
    <li class="q-good">Set Session persistence to Protocol.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>By setting Session persistence to Client IP and Protocol, you ensure that site users connect to the same web server for all requests made to the application. Setting Session persistence to None disables sticky sessions and an inbound NAT rule is used to forward traffic from a load balancer frontend to a backend pool.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">18.</span>You deploy web servers to two virtual machines named VM1 and VM2 in an availability set named AVSet1.</p>
  <p>You need to configure Azure Load Balancer with a backend pool of VM1 and VM2. The solution must minimize costs.</p>
  <p><b>Which SKU should you use for the Azure Load Balancer configuration?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Azure Standard Load Balancer with Basic SKU public IP</li>
    <li>Azure Standard Load Balancer with Standard SKU public IP</li>
    <li class="q-good">Basic Azure Load Balancer with Basic SKU public IP</li>
    <li>Basic Azure Load Balancer with Standard SKU public IP</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Basic Azure Load Balancer supports deployment in a single availability zone. Basic Azure Load Balancer supports only Basic SKU public IP. Azure Standard Load Balancer is zone-redundant, but has a higher cost.</i></p>
  </div>
</div>

---


<a name="3"></a>
## 3. Azure Storage

---

<div>
  <p><span class="k">1.</span>You need to generate the shared access signature (SAS) token required to authorize a request to a resource.</p>
  <p><b>Which two parameters are required for the SAS token? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li><code>SignedIP (sip)</code></li>
    <li class="q-good"><code>SignedResourceTypes (srt)</code></li>
    <li class="q-good"><code>SignedServices (ss)</code></li>
    <li><code>SignedStart (st)</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i><code>SignedServices (ss)</code> is required to refer blobs, queues, tables, and files. <code>SignedResourceTypes (srt)</code> is required to refer services, containers, or objects. <code>SignedStart (st)</code> is an optional parameter that refers to the time when the SAS becomes valid. If unmentioned, the start time is assumed to be the time when the storage service receives the request. <code>SignedIP (sip)</code> is an optional parameter that refers to the range of IP addresses from which to accept requests.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You need to create an Azure Storage account that supports the Azure Data Lake Storage Gen2 capabilities.</p>
  <p><b>Which two types of storage accounts can you use? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">premium block blobs</li>
    <li>premium file shares</li>
    <li class="q-good">standard general-purpose v2</li>
    <li>premium page blobs</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>To support Data Lake Storage, the storage account must support blob storage, which is available as standard general-purpose v2 and premium block blobs. Additionally, when you create the storage account, you must enable the hierarchical namespace.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You need to create an Azure Storage account that meets the following requirements:</p>
  <ul>
    <li>Stores data in multiple Azure regions</li>
    <li>Supports reading the data from primary and secondary regions</li>
  </ul>
  <p><b>Which type of storage redundancy should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>geo-redundant storage (GRS)</li>
    <li>locally-redundant storage (LRS)</li>
    <li class="q-good">read-access geo-redundant storage (RA-GRS)</li>
    <li>zone-redundant storage (ZRS)</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Since you must ensure that data can be read from a secondary region, you must choose read-access geo-redundant storage (RA-GRS).</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have an Azure subscription that contains an Azure Storage account named <code>vmstorageaccount1</code>.</p>
  <p>You create an Azure container instance named <code>container1</code>.</p>
  <p>You need to configure persistent storage for <code>container1</code>.</p>
  <p><b>What should you create in  <code>vmstorageaccount1</code>?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>a blob container</li>
    <li class="q-good">a file share</li>
    <li>a queue</li>
    <li>a table</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>An Azure container instance (Docker container) can mount Azure File Storage shares as directories and use them as persistent storage. An Azure container instance cannot mount and use as persistent storage blob containers, queues and tables.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You have an Azure Storage account named <code>corpimages</code> and an on-premises shared folder named <code>\\server1\images</code>.</p>
  <p>You need to migrate all the contents from <code>\\server1\images</code> to <code>corpimages</code>.</p>
  <p><b>Which two commands can you use?</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good"><code>Azcopy copy \\server1\images https://corpimages.blob.core.windows.net/public -recursive</code></li>
    <li><code>Azcopy sync \\server1\images https://corpimages.blob.core.windows.net/public -recursive</code></li>
    <li class="q-good"><code>Get-ChildItem -Path \\server1\images -Recurse | Set-AzStorageBlobContent -Container " corpimages"</code></li>
    <li><code>Set-AzStorageBlobContent -Container "ContosoUpload" -File "\\server1\images" -Blob " corporateimages "</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The AzCopy command allows you to copy all files to a storage account. You then use Get-ChildItem with the path parameter, recurse to select everything, and then use the Set-AzureStorageBlobContent cmdlet.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have an Azure Storage account.</p>
  <p>You need to copy data to the storage account by using the AzCopy tool.</p>
  <p><b>Which two types of data storage are supported by AzCopy? Each correct answer presents a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">blob</li>
    <li class="q-good">file</li>
    <li>queue</li>
    <li>table</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can provide authorization credentials by using Microsoft Entra, or by using a shared access signature (SAS) token. Both storage types, blob and file, are supported in AzCopy.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You have two premium block blob Azure Storage accounts named storage1 and storage2.</p>
  <p>You need to configure object replication from storage1 to storage2.</p>
  <p><b>Which three features should be enabled before configuring object replication? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">blob versioning for storage1</li>
    <li class="q-good">blob versioning for storage2</li>
    <li class="q-good">change feed for storage1</li>
    <li>change feed for storage2</li>
    <li>point-in-time restore for the containers on storage1</li>
    <li>point-in-time restore for the containers on storage2</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Object replication can be used to replicate blobs between storage accounts. Before configuring object replication, you must enable blob versioning for both storage accounts, and you must enable the change feed for the source account.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span>You have an Azure subscription that contains multiple storage accounts.</p>
  <p>A storage account named storage1 has a file share that stores marketing videos. Users reported that 99 percent of the assigned storage is used.</p>
  <p>You need to ensure that the file share can support large files and store up to 100 TiB.</p>
  <p><b>Which two PowerShell commands should you run? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li><code>New-AzRmStorageShare -ResourceGroupName RG1 -Name -StorageAccountName storage1 -Name share1 -QuotaGiB 100GB</code></li>
    <li class="q-good"><code>Set-AzStorageAccount -ResourceGroupName RG1 -Name storage1 -EnableLargeFileShare</code></li>
    <li><code>Set-AzStorageAccount -ResourceGroupName RG1 -Name storage1 -Type "Standard_RAGRS"</code></li>
    <li class="q-good"><code>Update-AzRmStorageShare -ResourceGroupName RG1 -Name -StorageAccountName storage1 -Name share1 -QuotaGiB 102400</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must enable the storage account to support large files and update the storage account quota to 102,400 GB. You do not need to change the type of storage account, and you are updating the existing share.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">9.</span>You create an Azure Storage account.</p>
  <p>You need to create a lifecycle management rule to move blobs to Cool storage if the blobs have not been used for 30 days.</p>
  <p><b>What should you do first?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Enable access tracking.</li>
    <li>Enable versioning for blobs.</li>
    <li>Refresh the blob inventory.</li>
    <li>Rotate the storage account keys.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>A lifecycle management rule can be used to move or delete blobs automatically. The rule can be based on the time the blob was last modified or the time the blob was last accessed (read or write). To perform an action based on the access time, access tracking must be enabled. This can incur additional storage costs.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">10.</span>You have an Azure Storage account that contains a file share.</p>
  <p>Several users work from a secure location that limits outbound traffic to the internet.</p>
  <p>You need to ensure that the users at the secure location can access the file share in Azure by using SMB protocol.</p>
  <p><b>Which outbound port should you allow from the secure location?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>80</li>
    <li>443</li>
    <li class="q-good">445</li>
    <li>5671</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>For accessing the file share, port 445 must be open. Port 5671 is used to send health information to Microsoft Entra. It is recommended, but not required, in the latest versions. Port 80 is used to download certificate revocation lists (CRLs) to verify TLS/SSL certificates. Port 443 is used for https traffic, for example to sync AD DS with Microsoft Entra.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">11.</span>You have an Azure Storage account named storage1.</p>
  <p>You plan to store long-term backups in storage1. The solution must minimize costs.</p>
  <p><b>Which storage tier should you use for the backups?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Archive</li>
    <li>Cold</li>
    <li>Cool</li>
    <li>Hot</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Archive is an offline tier that is optimized for storing data that is rarely accessed and has flexible latency requirements. Data in the Archive tier must be stored for a minimum of 180 days.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">12.</span>Your development team plans to deploy an Azure container instance. The container needs a persistent storage layer.</p>
  <p><b>Which service should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Azure Blob storage</li>
    <li class="q-good">Azure Files</li>
    <li>Azure Queue Storage</li>
    <li>Azure SQL Database</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can persist data for Azure Container Instances with the use of Azure Files. Azure Files offers fully managed file shares hosted in Azure Storage that are accessible via the industry standard Server Message Block (SMB) protocol.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">13.</span>You have an Azure subscription that contains a storage account named storage1.</p>
  <p>You need to provide storage1 with access to a partner organization. Access to storage1 must expire after 24 hours.</p>
  <p><b>What should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">a shared access signature (SAS)</li>
    <li>an access key</li>
    <li>Azure Content Delivery Network (CDN)</li>
    <li>lifecycle management</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>A SAS provides secure delegated access to resources in a storage account. With a SAS, you have granular control over how a client can access data, including time restrictions.</i></p>
    <p><i>Access keys and Azure CDN provide permanent access to resources. They will require manual steps to remove access. Lifecycle management is not needed.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">14.</span>You have an Azure subscription that contains a storage account named storage1.</p>
  <p>You need to ensure that public network access is disabled from all networks, including the internet.</p>
  <p><b>What should you configure on storage1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Access keys</li>
    <li>Data protection</li>
    <li>Encryption</li>
    <li class="q-good">Networking</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The Networking node of a storage account provides settings to configure public network access and network routing. To disable public network access, you can disable public network access, or configure the access to only allow specific virtual networks and IP addresses.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">15.</span>You have an Azure subscription.</p>
  <p>You plan to create a storage account named storage1 to store images.</p>
  <p>You need to replicate the images to a new storage account.</p>
  <p><b>What are three requirements of storage1? Each correct answer presents part of a complete solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">a container</li>
    <li>a file share</li>
    <li class="q-good">blob versioning</li>
    <li>queues</li>
    <li class="q-good">standard general-purpose v2</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Versioning must be enabled for the source and target. An object type container is needed to replicate the images. You must create a StandardV2 storage account. File shares are not needed, and queues are unsupported for replication.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">16.</span>You plan to configure object replication between two Azure Storage accounts.</p>
  <p>The Blob service of the source storage account has the following settings:</p>
  <ul>
    <li>Hierarchical namespace: Disabled</li>
    <li>Default access tier: Hot</li>
    <li>Blob public access: Enabled</li>
    <li>Blob soft delete: Enabled (7 days)</li>
    <li>Container soft delete: Enabled (7 days)</li>
    <li>Versioning: Disabled</li>
    <li>Change feed: Enabled</li>
    <li>NFS v3: Disabled</li>
    <li>Allow cross-tenant replication: Enabled</li>
  </ul>
  <p><b>Which setting should be modified on the source storage account to support object replication?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Blob soft delete</li>
    <li>Change feed</li>
    <li>Hierarchical namespace</li>
    <li class="q-good">Versioning</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Versioning must be enabled for both the source and destination accounts. In this scenario, versioning is currently disabled.</i></p>
  </div>
</div>

---


<a name="4"></a>
## 4. Azure Virtual Machines

---

<div>
  <p><span class="k">1.</span>You have an Azure virtual machine.</p>
  <p>You receive a notification that the virtual machine is going to be affected by an underlying maintenance activity on the physical infrastructure.</p>
  <p>You need to move the virtual machine to a different host to avoid a service interruption.</p>
  <p><b>What should you do?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Apply an Azure policy.</li>
    <li>Apply an Azure tag.</li>
    <li>Move the virtual machine to another Azure subscription.</li>
    <li class="q-good">Redeploy the virtual machine.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must redeploy the virtual machine, which can move the virtual machine to a different host. Azure will shut down the virtual machine and move the virtual machine to a new node within the Azure infrastructure.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You are deploying a virtual machine by using an availability set in the East US Azure region.</p>
  <p>You have deployed 18 virtual machines in two fault domains and 10 update domains.</p>
  <p>Microsoft performed planned physical hardware maintenance in the East US region.</p>
  <p><b>What is the maximum number of virtual machines that will be unavailable?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>2</li>
    <li class="q-good">8</li>
    <li>9</li>
    <li>18</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>18 virtual machines are shared across 10 update domains. The first 10 virtual machines go to 10 update domains, so eight update domains will have two virtual machines. When there is physical hardware maintenance, some virtual machines will be unavailable based on their configuration. If there was a rack failure, then 18 virtual machines will be distributed to two fault domains with nine virtual machines each.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You are creating an Azure virtual machine that will run Windows Server.</p>
  <p>You need to ensure that VM1 will be part of a virtual machine scale set.</p>
  <p><b>Which setting should you configure during the creation of the virtual machine?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Availability options</li>
    <li>Azure Spot instance</li>
    <li>Management</li>
    <li>Region</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must configure the virtual machine scale set from the availability options. Azure spot instance is used to add virtual machines with a discounted price. Region will not affect the configuration of the availability options. The management setting allows you to configure the monitoring and management options for the virtual machine.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have an Azure subscription that contains a resource group named RG1. RG1 contains a virtual machine that runs daily reports.</p>
  <p>You need to ensure that the virtual machine shuts down when resource group costs exceed 75 percent of the allocated budget.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Create an action group of type Runbook, and then select Scale Up VM.</li>
    <li class="q-good">Create an action group of type Runbook, and then select <b>Stop VM</b> as an action.</li>
    <li>From Cost Management + Billing, create a new cost analysis.</li>
    <li class="q-good">From Cost Management + Billing, modify the Budgets settings.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must go to Cost Management + Billing, and then Budgets to edit the budget associated with the resource group resources. You must also create a new action group of the Runbook type, and then choose Stop VM as an action. The cost analysis will not stop the virtual machine from running and the Scale Up VM action group is not required.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You have an Azure subscription that contains hundreds of virtual machines that were migrated from a local datacenter.</p>
  <p>You need to identify which virtual machines are underutilized.</p>
  <p><b>Which Azure Advisor settings should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">Cost</li>
    <li>High Availability</li>
    <li>Operational Excellence</li>
    <li>Performance</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The Cost blade allows you to optimize and reduce your overall Azure spending. You can use this to identify the virtual machines that are underutilized. The Performance blade allows you to improve the speed of your applications. High availability is unavailable via Azure Advisor. Operational Excellence helps you achieve process and workflow efficiency, resource manageability, and deployment best practices.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have an Azure subscription that contains 25 virtual machines.</p>
  <p>You need to ensure that each virtual machine is associated to a specific department for reporting purposes.</p>
  <p><b>What should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>administrative units</li>
    <li>management groups</li>
    <li>storage accounts</li>
    <li class="q-good">tags</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Tags are metadata elements that can be applied to Azure resources. Tags can be used for tracking resources such as virtual machines and associating each resource to a department for billing and reporting purposes.</i></p>
    <p><i>Administrative units are containers used for delegating administrative roles to manage a specific portion of Microsoft Entra. Administrative units cannot contain Azure virtual machines.</i></p>
    <p><i>Management groups are containers that can be used to manage access, policy, and compliance across multiple Azure subscriptions.</i></p>
    <p><i>Azure Storage accounts contain Azure Storage data objects, including blobs, file shares, queues, tables, and disks. A storage account cannot contain virtual machines.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You have an Azure subscription and an on-premises Hyper-V virtual machine named VM1. VM1 contains a single virtual disk.</p>
  <p>You plan to use VM1 as a template to deploy 25 new Azure virtual machines.</p>
  <p>You need to upload VM1 to Azure.</p>
  <p><b>Which cmdlet should you run?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good"><code>Add-AzVhd</code></li>
    <li><code>New-AzDataShare</code></li>
    <li><code>New-AzDisk</code></li>
    <li><code>New-AzVM</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i><code>Add-AzVhd</code>: Uploads an on-premises VHD to Azure</i></p>
    <p><i><code>Add-AzVM</code>: Used to create a new virtual machine</i></p>
    <p><i><code>Add-AzDisk</code>: Used to create a managed disk</i></p>
    <p><i><code>Add-AzDataShare</code>: Used to create an Azure data share</i></p>
  </div>
</div>

---





<a name="5"></a>
## 5. Azure PaaS Compute Options

---

<div>
  <p><span class="k">1.</span>You have an Azure subscription that contains an Azure container app named <code>cont1</code>.</p>
  <p>You plan to add scaling rules to <code>cont1</code>.</p>
  <p>You need to ensure that <code>cont1</code> replicas are created based on received messages in Azure Service Bus.</p>
  <p><b>Which scale trigger should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>CPU usage</li>
    <li class="q-good">event-driven</li>
    <li>HTTP traffic</li>
    <li>memory usage</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Container Apps allows a set of triggers to create new instances, called replicas. For Azure Service Bus, an event-driven trigger can be used to run the escalation method. The remaining scale triggers cannot use a scale rule based on messages in an Azure service bus.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You have a Basic Azure App Service plan that contains a web app.</p>
  <p>You need to ensure that the web app can scale automatically when the CPU percentage goes beyond 80 percent for a duration of 15 minutes.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Configure a deployment slot.</li>
    <li class="q-good">Configure a scaling condition to scale based on a metric, and then add the rules.</li>
    <li>Configure a scaling condition to scale based on an instance count, and then set the instance count.</li>
    <li>Scale out the App Service plan.</li>
    <li class="q-good">Scale up the App Service plan.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Scale up the web app by adding more CPU, memory, and disk space to fulfill the requirement. Increase the number of virtual machine instances that run the app. The scale settings take only seconds to apply and affect all the apps in the App Service plan. Then, you must set up a scaling condition with the required metrics to scale up/down and scale out/in when certain thresholds are met.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You need to create an Azure App Service web app that runs on Windows. The web app requires scaling to five instances, 45 GB of storage, and a custom domain name. The solution must minimize costs.</p>
  <p><b>Which App Service plan should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Basic</li>
    <li>Free</li>
    <li>Premium</li>
    <li class="q-good">Standard</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The Standard service plan can host unlimited web apps, up to 50 GB of disk space, and up to 10 instances. The plan will cost approximately $0.10/hour. The Free plan only offers 1 GB of disk size and 0 instances to host the app. The Premium plan offers 250 GB of disk space and up to 30 instances and will cost approximately $0.20/hour. The Basic plan offers 10 GB of disk space and up to three virtual machines.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have an Azure subscription that contains a container app named App1. App1 is configured to use cached data.</p>
  <p>You plan to create a new container.</p>
  <p>You need to ensure that the new container automatically refreshes the cache used by App1.</p>
  <p><b>Which type of container should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>blob</li>
    <li>init</li>
    <li>privileged</li>
    <li class="q-good">sidecar</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Container Apps manages the details of Kubernetes and container orchestration. Containers in Azure Container Apps can use any runtime, programming language, or development stack of your choice. You can define multiple containers in a single container app to implement the sidecar pattern, for example, an agent that reads logs from the primary app container in a shared volume and forwards them to a logging service.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You have an Azure subscription that contains a Docker container image named container1.</p>
  <p>You plan to create a new Azure web app named WebApp1.</p>
  <p>You need to ensure that you can use container1 for WebApp1.</p>
  <p><b>Which WebApp1 setting should you configure?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Continuous deployment</li>
    <li>Pricing plan</li>
    <li class="q-good">Publish</li>
    <li>Runtime stack</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>If you want to run a Docker container as an Azure web service, you must configure the Publish option and select Docker container.</i></p>
    <p><i>Runtime stack specifies the stack that you want to use for the web app. If you want to deploy a Docker container as web app, the runtime stack option is unavailable.</i></p>
    <p><i>Pricing plan specifies the location, features, and costs of the web app.</i></p>
    <p><i>Continuous deployment is a strategy for software releases. This option is unavailable when you publish a Docker container as an Azure web app.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have an Azure subscription that contains multiple resource groups and Azure App Service web apps. A resource group named RG1 hosts a web app named appservice1. The App Service uses an imported SSL certificate.</p>
  <p>You create a resource group named RG2.</p>
  <p>You plan to move all the resources in RG1 to RG2.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Create a new App Service plan in RG2.</li>
    <li>Create a new web app in RG2.</li>
    <li class="q-good">Delete the SSL Certificate from RG1 and upload it to RG2.</li>
    <li class="q-good">Move all the resources from RG1 to RG2.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The SSL certificate must be deleted. You will have to move all other resources to RG2.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You have an Azure subscription that contains a resource group named RG1. RG1 contains an application named App1 and a container app named containerapp1.</p>
  <p>App1 is experiencing performance issues when attempting to add messages to the containerapp1 queue.</p>
  <p>You need to create a job to perform an application resource cleanup when a new message is added to a queue.</p>
  <p><b>Which command should you run?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good"><code>az containerapp job create \ --name "my-job" --resource-group "RG1" -trigger-type "Event" \ -replica-timeout 60 --replica-retry-limit 1 ...</code></li>
    <li><code>az containerapp job create \ --name "my-job" --resource-group " RG1" -trigger-type "Manual" \ -replica-timeout 60 --replica-retry-limit 1 ...</code></li>
    <li><code>az containerapp job start \ --name "my-job" --resource-group " RG1" -trigger-type "Schedule" \ -replica-timeout 60 --replica-retry-limit 1 ...</code></li>
    <li><code>az containerapp job start \ --name "my-job" --resource-group " RG1" -trigger-type "Event" \ -replica-timeout 60 --replica-retry-limit 1 ...</code></li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Container Apps jobs enable you to run containerized tasks that execute for a finite duration, and then exit. You can use jobs to perform tasks such as data processing, machine learning, or any scenario where on-demand processing is required. Container apps and jobs run in the same environment, allowing them to share capabilities such as networking and logging.</i></p>
    <p><i>A job's trigger type determines how the job is started. The following trigger types are available:</i></p>
    <p><i>Manual: Manual jobs are triggered on demand.</i></p>
    <p><i>Schedule: Scheduled jobs are triggered at specific times and can run repeatedly.</i></p>
    <p><i>Event: Event-driven jobs are triggered by events such as a message arriving in a queue.</i></p>
  </div>
</div>

---


<a name="6"></a>
## 6. Azure Backup and Data Protection

---

<div>
  <p><span class="k">1.</span>You have multiple Azure virtual machines and an Azure recovery services vault. Virtual machines are configured with the default backup policy.</p>
  <p><b>What is the retention period of virtual machine backups in the default backup policy?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>7 days</li>
    <li>14 days</li>
    <li class="q-good">30 days</li>
    <li>90 days</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>By default, backups of virtual machines are kept for 30 days.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You have an Azure subscription that contains two protected virtual machines named VM1 and VM2. VM1 and VM2 are backed up to a Recovery Service vault named Vault1 by using the same backup policy.</p>
  <p>Your company plans to create additional virtual machines and Recovery Services vaults. During this process, Vault1 will be decommissioned.</p>
  <p>You need to delete Vault1.</p>
  <p><b>Which three actions should you perform before you can delete Vault1? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Delete VM1 and VM2.</li>
    <li class="q-good">Disable the soft delete feature and delete all data.</li>
    <li>Enable a Read lock on Vault1.</li>
    <li class="q-good">Permanently remove any items in the soft delete state.</li>
    <li class="q-good">Stop the backup of VM1 and VM2.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must stop the backups so that you can prepare to move to the new policy. The soft delete feature is enabled by default, so it must be disabled. You must remove all the items that are in the soft delete state. Deleting the virtual machines is not required. You cannot delete the policy without deleting the vault and backup, and a new policy is not required.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You have an Azure virtual machine named Server1 that runs Windows Server.</p>
  <p>You need to configure Azure Backup to back up files and folders.</p>
  <p><b>What should you install on Server1?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Microsoft Azure Backup Server (MABS)</li>
    <li>Microsoft Azure Site Recovery Provider</li>
    <li>the Azure Connected Machine agent</li>
    <li class="q-good">the Microsoft Azure Recovery Services (MARS) agent</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The Microsoft Azure Recovery Service (MARS) agent must be installed on the servers. The MARS agent is mandatory to perform backup and recovery services for any servers.</i></p>
  </div>
</div>

---


<a name="7"></a>
## 7. Azure Monitoring and Logging

---

<div>
  <p><span class="k">1.</span>You need to create Azure alerts based on metric values and activity log events.</p>
  <p>The solution must meet the following requirements:</p>
  <ul>
    <li>Set a limit on how many times an alert notification is sent.</li>
    <li>Call an Azure function when an alert is triggered.</li>
    <li>Configure the alert to have a severity of warning when triggered.</li>
  </ul>
  <p><b>Which two resources should you create? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">an action group</li>
    <li class="q-good">an alert rule</li>
    <li>a notification</li>
    <li>a secure webhook</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must create an action group to set up an action and create an alert rule to set the severity of the errors. A notification is only used to send email and you do not need to call a webhook.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">2.</span>You have an Azure virtual machine that runs Linux. The virtual machine hosts a custom application that outputs log data in the JSON format.</p>
  <p>You need to recommend a solution to collect the logs in Azure Monitor.</p>
  <p><b>What should you include in the recommendation?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>the Azure VMAccess extension</li>
    <li>the Custom Script Extension Version 2 extension</li>
    <li>the DSC extension for Linux</li>
    <li class="q-good">the Log Analytics agent for Linux</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can use the Log Analytics agent for Linux as part of a solution to collect JSON output from the Linux virtual machines.</i></p>
    <p><i>The Azure Custom Script Extension is used for post-deployment configuration, software installation, or any other configuration or management task.</i></p>
    <p><i>Desired State Configuration (DSC) is a management platform that you can use to manage an IT and development infrastructure with configuration as code.</i></p>
    <p><i>The Azure VMAccess extension acts as a KVM switch that allows you to access the console to reset access to Linux or perform disk-level maintenance.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">3.</span>You have a Log Analytics workspace that collects data from various data sources.</p>
  <p>You create a new Azure Monitor log query.</p>
  <p>You plan to view data pinned as a chart to a shared dashboard.</p>
  <p><b>What is the maximum number of days for which data can be pinned as a chart on the dashboard?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">14</li>
    <li>30</li>
    <li>90</li>
    <li>180</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Data pinned on a shared dashboard can only be displayed for a maximum of 14 days.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">4.</span>You have a Kusto query that returns 1,000 events from the SecurityEvent table in Azure Monitor.</p>
  <p>You need to configure the query to aggregate the results by the Account column.</p>
  <p><b>Which operator should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>extend</li>
    <li>project</li>
    <li class="q-good">summarize</li>
    <li>where</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Summarize is used to group records from one or more columns of data. Where is used to filter the rows. Project is used to rename and select columns. Extend is used to add columns.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">5.</span>You have an Azure virtual machine that runs Linux. The virtual machine hosts a custom application that outputs log data in the JSON format.</p>
  <p>You need to recommend a solution to collect the logs in Log Analytics workspace.</p>
  <p><b>What should you include in the recommendation?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>the Azure VMAccess extension</li>
    <li>the Custom Script Extension Version 2 extension</li>
    <li>the DSC extension for Linux</li>
    <li class="q-good">the Azure Monitor agent for Linux</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You can use the Log Analytics agent for Linux as part of a solution to collect JSON output from the Linux virtual machines.</i></p>
    <p><i>The Azure Custom Script Extension is used for post-deployment configuration, software installation, or any other configuration or management task.</i></p>
    <p><i>Desired State Configuration (DSC) is a management platform that you can use to manage an IT and development infrastructure with configuration as code.</i></p>
    <p><i>The Azure VMAccess extension acts as a KVM switch that allows you to access the console to reset access to Linux or perform disk-level maintenance.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">6.</span>You have an Azure virtual network named VNet1 that is deployed to the Azure East US region.</p>
  <p>You need to ensure that email is sent to an administrator when a virtual machine is connected to VNet1.</p>
  <p><b>What should you create?</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">an action group</li>
    <li>an alert processing rule</li>
    <li class="q-good">an alert rule</li>
    <li>a mail-enabled security group</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Monitor alerts proactively notify you when important conditions are found in monitoring data. They allow you to identify and address issues in the system before customers notice them. You can set alerts on metrics, logs, and the activity log. Different types of alerts have benefits and drawbacks. Metrics is a feature of Azure Monitor that collects numeric data from monitored resources into a time-series database. Metrics are numerical values that are collected at regular intervals and describe some aspect of a system at a particular time.</i></p>
    <p><i>When Azure Monitor data indicates that there may be an issue with an infrastructure or application, an alert is triggered. Azure Monitor, Azure Service Health, and Azure Advisor then use action groups to notify users about the alert and take action. An action group is a collection of notification preferences defined by the owner of an Azure subscription.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">7.</span>You plan to create an alert in Azure Monitor that will have an action group to send SMS messages.</p>
  <p><b>What is the maximum number of SMS messages that will be sent every hour if the alert gets triggered every minute?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>4</li>
    <li>6</li>
    <li class="q-good">12</li>
    <li>60</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>A maximum of one SMS message can be sent every five minutes. Therefore, a maximum of 12 messages will be sent per hour.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">8.</span>You have an Azure subscription that contains an Azure App Service web app named App1.</p>
  <p>You have the following diagnostic logging configurations:</p>
  <ul>
    <li>Application Logging (FileSystem): Error</li>
    <li>Application Logging (Blob): Information</li>
    <li>Detailed Error Message: Warning</li>
    <li>Web Server Logging: Verbose</li>
  </ul>
  <p>You need to configure diagnostic logging to store all warnings or higher.</p>
  <p><b>Which types of diagnostic logging and severity should you enable?</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li class="q-good">Application Logging (Blob)</li>
    <li>Application Logging (FileSystem)</li>
    <li>Detailed Error Message</li>
    <li>Verbose</li>
    <li class="q-good">Warning</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>You must enable the Application Logging (Blob) diagnostic, which can be stored for more than a week. You must also set the severity level to warning, to store warning, error, and critical log messages.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">9.</span>You have 100 virtual machines deployed to Azure. You have Azure Monitor alerts configured for CPU and memory utilization for the virtual machines.</p>
  <p>You open Azure Monitor alerts and discover 50 closed alerts for the virtual machines.</p>
  <p><b>What can cause the alert state to be Closed?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li class="q-good">An administrator manually changed the state of the alerts.</li>
    <li>The alerts are older than 60 days.</li>
    <li>The alert rule contains an action group that remediates the alert conditions.</li>
    <li>The conditions that caused the alerts are no longer present.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>The alert state is manually set by the user and does not have any automated logic behind it. The alert state can be either New, Acknowledged, or Closed.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">10.</span>You have an Azure subscription that contains a resource group named RG1. RG1 contains two virtual machines named VM1 and VM2.</p>
  <p>You need to inspect all the network traffic from VM1 to VM2.The solution must use Azure Monitor metrics.</p>
  <p><b>Which two actions should you perform? Each correct answer presents part of the solution.</b></p>
  <p>Select all answers that apply.</p>
  <ol type="A">
    <li>Configure a log alert.</li>
    <li>Configure Network In and Network Out.</li>
    <li class="q-good">Install AzureNetworkWatcherExtension.</li>
    <li class="q-good">Use packet capture.</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Network Watcher variable packet capture allows you to create packet capture sessions to track traffic to and from a virtual machine. Packet capture helps to diagnose network anomalies both reactively and proactively.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">11.</span>You have an Azure subscription that contains 20 virtual networks and 500 virtual machines.</p>
  <p>You deploy a new virtual machine named VM501.</p>
  <p>You discover that VM501 is unable to communicate with a virtual machine named VM20 in the subscription. You suspect that a network security group (NSG) is the cause of the issue.</p>
  <p>You need to identify whether an NSG is blocking communications. The solution must minimize administrative effort.</p>
  <p><b>What should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>diagnostic logs</li>
    <li class="q-good">IP flow verify</li>
    <li>NSG flow logs</li>
    <li>packet capture</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>IP flow verify lets you specify a source and destination IPv4 address, port, protocol (TCP or UDP), and traffic direction (inbound or outbound). IP flow verify can identify the specific network security group (NSG) that prevents communication. NSG flow logs is a feature of Azure Network Watcher that allows you to log information about IP traffic flowing through an NSG. Although the logs may help you identify the source of the issue, it requires much more configuration and manual evaluation. Packet capture allows you to create packet capture sessions to track traffic to and from a virtual machine. Packet capture may help narrow down the scope of the issue, but it will not identify the specific NSG that prevents communication.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">12.</span>You have an Azure virtual machine that hosts a third-party application named App1.</p>
  <p>Users report that they experience performance issues when they use the application.</p>
  <p>You need to find the root cause of the performance issue.</p>
  <p><b>What should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>activity logs</li>
    <li>Azure Advisor</li>
    <li>Azure Cost</li>
    <li class="q-good">Azure Monitor</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Monitor stores metrics in a time-series database that is optimized for analyzing time-stamped data. Activity logs detect and address issues before users notice them proactivity. Azure Advisor analyzes configuration and usage metrics but does not provide time-lapsed data. Azure Cost only helps to optimize and reduce overall Azure spending.</i></p>
  </div>
</div>

---

<div>
  <p><span class="k">13.</span>You have an Azure subscription that contains virtual machines, virtual networks, application gateways, and load balancers.</p>
  <p>You need to monitor the network health of the resources.</p>
  <p><b>Which Azure service should you use?</b></p>
  <p>Select only one answer.</p>
  <ol type="A">
    <li>Azure Monitor</li>
    <li class="q-good">Azure Network Watcher</li>
    <li>Azure Resource Manager</li>
    <li>network security groups (NSGs)</li>
  </ol>
  <button onclick="toggleSolution(this)">Reveal Solution</button>
  <div class="q-exp">
    <p><i>Azure Network Watcher provides tools to monitor, diagnose, view metrics, and enable or disable logs for resources on an Azure virtual network. Azure Resource Manager is the deployment and management service for Azure. Network security groups (NSGs) are used only for security, not monitoring. Azure Monitor is used for the HTTP Data Collector API to send log data to Log Analytics.</i></p>
  </div>
</div>

---
