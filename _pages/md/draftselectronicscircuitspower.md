# ⚡ Power Supplies, Voltage Regulators

<div style="text-align: right;"><h7>Posted: 2022-11-18 22:49</h7></div>

###### .

## 1. Introduction

...

## 2. Voltage Dividers

In theory, two similar (typically 10k) resistors form a **voltage divider**: 

![alt_text](vr_-_divider_-_resistors_-_simple.svg)

When we replace the first resistor by a **potentiometer**, we get a very simple voltage regulator. 

Moreover, that simple circuit can be expanded on demand, like in the following example, where the three outputs depend on the actual level of the potentiometer: 

![alt_text](vr_-_divider_-_resistors_-_multiple.svg)

<div class="warn">
Keep in mind that the resistors are never perfect, so tiny differences can be experienced, but the potentiometer helps us to tune the output voltage as we want.
</div>


## 3. Zener Diode-based Voltage Regulators

![alt_text](vr_-_zener.svg)

...

![alt_text](vr_-_zener-bjt_-_single.svg)

source: [powerelectronicsnews.com](https://www.powerelectronicsnews.com/power-supply-design-notes-zener-diode-voltage-regulator/)

...

![alt_text](vr_-_zener-bjt_-_double.svg)

...

## 4. Linear Voltage Regulators and Applications

![alt_text](vr_-_7805.svg)

...


## 5. Modern Voltage Regulators and DC-DC Converters

...

### 6.1 Voltage Regulators with LM317

Pin 1 of the IC does the adjustment, while Pin 2 is the Output and Pin 3 is the Input. The original circuit diagram is a bit re-designed here to get a better overview on the real pin positioning.



![alt_text](vr_-_lm317_-_simple.svg)

...

![alt_text](vr_-_lm317_-_advanced.svg)

...

### 5.2 Voltage Regulators with AMS1117

The AMS1117 series develops a 1.25V reference voltage between the output and the adjust terminal. Placing a resistor between these two terminals causes a constant current to flow through R1 and down through R2 to set the overall output voltage. This current is normally the specified minimum load current of 10mA.

The ICs pins are the same as for LM317, so Pin 1 does the adjustment (which is simply a GND for AMS1117-xx, fixed voltage regulators), while Pin 2 is the Output and Pin 3 is the Input. 

For AMS1117, not even protection diodes are needed, so the following can be considered as an advanced design already: 

![alt_text](vr_-_ams1117.svg)

### 5.3 DC-DC converter with SD8942

source: [oshwlab.com](https://oshwlab.com/wagiminator/78xx-replacement)

...

![alt_text](vr_-_sd8942.svg)

...
