# ![alt text](/_ui/img/icons/ansible_m.svg) Ansible

<div style="text-align: right;"><h7>Posted: 2021-05-01 08:37</h7></div>

###### .

![alt_text](ansible_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Basic Concepts](#2)
3. [Preparing the Environment](#3)
4. [The Inventory](#4)
5. [Simple Playbooks](#5)
6. [Variables](#6)
7. [Conditionals and Loops in Tasks](#7)
8. [Vault](#8)
9. [Roles](#9)
10. [Handlers and Notifiers](#10)
11. [Ansible Collections](#11)
12. [Linting our Ansible Code](#12)


<a name="1"></a>
## 1. Introduction

**Ansible** is a centralized remote management tool to keep your environment, no matter how complex it is, consistent, the same sub-types of servers identical and up-to-date easily. It implements the idea of **Configuration as Code** (CaC), which is quite close to IaC, but we will see it represents a quite different perspective for managing our infrastructure than IaC, since Ansible relies on the operating system. 

Ansible was originally written by **Michael DeHaan**, a Software Engineer from the US, and its initial release was submitted in **20 February, 2012**. Soon after, in 2015, Red Hat acquited the project and it maintains it since then under the **Ansible Community**. 

The tool itself is "only" **a bunch of Python scripts**, so in order to leverage the most of it, it won't hurt if you learn a bit about Python itself too. 


<a name="2"></a>
## 2. Basic Concepts

<div class="info">
<p>The solution of Ansible is mostly about running Shell commands on the target servers via SSH in a highly controlled manner.</p>
</div>

The basic idea is that **you don't need to have any client software** installed on the target machines (Managed Nodes), only a passwordless authenticated user (AD or local, that doesn't matter), issued through `ssh`. In several cases, this user should have `sudo` (so called "elevated") rigthts to access system-level resources, such as package installations and manipulating configuration files under `/etc` etc. 

Ansible **works mostly on Unix-like systems**, and also, it's used mostly on any kind of Linux distributions both in the Control and in the Managed Nodes' sides. A Managed Node, however, can be even a **Windows** host. 

### 2.1 General Terms in Ansible

The below list is only a summary for these terms. Further details on Basic Concepts can be found at [docs.ansible.com](https://docs.ansible.com/ansible/latest/user_guide/basic_concepts.html)

- **Control Node**: any machine with **Ansible installed**. You can run Ansible commands and playbooks by invoking the `ansible`, `ansible-playbook` or `ansible-galaxy` commands from any Control Node. You can use any computer that has a Python installation as a Control Node - laptops, shared desktops, and servers can all run Ansible. However, you cannot use a Windows machine as a Control Node. You can have multiple Control Nodes.
- **Managed Nodes**: the network devices (mostly servers (either physical or virtual), but even other devices are managable) you control with Ansible. Managed Nodes are also sometimes called "hosts". Having Ansible installed on Managed Nodes is not needed.
- **Inventory**: a list of Managed Nodes. An inventory file is also sometimes called a "hostfile". Your inventory can specify information like IP address for each Managed Node. An inventory can also organize Managed Nodes, creating and nesting groups for easier scaling. 
- **Collections**: these are a distribution format for Ansible content that can include playbooks, Roles, modules, and plugins. You can install and use collections through Ansible Galaxy. 
- **Modules**: the units of code Ansible executes. Each module has a particular use, from administering users on a specific type of database to managing VLAN interfaces on a specific type of network device. You can invoke a single module with a task, or invoke several different modules in a playbook. Starting in Ansible 2.10, modules are grouped in collections. 
- **Tasks**: the units of action in Ansible. You can execute a single task once with an ad hoc command.
- **Playbooks**: ordered lists of tasks, saved so you can run those tasks in that order repeatedly. Playbooks can include variables as well as tasks. Playbooks are written in YAML and are easy to read, write, share and understand. To learn more about playbooks, see Intro to playbooks.
- **Roles**: when your team's codebase grows big, it's almost mandatory to keep the significant components of the big mass in logic chunks, isolate the groups of tasks from each other but still keeping them dependent on each other when necessary, enabling them to be reused by different projects. The Ansible Roles make this idea real. 

### 2.2 The Ansible Workflow

According to the above general terms, there's nothing mysterious in the Ansible Workflow either: 

1. Our Control Node must be **prepared** for Playbook execution: 
  - Our **installation of Ansible executables** must be in place, along with `git`, if we need to keep a robust version control system among the teams. 
  - Our installation must contain the required **built-in and extra Ansible modules and plugins**.
  - Our **Inventory** must contain the hosts that represent the Managed Nodes, along with their categories, structures.
  - If we use **Roles**, they should be also maintained and version-controlled.
  - And, of course, our **Playbook** must contain all the preliminary actions and tasks that we want to execute against the selected Managed Nodes.
2. During Playbook Execution, the Control Node first opens SSH queries on nodes already to get to know them (**Gathering Facts**), such as their OS version, IP address, CPU cores and so on.
3. When all facts are known, the **execution of actual Tasks** that we defined in the Playbook happens on nodes that matched with conditionals if any.

![alt_text](ansible_-_workflow.png)

We will see examples and details about all of these steps of the workflow later.

There are some cases, when the invoked task must run either on the issuer host (`localhost`), the Control Node, or the task is about some HTTP calls against some APIs, but this doesn't change much on the basic idea: Ansible is generally a remote management solution with high control, in an error-proof simplicity and flexibility.

<a name="3"></a>
## 3. Preparing the Environment

As the first step to do on the Control Node, we need to get an **Ansible package**. The easiest way for it is to install it directly from the official repository of your Linux distribution. In the initial examples of this article, the Control Node is a Red Hat -like host, first a CentOS, which can get these packages from EPEL repo only. 

Nowadays, ensuring you have the proper Python version, then preparing a **PIP** environment, then installing Ansible and any further prerequisites with `pip` (or `pip3`) is rather preferred to ensure everybody has the same Ansible Control Node environment. **We will see examples for that later.** (PIP stands for *"Pip Installs Packages"* or *"Pip Installs Python"* or *"Preferred Installer Program"*; see the [Python3](/learning/it/prog/python/) article for further details about it.)

Apart from the Ansible packages, **Git** is also very important to have.

### 3.1 Preparing a Control Node

Let's see 4 different scenarios through examples:

- prepare a Control Node using **EPEL Repo's standard RPM packages**
- prepare a Control Node with **PIP**, with enforcing using `venv`
- prepare a Control Node with **PIP** and `venv`, with also altering `PATH`
- prepare a **container image** to be a Control Node that can be started up in seconds

#### 3.1.1 Preparing a Control Node using EPEL Repo's standard RPM packages

The method is very simple despite it requires the additional EPEL repo. Here is a walkthrough from years ago, but the concept is theoretically the same even today, if we choose this way. 

1. Install EPEL repo:

```ksh
# dnf install epel-release
Last metadata expiration check: 0:01:21 ago on Sat 01 May 2021 08:36:01 PM CEST.
Dependencies resolved.
[...]
Complete!

# dnf makecache
CentOS Linux 8 - AppStream                                                                                                               14 kB/s ...
Metadata cache created.
```

2. Install Ansible: 

```ksh
# dnf install ansible
Last metadata expiration check: 0:00:07 ago on Sat 01 May 2021 08:37:48 PM CEST.
Dependencies resolved.
[...]
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
[...]
Installed:
  ansible-2.9.20-1.el8.noarch              libsodium-1.0.18-2.el8.x86_64             python3-asn1crypto-0.24.0-3.el8.noarch     python3-babel-2.5.1-5.el8.noarch        
  python3-bcrypt-3.1.6-2.el8.1.x86_64      python3-cffi-1.11.5-5.el8.x86_64          python3-cryptography-2.3-3.el8.x86_64      python3-jinja2-2.10.1-2.el8_0.noarch    
  python3-jmespath-0.9.0-11.el8.noarch     python3-markupsafe-0.23-19.el8.x86_64     python3-paramiko-2.4.3-1.el8.noarch        python3-pyasn1-0.3.7-6.el8.noarch       
  python3-pycparser-2.14-14.el8.noarch     python3-pynacl-1.3.0-5.el8.x86_64         python3-pytz-2017.2-9.el8.noarch           sshpass-1.06-9.el8.x86_64               
[...]
```

3. Check and test: 

```ksh
# ansible --version
ansible 2.9.20
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3.6/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 3.6.8 (default, Aug 24 2020, 17:57:11) [GCC 8.3.1 20191121 (Red Hat 8.3.1-5)]

# ansible localhost -m ping
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}

# python3 --version
Python 3.6.8
```

#### 3.1.2 Preparing a Control Node with PIP with enforcing using `venv`

It might be useful **when you use a shared account** that might be used by someone else too. 

This scenario is even more secure as it doesn't require running everything locally with `root` privileges. Also, it can be improved by using Python Virtual Environment (see the [Python3](/learning/it/prog/python/) article about it). We will do that in the below example. 

So, all you need is to just ensure you have Python3 and its PIP installed, then install the `ansible` Python package with `pip` or `pip3` commands.

<div class="info">
<p>There's a high chance that your laptop or jump host or whatever that you would like to use as a Control Node has multiple Python3 installations, so it's especially useful if you keep a well-prepared development environment as an up-to-date Control Node.</p>
</div>

1. First you can look around for Python3 and PIP installations. In this case, we have a custom installation of that, compiled from source, so let's use that one:

```ksh
[infra@rocky9srv1 ~]$ ls -li $(which pip) ; pip --version
673401 -rwxr-xr-x 1 root root 648 May 16  2022 /usr/bin/pip
pip 21.2.3 from /usr/lib/python3.9/site-packages/pip (python 3.9)

[infra@rocky9srv1 ~]$ ls -li $(which pip3) ; pip3 --version
676878 -rwxr-xr-x 1 root root 648 May 16  2022 /usr/bin/pip3
pip 21.2.3 from /usr/lib/python3.9/site-packages/pip (python 3.9)

[infra@rocky9srv1 ~]$ python3 --version
Python 3.9.16

[infra@rocky9srv1 ~]$ ls -li /opt/Python3/bin/pip3 ; /opt/Python3/bin/pip3 --version
319749 -rwxr-xr-x 1 root root 232 Apr 19 16:05 /opt/Python3/bin/pip3
pip 24.0 from /opt/Python3/lib/python3.12/site-packages/pip (python 3.12)

[infra@rocky9srv1 ~]$ /opt/Python3/bin/python3 --version
Python 3.12.3
```

2. Prepare and activate your VEnv:

```ksh
[infra@rocky9srv1 ~]$ /opt/Python3/bin/python3 -m venv .venv312

[infra@rocky9srv1 ~]$ ls -la .venv312/bin/
total 92
drwxr-xr-x 2 infra users 4096 Apr 19 16:21 .
drwxr-xr-x 5 infra users 4096 Apr 19 16:20 ..
-rw-r--r-- 1 infra users 2036 Apr 19 16:20 activate
-rw-r--r-- 1 infra users  922 Apr 19 16:20 activate.csh
-rw-r--r-- 1 infra users 2201 Apr 19 16:20 activate.fish
-rw-r--r-- 1 infra users 9033 Apr 19 16:20 Activate.ps1
-rwxr-xr-x 1 infra users  237 Apr 19 16:20 pip
-rwxr-xr-x 1 infra users  237 Apr 19 16:20 pip3
-rwxr-xr-x 1 infra users  237 Apr 19 16:20 pip3.12
lrwxrwxrwx 1 infra users    7 Apr 19 16:20 python -> python3
lrwxrwxrwx 1 infra users   24 Apr 19 16:20 python3 -> /opt/Python3/bin/python3
lrwxrwxrwx 1 infra users    7 Apr 19 16:20 python3.12 -> python3

[infra@rocky9srv1 ~]$ source .venv312/bin/activate

(.venv312) [infra@rocky9srv1 ~]$ python --version
Python 3.12.3

(.venv312) [infra@rocky9srv1 ~]$ pip --version
pip 24.0 from /home/infra/.venv312/lib/python3.12/site-packages/pip (python 3.12)

(.venv312) [infra@rocky9srv1 ~]$ pip list
Package Version
------- -------
pip     24.0
```

3. Now install the `ansible` PIP package:

```ksh
(.venv312) [infra@rocky9srv1 ~]$ pip install ansible
Collecting ansible
  Using cached ansible-9.4.0-py3-none-any.whl.metadata (8.2 kB)
[...]
Successfully installed MarkupSafe-2.1.5 PyYAML-6.0.1 ansible-9.4.0 ansible-core-2.16.6 cffi-1.16.0 cryptography-42.0.5 jinja2-3.1.3 packaging-24.0 pycparser-2.22 resolvelib-1.0.1

(.venv312) [infra@rocky9srv1 ~]$ pip list
Package      Version
------------ -------
ansible      9.4.0
ansible-core 2.16.6
cffi         1.16.0
cryptography 42.0.5
Jinja2       3.1.3
MarkupSafe   2.1.5
packaging    24.0
pip          24.0
pycparser    2.22
PyYAML       6.0.1
resolvelib   1.0.1
```

4. Make sure the related commands' full paths reflect the VEnv: 

```ksh
(.venv312) [infra@rocky9srv1 ~]$ echo ansible ansible-galaxy ansible-inventory | xargs which
/home/infra/.venv312/bin/ansible
/home/infra/.venv312/bin/ansible-galaxy
/home/infra/.venv312/bin/ansible-inventory

(.venv312) [infra@rocky9srv1 ~]$ ansible --version
ansible [core 2.16.6]
  config file = None
  configured module search path = ['/home/infra/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/infra/.venv312/lib/python3.12/site-packages/ansible
  ansible collection location = /home/infra/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/infra/.venv312/bin/ansible
  python version = 3.12.3 (main, Apr 19 2024, 16:02:59) [GCC 11.3.1 20221121 (Red Hat 11.3.1-4)] (/home/infra/.venv312/bin/python3)
  jinja version = 3.1.3
  libyaml = True

(.venv312) [infra@rocky9srv1 ~]$ ansible localhost -m ping
[WARNING]: No inventory was parsed, only implicit localhost is available
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

5. You can either `deactivate` from the VEnv or just logout. It will be there, waiting for you from now on: 

```ksh
(.venv312) [infra@rocky9srv1 ~]$ deactivate

[infra@rocky9srv1 ~]$ python3 --version
Python 3.9.16
```

The above can be also controlled towards by strict VEnv versions, so that's why a so called *"Enterprise setup"* often uses this approach, also enforcing the use of an isolated VEnv. 

#### 3.1.3 Preparing a Control Node with PIP with altering `PATH`

This scenario is useful for **your individual user** or for a shared one if the purpose is clear.

The steps are entirelly the same as in the previous scenario but extend it with the following steps:

1. Edit your `PATH` variable by not only adding but prioritizing your VEnv directory:

```ksh
[infra@rocky9srv1 ~]$ vi .bash_profile
[...]
export PATH=$HOME/.venv312/bin:$PATH
[...]
```

3. Now re-login, then versions and functionality: 

```ksh
[infra@rocky9srv1 ~]$ logout

[root@rocky9srv1 ~]# su - infra

[infra@rocky9srv1 ~]$ python --version
Python 3.12.3

[infra@rocky9srv1 ~]$ pip --version
pip 24.0 from /home/infra/.venv312/lib/python3.12/site-packages/pip (python 3.12)

[infra@rocky9srv1 ~]$ ansible --version
ansible [core 2.16.6]
  config file = None
  configured module search path = ['/home/infra/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/infra/.venv312/lib/python3.12/site-packages/ansible
  ansible collection location = /home/infra/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/infra/.venv312/bin/ansible
  python version = 3.12.3 (main, Apr 19 2024, 16:02:59) [GCC 11.3.1 20221121 (Red Hat 11.3.1-4)] (/home/infra/.venv312/bin/python3)
  jinja version = 3.1.3
  libyaml = True

[infra@rocky9srv1 ~]$ ansible localhost -m ping
[WARNING]: No inventory was parsed, only implicit localhost is available
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

#### 3.1.4 Preparing a container image to be a Control Node

The main approach can be any of the above 3 ones, but in this case it's even improved by: 

- the container image already includes the user (which has its public SSH key populated accross the environment)
- all the ansible packages and `git` is installed on it
- the image is shipped with an "entrypoint" script which ensures the container starts immediately with cloning the Ansible Inventory (see later) to its working directory

The article about [GitLab](/learning/it/sysadmin/gitlab/) gives all the ideas to achieve this. In order to experiment with Ansible further on, we don't need this for now, but a real DevOps environment definitely must have this approach in everyday use by multiple reasons. 

### 3.2 Preparing the Managed Nodes - the Passwordless SSH Connection

The below is a quick walktrough on how to ensure the remote hosts are reachable through SSH key. 

As a prerequisite, the target user must exist on the remote host too. 

```ksh
$ ssh-keygen -t ecdsa -b 521
Generating public/private ecdsa key pair.
Enter file in which to save the key (/home/infra/.ssh/id_ecdsa): 
Enter passphrase (empty for no passphrase): 
[...]

$ ls -la .ssh
total 20
drwx------. 2 infra users 4096 Nov 28 14:55 .
drwx------. 5 infra users 4096 Nov 28 14:40 ..
-rw-------. 1 infra users  736 Nov 28 14:55 id_ecdsa
-rw-r--r--. 1 infra users  270 Nov 28 14:55 id_ecdsa.pub
-rw-r--r--. 1 infra users  880 Nov 28 14:46 known_hosts

$ ssh-copy-id -i .ssh/id_ecdsa.pub rockysrv2
[...]
infra@rockysrv2's password: 
[...]
Number of key(s) added: 1
[...]

$ ssh-copy-id -i .ssh/id_ecdsa.pub rockysrv3
[...]
infra@rockysrv3's password: 
[...]
Number of key(s) added: 1
[...]

$ ssh rockysrv2
Last login: Mon Nov 28 14:57:41 2022

$ logout
Connection to rockysrv2 closed.

$ ssh rockysrv3
Last login: Mon Nov 28 15:57:26 2022

$ logout
Connection to rockysrv3 closed.
```

### 3.2.1 Testing the connection with Ansible

<div class="warn">
<p>This test can be done only when you already prepared your Inventory: see next chapter!</p>
</div>

Here's a quick `ping` test, which also tests the SSH connection between the Ansible Controller, the targets, the user and its SSH key too: 

```ksh
$ ansible rocky8vms -m ping
rockysrv3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
}
rockysrv2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
}
```

Maybe it's not that obvious at this point, but this test doesn't simply ping the target hosts, but also connects to them with the configured credentials. It is done through Ansible's built-in [`ping` module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html). It's always a good practice to reference even the most simple modules by their full name or FQDN, which is, in this case, `ansible.builtin.ping`. 

#### 3.2.2 Setting up `sudo`

Ansible has no objections on directly using `root` account on the Control Node and authenticating to Managed Nodes as is, but that's far from being secure. Instead, **we need to find a way to use a privileged, but limited user**. 

When using Ansible, even the "become" (`root`) feature is possible, which means running commands as `root` user. It requires passwordless capability to issue the specified commands as `root` with `sudo`. Here you can find a very basic preparation for it: 

```ksh
$ id
uid=1001(infra) gid=100(users) groups=100(users),10(wheel)

$ sudo grep ^%wheel /etc/sudoers
%wheel	ALL=(ALL)	NOPASSWD: ALL
```


<a name="4"></a>
## 4. The Inventory

<img src="ansible_-_inventory.png" title="" align="right" class="right">

The Inventory has three main purposes:

- for building a **hierarchy** among your servers by **grouping** them
- for specifying how you **access** your inventoried hosts
- for assigning default **variables** for groups of hosts (`group_vars`) or for individual hosts (`host_vars`)

By default, any Inventory-related configuration is in `INI` format, but Ansible allows using `YAML` too. In this article, first we look for some examples for grouping hosts, to make a hierarchy.

Further info about inventory management can be found in [docs.ansible.com](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html). 

### 4.1 Locating the Inventory - The Ansible Config

Ansible assumes its Inventory is set up in any of these locations on your Control Node:

- at least the hostlist (the grouping part) is set up globally, in `/etc/ansible/hosts` or
- whatever the global `/etc/ansible/ansible.cfg` config file defines (see below) or 
- whatever the current user's `~/.ansible.cfg` config file defines (this has the highest priority)
- override everything by the `-i <inventory>` parameter

When working in a team, the inventory is always behind a **source-controlled repository**, so its location is **always custom**. It means, the default inventory file `/etc/ansible/hosts` and the default configuration is highly recommended to be overriden, so the third element from the above list applies: you just need to take care of your own `~/.ansible.cfg`.

When you haven't prepared any of the above configuration, here is an example for using the overriding parameter (`-i`) for `ansible` command: 

```ksh
$ ansible rocky8vms -i ~/Repositories/rockylab/ansible-inventory/ansible-hosts --list
  hosts (2):
    rockysrv2
    rockysrv3
```

When you have the configuration file in place, you no longer need to override anything: 

```ksh
$ cat .ansible.cfg
[defaults]
ansible_python_interpreter = /usr/bin/python3.8
inventory                  = ~/Repositories/rockylab/ansible-inventory/ansible-hosts

$ ansible rocky8vms --list
  hosts (2):
    rockysrv2
    rockysrv3
```

Some further examples for useful parameters to be added to your `~/.ansible.cfg`:

```
[defaults]
inventory                  = ~/Repositories/rockylab/ansible-inventory/ansible-hosts
ansible_managed            = This is an Ansible managed file, do not modify it directly.
host_key_checking          = false
timeout                    = 120
ansible_python_interpreter = /bin/python3

[ssh_connection]
retries = 1
```

### 4.2 Building a Hierarchy - Grouping Hosts

Usually every hosts should belong to at least one group, but it's not mandatory. The next example presents a very simple inventory with an "orphan" host in the beginning: 

According to the below, it is possible to reference...

- to `all`,
- to `rocky8vms` group, referencing to the respective two hosts, or
- to any single hosts. 

```
rockysrv1

[rocky8vms]
rockysrv2
rockysrv3
```

In practice, we group all servers in **subgroups**. The next example introduces how to define hosts with their short hostnames, if those are not in the DNS or in local `/etc/hosts` file, using the `ansible_host` inventory parameter. Also, the `children` directive instructs Ansible that the definition next to it will have children group(s), defined later: 

```
[vms]
rockysrv1  ansible_host=192.168.56.101
rockysrv2  ansible_host=192.168.56.102
rockysrv3  ansible_host=192.168.56.103

[rockyvms:children]
rocky8vms
rocky9vms

[rocky8vms]
rockysrv2
rockysrv3

[rocky9vms]
rockysrv1
```

**Testing** the results after any changes in the inventory is always a good idea. E.g.: 

```ksh
$ ansible all --list
  hosts (3):
    rockysrv1
    rockysrv2
    rockysrv3

$ ansible vms --list
  hosts (3):
    rockysrv1
    rockysrv2
    rockysrv3

$ ansible rockysrv1 -m debug -a var=group_names
rockysrv1 | SUCCESS => {
    "group_names": [
        "rocky9vms",
        "rockyvms",
        "vms"
    ]
}

$ ansible rockysrv2 -m debug -a var=group_names
rockysrv2 | SUCCESS => {
    "group_names": [
        "rocky8vms",
        "rockyvms",
        "vms"
    ]
}
```

Once you filled your Inventory, you can use `ansible-inventory` tool anytime to understand the grouping hierarchy better, e.g.: 

```ksh
$ ansible-inventory --graph rockyvms
@rockyvms:
  |--@rocky8vms:
  |  |--rockysrv2
  |  |--rockysrv3
  |--@rocky9vms:
  |  |--rockysrv1
```

### 4.3 Specifying Host Access

Using the `ansible_host` parameter in hour Inventory is extremely useful when your `/etc/hosts` file or your DNS configuration don't directly reflect how you access your servers or if you just simply want to override it. It can be either an IP address directly or another "known" hostname. Ideally, it's an FQDN. Example for using IPs:

```
[...]
[vms]
rockysrv1  ansible_host=192.168.56.101
rockysrv2  ansible_host=192.168.56.102
rockysrv3  ansible_host=192.168.56.103
[...]
```

Using the `ansible_user` is useful if the SSH connection initiator, your current user, at Control Node's side is not the same as the Managed Node's one. E.g.:

```
[...]
rocky9srv1  ansible_host=rocky9srv1.example.com  ansible_user=infra
[...]
```

### 4.4 Referencing to Hosts and/or Groups in Runtime

You can list multiple servers or groups (the elements of the host/group list must be separated either by `:` or `,`), e.g.:

```ksh
$ ansible 'rocky9srv1,rocky9srv2' --list
  hosts (2):
    rocky9srv1
    rocky9srv2
```

In real life practice, you always end up using `!` (what must **NOT** be included) and `&` (what must additionally (**AND**) be included) directives for specifying the hosts more precisely. 

For example, having this example structure in the inventory...

```
[...]
[rockyvms:children]
rocky8vms
rocky9vms

[rocky8vms]
rocky8srv1

[rocky9vms]
rocky9srv1
rocky9srv2
rocky9srv3

[webservers]
rocky9srv1
rocky9srv2

[maintenance]
rocky9srv2
```

...the following command results only one server:

```ksh
$ ansible 'rockyvms:&webservers:!maintenance' --list
  hosts (1):
    rocky9srv1
```

### 4.5 Additional Variables in Inventory

The Ansible Inventory is not only for making a hierarchy of group of servers and place our hostnames in it. You can also assign a huge amount of predefined variables, even map variables for your hosts or groups of hosts: 

- `host_vars/a_host.yml`: individually, for your `a_host` server only
- `group_vars/a_group_of_hosts.yml`: variables that are effective for the specified `a_group_of_hosts` hostgroup and all of its child groups, unless they are overriden by the child group's own `.yml` definition here or overriden by a `host_vars` definition individually
- `group_vars/all/anyname.yml`: variables that are effective for all the servers, unless they are overriden by any of the below

The variables can be dinamically linked to each other, e.g.:

```ksh
$ cat ./group_vars/dc_aaa_fsh.yml
# Allow SSH
sshd_allowed_hosts: "{{ management_ips }} {{ dc_aaa_prv }}"
```

The effectiveness of these variables always can be tested with the `debug` module, e.g.:

```ksh
$ ansible rockysrv1 -m debug -a var=sshd_allowed_hosts
[...]
```

Here's a query to print all the variables that you defined for a host on Inventory level: 

```ksh
$ ansible rockysrv1 -m debug -a "var=hostvars[inventory_hostname]"
[...]
```


<a name="5"></a>
## 5. Simple Playbooks

Even though we can achieve quite many tasks with using `ansible` commands only, anything more complex should be rather stored, shared and developed in the team, using the standard **Ansible Task description files**, the so called **Playbooks**. When we work with Playbooks, the invoking command is `ansible-playbook`, with slightly different syntax than `ansible` had. 

The Ansible Playbook files are **YAML** files, so they are in simple text format. When using YAML files, with either `.yml` or `.yaml` file extension, the most important thing is the indentation in these files. Solving errors based on indentation mistakes can be sometimes really annoying, because the cause of the failure is hard to be interpreted from the error message. 

Of course, if the **indentation is incorrect**, the whole Playbook can become invalid, like in the following example: 

```ksh
[...]
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'
ERROR! We were unable to read either as JSON nor YAML, these are the errors we got from each:
JSON: Expecting value: line 1 column 1 (char 0)

Syntax Error while loading YAML.
  did not find expected '-' indicator

The error appears to be in '/home/john.doe/ansible-test/first.yml': line 5, column 3, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

  tasks:
  ^ here
```

### 5.1 The Most Simple Playbook

Naming the Playbook itself (through `name` keyword) is optional, but all the tasks must have a `name`. Also, the Playbook should describe the matching `hosts`, which is the same type of definition as for simple `ansible` commands. 

Let's put the previously seen ping test into a Playbook file. Also notice that we use the FQDN of the `ansible.builtin.ping` module, but these all work also when referenced in the simple way, e.g. as `ping`. For the sake of experiment, let's use `all` of the hosts as inventory group, so we will reference to a currently shut down VM too during this test: 

```
---
- name: Ping test
  hosts: all

  tasks:

    - name: "Reachability test"
      ansible.builtin.ping:
...
```

The starting `---` and closing `...` are not mandatory, but they are part of the standard YAML format, so it's always better to have them. You can read more about **Ansible's recommendations on YAML Syntax** [here](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html).

So, when the Playbook file is ready (in this example, saved as `playbook.yml`), you can invoke it with `ansible-playbook` command, without any extra parameters, so just run against all the hosts, defined by the Playbook's `hosts` keyword: 

```ksh
$ ansible-playbook playbook.yml 

PLAY [Ping test] *****************************************************************************************

TASK [Gathering Facts] ***********************************************************************************
ok: [rockysrv3]
ok: [rockysrv2]
fatal: [rockysrv1]: UNREACHABLE! => {"changed": false, "msg": "Failed to connect to the host via ssh: ssh: connect to host 192.168.56.101 port 22: No route to host", "unreachable": true}

TASK [Reachability test] *********************************************************************************
ok: [rockysrv2]
ok: [rockysrv3]

PLAY RECAP ***********************************************************************************************
rockysrv1          : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   
rockysrv2          : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
rockysrv3          : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

In practice, usually we limit the list of target nodes by the `-l` parameter in order to be sure the Playbook is ran against the desired list of servers only, even though we wrote a good `hosts` definition. Example for limiting the targets:

```ksh
$ ansible ping.yml -l 'rockyvms:&webservers:!maintenance' -i custom_inventory.ini

PLAY [Ping test] *****************************************************************************************

TASK [Gathering Facts] ***********************************************************************************
ok: [rocky9srv1]

TASK [Reachability test] *********************************************************************************
ok: [rocky9srv1]

PLAY RECAP ***********************************************************************************************
rocky9srv1         : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### 5.2 Long Running Tasks

A good example for this is rebooting a server since you won't get the prompt back until the target hosts are rebooted.

A simple testing Playbook for this (also notice the `become` feature too, since `reboot` obviously requires `root` privileges):

```
---
- name: "My reboot play"
  hosts: webservers
  become: true

  tasks:

  - name: Unconditionally reboot the machine with all defaults
    ansible.builtin.reboot:
...
```

Running this Playbook, named as `reboot.yml`, now exceptionally in **verbose** mode: 

```ksh
# ansible-playbook reboot.yml -v
Using /etc/ansible/ansible.cfg as config file

PLAY [My reboot play] ************************************************************************************

TASK [Gathering Facts] ***********************************************************************************
ok: [rockysrv1]
ok: [rockysrv2]

TASK [Unconditionally reboot the machine with all defaults] *********
changed: [rockysrv1] => {"changed": true, "elapsed": 43, "rebooted": true}
changed: [rockysrv2] => {"changed": true, "elapsed": 43, "rebooted": true}

PLAY RECAP ***********************************************************************************************
rockysrv1          : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
rockysrv2          : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

As usual, further details and possibilities on this `reboot` module can be found at [docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/reboot_module.html)

### 5.2.1 Running Custom Shell Commands

In the following example, we use the `-b` ("become") option too for accessing the specified resources as root, through `sudo`. 

```ksh
$ ansible webservers -m shell -a 'ls -l /etc/httpd/conf/httpd.conf ; cat /etc/httpd/conf/httpd.conf | grep SRVROOT ; echo " "' -b
[...]
```

**Be careful with the redirections** when issuing commands with `ansible.builtin.shell` module this way, but you will soon notice it's a very powerful way to pretty much querying for the same thing on multiple remote hosts. You can read more about this module at [docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html). Also, use `set -o pipefail` right before your oneliner, e.g.:

```
- name: Example task
  ansible.builtin.shell: set -o pipefail && ls -ld /tmp | tr -d tmp
  args:
    executable: /usr/bin/bash
```

### 5.3 Multiple Tasks in a Playbook

The Playbooks have true meaning if they include multiple, sequential tasks. Let's see a very simple Playbook, saved as `ping-stress.yml`, with two tasks, issued explicitely on a host:

```
---
- name: "Ping and Stress installation play"
  hosts: rocky9srv1
  become: true

  tasks:

  - name: "Reachability test"
    ansible.builtin.ping:

  - name: "Install Stress test"
    ansible.builtin.dnf:
      name: stress-ng
      state: present
...
```

In this case, the package to be installed is `stress-ng`, a small programm to do stress tests on the server. The good thing is also that this `dnf` module never asks, it just installs any dependencies too, if needed. 

```ksh
$ ansible-playbook ping-stress.yml

PLAY [Ping and Stress installation play] *****************************************************************

TASK [Gathering Facts] ***********************************************************************************
ok: [rocky9srv1]

TASK [Reachability test] *********************************************************************************
ok: [rocky9srv1]

TASK [Install Stress test] *******************************************************************************
changed: [rocky9srv1]

PLAY RECAP ***********************************************************************************************
rocky9srv1         : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

More info on the `dnf` module: [docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

### 5.4 Looking for and using `ansible_facts`

A simple query for everything (commented out here), and for specific fields only from the huge JSON output:

```
---
- name: Look for Ansible Facts
  hosts: all
  
  tasks:

#  - name: Query for all facts (2000+ lines)
#    ansible.builtin.debug:
#      msg: "{{ ansible_facts }}"

  - name: Query for OS type and major version
    ansible.builtin.debug:
      msg: "{{ ansible_facts['distribution'] }} {{ ansible_facts['distribution_major_version'] }}"

```

The result tells everything we need in this particular case: 

```ksh
[...]
TASK [Display OS type and version only] ***********************************************************
ok: [my-server.example.com] => {
    "msg": "RedHat 8"
}
[...]
```

#### 5.4.1 Conditionals on `ansible_facts`

Example for a simple conditional: 

```
[...]
  - name: Print if it's a CentOS 7
    ansible.builtin.debug: 
      msg: "{{ ansible_facts['nodename'] }} is a {{ ansible_facts['distribution'] }} {{ ansible_facts['distribution_major_version'] }}"
    when: (ansible_facts['distribution'] == 'CentOS') and (ansible_facts['distribution_major_version'] == '7')
[...]
```

And its result on two servers, where one of them matches: 

```ksh
[...]
TASK [Gathering Facts] ****************************************************************************
ok: [my-server-1.example.com]
ok: [my-server-2.example.com]

TASK [Print if it's a CentOS 7] *******************************************************************
ok: [my-server-1.example.com] => {
    "msg": "my-server-1.example.com is a CentOS 7"
}
skipping: [my-server-2.example.com]
[...]
```

Here's another example with multiple conditionals and using multiple, even an embedded (key within a key) key in `ansible_facts`'s JSON data. 

The used keys from `ansible_facts`: 

```
{
[...]
        "cmdline": {
            "BOOT_IMAGE": "(hd1,gpt2)/vmlinuz-4.18.0-513.9.1.el8_9.x86_64",
            "audit": "1",
            "crashkernel": "auto",
            "mce": "off",
            "panic": "5",
            "quiet": true,
            "rd.lvm.lv": "vg_os/lv_swap",
            "resume": "/dev/mapper/vg_os-lv_swap",
            "rhgb": true,
            "ro": true,
            "root": "/dev/mapper/vg_os-lv_root"
        },
[...]
        "uptime_seconds": 2869874,
[...]
}
```

The small Playbook warns if: 

- the host is running less than 61 days and...
- the `cmdline.mce` is either undefined or not `off`

```
---
- name: Look for Ansible Facts
  hosts: all
  
  tasks:

#  - name: Simple query for uptime
#    ansible.builtin.debug:
#      msg: "Running for {{ (ansible_facts['uptime_seconds'] / 60 / 60 / 24) | int }} days"

  - name: Print if mce is NOT off
    ansible.builtin.debug: 
      msg: "{{ ansible_facts['nodename'] }} is running for \
            {{ (ansible_facts['uptime_seconds'] / 60 / 60 / 24) | int }} days and does not have mce=off!"
    when:
      - ansible_facts['uptime_seconds'] < 5270400
      - (ansible_facts['cmdline']['mce'] is undefined) or (ansible_facts['cmdline']['mce'] != 'off')
```

(Notice leveraging the list in `when`: the elements have implicit `and` relationship with each other!)

### 5.5 Transferring Files to Managed Nodes

There are multiple ways and scenarios for doing this: either by copying file from the Control Node by `ansible.builtin.copy`, or by fetching a file from a remote URL by `ansible.builtin.get_url`, you can also use directly `wget` or `curl` in the Playbook for the same.

#### 5.5.1 Using `ansible.builtin.copy`

Let's see a real life example for it. The file to be transferred below is named as `zabbix.repo` and it's just stored next to our playbook. 

The simple playbook (named as `zabbixrepofix.yml`) for populating it without any conditionals, pre-checks etc.:

```
---
- hosts: all
  become: yes

  tasks:
  - name: Copy file with owner and permissions
    ansible.builtin.copy:
      src: zabbix.repo
      dest: /etc/yum.repos.d/zabbix.repo
      owner: root
      group: root
      mode: '0644'
...
```

And here is the `ansible-playbook` command. For any development, even for small pieces of codes like this, it's always a good practice to try it on one single host only, then if it works, do it on a group of servers. Issue it against a single "dev" node first then: 

```ksh
$ ansible-playbook zabbixrepofix.yml -l devsrv03.example.com 
[...]
devsrv03.example.com       : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

Improve the same to have pre-check (also, optional debug part), so it can be deployed later on relatively safely: 

```
---
- hosts: all
  become: yes

  tasks:

  - name: Get the baseurl rows of zabbix.repo
    ansible.builtin.shell:
      cmd: grep baseurl /etc/yum.repos.d/zabbix.repo
    register: zabbixver
    changed_when: false
    ignore_errors: true

#  - name: Debug
#    ansible.builtin.debug:
#      var: zabbixver

  - name: Copy correct zabbix.repo file for 5.2 ("skipping" if non 5.2 or "ok" if the contents are the same)
    ansible.builtin.copy:
      src: zabbix.repo
      dest: /etc/yum.repos.d/zabbix.repo
      owner: root
      group: root
      mode: '0644'
    when: zabbixver.stdout is search("5.2")
...
```

#### 5.5.2 Using `ansible.builtin.get_url`

The `shell`, then `curl` or `wget` methods are obsolete ways by now, so use either `get_url` or `uri` modules. Using `get_url`, e.g.:

```
[...]
- name: "Download Package"
  ansible.builtin.get_url:
    url: "{{ pkg_host }}/{{ pkg_name }}"
    dest: "{{ pkg_localdir }}/{{ pkg_name }}"
    checksum: "{{ pkg_checksum }}" # must be in e.g. "sha256:asdf1234" format
[...]
```

More details on `get_url` can be found [here](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html).

#### 5.5.3 Check a downloaded file's statistics

...and fail if the stats do not pass the condition, e.g.:

```
[...]
- name: Get Stats of Downloaded RPM"
  ansible.builtin.stat:
    path: "{{ pkg_localdir }}/{{ pkg_name }}"
  register: rpmstats

- name: "Fail if RPM is Small"
  ansible.builtin.fail:
    msg: "The downloaded {{ pkg_name }} is not a valid RPM. Check https://blablabla.com for new version."
  when: rpmstats.stat.size < 1024
[...]
```

More details on `stat` can be found [here](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/stat_module.html).

### 5.6 Dry-run in Playbooks

Especially when manipulating files on the Managed Nodes like above, sometimes it's very useful and safe if you make sure first what the Playbook's run would change on the target system(s) without actually changing anything. E.g.:

```ksh
$ ansible-playbook playbook.yml -l devsrv03.example.com -t 'update_iptables' --check --diff
[...]
```

#### 5.6.1 The `check_mode` parameter

Ansible `check_mode` and dry-runs are controlled in two stages:

1. Ansible execution *knows* if it is running in `check_mode` or not, and stores that data in a boolean. This boolean can be then used for whatever purpose, although it is probably not something that should be leveraged too much to alter the behavior of Ansible runs. This would defeat the entire purpose of having dry-runs.
2. This boolean is then used to pass the information to an individual task, which will behave in whatever way they are configured to behave in `check_mode`. Some might completely ignore it and outright fail, while some might handle the `check_mode` elegantly, and produce a realistic simulation of what they would be doing. This also depends greatly on the nature of the task, and the module used.

Example of **evaluating** whether the run is in `check_mode` or not:

```
[...]
- name: "Unarchive jdk package to /usr/lib"
  ansible.builtin.unarchive:
    remote_src: true
    src: "/tmp/{{ item.java_package_filename }}"
     dest: "/usr/lib/"
     owner: root
     group: root
     mode: 0755
  when: not ansible_check_mode
[...]
```

Example for **ignoring** any errors if it's running in `check_mode`:

```
[...]
- name: Set management IP address
  set_fact:
    ipaddress_snmp_interface: "{{ ipmi_address.stdout|regex_replace(' ','') }}"
  ignore_errors: "{{ ansible_check_mode }}"
[...]
```

And examples for **enforcing** that a task is either never allowed to run in `check_mode` or just the opposite:

```
[...]
- name: This task will always make changes to the system
  ansible.builtin.command: /something/to/run --even-in-check-mode
  check_mode: false

- name: This task will never make changes to the system
  ansible.builtin.lineinfile:
    line: "important config"
    dest: /path/to/myconfig.conf
    state: present
  check_mode: true
  register: changes_to_important_config
[...]
```

More info on `check_mode` can be read [here](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_checkmode.html).

### 5.7 Organizing tasks in `block`

If you have multiple tasks that you would like to keep somehow linked, logically grouped to each other, you can use the `block` keyword, even multiple times within a Playbook. Also, .

Some more rules and hints to note regarding `block`: 

- all tasks in a block inherit directives applied at the block level
- the block's tasks can be defined even without spaces, but the indentation is important, as always. 
- you can even skip naming the tasks of a block one-by-one

Example for a Playbook using `block` **without** labeling the tasks in them: 

```
---
- name: Check for VPN network
  hosts: all:!maintenance
  become: true

  tasks:

  - name: Old RHELs
    block:

      - ansible.builtin.shell: iptables -L -n -w | grep '172.30.0.0'
        register: definition
        changed_when: false

      - ansible.builtin.debug:
          msg: "{{definition.stdout}}"

    when: (ansible_distribution_major_version | int < 8)

  - name: New RHELs
    block:

      - ansible.builtin.shell: nft list ruleset | grep '172.30.0.0'
        register: definition
        changed_when: false

      - ansible.builtin.debug:
          msg: "{{definition.stdout}}"

    when: (ansible_distribution_major_version | int >= 8)
...
```

The output of this: 

```ksh
$ ansible-playbook firewall_test.yml -l 'rockysrv2'

PLAY [Check for VPN network] *****************************************************************************

TASK [Gathering Facts] ***********************************************************************************
ok: [rockysrv2]

TASK [ansible.builtin.shell] *****************************************************************************
ok: [rockysrv2]

TASK [ansible.builtin.debug] *****************************************************************************
ok: [rockysrv2] => {
    "msg": "sshd       all  --  172.30.0.0/16        0.0.0.0/0           \nzabbix-proxy  all  --  172.30.0.0/16        0.0.0.0/0           "
}

TASK [ansible.builtin.shell] *****************************************************************************
skipping: [rockysrv2]

TASK [ansible.builtin.debug] *****************************************************************************
skipping: [rockysrv2]

PLAY RECAP ***********************************************************************************************
rockysrv2 : ok=3    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0   
```


<a name="6"></a>
## 6. Variables

From the variables' **source** point of view, as mentioned in a previous chapter already, a variable can "arrive" from the Inventory, defined as default one, defined for the Role or Playbook, or defined in a task too. 

Concerning data type of variables, it can be just a single value, a list, JSON data etc. Practically anything that Python3 also supports. 

### 6.1 Different Places for Variables

- the Inventory
- directly in the Playbook
- the `defaults/main.yml` or further files here
- the `vars/main.yml` or further files here
- passing `--extra-vars` to Playbook
- fetching the Shell's variables

A simple example for defining and using a variable directly in the Playbook: 

```
- hosts: all
  vars:
    remote_file_path: "/usr/lib/jvm/java-11-openjdk-11.0.21.0.9-1.portable.jdk.el.x86_64/bin/java"

  tasks:

  - name: Fetch sha256 checksum
    ansible.builtin.stat:
      name: "{{ remote_file_path }}"
      checksum_algorithm: sha256
    register: remote_checksum_result

  - name: Show checksum
    ansible.builtin.debug:
      var: remote_checksum_result.stat.checksum
```

### 6.2 Extra Variables for Playbook run

The `ansible` command supports overriding any variables in case of need. Here's an example when it was a list variable, referenced as this in the original Role: 

```
- name: Check if IdM port 53 (DNS) is reachable
  ansible.builtin.wait_for:
    host: "{{ item }}"
    port: 53
    state: started
    timeout: 10
  loop: "{{ dnsmasq_ipa_servers }}"
```

Then here's how we can **override** the list variable using Extra Vars (by `--extra-vars` or `-e` parameter), e.g.: 

```ksh
$ ansible-playbook playbook.yml -l 'myserver.example.com' --extra-vars dnsmasq_ipa_servers="['10.1.2.1', '10.1.2.2', '10.2.2.1', '10.2.2.2', '10.2.2.3']"
```

### 6.3 Fetching Shell's variables

It can be done with a combination of `lookup` plugin and `ansible.builtin.env` module: 

```
[...]
- name: Test connection against IdM
  ansible.builtin.uri:
    url: "https://{{ lookup('env','IPA_HOST') }}"
    method: GET
    timeout: 30
  register: connection_test
[...]
```

More on this technique can be read [here](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/env_lookup.html).

### 6.4 JSON Data

Here are two examples when a variable arrives as a JSON data and we want to print it: 

```
- name: Print JSON data
  debug:
    var: rhel_subs.json.body
```

...is the same as with...

```
- name: Print JSON data
  debug:
    msg: "{{ rhel_subs.json.body }}"
```


<a name="7"></a>
## 7. Conditionals and Loops in Tasks

![alt_text](python_-_control_flows.svg)

...

### 7.1 Basic Conditionals with `when`

...

If it's a simple check on a variable, then it can be used directly in conditionals, even without double curly braces, e.g.:

```
- name: Run the command if "foo" is defined
  ansible.builtin.shell: echo "I've got '{{ foo }}' and am not afraid to use it!"
  when: foo is defined
```

When a number is evaluated, you need to know that it's a text data, so it must be converted to integer first with `int`, e.g.:

```
- name: Install the latest version of Apache
  ansible.builtin.dnf:
    name: httpd
    state: latest
  when: ansible_distribution_major_version | int == 8
```

Listed items in a `when` condition are implicit `AND`s, e.g.:

```
  - name: Define default GW for the network in XYZ DC
    ansible.builtin.set_fact: 
      prv_gw:
[...]
    when:
      - "'dc_xyz' in group_names"
      - "'dc_xyz_z2' not in group_names"
```

This conditional searches for exact group names as words. In this case `dc_xyz` is **not** part of `dc_xyz_z2`. 


### 7.2 Conditionals in Jinja2 Format

Here's an example for such conditional for a Role's `defaults/main.yml` where defaults are defined for some local variables (if not found elsewhere, e.g. in Inventory, because that has higher priority): 

```
[...]
dnsmasq_domains: "{% if ( 'dc_aaa' in group_names ) -%}
                  xyzcompany.com
                  {%- else -%}
                  xyzcompany.com xyzcompany.fi xyzcompany.de
[...]
```

Using Inventory `group_vars` as conditional values for `with_items` parameter, e.g.: 

```
[...]
  with_items:
  - "{% if   ('dc_aaa_dev'  in group_names) or ('dc_bbb_dev'  in group_names) %}{{ query('inventory_hostnames', 'dc_bbb_fsh_dev:!maintenance') }}\
     {% elif ('dc_aaa_test' in group_names) or ('dc_aaa_prod' in group_names) %}{{ query('inventory_hostnames', 'dc_aaa_fsh:!maintenance') }}\
     {% elif ('dc_bbb_test' in group_names) or ('dc_bbb_prod' in group_names) %}{{ query('inventory_hostnames', 'dc_bbb_fsh:!maintenance') }}\
     {% endif %}"
[...]
```

Another good idea to have our variable defined in a `set_fact` block, e.g.:

```
[...]
- name: Locate RHEL Local Mirror
  ansible.builtin.set_fact:
    region: "{% if ('europe' in group_names) %}{{ 'eu' }}{% else %}{{ 'us' }}{% endif %}"

- name: Add RHEL Local Mirror Repo
  ansible.builtin.get_url:
    url: https://rhel-local-mirror-{{ region }}.example.com/rhel8mirror-{{ region }}.repo
    dest: /etc/yum.repos.d/rhel8mirror-{{ region }}.repo
    mode: '0644'
[...]
```

### 7.3 Invoking shell scripts

#### 7.3.1 When it is in separate `.sh` file

```
- name: Examine RHEL Customer POrtal's JSON data
  shell: roles/rhel-subscription-management/scripts/rhelsubdig.sh
  register: usage_rhel
  delegate_to: localhost
  when: rhel_subs_json is defined

- name: Print out RHEL Subscription quick report
  debug:
    msg: "{{ usage_rhel.stdout.split('\n') }}"
  when: usage_rhel is defined
```

#### 7.3.2 When the script is directly included to the playbook

```
- name: Check {{ inventory_hostname }}'s RHEL Subscription
  shell: |
    SUBSF=/tmp/subs_list.tmp
    CONSF=/tmp/subs_consumed.tmp
    subscription-manager list > $SUBSF
    subscription-manager list --consumed > $CONSF
    SUBNUM=$(grep Subscribed $SUBSF | wc -l)
    SKTNUM=$(dmidecode -t4 | grep Socket.Designation: | wc -l)
    echo "{{ inventory_hostname }} has $SKTNUM CPU Sockets and $SUBNUM RHEL Subscriptions."
    if [ $SKTNUM -gt 7 ] ; then SUBREQ=4 ; else if [ $SKTNUM -gt 3 ] ; then SUBREQ=2 ; else SUBREQ=1 ; fi ; fi
    if [ $SUBNUM -eq 0 ] ; then echo "{{ inventory_hostname }} is not registered.
    Estimated requirement is $(( 2 * $SUBREQ ))" ; exit ; fi
    echo "Required, calculated Quantity: $(( $SUBNUM * $SUBREQ ))"
    echo "RHEL Subscription Quantity used already: $(awk '/Quantity Used/{print $3}' $CONSF)"
    echo "RHEL Subsctiption Contract number assigned already: $(awk '/Contract:/{print $2}' $CONSF)"
    rm $SUBSF $CONSF
  register: usage_host

- name: Print out {{ inventory_hostname }}'s RHEL Subscription report
  debug:
    msg: "{{ usage_host.stdout.split('\n') }}"
  when: usage_host is defined
```

### 7.4 Looping Through Multiple Tasks

It's not possible in a single Playbook, but achieavable by using `ansible.builtin.include_tasks` module. E.g.:

1. In the directly invoked task file: 

```
---
- name: Looping through HFS hosts for adding host keys
  ansible.builtin.include_tasks: 02_hfs-host-keys_tasks.yml
  with_items:
    - "{{ dc_aaa_fsh }}"
  when: "'webservers' in group_names"
```

2. And in the invoked `_tasks.yml` file: 

```
---
- name: Fetch and deploy host keys
  block:

    - name: Fetch ED25519 host key of "{{ item }}"
      ansible.builtin.command: ssh-keyscan -t ssh-ed25519 "{{ item }}"
      register: hfs_hostkey
      delegate_to: localhost
      become: false
      changed_when: false

    - name: Ensure ED25519 host key is present for abcd user
      ansible.builtin.known_hosts:
        path: /home/abcd/.ssh/known_hosts
        name: "{{ item }}"
        key: "{{ hfs_hostkey.stdout }}"
```

### 7.5 Looping through shell command output results

Here's an example when the `register` function registers results of multiple items. 

It's not a simple  the `ansible.builtin.debug` task for printing out results also need to use multiple items because the `stdout` or `stdout_lines` aren't directly accessible: 

```
  - name: Check connectivity
    ansible.builtin.shell: |
      set -o pipefail
      nmap -Pn -p 4140,4141,4142 {{ item }}
    register: test_results
    with_items: "{{ query('inventory_hostnames', '{{ fleet }},!maintenance') }}"

  - name: Print results
    ansible.builtin.debug:
      msg: "{{ item.stdout_lines }}"
    with_items: "{{ test_results.results }}"
```

If we apply printing out the whole `test_results` temporarily, we can understand why it's needed: 

```ksh
$ ansible-playbook port_test_by_fleet.yml -l rockysrv1.example.com --extra-vars fleet=compute_servers
[...]
ok: [rockysrv1.example.com] => {
    "msg": {
        "changed": true,
        "msg": "All items completed",
        "results": [
            {
                "ansible_loop_var": "item",
                "changed": true,
                "cmd": "set -o pipefail\nnmap -Pn -p 4140,4141,4142 rockysrv2.example.com\n",
[...]
                "stdout_lines": [
                    "Starting Nmap 7.92 ( https://nmap.org ) at 2024-03-04 13:41 EET",
                    "Nmap scan report for rockysrv2.example.com (10.20.30.41)",
                    "Host is up (0.00096s latency).",
                    "",
                    "PORT     STATE    SERVICE",
                    "4140/tcp open     cedros_fds",
                    "4141/tcp open     oirtgsvc",
                    "4142/tcp filtered oidocsvc",
                    "",
                    "Nmap done: 1 IP address (1 host up) scanned in 1.33 seconds"
                ]
            },
            {
                "ansible_loop_var": "item",
[...]
```

### 7.6 Looping through JSON Arrays

Some references to this: 
- [basics](http://www.freekb.net/Article?id=2417)
- [adding looping through the indexes](https://stackoverflow.com/questions/69958185/ansible-loop-through-json-output-and-add-indexing)

Simple raw output of receiving all the data:

```
- name: Print JSON data
  debug:
    var: rhel_subs.json.body
  when: rhel_subs.json.body is defined
```

This example works for the 11th indexed element of the array:

```
- name: Print JSON data
  debug:
    msg: "{{ rhel_subs.json.body[10].subscriptionNumber}}"
  when: rhel_subs.json.body is defined
```

Print the specified value of all the elements of the JSON array:

```
- name: Print JSON data
  debug:
    msg: "subscriptionNumber[{{ my_idx }}]: {{ item.subscriptionNumber }}"
  loop: "{{ rhel_subs.json.body }}"
  loop_control:
    index_var: my_idx
  when: rhel_subs.json.body is defined
```


<a name="8"></a>
## 8. Vault

It's always handy to store all our "secrets" in hashed format in our simple text YAML files of Roles, Playbooks, anywhere. 

To encrypt any text, you can use the `ansible-vault` command directly. Examples for encrypting, then decrypting to another file:

```ksh
$ cat encrypted_data 
Very important and secret text.

$ ansible-vault encrypt encrypted_data 
New Vault password: 
Confirm New Vault password: 
Encryption successful

$ cat encrypted_data 
$ANSIBLE_VAULT;1.1;AES256
6138346166353638323533323431[...]

$ ansible-vault decrypt --output decrypted_data encrypted_data 
Vault password: 
Decryption successful
```

### 8.1 Storing and using Ansible Vault data

Storing is nothing but keeping our Ansible variables in either in a regular or an encrypted format. E.g., here are chunks of a `vars/main.yml` file:

```
[...]
ipaclient_principal: admin
ipaclient_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          32101[...]23456
[...]
```

Then you reference to it in the task you need that to be included, just like any variables:

```
[...]
- name: Run default FreeIPA Client installer
  command: >
    ipa-client-install
    --unattended
    --mkhomedir
    --no-ntp
    --no-nisdomain
    --no-dns-sshfp
    --no-krb5-offline-passwords
    --hostname={{ inventory_hostname  }}
    --domain={{ ipaclient_domain }}
    --principal={{ ipaclient_principal }}
    --password={{ ipaclient_password }}
[...]
```

When it's all done, you can issue your Playbook with `--ask-vault-pass` parameter:

```ksh
$ ansible-playbook playbook.yml -l my_target_host --ask-vault-pass
Vault password: 
[...]
```

### 8.2 The Vault Password File

In order to just make your Control Node working more conveniently, you can set an environment variable `ANSIBLE_VAULT_PASSWORD_FILE` storing the path to the Vault password file. All alone it doesn't sound too secure, but might be convenient during development. 

If Ansible is combined with GitLab CI/CD, it can be very efficiently just stored in a `File` type variable in Settings --> CI/CD:

![alt_text](ansible_-_vault_in_gitlab_cicd.png)


<a name="9"></a>
## 9. Roles

Ansible Roles are re-usable code collections to provide any relational functionalities by a single bundle. 

A typical example is when you create a Role called `httpd` which not only ensures the **packages are installed** but it automatically ensures all the **prerequisites** are in place, **configures** the service based on **conditions**, then also **starts** it. 

In fact, just because it's almost never an installation only, followed by some manual tasks but a fully automated set of tasks with all these above 4 steps, we always refer to these activities as **deployments instead of installations**.

Further details on Roles can be found at [docs.ansible.com](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html). 

### 9.3 The standard Role structure

For Roles, the directory structure is relatively strict (copied from Ansible's documentation): 

```
roles/
    common/               # this hierarchy represents a "Role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this Role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this Role
        meta/             #
            main.yml      #  <-- Role dependencies
        library/          # Roles can also include custom modules
        module_utils/     # Roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    webtier/              # same kind of structure as "common" was above, done for the webtier Role
    monitoring/           # ""
    fooapp/               # ""
```

You can prepare the above directory structure (and beyond) quickly by `ansible-galaxy` tool. The tool's primary purpose is to *"perform various Role and Collection related operations"*, so its `init` operation simply assumes that you want to create a new Role:

```ksh
$ ansible-galaxy init mynewrole
- Role mynewrole was created successfully

$ tree mynewrole
mynewrole
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml

9 directories, 8 files
```

### 9.2 Using a Role as an Individual Playbook

It's not a common practice to directly invoke the Role from itself, but there're couple of cases when it's necessary and it's the easiest way to go. 

Here's the most simplest example for such a Playbook YAML file, in the Role's root directory: 

```
---
- hosts: all
  roles:
    - ../mynewrole
```

A much more sophisticated example with strictly defining variables, `handlers` and `tasks`: 

```
---
- hosts: all
  become: true
  ignore_unreachable: yes
  vars_files:
    - defaults/main.yml
  handlers:
    - ansible.builtin.import_tasks: handlers/main.yml
  tasks:
    - name: Executing the Role as a playbook
      ansible.builtin.import_tasks: "tasks/main.yml"
```

### 9.3 Invoking Roles from External Playbooks

This is the way how an enterprise infrastructure should be managed always. Such as: 

1. keep all the codes in a structured way in a version controlled place, e.g. in **GitLab**
2. keep the installation/configuration/adjustment of the same component within a Role (e.g. `httpd`)
3. maintain tagging for its latest stable version (optional but recommended)
4. reference to the Role as its `main`/`master` branch or its latest stable tag (optional but recommended) like below. 

The required two placements of our Roles in a Playbook structure that invokes them: 

1. You need to invoke all the Roles you want with conditionals (restricted either by `hosts` or `when` keywords) **in the main Playbook file** (e.g. to `playbook.yml`) of the external repo, e.g.: 

```
---
- name: Apply baseline Roles
# Roles targeted to any Calculation or K8sop (but non-controllers) servers
  hosts: xyz_servers:!maintenance
  become: true
  roles:
    - newhost
    - {role: nftables, when: (ansible_distribution_major_version | int >= 8) and ('k8sop' not in group_names)}
    - {role: httpd, when: ('scaleout' not in group_names)}
[...]
```

(As you can see, the `when` conditionals can be easily combined, following the Python syntax.)

2. You need to **list all the Roles** in `./roles/requirements.yml` file of this external Playbook repo, e.g.:

```
---
- include: roles/scaleout/requirements.yml

- src: git@gitlab.examplecompany.com:system-management/ansible-roles/newhost.git
  scm: git
  version: v1.3.6

- src: git@gitlab.examplecompany.com:system-management/ansible-roles/nftables.git
  scm: git
  version: v1.3.3

- src: git@gitlab.examplecompany.com:system-management/ansible-roles/httpd.git
  scm: git
  version: v1.6.0
[...]
```

Once you are prepared with these, you can just `git clone` (or `git pull` to existing directory) to your Control Node, then you can run `ansible-galaxy install`, e.g.:

```ksh
$ ansible-galaxy install -r roles/requirements.yml -p roles --force
Starting galaxy role install process
- changing role newhost from v1.3.6 to v1.3.6
- extracting newhost to /home/john.doe/system-management/ansible-playbooks/nessus-playbook/roles/newhost
- newhost (v1.3.6) was installed successfully
[...]
```

You can also prepare a shell script, typically called as `install_requirements.sh`, for this purpose to make it convenient next time you need to do the same, e.g.:

```
#!/bin/bash

set -e
set -u

REQ="requirements.yml"

[[ -d ./roles ]] && [[ -f ./$REQ ]] && [[ $(which ansible-galaxy 2>&1) != *" no "* ]] && {

	#
	#
	ansible-galaxy role install -r $REQ -p roles -nfg | grep -v extracting
	ansible-galaxy collection install -r $REQ -p collections/ansible_collections -nf | grep -v Installing

} || {
	echo "Missing either ./roles or ./requirements.yml or ansible-galaxy itself!"
	exit 1
}
```


<a name="10"></a>
## 10. Handlers and Notifiers

Tasks can instruct one or more handlers to execute using the `notify` keyword which notifies the related, pre-configured handler for those notifications. 

Using handlers and notifiers is useful if our goal is that the tasks should just continue, not really caring about the outcome of the service restarts or other activities.  
Also, handlers from Roles are not just contained in their Roles but rather **inserted into global scope** with all other handlers from a play. As a result, they can be used outside of the Role they are defined in.

The typical usage of handlers and notifiers is combining them with the [ansible.builtin.service](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html) module, like in this example: 

- under `tasks/something-something.yml` file: 

```
[...]
- name: Copy PSK key to target server
  template:
    src: zabbix_agentd.psk.j2
    dest: /etc/zabbix/zabbix_agentd.psk
  notify:
  - Restart zabbix agent2
[...]
```

- in `handlers/main.yml` file: 

```
[...]
- name: Restart zabbix agent2
  ansible.builtin.service:
    name: zabbix-agent2
    state: restarted
    enabled: yes
[...]
```


<a name="11"></a>
## 11. Ansible Collections

The [docs.ansible.com](https://docs.ansible.com/ansible/latest/collections_guide/index.html) summarizes them as: 

>Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins.

However, usually we mean the installed Ansible Modules by this term.

Listing current collection:

```ksh
$ ansible-galaxy collection list

# /home/john.doe/.ansible/collections/ansible_collections
Collection           Version
-------------------- -------
amazon.aws           4.2.0  
ansible.netcommon    4.1.0  
ansible.posix        1.4.0  
ansible.utils        2.9.0  
ansible.windows      1.13.0 
community.aws        4.2.0  
community.general    5.6.0  
community.mysql      3.6.0  
community.postgresql 2.3.2  
community.zabbix     1.9.2  
```

Enforced update on a collection: 

```ksh
$ ansible-galaxy collection install community.zabbix
Starting galaxy collection install process
Nothing to do. All requested collections are already installed. If you want to reinstall them, consider using `--force`.

$ ansible-galaxy collection install community.zabbix --force
Starting galaxy collection install process
[...]
community.zabbix:2.0.0 was installed successfully
[...]

$ ansible-galaxy collection list

# /home/john.doe/.ansible/collections/ansible_collections
Collection           Version
-------------------- -------
[...]
community.zabbix     2.0.0  
```

You can also create your custom Collection directory under the current working directory (e.g. for a Playbook's Repo) in `collections/ansible_collections/` directory structure. 

It's completely fine storing the references to collections where the Roles are stored, e.g. a `requirements.yml`. In that case, you can install the collection with some extra parameters, e.g.: `ansible-galaxy collection install -r requirements.yml -p collections/ansible_collections -nf`

You can read more on Ansible Collection installations [here](https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html). 


<a name="12"></a>
## 12. Linting our Ansible Code

The tool can be installed the same way as Ansible itself: either from your distro's repo or as a PIP package. 

Simply just go into the Playbook's or Role's root directory and issue `ansible-lint .`, e.g.:

```ksh
$ ansible-lint .
WARNING  Listing 4 violation(s) that are fatal
[...]
Failed: 4 failure(s), 0 warning(s) on 7 files. Last profile that met the validation criteria was 'min'.
```

The validation criteria can be configured further; this `min` just comes from the default installation. Now that the failures/warnings are revealed, you just need to consider fixing them one-by-one in the code and try again, even though it is working already.
