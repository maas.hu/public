# 🌖 Diodes

<div style="text-align: right;"><h7>Posted: 2022-11-07 21:49</h7></div>

###### .

## Table of contents

<img src="diodes.svg" title="" align="right" class="right">

1. [Introduction and Packaging Types](#1)
2. [General Purpose Diodes](#2)
3. [Zener Diodes](#3)
4. [Varicap Diodes](#4)


<a name="1"></a>
## 1. Introduction and Packaging Types

...


<a name="2"></a>
## 2. General Purpose Diodes

<table class="tan">
  <tr>
    <th>Charasteristics</th>
    <th width="60px">
      <a href="https://www.vishay.com/docs/88503/1n4001.pdf">1N4001</a>
    </th>
    <th width="60px">
      <a href="https://www.vishay.com/docs/88503/1n4001.pdf">1N4004</a>
    </th>
    <th width="60px">
      <a href="https://www.onsemi.com/pdf/datasheet/1n914-d.pdf">1N4148</a>
    </th>
    <th width="60px">
      <a href="https://www.onsemi.com/pdf/datasheet/1n914-d.pdf">1N4448</a>
    </th>
    <th width="60px">
      <a href="https://www.mouser.com/datasheet/2/308/1n4151-1190007.pdf">1N4151</a>
    </th>
  </tr>
  <tr>
    <td><b>Repetitive Peak Reverse Voltage</b> <sup>1</sup> (max.) [V]</td>
    <td>50</td>
    <td>400</td>
    <td>100</td>
    <td>100</td>
    <td>75</td>
  </tr>
  <tr>
    <td><b>Average Rectified Forward Current</b> <sup>1</sup> (max.) [mA]</td>
    <td>1000</td>
    <td>1000</td>
    <td>200</td>
    <td>200</td>
    <td>150</td>
  </tr>
  <tr>
    <td><b>Instantaneous Forward Voltage</b> <sup>2</sup> (min., max.) [V]</td>
    <td>-, 1.1</td>
    <td>-, 1.1</td>
    <td>0.62, 0.72</td>
    <td>-, 1</td>
    <td>-, 1</td>
  </tr>
  <tr>
    <td><b>Junction Capacitance</b> <sup>3</sup> (min., avg., max.) [pF]</td>
    <td>-, 15, -</td>
    <td>-, 15, -</td>
    <td>-, -, 2</td>
    <td>-, -, 2</td>
    <td>-, -, 2</td>
  </tr>
  <tr>
    <td><b>Typical Packaging</td>
    <td>DO-41</td>
    <td>DO-41</td>
    <td>DO-35</td>
    <td>DO-35</td>
    <td>DO-35</td>
  </tr>
</table>

Test Conditions: 

- <sup>1</sup> T<sub>a</sub> = 25°C
- <sup>2</sup> Instantaneous Forward Voltage (V<sub>F</sub>):
    - I<sub>F</sub> = 1 A for 1N4001, 1N4004
    - I<sub>F</sub> = 10 mA for 1N4148
    - I<sub>F</sub> = 5 mA for 1N4448
    - I<sub>F</sub> = 50 mA for 1N4151
- <sup>3</sup> Junction Capacitance (C<sub>J</sub>):
    - 4.0 V, 1 MHz for 1N4001, 1N4004
    - V<sub>R</sub> = 0, f = 1 MHz for 1N4148, 1N4448, 1N4451

<a name="3"></a>
## 3. Zener Diodes

Test Conditions: 

- <sup>1</sup> T<sub>a</sub> = 25°C
- <sup>2</sup> Zener Voltage (V<sub>Z</sub>):
    - I<sub>Z</sub> = 5 mA for ZPD*, PHC3V3
- <sup>3</sup> Dynamic Resistance (r<sub>Z</sub>):
    - I<sub>Z</sub> = 1 mA for ZPD*
    - I<sub>Z</sub> = 5 mA for PHC3V3

### 3.1 ZPDxx Series

<table class="tan">
  <tr>
    <th>Charasteristics</th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD2V7</a>
    </th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD3V3</a>
    </th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD5V1</a>
    </th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD5V6</a>
    </th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD11</a>
    </th>
    <th width="80px">
      <a href="https://www.digchip.com/datasheets/parts/datasheet/420/ZPD4V7-pdf.php">ZPD27</a>
    </th>
  </tr>
  <tr>
    <td><b>Zener Voltage</b> <sup>2</sup> (min., max.) [V]</td>
    <td>2.5, 2.9</td>
    <td>3.1, 3.5</td>
    <td>4.8, 5.4</td>
    <td>5.2, 6</td>
    <td>10.4, 11.6</td>
    <td>25.1, 28.9</td>
  </tr>
  <tr>
    <td><b>Dynamic Resistance</b> <sup>3</sup> (min., max.) [Ω]</td>
    <td>83, 500</td>
    <td>95, 500</td>
    <td>60, 480</td>
    <td>40, 400</td>
    <td>20, 70</td>
    <td>80, 250</td>
  </tr>
  <tr>
    <td><b>Admissible Zener Current</b> <sup>1</sup> [mA]</td>
    <td>160</td>
    <td>130</td>
    <td>80</td>
    <td>70</td>
    <td>36</td>
    <td>14</td>
  </tr>
  <tr>
    <td><b>Typical Packaging</td>
    <td>DO-35</td>
    <td>DO-35</td>
    <td>DO-35</td>
    <td>DO-35</td>
    <td>DO-35</td>
    <td>DO-35</td>
  </tr>
</table>

### 3.2 BZXxx Series

<table class="tan">
  <tr>
    <th>Charasteristics</th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C2V7</a>
    </th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C3V3</a>
    </th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C5V1</a>
    </th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C5V6</a>
    </th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C11</a>
    </th>
    <th width="80px">
      <a href="https://www.vishay.com/docs/85607/bzx85.pdf">BZX85C27</a>
    </th>
  </tr>
  <tr>
    <td><b>Zener Voltage</b> <sup>2</sup> (min., max.) [V]</td>
    <td>2.64, 2.76</td>
    <td>2.24, 3.36</td>
    <td>5, 5.2</td>
    <td>5.49, 5.71</td>
    <td>10.8, 11.2</td>
    <td>26.5, 27.5</td>
  </tr>
  <tr>
    <td><b>Dynamic Resistance</b> <sup>3</sup> (min., max.) [Ω]</td>
    <td>20, 400</td>
    <td>20, 400</td>
    <td>10, 500</td>
    <td>7, 400</td>
    <td>8, 300</td>
    <td>30, 750</td>
  </tr>
  <tr>
    <td><b>Admissible Zener Current</b> <sup>1</sup> [mA]</td>
    <td>360</td>
    <td>300</td>
    <td>200</td>
    <td>190</td>
    <td>97</td>
    <td>41</td>
  </tr>
  <tr>
    <td><b>Typical Packaging</td>
    <td>DO-41</td>
    <td>DO-41</td>
    <td>DO-41</td>
    <td>DO-41</td>
    <td>DO-41</td>
    <td>DO-41</td>
  </tr>
</table>



<a name="3"></a>
## 4. Varicap Diodes

Test Conditions: 

- <sup>1</sup> T<sub>a</sub> = 25°C
- <sup>2</sup> Diode Capacitance (C<sub>d</sub>):
    - V<sub>R</sub> = 1 V; f = 1 MHz for BB141A
- <sup>2</sup> Capacitance Ratio:
    - f = 1 MHz for BB141A

<table class="tan">
  <tr>
    <th>Charasteristics</th>
    <th width="100px">
      <a href="https://datasheetspdf.com/pdf-file/493449/Philipss/BB141/1">BB141A (ITT)</a>
    </th>
  </tr>
  <tr>
    <td><b>Repetitive Peak Reverse Voltage</b> <sup>1</sup> (max.) [V]</td>
    <td>8</td>
  </tr>
  <tr>
    <td><b>Average Rectified Forward Current</b> <sup>1</sup> (max.) [mA]</td>
    <td>20</td>
  </tr>
  <tr>
    <td><b>Diode Capacitance</b> <sup>2</sup> (min., typ., max.) [pF]</td>
    <td>3.9, 4.2, 4.5</td>
  </tr>
  <tr>
    <td><b>Capacitance Ratio</b> <sup>3</sup> (min., typ.)</td>
    <td>1.65, 1.76</td>
  </tr>
  <tr>
    <td><b>Typical Packaging</td>
    <td>DO-35</td>
  </tr>
</table>
