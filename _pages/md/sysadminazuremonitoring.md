# ![alt text](/_ui/img/icons/azure_monitoring_m.svg) Azure Monitoring and Logging

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_monitoring_-_bg.jpg)


## Table of Contents

1. [Johdanto](#1)
2. [Azure Monitor](#2)
3. [Log Analytics](#3)
4. [Hälytykset ja Toiminnot](#4)
5. [Azure Insights](#5)
6. [Azure Workbooks](#6)


<a name="1"></a>
## 1. Johdanto

Azure tarjoaa laajan valikoiman työkaluja, jotka mahdollistavat järjestelmien seurantaa, lokien keräämistä ja automaattisten hälytysten asettamista. **Azure Monitor** ja siihen liittyvät työkalut, kuten Log Analytics ja Azure Insights, auttavat organisaatioita seuraamaan infrastruktuurin ja sovellusten suorituskykyä ja turvallisuutta. Tämän artikkelin tarkoituksena on esitellä yleisimmät Azure Monitorin ja lokien hallinnan ominaisuudet ja ratkaisut. Azure Monitor tarjoaa monipuoliset mahdollisuudet kerätä tietoa sovellusten ja infrastruktuurin tilasta, analysoida sitä sekä tehdä päätöksiä datan perusteella.

Azure Monitor ja siihen liittyvät työkalut, kuten **Log Analytics** ja **Azure Insights**, tarjoavat kattavan valikoiman työkaluja resurssien seurantaan ja hallintaan. Niiden avulla voidaan kerätä, analysoida ja visualisoida dataa infrastruktuurista, sovelluksista ja mukautetuista lähteistä. Järjestelmänhallinnassa nämä työkalut ovat välttämättömiä ennakoivan huollon, suorituskyvyn optimoinnin ja turvallisuuden varmistamiseksi.

Artikkelissa käydään läpi pääkohdat Azure Monitorin käytöstä ja siihen liittyvistä ratkaisuista, mutta uusimpien ominaisuuksien ja ajantasaisten tietojen osalta suosittelemme tutustumaan [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/azure-monitor/) -sivustoon.


<a name="2"></a>
## 2. Azure Monitor

**Azure Monitor** kerää ja analysoi tietoja useista lähteistä, kuten sovelluksista, infrastruktuurista, Azure-alustasta ja mukautetuista lähteistä. Monitorin keräämä data auttaa ymmärtämään järjestelmän tilaa, kuten resurssien käyttöä, suoritusta ja mahdollisia ongelmia.

### 2.1 Tietolähteet

Azure Monitor kerää dataa seuraavista lähteistä:

- **Sovellukset**: Kerätään tiedot sovelluksen suorituskyvystä ja toiminnasta.
- **Infrastruktuuri**: VM:ien ja muiden resurssien suorituskykytiedot.
- **Azure Platform**: Azure-resurssien tila ja käyttö.
- **Mukautetut lähteet**: Asiakkaan määrittelemät datalähteet.

### 2.2 Toiminnot

Azure Monitor tarjoaa useita hyödyllisiä toimintoja, kuten:

- **Insights**: Esimerkiksi sovellus-, säilö- ja VM-tiedot.
- **Visualisointi**: Työkuormat, mittaristot, Power BI, Grafana.
- **Analysointi**: Mittarien ja lokien analysointi.
- **Reagointi**: Hälytykset ja automaattinen skaalaus.
- **Integraatio**: Tiedonsiirto ulkoisiin järjestelmiin, kuten Event Hubs ja Logic Apps.


<a name="3"></a>
## 3. Log Analytics

**Log Analytics** on työkalu, jolla voidaan kerätä ja analysoida lokeja eri järjestelmistä. Se tallentaa datan NoSQL-pohjaiseen tietokantaan, jonka avulla voidaan suorittaa kyselyjä lokien analysoimiseksi. Tietoja kerätään eri lähteistä ja ne tallennetaan **Log Analytics Workspace**-tilaan.

### 3.1 Tietojen hallinta

Log Analytics Workspace mahdollistaa:

- Datan tallennuksen valittuun maantieteelliseen sijaintiin.
- Käyttöoikeuksien hallinnan, jossa eri tiimit voivat käyttää eristettyjä resursseja.
- Asetusten, kuten hintaluokan ja säilytysajan, määrittelyn.

### 3.2 Agentit

Azure Monitorin lokitietojen keräämiseksi käytetään **Azure Monitoring Agent** (AMA) -agenttia. Vanhempi **Log Analytics Agent** on kuitenkin edelleen käytössä joissain tapauksissa, mutta AMA on suositeltu.


<a name="4"></a>
## 4. Hälytykset ja Toiminnot

Azure Monitorin avulla voit luoda hälytyksiä, jotka käynnistyvät, kun tietty mittari tai lokikysely saavuttaa ennalta asetetut kynnysarvot. Näiden hälytysten avulla voidaan ryhtyä automaattisiin toimenpiteisiin, kuten skaalaamiseen tai viestien lähettämiseen.

### 4.1 Hälytykset

Hälytykset määritellään hälytyssääntöjen kautta, ja ne voidaan asettaa esimerkiksi tietylle VM:lle, joka saavuttaa CPU-käytön rajan tai jää käyntitilaan, vaikka se on sammutettu.

### 4.2 Toiminnot ja Reaktiot

Hälytysten aktivoituessa voidaan käyttää eri toimintoja, kuten:

- **Skaalaaminen**: Resurssien skaalaus automaattisesti kuormituksen perusteella.
- **Toimintaryhmät**: Voidaan konfiguroida lähettämään ilmoituksia tiimeille.


<a name="5"></a>
## 5. Azure Insights

**Azure Insights** on osa Azure Monitoria ja tarjoaa syvällisiä näkemyksiä sovellusten ja järjestelmien toiminnasta. Insights tarjoaa reaaliaikaista tietoa sovellusten tilasta ja auttaa diagnosoimaan suorituskykyongelmia.

### 5.1 Sovellus- ja Infrastruktuuri-Insights

Insights voidaan ottaa käyttöön esimerkiksi Web Appin tai virtuaalikoneen asetuksista, jolloin se alkaa kerätä tietoja sovelluksen tai infrastruktuurin suorituskyvystä.

### 5.2 Reaaliaikaiset Mittarit ja Analyysit

Insights tarjoaa reaaliaikaisia mittareita, kuten suorituskeskiarvoja, vastauksen viiveitä ja latensseja. Tämä auttaa järjestelmäarkkitehteja optimoimaan sovelluksia ja resursseja.


<a name="6"></a>
## 6. Azure Workbooks

**Azure Workbooks** on joustava raportointityökalu, joka tarjoaa mahdollisuuden yhdistää dataa eri lähteistä ja luoda visuaalisia raportteja. Workbooks yhdistää JSON-, Markdown- ja Kusto Query Language (KQL) -kielien elementtejä tarjotakseen laajoja analyysimahdollisuuksia.

### 6.1 Ominaisuudet

Workbooks mahdollistaa useiden datalähteiden hyödyntämisen ja niiden yhdistämisen. Sitä voidaan käyttää raportointiin ja analysointiin eri käyttötapauksissa, kuten liiketoimintamittareiden seuraamisessa tai infrastruktuurin tilan analysoinnissa.

### 6.2 Integraatio

Workbooks voidaan integroida muihin Azure-työkaluihin, kuten Power BI ja Log Analytics, mikä mahdollistaa dynaamisten raporttien luomisen ja jakamisen tiimeille.
