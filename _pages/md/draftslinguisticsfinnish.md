# ![alt text](/_ui/img/flags/fin_m.svg) Finnish Language / Suomen Kieli

<div style="text-align: right;"><h7>Posted: 2019-10-25 17:41</h7></div>

###### .

<div class="info">
<p>This article is under construction. It's about 40% ready.</p>
</div>

## Table of Contents

1. [Introduction](#1)
2. [Alphabet, Everyday Expressions, Idioms and Phrases](#2)
3. [Vowel Harmony, K-P-T-Variations](#3)
4. [Verbtypes and Wordtypes](#4)
5. [Forming Basic Sentences](#5)
6. [Directions and Locations](#6)
7. [Pronouns - Prononimit](#7)
8. [Partitiivi](#8)
9. [Akkusatiivi](#9)
10. [Past Tense](#10)
11. [Conditional](#11)


<a name="1"></a>
## 1. Introduction - Johdanto

<a href="suomi-revontulet.jpg" target="_blank" title="">
  <img src="suomi-revontulet.gif" title="" align="right" class="right"></a>

**Finnish** (*suomi* or *suomen kieli*) is the most significant language of the Finno-Ugric family's Balto-Finnic branch. It's spoken as a mother thounge by approx. 4.8 million people, and further half million speak it as a second language. It is a very close relative to the Estonian language: so close, that it is, more or less, mutually intelligable between the speakers of these languages. (More or less, because it is rather true only in very simple sentences, and situations, and mostly Estonians understand Finns more.)

### 1.1 Origin

The Finno-Ugric languages evolved from the ancient Uralic, and according to some experts' research, Finnish is the closest to the ancient Uralic among the languages still alive today. According to the current scientific results, the ancient Uralic was spoken roughly 6000 years ago (and it's widely accepted its most ancient dialect appeared already 10000 years ago), and then, as the folks were migrating, over the centuries, it split into the ancient Ugric and Finno-Permic subdivisions. The Proto-* branches below are all reconstructed, ancestral languages, based on the latest results of many-many research over the recent two centuries. 

![alt_text](uralic_lang_en.png "Relationships between extinct Uralic languages and the currently living Finno-Ugric ones")

Sources for the above illustration: 
- [Proto-Uralic #1](https://www.britannica.com/topic/Uralic-languages)
- [Proto-Uralic #2](https://en.wikipedia.org/wiki/Proto-Uralic_language)
- [Finno-Permic](https://en.wikipedia.org/wiki/Finno-Permic_languages)
- [Finno-Ugric, Samoyedic](https://en.wikipedia.org/wiki/Finno-Ugric_languages)
- [Proto-Ugric](https://en.wikipedia.org/wiki/Ugric_languages)
- [Proto-Samoyedic](https://en.wikipedia.org/wiki/Proto-Samoyedic_language)

### 1.2 Similarities with Hungarian

Although they are not mutually intelligible, due to their common origin, Finnish is also related to Hungarian. In addition to the similarity of several grammatical rules, there are approximately 800 common rooted words in Finnish and Hungarian. Sometimes this common root is very easy to notice and accept as such (e.g. *käsi*, *vesi*, *kivi*, *sydän*, *kala*, *veri*, *uida*, *mennä*, *tehdä* etc.). Sometimes the same origin was only discovered by linguists, but even these words are generally easier to learn by a Hungarian (e.g. *Missä?* &rarr; *Hol?* (*"messze"* - far away)).

In addition to this relatively large number of commonly originated, somewhat similar words, it is important to note some key similarities in these languages: 
- nouns have no gender,
- the stress always falls on the first syllable of words,
- both languages are as strongly *agglutinative*, i.e. *gluing* language (also known as *"ragozó"* or *toldalékoló* in Hungarian),
- vowel harmony is essential in the conjugated forms,
- using the same logic during describing time, or e.g. conjugating verbs for groups of nouns
- following the same logic even when creating combined words (e.g. *lentokone (repülőgép) - airplane*, *sateenvarjo (esernyő) - umbrella*, *ilotyttö (örömlány) - whore ("joygirl")*).

And this list is far from being complete, these are just some examples. 

These common features are at least as important in recognizing the linguistic kinship of these languages as the widely known, common, ancestral words. Despite the fact that Finnish is considered at least as complicated and difficult to learn as Hungarian, it can be confidently stated, thanks to the above similarities, that Finnish is easier to learn by a Hungarian than, for example, by a native English speaker.

<div class="info">
<p>For a list of common rooted words, see the subpage <b><a href="dictionary">Dictionary of Finnish-Hungarian Common Origin Words</a></b>.</p>
</div>


<a name="2"></a>
## 2. Alphabet, Everyday Expressions, Idioms and Phrases - Aakkoset, Jokapäiväiset Ilmaisut, Idiomit ja Fraasit

...

### 2.1 The Alphabet

<table class="tan">
  <tr>
    <td><b>A a</b></td><td><b>B b</b></td><td><b>C c</b></td><td><b>D d</b></td><td><b>E e</b></td><td><b>F f</b></td><td><b>G g</b></td><td><b>H h</b></td>
  </tr>
  <tr>
    <td><b>I i</b></td><td><b>J j</b></td><td><b>K k</b></td><td><b>L l</b></td><td><b>M m</b></td><td><b>N n</b></td><td><b>O o</b></td><td><b>P p</b></td>
  </tr>
  <tr>
    <td><b>Q q</b></td><td><b>R r</b></td><td><b>S s</b></td><td><b>T t</b></td><td><b>U u</b></td><td><b>V v</b></td><td><b>W w</b></td><td><b>X x</b></td>
  </tr>
  <tr>
    <td><b>Y y</b></td><td><b>Z z</b></td><td><b>Å å</b></td><td><b>Ä ä</b></td><td><b>Ö ö</b></td>
  </tr>
</table>

#### 2.1.1 Pronounciation

Finnish is almost completely pronounced as written, so it's a phonetic language. 

Short list of the bit more difficult sounds, with their IPA representatives: 

- **y** [ y ]
- **s** [ s ] - the Hungarian "s" ("sh") is very rare in Finnish and that's also written as "sh". E.g. *shamaani*
- **e** [ e ] - a magyar "e"-nél zártabb, a tájnyelvi "ë"-nek felel meg  
- **ä** [ æ ] - it's like "a" in the English *cat*, *fat*, *hat* words
- **å** [ o ] - sounds as a regular "o" - can be found only in Swedish-origin loan words

### 2.2 The Finnish Keyboard Layout

![alt_text](keyboard_fi.png)

### 2.3 Everyday Expressions, Idioms and Phrases

This list is far from being complete but it introduces the very basics. 

<img src="/_ui/img/imgfx/conversation.png" title="" align="right" class="right">

General greetings: 
- **Moi!** / **Hei!** - Hi!
- **Moikka!** - Hi! / Bye!
- **Terve!** - Hello!
- **Moro!** -  Hello! *(originated from Tampere region)*
- **Hei hei!** / **Moi moi!** - Bye! *(so it's usually for saying a short goodbye)*
- **Tervetuloa!** - Welcome!

A bit more formal greetings: 
- **Hyvää huomenta!** / **Huomenta!** - Good morning!
- **Hyvää päivää!** - Good day!
- **Hyvää iltaa!** - Good evening!
- **Hyvää yötä!** - Good night!
- **Nähdään!** - See you!
- **Nähdään pian! Hei!** - See you soon! Bye!
- **Nähdään myöhemmin!** - See you later!
- **Hyvää päivänjatkoa!** - Have a nice day!

Asking and thanking for things: 
- **Kiitos!** - Thank you!
- **..., kiitos.** - ..., please.
- **Kiitti!** - Thanks!
- **Ole hyvä!** - Here you are! / You're welcome!
- **Ei kestä!** - Don't mention it! / Not at all!

Other greetings, expressions, wishes: 
- **Tsemppiä!** - Good luck!
- **Hyvää joulua!** - Merry christmas!
- **Hyvää uutta vuotta!** - Happy new year!
- **Kiitos samoin!** - Thanks and same to you!
- **Ystävällisin terveisin** - Kind regards *(as a closing in a letter; literally means "Friendliest regards")*
- **Mitä kuuluu?** - How are you?
- **Ihan hyvää, kai.** - Quite well, I guess.
- **Ihan hyvää, kittos. Entä sinulle?** - Quite well, thank you. And you?
- **No, ihan hyvää, kiitos.** - Well, quite well, thank you.
- **No, ei mitään.** - Well, nothing special.
- **Oletko kunnossa?** - Are you all right?
- **Kaikki on ihan hyvin.** - Everything is all right.
- **No, ei se mitään.** - Well, it happens.
- **Olen pahoillani.** - My apologies (I'm sorry.)
- **Olen todella pahoillani.** - My deepest apologies.
- **Olemme pahoillamme.** - Our apologies.

Basic introduction: 
- **Minä olen Elsa.** - I am Elsa.
- **Minun nimi on Elsa.** - My name is Elsa.
- **Sinä olet Joni.** - You are Joni.
- **Anteeksi, kuka sina olet?** - Excuse me, who are you?
- **Anteeksi, oletko sinä Pöllönen?** - Excuse me, are you Pöllönen?


<a name="3"></a>
## 3. Vowel Harmony, K-P-T-Variations - Vokaaliharmonia, K-P-T-Vaihtelu

### 3.1 Vowel Harmony

In Finnish, there're three classes of vowels: 

- front vowels: ä, ö, y
- neutral vowels: e, i
- back vowels: a, o, u

In case of single, so non-compound words, if a word contains front vowels, it cannot contain back vowels and vice versa but neutral vowels can be both of them. 

There is a very simple rule in Finnish for figuring out what vowel we should use when conjugating words. 

- all words that include **front vowels only**, will get the front vowel in their word endings too in their other forms, e.g.:
    - **hyvä** --> hyvä<r>ssä</r>
    - **säästää** --> säästä<r>vät</r>
- the **neutral-only** words also get the rule with front vowels applied, e.g.:
    - **kieli** --> kieli<r>kö</r>
- so the **front/netutral-mixed** words obviously get the front vowel in conjugation, e.g.:
    - **päivä** --> päivä<r>ssä</r>
- all words that include **back vowels only**, obviously get the back vowel in their word endings too when conjugated, e.g.:
    - **auto** --> auto<r>ssa</r>
    - **tutustua** --> tutustu<r>vat</r>
- no matter the word **includes neutral vowel or not**: if a back vowel "ruins" the word, it gets the back vowel in conjugation too, e.g.:
    - **mestari** --> mestari<r>lla</r>
    - **kiva** --> kiva<r>ssa</r>
- any **compound words** (yhdyssanat) will get the conjugation of the **final word** it contains, e.g.:
    - **suklaajäätelö** --> suklaajäätelö<r>ssä</r>
    - **jäätelökioski** --> jäätelökioski<r>lla</r>

### 3.2 K-P-T-Variations

Finnish almost always use the softened version of the dictionary form of the word when conjugated.

Going through on the list of these rules with examples:

<table class="tan">
  <tr>
    <td colspan="2"><span style="font-size: 20px">vahva</span> (hard)</td>
    <td colspan="2"><span style="font-size: 20px">heikko</span> (soft)</td>
  </tr>
  <tr>
    <td>Afri<r>kk</r>a, Tur<r>kk</r>i, vii<r>kk</r>o</th>
    <th>kk</th>
    <th>k</th>
    <td>Afri<r>k</r>asta, Tur<r>k</r>issa, vii<r>k</r>olla</th>
  </tr>
  <tr>
    <td>Euroo<r>pp</r>a, La<r>pp</r>i, kaa<r>pp</r>i</th>
    <th>pp</th>
    <th>p</th>
    <td>Afrikasta, la<r>p</r>issa, kaa<r>p</r>in</th>
  </tr>
  <tr>
    <td>konser<r>tt</r>i, ty<r>tt</r>ö, planee<r>tt</r>a</th>
    <th>tt</th>
    <th>t</th>
    <td>konser<r>t</r>in, ty<r>t</r>ön, planee<r>t</r>alla</th>
  </tr>
  <tr>
    <td>Tur<r>k</r>u, ruo<r>k</r>a, sel<r>k</r>ä</th>
    <th>k</th>
    <th>-</th>
    <td>Turussa, ruoasta, selässä</th>
  </tr>
  <tr>
    <td>lei<r>p</r>ä, kyl<r>p</r>y, a<r>p</r>u</th>
    <th>p</th>
    <th>v</th>
    <td>lei<r>v</r>ällä, kyl<r>v</r>yssä, a<r>v</r>ulla</th>
  </tr>
  <tr>
    <td>ka<r>t</r>u, mai<r>t</r>o, pöy<r>t</r>ä</th>
    <th>t</th>
    <th>d</th>
    <td>ka<r>d</r>ulla, mai<r>d</r>ossa, pöy<r>d</r>ältä</th>
  </tr>
  <tr>
    <td>Helsi<r>nk</r>i, kaupu<r>nk</r>i, auri<r>nk</r>o</th>
    <th>nk</th>
    <th>ng</th>
    <td>Helsi<r>ng</r>issä, kaupu<r>ng</r>issa, auri<r>ng</r>on</th>
  </tr>
  <tr>
    <td>Holla<r>nt</r>i, Isla<r>nt</r>i, ra<r>nt</r>a</th>
    <th>nt</th>
    <th>nn</th>
    <td>Holla<r>nn</r>in, Isla<r>nn</r>ista, ra<r>nn</r>alle</th>
  </tr>
  <tr>
    <td>ru<r>mp</r>u, ka<r>mp</r>a, la<r>mp</r>pu*</th>
    <th>mp</th>
    <th>mm</th>
    <td>ru<r>mm</r>un, ka<r>mm</r>assa, la<r>m</r>pussa*</th>
  </tr>
  <tr>
    <td>i<r>lt</r>a, Itäva<r>lt</r>a, ku<r>lt</r>a</th>
    <th>lt</th>
    <th>ll</th>
    <td>illa<r>ll</r>a, Itäva<r>ll</r>assa, ku<r>ll</r>an</th>
  </tr>
  <tr>
    <td>pa<r>rt</r>a, yläke<r>rt</r>a, vi<r>rt</r>a</th>
    <th>rt</th>
    <th>rr</th>
    <td>pa<r>rr</r>an, yläke<r>rr</r>asa, vi<r>rr</r>an</th>
  </tr>
</table>

<div class="warn">
<p><b>There's no k-p-t variation in place if:</b></p>
<ul>
  <li>hk, sk, st, tk: posti --> postissa, matka --> matkalla, keittiö --> keittiössä</li>
  <li>closing with 2 vowels (2 vokaalia lopussa): vapaa --> vapaassa</li>
</ul>
</div>

<a name="4"></a>
## 4. Verbtypes and Wordtypes - Verbityypit ja Sanatyypit

<div class="info">
<p>For a list of most common verbs, see the subpage <b><a href="verbs">Finnish verbs, tyes, conjugations</a></b>.</p>
</div>

Finnish uses a complex, but mostly straightforward conjugation logic for almost all types of words. Understanding the **logic and steps for conjugating verbs** is the most important phase in the beginning, so let's start with that. It also heavily involves the above mentioned **k-p-t variations**. 

E.g. to conjugate any verbs for simple sentences that use present tense (**Preesens**), you need to go through on **5 steps**:

![alt_text](finnish_-_verb_conjugation_steps.svg)

As an example, to construct the sentence *"I watch."*, here are the steps:

Key notes to note:

1. **Hardening vs. Softening**:
    - Softening (*kpt-vaihtelu*) happens in Verb Types 1, 3, 4, 5, and 6, but not in Type 2.
    - Hardening is rare and happens mainly in Verb Type 3 (e.g., **pestä** → *pesen*, **tavata** → *tapaan*).
    - **Not all verbs undergo consonant gradation—you need to recognize which ones do.**
2. **Finnish verbs have two main stems**:
    - **Strong** stem (used in some conjugations like "minä" and "sinä" forms).
    - **Weak** stem (used in "hän" and "he" forms, often softened).

### 4.1 Verbtypes

As mentioned before already, we always work with the **infinitiivi** form of the verbs (the 1st part in the below examples) to start figuring out how to conjugate them, and examine how they end, then how to cut or construct the **stem** of them (the 2nd part in the examples). 

There are **6 types of verbs**:

<div class="dtable">
  <div class="dsub"><img src="finnish_-_verbtype_-_1.svg" title="" align="left" /></div>
  <div class="dsub"><img src="finnish_-_verbtype_-_2.svg" title="" align="left" /></div>
  <div class="dsub"><img src="finnish_-_verbtype_-_3.svg" title="" align="left" /></div>
  <div class="dsub"><img src="finnish_-_verbtype_-_4.svg" title="" align="left" /></div>
  <div class="dsub"><img src="finnish_-_verbtype_-_5.svg" title="" align="left" /></div>
  <div class="dsub"><img src="finnish_-_verbtype_-_6.svg" title="" align="left" /></div>
</div>

Test set of **12 verbs across all verb types**:

<table class="tan">
  <tr>
    <th>Infinitiivi (dictionary form)</th>
    <th>Verb Type</th>
    <th>Strong Stem (1st/2nd person)</th>
    <th>Weak Stem (3rd person, etc.)</th>
    <th>Example Forms</th>
    <th width="130 px">KPT-vaihtelu?</th>
  </tr>
  <tr>
    <td><b>puhua</b> (<i>to speak</i>)</td>
    <td><p><code>1</code></p></td>
    <td>puhu-</td>
    <td>puhu-</td>
    <td>puhun, puhut, puhuu</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>tappaa</b> (<i>to kill</i>)</td>
    <td><p><code>1</code></p></td>
    <td>tapa-</td>
    <td>tappaa-</td>
    <td>tapan, tapat, tappaa</td>
    <td>✅ Softens (pp → p)</td>
  </tr>
  <tr>
    <td><b>juoda</b> (<i>to drink</i>)</td>
    <td><p><code>2</code></p></td>
    <td>juo-</td>
    <td>juo-</td>
    <td>juon, juot, juo</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>syödä</b> (<i>to eat</i>)</td>
    <td><p><code>2</code></p></td>
    <td>syö-</td>
    <td>syö-</td>
    <td>syön, syöt, syö</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>nousta</b> (<i>to rise, get up</i>)</td>
    <td><p><code>3</code></p></td>
    <td>nouse-</td>
    <td>nouse-</td>
    <td>nousen, nouset, nousee</td>
    <td>✅ Hardens (st → s)</td>
  </tr>
  <tr>
    <td><b>pestä</b> (<i>to wash</i>)</td>
    <td><p><code>3</code></p></td>
    <td>pese-</td>
    <td>pese-</td>
    <td>pesen, peset, pesee</td>
    <td>✅ Hardens (st → s)</td>
  </tr>
  <tr>
    <td><b>haluta</b> (<i>to want</i>)</td>
    <td><p><code>4</code></p></td>
    <td>halua-</td>
    <td>halua-</td>
    <td>haluan, haluat, haluaa</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>tavata</b> (<i>to meet</i>)</td>
    <td><p><code>4</code></p></td>
    <td>tapaa-</td>
    <td>tapaa-</td>
    <td>tapaan, tapaat, tapaa</td>
    <td>✅ Hardens (v → p)</td>
  </tr>
  <tr>
    <td><b>tarvita</b> (<i>to need</i>)</td>
    <td><p><code>5</code></p></td>
    <td>tarvitse-</td>
    <td>tarvitse-</td>
    <td>tarvitsen, tarvitset, tarvitsee</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>häiritä</b> (<i>to disturb</i>)</td>
    <td><p><code>5</code></p></td>
    <td>häiritse-</td>
    <td>häiritse-</td>
    <td>häiritsen, häiritset, häiritsee</td>
    <td>❌ No kpt-vaihtelu</td>
  </tr>
  <tr>
    <td><b>rohjeta</b> (<i>to dare</i>)</td>
    <td><p><code>6</code></p></td>
    <td>rohkene-</td>
    <td>rohkene-</td>
    <td>rohkenen, rohkenet, rohkenee</td>
    <td>✅ Hardens (j → k)</td>
  </tr>
  <tr>
    <td><b>vanheta</b> (<i>to age</i>)</td>
    <td><p><code>6</code></p></td>
    <td>vanhene-</td>
    <td>vanhene-</td>
    <td>vanhenen, vanhenet, vanhenee</td>
    <td>✅ Hardens (t → n)</td>
  </tr>
</table>

### 4.2 Wordtypes

...


<a name="5"></a>
## 5. Forming Basic Sentences - Perus Lauseiden Muodostaminen

...

Soitatko mitään soitinta?

Voinko harjoitella kanssasi?

Mahtavat juhlat, eikö?

...


<a name="6"></a>
## 6. Directions and Locations - Reittiohjeet ja Paikat

...

### 6.1 Paikansijat

These are the fundamental words to describe directions when the sentence concerns "where", "where to", "from", "in", "on" etc.

![alt_text](paikansijat.png)

...

### 6.2 Describing Locations

- **päällä** - on top of smth
- **alla** - under smth
- **vasemmalla puolella** - on the left side of smth
- **oikealla puolella** - on the right side of smth
- **edessä** - in front of smth
- **takana** - behind smth
- **keskellä** - in the middle of smth
- **välissä** - in between of smth and smth
- **vieressä** - next to smth
- **ääressä** - on the edge of smth

**Malli: Pankki on koulun vieressä.** - Model: The bank is next to the school.

Examples: 
- posti → postin ...
- museo → museon ...
- pankki → pankin ...
- kirjasto → kirjaston ... - library
- hotelli → hotellin ... ← this remains with double "l"!!!
- koulu → koulun ...
- asema → aseman ... - station
- kauppa → kaupan ... - shop
- koti → kotin ...

**Missä koulu on?** → **Koulu on kartan keskellä.** (or maybe it can be also *"Koulu on keskellä kartalla."* based on e.g. *"Kirjasto on keskellä Helsingissä."*)

- ... on ...-in/-en/-än/-an/-on/-un **takana**. (... is behind ...)
- ... on ...-in/-en/-än/-an/-on/-un **edessä**. (... is in front of ...)
- ... on ...-in/-en/-än/-an/-on/-un **vasemmalla puolella**. (... is on the left side of ...)
- ... on ...-in/-en/-än/-an/-on/-un **oikalla puolella**. (... is on the rigth side of ...)


<a name="7"></a>
## 7. Pronouns - Pronominit

### 7.1 Personal Pronouns - Persoonaprononimit

The rough summary on all the cases:

<table class="tan">
  <tr>
    <th></th>
    <th>I</th>
    <th>you</th>
    <th>he/she/it</th>
    <th>we</th>
    <th>you (pr.)</th>
    <th>they</th>
  </tr>
  <tr>
    <th>nominatiivi</th>
    <td>minä</td>
    <td>sinä</td>
    <td>hän</td>
    <td>me</td>
    <td>te</td>
    <td>he</td>
  </tr>
  <tr>
    <th>genetiivi</th>
    <td>minun</td>
    <td>sinun</td>
    <td>hänen</td>
    <td>meidän</td>
    <td>teidän</td>
    <td>heidän</td>
  </tr>
  <tr>
    <th>akkusatiivi</th>
    <td>minut</td>
    <td>sinut</td>
    <td>hänet</td>
    <td>meidät</td>
    <td>teidät</td>
    <td>heidät</td>
  </tr>
  <tr>
    <th>partitiivi</th>
    <td>minua</td>
    <td>sinua</td>
    <td>häntä</td>
    <td>meitä</td>
    <td>teitä</td>
    <td>hetiä</td>
  </tr>
  <tr>
    <th>illatiivi</th>
    <td>minuun</td>
    <td>sinuun</td>
    <td>häneen</td>
    <td>meihin</td>
    <td>teihin</td>
    <td>heihin</td>
  </tr>
  <tr>
    <th>inessiivi</th>
    <td>minussa</td>
    <td>sinussa</td>
    <td>hänessä</td>
    <td>meissä</td>
    <td>teissä</td>
    <td>heissä</td>
  </tr>
  <tr>
    <th>elatiivi</th>
    <td>minusta</td>
    <td>sinusta</td>
    <td>hänestä</td>
    <td>meistä</td>
    <td>teistä</td>
    <td>heistä</td>
  </tr>
  <tr>
    <th>allatiivi</th>
    <td>minulle</td>
    <td>sinulle</td>
    <td>hänelle</td>
    <td>meille</td>
    <td>teille</td>
    <td>heille</td>
  </tr>
  <tr>
    <th>adessiivi</th>
    <td>minulla</td>
    <td>sinulla</td>
    <td>hänellä</td>
    <td>meillä</td>
    <td>teillä</td>
    <td>heillä</td>
  </tr>
  <tr>
    <th>ablatiivi</th>
    <td>minulta</td>
    <td>sinulta</td>
    <td>häneltä</td>
    <td>meiltä</td>
    <td>teiltä</td>
    <td>heiltä</td>
  </tr>
  <tr>
    <th>translatiivi</th>
    <td>minuksi</td>
    <td>sinuksi</td>
    <td>häneksi</td>
    <td>meiksi</td>
    <td>teiksi</td>
    <td>heiksi</td>
  </tr>
  <tr>
    <th>essiivi</th>
    <td>minuna</td>
    <td>sinuna</td>
    <td>hänenä</td>
    <td>meinä</td>
    <td>teinä</td>
    <td>heinä</td>
  </tr>
</table>

The *abessiivi*, *instruktiivi* and *komitatiivi* aren't listed here due to their usage only with conjugating nouns directly. Without them, there are the above **12 cases** of personal pronouns. 

Let's see a bit more details about the most often used ones.

#### 7.1.1 Nominatiivi

**Kuka? / Mitä?** - Who? / What? (*Ki? / Mi?*)

<table class="tan">
  <tr>
    <th></th>
    <th>Kirjakieli</th>
    <th>Puhekieli</th>
  </tr>
  <tr>
    <th rowspan=3>yksikkö</th>
    <td><b>minä</b> olen</td>
    <td><b>mä</b> oon</td>
  </tr>
  <tr>
    <td><b>sinä</b> olet</td>
    <td><b>sä</b> oot</td>
  </tr>
  <tr>
    <td><b>hän / se</b> on</td>
    <td><b>se</b> on</td>
  </tr>
  <tr>
    <th rowspan=3>monikko</th>
    <td><b>me</b> olemme</td>
    <td><b>me</b> ollaan</td>
  </tr>
  <tr>
    <td><b>te</b> olette</td>
    <td><b>te</b> ootte</td>
  </tr>
  <tr>
    <td><b>he / ne</b> ovat</td>
    <td><b>ne</b> on</td>
  </tr>
</table>

#### 7.1.2 Genetiivi

**Känen? / Minkä?** - Whom? / Which? (*Kié? / Kinek a...? / Mié? / Minek a...?*)

<table class="tan">
  <tr>
    <th></th>
    <th>Kirjakieli</th>
    <th>Kirjakieli with conjugated noun</th>
    <th>Puhekieli</th>
  </tr>
  <tr>
    <th rowspan=3>yksikkö</th>
    <td><b>minun</b> ystävä</td>
    <td>(minun) <b>ystäväni</b></td>
    <td><b>mun</b> ystävä</td>
  </tr>
  <tr>
    <td><b>sinun</b> kirja</td>
    <td>(sinun) <b>kirjasi</b></td>
    <td><b>sun</b> kirja</td>
  </tr>
  <tr>
    <td><b>hänen</b> auto</td>
    <td><b>hänen autonsa</b></td>
    <td><b>sen</b> auto</td>
  </tr>
  <tr>
    <th rowspan=3>monikko</th>
    <td><b>meidän</b> opettaja</td>
    <td>(meidän) <b>opettajamme</b></td>
    <td><b>meiän</b> opettaja</td>
  </tr>
  <tr>
    <td><b>teidän</b> kotimaa</td>
    <td>(teidän) <b>kotimaanne</b></td>
    <td><b>teiän</b> korimaa</td>
  </tr>
  <tr>
    <td><b>heidän</b> kurssi</td>
    <td><b>heidän kurssinsa</b></td>
    <td><b>niiden</b> kurssi</td>
  </tr>
</table>

#### 7.1.3 Partitiivi

**Ketä? / Mitä?** - Who? / What? (*Kit?, Mit?*)

This form has multiple meanings and usage, but we summarize the personal noun conjugations only here.

<table class="tan">
  <tr>
    <th></th>
    <th>Kirjakieli</th>
    <th>Puhekieli</th>
  </tr>
  <tr>
    <th rowspan=3>yksikkö</th>
    <td><b>minua</b></td>
    <td><b>mua</b></td>
  </tr>
  <tr>
    <td><b>sinua</b></td>
    <td><b>sua</b></td>
  </tr>
  <tr>
    <td><b>häntä / sitä</b></td>
    <td><b>sitä</b></td>
  </tr>
  <tr>
    <th rowspan=3>monikko</th>
    <td><b>meitä</b></td>
    <td><b>meitä</b></td>
  </tr>
  <tr>
    <td><b>teitä</b></td>
    <td><b>teitä</b></td>
  </tr>
  <tr>
    <td><b>heitä / niitä</b></td>
    <td><b>niitä</b></td>
  </tr>
</table>

E.g.:
- Minä rakastan **sinua**.
- Voitko auttaa **meitä**?
- Missä Kim ja Julian ovat? Minä etsin **heitä**.

#### 7.1.4 Allatiivi, Adessiivi ja Ablatiivi

- **Kenelle? / Mille?** - To whom? / To what? (*Kinek, kifelé? / Minek, mifelé?*)
- **Kenellä? / Millä?** - By whom? / By what? (*Ki által? / Mi által?*)
- **Keneltä? / Miltä?** - From whom? / From what? (*Kitől? / Mitől?*)

<table class="tan">
  <tr>
    <th></th>
    <th>Kirjakieli</th>
    <th>Puhekieli</th>
  </tr>
  <tr>
    <th rowspan=3>yksikkö</th>
    <td><b>minulle, minulla, minulta</b></td>
    <td><b>xxx</b></td>
  </tr>
  <tr>
    <td><b>sinulle, sinulla, sinulta</b></td>
    <td><b>xxx</b></td>
  </tr>
  <tr>
    <td><b>hänelle / sille, hänellä / sillä, häneltä / siltä</b></td>
    <td><b>xxx</b></td>
  </tr>
  <tr>
    <th rowspan=3>monikko</th>
    <td><b>meille, meillä, meiltä</b></td>
    <td><b>xxx</b></td>
  </tr>
  <tr>
    <td><b>teille, teillä, teiltä</b></td>
    <td><b>xxx</b></td>
  </tr>
  <tr>
    <td><b>heille / niille, heiltä / niitä, heillä / niitä</b></td>
    <td><b>xxx</b></td>
  </tr>
</table>

E.g.: 
- Kenelle sinä annat pallon? &rarr; Minä allan pallon Janille.
- Kenellä pallo on? &rarr; Pallo on Janilla.
- Keneltä sinä otit pallon? &rarr; Minä otin pallon Janilta.


<a name="8"></a>
## 8. Partitiivi

The **partitive case** in Finnish has several key purposes. Here’s a breakdown of its main uses with example sentences in both singular and plural:

### 8.1 Indicating incomplete or ongoing actions (unquantified objects)

- **Singular:** Syön omenaa. *(I am eating an apple.)*
- **Plural:** Syön omenoita. *(I am eating apples.)*

### 8.2 Expressing partial amounts or indeterminate quantities

- **Singular:** Haluan vettä. *(I want some water.)*
- **Plural:** Haluan hedelmiä. *(I want some fruits.)*

### 8.3 Describing negative sentences

- **Singular:** En näe koiraa. *(I don’t see a dog.)*
- **Plural:** En näe koiria. *(I don’t see dogs.)*

### 8.4 Indicating indefinite qualities or types

- **Singular:** Hän on opettaja. *(He/She is a teacher.)*
- **Plural:** Me olemme opiskelijoita. *(We are students.)*

### 8.5 Expressing desires, emotions, or needs

- **Singular:** Rakastan musiikkia. *(I love music.)*
- **Plural:** Rakastan kukkia. *(I love flowers.)*

### 8.6 Describing quantities with certain verbs

- **Singular:** Minulla on rahaa. *(I have money.)*
- **Plural:** Meillä on ongelmia. *(We have problems.)*

### 8.7 Specifying measurements or amounts

- **Singular:** Tarvitsen kiloa sokeria. *(I need a kilo of sugar.)*
- **Plural:** Tarvitsen kaksi litraa maitoja. *(I need two liters of milk.)*

Each of these examples demonstrates a common use of the partitive case in Finnish, showing both singular and plural forms in context.

### 8.8 Suggesting the future

Although Finnish doesn’t have a specific grammatical case or tense for the future, context, **partitive case** usage, and sometimes word choice help indicate future intentions.

In Finnish, **partitiivi** is often used to imply **intended or expected actions**, which can suggest a future event. This contrasts with **nominatiivi** or **akkusatiivi**, which would more likely indicate an ongoing or completed action, typically in the present.

When a verb takes a **partitiivi** object, it sometimes implies that the action is not happening right now but is expected to happen in the future. This subtle difference relies on context and can make it clear to listeners that the speaker is discussing a future action rather than a current one.

#### 8.8.1 Examples

1. **Reading a book (present vs. future)**
    - **Present:** Luen kirjan. *(I am reading the book — implying I will finish it now or am focused on it)*
    - **Future:** Luen kirjaa. *(I will read a book — suggesting a plan to start reading it later)*

2. **Buying a car (present vs. future)**
    - **Present:** Ostamme auton. *(We are buying the car — indicates a specific, ongoing action)*
    - **Future:** Ostamme autoa. *(We will buy a car — implies a future plan to buy one)*

3. **Building a house (present vs. future)**
    - **Present:** Rakennan talon. *(I am building the house — it’s happening now, and the action is more definite)*
    - **Future:** Rakennan taloa. *(I will build a house — implies it’s in the future or planning phase)*

4. **Learning Finnish (present vs. future)**
    - **Present:** Opiskelen suomen kielen. *(I am studying the Finnish language — the study is active and focused)*
    - **Future:** Opiskelen suomen kieltä. *(I will study Finnish — I intend to study it in the future)*

Using **partitiivi** in these cases softens the action, making it seem less immediate or definitive, which often gives the impression that the action is planned or anticipated rather than current. This is particularly useful for Finnish speakers to imply future actions without needing a future tense.


<a name="9"></a>
## 9. Akkusatiivi

The **akkusatiivi** (accusative) case in Finnish is used primarily to indicate the **direct object** of a completed or intended action. While **partitiivi** marks incomplete actions or partial objects, **akkusatiivi** indicates a **fully completed action** or a **specific, entire object**.

This distinction often causes confusion because both cases can apply to direct objects, but they differ in how they relate to the action. If an action is ongoing, habitual, or refers to a part of something, **partitiivi** is used. If the action is complete or aims to affect the entire object, **akkusatiivi** is used.

### 9.1 How Akkusatiivi Differs from Partitiivi

- **Akkusatiivi**: Marks a complete action on the whole object.
- **Partitiivi**: Marks an incomplete action or a part of the object.

For example:
- **Partitiivi**: "Luen kirjaa." *(I am reading a book — ongoing or partial action)*
- **Akkusatiivi**: "Luin kirjan." *(I read the book — completed action on the whole book)*

### 9.2 Examples of Akkusatiivi

**Singular**:

- Näen koiran. *(I see the dog — the entire object is seen)*
- Syön omenan. *(I eat the apple — the entire apple is eaten)*

**Plural**:
- Ostin kukat. *(I bought the flowers — the entire bouquet is bought)*
- Tapasin ystävät. *(I met the friends — meeting all the friends)*

Understanding whether an action is completed (akkusatiivi) or ongoing/partial (partitiivi) helps clarify the correct case usage in Finnish sentences.

Pages on this topic that can be useful:
- [Finking Cap - Akkusatiivi vai partitiivi?](https://finkingcap.com/blog/finnish-accusative-partitive/) (in English)
- [Charmedand Dangerous - Névszók](http://www.charmedanddangerous.gportal.hu/gindex.php?pg=24319124) (in Hungarian)
- [Uusikielemme - The Partitive Case](https://uusikielemme.fi/finnish-grammar/finnish-cases/grammatical-cases/the-partitive-case-partitiivi#usage) (in English)


<a name="10"></a>
## 10. Past Tense - Imperfekti

...


<a name="10"></a>
## 11. Conditional - Konditionaali

...

Probably the first verb that you construct and use is the conditional form of "I want" to get the "I would like" in a polite form: 

**haluta** (to want) &rarr; **minä haluan** (I want) &rarr; **minä haluaisin** (I would like)

Based on the same logic, another good and quite frequent example for constructing the conditional form of a werb is following along the procedure with **saada** (to get): 

- **Preesens**: saan, saat, saa, saamme, saatte, saavat (*I/you/she/we/you/they get/gets.*)
- **Preesens**, negatiivinen: ... saa (*I/you/she/we/you/they don't/doesn't get.*)
- **Imperfekti**: sain, sait, sai, saimme, saitte, saivat (*I/you/she/we/you/they got.*)
- **Konditionaali**: saisin, saisit, saisi, saisimme, saisitte, saisivat (*I/you/she/we/you/they could get.*)
- **Konditionaali in question**: saisinko, saisitko, saisiko, saisimmeko, saisitteko, saisivatko (*Could I/you/she/we/you/they get?*)

Examples for conditional in declarative sentence: 

- **Haluaisin kupin kahvia.** - I would like a cup of coffee. 
- **Kävelisin, jos voisin.** - I would walk if I could.
- **Lukisin lehteä, jos löytäisin silmälasit.** - I would read the magazine if I could find the glasses.
- **Etsisin avaimiani, jos minulla olisi aikaa.** - I would look for my keys if I had time.

Examples for conditional in question: 

- **Haluaisitko kupin kahvia?** - Would you like a cup of coffee?
- **Saisimmeko kalaa ja riisiä?** - Could we get fish and rice?
- **Saisimmeko lisää viiniä?** - Could we get/have more wine?
- **Tulisitko tänne?** - Would you come here?
...

