# 📐 Circuits, Wiring Diagrams

![alt_text](electronics_-_circuits_-_bg.jpg)

- 🎄 [Christmas Lights](/electronics/circuits/christmas)
<!-- - ⚡ [Power Supplies, Voltage Regulators](/electronics/circuits/power) -->
- 🧮 [Prototype Logic Gates](/electronics/circuits/gates)
