# ![alt text](/_ui/img/flags/eng_m.svg) Angol nyelv / English language

###### .

<div class="info">
<p>Ez a bejegyzés csak különféle példamondatokat sorol fel kategóriákra bontva.</p>
</div>

<img src="english.jpg" title="" align="right" class="right">

## Tartalomjegyzék

1. [Indirekt kérdések](#1)
2. [Question tag-ek](#2)
3. [A múltban kezdődő, jelenleg is folyamatban lévő vagy a jelenre is jellemző cselekmények](#3)
4. [A múltban kezdődő és a múltban le is záródó cselekmények](#4)
5. [A "bárcsak" és a különféle feltételes módok](#5)
6. [Befejezett cselekmény a jövőben](#6)
7. [Mindenféle egyéb példamondat különféle témakörökből](#7)


<a name="1"></a>
## 1. Indirekt kérdések

**When is Mr Patel leaving? &rarr; Do you know when Mr Patel is leaving?**

**Where does Mr Elmore work? &rarr; Could you tell me where Mr Elmore works?**

**How much does it cost? &rarr; Can you tell me how much it costs?**

**Who left this message? &rarr; Do you have any idea who left this message?**

**Is it going to rain tomorrow? &rarr; Do you know whether it's going to rain tomorrow?**

**Why did she cry? &rarr; Can you tell me why she cried?**

**What time does the show start? &rarr; Do you have any idea what time the show starts?**

**How long have you been waiting? &rarr; Can you tell me how long you have been waiting?**

**Why is it that pigs don’t fly? &rarr; Explain to me why it is that pigs don’t fly.**


<a name="2"></a>
## 2. Question tag-ek

**It's hot, isn't it?**  
Ez forró, ugye?

**You haven't seen my keys, have you?**  
Ugye nem láttad a kulcsaimat?

**She won't go to China, will she?**  
Nem fog kínába menni, ugye?

**You can swim, can't you?**  
Ugye tudsz úszni?

**You work for Siemens, don't you?**  
A Siemens-nek dolgozol, ugye?

**He left early yesterday, didn't he?**  
Tegnap korán távozott, ugye?

**Anyone can use the meeting room, can't they?**  
Ugye mindenki használhatja a meeting szobát?

**Nobody knew about that, did they?**  
Ugye senki sem tudott arról?

**I'm not interrupting anything, am I?**  
Ugye nem szakítok meg semmit?

**I'm quite stupid, aren't I?**  
Elég hülye vagyok, mi?

**I suppose I should go one day, shouldn't I?**  
Feltételezem, hogy egyszer el kell mennem, ugye?

**The lunch will be meat soup. Everybody likes it, don’t they?**  
Húsleves lesz ebédre. Ugye mindenki szereti?


<a name="3"></a>
## 3. A múltban kezdődő, jelenleg is folyamatban lévő vagy a jelenre is jellemző cselekmények

Kifejezetten a *Present Perfect Continuous* igeidő tartozik ide. 

**I’ve been thinking about what you said since yesterday.**  
Tegnap óta azon gondolkodom, amit mondtál.

**I have been participating in every English classes but the last one.**  
Minden angol órán részt vettem, csak az utolsón nem.

**\- Have you finished with the translation yet?**  
**\- Not yet, although I have been working on it for three-quarter of an hours.**  
\- Készen vagy a fordítással?  
\- Még nem, bár háromnegyed órája már dolgozom rajta.

**They have been living in the new house for three years but he still has not got the tap water connected.**  
Három éve már az új házban laknak, de még mindig nem vezettette be a vizet.

**No matter how long you have been learning English, you still have a lot to learn.** *(However long...)*  
Bármilyen régóta tanulsz angolul, még sokat kell tanulnod.

**I have been traveling by plain for several years but I doubt that I will ever get used to flying.**  
Már több éve repülök, de meg kell mondjam, nem hiszem, hogy valaha is megszokom a repülést. 

**He has been learning diligently for weeks, he will surely pass the exam perfectly.**  
Hetek óta szorgalmasan tanul, biztos, hogy kitűnően fog vizsgázni.

**She said she wouldn’t be able to speak English, but I know she will be able to do that, because she has been learning English for six years.**  
Azt mondta, hogy nem tud majd angolul beszélni, de én tudom, hogy ő tud majd, mert hat éve tanul már angolul.


<a name="4"></a>
## 4. A múltban kezdődő és a múltban le is záródó cselekmények

Ez pedig a *Past Perfect Continuous* igeidőről szól.

**I had been talking with him for a long time, then I noticed that he was not listening to me.**  
Sokáig beszéltem vele, amikor észrevettem, hogy nem figyel rám.

**He wrote that he had been ill for five days and wouldn’t be able to come to me.**  
Azt írta, hogy öt napja már beteg, és nem tud majd eljönni hozzám.

**For how long had she been watching the television when her parents arrived?**  
Mennyi ideje nézte már a televíziót, amikor a szülei megérkeztek?


<a name="5"></a>
## 5. A "bárcsak" és a különféle feltételes módok

**I could have been there on time if I had left home earlier.**  
Időben ott tudtam volna lenni, ha korábban eljöttem volna otthonról.

**He said he wished his son had been admitted to university.**  
Azt mondta, hogy bárcsak felvették volna a fiát az egyetemre. 

**I was really looking forward to this party. I wish it hadn’t been canceled!**  
Annyira vártam ezt a partyt. Bárcsak ne maradt volna el!

**If I had spoken with my father like you did with me, he would have beaten me.**  
Ha én úgy beszéltem volna az apámmal, mint te velem, megvert volna.

**If my money hadn’t been stolen, I would have bought two pair of pants to my son.**  
Ha nem lopták volna el a pénzem, vettem volna két nadrágot a fiamnak.

**If you had had the washing machine repaired, now I would not have to wash these shirts by hand.**  
Ha megcsináltattad volna a mosógépet, most nem kéne kézzel mosnom ezeket az ingeket.

**I haven't heard a word from him since he left. Really? At least he might have written a letter.**  
Egy szót sem hallottam felőle, amióta elment. Tényleg? Legalább írhatott volna egy lapot.

**I would have preferred if he had got an English book.**  
Jobban szerettem volna, ha egy angol könyvet adtak volna neki.

**You couldn’t have helped even if I had asked you to.**  
Nem tudtál volna segíteni még akkor sem, ha kértelek volna rá.

**I would have brought some beer if I had known you were thirsty.**  
Hoztam volna egy kis sört, ha tudtam volna, hogy szomjas vagy.

**You should have come to the party.**  
El kellett volna jönnöd a buliba.  
(A *"come"* már alapban is "múlt idejű melléknévi igenév" (Past Participle), emiatt nem *"came"*.)

**They should have had the electric wire examined in their house.**  
Át kellett volna vizsgáltatniuk a villamosvezetéket a házukban.

**It is very late. The children should have gone to bed two hours ago.**  
Nagyon későn van. A gyerekeknek már két órával ezelőtt le kellett volna feküdniük.

**He should have repaid the debt a long time ago, but he seems to have forgotten.**  
Már régen vissza kellett volna fizetnie az adósságot, de úgy látszik, elfelejtette.

**I’ve made two copies. I shouldn’t have taken two of them, one would have been enough.**  
Készítettem két másolatot. Nem kellett volna kettőt, egy is elég lett volna.

**If the process had existed since the beginning, we would already have rolled out these patches.**  
Ha a folyamat a kezdetől létezett volna, már kiterítettük volna ezeket a javításokat.


<a name="6"></a>
## 6. Befejezett cselekmény a jövőben

**By the end of this year, she will have been teaching at the university for twenty years.**  
Ez év végén 20 éve lesz, hogy tanít az egyetemen. 

**Before you come back, we will have the flat painted and everything will be all right.**  
Mielőtt visszajöttök, ki fogjuk festetni a lakást, és minden rendben lesz.

**I still have some minutes. I will wait until you have finished your work.**  
Van még néhány percem. Várni fogok, amíg be nem fejezed a munkád.

**I will have finished the work until my boss comes back.**  
Be fogom fejezni a munkát, mire a főnököm visszajön.


<a name="7"></a>
## 7. Mindenféle egyéb példamondat különféle témakörökből

**Despite the fact that the piece got good reviews, only few people went to the performance.**  
Annak ellenére, hogy a darab jó kritikát kapott, kevesen mentek el az előadásra.

**It is very nice of you and your wife to have met me at the airport.**  
Nagyon kedves öntől és a feleségétől, hogy kijöttek elém a repülőtérre. 

**The public transport becomes worse and worse. It takes me twice the time to get to my workplace than last year.**  
A közlekedés egyre rosszabb lesz. Kétszer annyi időbe telik, míg beérek a munkahelyre, mint tavaly. 

**Much to my surprise, the computer is much more valuable than I expected.**  
A számítógép meglepetésemre sokkal értékesebb, mint arra számítottam. 

**I am terribly sorry, but I seem to have lost the book that you lent to me.** *(that I borrowed from you)*  
Borzasztóan sajnálom, de úgy tűnik, hogy elvesztettem a könyvet, amit kölcsönadtál. 

**She always visits my relatives unexpectedly: she does it without asking if they have time.** 
Mindig váratlanul látogatja meg rokonaimat: anélkül, hogy telefonon megkérdezné, ráérnek-e. 

**I'm glad to have had the opportunity to talk all of you.**  
Örülök, hogy lehetőségem nyílt, hogy valamennyiükkel beszélhettem. 

**What about coming to us this evening for a dinner?**  
Mit szólna hozzá, ha ma este eljönne hozzánk vacsorára? 

**I have never seen such a good movie. You will surely like it.**  
Sohasem láttam ilyen jó filmet. Biztos tetszeni fog neked. 

**If you share your problems with me, I will see if I can help with any of them.**  
Ha elmondod a problémáidat, majd meglátom, hogy miben segíthetek. 

**She left from home five minutes ago, probably she will arrive soon.**  
Öt perccel ezelőtt indult otthonról, valószínű, hogy hamarosan jön. 

**You had better not to go by car. I think snow will fall.**  
Jobban teszed, ha nem gépkocsival mész. Azt hiszem, esni fog a hó.

**Once he decided something, it was almost impossible to dissuade him from his plan.**  
Ha ő egyszer elhatározott valamit, szinte lehetetlen volt lebeszélni a tervéről.

**He might have got into a traffic jam, supposedly this is why he did not arrive on time.**  
Lehet, hogy közlekedési dugóba került, valószínű, hogy ezért nem ért ide időben.

**Usually I wake up at six A.M., but today I woke up at eight.**  
Általában hatkor szoktam kelni, de ma nyolckor keltem.

**Our shopping center and the one in London share a lot of similarities.**  
Bevásárló központunk sokban hasonlít a londoniéhoz.

**It is not right how they treated with this student of mine.** *(It is not right that my student is being treated this way.)*  
Nem helyes, hogy így bánnak ezzel a tanulómmal. 

**I managed to get two more tickets to the concert on Sunday evening.** *(I succeeded in getting...)*  
Sikerült még két jegyet szereznem a vasárnap esti hangversenyre. 

**Would you take a look at this letter? It could be important but I do not understand it.**  
Megnéznéd ezt a levelet? Lehet, hogy fontos, de én nem értem.

**She told us yesterday afternoon that she couldn’t participate in today’s meeting.**  
Azt mondta nekünk tegnap délután, hogy nem tud majd részt venni a mai megbeszélésen.

**If he was in a serious condition, he would be taken to hospital.**  
Ha komoly baja lenne, bevinnék a kórházba.

**Are you still here? I thought you had left long ago.**  
Még mindig itt vagy? Azt hittem, hogy régen elmentél.

**The doctor insisted on Peter quiting smoking but he did not want to.**  
Az orvos ragaszkodott ahhoz, hogy Péter abbahagyja a dohányzást, de nem akarta.

**She told me that she could not visit me either on Wednesday or Friday.**  
Azt mondta, hogy nem tudott meglátogatni sem szerdán, sem pénteken.

**He spend every afternoons in the club instead of preparing for the exams.**  
Minden délutánt a klubban tölt ahelyett, hogy készülne a vizsgákra.

**Are people afraid of accidents? Some are, others are not.**  
Félnek az emberek a balesetektől? Egyesek igen, mások nem.

**What were you doing while your car was being fixed?**  
Mit csináltál mialatt javították a kocsidat?

**As soon as we put our suitcases out, we left the hotel in order to meet with our friends.**  
Mihelyt kiraktuk a bőröndjeinket, elmentünk a szállodából, hogy találkozzunk barátainkkal.

**We are going to the cinema today, you will sit next to me.**  
Ma moziba megyünk, mellettem fogsz ülni.

**He told me if he met with my uncle in London, he would tell him to send money to my travel to England.**  
Azt mondta, ha találkozni fog a nagybátyámmal Londonban, meg fogja mondani neki, hogy küldjön pénzt az angliai utamra.

**On Friday last week she arrived at about eight a.m..**  
A múlt héten pénteken reggel kb. Nyolc órakor érkezett.

**Yesterday I bought two books on art. Both of them will be very useful for my studies.**  
Tegnap vásároltam két művészeti könyvet. Mindkettő nagyon hasznos lesz a tanulmányaimhoz.

**Unfortunately I have little money, but I am glad to land you that little I have.**  
Sajnos kevés pénzem van, de szívesen kölcsönadom neked, ami kevés van (inkább azt a keveset, ami van). 

**When these flowers are planted, it is important to take care not to damage their roots.**  
Amikor ültetik ezeket a virágokat, ügyelni kell arra, hogy a gyökereiket meg ne sértsék.

**Having gone to the shop, a huge amount of customers were waiting in front of the entrance.**  
Amikor mentem az üzletbe, rengeteg vásárló várakozott a bejárat előtt.

**If the car drivers were not drunk, thousands of accidents could be prevented annually.**  
Ha a gépkocsivezetők nem lennének ittasak, a balesetek ezreit lehetne megelőzni évente.

**I could not speak with the professor even though I was waiting for him until noon.**  
Nem tudtam beszélni a professzorral, bár (inkább pedig) délig vártam rá.

**None of us was asked to attend the party.**  
Egyikünket sem kérték meg, hogy vegyünk részt a fogadáson.

**We were waiting until afternoon but the rain did not want to stop.**  
Vártunk délutánig, de az eső nem akart elállni.

**By the time the guests arrived, my daughter had cleaned the apartment.**  
Mire megérkeztek a vendégek, lányom kitakarította a lakást.

**Lehet, hogy a televíziót nézték, ezért nem hallották, hogy csöngetsz.**  
They might had been watching the television, that is why they did not hear you ringing.

**I was afraid that the shops would have closed by the time I got there.**  
Attól féltem, hogy bezárnak az üzletek, mire odaérek.

**She said I wasn’t allowed to tell anybody that she told me.**  
Azt mondta, hogy nem szabad elmondanom senkinek azt, amit mondott.

**My cousin lives in Canada now, but I think he will come home next year.**  
Unokatestvérem jelenleg Kanadában lakik, de azt hiszem, jövő évben hazajön.

**She said she would go to the lake unless it was cold.**  
Azt mondta, hogy le fog menni a tóra, hacsak nem lesz hideg.

**I don’t like these blouses. I won’t buy either this or that one.**  
Nem tetszenek ezek a blúzok. Nem fogom megvenni sem ezt sem a másikat.

**We don’t have to leave yet. There is still enough time until the bus starts.**  
Még nem kell menni. Van még elég idő a busz indulásáig.

**We were close enough to see everything.**  
Elég közel voltunk ahhoz, hogy mindent lássunk.

**The space ship will have orbitted the Earth for the tenth time by 5 p.m.**  
Az űrhajó már tízedszer fogja megkerülni a Földet délután 5 órára.

**My parents are poor, neither of them has a car.**  
Szüleim szegények, egyiknek sincs kocsija.

**My sister asked if she should send the letter by airmail.**  
A húgom megkérdezte, hogy légi postával küldje-e a levelet.

**Silver is more beautiful than gold but gold is much more valuable.**  
Az ezüst szebb, mint az arany, de az arany sokkal értékesebb.

**Have you read his latest article which was published in yesterday’s newspaper?**  
Olvastad a legújabb cikkét, ami a tegnapi újságban jelent meg?

**The gallery is currently presenting some of Picasso’s early paintings.**  
A képcsarnok jelenleg Picasso néhány korai festményét mutatja be.

**You have a car too. Why do you want me to take you home?**  
Neked is van kocsid. Miért akarod, hogy hazavigyelek?

**I’ve heard a lot about that town, however, I myself have never been there.**  
Sokat hallottam arról a városról, bár magam soha nem jártam ott.

**Last year on this date we were lying lazily on the sun in Italy.**  
A múlt évben ilyenkor Olaszországban heverésztünk lustán a napon.

**What a pleasure! Our daughter is coming home next week.**  
Milyen öröm! Jövő héten hazajön a lányunk.

**Yesterday I bought a pair of good quality salamander shoes.**  
Tegnap vettem egy pár jó minőségű salamander cipőt.

**Shortly after takeoff, the stewardesses served the passengers.**  
Nem sokkal a felszállás után a légikisasszonyok megvendégelték az utasokat.

**I like traveling by train the most. Me too, but I used to like traveling by car some time ago.**  
Vonattal szeretek leginkább utazni. Én is, de valamikor kocsival jártam a legszívesebben.

**Our youngest son will go to college as soon as he finishes school.**  
Legkisebb fiunk egyetemre fog menni, mihelyt elvégzi az iskolát.

**Have you taken your pill? Yes, I did it two minutes ago.**  
Bevetted a gyógyszert? Igen, két perccel ezelőtt.

**My friend would like to get married, he is tired of loneliness, but he can’t save enough money for an own house, he is waiting for a municipal apartment.**  
Barátom szeretne megnősülni, megunta már a magányt, de nem tud saját házra gyűjteni, önkormányzati lakásra vár.

**If I am offered this job, of course, I will accept it.**  
Ha fel fogják ajánlani nekem ezt az állást, természetesen elfogadom.

**How can you be so thoughtless? Have you forgotten what you were told?**  
Hogy lehetsz ilyen meggondolatlan? Elfelejtetted már, amit mondtak?

**I have told you many times not to be late, because I don’t like to substitute.**  
Többször mondtam már, hogy ne késs el, mert nem szeretem, ha helyettesítenem kell.

**There were so many nice clothes in the shop that I could hardly choose.**  
Annyi szép ruha volt az üzletben, hogy nehezen tudtam választani.

**We advised them to leave early so that they could reach the border before noon.**  
Azt tanácsoltuk nekik, induljanak korán, hogy még délelőtt elérjék a határt.

**Do you happen to know when and who the temple was built by?**  
Tudja véletlenül, hogy mikor épült a templom és ki építette?

**They were speaking French when I entered the room.**  
Franciául beszélgettek, amikor beléptem a terembe.

**I still cannot use my household appliances because the electricity hasn’t been installed yet.**  
Még mindig nem tudom használni a háztartási gépeimet, mert a villanyt nem vezették be.

**Why are you so sad? Neither me, nor Peter are angry with you.**  
Miért vagy ilyen szomorú? Sem én, sem Péter nem haragszunk rád.

**Since my house plan has not been approved, it cannot be rebuilt.**  
Mivel a házam tervét nem hagyták jóvá, nem épülhet fel.

**Who did he say had won the World Cup?** *("When do you think the grandmother will come?")*  
Mit mondott, ki nyerte a világbajnokságot?

**The kitchen of the old flat is dark but that of the new one is bright.** *(...higher than that of...)*  
A régi lakás konyhája sötét, de az újé világos.

**That handsome young man from London is my English friend’s oldest son.**  
Az a jóképű londoni fiatalember az angol barátom legidősebb fia.

**It doesn’t make much sense that you have a computer but you cannot handle it.**  
Nincs sok értelme, hogy van számítógéped, ha nem értesz hozzá.

**Traveling on tram is terrible nowadays. It is very crowded and I don’t like when I’m being pushed.**  
Szörnyű villamoson utazni manapság. Nagyon zsúfolt, és nem szeretem, ha lökdösnek.

**I never go to party with her, I don’t like the way how she laughs.**  
Soha nem megyek el vele szórakozni, nem szeretem, ahogy nevet.

**Peter told Mary that she was the best dancer he had ever danced with.**  
Péter azt mondta Máriának, hogy a legjobb táncos, akivel valaha is táncolt.

**I’d like to know what we’re going to do in a year if we see each other again.**  
Szeretném tudni, hogy mit fogunk csinálni egy év múlva, ha újra látjuk egymást.

**A lot has been done to improve the housing situation but there is still a lot to do.**  
Sokat tettek a lakáshelyzet javítása érdekében, de még sok a tennivaló.

**Nobody has done anything in order to improve the terms of purchase.**  
Senki nem tett semmit a vásárlási feltételek javítása érdekében.

**When do you think their house was built? Certainly in the last century.**  
Mit gondolsz, mikor épült a házuk? Bizonyára a múlt században.

**Too bad that you have to go! I would like to know if we will see each other again.**  
Milyen kár, hogy el kell menned! Szeretném tudni, hogy látni fogjuk-e még egymást.

**She thought of what she had been just told.**  
Arra gondolt, amit éppen mondtak neki.

**When I read the book, I took it back to my friend so that he could read it too.**  
Amikor elolvastam a könyvet, visszavittem a barátomnak, hogy ő is elolvasassa azt.

**I won’t be able to help you if my mother-in-law comes over, because I’m going to work in the whole afternoon.**  
Nem tudok majd segíteni neked, ha anyósom eljön, mert egész délután dolgozni fogok.

**When I saw her, I thought I had seen her somewhere.**  
Amikor megpillantottam őt, arra gondoltam, hogy láttam már valahol.

**I’m willing to lend you my bike if you promise that you’ll take care of it.**  
Hajlandó vagyok kölcsönadni a kerékpáromat, ha megígéred, hogy vigyázni fogsz rá.

**I hope you aren’t mad at me for calling you this late.**  
Remélem, nem haragszol, hogy ilyen későn hívtalak.

**He made a lot of money by buying the tickets in advance and by selling them in double price on the day of the match.**  
Sok pénzt keresett azzal, hogy előre felvásárolta a jegyeket, és a mérkőzés napján kétszer olyan magas áron eladta.

**My relatives were thought to be living in London.**  
Rokonaimról azt gondolták, hogy Londonban laknak. 

**I sign a life insurance so that my children have something to live on if I die.**  
Életbiztosítást kötök, hogy gyermekeimnek legyen valamiből élniük, ha meghalok.

**By the time my mother came home, I have cleaned up the flat. She told me not to cook dinner because she was about to cook it.**  
Amikor anyám hazajött, én már kitakarítottam a lakást. Azt mondta, hogy ne főzzek vacsorát, mert ő majd megfőzi.

**As a member of a sixteen-member delegation, I spent three weeks in England.**  
Egy tizenhat fős delegáció tagjaként három hetet töltöttem Angliában.

**Nowadays they don’t have to worry as much as they once did.**  
Mostanában nem kell annyit aggódniuk, mint valamikor.

**He doesn’t know this book at all. He hardly read it.**  
Nem ismeri egyáltalán ezt a könyvet. Alig valószínű, hogy olvasta azt.

**I like the way how this modern building fits to the environment of the old houses.**  
Tetszik, ahogy ez a modern épület beilleszkedik a régi házak környezetébe.
