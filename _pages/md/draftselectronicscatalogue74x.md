# 🐛 74x Catalogue

<div style="text-align: right;"><h7>Posted: 2022-11-07 21:49</h7></div>

###### .

<img src="dip14.png" title="" align="right" class="right">


## Table of contents

1. [Gates](#1)
2. [Flip-Flops and Latches](#2)
3. [Counters and Shift Registers](#3)
4. [Encoders, Decoders, Multiplexers, Demultiplexers](#4)


<a name="1"></a>
## 1. Gates

### 1.1 AND and NAND Gates

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="08.svg" title="" class="cat">
    <img src="11.svg" title="" class="cat">
    <img src="21.svg" title="" class="cat">
  </div>
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="00.svg" title="" class="cat">
    <img src="10.svg" title="" class="cat">
    <img src="20.svg" title="" class="cat">
    <img src="30.svg" title="" class="cat">
  </div>
</div>

### 1.2 OR and NOR Gates

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="32.svg" title="" class="cat">
    <img src="4075.svg" title="" class="cat">
    <img src="4072.svg" title="" class="cat">
  </div>
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="02.svg" title="" class="cat">
    <img src="27.svg" title="" class="cat">
    <img src="4002.svg" title="" class="cat">
  </div>
</div>

### 1.3 XOR and XNOR Gates

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="86.svg" title="" class="cat">
    <img src="266.svg" title="" class="cat">
  </div>
</div>

### 1.4 Inverters, Buffers and 3-State Drivers

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="04.svg" title="" class="cat">
    <img src="14.svg" title="" class="cat">
  </div>
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="125.svg" title="" class="cat">
    <img src="244.svg" title="" class="cat">
  </div>
</div>


<a name="2"></a>
## 2. Flip-Flops and Latches

### 2.1 D-Type Flip-Flops

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="74.svg" title="" class="cat">
    <img src="174.svg" title="" class="cat">
    <img src="374.svg" title="" class="cat">
    <img src="574.svg" title="" class="cat">
  </div>
</div>

### 2.2 JK-Type Flip-Flops

<div style="display: grid; margin-top: 20px;">
  <div style="display: block; align-items: left; flex-wrap: wrap;">
    <img src="73.svg" title="" class="cat">
    <img src="107.svg" title="" class="cat">
     <img src="109.svg" title="" class="cat">
 </div>
</div>

<a name="3"></a>
## 3. Counters and Shift Registers

>Please, refer to the [related Wikipedia site](https://en.wikipedia.org/wiki/List_of_7400-series_integrated_circuits) for now. 

<a name="4"></a>
## 4. Encoders, Decoders, Multiplexers, Demultiplexers

>Please, refer to the [related Wikipedia site](https://en.wikipedia.org/wiki/List_of_7400-series_integrated_circuits) for now. 
