# ![alt text](/_ui/img/icons/azure_identity_m.svg) Azure Identities and Governance

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_governance_-_bg.jpg)


## Sisällysluettelo

1. [Johdanto](#1)
2. [Entran Keskeiset Käsitteet](#2)
3. [Käyttäjät ja Ryhmät](#3)
4. [Resurssihierarkia](#4)
5. [Azure Policy](#5)
6. [Role-Based Access Control (RBAC)](#6)
7. [Johdanto "Infrastructure as a Code" -konseptiin](#7)


<a name="1"></a>
## 1. Johdanto

Azure Identities and Governance hallitsee käyttäjätilejä ja pääsyoikeuksia pilvipalveluissa. Sen avulla varmistetaan, että vain oikeat henkilöt ja laitteet pääsevät käsiksi resursseihin. Tärkein osa tätä on **Microsoft Entra**, joka hoitaa identiteettien hallinnan ja pääsyn valvonnan.

Azure tarjoaa myös työkaluja, kuten ehdollisen käyttöoikeuden ja roolipohjaiset pääsynhallinnat (**RBAC**).

Jos haluat oppia Microsoftin virallisesta dokumentaatiosta, aloita [täältä](https://learn.microsoft.com/en-us/entra/identity/).


<a name="2"></a>
## 2. Entran Keskeiset Käsitteet

Termi *"Entra"* on iso tuoteperhe, ja *"ID"* on osa siitä, mutta se on tärkein.

Microsoft muutti tuotteen nimen **Microsoft Azure Active Directory**sta, tai lyhyesti **Azure AD** tai **AAD**, **Microsoft Entra**ksi [elokuussa 2023](https://techcommunity.microsoft.com/t5/microsoft-entra-blog/azure-ad-is-becoming-microsoft-entra-id/ba-p/2520436). Pääsyy oli päästä eroon sekaannuksesta, joka liittyi Active Directoryn ja Azure Active Directoryn erottamiseen.

<div class="info">
<p>Entra ID on identiteettipalvelin Microsoftin <i>"pilvimaailmassa"</i>.</p>
</div>

Nopeana vertailuna Entran, entisen AAD:n, ja Active Directoryn välillä tärkein ero on, että Entra käyttää **erilaisia protokollia** kuin AD. Se tarjoaa myös paljon enemmän toimintoja, eikä siinä ole AD:n vanhoja jäänteitä. 

Identiteettipalvelimen lisäksi Entra ID on joukko erilaisia hallintamahdollisuuksia. Muuten sekä AD että Entra käsittelevät käyttäjiä, ryhmiä ja hyvin samanlaisia prosesseja todennuksessa ja valtuutuksessa.

### 2.1 Mikä on Entra ID?

![alt_text](azure_-_entra_overview.svg)

Entra ID on osa Entra-tuoteperhettä, joka tarjoaa identiteettipalveluja. Se auttaa hallitsemaan käyttäjiä ja heidän oikeuksiaan Microsoftin pilvimaailmassa.

Yleisimmät yleiset termit, jotka meidän on muistettava Entra ID:ä, ovat **Identiteetti ja Tili (Account)**. Nämä termit kuulostavat samanlaisilta, mutta ne ovat periaatteessa erilaisia:

- **Identiteetti** on objekti, joka voidaan todentaa.
- **Tili** on identiteetti, johon liittyy tietoja.

Käyttäjänä sinulla **ei ole identiteettiä**, mutta sinulla on tili MS Entra ID:ä varten. Tätä tiliä käytetään oma itsesi todentamiseen. **Entra ID -tili** on se erityinen tili, joka luodaan Entra ID:ä varten.

Entra ID kattaa seuraavat objektit **entiteetteinä**:

- **Käyttäjät**
- **Ryhmät**
- **Laitteet**
- **Konfiguraatio**

Vierailemalla osoitteessa [portal.azure.com](https://portal.azure.com) ja etsimällä *"Microsoft Entra ID"* voit nähdä yleiskatsauksen näistä entiteeteistä:

![alt_text](azure_-_portal_-_entra_id_overview.png)

### 2.2 Vuokraaja vs. Tilauksen

<div class="info">
<p>Vuokraaja (Tenant) edustaa identiteettikerrosta Entrassa.</p>
</div>

**Tenant** tai **hakemisto** ei ole tili, vaan se on identiteetti, jonka luot, kun rekisteröidyt Entraan. Itse asiassa Tenant **luodaan automaattisesti**, kun organisaatio tilaa Microsoftin pilvipalvelun.

Se on **omistettu ja luotettu Microsoft Entra ID -instanssi**. *"Tenant"* tarkoittaa yhtä instanssia, joka edustaa **yhtä organisaatiota**, joten yleensä yrityksellä on yksi tenant, mutta poikkeuksia voi olla. Termit tenant ja hakemisto käytetään usein toistensa sijasta.

**Tenant ei voi sisältää suoraan resursseja**, kuten sovelluksia, Storage Accountseja tai virtuaalikoneita. Näitä varten on toinen termi, kuten **Subscription**.

<div class="info">
<p>Tilauksen (Subscription) on Azure-palvelujen ja -resurssien laskutuskerros.</p>
</div>

**Subscriptionilla ei ole identiteettiä**, koska identiteetti hoidetaan täysin Tenant-kerroksessa. Subscription on siis Azure-pilvipalvelujen laskutusta varten. Yhdellä tenantilla voi olla useita subscriptioneja, ja suurilla IT-yrityksillä niitä voi olla satoja tai tuhansia.

Voit olla vuorovaikutuksessa Azure-infrastruktuurin kanssa suoraan komentoriviltä, esimerkiksi Bashissa, käyttämällä **Azure Resource Manager** -rajapintaa (katso tarkemmin [luvusta 7](#7)). Tämä menetelmä edellyttää, että järjestelmässäsi on `az`-komento, joka sisältyy `azure-cli`-pakettiin. Kyseinen paketti on kokoelma Python-skriptejä, mutta `az`-komento tarjoaa korkean tason ja kätevän hallintaliittymän, jolla voit kysellä ja määrittää lähes mitä tahansa Azure-infrastruktuurissasi.

Aloittaminen edellyttää kirjautumista sisään. Komento avaa selaimen, jossa voit vahvistaa identiteettisi, minkä jälkeen voit jatkaa käytettävän tilauksen valinnalla. Voit vaihtaa tilausten välillä tarpeen mukaan, jos käyttäjälläsi on niihin pääsy:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az login</span>
A web browser has been opened at https://login.microsoftonline.com/organizations/oauth2/v2.0/authorize. Please continue the login in the web browser. If no web browser is available or if the web browser fails to open, use device code flow with `az login --use-device-code`.

Retrieving tenants and subscriptions for the selection...

[Tenant and subscription selection]

No     Subscription name                     Subscription ID                       Tenant
-----  ------------------------------------  ------------------------------------  -----------
[1]    Critical Components DevTest           abcd1234-a1b2-c3d4-e5f6-a1b2c3d4e5f6  XYZ Tech Oy
[2]    Critical Components Production        efda7890-b2c3-d4e5-f6a1-b2c3d4e5f6a1  XYZ Tech Oy
[3]    Kube Platform Development             abfe3456-a1b2-c3d4-e5f6-a1b2c3d4e5f6  XYZ Tech Oy
[4]    Kube Platform Production              fade9012-f6a1-b2c3-d4e5-f6a1b2c3d4e5  XYZ Tech Oy
[5]    Monitoring Dev                        dead5678-e5f6-a1b2-c3d4-e5f6a1b2c3d4  XYZ Tech Oy
[6]    Monitoring Test                       cafe1234-d4e5-f6a1-b2c3-d4e5f6a1b2c3  XYZ Tech Oy
[7]    Monitoring Prod                       bead8901-c3d4-e5f6-a1b2-c3d4e5f6a1b2  XYZ Tech Oy
[8]    SRE DevTest                           face5678-b2c3-d4e5-f6a1-b2c3d4e5f6a1  XYZ Tech Oy
[9] *  SRE                                   befa9012-a1b2-c3d4-e5f6-a1b2c3d4e5f6  XYZ Tech Oy
</pre>

Jos olet hiljattain saanut muutoksia käyttäjäoikeuksiisi, esimerkiksi pyytänyt yhden tai useamman väliaikaisen **Access packagen**, voi kestää muutamia minuutteja, ennen kuin pääset käsiksi uusiin resursseihin. Nopeuttaaksesi prosessia voi olla hyödyllistä kirjautua ulos, tyhjentää tilitiedot ja kirjautua takaisin sisään:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az logout</span>

<span class="psu">$</span> <span class="cmd">az account clear</span>

<span class="psu">$</span> <span class="cmd">az login</span>
[...]
</pre>

### 2.3 Entra ID keskeiset ominaisuudet

Verrattaessa **MS Entraa AD Domain Servicesiin**, voidaan sanoa, että Entra ID on ensisijaisesti **identiteettiratkaisu**. Se on suunniteltu internet-pohjaisille sovelluksille, käyttäen HTTP- ja HTTPS-protokollia. Useimmiten siihen tehdään kyselyitä **REST API** kautta HTTPS:ä.

Se käyttää HTTPS-protokollia, kuten **SAML**, **WS-Federation** ja **OpenID Connect** todentamiseen. Lisäksi se käyttää **OAuthia** valtuutukseen.

Siihen sisältyy **federointipalveluja** ja monia kolmannen osapuolen palveluja, kuten Facebook.

MS Entra ID **käyttäjät ja ryhmät** luodaan litteässä rakenteessa, **eikä siinä ole Organisaatioyksiköitä** (**OUs**) tai **Group Policy Objectseja** (**GPOs**).

### 2.4 Entra ID lisensointi

Termi **lisensointi** on eri asia kuin **hinnoittelu**, koska hinnoittelu viittaa palvelujen ja resurssien listahintoihin. Lisensointi määrittää, mitkä ominaisuudet ja toiminnot ovat käytettävissä. Esimerkiksi maksullisissa Entra ID -versioissa saat lisäominaisuuksia, kuten paremman turvallisuuden ja laajemmat hallintamahdollisuudet.

Voit rekisteröityä vain **ilmaiseen Entra ID**:hen, jossa on vain perustoiminnot. Myös yritykset aloittavat usein ilmaisversiolla.

#### 2.4.1 Ilmainen vs. maksullinen

Entra ID **ilmaisversio** tarjoaa perusominaisuuksia, kuten **yhden kirjautumisen** (**Single Sign-On**) ilman käyttäjämäärärajoitusta, **pilvi- ja federointitodennuksen**, **itsepalvelutilinhallinnan portaalin** sekä **monivaiheisen tunnistautumisen** (**MFA**). Näillä perusominaisuuksilla yritykset voivat aloittaa, mutta monimutkaisempia tarpeita varten on olemassa maksullisia **Premium-versioita**.

Maksullisissa Premium-versioissa saat edistyneempiä toimintoja, kuten **ryhmien hallinnan**, **ehdollisen pääsyn** sekä **riskipohjaisen ehdollisen pääsyn**. Lisäksi Premium-versiot mahdollistavat käyttäjien ja ryhmien **automaattisen provisioinnin** sovelluksiin, mikä säästää aikaa ja vähentää manuaalista työtä. Erityisen hyödyllinen ominaisuus on **Privileged Identity Management** (**PIM**), joka antaa *"just-in-time"* -oikeudet tiettyihin **rooleihin ja resursseihin** vain tarvittaessa.

Maksullisten versioiden hallintasuunnitelma kattaa kaksi tärkeää ominaisuutta: käyttäjien ja ryhmien automaattisen provisioinnin sovelluksiin sekä PIM.

<div class="info">
<p>Entra ID:ä on kaksi pääasiallista Premium-versiota: <b>P1</b> ja <b>P2</b>.</p>
</div>

P1 on premium-tason lisenssi, joka vastaa "**E3**" -versiota, ja P2, joka vastaa "**E5**" -versiota, sisältää kaikki edellä mainitut ominaisuudet. Jos yrityksellä on tarvetta kattavammille turvaominaisuuksille ja hallintatyökaluille, P2-versio on suositeltava valinta.

Yksityiskohtaisempaa vertailua ja paremman yleiskatsauksen eri Entra-suunnitelmista ja hinnoittelusta löydät [Microsoftin viralliselta tietoturvasivulta](https://www.microsoft.com/en-us/security/business/microsoft-entra-pricing).

### 2.5 Laitteiden identiteetit

Saatat haluta liittää oman laitteesi Entra ID turvallisuus- ja hallintatarkoituksia varten. Yleisesti ottaen, kun yritys "tuntee" liitetyt laitteesi, se voi hallita pääsyäsi yrityksen resursseihin.

#### 2.5.1 Laitteen liittäminen Entra ID:hen

Käytännössä tämä tarkoittaa, että käyttäjät haluavat kirjautua laitteisiin, sovelluksiin ja palveluihin mistä tahansa. Käyttäjät haluavat kirjautua käyttämällä organisaation työhön tai kouluun tarkoitettua tiliä henkilökohtaisen tilin sijasta. Ensimmäinen askel onkin liittää laite, joka tarjoaa tarvittavat ominaisuudet.

#### 2.5.2 Kolme eri laitteen identiteettiä

Tarpeistasi riippuen, on olemassa kolme eri laitteen identiteettiä:

- **Rekisteröidyt laitteet**: sopii *"Bring Your Own Device"* -lähestymistapaan
    - hallinta **Mobile Device Management** (**MDM**) -työkaluilla, kuten MS Intune
    - käytä tätä, jos haluat valvoa käytäntöjen noudattamista
    - soveltuu Win10+, iOS, Android ja MacOS -laitteille
- **Liitetyt laitteet**: pilvipohjaisille tai pelkästään pilvessä toimiville organisaatioille
    - voi käyttää Conditional Access -käytäntöjä
    - soveltuu Win10+ -laitteille
- **Hybridiliitetyt laitteet**:
    - jos olet ottanut käyttöön Win32-sovelluksia näille laitteille
    - jos haluat jatkaa ryhmäkäytäntöjen (Group Policy) käyttöä laitteen hallintaan
    - jos haluat käyttää olemassa olevia laitekuvaratkaisuja laitteiden käyttöönotossa
    - soveltuu Win7+ -laitteille

### 2.6 Microsoftin identiteetti- ja pääsynhallintaratkaisut

Vaikka Entra ID on ensisijainen ja yleinen tarjonta, Microsoft tarjoaa myös muita tuotteita eri tilanteisiin, joissa identiteetti- ja pääsynhallinta koskee ulkopuolisia kumppaneita, toimittajia tai asiakkaita/kuluttajia:

- **Entra ID**: tarjoaa yleisen identiteetin ja pääsynhallinnan työntekijöille pilvi- tai hybridiympäristössä
- **[Entra B2B](https://learn.microsoft.com/en-us/entra/external-id/what-is-b2b)** (*"Business-to-Business"*): jos haluat tehdä yhteistyötä vieraskäyttäjien ja ulkopuolisten liikekumppaneiden, kuten toimittajien kanssa
- **[Azure AD B2C](https://learn.microsoft.com/en-us/azure/active-directory-b2c/overview)** (*"Business-to-Consumer"*): jos haluat hallita asiakkaiden rekisteröitymistä, kirjautumista ja profiilien hallintaa heidän käyttäessään sovelluksiasi.

#### 2.6.1 Azure Portal vs. Entra Admin Center

Entra ID muiden asetusten lisäksi on kaksi verkkokäyttöliittymää, joilla voidaan hallita Entra ID ydintoimintoja, kuten Käyttäjien ja Ryhmien hallintaa:

- **[Azure Portal](https://portal.azure.com/)**: kaikki Azure-toiminnot, mukaan lukien suurin osa Entra ID ominaisuuksista
- **[Entra Admin Center](https://entra.microsoft.com/)**: kaikki Entra ID ominaisuudet

Näin Entra Admin Center toivottaa sinut tervetulleeksi lokakuussa 2024:

![alt text](azure_-_entra_admin_center.png)

### 2.7 Tilauksen vaihtaminen

Joskus on tarpeellista vaihtaa tilausta, erityisesti silloin, kun olet kirjautunut sisään komennolla `az login` ja valinnut aktiivisesti käytettävän tilauksen, mutta myöhemmin haluat vaihtaa toiseen tilaukseen.

Jos et ole varma, mihin tilauksiin voit vaihtaa, on hyödyllistä ensin tulostaa kaikki tiedossa olevat Azure-tilaukset, jotka liittyvät käyttäjätiliisi. Tämä voidaan tehdä komennolla `az account list`. Komento näyttää myös "State"- ja "IsDefault"-sarakkeet, joten tiedät, mikä tili oli viimeksi aktivoituna. Esimerkki komennosta, jossa käytetään taulukkomuotoista tulostusta:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az account list --output table</span>
Name                                  CloudName    SubscriptionId                        TenantId                              State    IsDefault
------------------------------------  -----------  ------------------------------------  ------------------------------------  -------  -----------
Critical Components DevTest           AzureCloud   abcd1234-a1b2-c3d4-e5f6-a1b2c3d4e5f6  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Critical Components Production        AzureCloud   efda7890-b2c3-d4e5-f6a1-b2c3d4e5f6a1  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Kube Platform Development             AzureCloud   abfe3456-a1b2-c3d4-e5f6-a1b2c3d4e5f6  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Kube Platform Production              AzureCloud   fade9012-f6a1-b2c3-d4e5-f6a1b2c3d4e5  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Monitoring Dev                        AzureCloud   dead5678-e5f6-a1b2-c3d4-e5f6a1b2c3d4  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Monitoring Test                       AzureCloud   cafe1234-d4e5-f6a1-b2c3-d4e5f6a1b2c3  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
Monitoring Prod                       AzureCloud   bead8901-c3d4-e5f6-a1b2-c3d4e5f6a1b2  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
SRE DevTest                           AzureCloud   face5678-b2c3-d4e5-f6a1-b2c3d4e5f6a1  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  False
SRE                                   AzureCloud   befa9012-a1b2-c3d4-e5f6-a1b2c3d4e5f6  9876dcba-1111-2222-3333-a1b2c3d4e5f6  Enabled  True
</pre>

Esimerkki tilauksen vaihtamisesta:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az account set --subscription=face5678-b2c3-d4e5-f6a1-b2c3d4e5f6a1</span>
</pre>


<a name="3"></a>
## 3. Käyttäjät ja Ryhmät

Entra ID:n käyttäjien ja ryhmien hallinta on keskeinen osa organisaation identiteettien ja pääsyn hallintaa. Azure tarjoaa monia ominaisuuksia käyttäjien ja ryhmien tehokkaaseen hallintaan, jotka tukevat erilaisia organisaatioympäristöjä, kuten pilvi- ja hybridiratkaisuja. Alla on yleiskatsaus keskeisiin käsitteisiin ja ominaisuuksiin. 

Tarkempia tietoja ja ajantasaiset ohjeet löydät aina osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/entra/identity/users/directory-overview-user-model).

### 3.1 Käyttäjät

Käyttäjät ovat olennainen osa Entra ID:ä, ja heidän hallintaansa liittyy useita keskeisiä käsitteitä. Ensinnäkin **User Principal Name** (**UPN**) on käyttäjän sähköpostimuotoinen identiteetti, jota käytetään kirjautumisessa Entra ID -ympäristöön.

**On-premises synkronointi** mahdollistaa käyttäjätilien ja muiden identiteettien synkronoinnin paikallisesta Active Directorysta (AD) pilveen, mikä helpottaa hybridiympäristön hallintaa. Käyttäjiä voidaan myös luoda massana, käyttämällä ominaisuutta **Bulk create**, mikä on hyödyllistä suurille organisaatioille.

Jokaisella käyttäjällä on myös tärkeitä parametreja, kuten **Job Information**, joka määrittelee käyttäjän tehtävänimikkeen ja osaston. Käyttäjälle voidaan lisätä myös esimies **Add manager** -linkin kautta sekä määritellä **Usage locations**, joilla rajataan käyttäjän maantieteellistä sijaintia ja käyttöoikeuksia.

Lopuksi on syytä mainita **Self-Service Password Reset**, jonka avulla käyttäjät voivat palauttaa salasanansa itsepalveluna, mikä vähentää IT-tuen kuormitusta ja parantaa käyttäjäkokemusta.

### 3.2 Ryhmät

Ryhmiä käytetään käyttäjien hallinnan yksinkertaistamiseen, ja niitä on kahta tyyppiä: **Security-ryhmät**, jotka rajoittavat ja hallitsevat pääsyä resursseihin, sekä **Microsoft 365 -ryhmät**, jotka keskittyvät yhteistyön mahdollistamiseen, esimerkiksi Teamsissa.

Ryhmien jäsenyyksiä voi hallita kahdella tavalla: **Assigned**-jäsenyyttä käytetään silloin, kun jäsenet lisätään manuaalisesti, ja **Dynamic**-jäsenyyttä dynaamisilla säännöillä (**Dynamic membership rules**), joissa jäsenyys määräytyy automaattisesti ehtojen perusteella. Näissä säännöissä käytetään **Rule syntax** -syntaksia, jolla määritellään säännöt.

Ryhmille voidaan myös lisätä lisenssejä, kuten **Office 365 E3**, mikä antaa jäsenille pääsyn tietyille resursseille. Jäsenyyden polkuja voidaan hallita **Direct**-, **Inherited**- tai näiden yhdistelminä, riippuen siitä, miten ryhmän jäsenten pääsy määritellään.

### 3.3 Hallintayksiköt (Administrative Units)

**Administrative Unit** (**AU**) on ominaisuus, jonka avulla voidaan jakaa käyttäjät ja resurssit maantieteellisesti tai organisatorisesti hallittaviin osiin, kuten *Europe*. AU eroaa perinteisestä **Organization Unit** (**OU**) -käsitteestä, sillä AU:ta ei voida käyttää paikallisissa ympäristöissä, eikä olemassa olevaa OU:ta voi siirtää AU:ksi.

AU voi sisältää käyttäjiä, ryhmiä ja laitteita. **Restricted Management** on AU:n tärkeä ominaisuus, jonka avulla hallintaoikeuksia voidaan rajoittaa vain tietyille resursseille ja alueille. Tätä ominaisuutta voidaan hyödyntää esimerkiksi, kun hallintavastuu halutaan jakaa eri maantieteellisille alueille tai osastoille. Lisätietoja ja päivityksiä Restricted Management -ominaisuudesta löydät aina osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/entra/identity/role-based-access-control/administrative-units).


<a name="4"></a>
## 4. Resurssihierarkia

Azure-resurssien hallinta perustuu selkeään hierarkiaan, jonka avulla organisaatiot voivat järjestää ja hallita resurssejaan tehokkaasti. Tämä hierarkia lähtee liikkeelle **Microsoft Entra ID -tenantista** ylimpänä tasona ja jatkuu **Management Groupseihin** (**Hallitsemisryhmät**), joiden avulla voidaan jakaa politiikkoja (**Azure Policy**) ja budjetteja alempien tasojen tilauksiin. Tämän jälkeen hierarkia sisältää **Tilaukset** (**Subscriptions**), **Resurssiryhmät** (**Resource Groups**) ja lopulta varsinaiset **resurssit**, kuten virtuaalikoneet (VM), tietokannat (DB) ja verkot (VNet).

![alt_text](azure_-_resource_hierarchy.svg)

Artikkelin mukana oleva kaavio havainnollistaa näitä keskeisiä komponentteja ja niiden hierarkiaa. Lisätietoja löydät aina osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/cloud-adoption-framework/ready/azure-setup-guide/organize-resources).

### 4.1 Hallintaryhmät (Management Groups)

**Hallintaryhmät** tarjoavat tason tilausten yläpuolella ja helpottavat useiden tilausten hallintaa organisaation laajuisesti. Hallintaryhmien avulla voit kohdentaa politiikkoja ja budjetteja laajasti eri tilauksiin ja varmistaa niiden periytymisen hierarkian alemmille tasoille. Näin esimerkiksi tietoturva- ja talousraportoinnin vaatimukset saadaan toteutettua yhtenäisesti kaikissa tilauksissa.

- Hallintaryhmissä asetetut käyttöoikeudet ja politiikat **periytyvät automaattisesti** tilauksiin ja niiden alaisiin resurssiryhmiin ja resursseihin.
- Politiikat voivat esimerkiksi **rajoittaa**, mitä resursseja voidaan käyttää, tai asettaa kustannusseurannan sääntöjä organisaation eri osastoille.
- **Oikeuksien periytyminen** tarkoittaa, että rajoittavin oikeus voittaa, ja tämä mahdollistaa selkeän hallinnan eri tasoilla.
- Lisäksi voit lisätä **resurssilukituksia** tilaus- ja resurssiryhmätasolla estääksesi vahingossa tehtyjä muutoksia.

Hallintaryhmät tarjoavat hallinnan ja raportoinnin mahdollisuuden organisaation eri tasoilla, erityisesti silloin, kun halutaan noudattaa sääntelyä tai hallita kustannuksia keskitetysti.

### 4.2 Tilaukset (Subscriptions)

Tilaus on Azuren palveluiden **looginen yksikkö**, joka on liitetty Azure-tiliin. Se toimii sekä **turvallisuus- että laskutusrajana**, ja vain Entra ID:ä tai sen hyväksymässä hakemistossa olevat identiteetit voivat luoda tilauksen. Tilaukset mahdollistavat resurssien ja käyttöoikeuksien hallinnan erillisinä yksiköinä, mikä on hyödyllistä organisaatioille, joilla on laajoja infrastruktuuritarpeita.

- Tilaus on turvallisuuden ja kustannusten hallinnan perusyksikkö, ja sitä **ei voi jakaa useiden tenanttien kesken**.
- Samalla tenantilla voi kuitenkin olla **satoja tai jopa tuhansia tilauksia**.

#### 4.2.1 Tilauksen käyttö (Subscription Usage)

Eri tilaustyypit tarjoavat joustavuutta ja mahdollisuuksia erilaisten organisaatioiden tarpeisiin:

- **Free**: sisältää 200 dollarin krediitin ensimmäisten 30 päivän ajaksi ja ilmaisen, rajoitetun käyttöoikeuden 12 kuukauden ajan.
- **Pay-As-You-Go**: joustavin, mutta kallein vaihtoehto, jossa laskutus tapahtuu kuukausittain käytön mukaan.
- **Cloud Solutions Provider** (**CSP**): sopii erityisesti pienille ja keskisuurille yrityksille, jotka ostavat palveluita kumppanin kautta.
- **Enterprise**: suuryrityksille, joissa hinta määräytyy sopimuksen perusteella ja joissa voi olla merkittäviä alennuksia volyymikäytön myötä.

#### 4.2.2 Tilauksen hankkiminen

Tilauksen voi hankkia useilla tavoilla:

- **yrityssopimuksella** (Enterprise Agreement)
- **jälleenmyyjien** kautta (Cloud Solution Provider -ohjelma)
- **kumppaneiden** kautta
- **henkilökohtaisella ilmaisella tilillä**, joka voidaan ottaa käyttöön välittömästi.

Resursseja ei voi ottaa käyttöön suoraan tilauksen alla, vaan ne on ensin sijoitettava **Resurssiryhmään**.

### 4.3 Resurssiryhmät (Resource Groups)

**Resurssiryhmät** ovat loogisia kokonaisuuksia, jotka ryhmittävät yhteen liittyvät resurssit. Resurssiryhmät tarjoavat helpon tavan hallita resurssien elinkaarta, käyttöoikeuksia ja käyttöön liittyviä sääntöjä. Ne ovat tärkeä osa Azuren hierarkiaa, sillä kaikki resurssit on sijoitettava johonkin resurssiryhmään.

- Resurssiryhmät voivat sisältää resursseja monista **eri palveluista ja sijainneista**.
- Resurssiryhmien uudelleennimeäminen **ei ole mahdollista, eikä niitä voi asettaa sisäkkäisiksi**.
- Voit **siirtää resursseja** ryhmien välillä tai jopa eri tilausten välillä.

Resurssiryhmät voidaan organisoida joko **projektin** tai **resurssityypin** perusteella. Esimerkiksi yksi ryhmä voi sisältää kaikki tietyn projektin palvelut, kun taas toinen voi olla erikoistunut tietyn tyyppisiin resursseihin, kuten verkko- tai tallennusratkaisuihin.

### 4.4 Alueet (Regions)

Azure-palvelut ovat hajautettuja maailmanlaajuisesti alueisiin, jotka edustavat useiden datakeskusten kokoelmia. Oikean alueen valinta voi parantaa suorituskykyä ja auttaa noudattamaan lainsäädännön vaatimuksia, kuten tietojen paikallista säilyttämistä.

- Alueiden hinnat vaihtelevat sijainnin mukaan, ja esimerkiksi **East US** on usein yksi edullisimmista vaihtoehdoista, mutta sopii parhaiten ei-kriittisiin ympäristöihin, kuten kehitykseen.
- Alueet on paritettu varmistamaan korkea käytettävyys ja tietojen palauttaminen katastrofitilanteissa.
- On olemassa myös globaaleja palveluita, jotka eivät ole sidottuja mihinkään tiettyyn alueeseen.

### 4.5 Resurssien merkintä (Resource Tagging)

Resurssien merkintä auttaa lisäämään lisätietoja Azure-resursseihin, mikä helpottaa niiden organisointia ja kustannusten hallintaa. Merkinnät koostuvat nimi-arvo-parista, jotka voivat olla hyödyllisiä esimerkiksi laskutuksen seurannassa.

Esimerkki: `Cost-center: Marketing` voi helpottaa markkinointiosaston resurssien erittelyä ja kustannusten seuraamista.

### 4.6 Kustannusten hallinta

Azure tarjoaa erittäin tarkan ja joustavan kustannustenhallintamallin, jonka avulla voit hallita organisaation pilvipalveluiden kustannuksia tehokkaasti. Tärkeimmät tekijät, jotka vaikuttavat kustannuksiin, ovat:

- **Resurssikohtaiset kustannukset**: Jokainen resurssi, kuten virtuaalikoneet, tietokannat tai tallennustila, hinnoitellaan käytön mukaan. Mitä enemmän resursseja käytetään, sitä korkeammat kustannukset.
- **Sijainnin vaikutus kustannuksiin**: Aluekohtaiset hinnat vaihtelevat, ja resurssien sijoittaminen eri alueille voi vaikuttaa huomattavasti kokonaiskustannuksiin.
- **Verkkoliikenteen kustannukset**: Lähtevän ja saapuvan verkkoliikenteen kustannukset eroavat toisistaan, ja erityisesti lähtevä liikenne voi lisätä kustannuksia merkittävästi.
- **Azure Reserved Instances**: Ennakoimalla resurssitarpeet ja sitoutumalla resurssien käyttöön pidemmäksi ajaksi, kuten kahdeksi vuodeksi, voi saavuttaa merkittäviä säästöjä.
- **Azure Hybrid Benefit**: Mahdollisuus hyödyntää organisaation olemassa olevia on-premises-lisenssejä Azuren palveluiden kanssa, mikä vähentää lisenssikustannuksia.

Kustannusten hallinnan tueksi Azure tarjoaa työkaluja, kuten hälytyksiä, budjettiseurantaa ja suosituksia Azure Advisorin avulla, mikä mahdollistaa kustannusten optimoinnin. Lisäksi organisaatiot voivat hyödyntää **Azure Pricing Calculator** -työkalua osoitteessa [azure.microsoft.com](https://azure.microsoft.com/en-us/pricing/calculator), joka auttaa laskemaan resurssien hinnat ja vertaamaan niitä esimerkiksi on-premises-ratkaisuihin. Tämä työkalu auttaa sinua tekemään tarkkoja päätöksiä resurssien ja kustannusten suhteen.


<a name="5"></a>
## 5. Azure Policy

Azure Policy on **palvelu**, jonka avulla voit **luoda, määrittää ja hallita sääntöjä**, jotka ohjaavat Azure-resurssien käyttöä ja noudattavat organisaatiosi vaatimuksia. Azure Policy tarkistaa resurssien vaatimustenmukaisuuden automaattisesti ja arvioi niitä säännöllisesti, etsien resursseja, jotka eivät täytä asetettuja sääntöjä.

<div class="info">
<p>Azure Policy on työkalu, jolla varmistetaan Azure-resurssien vaatimustenmukaisuus ja hallitaan niiden käyttöä asettamalla sääntöjä ja ehtoja, mutta se ei hallinnoi käyttäjäoikeuksia tai käyttöoikeustasoja, kuten Azure RBAC.</p>
</div>

### 5.1 Azure Policyn edut

Azure Policy tarjoaa useita merkittäviä etuja organisaatioille, jotka haluavat hallita pilviympäristöjään tehokkaasti ja vaatimustenmukaisesti:

- **Noudattaminen ja valvonta**: Azure Policy varmistaa, että kaikki resurssit noudattavat organisaation vaatimuksia.
- **Skaalautuvuus**: Voit soveltaa sääntöjä laajassa mittakaavassa useiden tilausten ja resurssiryhmien välillä.
- **Korjaustoimenpiteet**: Azure Policy voi myös suorittaa automaattisia korjauksia, jos resurssit eivät täytä asetettuja sääntöjä, kuten puuttuvan tagin lisääminen.

### 5.2 Käyttötapaukset

Azure Policy tarjoaa monipuoliset mahdollisuudet valvoa ja rajoittaa resurssien käyttöä. Tässä on muutamia yleisiä käyttötapauksia:

- **Sallittujen resurssityyppien määrittäminen**: Voit rajoittaa organisaation käyttämien resurssien tyyppejä, esimerkiksi sallia vain tietynlaiset tallennus- tai verkkotyypit.
- **Sallittujen virtuaalikoneiden SKU-tasojen rajoittaminen**: Voit määrittää, mitä virtuaalikoneiden SKU-tasoja organisaatiosi saa käyttää, varmistaen kustannusten ja suorituskyvyn hallinnan.
- **Sallittujen sijaintien rajoittaminen**: Azure Policy voi rajoittaa resurssien käyttöä vain tietyille alueille, esimerkiksi organisaation vaatimusten tai lakisääteisten vaatimusten mukaisesti.
- **Tagien ja niiden arvojen pakottaminen**: Voit varmistaa, että kaikki resurssit sisältävät vaaditun tagin ja sen arvon, mikä helpottaa kustannusten ja vastuiden seurantaa.
- **Azure Backupin käytön pakottaminen**: Voit käyttää Azure Policyä tarkistamaan ja varmistamaan, että virtuaalikoneille on otettu varmuuskopiointi käyttöön, mikä on tärkeää auditoinnin ja tietojen palauttamisen kannalta.

### 5.3 Policy Definition ja Policy Initiative

Azure Policy -sääntöjä voidaan soveltaa useisiin eri tasoihin (**scope**):

- **Management Group**: Hallintaryhmätasolla voidaan valvoa useita tilauksia yhdellä politiikalla.
- **Subscription**: Tilaustasolla voit asettaa sääntöjä yksittäisille tilauksille.
- **Resource Group**: Resurssiryhmätasolla voit valvoa resurssiryhmän kaikkia resursseja.

Kun halutaan hallita useita sääntöjä yhdessä, voidaan käyttää **Policy Initiative** -ominaisuutta. Se tarkoittaa useiden Policy Definition -sääntöjen yhdistämistä yhteen ryhmään, jolloin voit soveltaa koko sääntökokonaisuutta kerralla laajassa mittakaavassa.

Azure Policy tarjoaa organisaatioille tehokkaan tavan hallita ja valvoa resursseja, varmistaa vaatimustenmukaisuus sekä optimoida kustannuksia. Lisää tietoa ja ajantasaisia ohjeita löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/governance/policy/overview).


<a name="6"></a>
## 6. Role-Based Access Control (RBAC)

**Azure Role-Based Access Control** (**RBAC**) on keskeinen mekanismi resurssien käyttöoikeuksien hallintaan Azure-ympäristössä. RBAC:n avulla voit myöntää tietyt oikeudet yksittäisille käyttäjille, ryhmille ja sovelluksille organisaation vaatimusten mukaisesti. Tämä mahdollistaa sen, että käyttäjät voivat suorittaa vain ne tehtävät, jotka heidän roolinsa sallivat. Azure RBAC:ssä on tarjolla useita sisäänrakennettuja rooleja, mutta tarvittaessa voit myös luoda omia mukautettuja rooleja.

<div class="info">
<p>Azure RBAC on käyttöoikeuksien hallintajärjestelmä, jonka avulla voit tarkasti määrittää, mitä resursseja käyttäjät ja ryhmät voivat käyttää, mutta se ei hallinnoi käyttäjien pääsyä Microsoft Entra ID:n käyttäjä- ja ryhmähallintaan liittyviin tehtäviin.</p>
</div>

### 6.1 Roolit ja niiden erot

Roolit määrittävät joukon oikeuksia ja tehtäviä, joita käyttäjät tai ryhmät voivat suorittaa. On olemassa kaksi erillistä roolityyppiä, joita hallitaan eri tasoilla:

- **Azure RBAC -roolit**: hallitsevat pääsyä Azure-resursseihin, kuten virtuaalikoneisiin, tietokantoihin ja verkkoihin.
- **Azure AD -roolit**: hallitsevat pääsyä Microsoft Entra ID -resursseihin, kuten käyttäjiin, ryhmiin ja sovelluksiin.

Yleisimmät roolit tenantin tasolla ovat:

- **Global Administrator**: Tällä roolilla varustettu käyttäjä voi hallita kaikkia Entra ID:n ja siihen liitettyjen Microsoft-palveluiden toimintoja ilman rajoituksia.
- **User Administrator**: Käyttäjänhallitsija voi hallita käyttäjiä ja ryhmiä sekä resetoida salasanoja, mutta ei esimerkiksi muuttaa Global Administratorin salasanaa. Suositeltavaa on käyttää PIM:ää (Privileged Identity Management) salasanojen hallinnassa turvallisuussyistä.

Yleisimmät roolit Subscription-tasolla ovat:

- **User Access Administrator**: Tämä rooli mahdollistaa käyttäjien pääsyn hallinnan Azure-resursseihin, kuten virtuaalikoneisiin tai tallennustilaan, Subscription-tasolla.
- **Owner**: Tarjoaa täyden hallintaoikeuden kaikkiin resursseihin kyseisessä tilauksessa.

### 6.2 Roolin määrittely

Roolien määrittely voidaan tehdä JSON-formaatissa, joka määrittää tarkasti roolin oikeudet. Esimerkiksi "Contributor"-rooli on lähes identtinen "Owner"-roolin kanssa, mutta siinä on joitakin rajoituksia.

Roolimäärittelyssä keskeisiä komponentteja ovat:

- **Actions**: Tämä määrittää sallitut toiminnot, esimerkiksi `*` merkitsee, että roolilla on oikeus kaikkiin toimintoihin.
- **NotActions**: Määrittelee toiminnot, joita rooli ei saa tehdä. Esimerkiksi "Contributor"-rooli ei voi poistaa tai muuttaa käyttöoikeuksia eikä käyttää elevated access -toimintoa.
- **DataActions**: Määrittelee oikeudet dataan liittyviin toimiin, kuten tallennustilaan pääsyyn.
- **NotDataActions**: Määrittelee dataan liittyvät toiminnot, joita ei sallita.

Esimerkkinä "Contributor"-roolin JSON-määrittelystä:

```
"permissions": [
    {
        "actions": [
            "*"
        ],
        "notActions": [
            "Microsoft.Authorization/*/Delete",
            "Microsoft.Authorization/*/Write",
            "Microsoft.Authorization/elevateAccess/Action"
        ],
        "dataActions": [],
        "notDataActions": []
    }
]
```

### 6.3 Käyttöoikeuksien määrittäminen Azure-resursseille

Azure-resursseihin voidaan määrittää käyttöoikeuksia useilla tavoilla, ja käytettävissä ovat kolme pääasiallista menetelmää:

1. **Azure Portal**: Käyttöoikeuksien hallinta onnistuu intuitiivisesti Azure-portaalin kautta, jossa voit tarkastella, lisätä tai muokata rooleja ja oikeuksia.
2. **Azure Cloud Shell**: Portaalissa sisäänrakennettu komentoriviliittymä, jonka kautta voit suorittaa komentoja suoraan ilman ulkoisia työkaluja.
3. **Azure PowerShell ja CLI**: Voit käyttää PowerShell- ja CLI-komentoja esimerkiksi resurssien uudelleenkäynnistämiseen, käyttöoikeuksien hallintaan ja automaatioiden luomiseen (esim. `az vm restart -g MyResourceGroup -n MyVm`).

### 6.4 RBAC-konfiguraatioiden tarkennukset

Kun käytät RBAC:ää ja määrität rooleja, on tärkeää muistaa, että käyttöoikeudet ovat kumulatiivisia. Tämä tarkoittaa, että käyttäjälle voidaan myöntää useita rooleja, ja niiden yhteisvaikutus määrittää, mitä käyttäjä voi tehdä. **Kieltäminen** (**Deny**)-sääntö on kuitenkin aina vahvempi kuin salliva sääntö, joten rajoituksia ei voi kiertää useilla rooleilla.

Azure RBAC tarjoaa erittäin tarkat ja skaalautuvat keinot hallita pilviresurssien käyttöä ja oikeuksia, tehden siitä oleellisen työkalun Azure-hallinnan arjessa. Lisätietoja ja ajantasaisia ohjeita löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/role-based-access-control/overview).


<a name="7"></a>
## 7. Johdanto "Infrastructure as a Code" -konseptiin

**Infrastructure as a Code** (**IaC**) on konsepti, jossa infrastruktuurin määrittelyt, konfiguraatiot ja resurssien väliset yhteydet tallennetaan koodina. Tämä mahdollistaa sen, että kuka tahansa organisaatiossa voi tarkastella, muokata ja ylläpitää ympäristöä koodipohjaisesti ilman, että toimenpiteitä tarvitsee tehdä suoraan esimerkiksi Azure-portaalissa. IaC:n avulla ympäristö voidaan myös luoda uudelleen alusta asti automaattisesti.

Konsepti tukeutuu vahvasti **Azure Resource Managerin** rajapintaan, jonka avulla voidaan olla vuorovaikutuksessa Azure Portalin kanssa, käyttää PowerShellia, Azure CLI:tä tai erilaisia REST Client -toteutuksia:

![alt_text](azure_-_resource_manager.svg)

Lisätietoja IaC:stä ja sen käytöstä löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/azure-resource-manager/templates/overview).

### 7.1 Mitä Infrastructure as a Code mahdollistaa?

IaC tarjoaa tehokkaita työkaluja suurtenkin ympäristöjen luomiseen ja hallintaan. Koodi, joka kuvaa resurssit ja niiden asetukset, voidaan ottaa käyttöön suoraan ympäristössä joko sisäänrakennetuilla ratkaisuilla tai ulkoisilla työkaluilla, kuten **Terraformilla**.

Azuren sisäänrakennettu ratkaisu IaC:n toteuttamiseen on **Azure Resource Manager** (**ARM**) **Templates**.

ARM Templates tallennetaan tyypillisesti **JSON**-muotoon, mutta Microsoft on myös kehittänyt **Bicep**-kielen, joka tarjoaa yksinkertaisemman syntaksin ja mahdollistaa modulaarisemman lähestymistavan IaC:n käyttöön. Bicep-tiedostot ovat pienempiä ja helpompia ylläpitää kuin perinteiset JSON-mallit.

### 7.2 ARM Templatesin edut

ARM Templatesin käytöllä on monia etuja infrastruktuurin hallinnassa:

- **Johdonmukaisuus ja uudelleenkäytettävyys**: ARM Templates varmistavat, että resurssit luodaan samalla tavalla jokaisessa käyttöönotossa, mikä vähentää inhimillisiä virheitä.
- **Manuaalisen työn vähentäminen**: Templates vähentävät toistuvia ja virheille alttiita manuaalisia tehtäviä, mikä nopeuttaa käyttöönottoa.
- **Monimutkaisten ympäristöjen hallinta**: Voit määritellä ja hallita monimutkaisia ympäristöjä koodilla ja automatisoida niiden käyttöönoton.
- **Validointi ja testaus**: ARM Templates tarjoavat työkalut, joilla voit validoida ja testata ympäristön ennen käyttöönottoa.
- **Modulaarisuus ja linkitys**: Templates ovat modulaarisia, ja niitä voidaan linkittää toisiinsa, mikä helpottaa suurten ympäristöjen orkestrointia.

### 7.3 ARM Templatesin käyttöprosessi

Kun käytät ARM Templatesia infrastruktuurin hallintaan, prosessi etenee tyypillisesti kolmessa vaiheessa:

1. **Kehitys**: Aluksi kehität ja määrittelet mallin, joka kuvaa ympäristön resurssit.
2. **Tuotanto**: Validoitu template otetaan käyttöön tuotantoympäristössä, ja resurssit luodaan automaattisesti.
3. **Laadunvarmistus**: Template tarkistetaan ja testataan ennen laajempaa käyttöönottoa laadunvarmistusvaiheessa.

### 7.4 Templatesin laajennus ja muokkaus

Yksi ARM Templatesin hyödyllisimmistä ominaisuuksista on mahdollisuus viedä olemassa olevat resurssiryhmät (RG) malliksi. Tämä mahdollistaa resurssien kopioinnin, muokkaamisen ja edelleenkehittämisen tulevaisuudessa.

Microsoft tarjoaa myös valmiita **QuickStart Templates** -malleja, joiden avulla voit helposti ottaa käyttöön uusia resursseja, kuten virtuaalikoneita. Näitä malleja löytyy osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/samples/browse/?expanded=azure&products=azure-resource-manager).
