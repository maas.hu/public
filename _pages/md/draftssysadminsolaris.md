# ![alt text](/_ui/img/icons/solaris_m.svg) Solaris

###### .

>Szerkesztés alatt...

## Tartalomjegyzék

1. [Bevezető](#1)
2. [Az OpenIndiana telepítése](#2)

<a name="1"></a>
## 1. Bevezető

A **Solaris** eredetileg a **Sun Microsystems** saját fejlesztésű operációs rendszere volt ez 1992-től 2010-ig. A Solaris elnevezésen kívül ismert volt még úgy is, mint **SunOS**. A 10-es verzióig főleg a Sun saját szerverein, **SPARC** *(Scalable Processor Architecture)* platformon volt ismertebb a rendszer, más környezetben nem igazán volt elterjedt, de már egészen a korai verzióitól kezdve kiadták x86-os platformra is. 

A Sun még 2007-ben elindította – eleinte a **Projekt Indiana** kódnév alatt – a nyílt forráskódú változatát is a rendszernek **OpenSolaris** néven, aminek az első stabil változata 2008-ban jelent meg. Ezzel így párhuzamosan fejlődhetett a rendszer közösségi, nyílt forrású változata és a zárt kódbázisú Solaris is enterprise rendszerek számára. 

2010-ben a Sun Microsystems-t felvásárolta az **Oracle**, ezzel az OS sorsa is nagyjából megpecsételődött. A 11-es verziót még kiadták, nem lett túlságosan elterjedt, továbbá az Oracle sem igyekezett karbantartani a kódbázist: hónapokig nem jöttek ki frissítések sem Solarishoz. Az OpenSolaris ágat is lezárta az Oracle és inkább a Solaris Express nevet adta neki a fejlesztési kiadások számára. 

Már 2010-ben, amikor a rendszer számos fejlesztőinek számára nyilvánvalóvá vált, hogy az Oracle nem hajlandó elegendő erőforrást fordítani a Solaris fenntartásához, gyorsan összeálltak néhányan, és forkolták a rendszert **OpenIndiana** néven. Ezzel egy különálló disztribúció jött létre az Oracle-től teljesen függetlenül. A rendszert napjainkban is fejlesztik, bővítik, jelenleg az *"OpenIndiana Hipster 2021.04 (powered by illumos)"* verzió az aktuális. A továbbiakban is ezzel a rendszerrel fogunk foglalkozni. 

<a name="2"></a>
## 2. Az OpenIndiana telepítése

...

![alt_text](oi-ins-01.png)

...

![alt_text](oi-ins-02.png)

...

![alt_text](oi-ins-03.png)

...

![alt_text](oi-ins-04.png)

...

![alt_text](oi-ins-05.png)

...

![alt_text](oi-ins-06.png)

...

![alt_text](oi-ins-07.png)

...

![alt_text](oi-ins-08.png)

...

![alt_text](oi-ins-09.png)

...

![alt_text](oi-ins-10.png)

...

![alt_text](oi-ins-11.png)

...

![alt_text](oi-ins-12.png)

...

![alt_text](oi-ins-13.png)

...

![alt_text](oi-ins-14.png)

...

![alt_text](oi-ins-15.png)

...

![alt_text](oi-ins-16.png)

...

![alt_text](oi-ins-17.png)

...

![alt_text](oi-ins-18.png)

...

![alt_text](oi-ins-19.png)

...

```ksh
root@oindicln1:~# grep -i permitroot /etc/ssh/sshd_config
PermitRootLogin no
root@oindicln1:~# vi /etc/ssh/sshd_config
root@oindicln1:~# grep -i permitroot /etc/ssh/sshd_config
PermitRootLogin yes
root@oindicln1:~# svcadm refresh network/ssh
```