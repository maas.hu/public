# 🗺️ Site Map

###### .

- 👨‍💻 [Programming](/prog)
    - ![alt text](/_ui/img/icons/bash_s.svg) [Bash, KSH](/prog/bash) 🇬🇧📣
    - ![alt text](/_ui/img/icons/python_s.svg) [Python](/prog/python) 🇬🇧📣
- ⚙️ [System Administration](/sysadmin)
    - ![alt text](/_ui/img/icons/babytux_s.svg) [Linux](/sysadmin/linux)
        - ![alt text](/_ui/img/icons/babytux_s.svg) [Storage](/sysadmin/linux/storage) 🇬🇧📣
        - ![alt text](/_ui/img/icons/babytux_s.svg) [Network and Security Basics](/sysadmin/linux/network) 🇬🇧📣
    - ![alt text](/_ui/img/icons/xen_s.svg) [Xen Project](/sysadmin/xen) 🇬🇧📣
    - ![alt text](/_ui/img/icons/qemu_s.svg) [QEMU](/sysadmin/qemu) 🇬🇧📣
    - ![alt text](/_ui/img/icons/azure_s.svg) [Microsoft Azure](/sysadmin/azure) 🔐
- 🖌️ [Computer Graphics and Multimedia](/multimedia)
    - 🪄 [ImageMagick](/multimedia/imagemagick) 🇬🇧
    - 🎵 [OGG Vorbis](/multimedia/ogg) 🇬🇧
    - 🎬 [Avidemux](/multimedia/avidemux) 🇬🇧
- 🤖 [Electronics](/electronics)
    - 📚 [Catalogues About Electronics](/electronics/catalogue)
        - 🪤 [Transistors](/electronics/catalogue/transistors) 🇬🇧
    - 📐 [Circuits / Wiring Diagrams](/electronics/circuits)
        - 🎄 [Christmas Lights](/electronics/circuits/christmas) 🇭🇺📣
        - 🧮 [Prototype Logic Gates](/electronics/circuits/gates) 🇬🇧📣
- 🙊 [Linguistics](/linguistics) 🇭🇺
    - ![alt text](/_ui/img/flags/fin_s.svg) [Finnish](/linguistics/finnish) 🇭🇺
    - ![alt text](/_ui/img/flags/rusyn_s.svg) [Rusyn](/linguistics/rusyn) 🇭🇺📣
- 📥 [Downloads, Links](/downloads)
- 📬 [Contact](/contact) 🇬🇧📣
- 📗 [About](/about) 🇬🇧

## Legend

- the 😶 emoji represents emptiness on that page but the content creation is planned
- the 📣 marking means so high level of readiness that there is already a possibility to leave comments
- the 🇭🇺 flag indicates the page is in Hungarian (temporary)
- the 🇬🇧 flagged pages are completely in English
- the 🔐 indicates the page and its occasional subpages are keyword protected
