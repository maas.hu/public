# ![alt text](/_ui/img/icons/babytux_m.svg) Linux Basics

<div style="text-align: right;"><h7>Posted: 2021-10-25 17:41</h7></div>

###### .

![alt_text](linux_-_basics_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Files and Permissions](#2)
3. [Scheduling Jobs](#3)
4. [Boot and System Services](#4)
5. [Package Managers](#5)
6. [Managing Processes and Threads](#6)
7. [Limits](#7)
8. [Auditing](#8)


<a name="1"></a>
## 1. Introduction

...


<a name="2"></a>
## 2. Files and Permissions

...


<a name="3"></a>
## 3. Scheduling Jobs

Before doing any investigations, **check the date and time first** if they are correct. If it's possible, sync with the NTP servers. If not, here are the quick commands for **setting and checking** the system date and time: 

```ksh
# date
Fri Oct 22 11:37:00 CEST 2021

# date +%Y%m%d -s "20211025"
20211025

# date
Mon Oct 25 00:00:03 CEST 2021

# date +%T -s "09:07:30"
09:07:30

# date
Mon Oct 25 09:07:32 CEST 2021
```

### 3.1 System default jobs

The traditional way to schedule a job is to use crontab. The following are the the system default jobs, but you can also set up any jobs in this hierarchy: 

```ksh
# ls -la /etc | grep cron
-rw-r--r--.   1 root root       541 Jun 12  2019 anacrontab
drwxr-xr-x.   2 root root      4096 Jun 12  2019 cron.d
drwxr-xr-x.   2 root root      4096 May 27 17:52 cron.daily
-rw-r--r--.   1 root root         0 Jun 12  2019 cron.deny
drwxr-xr-x.   2 root root      4096 Jan  8  2021 cron.hourly
drwxr-xr-x.   2 root root      4096 Jan  8  2021 cron.monthly
-rw-r--r--.   1 root root       490 Oct  1  2019 crontab
drwxr-xr-x.   2 root root      4096 Jan  8  2021 cron.weekly

# cat /etc/anacrontab
[...]
1       5       cron.daily              nice run-parts /etc/cron.daily
7       25      cron.weekly             nice run-parts /etc/cron.weekly
@monthly 45     cron.monthly            nice run-parts /etc/cron.monthly

# ls -la /etc/cron.daily
total 20
drwxr-xr-x.   2 root root  4096 May 27 17:52 .
drwxr-xr-x. 122 root root 12288 Aug 18 10:54 ..
-rwxr-xr-x.   1 root root   189 Jan  4  2018 logrotate
```

<div class="info">
<p>There is also another, a more modern, easy to track solution to set up schedules on Linux: the <b>SystemD Timer</b>. You can find more details about it in the chapter about <a href="#4">Boot and System Services</a>.</p>
</div>

### 3.2 `crontab`

Under almost any Unix-like systems, the most traditional way to keep cron jobs per user is to store them under `/var/spool/cron` and Linux is not an exception either. (As a comparison, on AIX, these are under an additional folder, under `/var/spool/cron/crontabs`.)

You can list the default user's cronjobs by `crontab -l`, while you can edit them by `crontab -e`. 

Explanations for the fields: 

```
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * [user-name] command to be executed
 10  1  *  *  0 /usr/local/bin/weeklyscript.sh
 30  2  *  *  * /usr/local/bin/dailyscript.sh HU ; /usr/local/bin/dailyscript.sh SK
```

Some examples for special schedules (the first one is universal: even e.g. AIX supports it, while the second one is GNU/Linux-specific, allowing more flexibility): 

```ksh
0 6,18 * * * /usr/local/bin/twiceadayscript.sh
*/10 * * * * /usr/local/bin/everytenminutesscript.sh
```

If a cronjob has any output or error message, the cron daemon sends an email to the affected user. Usually, it's not needed, so it's a common practice to disable such messaging completely: 

```ksh
10 1 * * 0 /usr/local/bin/weeklyscript.sh >/dev/null 2>&1
```

It's also usual that you do log rotation by a single oneliner in the crontab, using the `find` command. However, if you use the the `-exec` parameter for it, you may need an extra `\` character, which is usually not needed under Linux, but better to test it first, e.g.:

```ksh
0 7,14 * * * /usr/bin/find /logs/wmqcml_ma0z -type f ! -perm -g+r,g+w -exec chmod g+rw {} \\; >/dev/null 2>&1
```

### 3.2.1 Controlling the access to Cron

There are no further access control-related entries under `/etc/cron.d` directories by default, but you can even improve security by them if needed: 

- if neither `cron.allow` nor `cron.deny` files are present, only superuser is allowed to run the `crontab` command
- if there's `cron.allow`, then only its listed users are allowed to create, edit, list or remove (shortly: submit) cron files
- if there's `cron.deny`, but there's no `cron.allow`, then only the listed users are not allowed to submit cron files.

Typical example for `cron.deny`: 

```ksh
# cat /etc/cron.d/cron.deny
daemon
bin
smtp
nuucp
listen
nobody
noaccess
```

### 3.3 `at`

It's the traditional solution to schedule a single, scheduled job. In order to use it, the system must run the `atd` daemon already. By default, the `at` package is not installed that would ship the `atd` too. 

The tool is very flexible accepting date specifications. Here's a simple example for scheduling a job for 10:53am on the same day. The list of commands can be just entered one after the other, then you can close the list by `[Ctrl+D]`: 

```ksh
# date
Mon Oct 25 10:51:19 CEST 2021

# at 1053
warning: commands will be executed using /bin/sh
at> echo "hello" > /tmp/hello.txt
at> <EOT>
job 2 at Mon Oct 25 10:53:00 2021

# atq
2       Mon Oct 25 10:53:00 2021 a root
```

Running a job on a specific date and time: 

```ksh
# at 0848 Apr 19
[...]
```

Examples for text-based schedules: 

```ksh
# at 1730 tomorrow
[...]
The job will be run at 17:30 on the next day. 

# at teatime
[...]
The job will be executed today at 16:00. 
```

It's important to use full path both for `crontab` and `at`, e.g.: 

```ksh
# which userdel
/usr/sbin/userdel

# at 1730 tomorrow
at> /usr/sbin/userdel -r dbauser
[...]
```

Further text-based schedules, accepted by `at`:

```
noon, midnight, tomorrow, noon tomorrow, next week, next monday, fri ...
```

Control commands:

- listing `at` jobs: `atq`
- removing `at` jobs: `atrm <jobID>`


<a name="4"></a>
## 4. Boot and System Services

The **SysV init** (`service` and `runlevel` commands, and also the `/etc/inittab` file) became obsolete and completely replaced by **SystemD** by the beginning of '10s. Accordingly, the main control command for Linux services is `systemctl`.

### 4.1 Runlevels

Starting with RHEL8, installing the system using the "minimal" ISO sets up the `multi-user.target` as default runlevel, even though when we install GUI packages later. You can still use the traditional `startx` command to start the GUI, but you can also switch to the respective runlevel. 

Comparison of the main Linux runlevels and their SystemD representatives: 

<table class="tan">
  <tr>
    <th>SysV init</th><th>Systemd</th><th>Function</th>
  </tr>
  <tr>
    <td>0</td>
    <td>poweroff.target</td>
    <td>System is shut down</td>
  </tr>
  <tr>
    <td>1</td>
    <td>rescue.target</td>
    <td>Single user mode / Rescue mode</td>
  </tr>
  <tr>
    <td>3</td>
    <td>multi-user.target</td>
    <td>Multi-user mode: logging is possible by multiple user + networking</td>
  </tr>
  <tr>
    <td>5</td>
    <td>graphical.target</td>
    <td>Multi-user mode + GUI</td>
  </tr>
  <tr>
    <td>6</td>
    <td>reboot.target</td>
    <td>Rebooting the system</td>
  </tr>
</table>

Setting the default runlevel from multi-user to graphical: 

```ksh
# systemctl get-default
multi-user.target

# systemctl set-default graphical.target
Removed /etc/systemd/system/default.target.
Created symlink /etc/systemd/system/default.target → /usr/lib/systemd/system/graphical.target.
```

Checking by the `who` command: 

```ksh
$ who -r
         run-level 5  2020-11-07 20:46
```

Listing all the known runlevels: 

```ksh
# systemctl list-units --type target --all --no-page
  UNIT                      LOAD      ACTIVE   SUB    DESCRIPTION                
  basic.target              loaded    active   active Basic System               
[...]
  emergency.target          loaded    inactive dead   Emergency Mode             
[...]
  graphical.target          loaded    active   active Graphical Interface        
[...]
  multi-user.target         loaded    active   active Multi-User System          
[...]
  rescue.target             loaded    inactive dead   Rescue Mode                
  shutdown.target           loaded    inactive dead   Shutdown                   
[...]
```

You can switch between the runlevels by the `systemctl` command. Traditionally, it was done by the `init`, and many-many sysadmins preferred using `init 6` instead of `reboot` command. Therefore, sometimes it's still possible, because of these symlinks, and because `systemd` is prepared to receive that call: 

```ksh
# ls -lai /sbin/init /usr/sbin/init
1061832 lrwxrwxrwx 1 root root 22 Nov  2 21:59 /sbin/init -> ../lib/systemd/systemd
1061832 lrwxrwxrwx 1 root root 22 Nov  2 21:59 /usr/sbin/init -> ../lib/systemd/systemd
```

A possible new way to reboot a system: 

```ksh
# systemctl start reboot.target
```

### 4.2 SystemD Units

By default, the `systemctl` tool lists a complex and long sheet, also using a pager by default, so it's hard to search in the list. The following command lists only the controllable services and their current state, without pager: 

```ksh
# systemctl list-unit-files --type=service --no-page
UNIT FILE                                  STATE   
accounts-daemon.service                    enabled 
arp-ethers.service                         disabled
atd.service                                enabled 
[...]
```

Searching in this list is much easier already, e.g.:

```ksh
# systemctl list-unit-files --type=service --no-page | grep -i -E "gst|networker"
gst.service                                   enabled
networker.service                             enabled
```

Turning services off/on, re-reading their configuration (`reload`), enabling/disabling them for system reboot: 

```ksh
# systemctl <start|stop|reload|restart|enable|disable|...> <service1> <serviceN>
```

For example, you can control multiple services at once: 

```ksh
# systemctl disable gst networker
```

#### 4.2.1 Location of `.service` files

The SystemD configuration files (both the Units and the Timers) can be set up either: 

- under the `/etc/systemd/system` directly (less preferred for custom Units and Timers)
- under the `/usr/lib/systemd/system` (preferred)

#### 4.2.2 Further useful features

Besides the long `systemctl status <unitname>` query (which can be issued as a non-privileged user too), you can just use the `is-active` option to get a quick and straightforward status. Also, it's alrady provides different exit codes depending on the status that you can leverage in your scripts: 

```ksh
$ systemctl is-active my-custom.service
active

$ echo $?
0

$ systemctl is-active my-custom.service
inactive

$ echo $?
3
```

### 4.3 The SystemD Timer

Listing all the existing, **non inactive** timers: 

```ksh
# systemctl list-timers -all
NEXT                         LEFT              LAST                         PASSED            UNIT                         ACTIVATES                     
[...]
Mon 2022-10-24 00:17:05 EEST 7h left           Sun 2022-10-23 14:34:44 EEST 2h 28min ago      check-battery.timer          check-battery.service
[...]
12 timers listed.
```

Status of **all** timers: 

```ksh
# systemctl status *timer 
[...]
```

Preparing a simple `systemd` timer service and execute it: 

```ksh
# cat > /etc/systemd/system/freemem.service
[Unit]
Description=Logs system statistics to the systemd journal
Wants=freemem.timer

[Service]
Type=oneshot
ExecStart=/usr/bin/free

[Install]
WantedBy=multi-user.target
^D

# systemctl status freemem.service
○ freemem.service - Logs system statistics to the systemd journal
     Loaded: loaded (/etc/systemd/system/freemem.service; disabled; preset: disabled)
     Active: inactive (dead)

# systemctl start freemem.service

# journalctl -u freemem.service
Oct 23 16:55:21 H92VFK3 systemd[1]: Starting Logs system statistics to the systemd journal...
Oct 23 16:55:21 H92VFK3 free[7972]:                total        used        free      shared  buff/cache   available
Oct 23 16:55:21 H92VFK3 free[7972]: Mem:        16099100     6208224     6896900     1052768     4370196     9890876
Oct 23 16:55:21 H92VFK3 free[7972]: Swap:       16777212           0    16777212
Oct 23 16:55:21 H92VFK3 systemd[1]: freemem.service: Deactivated successfully.
Oct 23 16:55:21 H92VFK3 systemd[1]: Finished Logs system statistics to the systemd journal.
```

The `Wants` parameter is the one which would invoke the respective `.timer` pair of the setup, but as you can see above, the `.service` is successful even if the `.timer` isn't there yet.

### 4.3.1 Example for creating a standard `.service` and a `.timer`

1. Prepare a shell script if the command to be executed is a bit complex: 

```ksh
# cat /usr/local/bin/battery_health.sh 
#!/bin/sh
#
for i in $(/usr/bin/upower -e | grep battery)
  do
    echo $i:
    /usr/bin/upower -i $i | grep -E 'vendor|model|percentage|capacity'
    echo
  done

# ls -l /usr/local/bin/battery_health.sh
-rwxr-xr-x 1 root root 163 Oct 23 17:38 /usr/local/bin/battery_health.sh

# /usr/local/bin/battery_health.sh
/org/freedesktop/UPower/devices/battery_BAT0:
  vendor:               SWD-ATL4.175
  model:                DELL 1PP6318
    percentage:          100%
    capacity:            87.4417%
```

2. Create the `.service` file: 

In this example, this is `/usr/lib/systemd/system/battery-health.service`: 

```
[Unit]
Description=Battery status and health
Wants=battery-health.timer

[Service]
ExecStart=/usr/local/bin/battery_health.sh
KillMode=mixed

[Install]
WantedBy=multi-user.target
```

3. Create the `.timer` file: 

In this example, this is `/usr/lib/systemd/system/battery-health.timer`: 

```
[Unit]
Description=Battery status and health
Requires=battery-health.service

[Timer]
OnCalendar=daily
AccuracySec=1h
RandomizedDelaySec=2h
Persistent=true

[Install]
WantedBy=timers.target
```

4. Verify (this is just a warning): 

```ksh
# systemd-analyze verify /usr/lib/systemd/system/battery-health.*
/usr/lib/systemd/system/plymouth-start.service:15: Unit configured to use KillMode=none. This is unsafe, as it disables systemd's process lifecycle management for the service. Please update your service to use a safer KillMode=, such as 'mixed' or 'control-group'. Support for KillMode=none is deprecated and will eventually be removed.
```

5. Start and check results: 

```ksh
# systemctl start battery-health.service

# systemctl status battery-health.service
○ battery-health.service - Battery status and health
     Loaded: loaded (/usr/lib/systemd/system/battery-health.service; enabled; preset: disabled)
     Active: inactive (dead) since Sun 2022-10-23 17:46:18 EEST; 1s ago
   Duration: 14ms
TriggeredBy: ● battery-health.timer
    Process: 9678 ExecStart=/usr/local/bin/battery_health.sh (code=exited, status=0/SUCCESS)
   Main PID: 9678 (code=exited, status=0/SUCCESS)
        CPU: 14ms
[...]
Oct 23 17:46:18 H92VFK3 systemd[1]: Started Battery status and health.
Oct 23 17:46:18 H92VFK3 battery_health.sh[9678]: /org/freedesktop/UPower/devices/battery_BAT0:
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:   vendor:               SWD-ATL4.175
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:   model:                DELL 1PP6318
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:     percentage:          100%
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:     capacity:            87.4417%

# journalctl -u battery-health.service
[...]
Oct 23 17:46:18 H92VFK3 battery_health.sh[9678]: /org/freedesktop/UPower/devices/battery_BAT0:
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:   vendor:               SWD-ATL4.175
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:   model:                DELL 1PP6318
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:     percentage:          100%
Oct 23 17:46:18 H92VFK3 battery_health.sh[9685]:     capacity:            87.4417%
Oct 23 17:46:18 H92VFK3 systemd[1]: battery-health.service: Deactivated successfully.
```

6. Enable the service and the timer: 

```ksh
# systemctl enable battery-health.service
Created symlink /etc/systemd/system/multi-user.target.wants/battery-health.service → /usr/lib/systemd/system/battery-health.service.

# systemctl enable battery-health.timer
Created symlink /etc/systemd/system/timers.target.wants/battery-health.timer → /usr/lib/systemd/system/battery-health.timer.
```

Some more notes on configuring the schedule and the job: 

- If you want to apply a **simple to low complexity one-liner**, maybe even including redirection, for `ExecStart`, then you don't need to keep a separated `.sh` script. In that case, you should consider invoking it like this: 

```
ExecStart=/bin/bash -c "echo $(date) >> /tmp/buffer-cache-cleanup-test"
```

### 4.3.2 More on SystemD Timer Parameters

The most important parameter `OnCalendar` is like this: 

```
DayOfWeek Year-Month-Day Hour:Minute:Second
```

- `OnCalendar` is using `daily` by default to run the service at midnight. However, for more flexibility, the...
- `RandomizedDelaySec` (e.g. `RandomizedDelaySec=1h`) instructs the `systemd` to choose a launch at a random time within the specified timeslot of the `On`. `RandomizedDelaySec` can be essential if you have many timers running with `OnCalendar=daily`.
- `OnCalendar=daily`: Defines realtime (i.e. wallclock) timers with calendar event expression daily. which effectively means that it will run at midnight every night.
- `Persistent=true`: means "The time when the service unit was last triggered is stored on disk. When the timer is activated, the service unit is triggered immediately if it would have been triggered at least once during the time when the timer was inactive. Such triggering is nonetheless subject to the delay imposed by RandomizedDelaySec=. This is useful to catch up on missed runs of the service when the system was powered down."
- `OnStartupSec=60m` defines a timer relative to when the service manager was first started.
- `RandomizedDelaySec=60m` Delays the timer by a randomly selected, evenly distributed amount of time between 0 and 60m. Each timer unit will determine this delay randomly before each iteration, and the delay will simply be added on top of the next determined elapsing time. This setting is useful to stretch dispatching of similarly configured timer events over a certain time interval, to prevent them from firing all at the same time, possibly resulting in resource congestion.

Some examples for `OnCalendar` definition:

- `OnCalendar=*-*-* *:*:00` - every minutes,
- `OnCalendar=*:0/15` - every 15 minutes,
- `OnCalendar=*:45` - every 45 minutes,
- `OnCalendar=*-*-* *:00:00` - hourly,
- `OnCalendar=Sat *-*-01,02,03,04,05,06,07,15,16,17,18,19,20,21 1:00:00` - bi-weekly, in every 2nd Saturdays (the `crontab` equivalent of the same challenge was a bit easier anyway: `0 1 1-7,15-21 * sat`)

More on time and date specifications can be read [here](https://www.freedesktop.org/software/systemd/man/latest/systemd.time.html#).

Unfortunately, the following format is still just a [suggestion for enhancement](https://github.com/systemd/systemd/issues/6024): 

```
[Timer]
OnCalendar=2023-11-18
RepeatEveryUnit=week
RepeatEveryN=2
Persistent=true
```

### 4.3.3 More on `systemd-analyze`

Besides `systemd-analyze verify`, you can also use the tool's calendar to decode your `OnCalendar` definition, e.g.:

```ksh
# systemd-analyze calendar Sat *-*-01,02,03,04,05,06,07,15,16,17,18,19,20,21 1:00:00
  Original form: Sat
Normalized form: Sat *-*-* 00:00:00
    Next elapse: Sat 2023-11-18 00:00:00 EET
       (in UTC): Fri 2023-11-17 22:00:00 UTC
       From now: 1 day 6h left

  Original form: *-*-01,02,03,04,05,06,07,15,16,17,18,19,20,21
Normalized form: *-*-01,02,03,04,05,06,07,15,16,17,18,19,20,21 00:00:00
    Next elapse: Fri 2023-11-17 00:00:00 EET
       (in UTC): Thu 2023-11-16 22:00:00 UTC
       From now: 6h left

  Original form: 1:00:00
Normalized form: *-*-* 01:00:00
    Next elapse: Fri 2023-11-17 01:00:00 EET
       (in UTC): Thu 2023-11-16 23:00:00 UTC
       From now: 7h left
```

### 4.4 The systemd-journald service

The core service behind `systemd`'s logging is `systemd-journald.service`. The logs of this service do not persist across reboots, but it [can be solved](https://www.redhat.com/sysadmin/store-linux-system-journals) by a custom configuration for it: 

1. Create the `/var/log/journal` directory.

2. Set the parameters for using it persistently in `journald.conf`: 

```ksh
# vi /etc/systemd/journald.conf
[Journal]
Storage=persistent
[...]
```

3. Restart `systemd-journald.service`

#### 4.4.1 Reading the journal logs

There are different ways to do that. Here are some examples: 

```ksh
$ sudo journalctl --since=yesterday
-- Logs begin at Tue 2023-06-06 11:40:01 EEST, end at Tue 2023-06-06 14:09:12 EEST. --
[...]

$ sudo journalctl --since="2023-06-06 12:45:00" --no-page | grep performance-4 | less
[...]

$ sudo journalctl _PID=2770183
-- Logs begin at Tue 2023-06-06 12:15:02 EEST, end at Tue 2023-06-06 14:51:32 EEST. --
[...]
```

And the classic one, e.g.: 

```ksh
$ sudo journalctl -u my_custom.service --no-page
-- Logs begin at Tue 2023-06-06 12:33:32 EEST, end at Tue 2023-06-06 15:07:17 EEST. --
[...]
```

#### 4.4.2 Setting debug logs for `systemd-analize`

Some articles for it: 
- [How can I enable systemd debug logging while the system is online?](https://access.redhat.com/solutions/2788121)
- [How to display more verbose boot-related messages during system startup?](https://access.redhat.com/solutions/28921)
- [systemd-analyze's manpage](https://www.freedesktop.org/software/systemd/man/systemd-analyze.html)
- get boot time:
```ksh
$ systemd-analyze time
Startup finished in 4.781s (kernel) + 7.660s (initrd) + 27.933s (userspace) = 40.375s
multi-user.target reached after 17.638s in userspace
```
- querying and setting the log level:
```ksh
$ sudo systemd-analyze get-log-level
info

$ sudo systemd-analyze set-log-level debug

$ sudo systemd-analyze get-log-level
debug
```

### 4.5 The GRUB2 Config

Simple change for GRUB2 Config, e.g.:

```ksh
# vi /etc/default/grub
[...]
GRUB_INIT_TUNE="480 440 1"
[...]

# grub2-mkconfig -o /boot/grub2/grub.cfg
Generating grub configuration file ...
[...]
done
```

Changing the **default menu entry**, e.g.: 

```ksh
# grep menuentry /boot/grub2/grub.cfg | awk -F'--class' '{print $1}'
if [ x"${feature_menuentry_id}" = xy ]; then
  menuentry_id_option="--id"
  menuentry_id_option=""
export menuentry_id_option
menuentry 'openSUSE Tumbleweed'  
submenu 'Advanced options for openSUSE Tumbleweed' --hotkey=1 $menuentry_id_option 'gnulinux-advanced-3f31e844-43e9-44ec-a93d-34a2b0ef7e8b' {
	menuentry 'openSUSE Tumbleweed, with Linux 5.19.7-1-default' --hotkey=2 
	menuentry 'openSUSE Tumbleweed, with Linux 5.19.7-1-default (recovery mode)' --hotkey=3 
	menuentry 'openSUSE Tumbleweed, with Linux 5.19.1-1-default'  
	menuentry 'openSUSE Tumbleweed, with Linux 5.19.1-1-default (recovery mode)' --hotkey=1 
menuentry 'openSUSE Tumbleweed, with Xen hypervisor' 
submenu 'Advanced options for openSUSE Tumbleweed (with Xen hypervisor)' $menuentry_id_option 'gnulinux-advanced-3f31e844-43e9-44ec-a93d-34a2b0ef7e8b' {
	submenu 'Xen hypervisor, version 4.16.2_04-2' $menuentry_id_option 'xen-hypervisor-4.16.2_04-2-3f31e844-43e9-44ec-a93d-34a2b0ef7e8b' {
		menuentry 'openSUSE Tumbleweed, with Xen 4.16.2_04-2 and Linux 5.19.7-1-default' 
		menuentry 'openSUSE Tumbleweed, with Xen 4.16.2_04-2 and Linux 5.19.7-1-default (recovery mode)' 
		menuentry 'openSUSE Tumbleweed, with Xen 4.16.2_04-2 and Linux 5.19.1-1-default' 
		menuentry 'openSUSE Tumbleweed, with Xen 4.16.2_04-2 and Linux 5.19.1-1-default (recovery mode)' 

# grub2-editenv list
saved_entry=openSUSE Tumbleweed

# grep GRUB_DEFAULT /etc/default/grub
GRUB_DEFAULT=saved

# grub2-set-default "openSUSE Tumbleweed, with Xen hypervisor"

# grub2-editenv list
saved_entry=openSUSE Tumbleweed, with Xen hypervisor
```


<a name="5"></a>
## 5. Package Managers

...

### 5.1 RPM

...

#### 5.1.2 What requires what

...

```ksh
$ rpm -q --whatrequires ipa-server
ipa-healthcheck-0.12-3.module+el8.9.0+19634+c162f948.noarch
ipa-server-dns-4.9.12-9.module+el8.9.0+20420+fef9eb45.noarch
```

#### 5.1.3 DNF Modules

Read through [this article](https://docs.fedoraproject.org/en-US/modularity/using-modules/) about it.

...

### 5.2 DEB

...


<a name="6"></a>
## 6. Managing Processes and Threads

...


```ksh
$ ffor PID in $(ps -fu root --no-headers | awk '{print $2}') ; do ps --no-headers -o nlwp $PID ; done | awk '{sum +=$1} END {print sum}'
234
```

...


<a name="7"></a>
## 7. Limits

...


<a name="8"></a>
## 8. Auditing

The main config, the main `.rules` and `stop.rules`, and the additional rules: 

```ksh
$ sudo ls -la /etc/audit/auditd.conf /etc/audit/audit.rules /etc/audit/audit-stop.rules /etc/audit/rules.d
-rw-r-----. 1 root root  718 Feb  2  2021 /etc/audit/auditd.conf
-rw-r-----. 1 root root 3043 Aug 19  2019 /etc/audit/audit.rules
-rw-r-----. 1 root root  127 Oct 30  2018 /etc/audit/audit-stop.rules

/etc/audit/rules.d:
total 12
drwxr-x---. 2 root root   25 Feb  2  2021 .
drwxr-x---. 3 root root 4096 Feb  2  2021 ..
-rw-------. 1 root root 5061 Feb  2  2021 audit.rules
```

At least the buffer can be adjusted in `/etc/audit/audit.rules` and in `/etc/audit/rules.d/audit.rules`: 

```
[...]
# Increase the buffers to survive stress events.
# Make this bigger for busy systems
-b 320
[...]
```

The service is `auditd.service` and you can see what is effective by these commands: 

```ksh
$ sudo auditctl -l
-a always,exit -F arch=b64 -S adjtimex,settimeofday -F key=time-change
-a always,exit -F arch=b32 -S stime,settimeofday,adjtimex -F key=time-change
-a always,exit -F arch=b64 -S clock_settime -F key=time-change
-a always,exit -F arch=b32 -S clock_settime -F key=time-change
-w /etc/localtime -p wa -k time-change
[...]

$ sudo auditctl -s
enabled 2
failure 1
pid 2780
rate_limit 0
backlog_limit 320
lost 48
backlog 0
loginuid_immutable 0 unlocked
```

Mostly, NO any operation is permitted without **reboot**. See [this article](https://www.cyberithub.com/how-to-define-audit-control-rules-in-rocky-linux-8/) for more details.
