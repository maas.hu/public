# ![alt text](/_ui/img/icons/win_m.svg) Windows

<div style="text-align: right;"><h7>Posted: 2020-09-21 17:21</h7></div>

###### .

![alt_text](windows_-_bg_-_bliss.png)


## Table of Contents

1. [Introduction](#1)
2. [Getting Comfortable with Windows 11](#2)
3. [Useful Queries](#3)


<a name="1"></a>
## 1. Introduction

**Microsoft Windows**, first released in **1985**, was created to address the limitations of **MS-DOS**, which, while widely used, offered no Graphical User Interface (**GUI**). The idea for this GUI came from **Bill Gates and Paul Allen**, who recognized the need for a more user-friendly way to interact with computers. The initial development of Windows was led by a small team at Microsoft, with key contributions from developers like Bill Gates, and later, industry veterans such as **Terry Myerson and Joe Belfiore**. Windows 1.0 was a modest start, simply layering a GUI over MS-DOS, but it marked the beginning of a new era. 

As Windows evolved, so did its impact. **Windows 95**, released in **1995**, introduced groundbreaking features like the **Start menu** and **plug-and-play** hardware support. **Windows XP**, launched in **2001**, became iconic for its stability and wide adoption. Later versions, like **Windows 7** and **Windows 8**, reflected shifts in both design and user expectations, particularly with the rise of mobile computing. **Windows 10** aimed to create a seamless experience across devices, and the latest version, **Windows 11**, brings a modern, streamlined design with a focus on performance and new technologies. Today, Microsoft Windows is the most widely used operating system globally, at least on the desktop segment, continuing to evolve to meet the demands of a fast-changing tech landscape.

This article doesn’t aim to provide detailed guides on how to install, configure, or set up Windows for everyday use. Instead, it focuses on highlighting some of the lesser-known, yet highly useful features that are often overlooked. These tips and tricks will help you make the most out of your Windows experience by uncovering tools and functions that aren't immediately obvious but can greatly enhance your productivity and overall user experience.


<a name="2"></a>
## 2. Getting Comfortable with Windows 11

### 2.1 Use a local user

[Win+R] → `netplwiz` → ...

https://www.howtogeek.com/838506/how-to-sign-into-your-windows-11-pc-automatically/

### 2.2 Re-align Taskbar, along with Start button

![alt_text](win11_-_default_taskbar.png)

Personalization → Taskbar → Taskbar behaviors → Taskbar alignment → Left

Optionally, if you don't like having the Search box on your Task bar either, you can hide it by Taskbar items → Search → Hide

Also here, if you prefer more legacy look but only for the Taskbar: Colors → Choose your mode → Custom →

- Choose your default Windows mode: Dark
- Choose your default app mode: Light

### 2.3 Disable automatic updates

...

https://www.tomsguide.com/computing/how-to-turn-off-automatic-updates-in-windows-11

#### 2.3.1 Simple pausing for a period of time (up to 5 weeks)

Setting → Windows Update → 

#### 2.3.2 How to permanently disable automatic updates?

[Win+R] → `services.msc` → Windows Update → select "Disabled" for Startup type.


<a name="3"></a>
## 3. Useful Queries



---


## Old notes (to be re-phrased and re-organized)

## 2. Serial number

A PC serial number-ének kiírása: 

```ksh
> wmic bios get serialnumber
SerialNumber
ABCDE12345
```


## 3. 32bit? 64bit?

Eltekintve attól, hogy maga a Windows 32 vagy 64 bites, annak megállapítása, hogy egy program `.exe`-je 32 vagy 64 bites-e szintén egész egyszerű. 

A vizsgálandó `.exe` fájlt nyisd meg egy sima text nézőkében (pl. Total Commander nézőkéje), és keresd az alábbi karaktersort a kód elején: 

32 bit:  
![alt_text](bit-32.png "32 bites")

64 bit:  
![alt_text](bit-64.png "64 bites")


## 4. Automatikus indítás

Egy program automatikusan elindulóvá tétele: 

1. [Win]+[R] gombok megnyomása → `shell:startup`  
Ez meg fog nyitni egy Windows Intézőt (File Explorer) a megfelelő könyvtárban. 
2. Másolj ide egy parancsikont az indítani kívánt programból. A parancsikon tulajdonságainál akár paramétereket is megadhatsz. 
3. Ellenőrizd a végeredményt: [Win] → "Startup Apps"


## 5. Hálózati meghajtó

Hálózati meghajtó csatlakoztatása, leválasztása: 

```ksh
> net use k: \\domain\directory
...

> net use k: /delete
K: was deleted successfully.
```


## 6. Felhasználói információk

Domain felhasználó esetén kell csak a `/domain` a végére: 

```ksh
> net user %USERNAME% /domain
The request will be processed at a domain controller for domain abs.sk.

User name                    JakabG
Full Name                    Gipsz Jakab (Company)
Comment                      REQ000000012345
User's comment
Country/region code          (null)
Account active               Yes
Account expires              Never

Password last set            2020. 09. 25. 8:03:10
Password expires             2020. 12. 24. 8:03:10
Password changeable          2020. 09. 26. 8:03:10
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   2020. 12. 21. 17:30:07
...
```


## 7. Processzek

Processzek kezelése terminálból

```ksh
> tasklist | find "webex"
webexmta.exe                 12984 Console                    1     22 492 K

> taskkill /f /im webexmta.exe
SUCCESS: The process "webexmta.exe" with PID 12984 has been terminated.
```


## 8. Uptime és Fast Startup

Jobb gomb a tálcán → Task Manager → More details (ha nem lenne bekapcsolva) → Performance → CPU: 

![alt_text](win10-uptime.png "Task Manager: uptime")

Parancssorból ugyanez: 

```ksh
> systeminfo | find /i "Boot Time"
System Boot Time:          2021. 11. 10., 10:58:02
```

Ha valami irtózatosan régi időt ír itt a Windows, az a **Fast Startup** miatt van: az OS alapértelmezés szerint már soha nem áll le teljesen, hanem részben hibernálódik. 

Rábírni a Windows-t, hogy "cold shutdown"-t csináljon, ezáltal a következő boot során veszítsük inkább el a Fast Startup előnyeit (lassabban indul), az alábbi módon tudod: 

```ksh
> shutdown /s /t 0
```

További infó erről [itt](https://www.winhelponline.com/blog/incorrect-uptime-taskmgr-wmi-refresh/) olvasható. 

Véglegesen itt lehet ezt kikapcsolni (admin módban): 

![alt_text](fastboot-on_off.png "Fast Startup")


## 9. Checksum

Egy nagyobbacska fájl MD5 vagy SHA1 "ellenőrző összegének" ellenőrzésére a leggyorsabb eszköz a Microsoft saját FCIV *(File Checksum Integrity Verifier)* nevű toolja. Letölthető a MS [oldaláról](https://www.microsoft.com/en-us/download/details.aspx?id=11533). 

Példa a használatára: 

```ksh
> dir 7.1.0.40-663551.rpm
 Volume in drive X is 1st
 Volume Serial Number is F0F3-1F0D

 Directory of x:\Install\Server SW\DDOS

2020. 11. 06.  20:06      2 469 577 433 7.1.0.40-663551.rpm
               1 File(s)  2 469 577 433 bytes
               0 Dir(s)   93 102 899 200 bytes free

> fciv.exe 7.1.0.40-663551.rpm -md5
//
// File Checksum Integrity Verifier version 2.05.
//
8c1911665f8e2958b0f644947333506f 7.1.0.40-663551.rpm
```

Természetesen a Total Commander is tudja ezt, és még további számos fajta checksum-fájl legyártását.
