# 🍊 Orange Pi 4 LTS - Additional Notes

## Heatsinks to order

### Orange Pi 4 LTS

- RK3399: outside: 21 x 21 mm; inside: 16.4 x 16.4 mm --> 19 x 19 x 24 mm
- RAM: two 14.5 x 10 mm
- eMMC: 12 x 10 mm
- Wifi: 0.8 x 0.8 x 0.5 cm (remains the same as now)

#### Orange Pi 5

- RK3588: 18 x 18 mm --> 
- RAM: two 14.5 x 10 mm
- eMMC: 12 x 10 mm


## Plans with this device

- **Testing OS re-installation methods**
    - **OrangePi's official releases and Armbian images** 🟢
    - **any official RPM-based distro installer (Rocky, Fedora, openSUSE)** 🔴  
      (the boot method is tricky and not UEFI compliant, so I haven't found the right method yet)
- **GitLab CE Server**: preparations, installation, configuration, login, initial settings, some testing 🟢
- **Installing and testing Node.js** from official release 🟢
- **Compiling and testing Node.js from source** 🟢
- **Compiling and testing Raku from source** 🟢
- **Installing and testing Xen** (creating at least one VM) 🟡


## OS struggle

- Official installers cannot be booted and other custom images cannot be created because *"There's some u-boot work to let it function as a UEFI abstraction layer."* ([link](https://forum.armbian.com/topic/10599-uefi-for-orange-pi/))

- https://uthings.uniud.it/building-mainline-u-boot-and-linux-kernel-for-orange-pi-boards


1. Downloaded the `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.7z` file from Orange Pi's official site, from their [Google Drive](https://drive.google.com/drive/folders/1W0Bm-GGvVqgiDeSdEy_PSGlA23ahMGC6). 

2. Uncompressed it with `7z e ...`

3. Downloaded their `balenaEtcher` tool (it is not crucial as `dd` does the same, and the SBC will boot if the image is good)

4. Just to be sure, re-partitioned the SD card with `fdisk` to have one single, big partition on it only and added type `25` to it. (It doesn't matter, because the image writing overwrites everything anyway.): 
```ksh
# fdisk /dev/sda
[...]
Device     Start       End   Sectors  Size Type
/dev/sda1   2048 122191838 122189791 58.3G Linux root (ARM-64)
```
5. "Flashed" the Debian image to SD card


Can be useful: 

- https://github.com/jakogut/uboot-orangepi-rk3399
- https://www.mininodes.com/how-to-install-rocky-linux-8-on-arm-using-a-raspberry-pi-4/
- https://opensource.rock-chips.com/wiki_Boot_option
- https://en.opensuse.org/HCL:Firefly-RK3399


### `dd`-ready images, tried out so far

Image | Size | Usability | Notes
------|------|-----------|-------
RockyLinuxRpi_9.0.img | 1.3G | 🔴 | ...
rocky-9.0-aarch64-minimal-20220805.iso | 1.2G | 🔴 | ...
Fedora-Minimal-36-1.5.armhfp.raw | 2.1G | 🔴 | ...
Fedora-Server-36-1.5.armhfp.raw | 2.8G | 🔴 | ...
fedora-server-aarch64-f36-20220510-sda.raw | 2.6G | 🔴 | ...
openSUSE-Tumbleweed-ARM-JeOS-rockpro64.aarch64.raw | 1.5G | 🔴 | ...
Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img | 1.7G | 🟢 | ...
Orangepi4-lts_3.0.6_ubuntu_jammy_server_linux5.18.5.img | 2.1G | 🟢 | ...
Orangepi4-lts_3.0.6_ubuntu_jammy_desktop_xfce_linux5.18.5.img | 3.8G | 🟢 | ...
Armbian_22.08.2_Orangepi4-lts_bullseye_current_5.15.69.img | 1.5G | 🟡 | ...
Armbian_22.08.2_Orangepi4-lts_sid_edge_5.19.10_budgie_desktop.img | 1.6G | 🟡 | ...
Armbian_22.08.2_Orangepi4-lts_jammy_current_5.15.69_gnome_desktop.img | 4.8G | 🟡 | ...
Manjaro-ARM-gnome-generic-22.08.img | 5.3G | 🟠 | almost boots when `dd`-ed, `manjaro_arm_install` fails
Manjaro-ARM-gnome-opi4-lts-20221010.img | 5.5G | 🟠 | almost boots when `dd`-ed, `manjaro_arm_install` fails
Manjaro-ARM-minimal-opi4-lts-22.08.img | 1.2G | 🟠 | almost boots when `dd`-ed, `manjaro_arm_install` fails
Manjaro-ARM-minimal-opi4-lts-20221010.img | 1.6G | 🟠 | almost boots when `dd`-ed, `manjaro_arm_install` fails

Next idea: 

1. write `openSUSE-Tumbleweed-ARM-JeOS-rockpro64.aarch64.raw` to SD
2. mount it, and save all the data from it
3. re-partition `/dev/sda`
3. write `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img` to SD
4. mount it, and save ~~all the data~~ at least `/boot/grub2/grub.cfg`, `/etc/fstab` for UUIDs
5. and overwrite all the contents by the previously saved data and restore those UUIDs    

--> fail

Next idea: 

1. write `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img` to SD (e.g. with `dd`)
2. write `rocky-9.0-aarch64-minimal-20220805.iso` to a USB flash drive or another SD through SD adapter (also e.g. with `dd`)
3. put both storage devices to Pi, power it up (it should boot from the SD)
4. save the outputs of `blkid` and `lsblk`
5. edit `/etc/fstab` for which `/` should be mounted: change the UUID to the USB stick's UUID for `/`
6. edit `/boot/orangepiEnv.txt`: enter the same UUID
7. restart the Pi
8. start and enjoy?

--> fail

Next idea: 

1. write `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img` to SD (e.g. with `dd`)
2. partition and format a USB flash drive or another SD through SD adapter (e.g. `sda1`) with "Win95 FAT32" type, and mount it
3. mount the `rocky-9.0-aarch64-minimal-20220805.iso` as loop mount
4. copy all the contents of the installer to USB flash drive without trying to preserve attributes
5. put both storage devices to Pi, power it up (it should boot from the SD)
6. save the outputs of `blkid` and `lsblk`
7. edit `/etc/fstab` for which `/` should be mounted: change the UUID to the USB stick's UUID for `/`
8. edit `/boot/orangepiEnv.txt`: enter the same UUID
9. restart the Pi

--> fail

Next idea: 

1. write `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img` to SD (e.g. with `dd`)
2. `tar` the `usr` of the extracted `openSUSE-Tumbleweed-ARM-JeOS-rockpro64.aarch64.raw` to a `usr.tar`, and `scp` it over to a `/openSUSE-ARM-Extracted` directory
4. `tar` any `*zypp*` files/directories and `/etc/zypp/repos.d` too with archiving, then `scp`-ing, then extracting
3. extract the `usr.tar` and make the `bin -> usr/bin`, `lib -> usr/lib`, `lib64 -> usr/lib64`, `sbin -> usr/sbin` symlinks. 
5. create an empty `dev`, and a `tmp`
6. copy over Debian's `group hosts passwd profile resolv.conf shadow sudoers` files
7. `mkdir var/lib ; cd var/lib ; ln -s ../../usr/lib/sysimage/rpm`
8. switch to your new system for testing: `chroot /openSUSE-ARM-Extracted`
9. install a `mc` with `zypper` to test: `zypper install -y mc`
10. `exit`, `umount` the 2nd `/proc` and create the archive for saving the effort so far

--> fail

Next idea: Grabbing the working boot part of a working OS and use it for booting the installer up

[This](https://github.com/jakogut/uboot-orangepi-rk3399) gave me the idea. 

```
arch-chroot /mnt dd if=/boot/idbloader.img of=/dev/sdX seek=64 conv=notrunc
arch-chroot /mnt dd if=/boot/uboot.img of=/dev/sdX seek=16384 conv=notrunc
```


```ksh
root@orangepi4-lts:/# dd if=/dev/mmcblk2 of=orangeboot.img bs=512 count=32767
32767+0 records in
32767+0 records out
16776704 bytes (17 MB, 16 MiB) copied, 0.3373 s, 49.7 MB/s
```

Writing it, then the installer to the SD card: 

```ksh
# dd if=orangeboot.img of=/dev/sda bs=512 count=32767 conv=fdatasync
32767+0 records in
32767+0 records out
16776704 bytes (17 MB, 16 MiB) copied, 3.05678 s, 5.5 MB/s

# dd if=rocky-9.0-aarch64-minimal-20220805.iso of=/dev/sda bs=512 seek=32768 conv=fdatasync
2477696+0 records in
2477696+0 records out
1268580352 bytes (1.3 GB, 1.2 GiB) copied, 210.479 s, 6.0 MB/s
```

When the `rocky-9.0-aarch64-minimal-20220805.iso` is simply written to `/dev/sda`: 

```ksh
# gdisk -l /dev/sda
GPT fdisk (gdisk) version 1.0.9

Partition table scan:
  MBR: not present
  BSD: not present
  APM: not present
  GPT: not present

Creating new GPT entries in memory.
Disk /dev/sda: 122191872 sectors, 58.3 GiB
Model: STORAGE DEVICE  
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): F791234C-5F3E-430A-9A38-A5685D5C022F
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 122191838
Partitions will be aligned on 2048-sector boundaries
Total free space is 122191805 sectors (58.3 GiB)

Number  Start (sector)    End (sector)  Size       Code  Name
```




### Working images

#### Official Orange Pi releases

- `Orangepi4-lts_3.0.6_ubuntu_jammy_server_linux5.18.5.img`
  --> `glibc`: `2.35`; `linux`: `5.15.0`

- `Orangepi4-lts_3.0.6_debian_bullseye_server_linux5.10.43.img`
  --> `glibc`: `2.31`; `linux`: `5.10.46`

#### Armbian releases

- `Armbian_22.08.2_Orangepi4-lts_bullseye_current_5.15.69.img`
  --> 

- `Armbian_22.08.2_Orangepi4-lts_sid_edge_5.19.10_budgie_desktop.img`
  --> 

- `Armbian_22.08.2_Orangepi4-lts_jammy_current_5.15.69_gnome_desktop.img`
  --> 

#### Rocky 9

- `RockyLinuxRpi_9.0.img`
  --> `glibc`: `2.34`; `linux`: `5.14`

#### openSUSE Tumbleweed

- `openSUSE-Tumbleweed-ARM-JeOS-rockpro64.aarch64.raw`
  --> `glibc`: `2.36`; `linux`: `5.19.10`

#### Fedora 36

- `Fedora-Server-36-1.5.armhfp.raw.xz`
  --> `glibc`: `2.35`; `linux`: `5.17.5`

- `fedora-server-aarch64-f36-20220510-sda.raw.xz`
  --> `glibc`: `2.35`; `linux`: `5.17.5`


## Old stress tests and results

This particular test requires installing `rakudo`/`raku`, taking several minutes (originally, this example was mentioned [here](https://zostay.com/posts/2019/12/01/introducing-async-concurrency-in-raku/) with `1...10_000_000` but I didn't want the device to suffer forever): 

```ksh
# time raku -e 'my @doubles = (1...2_000_000).hyper.map(* * 2); say @doubles;'
[...]
```

(It takes `real  0m33.103s` on a 4 cores, 8 threads Intel(R) Core(TM) i7-1185G7 @ 3.00GHz.)

### Results without heat sink (RK3399 is totally "naked")

- 1st run, from freshly booted up system, with 1 minute of uptime (with 39-41 °C):

```ksh
[...]
real	4m2.877s
user	4m41.350s
sys	0m7.549s
```

- 2nd run, at 15 minutes uptime: 

```ksh
[...]
real	4m10.944s
user	4m54.139s
sys	0m8.226s
```

- 3rd run, at 30 minutes uptime: 

```ksh
[...]
real	4m44.573s
user	5m33.788s
sys	0m8.687s
```

#### Additional notes, conclusion and summary

- all CPU cores are working on such high load but especially cores 5 and 6 (the two Cortex-A72)
- the system boots up with 39-41 °C
- during the 1st run, it reaches 70 °C after 1 minute
- before it finishes, it reaches 85 °C each time (it looks like the achievable maximum)
- when the load test stopped (the system is basically back to 99% idle), and 10 minutes passed, it goes back to 58-60 °C, but it takes several more mintues to 

### Results with passive heat sink

- 1st run, from freshly booted up system, with 1 minute of uptime (with 39-41 °C):

```ksh
[...]
real	4m4.147s
user	4m43.918s
sys	0m6.631s
```

- 2nd run, at 15 minutes uptime (still monitoring temp with `htop`): 

```ksh
[...]
real	3m50.586s
user	4m33.653s
sys	0m6.071s
```

- 3rd run, at 30 minutes uptime (temp is not monitored; `htop` is not running): 

```ksh
[...]
real	4m4.293s
user	4m47.516s
sys	0m7.006s
```

#### Additional notes, conclusion and summary

- during the 1st run, it reaches 66 °C after 1 minute
- before it finishes the 1st run, it reaches 80 °C, but `htop` remains bouncing between 75 - 81 °C, and not at a stagning value (especially nothing above 81 °C)
- during the 2nd run, 85 °C is reached, but bouncing between 81 and 85 °Cq
- when the load test stopped, and 10 minutes passed, it goes back to 56-59 °C
- another 10 minutes of idle is needed to reach 58 °C

Same, under Ubuntu Jammy, with `raku` version `v2022.02`: 

```ksh
[...]
real	3m10.893s
user	3m41.801s
sys	0m5.462s
```

### Results with passive heat sink and a small spanner

Also, with a latest, locally compiled `raku`: 

```ksh
# raku -v
Welcome to Rakudo™ v2022.07.
Implementing the Raku® Programming Language v6.d.
Built on MoarVM version 2022.07.
```

- 1st run, from freshly booted up system, with 1 minute of uptime (with 39-41 °C):

```ksh
[...]
real	3m46.400s
user	4m21.349s
sys	0m6.341s
```

- 2nd run, at 15 minutes uptime (still monitoring temp with `htop`): 

```ksh
[...]
real	3m54.776s
user	4m31.060s
sys	0m6.192s
```

- 3rd run, at 30 minutes uptime (temp is not monitored; `htop` is not running): 

```ksh
[...]
real	3m45.808s
user	4m24.168s
sys	0m6.445s
```

#### Additional notes, conclusion and summary

- the system boots up with 37 °C only
- during the 1st run, it reaches 50 °C after 1 minute
- before finishes, it reaches 64 °C, but never above!
- it cools down to 53 °C at 15 minutes
- during the 2nd round, 73 °C is reached and bouncing between 69 and 73 °C
- it cools down to 55 °C at 30 minutes
- it cools down only to 53 °C at 90 minutes uptime


## Xen

Some notes: 
https://wiki.xenproject.org/wiki/Xen_ARM_with_Virtualization_Extensions/Ibox3399
https://vadion.com/getting-started-with-xen-hypervisor-on-orangepi-pc2-allwinner-h5/
