# ![alt text](/_ui/img/icons/azure_vm_m.svg) Azure Virtual Machines

<div style="text-align: right;"><h7>Posted: 2024-01-26 18:40</h7></div>

###### .

![alt_text](azure_-_vms_-_bg.jpg)


## Sisällysluettelo

1. [Johdanto](#1)
2. [Azure Virtual Machine -konfiguraatio](#2)
3. [Virtuaalikoneen Luominen](#3)
4. [VM:n Saatavuus ja Huolto](#4)
5. [Skaalautuminen](#5)
6. [Virtuaalikoneen Levyjärjestelmät](#6)
7. [Virtuaalikoneiden Valvonta](#7)
8. [Hyödylliset `az`-komennot VM:ien kyselyyn ja hallintaan](#8)


<a name="1"></a>
## 1. Johdanto

Azure Virtual Machines (VM) on yksi keskeisistä infrastruktuurin palveluista pilvessä, joka tarjoaa joustavan ja skaalautuvan tavan ajaa sovelluksia ilman fyysisen laitteiston rajoituksia. VM:t mahdollistavat monenlaisten sovellusten ja työkuormien ajamisen Azure-alustalla tarjoten valinnan vapauden käyttöjärjestelmissä, laitteistossa ja tallennuksessa. Ne soveltuvat hyvin erilaisiin tarpeisiin, kuten suurien tietokantojen ajamiseen, laskentatehoa vaativiin sovelluksiin tai yksinkertaisiin web-palveluihin.

Azure VM:t tarjoavat useita eri kokoonpanoja ja ominaisuuksia, jotka mahdollistavat tehokkaan suorituskyvyn eri käyttötapauksissa. Yksi Azure VM:ien eduista on mahdollisuus yhdistää skaalautuvuus, korkea saatavuus ja turvallisuus. 

Jatkuvasti päivittyvää ja kattavaa tietoa Azure VM:stä löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/virtual-machines/).


<a name="2"></a>
## 2. Azure Virtual Machine -konfiguraatio

Azure VM:ien luominen ja hallinta alkaa huolellisesta suunnittelusta ja konfiguraatiosta.

### 2.1 VM:n suunnitteluvaihe

VM:n suunnittelussa otetaan huomioon useita tekijöitä:
- **Verkko**: Ensimmäinen askel on suunnitella, miten VM liittyy verkkoon.
- **VM:n nimi**: Nimeä VM huolella, sillä nimeä ei voi muuttaa myöhemmin.
- **Sijainti**: Valitse sijainti Azure-alueiden välillä. Tämä vaikuttaa suorituskykyyn ja kustannuksiin.
- **Hinnoittelu**: Eri VM-tyypit ja alueet tarjoavat erilaisia hinnoittelumalleja.

VM:t voidaan dealloida (deallocate), jolloin resursseja ei käytetä eikä kustannuksia synny. VM:n tila voi olla joko:
- **Käynnissä (Running)**
- **Pysäytetty ja dealloitu (Stopped (deallocated))**

### 2.2 VM:n koko

Azure VM:n koon valinta perustuu käyttötapauksen vaatimuksiin, ja Microsoft tarjoaa erilaisia koodeja, joilla kuvataan virtuaalikoneen suorituskykyä ja ominaisuuksia. VM:n koodeilla, kuten **D** (yleiskäyttöinen) tai **F** (laskentatehoa optimoitu), voidaan helposti tunnistaa virtuaalikoneen kapasiteetti.

- **Yleiskäyttöiset** (D- ja B-sarjat): Tasapainoinen suhde CPU:n ja muistin välillä, sopii useimmille sovelluksille, kuten verkkopalvelimille ja pienten tietokantojen ajamiseen.
- **Laskentatehoa optimoidut** (F-sarja): Korkea CPU:n ja muistin suhde, sopii laskentaintensiivisiin työkuormiin, kuten tieteellisiin simulointeihin ja datankäsittelyyn.
- **Muistia optimoidut** (E- ja M-sarjat): Korkea muistin ja CPU:n suhde, sopii suurille tietokannoille ja muisti-intensiivisille sovelluksille.
- **Tallennusta optimoidut** (L-sarja): Korkea levytilan läpäisy ja I/O-suorituskyky, sopii sovelluksille, jotka vaativat suurta levyn I/O-suorituskykyä, kuten tiedon käsittelyyn ja analysointiin.
- **GPU** (N-sarja): Suunniteltu grafiikkaprosessointiin ja tekoälymalleille, kuten videoeditointiin ja koneoppimiseen.

Valitsemalla oikean VM-koodin ja -sarjan voit optimoida sovelluksesi suorituskyvyn ja kustannukset. Microsoft tarjoaa myös erityisiä SKU-koodeja, jotka auttavat valitsemaan oikean VM-tyypin työkuormille, kuten **s**-sarjan (SSD-levyjen tuki) tai **a**-sarjan (AMD-pohjaiset VM:t).

### 2.3 VM:n levytilat

Jokaisella Azure VM:llä on useita levyjä, jotka vastaavat eri käyttötarpeita:

- **OS-levy**: Käyttöjärjestelmän tallennuspaikka. Tämä levy tallennetaan Azure Managed Disk -ratkaisun kautta, mikä takaa saatavuuden ja suorituskyvyn.
- **Väliaikainen levy**: Käytetään väliaikaisten tiedostojen tallentamiseen, mutta sen sisältö ei ole pysyvää ja se voi hävitä, esimerkiksi VM:n uudelleenkäynnistyksen yhteydessä. Tämä levy on erityisen hyödyllinen swap- tai temp-tiedostoille, mutta sen käyttö on riskialtista pysyvässä tallennuksessa.
- **Datalevy**: Valinnainen levy, joka on tarkoitettu sovellusten ja datan tallentamiseen. Datalevy voi olla joko HDD- tai SSD-pohjainen, riippuen suorituskykytarpeista.

On myös mahdollista valita [VM-tyyppi, joka ei sisällä väliaikaista levyä](https://learn.microsoft.com/en-us/azure/virtual-machines/azure-vms-no-temp-disk). Tämä voi olla hyödyllistä, jos haluat vähentää riskin väliaikaisten tiedostojen menetyksestä. Azure Managed Disks varmistaa, että kaikki levyresurssit ovat automaattisesti hallinnoituja, mikä parantaa luotettavuutta ja hallittavuutta suurillekin työkuormille. Voit myös valita Premium-levyt (SSD) tai Ultra-levyt korkean suorituskyvyn sovelluksille, kuten SQL Serverille tai SAP HANA:lle.


<a name="3"></a>
## 3. Virtuaalikoneen Luominen

Azure VM:n luominen tapahtuu vaiheittain määrittelemällä verkkoasetukset, käyttöjärjestelmä ja muut tärkeät parametrit.

### 3.1 Käyttöjärjestelmän valinta

Azure tukee laajasti eri käyttöjärjestelmiä, kuten Windows ja Linux, ja tarjoaa myös mahdollisuuden tuoda omat kuvat **BYOS**-mallilla (*"Bring-Your-Own-Subscription"*). Tämä malli on erityisen hyödyllinen, jos yrityksellä on omat Red Hat- tai SUSE-lisenssit, joita halutaan käyttää pilvessä kustannusten optimointiin. BYOS-kuvia käytettäessä yritys voi hallita omia lisenssejään, mikä tuo lisäjoustavuutta ja voi vähentää lisenssikustannuksia. BYOS ei kuitenkaan sovi kaikkiin tilanteisiin, kuten silloin, kun tarvitaan suoraan Azure Marketplacen tuettuja ja täysin hallittuja kuvia.

Kun valitset käyttöjärjestelmää Azure-VM:lle, on tärkeää ottaa huomioon seuraavat tekijät:

- **Käyttötarkoitus**: Valitse käyttöjärjestelmä sen mukaan, mitä sovelluksia aiot ajaa. Windows-ympäristöt ovat tyypillisesti tarpeen Microsoft-teknologioiden kanssa, kun taas Linux sopii avoimen lähdekoodin sovelluksiin ja palveluihin.
- **Yhteensopivuus**: Varmista, että valittu käyttöjärjestelmä on yhteensopiva sovellusten ja yrityksesi IT-arkkitehtuurin kanssa.
- **Päivitysten hallinta**: Windows Serverillä on sisäänrakennetut päivitysominaisuudet, mutta Linux-jakelut saattavat vaatia erillisiä työkaluja, kuten **Azure Automationia**, päivitysten hallintaan.
- **Tuki ja turvallisuus**: Valitse käyttöjärjestelmä, jolla on pitkäaikainen tuki ja säännölliset tietoturvapäivitykset.

Jos haluat käyttää **BYOS**-kuvia, voit tarkistaa saatavilla olevat RHEL-kuvat käyttämällä seuraavaa komentoa:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm image list --publisher RedHat --all --output table</span>
Architecture    Offer                     Publisher       Sku                        Urn                                                                    Version
--------------  ------------------------  --------------  -------------------------  ---------------------------------------------------------------------  -----------------
x64             rh-ansible-self-managed   redhat-limited  rh-aap2                    redhat-limited:rh-ansible-self-managed:rh-aap2:9.0.20231213            9.0.20231213
[...]
x64             rh_rhel_8_main_1          RedHat          rhel_8_main_a1_gen1        RedHat:rh_rhel_8_main_1:rhel_8_main_a1_gen1:8.4.2021110400             8.4.2021110400
x64             test-rhel-byos            RedHat          another-test-plan          RedHat:test-rhel-byos:another-test-plan:9.2.2023060509                 9.2.2023060509
x64             test-rhel-byos            RedHat          test-private               RedHat:test-rhel-byos:test-private:9.2.2023060509                      9.2.2023060509
x64             test-rhel-byos            RedHat          test-private-plan          RedHat:test-rhel-byos:test-private-plan:9.2.2023060509                 9.2.2023060509
</pre>

Tämä näyttää sinulle saatavilla olevat kuvat. Jos haluat suodattaa vain **LVM8-tyyppiset BYOS-kuvat**, voit käyttää seuraavaa komentoa:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm image list --output table --all --publisher "RedHat" --offer "byos" --sku rhel-lvm8</span>
Architecture    Offer      Publisher    Sku               Urn                                                Version
--------------  ---------  -----------  ----------------  -------------------------------------------------  ---------------
x64             rhel-byos  RedHat       rhel-lvm8         RedHat:rhel-byos:rhel-lvm8:8.0.20200402            8.0.20200402
[...]
x64             rhel-byos  RedHat       rhel-lvm89        RedHat:rhel-byos:rhel-lvm89:8.9.2023122211         8.9.2023122211
x64             rhel-byos  RedHat       rhel-lvm89-gen2   RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023112310    8.9.2023112310
x64             rhel-byos  RedHat       rhel-lvm89-gen2   RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023122211    8.9.2023122211
</pre>

Ennen kuin voit ottaa käyttöön valitun RHEL-kuvan, sinun on todennäköisesti hyväksyttävä sen ehdot. Tämä onnistuu seuraavalla komennolla:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm image accept-terms --urn RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023122211</span>
This command has been deprecated and will be removed in version '3.0.0'. Use 'az vm image terms accept' instead.
{
  "accepted": true,
  "id": "/subscriptions/[...]
[...]
}
</pre>

Tämä komento hyväksyy kyseisen BYOS-kuvan käyttöehdot. Huomaa, että komento `az vm image accept-terms` on vanhentumassa, ja se korvataan tulevaisuudessa komennolla `az vm image terms accept`.

Lisätietoja mahdollisista RHEL-kuvista löytyy osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/virtual-machines/workloads/redhat/redhat-imagelist), ja `az vm image accept-terms` -komennosta voit lukea lisää [täältä](https://learn.microsoft.com/en-us/cli/azure/vm/image?view=azure-cli-latest#az-vm-image-accept-terms).

Valinta BYOS-kuvien ja Azure Marketplacen valmiiden kuvien välillä riippuu usein siitä, kuinka paljon hallintaa ja mukauttamista tarvitset sekä miten lisenssien hallinta on järjestetty yrityksessäsi.

### 3.2 VM:n luominen ja hallinta

Azure Virtual Machinet voidaan luoda useilla eri tavoilla, kuten:

- **Azure Portal**: graafinen käyttöliittymä manuaaliseen VM:n luontiin.
- **Azure CLI** ja **PowerShell**: komentorivipohjaiset työkalut, jotka tarjoavat joustavuutta ja automatisointimahdollisuuksia.
- **Infrastructure as Code** (IaaC) -työkalut, kuten **Terraform**: mahdollistavat infrastruktuurin määrittelyn koodina, mikä helpottaa ympäristöjen hallintaa ja toistettavuutta, erityisesti monimutkaisissa käyttöönottoissa.

Kun luot virtuaalikoneen, sinun on määriteltävä useita parametreja, kuten:

- **Sijainti**: Azure-alue, jossa VM sijaitsee, vaikuttaa kustannuksiin ja viiveisiin.
- **Käyttöjärjestelmä**: valitse tarpeiden mukaan, esim. Linux tai Windows.
- **Verkkoasetukset**: valitse VNet, aliverkko ja muut verkkoon liittyvät asetukset.
- **Resurssin koko**: valitse prosessointitehon ja muistin tarpeiden perusteella.

Linux-käyttöjärjestelmillä hallinta tapahtuu usein **SSH-avaimilla**, kun taas **Windows VM:ien** hallinta voidaan integroida **Azure AD** -ympäristöön. Tämä mahdollistaa käyttäjäoikeuksien synkronoinnin organisaation hallintamallin kanssa.

Kun VM on luotu, voit määritellä käyttöoikeuksia, kuten:

- **Virtual Machine Administrator Login**: pääsy täydellisiin hallintaoikeuksiin.
- **Virtual Machine User Login**: rajoitetut käyttöoikeudet VM:ään kirjautumiseen.

Azure AD -integrointi on erityisen hyödyllinen **Windows-VM**-ympäristöissä, mutta Linux-ympäristöissä käytetään useimmiten **SSH-pohjaista** hallintaa.

### 3.3 Suojattu pääsy Bastionin avulla

**Azure Bastion** tarjoaa turvallisen tavan päästä käsiksi VM:ään suoraan selaimen kautta ilman, että sinun tarvitsee määrittää julkista IP-osoitetta virtuaalikoneelle. Bastion toimii välityspalvelimena, joka suojaa RDP- ja SSH-yhteydet, jolloin verkon altistuminen vähenee merkittävästi. Tämä on erityisen tärkeää korkean tietoturvan vaatimuksissa, koska suora pääsy internetin kautta voidaan estää.

Bastion-palvelu on helppo ottaa käyttöön ja se integroituu suoraan Azure-portaaliin ja virtuaaliverkkoihin. Se tarjoaa suojatun tavan päästä Linux- ja Windows-virtuaalikoneisiin ilman erillisiä VPN-ratkaisuja tai julkisia IP-osoitteita. Tämä palvelu on kuitenkin maksullinen ja erityisesti Linux-käyttäjien kannattaa varmistaa, että Bastionin käyttökustannukset ovat perusteltuja käytön laajuuteen nähden.


<a name="4"></a>
## 4. VM:n Saatavuus ja Huolto

Azure VM:t tarjoavat monipuoliset ja joustavat vaihtoehdot korkean käytettävyyden saavuttamiseksi sekä huoltotoimenpiteiden tehokkaaksi hallinnaksi. Microsoftin pilvipalvelu takaa, että VM:t voidaan sijoittaa ja konfiguroida siten, että vikasietoisuus, suorituskyky ja huollon vaikutukset minimoidaan.

### 4.1 Saatavuusryhmät

**Saatavuusryhmät** (**Availability Sets**) ovat ratkaisu korkean käytettävyyden varmistamiseksi virtuaalikoneille saman alueen sisällä. Saatavuusryhmissä VM:t jaetaan:

- **Vika-alueisiin** (Fault Domains): Vika-alueet takaavat, että virtuaalikoneet eivät ole kaikki samalla fyysisellä laitteistolla. Jos yksi fyysinen palvelin tai datakeskuksen osa kaatuu, vain osa VM:stä menettää yhteyden, mikä parantaa vikasietoisuutta.
- **Päivitysalueisiin** (Update Domains): Päivitysalueet varmistavat, että vain osa VM:stä päivitetään kerrallaan, jolloin suunnitellut huoltotoimenpiteet, kuten ohjelmistopäivitykset, eivät vaikuta koko sovellukseen.

Saatavuusryhmiä käyttämällä voidaan saavuttaa jopa **99,95 % SLA** (Service Level Agreement).

### 4.2 Saatavuusalueet

**Saatavuusalueet** (**Availability Zones**) tarjoavat korkeamman vikasietoisuuden jakamalla VM:t fyysisesti erillisiin datakeskuksiin saman alueen sisällä. Jokainen saatavuusalue sisältää yhden tai useamman täysin riippumattoman datakeskuksen, joilla on omat energiansyötöt, jäähdytysjärjestelmät ja verkkoyhteydet. Saatavuusalueiden avulla saavutetaan:

- Parempi vikasietoisuus, koska VM:t sijaitsevat fyysisesti erillisissä datakeskuksissa.
- Parempi suorituskyky ja vähäisempi seisokkiaika, erityisesti kriittisissä sovelluksissa, joissa seisokit eivät ole sallittuja.

Saatavuusalueiden käytöllä voit saavuttaa jopa **99,99 % SLA**, mikä on erityisen tärkeää liiketoimintakriittisissä sovelluksissa.

### 4.3 Huolto ja seisokit

Azure suorittaa ajoittain suunniteltuja huoltoja ja päivityksiä datakeskuksissaan varmistaakseen infrastruktuurin turvallisuuden ja suorituskyvyn. Saatavuusryhmät ja -alueet tarjoavat mahdollisuuden minimoida huollon vaikutukset:

- **Joustavat huoltoikkunat**: Päivitykset tehdään usein vain yhdelle päivitysalueelle tai saatavuusalueelle kerrallaan, mikä minimoi vaikutukset sovelluksille.
- **Live Migration**: Azure voi suorittaa **live migration** -siirtoja, jotka siirtävät VM:n toiselle isännälle ilman merkittävää seisokkia.

Seisokkien vaikutukset voidaan minimoida myös käyttämällä **Azure Planned Maintenance Notifications** -palvelua, joka antaa ennakkovaroituksia tulevista huoltotoimenpiteistä.

### 4.4 Varautuminen suunnittelemattomiin seisokkeihin

Vaikka suunnitellut huollot voidaan ennakoida ja hallita, suunnittelemattomiin seisokkeihin on syytä varautua. Käyttämällä saatavuusryhmiä ja -alueita yhdessä, voit varmistaa, että sovelluksesi jatkavat toimintaa vaikka osa infrastruktuurista pettäisi.

- **Rajoita yhden vikakohdan vaikutusta** käyttämällä sekä saatavuusryhmiä että saatavuusalueita.
- Käytä **Virtuaalikoneiden skaalausryhmiä** (**VM Scale Sets**) hallitsemaan ja skaalamaan VM:iä automaattisesti seisokkien tai korkean kuormituksen aikana.

Näillä ratkaisuilla voit minimoida sekä suunniteltujen että suunnittelemattomien seisokkien vaikutukset.


<a name="5"></a>
## 5. Skaalautuminen

Azure tarjoaa sekä vertikaalisen että horisontaalisen skaalautuvuuden vaihtoehtoja.

### 5.1 Vertikaalinen skaalautuminen

Vertikaalinen skaalautuminen tarkoittaa yksittäisen VM:n suorituskyvyn nostamista lisäämällä resursseja, kuten CPU:ta ja RAM:ia. Tämä prosessi vaatii manuaalista toimenpidettä ja aiheuttaa hetkellisen käyttökatkoksen.

### 5.2 Horisontaalinen skaalautuminen

Horisontaalinen skaalautuminen tarkoittaa useamman VM-instanssin käyttöönottoa työkuorman jakamiseksi. Tämä tapahtuu usein automaattisesti **Virtual Machine Scale Sets** -ratkaisun avulla, joka lisää tai vähentää instansseja kuormituksen mukaan.

### 5.3 Skaalaussäännöt

Autoskaalaus voidaan määrittää perustuen eri mittareihin, kuten CPU:n käyttöön, tai aikataulun mukaisesti, jolloin kuormituspiikit voidaan käsitellä tehokkaasti.

Lisätietoja VM Scale Sets:stä löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/overview).


<a name="6"></a>
## 6. Virtuaalikoneen Levyjärjestelmät

Azure tarjoaa useita levyjärjestelmiä, jotka tukevat erilaisia suorituskyky-, saatavuus- ja kustannustarpeita. Levyjen valinta riippuu siitä, minkälaista suorituskykyä ja tallennustilaa sovellukset vaativat.

### 6.1 Levyjen tyypit

Azure tarjoaa seuraavat levytyypit, jotka on optimoitu erilaisiin käyttötarkoituksiin:

- **Ultra-disk SSD**: Suunniteltu erittäin intensiivisiin I/O-tehtäviin, kuten suurille tietokannoille ja SAP HANA -työkuormille. Tukee erittäin korkeita IOPS- ja läpäisynopeuksia.
- **Premium SSD**: Suorituskykyä vaativiin sovelluksiin, kuten verkkosovelluksiin, tietokantoihin ja tuotantoympäristöihin, joissa vaaditaan alhaista viivettä ja korkeaa luotettavuutta.
- **Standard SSD**: Soveltuu kevyemmille työkuormille, kuten web-palvelimille ja dev/test-ympäristöihin. Tarjoaa paremman suorituskyvyn kuin HDD, mutta edullisempaan hintaan.
- **Standard HDD**: Käytetään ei-kriittisiin ja harvoin käytettyihin tietoihin, kuten varmuuskopioihin ja arkistoihin, joissa suorituskyky ei ole ensisijainen kriteeri.

Levytyypit valitaan sovelluksen tarpeiden mukaan. **Ultra-disk SSD** on ihanteellinen korkean suorituskyvyn tarpeisiin, kun taas **Standard HDD** soveltuu paremmin kustannustehokkaaseen tallennukseen.

### 6.2 Levyjen suorituskyky

Levyjen suorituskyky riippuu paitsi valitusta levytyypistä myös käytettävän **virtuaalikoneen koosta**. Esimerkiksi suuremmilla instansseilla on usein korkeammat IOPS- ja läpäisyrajoitukset. Premium- ja Ultra-levyjen avulla voidaan skaalata suorituskykyä työkuormien mukaan, ja levyjä voidaan optimoida käyttämällä esimerkiksi **levyvälitystä** (disk caching), mikä parantaa luku- ja kirjoitustoimintoja.

### 6.3 Levyjen jakaminen

Azure mahdollistaa levyjen jakamisen useiden virtuaalikoneiden kesken, mikä on erityisen hyödyllistä klusteroiduissa ympäristöissä tai sovelluksissa, joissa on tarve jakaa sama tallennustila useille instansseille. **Levyjen jakaminen** parantaa joustavuutta ja vähentää datan replikoinnin tarvetta useiden instanssien välillä. Tämä voi myös parantaa käytettävyyttä monimutkaisissa sovellusarkkitehtuureissa.

Lisätietoja ja ajantasaiset tiedot levyjärjestelmistä löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/virtual-machines/disks-types).


<a name="7"></a>
## 7. Virtuaalikoneiden Valvonta

Azure tarjoaa kattavat valvontatyökalut, joiden avulla voit seurata virtuaalikoneiden suorituskykyä ja käytettävyyttä. Nämä työkalut auttavat ennakoimaan mahdolliset ongelmat ja optimoimaan VM-ympäristön toimintaa.

### 7.1 Azure Monitor

**Azure Monitor** kerää ja analysoi VM:ien suorituskykyyn liittyviä mittareita, kuten CPU-, muisti-, levy- ja verkkokäyttöä. Se tarjoaa keskitetyn näkymän, jonka avulla voidaan havaita mahdolliset suorituskykyongelmat ja tehdä päätöksiä resurssien skaalaamisesta. Azure Monitorin avulla voidaan asettaa myös hälytyksiä, jotka ilmoittavat, kun tietty raja-arvo ylitetään.

### 7.2 Diagnostiikkalokit

**Diagnostiikkalokit** tarjoavat tarkempaa tietoa VM:n sisäisestä toiminnasta, kuten järjestelmän virheistä, palvelinlokeista ja resurssien käytöstä. Näiden lokien avulla voidaan nopeasti tunnistaa ja korjata vikatilanteet sekä optimoida järjestelmän suorituskykyä. Diagnostiikkatietojen kerääminen ja analysointi voi tapahtua Azure Monitorin kautta.

### 7.3 Autoskaalaus ja valvonta

Autoskaalaus on tärkeä ominaisuus, joka voidaan integroida Azure Monitorin kanssa. Autoskaalauksella varmistetaan, että VM-instanssien määrä kasvaa tai vähenee automaattisesti kuormituksen perusteella. Tämä auttaa optimoimaan resurssien käyttöä ja kustannuksia. Autoskaalaus voi perustua esimerkiksi CPU-käyttöön, muistinkulutukseen tai muihin mittareihin, joita seurataan reaaliajassa.

Lisätietoja ja ajantasaiset tiedot VM-valvonnasta löydät osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/azure-monitor/autoscale/autoscale-overview).


<a name="8"></a>
## 8. Hyödylliset `az`-komennot VM:ien kyselyyn ja hallintaan

On usein hyödyllistä saada nopeasti tietoja organisaation VM:istä, erityisesti ohjelmallisten ja nopeiden ratkaisujen pohjaksi.

Käymme läpi joitakin hyödyllisiä komentoja.

Lisätietoja ja ajan tasalla olevat ohjeet löytyvät learn.microsoft.com-sivustolta [az vm](https://learn.microsoft.com/en-us/cli/azure/vm?view=azure-cli-latest).

### 8.1 Yleiset VM-ominaisuudet ja Linux-koneiden admin-avaimet

Voit saada yhteenvedon kaikista aktiivisen tilauksen alla olevista VM:istä, ensisijaisesti alueittain järjestettynä:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm list --output table</span>
Name                  ResourceGroup      Location    Zones
--------------------  -----------------  ----------  -------
cockpitvirt-us_vm     COCKPITVIRT        eastus2
log-central-us_vm     LOG-CENTRAL        eastus2
cockpitvirt-eu_vm     COCKPITVIRT        westeurope
log-central-eu_vm     LOG-CENTRAL        westeurope
</pre>

Komento `az vm list` ilman parametreja tuottaa saman tuloksen kuin `az vm list --output json`. Oletustuloste on suuri JSON, joka sisältää jopa `admin_key`-tiedot kohdassa `osProfile.linuxConfiguration.ssh.publicKeys[0].keyData`:

```
[...]
    "instanceView": null,
    "licenseType": null,
    "location": "westeurope",
    "managedBy": null,
    "name": "cockpitvirt-eu_vm",
[...]
    "osProfile": {
      "adminPassword": null,
      "adminUsername": "infra",
      "allowExtensionOperations": true,
      "computerName": "cockpitvirt-eu",
      "customData": null,
      "linuxConfiguration": {
        "disablePasswordAuthentication": true,
        "enableVmAgentPlatformUpdates": false,
        "patchSettings": {
          "assessmentMode": "ImageDefault",
          "automaticByPlatformSettings": null,
          "patchMode": "ImageDefault"
        },
        "provisionVmAgent": true,
        "ssh": {
          "publicKeys": [
            {
              "keyData": "ssh-ed25519 AAAAC3[...]7TqoLY temporary key for vm provisioning\n",
              "path": "/home/infra/.ssh/authorized_keys"
            }
          ]
        }
      },
      "requireGuestProvisionSignal": true,
      "secrets": [],
      "windowsConfiguration": null
    },
[...]
```

### 8.1.1 Tulosten suodattaminen

`--query`-parametri on sisäänrakennettu toiminto, jolla voit suodattaa tietoja ilman tarvetta käyttää esimerkiksi `jq`-työkalua. Esimerkiksi näin voit tulostaa VM:ien resurssi-ID:t tietyn resurssiryhmän alla:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm list -g COCKPITVIRT --query "[].id" -o tsv</span>
/subscriptions/[...]/resourceGroups/COCKPITVIRT/providers/Microsoft.Compute/virtualMachines/cockpitvirt-us_vm
/subscriptions/[...]/resourceGroups/COCKPITVIRT/providers/Microsoft.Compute/virtualMachines/cockpitvirt-eu_vm
</pre>

Kaikkien VM:ien `name`- ja `resourceGroup`-ominaisuuksien tulostaminen:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm list --query "[].{Name:name, ResourceGroup:resourceGroup}" --output table</span>
Name                  ResourceGroup
--------------------  -----------------
cockpitvirt-us_vm     COCKPITVIRT
log-central-us_vm     LOG-CENTRAL
cockpitvirt-eu_vm     COCKPITVIRT
log-central-eu_vm     LOG-CENTRAL
</pre>

Jos haluat kaikki ominaisuudet yhdestä VM:stä, voit käyttää komentoa `az vm show` tarvittavilla parametreilla, kuten `--name` ja `--resource-group`, esimerkiksi:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm show --name cockpitvirt-eu_vm --resource-group COCKPITVIRT --query "osProfile.linuxConfiguration.ssh.publicKeys"</span>
[
  {
    "keyData": "ssh-ed25519 AAAAC3[...]7TqoLY temporary key for vm provisioning\n",
    "path": "/home/infra/.ssh/authorized_keys"
  }
]
</pre>

Alla esimerkki shell-skriptistä, joka tekee yhteenvedon kaikista VM:istä, niiden resurssiryhmistä ja admin-avaimista käyttäen `jq`-työkalua:

```
#!/bin/sh

# Print the table header
echo -e "Name\tRG\tKey"

# Get the list of VMs in JSON format and filter using jq
az vm list --output json | jq -r '.[] | 
  {
    Name: .name,
    ResourceGroup: .resourceGroup,
    SSHKey: (.osProfile.linuxConfiguration.ssh.publicKeys[0].keyData // "")
  } | 
  "\(.Name)\t\(.ResourceGroup)\t\(.SSHKey | sub("\n"; "") | .[:10] + "[...]" + .[-9:])"'
```

### 8.1.2 Admin-avaimen päivittäminen

Voit lisätä uusia admin-avaimia, mutta niitä ei näytetä eikä korvata alkuperäisessä kokoonpanossa, esimerkiksi:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az vm user update --name cockpitvirt-eu_vm --resource-group COCKPITVIRT --username infra --ssh-key-value "ssh-rsa ASDF1234== dummy key"</span>
{
  "autoUpgradeMinorVersion": false,
  "enableAutomaticUpgrade": null,
  "forceUpdateTag": null,
  "id": "/subscriptions/[...]/resourceGroups/COCKPITVIRT/providers/Microsoft.Compute/virtualMachines/cockpitvirt-eu_vm/extensions/enablevmaccess",
  "instanceView": null,
  "location": "westeurope",
  "name": "enablevmaccess",
  "protectedSettings": null,
  "protectedSettingsFromKeyVault": null,
  "provisionAfterExtensions": null,
  "provisioningState": "Succeeded",
  "publisher": "Microsoft.OSTCExtensions",
  "resourceGroup": "COCKPITVIRT",
  "settings": {},
  "suppressFailures": null,
  "tags": null,
  "type": "Microsoft.Compute/virtualMachines/extensions",
  "typeHandlerVersion": "1.5",
  "typePropertiesType": "VMAccessForLinux"
}
</pre>
