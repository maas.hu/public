# ![alt text](/_ui/img/icons/raku_m.svg) Raku

<div style="text-align: right;"><h7>Posted: 2019-05-22 13:50</h7></div>

###### .

## Tartalomjegyzék

1. [Bevezetés](#1)
2. [A fejlesztési környezet kialakítása](#2)
3. [Programírási alapok](#3)
4. [Konstansok és változók](#4)
5. [Vezérlési szerkezetek](#5)
6. [Operátorok](#6)
7. [Reguláris kifejezések](#7)
8. [Fájl I/O](#8)
9. [Alprogramok](#9)
10. [Modulok](#10)
11. [Osztályok és objektumok](#11)
12. [A nyelvtan (grammar) használata](#12)

<a name="1"></a>
## 1. Bevezetés

<a href="larry-wall-perl-virtues.jpg" target="_blank" title="">
  <img src="larry-wall-perl-virtues.gif" class="right"></a>
Egy Linux/Unix rendszereket üzemeltető adminisztrátor, vagy az ezeken a rendszereken futó bármely szolgáltatás 
üzembentartója, fejlesztője gyakran azon kaphatja magát, hogy kisebb-nagyobb parancsállományokat (szkripteket) készít, illetve karbantartja a meglévőket. A szkriptek számításokat, feldolgozást végeznek, amelyhez a szükséges adatok többnyire nem saját maguktól, előre definiáltan érkeznek. Sokkal inkább a további programok meghívásából származó, vagy egyéb forrásból érkező, nyers formátumban megkapott adatok ezek. A feldolgozás végén jelentés, riport készül, vagy akár a riport tartalmának megfelelően automatikusan egy „döntés” születik, ami alapján az emberi adminisztrátor email vagy egyéb megoldás révén értesítésre kerül, figyelmeztetik őt. Ez a szkriptek egy rendkívül gyakori felhasználási módja, de csak egy a sok közül.

A fentihez hasonló feladatok miatt meglehetősen egyértelmű volt, hogy a shell szkriptek fejlesztése mellett, majd remélhetőleg később azok helyett egy univerzálisabb, az adatfeldolgozásban is jóval erősebb, ugyanakkor más területeken is kiválóan alkalmazható programozási nyelv, a Perl elsajátításába kezdjek bele. Neki is lendültem a jelenleg széleskörűen elterjedt változat, a **Perl 5** tanulmányozásába, néhány rövidebb programot össze is hoztam, és bár a `sed`, a `grep` és az `awk` turmixának ható reguláris kifejezések és a helyenként már-már követhetetlenné faragott szintaktika kissé riasztottak, alapjában véve kezdett megtetszeni. Tovább kutatva a Perl fejlődéstörténetében, megdöbbenve vettem tudomásul, hogy gyakorlatilag készen áll, sőt, minden közismert platformra elérhető a 6-os verzió, csak éppen annak optimalizálása, és ezzel együtt alapértelmezetté tétele a különféle Unix-szerű rendszereken már hosszú évek óta várat magára. Ennek legfőbb oka, hogy a **Perl 6**, azaz a **Raku** olyan szinten megváltozott, annyira eltér elődjétől, hogy azzal visszafelé már nem kompatibilis. Lehetséges ugyan egy-egy nagyon egyszerű kódot írni, ami változtatás nélkül lefut az 5-ös és a 6-os Perl meghívásával egyaránt, de ennél többre jobb, ha nem is számítunk. Egy vadonatúj programozási környezet áll tehát rendelkezésünkre, ami szinte minden korábbinál letisztultabb, átgondoltabb, és csak arra vár, hogy használatba vegyük. Ez borzasztóan izgalmas kihívásnak ígérkezett, amit egyszerűen képtelenség volt kihagyni.

### 1.1 A Raku születése

<a href="camelia.png" target="_blank" title="">
  <img src="camelia.gif" class="right"></a>
A Raku a Perl nyelv egyenesági leszármazottja. A vaskos, 30 éves múlttal rendelkező elődnek számos alkalmazási területe van. Kétségkívül a szöveges és egyéb adatok begyűjtésében, feldolgozásában, riportok készítésben a legismertebb, de többek között kiemelkedő sikereket ért el a CGI alapú weboldalak nyelveként is. A Perl név ugyan eredetileg nem egy mozaikszó, de idővel a legnépszerűbbé vált, utólagos elnevezéssel akár rövidítésként is megállja a helyét, hiszen úgy a *Practical Extraction and Report Language*, szavak kezdőbetűiből is kirakható, melynek jelentése nagyjából: *Gyakorlatias Kivonatoló és Jelentéskészítő Nyelv*. A kivonatolás és jelentéskészítés területein szerzett érdemei mellett rövid időn belül rengeteg további feladat végrehajtására is kiválóan alkalmassá vált, így egy tipikus általános célú programozási nyelv lett belőle. Használhatóságát nagy mértékben fokozza a rendelkezésre álló modulok tömkelege, és az azok kezelésében nagy segítséget nyújtó, folyamatosan bővített modul-adatbázis, a **CPAN** (*Comprehensive Perl Archive Network*, magyarul Kiegészítő Perl Archív Hálózat).

De ha ennyi jót tudunk elmondani a Perlről, ugyan miért volt szükség egy utódra? Ennek egyik oka, hogy az ezredfordulóra már kifejezetten érezhető volt, ahogy egyre kevesebb felhasználó áll a nyelv mellé, különösen annak helyenként nehezen követhető szintaktikája, zavaros megvalósításai miatt. A szintaktika rendkívül rugalmas és megengedő: egyféle feladat leprogramozása számos módon kivitelezhető, így alakult ki az évek során a *„There is more than one way to do it”*, rövidítve TIMTOWTDI (*„Tim Toady”*), szlogen a Perl közösségén belül, amit nagyjából így fordíthatunk: *„Egynél több módon is megoldható ez”*. Azonban ez a sokszor már-már eltúlzott rugalmasság csakhamar a nyelv egyik taszító jellemzőjévé vált, hiszen rendkívül könnyű olyan kódot írni vele, amit szinte senki más nem tud értelmezni a készítőjén kívül. Mindemellett a nyelv bővülése is hosszú évek óta visszafogottabb ütemben haladt már csak, és mindössze javítások, apróbb újdonságok, optimalizálások történtek a kódbázisban. A 2000 júliusában tartott Perl Konferencián **Larry Wall**, a Perl nyelv eredetijének megalkotója – aki egyben jelenleg is a fejlesztés első számú koordinátora – bejelentette, hogy megkezdték a Perl 6 kialakításának folyamatát. Ez egyértelműen a nyelv alapos kitakarítását, és a mai modern IT világ elvárásainak megfelelő teljes megújulást jelentette, amely így egyben lehetséges megoldásként szolgált a nyelv megmentésére. 

Larry Wall eképpen foglalta össze a fejlesztés új irányvonalát: 

>A Perl 6-nál úgy döntöttünk, hogy jobb lenne inkább a nyelvet kijavítani, mint a felhasználót.

A Raku elnevezés születéséhez igencsak rögös út vezetett. Annak ismeretében, hogy elődjétől, a Perl-től milyen nagy mértékben különbözik, jogosan merült fel a kérdés a fejlesztők fejében már a kezdetekben is, hogy a népszerűségében napjainkra már meglehetősen megkopott Perl nevet vigyék-e csupán tovább – ezzel megkockáztatva azt, hogy sokan így bele sem vágnának az új nyelv megtanulásába –, vagy kapjon-e teljesen új nevet. Ugyanakkor, ha nem is rögtön egy teljesen új elnevezést, hanem egy időre csak egy „művésznevet”, alias-t kapna, annak véglegessé válása a Perl név rovására történne. Ez a kérdés a nyelv fejlesztésében résztvevők és a felhasználók közösségében már az első hivatalos, 2015. karácsonyi kiadás előtt is felmerült, de attól kezdve egyre inkább egy állandó, nyomasztó, megoldandó problémaként mutatkozott.

A Perl 6 nyelv megvalósítását a **Rakudo** nevű fordítóprogram látja el, amely minden korábbi próbálkozásnál jobbnak, hatékonyabbnak bizonyult, így elnyerte a stabil, hivatalos, éles környezetben is használható státuszt. Neve kiötlésének hátterében szintén egy érdekes gondolatmenet zajlott le. Először is, fontos tudnunk, hogy a Perl logója, vagy ha úgy tetszik kabalája a teve. A japán らくだ („rakuda”) szó jelentése: teve, míg a どう („dō”) jelentése: út, mód. Ennek a „rakuda-do” („út a tevéhez”) kifejezésnek az egyszerűsítéséből született meg a „rakudo”. Ezen a logikán egy kicsit tovább szárnyalva az angol „do" („csinálni”) szó utólagos „odaértésével” frappáns módon jött az ötlet, hogy a "raku-do" tulajdonképpen az eszköz, ami „csinálja” a „raku”-t. Azaz, a Rakudo fordító végcélja a Raku megvalósítása. Ez olyan jó ötletnek bizonyult, hogy végül 2018. november 3-án többen is ki merték jelenteni, hogy a Raku hivatalosan is használható, miután Larry Wall erre előzőleg áldását adta. Először csak alias-ként, de ha úgy alakul, akár le is válthatja a Perl elnevzést, hiszen ez a Perl már nem az a Perl. Valami, ami sokkal átgondoltabb, egységesebb, letisztultabb és modernebb annál: egy valódi XXI. századi programozási nyelv.

### 1.2 A Raku felépítése

A Raku, a Perl 6, vagy a legutóbbi *6.d Spec. Release Brochure* dokumentum alapján a Raku Perl 6 egy teljesen új nyelvi specifikáció. Száműztek belőle szinte mindent, ami miatt sokan már évek óta ódzkodnak a Perltől, miközben mégis meg tudtak tartani rengeteg olyan hasznos megoldást, amik az elődöt egy igazán erős, stabil platformmá tették. Végre egy valódi objektumorientált rendszert kaptunk, a program futása során akár a párhuzamos feldolgozás is támogatott, és még egy sor érdekességet tartalmaz. A pillanatnyilag legfrissebb 6.d verzió már jelenleg is stabil, éles környezetben való használatra is ajánlott kiadás. Sőt, már a 2015. decemberi, verzióját tekintve a 6.c is az volt, amely méltán érdemelte akkor ki a Perl 6 közösségben az „1.0” elnevezést is.

<a href="raku_retegek.png" target="_blank" title="">
  <img src="raku_retegek.gif" class="right"></a>
A Raku egy magas szintű, általános célú, interpretált programozási nyelv. Az egyszerű strukturált programozási módszertanon kívül támogatja a funkcionális, a procedurális és az objektum-orientált paradigmákat egyaránt. Interpretált, azaz szkriptnyelv, vagy más szóval élve parancsnyelv. Ez annyit jelent, hogy a szöveges állományokban található kódsort a fordítóprogram azonnal, parancsról-parancsra megpróbálja értelmezni, majd végrehajtani. A fordítóprogram általában nem állít elő gépi kódú állományt a forráskódunkból. Ez főleg kisebb programok esetében előnyös, továbbá ezek a programok sok-kal hordozhatóbbak, egyben nyíltabbak, könnyen bővíthetőek.

A Raku nyelvi specifikáció rétegét, annak közvetlen értelmezését tehát a Rakudo nevű program látja el. A Rakudo az, ami fogadja az általunk írt Raku kódot, értelmezi, és ha nem talál benne hibát, egy átmeneti bájtkódot állít elő a neki beállított virtuális gép számára.

A bájtkód egy köztes kód, ami akár hordozható is lehetne különböző számítógépek között, önmagában viszont még nem működőképes. Futtatásához szükség van egy virtuális gépre, ami azt értelmezi, futtatja, és végül elkészít belőle egy gépi kódot az adott rendszer számára. A Perl 6 eddigi történetében több program is betöltötte már a virtuális gép szerepét, végül 2014-től a **MoarVM** bizonyult a leggyorsabbnak, legstabilabbnak, így jelenleg ez az aktívan fejlesztett irányvonal. A teljes Perl 6 specifikáción kívül az **NQP** („Not Quite Perl”) feldolgozására is képes, aminek csak a fejlesztés, és a Rakudo futtatókörnyezet forrásból való fordításának, létrehozásának szempontjából van jelentősége.

<a name="2"></a>
## 2. A fejlesztési környezet kialakítása

Napjainkban a legtöbb elterjedt fejlesztőeszköz nem korlátozódik egyetlen, vagy legfeljebb egynéhány megoldásra. Két évtizeddel ezelőtt talán még így volt, de ma már nem ér minket olyan jellegű kötöttség, hogy csak egyféle operációs rendszeren, egyféle platformra, és a legkevésbé sem kell egyféle szövegszerkesztő programban írnunk programjainkat. Mindez a Rakura (egyben a Perlre is) fokozottan érvényes: a lehetőségek tárháza gyakorlatilag határtalan. Az alábbiakban csak néhány lehetőség kerül bemutatásra, ám a sok-sok megoldás közül remélhetőleg a leghasználhatóbbakat, legáltalánosabbakat ismerhetjük meg.

Említésre érdemes még a Rakuval való ismerkedés kapcsán egy weboldal, ahol egyszerűen csak a böngésző ablakában begépeljük a programkódot, majd futtatjuk azt a weboldal mögött üzemelő számítógépen, azaz saját gépünkön semmit sem kell telepítenünk. Egy ilyen oldal jelenleg ez.

A fejlesztési környezet röviden összefoglalva az alábbiakat követeli meg, amelyek akár a tanulás, gyakorlás és valódi fejlesztés közben folyamatosan változhatnak is, de jelenlétük mindvégig nélkülözhetetlen:

- Linux, Windows vagy MacOS operációs rendszer
- Telepített Raku fordító
- Egy szövegszerkesztő

### 2.1 A Raku beszerzése

Az éppen aktuális stabil verzió azonnal használható telepítőkészletei Windows és macOS rendszerekre a [rakudo.org](http://rakudo.org) oldalról tölthetőek le jelenleg. Ez azt jelenti, hogy az ott található telepítőcsomagot mindössze le kell töltenünk, és máris telepíthetjük azt a hagyományos módon. A telepítés eredményeként az ebben a fejezetben többször is említett három fő összetevőt, továbbá a leggyakoribb kiegészítő modulokat, és némi dokumentációt kapunk, mely gyűjtemény összefoglaló neve **Rakudo Star**. Windows operációs rendszer alatt egyelőre kizárólag a `C:\rakudo` könyvtár alá telepíthető a csomag, amely bekerül a `PATH` környezeti változóba is.

#### 2.1.1 Gyári csomagok

Néhány Linux disztribúcióra (pl. openSUSE) már a gyári repository-ban is fellelhetőek a készlet főbb összetevői, amelyeket a disztribúció csomagkezelőjének megfelelő módon, a raku vagy rakudo csomagra keresve van lehetőségünk telepíteni. Az említett openSUSE rendszeren pl. az alábbi módon kereshetünk rá, majd telepíthetjük, természetesen root-ként:

```ksh
# zypper search raku
Loading repository data...
Reading installed packages...

S | Name   | Summary                                  | Type   
--+--------+------------------------------------------+--------
  | rakudo | Perl 6 implemenation that runs on MoarVM | package

# zypper install rakudo
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following 3 NEW packages are going to be installed:
  moarvm nqp rakudo

3 new packages to install.
Overall download size: 4.8 MiB. Already cached: 0 B. After the operation, additional 31.1 MiB will be used.
Continue? [y/n/...? shows all options] (y): y
```

Amennyiben ki szeretnénk próbálni inkább egy újabb verziót Linux, AIX vagy más Unix-szerű rendszeren, esetleg teljesen saját fordítást szeretnénk készíteni magunknak, illetve más rendszerekre is felmásolnánk a kész, saját fordítású csomagot, úgy választhatjuk a forráskódból való fordítást és telepítést is.

A fordítás (*compile*) a Linux világában jártas olvasó számára bizonyára nem ismeretlen fogalom, hiszen többnyire nyílt forráskódú programokkal találkozhatunk Linux alatt, és nem ritka, hogy amit keresünk, az az adott géptípusra csak forráskód formájában érhető el. A fordítás folyamata során a forráskódokat és egyéb kiegészítő fájlokat tartalmazó csomagból – megfelelő fordítóprogramok és egyéb függvénykönyvtárak megléte esetén – a program gépi kódú, az adott számítógép környezetéhez illeszkedő, bináris állományokká való átalakítása történik. Ez a folyamat kisebb programok esetében csak pár másodperc, nagyobbaknál akár sok-sok percig is eltarthat.

#### 2.1.2 Rakudo Star forrásból fordítása

Forrásból való telepítéskor akár a legutóbbi, stabil **Rakudo Star** összeállítást is választhatjuk. A Rakudo Star mindenkori aktuális változata a [rakudo.org/latest/star/src](https://rakudo.org/latest/star/src) címről tölthető le.

A letöltött .tar.gz csomag kibontása után a Rakudo Star fordítása Linux alatt egyetlen paranccsal végrehajtható, amennyiben a rendszeren a gcc, make és a perl csomagok már telepítve vannak. Az alábbi példákban a célkönyvtár az `/opt/raku`, ami könnyen karbantartható a későbbiekben:

```ksh
# cd rakudo-star-&lt;kiadás&gt;

# perl Configure.pl --gen-moar --make-install --prefix /opt/raku
...
```

#### 2.1.3 MoarVM, NQP és Rakudo forrásból fordítása egyenként

Ha kíváncsiak vagyunk a legújabb fejlesztésekre, vagy olyan rendszeren használnánk a környezetet, ahol a Rakudo Star helyett az összetevők csak külön-külön fordíthatók és telepíthetők (pl. AIX), esetleg csak egyszerűen szeretnénk a legújabb Rakut használni a tanulás, gyakorlás során, annak sincs akadálya.  

A sorrend különösen fontos, hiszen az egyik a másikra épül. 

Az alábbiakban a három összetevő általam utoljára ismert lelőhelye található meg:  
1. [MoarVM](https://www.moarvm.org/releases/)
2. [NQP](https://rakudo.org/dl/nqp)
3. [Rakudo](https://rakudo.org/dl/rakudo)

Fontos, hogy mindig ugyanabból a kiadásból töltsük le az egyes csomagokat. Pl.:

```ksh
# pwd
/usr/src

# ls -la *tar.gz
-rw-r-----. 1 root root 5451480 Mar  1 14:16 MoarVM-2021.02.tar.gz
-rw-r-----. 1 root root 3987077 Mar  1 14:41 nqp-2021.02.tar.gz
-rw-r-----. 1 root root 5709586 Mar  1 14:11 rakudo-2021.02.1.tar.gz
```

Miután a `.tar.gz` csomagokat letöltöttük, kicsomagoltunk, kezdődhet is a fordítás. A fordításhoz itt is a `gcc`, `make` és `perl` csomagok megléte az előfeltétel. 

Az opcionális, de ajánlott `make test`-ek futtatásához szükség van a `perl-Test-Harness` csomag meglétére is. 

A külön-külön fordítás és telepítés végeredménye nagyjából 63 MB körüli helyet foglal el az `/opt/raku` könyvtárban. 

Linux alatt a GCC, make, perl csomagokon kívül az alábbiakra (és azok függőségeire) is szükségünk lesz a fordításhoz, így gondoskodnunk kell ezek telepítéséről az adott disztribúció repository-jából. Keresés utánuk és telepítés pl. CentOS 8-on: 
```ksh
# dnf search perl | grep -E "Long|ExtUtils-Command|IPC-Cmd|SHA|Test-Harness"
Last metadata expiration check: 1:00:14 ago on Fri 07 May 2021 06:21:59 PM CEST.
perl-Digest-SHA.x86_64 : Perl extension for SHA-1/224/256/384/512
perl-ExtUtils-Command.noarch : Perl routines to replace common UNIX commands in Makefiles
perl-Test-Harness.noarch : Run Perl standard test scripts with statistics
perl-Getopt-Long.noarch : Extended processing of command line options
perl-IPC-Cmd.noarch : Finding and running system commands made easy

# dnf install perl-Digest-SHA.x86_64 perl-ExtUtils-Command.noarch perl-Test-Harness.noarch perl-Getopt-Long.noarch perl-IPC-Cmd.noarch
Last metadata expiration check: 0:52:07 ago on Fri 07 May 2021 06:21:59 PM CEST.
Dependencies resolved.
...
Is this ok [y/N]: y
...
Complete!
```

##### 2.1.3.1 MoarVM

```ksh
# cd MoarVM-&lt;kiadás&gt;

# perl Configure.pl --prefix /opt/raku
...

# make
...

# make install
...
```

##### 2.1.3.2 NQP

```ksh
# cd ../nqp-&lt;kiadás&gt;

# perl Configure.pl --prefix /opt/raku
...

# make
...

# make test
...

# make install
...
```

##### 2.1.3.3 Rakudo

```ksh
# cd ../rakudo-&lt;kiadás&gt;

# perl Configure.pl --prefix /opt/raku
...

# make
...

# make test
...

# make install
...

# ln -s /opt/raku/bin/raku /usr/bin/raku
```

#### 2.1.4  Tesztelés

Akár gyári csomagból, akár a Rakudo Star forrásából, akár az előző fejezetben ismertetett, egyenkénti fordítással telepítjük a Rakut, tesztelnünk kell azt, mielőtt használatba vesszük. 

Gyorsteszt: 

```ksh
# raku -v
Welcome to Rakudo(tm) v2021.02.1.
Implementing the Raku(tm) programming language v6.d.
Built on MoarVM version 2021.02.

# time raku -e 'print "Yeyy, it is Raku "~ $*RAKU.version ~" on ";shell "uname";'
Yeyy, it is Raku 6.d on Linux

real    0m0.168s
user    0m0.176s
sys     0m0.053s
```

Példa a számítási sebesség tesztjére:

```ksh
# time raku -e 'my $s; for 1..10000 {$s+=1/$_**2};say $s'
1.6448340718480652

real    0m0.202s
user    0m0.233s
sys     0m0.031s
```

Egy másik, jóval számításigényesebb teszt (de csak 1 CPU mag igénybevételével), ami egy erősebb gépen is több percig eltart: 

```ksh
# raku -v
Welcome to Rakudo(tm) v2021.02.1.
Implementing the Raku(tm) programming language v6.d.
Built on MoarVM version 2021.02.

# time raku -e 'my $s;for (1..10000) {my $i=$_; $s=$i*$i*$i;for (1..10000) {my $j=$_;$s=$j/$j/$j}};say $s'
0.0001

real    5m47.81s
user    2m33.07s
sys     0m0.01s
```

Több mag igénybevétele: 

```ksh
# time raku -e 'my @doubles = (1...10_000_000).hyper.map(* * 2);'
[...]
```


#### 2.1.4 MoarVM, NQP és Rakudo AIX-on

Az AIX-en való fordításhoz néhány további trükk is szükséges, amiről itt, [egy külön ismertetőben](/learning/it/prog/raku/aix) olvashatsz. 

### 2.2 A szövegszerkesztő

A Raku programok megírását egy egyszerű szövegszerkesztőben is elvégezhetjük, aminél különösen ajánlott a megfelelő szintaxiskiemelés. Ez azt jelenti, hogy a szerkesztőprogram az egyszerű szövegmegjelenítésen kívül a beállításoknak megfelelő színnel és stílussal emel ki bizonyos parancsszavakat, operátorokat, értékeket, továbbá a kommenteket valamilyen halvány színnel jelöli már alapértelmezetten is és így tovább. A szintaxiskiemelés persze csak egy jelentős előny, hiszen egyébként tökéletesen elegendő bármelyik ASCII szerkesztőprogram is, a megírt kódot pedig akár egyszerűen egy terminálban (Linux alatt a GNOME Terminal vagy Konsole programok segítségével), vagy Windows esetében DOS parancssorban futtathatjuk. Gyakran ennyiben ki is merül a fejlesztés menete: azaz a saját, kedvenc szerkesztőnkben megírjuk a programot, majd a szövegfájlt egyszerűen bemásoljuk a célszámítógép parancssorában egy ottani `.p6` vagy `.raku` állományba (pl. `cat > programom.raku`), majd azt ott futtatjuk, és figyeljük a kimenetét.

Mivel a Raku még egy viszonylag új dolog, ezért nem biztos, hogy a kedvenc kódszerkesztőnk szintén rendelkezik a Raku, azaz Perl 6-os szintaxis teljesen hibátlan megformázásának képességével. Bár a régi Perl kód szerinti kiemelés is megteszi az esetek 90%-ában, akadhatnak azért olyan karakterláncok az újban, amelyek kissé összezavarhatnak minket. Minden esetre, a széles körben ismert **Notepad++**-t Windowson, és a hozzá sok hasznos funkció tekintetében hasonló **Kate** szövegszerkesztőt Linux-on bátran ajánlom. Mindkettő esetében érdemes azonban arról is meggyőződnünk, hogy a program a `.p6` és/vagy `.raku` kiterjesztést Perl kódként kezelje már azonnal, a fájl megnyitásakor, és ne kelljen külön kiválasztanunk a Perl szintaxiskiemelést. Ennek beállítását láthatjuk a következő ábrán, ahol a Notepad++ **Settings** → **Style Configurator...** ablakában a `p6`-ot is felvesszük, mint egyéni kiterjesztést:

![alt_text](notepad++_style_configurator_p6.png)

Ha különlegesebb szerkesztői és fejlesztői képességeket is szívesen kipróbálnánk, akkor jelenleg a GitHub közösség által fejlesztett **Atom** nevű programja az, amely egy szinte mindent tudó integrált fejlesztői környezetként (IDE-ként) biztosan maradéktalanul alkalmas a Raku kód szintaxisának kiemelésére, és még egy sor további kényelmi funkciót tartalmaz.

A Perl programok fejlesztésének terén rendkívül erős versenytárs a **Komodo Edit** nevű szövegszerkesztő és fejlesztői környezet. A programot pillanatok alatt beállíthatjuk, hogy a Raku kódhoz tartozó fordítóprogram hol található, és a helyben fejlesztett programok esetében ez rendkívül nagy segítséget nyújthat. Az előbbiekben említett Nodepad++, Kate, Atom és Komodo Edit egyaránt tartalmaz egy rendkívül hasznos, extra képességet, a dupla kattintással kijelölt változó (vagy sok esetben egyszerűen bármely szó) minden további előfordulásának kiemelését a programkód teljes tartalmában.

Amit jelenleg a Rakuhoz is a legjobban ajánlok, az a **Microsoft Visual Studio Code**, röviden **VSCode**, ami bemutatása egy [külön leírást](/learning/it/prog/vscode) kapott, ugyanis az minden modern igénynek megfelel és többek között a Raku kóddal is kifogástalanul elbánik. 

<a name="3"></a>
## 3. Programírási alapok

A számítógépes program (computer program) a legegyszerűbb definíció szerint a számítógép hardverelemeit, erőforrásait együttműködésre bíró szoftver. Ez éppenséggel nem nevezhető egy könnyen befogadható megfogalmazásnak, úgyhogy gyorsan bontsuk is ezt le egy kicsit részletesebben, hogy hogyan is valósul ez meg a gyakorlatban, de továbbra is csak madártávlatból.

Történjen az akár emberi beavatkozásra, akár automatikusan, amikor a valamilyen tárolóeszközön tárolt program futni (execute) kezd, az elindított program utasítások segítségével arra kéri számítógépet, hogy a rendelkezésre álló adatokon műveleteket végezzen el. A programban a műveletek egymásra épülve, egymás után (sequence) hajtódnak végre, amelyek együttesen egy feldolgozást, számítást (computation) alkotnak. Ez a számítás gyakran valóban matematikai számításokat jelent, de lehet szimbolikus számítás is, amikor például szöveges adatok keresése, elkülönítése hajtódik végre, vagy egy videolejátszó program megnyit egy videofájlt.

Bármilyen feldolgozásról is legyen szó, valamint a szóban forgó program íródjon bármilyen programozási nyelven is, öt lényeges összetevő szinte minden programban megtalálható:

- **bemenet** (input): ez az adatok begyűjtésének folyamata, amelynek forrása lehet akár a billentyűzet, egy fájl, érkezhet a hálózaton át, létrehozhatja azt egy érzékelő vagy bármilyen egyéb eszköz
- **kimenet** (output): a kapott vagy feldolgozott adatok megjelenítése a képernyőn, mentése egy fájlba, elküldése a hálózaton át vagy küldése bármilyen egyéb eszközre
- **számítás** (math): alapszintű matematikai számítások végrehajtása a megadott feltételeknek megfelelően
- **elágazás** (conditional execution): az adatok alapszintű szétosztása kettő vagy több irányba a feltételeknek megfelelően (pl. „ha a felhasználó férfi, csináld ezt, ha nő, csináld azt”)
- **ismétlés** (iteration): művelet végrehajtása újra és újra addig, amíg a feltételek teljesítése érvényben van (pl. „addig kérd a felhasználót a születési évének megadására, amíg az egy elfogadható évszám nem lesz.” Ez lehet, hogy elsőre sikerül, de – egy ügyetlen felhasználó esetében – lehet, hogy csak a 48. alkalomra.).

Hogy a fenti alapműveletekből pontosan mennyi található egy programban, azokat egymáshoz képest milyen sorrendben, milyen részletekkel hozták létre, természetesen programonként eltérő. Pontos leírásukat a programkód tárolja. Ez a programkód általában egy vagy több sima szöveges állomány, definíciókkal, deklarációkkal, utasításokkal, kifejezésekkel és a programozó által közbeszúrt megjegyzésekkel saját maga, vagy más programozók számára.

Amennyiben gépi kóddá, azaz bináris állománnyá (Windows esetében pl. .exe állománnyá) alakító programozási környezettel van dolgunk, úgy egy fordítóprogram lesz az, ami értelmezi a kódot, hiba esetén figyelmeztet, a teljes mértékben feldolgozható kód esetén pedig lefordítja a programunkat. Ilyen fordítóprogram, többek között, a GNU C fordító vagy a Free Pascal.

A Raku esetében egy interpretáló nyelvről beszélhetünk, amely bináris állomány legyártása helyett feldolgozza és azonnal lefuttatja a programot. A futás szempontjából tehát egy lépéssel közvetlenebb kapcsolatot élhetünk át, hiszen a felhasználó számára láthatatlan folyamatokkal a háttérben mindössze egyetlen dolgot észlelünk: ha a szöveges fájlba írt programunk jó, annak futtatásakor a program bemenetére a felhasználót megkérve, vagy automatikusan érkező adatokból az eredmény a feldolgozás után szinte azonnal a kimenetre kerül, amiről a képernyőn is visszajelzést kapunk.

### 3.1 Előkészületek

Van tehát egy telepített Raku környezetünk, amelyet a `raku` vagy `perl6` parancs segítségével foghatunk munkára. Ahhoz, hogy ez a parancs egyáltalán kiadható legyen, a két következő feltétel legalább egyike szükséges:

- az indítóparancs egy szabványos útvonalon (`/usr/bin/`) található szimbolikus link (Linux/Unix rendszerek esetén)
- az indítóparancs benne van a `PATH` változóban (ez Linux/Unix vagy Windows rendszerek esetén egyaránt megoldható)

A két feltétel teljesítéséről nagyon egyszerűen meggyőződhetünk, akármilyen operációs rendszert is használunk. Linux/Unix rendszerek esetében a `which`, míg Windows-on a DOS parancssorba írt `where` parancs segítségével.

Linuxon (ebben a példában a korábbiakban már említett `/usr/bin/raku` szimbolikus link lett elkészítve):

```ksh
$ which raku perl6
/usr/bin/raku
/usr/bin/which: no perl6 in (/usr/local/bin: ... :/sbin)

$ ls -l /usr/bin/raku
lrwxrwxrwx 1 root root 19 Nov  5 15:52 /usr/bin/raku -> /opt/raku/bin/perl6
```

Windows-on (itt a telepítő alapértelmezett beállításaival, a `perl6.bat` jött létre):

```ksh
d:\>where raku perl6
C:\rakudo\bin\perl6.bat
INFO: Could not find "raku".
```

A továbbiakban a Raku indítófájljára `raku`-ként fogunk hivatkozni. A Raku verziójának, kiadásának kiírását a `-v` (egyszerű) vagy `-V` (részletes) kapcsolók valamelyikével tehetjük meg:

```ksh
$ raku -v
This is Rakudo version 2018.10 built on MoarVM version 2018.10
implementing Perl 6.c.
```

### 3.2 A „Hello világ!” program

A programozási nyelvekről szóló könyvek, útmutatók nélkülözhetetlen lépéseként essünk át a szinte már kötelező „Hello Világ”-on. Ugyan a szövegszerkesztő beállításáról szóló fejezet befejező lépéseként volt már lehetőségünk elsütnünk egy „Hello Világ”-ot, de nézzük át, milyen egyéb alternatívák állnak rendelkezésünkre, hogy kicsikarjuk ezt a Rakuból.

Ha a telepítés során minden rendben ment, rögtön három mód is nyílik arra, hogy használatba vegyük a Rakut:

- a Raku saját parancssori értelmezőjével, amelyet interpreter-nek, vagy **REPL**-nek is hívnak (*„Read, Evaluate, Print, Loop”*)
- a -e kapcsoló után egyetlen sorban, a rövidebb programunkat idézőjelek közé írva
- önálló forrásfájl készítésével és futtatásával

#### 3.2.1 A REPL

Kezdjük az egyszerű programunk megírását sorban az első megoldás a Raku saját, belső parancssorának használatával. Ehhez indítsunk el egy terminálprogramot (Linuxon pl. GNOME Terminal, Windows-on a Parancssori Ablak (Command Prompt)), majd írjuk be a `raku` parancsot. Ekkor egy belső parancsértelmezőbe jutunk, ahol gyakorolhatjuk az egyszerűbb utasításokat, majd az exit utasítással távozhatunk. A Raku `say` utasítása a neki paraméterként megadott szöveget, vagy változó(k) tartalmát hivatott megjeleníteni a képernyőn:

```ksh
$ raku
You may want to `zef install Readline` or `zef install Linenoise` or use rlwrap for a line editor

To exit type 'exit' or '^D'
> say "Hello Világ!";
Hello Világ!
> exit
```

#### 3.2.2 Az „egysoros”

Amennyiben nem elsősorban Raku programot írunk, hanem inkább csak egy shell szkript számára, de a Raku képességeit kihasználva egy rövid programot futtatnánk csak le, nagyon jól jöhet a második módszer, a raku parancs utáni `-e` kapcsoló használata:

```ksh
$ raku -e "say 'Hello világ';"
```

Az előbbi példa működik Linux/Unix és Windows rendszereken egyaránt, ugyanis a dupla idézőjel (") megfelel mindkét típusú rendszer követelményeinek, míg ha ezt szimpla idézőjellel (') tettük volna meg, az csak Linux/Unix rendszeren lenne eredményes.

Egy Linuxon tehát mindkét változat működni fog az ehhez hasonló, egysoros programokból:

```ksh
$ raku -e "say 'Hello világ';"
Hello világ

$ raku -e 'say "Hello világ";'
Hello világ
```

Természetesen az adott feladat bonyolultságától függően akár több deklarációból, több utasításból is állhat az egysorosunk:

```ksh
$ raku -e 'my $x=123; my $y=321; say $x*$y;'
39483
```

#### 3.2.3 Forrásfájl

A `-e` elhagyása esetén a forrásfájl nevét és esetleges elérési útját kell megadnunk, ami a sorban már a harmadik módszer Raku programok futtatására.

Összetettebb programokat már tanácsos egy önálló forrásállományban megírni. Ez a sokkal jobb áttekinthetőség mellett már csak amiatt is tanácsos, mert a továbbiakban felesleges lenne külön kitérni minden egyes alkalommal arra, hogy Windows-on így, Linux-on úgy kell beírnunk a soron következő programot.

A Linux/Unix világban a szkriptek legelső sora hagyományosan a "shebang", jelentése: "játékbarlang". Ez a Raku számára csak egy megjegyzést jelent, a Linux/Unix shell-ek számára viszont annak pontos meghatározása, hogy a chmod paranccsal futtathatóvá tett fájlt milyen program meghívásával kellene futtatnia:

```
#!/usr/bin/env raku

say "Hello világ!";
```

A programot a `raku` paranccsal is futtathatjuk, vagy Linux esetében egyszerűen adhatunk neki futtatási jogot, majd önálló programként is elindíthatjuk. Erre a kettőre láthatunk példát itt:

```ksh
$ raku hello.p6
Hello világ!

$ chmod +x hello.p6

$ ./hello.p6
Hello világ!
```

Most, hogy megismertük a felkínált programírási lehetőségeket, a továbbiakban a legpraktikusabbal, leggyakoribbal lenne érdemes továbbhaladnunk, tehát a programjainkat mindig igyekezzünk önálló fájlokban tárolni.

### 3.3 A program felépítése

Más programozási nyelvekkel ellentétben a felépítést tekintve sokkal nagyobb lazaság van. Van olyan meghatározni való is, amit szigorúan a program legelső sorában, olyan is, amit valahol az első soraiban kell elhelyeznünk, míg olyan is van, amit csak erősen ajánlott valahol az eleje felé beírnunk a rend, a jobb áttekinthetőség kedvéért.

- A program fejrésze
    - shebang
    - pragma-k bekapcsolása a `use`-al
    - modulok listája a `use`-al
- Definíciós és deklarációs rész
    - konstansok, változók deklarálása, osztályok és azok összetevőinek definíciója
- Főprogram
    - utasítások egymás után
    - utasítások szubrutinokban

### 3.4 Programírási szabályok

...

### 3.5 Szintaxis

A definíciók, deklarációk és utasítások végén mindig pontosvesszőt kell tennünk. A szövegtípusú adatokat idézőjelek közé írjuk. Ez lehet aposztróf (szimpla idézőjel) vagy dupla idézőjel is. Mindig olyannal zárjuk, amilyennel nyitottuk.

Rendkívül fontos, hogy a belső blokkok sorait egyenlő mértékű behúzásokkal tegyük jól áttekinthetővé. Ez a későbbi módosításokat, bővítéseket is elképesztő mértékben segíti, továbbá másokat is segítünk vele a programunk megértésében.

### 3.6 Megjegyzések

A programkód nem csak tömören a fordító számára értelmezendő kódokat kell, hogy tartalmazza, hanem a későbbi áttekinthetőség, bővíthetőség és más programozók megértését segítve, emberi olvasásra szánt megjegyzésekkel (comment) is ellátható. A Raku esetében ezt elsődlegesen a shell szkriptekből talán már jól ismert # karakterrel oldották meg.

Ne sajnáljunk megjegyzéseket beszúrni egy-egy lényegesebb funkció, lépés programkódon belüli dokumentálásához. Sőt, a # alkalmazása akkor is nagyon jól jöhet, ha egy-egy sort ideiglenesen, vagy véglegesen le szeretnénk tiltani, de szándékunkban áll azt szemléltetésképpen meghagyni. A következő példában a `++` aritmetikai operátor működésére láthatunk egy rövid megyarázatot egy egész soros megjegyzésben:

```
# működése megegyezik a következővel: $x = $x + 1; 
$x++;

További a példák egy vagy többsoros megjegyzések beszúrására:

# Ez itt egy egy soros komment

say "Hello világ!";  # Komment az utasítás mellett (gyakori)

say #`( Ez egy ritka, közbeszúrt komment ) "Hello világ!";

=begin
Ezek a sorokat
szintén kommentként
kezeli a fordító. (Pod szintaxis)
=end
```

A Perlhez hagyományosan egy POD (Plain Old Documentation) nevű jelölőnyelvvel írhatunk dokumentációt a Perlhez, Perl programokhoz és modulokhoz. Ugyanez a Raku esetében is használható. A megoldás akkor jön nagyon jól, ha több soros megjegyzést szeretnénk beszúrni. Szabályrendszere szerint a `=`-t követő bármilyen szövegtől a következő `=`-t követő bármilyen karakterig tart egy ilyen megjegyzés, így nem kell egyenként, a sorok elejére # karaktereket beszúrogatnunk.

### 3.7 Blokk és hatőkör

Az egymás után futtatandó, de egy vagy több közös jellemző meglétéhez kötött utasítások csoportját blokkokba rendezve kell elhelyeznünk. Az egész programunk, az elejétől a végéig is egy blokk, de könnyen kialakíthatunk belső, kisebb blokkokat is. Blokkot a `{` nyitó és `}` záró kapcsos-zárójel között hozhatunk létre. Ez a megoldás a vezérlési szerkezeteken kívül még további esetekben is hasznunkra válik majd. Íme, egy most még nem kifejezetten hasznos, de a fogalom szemléltesében remek példa:

```
{ say "Hello"; say "Világ" }
```

Láthatóan a második esetben a `say` utasítás paraméterének végére még csak nem is tettünk `;` karaktert, mégis a kód sikeresen, hiba vagy figyelmeztetés nélkül lefut. Érdemes azonban az utolsó utasítás után, tehát még a `}` előtt is kitenni a `;`-t, az esetleges későbbi bővítésekből keletkező hibák elkerülése miatt.

A fenti szerkezet többnyire az alábbi módon valósul meg vezérlési szerkezetekben, alprogramokban:

```
{
  say "Hello";
  say "Világ";
}
```

Fontos megjegyeznünk, hogy az utasításblokkon belül deklarált változók csak azon belül lesznek elérhetőek! A blokkba „érkező” változók értelmezhetőek (már a legegyszerűbb program is önmagában egy nagy blokkban van), de az azon belül létrehozott új változót később hiába próbálnánk meg kívül felhasználni, a Raku hibát fog kiírni. A blokkon belül további blokkok létrehozása természetesen lehetséges, így a fentieknek megfelelően a belső blokk szintén képes lesz használni az előző blokkban is már ismert változót.

Hogy egy kicsit jobban megértsük a blokkok, egyben a vezérlési szerkezetek működését, lássuk, mit tud a kategória legegyszerűbb, `do` utasítása.

#### 3.7.1 do

A `do` a blokk belsejében található utasítássort egyetlen alkalommal futtatja le:

```
my $x = 13;

do {
  $x++;
}
say $x;   # 14
```

Ennek az egyszerű programblokknak természetesen még nincs sok értelme, de működését tekintve kiválóan szemlélteti az ilyen jellegű utasítássorok megvalósítását.

<a name="4"></a>
## 4. Konstansok és változók

...


### 4.1 A sigil

A Perlben és a Rakuban egy egészen hatékony megoldással, a konstans vagy változó neve elé kitett különleges karakterrel, a **sigil**-el (*„szigil”*, vagy magyarosítva *„szigla”*) határozhatjuk meg a típust. Ez jelzi egyértelműen, hogy a 4 fő változótípusból épp melyikkel van dolgunk.

```
$scalar  # egy skaláris változót jelöl
@array   # egy tömböt jelöl
%hash    # egy hash-t (asszociatív tömb) jelöl
&subr    # egy szubrutin meghívását jelöli
```

### 4.2 Skaláris változó

### 4.3 Tömb

### 4.4 Asszociatív tömb: a hash

### 4.5 Szubrutinra mutató változó

### 4.6 Konstans

Amennyiben egy értéket a programban többször is felhasználnánk, illetve a későbbi, esetleges módosítások, bővítések során egy jól elkülöníthető helyről szeretnénk karbantartani, tökéletes választás lehet a konstans használata.

```
constant GREET = 'Üdvözlet';

say GREET;

constant $PI = 3.14;
constant @GYUMI = ('alma', 'dió', 'mogyoró');

say 30*30*$PI;
say @GYUMI[2];
```

A fenti program kimenete:

```ksh
2826
mogyoró
```

<a name="5"></a>
## 5. Vezérlési szerkezetek

A Raku az összes közismert vezérlési szerkezet használatára lehetőséget biztosít. Ezek közül rendszerint csak néhányat használunk a gyakorlatban, de a ritkább esetekre rendelkezésünkre állnak akár egészen egzotikusak is. Ami mindegyikükre egyaránt jellemző, az az, hogy a vezérlési szerkezetek előzetes vizsgálatának eredményeként lefuttatandó utasítások során egy blokkba helyezzük.

### 5.1 Elágazások


#### 5.1.1 if, elif, else

...

```
if feltétel {
  utasítás;
}
```

...

```
if feltétel {
  utasítás;
} else {
    utasítás_egyéb_esetben;
}
```

...

```
if feltétel {
  utasítás;
} elsif másik_feltétel {
    utasítás_másik_esetben;
} else {
    utasítás_minden_más_esetben;
}
```

...

#### 5.1.2 unless

...

```
unless feltétel {
  utasítás;
}
```

Az `unless` segítségével kizárólag egyirányú elágazások alkothatóak. (Ha lehetne, az valószínűleg csak félreértésekhez vezetne.) Tulajdonképpen az `if`-el is megoldható minden, de mégis akadhatnak esetek, amikor egy egyirányú elágazás unless-el megvalósítva rövidebb.

#### 5.1.3 given, when

...

#### 5.1.4 with

...

### 5.2 Ciklusok

...

#### 5.2.1 for

...

```
say "for";
for (1..5) -> $x {
  $x.say;
}
```

...

#### 5.2.2 loop

...

```
my $x = 1;

say "loop";
loop ($x = 1; $x < 5; $x++) {
  $x.say;
}
say "A végén: $x";
```

...

#### 5.2.3 while

...

```
my $x = 1;

say "while";
while $x > 5 {
  $x++;
  $x.say;
}
say "A végén: $x";
```

...

#### 5.2.4 repeat/while

...


```
my $x = 1;

say "repeat/while";
repeat {
  $x++;
  $x.say;
} while $x > 5;
say "A végén: $x";
```

...


#### 5.2.5 until

...


```
my $x = 1;

say "until";
until $x > 5 {
  $x++;
  $x.say;
}
say "A végén: $x";
```

...


#### 5.2.6 repeat/until

...


```
my $x = 1;

say "repeat/until";
repeat until $x > 5 {
  $x++;
  $x.say;
}
say "A végén: $x";
```

...


#### 5.2.7 Címkék

...


```
say "for, de next-el";
for (1..5) -> $x {
  next if $x == 3;
  $x.say;
}
```

...


```
say "for, de last-al";
for (1..5) -> $x {
  last if $x == 3;
  $x.say;
}
```

...


<a name="6"></a>
## 6. Operátorok

Az alábbi táblázat a leggyakoribb operátorok listáját tartalmazza:

<table class="tan">
  <tr>
    <th>Operátor</th>
    <th>Típus</th>
    <th>Leírás</th>
    <th>Példa</th>
    <th>Eredmény</th>
  </tr>
  <tr>
    <td><p><code>+</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Addition</p></td>
    <td><p><code>1 + 2</code></p></td>
    <td><p><code>3</code></p></td>
  </tr>
  <tr>
    <td><p><code>-</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Subtraction</p></td>
    <td><p><code>3 - 1</code></p></td>
    <td><p><code>2</code></p></td>
  </tr>
  <tr>
    <td><p><code>*</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Multiplication</p></td>
    <td><p><code>3 * 2</code></p></td>
    <td><p><code>6</code></p></td>
  </tr>
  <tr>
    <td><p><code>**</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Power</p></td>
    <td><p><code>3 ** 2</code></p></td>
    <td><p><code>9</code></p></td>
  </tr>
  <tr>
    <td><p><code>/</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Division</p></td>
    <td><p><code>3 / 2</code></p></td>
    <td><p><code>1.5</code></p></td>
  </tr>
  <tr>
    <td><p><code>div</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Integer Division (rounds down)</p></td>
    <td><p><code>3 div 2</code></p></td>
    <td><p><code>1</code></p></td>
  </tr>
  <tr>
    <td><p><code>%</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Modulo</p></td>
    <td><p><code>7 % 4</code></p></td>
    <td><p><code>3</code></p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>%%</code></p></td>
    <td rowspan="2"><p><code>Infix</code></p></td>
    <td rowspan="2"><p>Divisibility</p></td>
    <td><p><code>6 %% 4</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>6 %% 3</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>gcd</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Greatest common divisor</p></td>
    <td><p><code>6 gcd 9</code></p></td>
    <td><p><code>3</code></p></td>
  </tr>
  <tr>
    <td><p><code>lcm</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Least common multiple</p></td>
    <td><p><code>6 lcm 9</code></p></td>
    <td><p><code>18</code></p></td>
  </tr>
  <tr>
    <td><p><code>==</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric equal</p></td>
    <td><p><code>9 == 7</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>!=</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric not equal</p></td>
    <td><p><code>9 != 7</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>&lt;</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric less than</p></td>
    <td><p><code>9 &lt; 7</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>&gt;</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric greater than</p></td>
    <td><p><code>9 &gt; 7</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>&lt;=</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric less than or equal</p></td>
    <td><p><code>7 &lt;= 7</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>&gt;=</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Numeric greater than or equal</p></td>
    <td><p><code>9 &gt;= 7</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td rowspan="3"><p><code>&lt;=&gt;</code></p></td>
    <td rowspan="3"><p><code>Infix</code></p></td>
    <td rowspan="3"><p>Numeric three-way comparator</p></td>
    <td><p><code>1 &lt;=&gt; 1.0</code></p></td>
    <td><p><code>Same</code></p></td>
  </tr>
  <tr>
    <td><p><code>1 &lt;=&gt; 2</code></p></td>
    <td><p><code>Less</code></p></td>
  </tr>
  <tr>
    <td><p><code>3 &lt;=&gt; 2</code></p></td>
    <td><p><code>More</code></p></td>
  </tr>
  <tr>
    <td><p><code>eq</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String equal</p></td>
    <td><p><code>"John" eq "John"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>ne</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String not equal</p></td>
    <td><p><code>"John" ne "Jane"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>lt</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String less than</p></td>
    <td><p><code>"a" lt "b"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>gt</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String greater than</p></td>
    <td><p><code>"a" gt "b"</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>le</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String less than or equal</p></td>
    <td><p><code>"a" le "a"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>ge</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>String greater than or equal</p></td>
    <td><p><code>"a" ge "b"</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td rowspan="3"><p><code>leg</code></p></td>
    <td rowspan="3"><p><code>Infix</code></p></td>
    <td rowspan="3"><p>String three-way comparator</p></td>
    <td><p><code>"a" leg "a"</code></p></td>
    <td><p><code>Same</code></p></td>
  </tr>
  <tr>
    <td><p><code>"a" leg "b"</code></p></td>
    <td><p><code>Less</code></p></td>
  </tr>
  <tr>
    <td><p><code>"c" leg "b"</code></p></td>
    <td><p><code>More</code></p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>cmp</code></p></td>
    <td rowspan="2"><p><code>Infix</code></p></td>
    <td rowspan="2"><p>Smart three-way comparator</p></td>
    <td><p><code>"a" cmp "b"</code></p></td>
    <td><p><code>Less</code></p></td>
  </tr>
  <tr>
    <td><p><code>3.5 cmp 2.6</code></p></td>
    <td><p><code>More</code></p></td>
  </tr>
  <tr>
    <td><p><code>=</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Assignment</p></td>
    <td><p><code>my $var = 7</code></p></td>
    <td><p>Assigns the value of <code>7</code> to the variable <code>$var</code></p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>~</code></p></td>
    <td rowspan="2"><p><code>Infix</code></p></td>
    <td rowspan="2"><p>String concatenation</p></td>
    <td><p><code>9 ~ 7</code></p></td>
    <td><p><code>97</code></p></td>
  </tr>
  <tr>
    <td><p><code>"Hi " ~ "there"</code></p></td>
    <td><p><code>Hi there</code></p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>x</code></p></td>
    <td rowspan="2"><p><code>Infix</code></p></td>
    <td rowspan="2"><p>String replication</p></td>
    <td><p><code>13 x 3</code></p></td>
    <td><p><code>131313</code></p></td>
  </tr>
  <tr>
    <td><p><code>"Hello " x 3</code></p></td>
    <td><p><code>Hello Hello Hello</code></p></td>
  </tr>
  <tr>
    <td rowspan="5"><p><code>~~</code></p></td>
    <td rowspan="5"><p><code>Infix</code></p></td>
    <td rowspan="5"><p>Smart match</p></td>
    <td><p><code>2 ~~ 2</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>2 ~~ Int</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>"Perl 6" ~~ "Perl 6"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>"Perl 6" ~~ Str</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>"enlightenment" ~~ /light/</code></p></td>
    <td><p><code>｢light｣</code></p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>++</code></p></td>
    <td><p><code>Prefix</code></p></td>
    <td><p>Increment</p></td>
    <td><p><code>my $var = 2; ++$var;</code></p></td>
    <td><p>Increment the variable by 1 and return the result <code>3</code></p></td>
  </tr>
  <tr>
    <td><p><code>Postfix</code></p></td>
    <td><p>Increment</p></td>
    <td><p><code>my $var = 2; $var++;</code></p></td>
    <td><p>Return the variable <code>2</code> and then increment it</p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>--</code></p></td>
    <td><p><code>Prefix</code></p></td>
    <td><p>Decrement</p></td>
    <td><p><code>my $var = 2; --$var;</code></p></td>
    <td><p>Decrement the variable by 1 and return the result <code>1</code></p></td>
  </tr>
  <tr>
    <td><p><code>Postfix</code></p></td>
    <td><p>Decrement</p></td>
    <td><p><code>my $var = 2; $var--;</code></p></td>
    <td><p>Return the variable <code>2</code> and then decrement it</p></td>
  </tr>
  <tr>
    <td rowspan="3"><p><code>+</code></p></td>
    <td rowspan="3"><p><code>Prefix</code></p></td>
    <td rowspan="3"><p>Coerce the operand to a numeric value</p></td>
    <td><p><code>+"3"</code></p></td>
    <td><p><code>3</code></p></td>
  </tr>
  <tr>
    <td><p><code>+True</code></p></td>
    <td><p><code>1</code></p></td>
  </tr>
  <tr>
    <td><p><code>+False</code></p></td>
    <td><p><code>0</code></p></td>
  </tr>
  <tr>
    <td rowspan="3"><p><code>-</code></p></td>
    <td rowspan="3"><p><code>Prefix</code></p></td>
    <td rowspan="3"><p>Coerce the operand to a numeric value and return the negation</p></td>
    <td><p><code>-"3"</code></p></td>
    <td><p><code>-3</code></p></td>
  </tr>
  <tr>
    <td><p><code>-True</code></p></td>
    <td><p><code>-1</code></p></td>
  </tr>
  <tr>
    <td><p><code>-False</code></p></td>
    <td><p><code>0</code></p></td>
  </tr>
  <tr>
    <td rowspan="6"><p><code>?</code></p></td>
    <td rowspan="6"><p><code>Prefix</code></p></td>
    <td rowspan="6"><p>Coerce the operand to a boolean value</p></td>
    <td><p><code>?0</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>?9.8</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>?"Hello"</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>?""</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>my $var; ?$var;</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>my $var = 7; ?$var;</code></p></td>
    <td><p><code>True</code></p></td>
  </tr>
  <tr>
    <td><p><code>!</code></p></td>
    <td><p><code>Prefix</code></p></td>
    <td><p>Coerce the operand to a boolean value and return the negation</p></td>
    <td><p><code>!4</code></p></td>
    <td><p><code>False</code></p></td>
  </tr>
  <tr>
    <td><p><code>..</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Range Constructor</p></td>
    <td><p><code>0..5</code></p></td>
    <td><p>Creates a range of the interval [0, 5] <sup class="footnote" id="_footnote_intervals">[<a id="_footnoteref_1" class="footnote" href="#_footnotedef_1" title="View footnote.">1</a>]</sup></p></td>
  </tr>
  <tr>
    <td><p><code>..^</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Range Constructor</p></td>
    <td><p><code>0..^5</code></p></td>
    <td><p>Creates a range of the interval [0, 5) <sup class="footnoteref">[<a class="footnote" href="#_footnotedef_1" title="View footnote.">1</a>]</sup></p></td>
  </tr>
  <tr>
    <td><p><code>^..</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Range Constructor</p></td>
    <td><p><code>0^..5</code></p></td>
    <td><p>Creates a range of the interval (0, 5] <sup class="footnoteref">[<a class="footnote" href="#_footnotedef_1" title="View footnote.">1</a>]</sup></p></td>
  </tr>
  <tr>
    <td><p><code>^..^</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Range Constructor</p></td>
    <td><p><code>0^..^5</code></p></td>
    <td><p>Creates a range of the interval (0, 5) <sup class="footnoteref">[<a class="footnote" href="#_footnotedef_1" title="View footnote.">1</a>]</sup></p></td>
  </tr>
  <tr>
    <td><p><code>^</code></p></td>
    <td><p><code>Prefix</code></p></td>
    <td><p>Range Constructor</p></td>
    <td><p><code>^5</code></p></td>
    <td><p>Same as 0..^5 Creates a range of the interval [0, 5) <sup class="footnoteref">[<a class="footnote" href="#_footnotedef_1" title="View footnote.">1</a>]</sup></p></td>
  </tr>
  <tr>
    <td><p><code>&#8230;&#8203;</code></p></td>
    <td><p><code>Infix</code></p></td>
    <td><p>Lazy List Constructor</p></td>
    <td><p><code>0&#8230;&#8203;9999</code></p></td>
    <td><p>Return the elements only if requested</p></td>
  </tr>
  <tr>
    <td rowspan="2"><p><code>|</code></p></td>
    <td rowspan="2"><p><code>Prefix</code></p></td>
    <td rowspan="2"><p>Flattening</p></td>
    <td><p><code>|(0..5)</code></p></td>
    <td><p><code>(0 1 2 3 4 5)</code></p></td>
  </tr>
  <tr>
    <td><p><code>|(0^..^5)</code></p></td>
    <td><p><code>(1 2 3 4)</code></p></td>
  </tr>
</table>

<a name="7"></a>
## 7. Reguláris kifejezések

A reguláris kifejezéseket általában a Smart Match (`~~`) operátorral együtt használjuk.
Pl.:

```
if "karalábé" ~~ / láb / {
  say "A karalábéban van láb.";
}
```

A kifejezést sokféleképpen definiálhatjuk, amelyek közül az általános gyakorlat a `/ láb /` formátum. Íme néhány további példa:

```
/láb/
m/láb/
m/ láb /
rx/láb/
m|láb|
m"láb"
m§láb§
```

A Smart Match (`~~`) operátor tehát az, ami általában megelőzi a reguláris kifejezést. A `!~~` karaktersorozat a Smart Match operátor ellentéte: az ezutáni reguláris kifejezés helyett bármi más találatot eredményez.

...
```
/:s ... / - a szóközök is beleszámítanak a mintába
/^ ... / - előtte nincs szóköz, vagy más, speciális karakter
/^^ ... / - pontosan a sor elejétől kell kezdődnie a keresendő mintának
/ ... $/ - utána nincs szóköz, vagy más, speciális karakter
/ ... $$/ - pontosan a sor végéig kell tartania a keresendő mintának
/ $ssid / - egy változó értékének beszúrása keresendő kifejezésként
/ $(%sscld{$ssid}) / - egy tömb vagy hash értékének beszúrása keresendő kifejezésként
```
...

<a name="8"></a>
## 8. Fájl I/O

...

Egy fájl beolvasása legegyszerűbben a `slurp` (szürcsölés) utasítással végezhető el.

Íme, egy tipikus alkalmazási mód:

```
my $tmpfile = "/tmp/tmpfile";
...
my $tccsv = $tmpfile.IO.slurp || die say "The \""~ $tmpfile ~"\" file cannot be opened. :(";
for lines $tccsv -> $row {
  ...
}
...
```

...

A fájlba írás műveletét - számos egyéb módszer mellett - a `spurt` (kiköpés) utasítással tanácsos megoldanunk.

A következő példában a fájlt elsőként kiürítjük (akár létezik, akár nem), majd egy esetben hozzá is fűzünk egy sort az `:append` opció segítségével:

```
my $cltmp = "/tmp/nwcltmp";
...
spurt $cltmp, "";
...
spurt $cltmp, "$ssid/@ssrow[1]\n", :append;
...
```

...

Egy fájl létezésének vizsgálata egészen egyszerűen kivitelezhető az `IO.e` utasítás meghívásával. Pl.:

```
if $tctmp.IO.e {
  say "Done. The file has "~ $tctmp.IO.lines.elems ~" lines.";
} else {
  say "Error: $tctmp is not ready.";
}
```
...

<a name="9"></a>
9. Alprogramok

<a name="10"></a>
10. Modulok

<a name="11"></a>
11. Osztályok és objektumok

<a name="12"></a>
12. A nyelvtan (grammar) használata

...