# ![alt_text](/_ui/img/icons/powerha_m.svg) IBM PowerHA SystemMirror

###### .

## Tartalomjegyzék

1. [Bevezetés](#1)
2. [Alapfogalmak](#2)
3. [Napi ellenőrzések](#3)
4. [Rendszeres karbantartások](#4)
5. [Cluster tervezése](#5)
6. [Cluster fileset-ek, előfeltételek, telepítés, eltávolítás](#6)
7. [Cluster konfigurálás az alapoktól](#7)
8. [Clustertesztelés](#8)

<a name="1"></a>
## 1. Bevezetés

Az **IBM PowerHA SystemMirror** (korábban csak **IBM PowerHA**, mégkorábban **HACMP**) az IBM megoldása magas elérésű *(High-Availability, HA)* clusterekhez AIX és Linux for IBM System p platformokon. A HACMP a High Availability Cluster Multiprocessing rövidítéséből származik. A HACMP első stabil verziója 1991-ben jelent meg.

Lényege, hogy bár a szolgáltatás csak egy számítógép felől érkezik a végfelhasználó felé, ám ez a számítógép összeköttetésben van egy (vagy maximum 15) másik számítógéppel (node-al), amikkel folyamatosan kapcsolatban áll, így egy esetleges katasztrófa vagy szándékos karbantartás esetén a másodlagos node (standby node) azonnal és automatikusan átveszi a szolgáltatás biztosítását, így a végfelhasználó mit sem érzékel a pillanatnyi kimaradásból. 

Ezt az alábbi ábra próbálja szemléltetni: 

![alt_text](powerha_concepts_full.png)

A **PowerHA SystemMirror for AIX Enterprise Edition** magában foglalja a **Standard Edition**-t, valamint további képességekkel ruházza fel azt, mint pl. failoverelés távoli lokációkra.

Az Enterprise Edition az alábbiakat támogatja storage oldalról: 

- Metro Mirror/Global Mirror in DS8700 and DS8800 configurations
- HyperSwap in a Metro Mirror with DS8800 configuration
- Global Mirror/Metro in an SVC or V7000 configuration
- Global Mirror/Metro Mirror in an XIV configuration
- GLVM sync and async mode 

<a name="2"></a>
## 2. Alapfogalmak

### 2.1 Standalone kiépítés

Az alábbiakban láthatjuk az alapesetet **standalone** kiépítés esetén. Van egy db szerverünk, arra lokálisan telepítve van egy "app" (ami lehet bármi: webes applikáció, adatbázis stb.). Amennyiben ez egy DB, arról általában az mondható el, hogy az egy (vagy több) elkülönített diszken tárolja az adatait. 

![alt_text](powerha_concepts_standalone.png)

Ezt bővíthetjük ki, mint egy építőkockákból összerakott építményt a fenti, teljes kiépítéssé. 

### 2.2 A cluster összetevői

Egyes részek itt már annyira hülyén hangoznának magyarul, hogy érjük be az angol megfogalmazással: 

- **cluster**: the environment that contains the nodes, the cluster configuration, the involved disks, network interfaces, IPs, application start and stop scripts etc.
- **node**: represented by an AIX system (LPAR) itself. Usually we can reference to them as nodeA and nodeB
- **boot IP**: the IP which becomes online when the AIX just boots, like in a standalone environment. Boot ip is included in PowerHA monitoring and is the primary IP of the interface. It may be a fake IP like 1.1.1.1 which you can never reach from outside.
- **persistent IP**: also a kind of boot IP (so maintained by AIX and not by PowerHA) but it must be in the same network as the service IP is. Persistent IP is an alias, which you use when you have a problem with your service IP and the node is unreachable. So you have a persistent IP in order to reach the node.
- **service IP**: the IP which is accessed by the connecting external applications / customer, so obviously this is one of the most precious resources in the cluster. The service IP is handled by PowerHA.
- **heartbeat disk**: in previous PowerHA versions, we used this component to have a non-IP based communication layer between the two nodes to send and recieve continuous heartbeat messages. So, it also must be a shared disk. There can be more than one HB disks involved and configured. If a predefined number of heartbeats are missed, then the non-active node may "decide" to take the resources over.
- **repository disk**: PowerHA SystemMirror uses this new component while it does not use HB disks anymore. The repo disk is a central repository for all cluster topology-related information and must be shared by all servers in the cluster. The repository disk is also used for the heartbeat mechanism. Important: only one repo disk is allowed, it must be at least 2 GB in size (5 GB is recommended).
- **shared VG**: the most precious component where the customer data resides, so it is shared between the participant nodes, and it can be writable only on one of the nodes at a time
- **resource group**: the collection of components that all should become available to get the RG online. Mainly these components are: filesystems, service IP, application
- **application**: services / processes that are started by a simple shell script that is issued as the last step when a cluster brings a RG online
- **failover**: an automated event for moving the RG, which event is usually caused by unexpected outage of an interface or the whole active node
- **takeover**: a human-initated event to switch the RG for testing purposes or e.g. when the administrator would like to take an APAR update on the active node

### 2.3 Katasztrófa-esemény

Elsősorban emiatt létezik az egész clusteres környezetünk, annak azonnali és automatikus lekezelése miatt. 

![alt_text](powerha_concepts_disaster.png)

### 2.4 Concurrent? Non-concurrent?

A szolgáltatáshoz szükséges adatokat tartalmazó shared VG-k "**Concurrent Capable**" beállítása manapság már rendszerint Enhanced Concurrent, ami többnyire csak annyit jelent, hogy mindegyik node `lspv` kimenetében `concurrent`-ként láthatóak, mindegyik node-on lekérdezhetjük a VG beállításait, LV-inek, azok FS-einek a listáját, de mountolva továbbra is ugyanúgy csak az aktív node-on vannak.  Elméletileg lehetséges lenne a párhuzamos hozzáférés, de az adatintegritás már az applikáción múlna, így a gyakorlatban csak az aktív node-on történik fájlrendszerszintű írás/olvasás. A RG átmozgatását viszont már ez a kiépítés is jelentősen gyorsítja. 

Az **aktív** node-on ez így néz ki pl.:

```ksh
# lspv
hdisk0          00c60f370aefbdec                    rootvg          active
hdisk1          00c60f374a254331                    datavg          concurrent
hdisk2          00c59a0779ba8110                    caavg_private   active
hdisk3          00c60f374a011ccb                    None
 
# lsvg -l datavg
datavg:
LV NAME             TYPE       LPs     PPs     PVs  LV STATE      MOUNT POINT
nginxlv             jfs2       11      11      1    open/syncd    /etc/nginx
 
# lsvgfs datavg | xargs df -m
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/nginxlv    2816.00   2504.14   12%       26     1% /etc/nginx
```

A **standby** node-on ugyanez: 

```ksh
# lspv
hdisk0          00c60f370aefbdec                    rootvg          active
hdisk1          00c60f374a254331                    datavg          concurrent
hdisk2          00c59a0779ba8110                    caavg_private   active
hdisk3          00c60f374a011ccb                    None
 
# lsvg -l datavg
datavg:
LV NAME             TYPE       LPs     PPs     PVs  LV STATE      MOUNT POINT
nginxlv             jfs2       11      11      1    closed/syncd  /etc/nginx
 
# lsvgfs datavg | xargs df -m
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/hd4         768.00    448.00   42%    19742    15% /
```

Erről a kérdésről [ebben a cikkben](http://unix.worldiswelcome.com/non-concurrent-concurrent-and-enhanced-concurrent-volume-groups) olvashatunk bővebben.

Tehát:

>**Non Concurrent VG:**  
>In Non Concurrent VG, application runs typically on a single node only and data of VG is accessible to that node only. If however present node fails then application moves to other nodes and VG is varied on that node. Then only present node can access the data on VG.  
>&nbsp;  
>**Concurrent VG:**  
>In concurrent VG, the application runs on all nodes of cluster simultaneously. All of the nodes can access the data at the same time. All of the nodes can read and write the data simultaneously. The data integrity is then the responsibility of the application.  
>&nbsp;  
>**Enhanced Concurrent VG:**  
>In enhanced concurrent VG, access to VG is allowed on all of the nodes of cluster. Certain restrictions apply in this case. The VG can be varied on different nodes in active or passive states.  
>&nbsp;  
>-Active State Varyon:  
>In active state varyon of VG the following operations are allowed:  
>\- Filesystem operations and mounting of filesystems.  
>\- Operations on Logical Volumes.  
>\- Operations on application.  
>\- Syncing of VGs.  
>&nbsp;  
>-Passive State Varyon  
>In passive state varyon of VG certain restrictions apply:  
>\- Filesystems mounting and operations on filesystems are not allowed.  
>\- No operations on LVs are allowed.  
>\- Syncing of VGs is not allowed.  
>&nbsp;  
>The following are allowed:  
>\- LVM read only access to VGs special file is allowed.  
>\- LVM read only access to first 4k of all Lvs under VG is allowed.

<a name="3"></a>
## 3. Napi ellenőrzések

### 3.1 A legalapvetőbb ellenőrzés

Az általános, AIX-os service ellenőrző paranccsal, az `lssrc`-vel jól feltűnik, ha a `cluster` service groupból semmi vagy legfeljebb egy (a legelső) service aktív. 

A service-eken túl az első dolog, amivel meggyőződhetünk a cluster RG-jének aktivitásáról, a `clRGinfo`: 

```ksh
# lssrc -g cluster
Subsystem         Group            PID          Status
 clstrmgrES       cluster          15466736     active
 clevmgrdES       cluster          13434894     active
 clinfoES         cluster          11403354     active


# clRGinfo
----------------------------------------------------
Group Name                   State            Node
----------------------------------------------------
cheese_RG                    ONLINE           tom
                             OFFLINE          jerry
```

Így néz ki, ha a PowerHA telepítve van ugyan (valószínűleg konfigurálva is), de sem a service-ei, sem maga a cluster nem online: 

```ksh
# lssrc -g cluster
Subsystem         Group            PID          Status
 clstrmgrES       cluster          17367064     active

# clRGinfo
Cluster IPC error: The cluster manager on node tom is in ST_INIT or NOT_CONFIGURED state and cannot process the IPC request.
```

### 3.2 A clusteres parancsok elérési útvonala

Megeshet, hogy az előző és a többi clusteres parancsot nem találja a rendszer. Ilyenkor ellenőrizzük a `$PATH` változót és szükség esetén javítsuk. Ez a normális állapot: 

```ksh
# which clRGinfo
/usr/es/sbin/cluster/utilities/clRGinfo

# which clstat
/usr/es/sbin/cluster/clstat

# echo $PATH
/usr/bin: ... /usr/es/sbin/cluster:/usr/es/sbin/cluster/utilities:/usr/es/sbin/cluster/sbin:/usr/es/sbin/cluster/cspoc
```

### 3.3 Valósidejű monitorozás

Ennek gyári megoldása a `clstat`: 

```ksh
# clstat

                clstat - PowerHA SystemMirror Cluster Status Monitor
                -------------------------------------

Cluster: cheese (1400496933)
Wed Dec  5 10:14:24 CET 2018
                State: UP               Nodes: 2
                SubState: STABLE

        Node: jerry             State: UP
           Interface: jerry-en20 (0)            Address: 192.168.10.11
                                                State:   UP
           Interface: jerry-en21 (0)            Address: 192.168.20.11
                                                State:   UP

        Node: tom               State: UP
           Interface: tom-en20 (0)              Address: 192.168.10.10
                                                State:   UP
           Interface: tom-en21 (0)              Address: 192.168.20.10
                                                State:   UP
           Interface: cheese-b (0)              Address: 10.163.110.36
                                                State:   UP
           Resource Group: cheese_RG                    State:  On line
```

Ez azonban gyakran nem működik. Általában azért nem, mert a neki szükséges SNMPD helytelenül van konfigurálva (ennek javítása később következik). 

Egy külön fejlesztett, SNMPD-től független, scriptes megoldást nyújt a `qha`. A script az alábbi oldalról tölthető le: [sourceforge.net/projects/qha](https://sourceforge.net/projects/qha/). 

A program kapcsolók nélküli, minimális áttekintést nyújtó kimenete: 

```ksh
# qha

           Cluster: cheese (7221)
                10:15:16 05Dec18

jerry    iState: ST_STABLE

tom      iState: ST_STABLE
 cheese_RG          ONLINE
```

Az interface-eket is részletező mód: 

```ksh
# qha -nev
           Cluster: cheese (7221)
                10:06:34 05Dec18

jerry    iState: ST_STABLE
 en10 jerry
en11 jerryboot
en20 jerry-en20 jerry-b
en21 jerry-en21
- -

tom      iState: ST_STABLE
 cheese_RG          ONLINE
 en10 tom
en11 tomboot
en20 tom-en20 tom-b
en21 cheese-b tom-en21
- datavg(1) -
```

### 3.4 Topológia

A `cltopinfo` nagyjából a legfontosabb parancsunk a `clRGinfo` után, mert segítségével azonnal megismerhetjük, áttekinthetjük a cluster legfontosabb összetevőit: 

```ksh
# cltopinfo
Cluster Name:    cheese
Cluster Type:    Standard
Heartbeat Type:  Unicast
Repository Disk: hdisk3 (00c59a0779ba8110)

There are 2 node(s) and 1 network(s) defined

NODE jerry:
        Network cheese_net
                cheese-b        10.163.110.36
                jerry-en21      192.168.20.11
                jerry-en20      192.168.10.11

NODE tom:
        Network cheese_net
                cheese-b        10.163.110.36
                tom-en20        192.168.10.10
                tom-en21        192.168.20.10

Resource Group cheese_RG
        Startup Policy   Online On Home Node Only
        Fallover Policy  Fallover To Next Priority Node In The List
        Fallback Policy  Never Fallback
        Participating Nodes      tom jerry
        Service IP Label                 cheese-b
```

A fenti példában a `cheese-b` a Service IP címkéje (az `/etc/hosts` fájlban is ugyanez van). 

A legalsó szekcióban (vagy szekciókban, ha több RG van) láthatjuk az alábbiakat: 

- **Startup Policy**: mi történjen, ha elindul akár az összes node-on a cluster service group. 
- **Fallover Policy**: mi történjen, ha a nagyobb prioritású node kiesik a clusterből (disaster vagy hanyagul tervezett karbantartás miatt)
- **Fallback Policy**: mi történjen, ha a nagyobb prioritású node újra visszakapcsolódik a clusterbe
- **Participating Nodes**: a node-ok listája prioritási sorrendben
- **Service IP Label**: a fentebbi, interface-es listában melyik címke az, ami az adott RG-al van összepárosítva

A Service IP-t ne feledjük az `ifconfig -a` kimenetben is nyakon csípni: 

```ksh
# ifconfig -a
en10: flags=1e084863,480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet 10.161.138.211 netmask 0xffffffc0 broadcast 10.161.138.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
en11: flags=1e084863,480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet 10.161.138.213 netmask 0xffffffc0 broadcast 10.161.138.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
en20: flags=1e084863,480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet 192.168.10.10 netmask 0xfffffe00 broadcast 192.168.11.255
        inet 10.163.110.34 netmask 0xfffffe00 broadcast 10.163.111.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
en21: flags=1e084863,10480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet <b><r>10.163.110.36</r></b> netmask 0xfffffe00 broadcast 10.163.111.255
        inet 192.168.20.10 netmask 0xfffffe00 broadcast 192.168.21.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
```

### 3.5 Cluster service-ek és egy újabb összefoglaló

Az újabb, 7.1 utáni verziókon már további eszközök is rendelkezésünkre állnak, amilyen pl. a `clshowsrv`, annak is a verbose módja: 

```ksh
# clshowsrv -v
Local node: "tom" ("tom", "tom")
         Cluster services status:   "NORMAL" ("ST_STABLE")
         Remote communications:     "UP"
         Cluster-Aware AIX status:  "UP"

Remote node: "jerry" ("jerry", "jerry")
         Cluster services status:   "NORMAL" ("ST_STABLE")
         Remote communications:     "UP"
         Cluster-Aware AIX status:  "UP"

Status of the RSCT subsystems used by PowerHA SystemMirror:
Subsystem         Group            PID          Status
 cthags           cthags           12320946     active
 ctrmc            rsct             8650762      active

Status of the PowerHA SystemMirror subsystems:
Subsystem         Group            PID          Status
 clstrmgrES       cluster          15466736     active
 clevmgrdES       cluster          13434894     active

Status of the CAA subsystems:
Subsystem         Group            PID          Status
 clcomd           caa              6029506      active
 clconfd          caa              13369524     active
```

### 3.6 A cluster által valamiképpen kezelt fájlrendszerek

A clusteres `cllsfs` parancs után, az abból következő hagymányos `df` paranccsal is érdemes ezt ellenőrizni: 

```ksh
# cllsfs
cheese_RG       /etc/nginx

# df -m /etc/nginx
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/nginxlv    2560.00   2249.18   13%       26     1% /etc/nginx
```

### 3.7 Applikációs scriptek

Az applikációs start/stop script elkészítése általában egy közös feladat, de az alábbi példában nagyon egyszerű a helyzet, ugyanis a scriptek semmi mást nem csinálnak, csak elindítják vagy éppen leállítják az NGINX service-t: 

```ksh
# cllsserv
cheese_app  /etc/hacmp/start_cheese  /etc/hacmp/stop_cheese   background

# ps -ef | grep nginx
   nginx 13303914 15007910   0 11:32:14      -  0:00 /opt/freeware/sbin/nginx
   nginx 14680126 15007910   0 11:32:14      -  0:00 /opt/freeware/sbin/nginx
   nginx 14876780 15007910   0 11:32:14      -  0:00 /opt/freeware/sbin/nginx
    root 15007910        1   0 11:32:14      -  0:00 /opt/freeware/sbin/nginx
   nginx 15270068 15007910   0 11:32:14      -  0:00 /opt/freeware/sbin/nginx
```

### 3.8 A cluster logjai

A hagyományos `hacmp.out` fájl a leges legfontosabb log, amit egy cluster esetében figyelhetünk, kutathatunk benne. Rendkívül részletes, így néha (sokszor) nehéz megtalálni benne a lényeget, de legalább ugyanennyire hasznos is: 

```ksh
# cllistlogs
/var/hacmp/log/emuhacmp.out
/var/hacmp/log/hacmp.out
/var/hacmp/log/hacmp.out.1
/var/hacmp/log/hacmp.out.2
/var/hacmp/log/hacmp.out.3
/var/hacmp/log/hacmp.out.4
/var/hacmp/log/hacmp.out.5
/var/hacmp/log/hacmp.out.6
/var/hacmp/log/hacmp.out.7

# tail -f /var/hacmp/log/hacmp.out
lpm_in_progress_file=
:node_up_complete[+306] ls /var/hacmp/.lpm_in_progress/lpm_*
:node_up_complete[+306] 2> /dev/null
:node_up_complete[+306] wc -l
:node_up_complete[+307] ((         0  ))
:node_up_complete[+318] exit 0
Dec  4 2018 16:14:40 EVENT COMPLETED: node_up_complete jerry 0

&lt;LAT&gt;|2018-12-04T16:14:40|EVENT COMPLETED: node_up_complete jerry 0|&lt;/LAT&gt;
```

### 3.9 Minimális teszt a `clcmd` paranccsal

Ez is az újabb verziókban került bevezetésre, de nagyon hasznos lehet. Az alábbi példában egyetlen képernyőn láthatjuk, hogy a két node-on az applikációs scriptek elvileg meg kell, hogy egyezzenek egymással: 

```ksh
# clrsh jerry hostname
jerry

# clcmd ls -la /etc/hacmp

-------------------------------
NODE tom
-------------------------------
total 40
drwxr-xr-x    2 root     system          256 Dec 15 2016  .
drwxr-xr-x   43 root     system        12288 Dec 05 10:11 ..
-rwxr-xr-x    1 root     system           29 Dec 15 2016  start_cheese
-rwxr-xr-x    1 root     system           28 Dec 15 2016  stop_cheese

-------------------------------
NODE jerry
-------------------------------
total 40
drwxr-xr-x    2 root     system          256 Dec 15 2016  .
drwxr-xr-x   43 root     system        12288 Dec 05 10:09 ..
-rwxr-xr-x    1 root     system           29 Dec 15 2016  start_cheese
-rwxr-xr-x    1 root     system           28 Dec 15 2016  stop_cheese
```

### 3.10 Verziók

Újabb verziókon, tehát 7.1 fölött, az `oslevel`-hez hasonló `halevel` parancs is használható: 

```ksh
# oslevel -s
7100-05-03-1846

# halevel -s
7.2.2 SP1
```

A szintén újnak számító `clmgr` paranccsal a verzión túl még az **edition**-t is kinyerhetjük: 

```ksh
# clmgr -v query node | grep -E "^NAME=|VERSION=|EDITION="
NAME="jerry"
VERSION="7.2.2.1"
EDITION="STANDARD"
NAME="tom"
VERSION="7.2.2.1"
EDITION="STANDARD"
```

A hagyományos megoldás, ami minden HACMP verzión már nagyon régóta működik, az a `clstrmgrES` service részleteinek `lssrc`-vel való lekérdezése. 

Újabb verziókon: 

```ksh
# lssrc -ls clstrmgrES
Current state: ST_STABLE
sccsid = "@(#)  22e8ab4 43haes/usr/sbin/cluster/hacmprd/main.C, 722, 1746C_aha722, Oct 24 2017 05:00 PM"
build = "Jun 22 2018 06:22:48 1807z_aha722"
...
```

Régebbin (jelen esetben egy 6.1-esen): 

```ksh
# lssrc -ls clstrmgrES
Current state: ST_STABLE
sccsid = "@(#)36    1.135.1.97 src/43haes/usr/sbin/cluster/hacmprd/main.C, hacmp.pe, 53haes_r610, 0933A_hacmp610 8/8/09 14:44:29"
i_local_nodeid 0, i_local_siteid -1, my_handle 3
ml_idx[3]=0     ml_idx[4]=1
There are 0 events on the Ibcast queue
There are 0 events on the RM Ibcast queue
CLversion: 11
local node vrmf is 6102
cluster fix level is "2"
...
```

Természetesen, mivel szabványos csomagokból települ a PowerHA és annak minden komponense, így az `lslpp`-vel is nagyjából képbe kerülhetünk, hogy milyen verzió lehet telepítve: 

```ksh
# lslpp -L | awk '/ cluster./'
  cluster.adt.es.client.include
  cluster.adt.es.client.samples.clinfo
  cluster.adt.es.client.samples.clstat
  cluster.adt.es.client.samples.libcl
  cluster.doc.en_US.assist.smartassists.pdf
  cluster.doc.en_US.es.pdf   7.2.2.1    A     F    PowerHA SystemMirror PDF
  cluster.es.assist.common   7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.db2      7.2.2.1    A     F    PowerHA SystemMirror Smart
  cluster.es.assist.dhcp     7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.dns      7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.domino   7.2.2.0    C     F    PowerHA SystemMirror
  cluster.es.assist.filenet  7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.ihs      7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.maxdb    7.2.2.1    A     F    PowerHA SystemMirror Smart
  cluster.es.assist.oraappsrv
  cluster.es.assist.oracle   7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.printServer
  cluster.es.assist.sap      7.2.2.1    A     F    PowerHA SystemMirror Smart
  cluster.es.assist.tds      7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.es.assist.tsmadmin
  cluster.es.assist.tsmclient
  cluster.es.assist.tsmserver
  cluster.es.assist.websphere
  cluster.es.assist.wmq      7.2.2.1    A     F    PowerHA SystemMirror Smart
  cluster.es.client.clcomd   7.2.2.1    A     F    Cluster Communication
  cluster.es.client.lib      7.2.2.1    A     F    PowerHA SystemMirror Client
  cluster.es.client.rte      7.2.2.1    A     F    PowerHA SystemMirror Client
  cluster.es.client.utils    7.2.2.1    A     F    PowerHA SystemMirror Client
  cluster.es.cspoc.cmds      7.2.2.1    A     F    CSPOC Commands
  cluster.es.cspoc.rte       7.2.2.1    A     F    CSPOC Runtime Commands
  cluster.es.migcheck        7.2.2.0    C     F    PowerHA SystemMirror Migration
  cluster.es.nfs.rte         7.2.2.0    C     F    NFS Support
  cluster.es.server.diag     7.2.2.1    A     F    Server Diags
  cluster.es.server.events   7.2.2.1    A     F    Server Events
  cluster.es.server.rte      7.2.2.1    A     F    Base Server Runtime
  cluster.es.server.testtool
  cluster.es.server.utils    7.2.2.1    A     F    Server Utilities
  cluster.license            7.2.2.0    C     F    PowerHA SystemMirror
  cluster.man.en_US.es.data  7.2.2.0    C     F    SystemMirror manual commands -
  cluster.msg.en_US.assist   7.2.2.0    C     F    PowerHA SystemMirror Smart
  cluster.msg.en_US.es.client
  cluster.msg.en_US.es.server
```

### 3.11 A clstat "megjavítása"

**1.)** Készíts egy backupot a `/etc/snmpdv3.conf`-ról:

```ksh
# cp /etc/snmpdv3.conf /etc/snmpdv3.conf.`date +%Y%m%d`
```

**2.)** Másold át az alábbi tartalmat a `/etc/snmpdv3.conf` fájlba egyszerűen beleirányítva az alábbi kódblokkot a `cat > /etc/snmpdv3.conf` paranccsal, majd `Ctrl+D`-t és `Enter`-t nyomva:

```
VACM_GROUP group1 SNMPv1  public  -
 
VACM_VIEW defaultView        internet                   - included -
 
# exclude snmpv3 related MIBs from the default view
VACM_VIEW defaultView        snmpModules                - excluded -
VACM_VIEW defaultView        1.3.6.1.6.3.1.1.4          - included -
VACM_VIEW defaultView        1.3.6.1.6.3.1.1.5          - included -
VACM_VIEW defaultView        1.3.6.1.4.1.2.3.1.2.1.5    - included -
 
# exclude aixmibd managed MIBs from the default view
VACM_VIEW defaultView        1.3.6.1.4.1.2.6.191        - excluded -
 
VACM_ACCESS  group1 - - noAuthNoPriv SNMPv1  defaultView - defaultView -
 
NOTIFY notify1 traptag trap -
 
TARGET_ADDRESS Target1 UDP 127.0.0.1       traptag trapparms1 - - -
 
TARGET_PARAMETERS trapparms1 SNMPv1  SNMPv1  public  noAuthNoPriv -
 
COMMUNITY   public     public     noAuthNoPriv     0.0.0.0     0.0.0.0     -
 
DEFAULT_SECURITY no-access - -
 
logging     file=/usr/tmp/snmpdv3.log     enabled
logging     size=100000                   level=0
 
smux        1.3.6.1.4.1.2.3.1.2.1.2       gated_password
smux        1.3.6.1.4.1.2.3.1.2.3.1.1     muxatmd_password
smux        1.3.6.1.4.1.2.3.1.2.1.5       clsmuxpd_password
```

**3.)** Add ki az alábbi parancsokat ebben a sorrendben:
```ksh
# stopsrc -s snmpd ; lssrc -s snmpd ; stopsrc -s clinfoES ; lssrc -s clinfoES ; sleep 10 ; startsrc -s snmpd ; lssrc -s snmpd ; startsrc -s clinfoES ; lssrc -s clinfoES ; sleep 20
```

**4.)** Próbálkozz újra a `clstat`-al. Ezt mindkét node-on végigcsinálhatod. 

<a name="4"></a>
## 4. Rendszeres karbantartások

### 4.1 Karbantartási feladatok összegzése

Az alábbi feladatok hajthatók végre **kiesés nélkül**: 

- User creation / modification / deletion
- Add / change cluster-aware FS
- Cluster-aware disk replacement
- Repo. disk replacement
- and so on...

Feladatok egy **rövid kieséssel**: 

- Cluster RG move:
    - for AIX update
    - for PowerHA update
    - for any other minor-to-medium maintenances that requires the active node offline
- Swap service IP

### 4.2 A `smitty hacmp`

A tool fő képernyője: 

```ksh
# smitty hacmp
                                   PowerHA SystemMirror

Move cursor to desired item and press Enter.

  Cluster Nodes and Networks
  Cluster Applications and Resources

  System Management (C-SPOC)
  Problem Determination Tools
  Custom Cluster Configuration

  Can't find what you are looking for ?
  Not sure where to start ?
```

### 4.3 A cluster elindítása, leállítása

Az **elindítás** történhet a hagyományos módon: 

```ksh
# smitty clstart
...
```

...vagy az új `clmgr` paranccsal: 

```ksh
# clmgr online node node1,node2 MANAGE=auto FIX=yes 
...
```

A **leállítás** – hasonlóképpen – történhet a hagymányos módon: 

```ksh
# smitty clstop
...
```

...vagy az új `clmgr` paranccsal: 

```ksh
# clmgr offline node node2 MANAGE=offline 
...
```

### 4.4 A Resource Group átmozgatása a másik node-ra

Hagyományosan: 

```ksh
# smitty hacmp -> 
  System Management (C-SPOC)
  Resource Group and Applications
  Move Resource Groups to Another Node ->
    Select a Resource Group ->
      Select a Destination Node
```

...vagy: 

```ksh
# clmgr move resource_group cheese_RG NODE=jerry STATE=online
...
```

Átmozgatáskor egy másik session-ben bármelyik node-ról folyamatosan figyeljük a `clstat`-ot, használjuk a `qha`-t vagy akár a `clRGinfo`-t hívogassuk meg néhányszor. 

A kiindulópont ebben a példában az alábbi: 

```ksh
# clRGinfo
----------------------------------------------------
Group Name                   State            Node
----------------------------------------------------
cheese_RG                    ONLINE           tom
                             OFFLINE          jerry
```

Indulhat is az átmozgatás az új `clmgr` paranccsal: 

```ksh
# clmgr move resource_group cheese_RG NODE=jerry STATE=online
Attempting to move resource group cheese_RG to node jerry.

Waiting for the cluster to process the resource group movement request....

Waiting for the cluster to stabilize.......

Resource group movement successful.
Resource group cheese_RG is online on node jerry.


Cluster Name: cheese

Resource Group Name: cheese_RG
Node                                 State
------------------------------------ ---------------
tom                                  OFFLINE
jerry                                ONLINE
```

### 4.5 Network interface-ek közötti váltás

Több interface esetében (és általában több interface-ünk van erre) arra is lehetőségünk van, hogy a service IP-t átmozgassuk egy másik interface-re, hogy ott legyen az egy IP alias. Ennek is van hagyományos és újhullámos megoldása. 

A hagyományos: 

```ksh
# smitty hacmp -> 
  System Management (C-SPOC)
  Communication Interfaces
  Swap IP Addresses between Communication Interfaces ->
    Available Service/Communication Interfaces ->
      tom-en21        cheese_net
```

Az új: 

```ksh
# clmgr move service_ip cheese-b INTERFACE=tom-en21
...
```

### 4.6 A cluster szinkronizálása

Erre akkor lehet szükség, ha pl. módosult a cluster konfigurációja az első node-on és azt a másik node-ra is át kívánjuk azonnal, akár online clusterrel szinkronizálni. Ez ellenőrzést is végez, ezért mindenképpen hasznos. 

A funkció a `smitty` menükön belül is több helyen felbukkan és mindegyik ugyanazt végzi: 

```ksh
# smitty hacmp -> 
  Cluster Nodes and Networks
  Verify and Synchronize Cluster Configuration
```

...vagy: 

```ksh
  Cluster Applications and Resources
  Verify and Synchronize Cluster Configuration
```

Erre is rendelkezésre áll `clmgr`-es megoldás: 

```ksh
# clmgr sync cluster 
...
```

Kiterjesztett szinkronizálás: 

```ksh
# smitty hacmp -> 
  Custom Cluster Configuration
  Verify and Synchronize Cluster Configuration (Advanced)
```

### 4.7 Felhasználók és csoportok karbantartása

Alapvető fontosságú, hogy a node-ok mindegyikén legalább az applikációt bármilyen szinten érintő felhasználók ugyanazzal a névvel, ID-val, csoporttagsággal, jogosultságokkal rendelkezzenek. Emiatt erre is vannak `smitty` menük vagy akár egyből futtatható parancsok is. Ezek már a régebbi verziókon is ugyanígy elérhetőek. 

A `smitty` menüin belül: 

```ksh
# smitty hacmp -> 
  System Management (C-SPOC)
  Security and Users
  Users / Groups / Passwords in an PowerHA SystemMirror cluster
```

Egy új, cluster-es user létrehozása (tehát ugyanazzal az ID-val stb-vel), pl.:

```ksh
# cl_mkuser pgrp=staff gecos="This is a test user, please ignore it" kiskutya
...
```

Ellenőrzése az előbbinek: 

```ksh
# cl_lsuser -fa pgrp groups gecos kiskutya
jerry: kiskutya:
jerry:  pgrp=staff
jerry:  groups=staff
jerry:  gecos=This is a test user, please ignore it
jerry:
tom: kiskutya:
tom:    pgrp=staff
tom:    groups=staff
tom:    gecos=This is a test user, please ignore it
tom:
```

User eltávolítása: 

```ksh
# cl_rmuser kiskutya
```

### 4.8 Fájlrendszer és storage management

Az erre való `smitty` menü: 

```ksh
# smitty hacmp -> 
  System Management (C-SPOC)
  Storage -> 
    Volume Groups
    Logical Volumes
    File Systems

    Physical Volumes
```

Egy shared VG-ben lévő fájlrendszer ellenőrzése, pl.:

```ksh
# cl_lsfs -q /etc/nginx
Node: Name            Nodename   Mount Pt VFS   Size    Options    Auto Accounting
tom: /dev/nginxlv    --         /etc/nginx             jfs2  5242880 rw         no   no
tom:   (lv size: 5242880, fs size: 5242880, block size: 4096, sparse files: yes, inline log: yes, inline log size: 10, EAformat: v1, Quota: no, DMAPI: no, VIX: yes, EFS: no, ISNAPSHOT: no, MAXEXT: 0, MountGuard: yes)
```

Egy shared VG-ben lévő fájlrendszer növelése 256 MB-al (természetesen alapos előzetes ellenőrzés után), pl.:

```ksh
# cl_chfs -a size=+256M /etc/nginx
```

<a name="5"></a>
## 5. Cluster tervezése

A legfőbb tudnivalók, mielőtt belevágnánk: 

- a node-oknak egyformáknak kell lenniük (ugyanaz az OS, ugyanazok a felhasználók/csoportok és ugyanazok a jelszavak)
- az interface-ek, VLAN-ok előre fel legyenek jegyezve, létezésük megértve, az `/etc/hosts` fájl megfelelően kitöltve
- általában a rootvg kell, hogy tartalmazza az applikációt vagy a DB binárisait, tehát helyileg
- ha csak lehet, shared VG-t használj az állandóan közös adatok tárolására, tehát ugyanazon diszk több géphez kell legyen allokálva
- legalább egy, de inkább 2 shared repository diszket kell használnod, aminek a minimális mérete 2 GB legyen, de ajánlott inkább 5 GB-ossal kezdeni
- a cluster fileset-jeinek mindkét node-on telepítve kell lenniük, a konfigurációjuk is meg kell egyezzen

<a name="6"></a>
## 6. Cluster fileset-ek, előfeltételek, telepítés, eltávolítás

### 6.1 Előfeltételek

Az alábbi 3 filesetnek telepítve kell lennie: 

- `bos.cluster.rte`
- `bos.ahafs`
- `bos.clvm.enh`

Ellenőrzésük egyetlen paranccsal: 

```ksh
# lslpp -L | grep -E "bos.cluster|bos.ahafs|bos.clvm"
...
```

Telepítésük (preview, majd install), ha szükséges: 

```ksh
# ls -l
...
-rw-r--r--    1 root     system       303104 Jul 12 14:09 bos.ahafs
-rw-r--r--    1 root     system     11597824 Jul 12 14:09 bos.cluster
-rw-r--r--    1 root     system       179200 Jul 12 14:09 bos.clvm
...

# installp -a -XYpd . bos.cluster.rte bos.ahafs bos.clvm.enh
...

# installp -a -XYd . bos.cluster.rte bos.ahafs bos.clvm.enh
...
```

### 6.2 Korábbi, esetleges, régi cluster verzió teljes eltávolítása

Mindenek előtt, amennyiben ez még lehetséges, dobd el a korábbi cluster definíciót a `smitty hacmp` menüjeivel. Ezután először győződj meg róla, hogy az alábbi `lslpp` parancs tényleg csak azt mutatja, amit kell, majd végezd el a deinstallt: 

```ksh
# lslpp -L | awk '/ cluster./'
...

# installp -u $(lslpp -L | awk '/ cluster./{print $1}' | sort)
...
```

**Egy ilyen után mindenképpen kell reboot is!**

Amennyiben szükséges, az előzőleg még repo vagy heart beat diszknek használt diszkeket "ganézd ki" a lehető legalapabb szinten: 

```ksh
# chvg -a n caavg_private
...

# dd if=/dev/zero of=/dev/&lt;hdiskX&gt; bs=1M count=100
...

# lspv
...
```

### 6.3 A telepítés

Az új verzió fileset-jeit tartalmazó, alaposan ellenőrzött könyvtárba való váltás után ezt végezheted az alapos módon is:

```ksh
# smitty install_all
...
```

...vagy itt egy már előre felparaméterezett `installp` parancs (remélhetőleg ez még évek múlva is érvényes lesz): 

```ksh
# installp -a -XYd . cluster.adt.es cluster.doc.en_US.assist cluster.doc.en_US.es cluster.es.assist cluster.es.client cluster.es.cspoc cluster.es.migcheck cluster.es.nfs cluster.es.server cluster.license cluster.man.en_US.es.data cluster.msg.en_US.assist cluster.msg.en_US.es
...
```

További SP telepítésére, azaz frissítésre is szükség lehet. Ehhez bontsd ki a cuccot, majd az ismert AIX-os módon frissíts. 

Ezt is megteheted `smitty update_all` módszerrel, vagy egyetlen parancs kétszeri használatával (preview, majd update): 

```ksh
# install_all_updates -Ypd .
...

# install_all_updates -Yd .
...
```

**Az install és az update után egyaránt szükség van 1-1 reboot-ra!** Természetesen, mindkét node-on. 

<a name="7"></a>
## 7. Cluster konfigurálás az alapoktól

Alapos, parancsokra és `smitty` menük szintjére részletezett leírás helyett álljon itt az a 11 fő lépés, amivel kialakíthatsz egy tisztességes clustert. 

Ez esetben is angolul hagyom a szöveget, mivel az egyes `smitty` menük is többnyire pont így szólnak: 

1. Fill up `/etc/cluster/rhosts` file with all the non-service IPs, restart `clcomd` and test it
2. Set up the basic cluster definion
3. Set up the repository disk
4. Set up networks
5. Add application controller scripts
6. Add service IP
7. Add resource group
8. Put it all together, so add the application controller, the service IP resources and the VG to the resource group
9. Verify and Synchronize Cluster Configuration
10. Start the cluster
11. Check the service IP, the shared VGs/FSs, the processes that should be running by now

<a name="8"></a>
## 8. Clustertesztelés, dokumentálás

1. A cluster tesztelésének főbb lépései, amelyeket megegyezés szerint dokumentálnod is kell: 
     - egyszerű RG mozgatások oda-vissza
     - cluster node leállítása RG átmozgatással együtt
     - az aktív node HMC-n való azonnali kilövése (ez az igazi failover test)
     - a HMC CLI-ben a node-ra `vtmenu`-t nyitva a hálózati interface-ek ki/be kapcsolgatása és eredményének vizsgálata
2. Készíts cluster snapshotot!
3. Készíts HTML riportot a clusterről!

A HTML riporthoz egy lehetséges parancs: 

```ksh
# clmgr view report cluster TYPE=html FILE=/tmp/powerha.report
...
```
