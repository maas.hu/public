# ![alt text](/_ui/img/icons/powervm_m.svg) IBM PowerVM

<div style="text-align: right;"><h7>Posted: never</h7></div>

###### .

![alt_text](hmc-v9.2-censured-transformed.jpg)


>Under construction...


## Table of Contents

1. [Introduction](#1)
2. [General Tasks on HMC GUI](#2)
3. [General Tasks on HMC CLI](#3)
4. [Creating an LPAR](#4)
5. [Deleting an LPAR](#5)
6. [HMC Update](#6)
7. [VIOS Update](#7)
8. [Setting up VIOS](#8)


<a name="1"></a>
## 1.Introduction

...


<a name="2"></a>
## 2. General Tasks on HMC GUI

...


<a name="3"></a>
## 3. General Tasks on HMC CLI

...


<a name="4"></a>
## 4. Creating an LPAR

...


<a name="5"></a>
## 5. Deleting an LPAR

...


<a name="6"></a>
## 6. HMC Update

...


<a name="7"></a>
## 7. VIOS Update

...


<a name="8"></a>
## 8. Setting up VIOS

...


