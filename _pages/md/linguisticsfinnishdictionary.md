# ![alt text](/_ui/img/flags/fin-hun_m.svg) Dictionary of Finnish-Hungarian Common Origin Words

<div style="text-align: right;"><h7>Posted: 2023-08-31 20:48</h7></div>

###### .

<div class="info">
<p>The below list is my own collection of words and <b>still under development/collection</b>.<br>
While learning Finnish, several of the words were too obviously commonly-rooted, while some others were just very suspicious, and required a short checkout on the <a href="https://www.arcanum.com/hu/online-kiadvanyok/Lexikonok-magyar-etimologiai-szotar-F14D3/f-F2003/feszek-F20DF/" target="_blank">Etymological Dictionary of Hungarian</a>.</p>
</div>

**alla** - alatt, lent  <img src="/_ui/img/imgfx/animals.png" title="" align="right" class="right"> - *under*  
**antaa** - adni - *to give*  
**auto** - autó - *car*  
**elävä** - eleven, élő - *alive*  
**elää** - élni - *to live*  
**en** - én nem - *I'm not*  
**harava** - gereblye - *rake*  
**hau hau** - vau vau - *woof woof*  
**hiu** - haj - *hair*  
**ien** - íny, fogíny - *gum*  
**joki** - folyó (maradvány pl. Sajó) - *river*  
**joo** - ja', igen, rendben - *yeah*  
**jousi** - íj - *bow*  
**jää** - jég (**jääkiekko** - jégkorong; **jääkylmä** - jéghideg) - *ice*  
**kala** - hal - *fish*  
**karsina** - karám - *stall*  
**kivi** - kő - *stone*  
**kori** - kosár - *basket*  
**kot kot** - kot-kot - *cluck cluck*  
**kutoa** - kötni - *to weave*  
**kysyä** - kérdezni - *to ask*  
**kytkeä** - megkötni, kapcsolódni - *to connect*  
**kyynärpää** - könyök - *elbow*  
**käki** - kakukk - *cuckoo*  
**käsi** - kéz - *hand*  
**laatikko** - doboz, ládikó - *box*  
**lakata** - véget érni, abbamaradni ("lankadni") - *to stop, to quit*  
**lapio** - lapát - *shovel*  
**lätäkkö** - pocsolya ("latyak") - *puddle*
**me** - mi - *we*  
**mehiláinen** - méh - *bee*  
**mehiläinen** - méh - *bee*
**menee hyvin** - jól megy (pl. jók az eladási mutatók) - *going well*  
**mennä** - menni - *to go*  
**millainen** - milyen, miféle - *what kind of*  
**missä** - hol (possible answer: "messze") - *where*  
**mitä** - mit - *what*  
**morsian** - menyasszony - *bride*  
**myrkyllinen** - mérgező - *poisonous*  
**neljä** - négy - *four*  
**nuoli** - nyíl - *arrow*  
**oksentaa** - okádni, hányni - *to vomit*  
**pehmeä** - puha - *soft*  
**pesä** - fészek - *nest*  
**puhveli** - bölény - *buffalo*  
**runko** - fatörzs (~rönk) - *trunk*  
**siisti** - tiszta - *clean*  
**silmä** - szem - *eye*  
**solisti** - szólista - *soloist*  
**surullinen** - szomorú - *sad*  
**suu** - száj - *mouth*  
**suutari** - suszter, cipész - *shoemaker*  
**sydän** - szív - *heart*  
**syntymä** - születés - *birth*  
**syö** - eszik (**syödä** - enni) - *eats*  
**säkki** - zsák - *bag*  
**sämpylä** - zsemle - *bun*  
**sääkki** - zsák - *bag*  
**tarina** - történet - *story*  
**tasan** - pontosan (**tasan kuusi** - pontosan hat (óra)) - *exactly (time)*  
**te** - ti - *you all*  
**tehdä** - tenni, csinálni - *to do, to make*   
**tiili** - tégla - *brick*  
**torni** - torony - *tower*  
**täydellinen** - tökéletes - *perfect*  
**uida** - úszni - *to swim*  
**unelma** - álom - *dream*  
**valitse** - választani - *to choose*  
**valjeta** - (világossá) válik - *to be clarified*  
**vanha** - öreg, idős ("vén") - *old*  
**varis** - varjú - *crow*  
**varis** - varjú - *crow*  
**varsi** ~ szár - *arm*, *stem*  
**vastata** - válaszolni - *to answer*  
**veri** - vér - *blood*  
**vesi** - víz (**vesimylly** - vízimalom; **vesipullo** - vizespalack) - *mall*  
**viiriäinen** - fürj - *quail*  
**virkailija** - hivatalnok ("firkáló") - *office worker*  
**voi** - vaj - *butter*  
**vyö** - öv - *belt*  
**yhteensä** - összesen - *total*  
**yksinkertainen** - egyszerű ("egy-szerű") - *simple*  


