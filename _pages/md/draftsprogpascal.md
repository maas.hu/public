# ![alt text](/_ui/img/icons/fpc_m.svg) Pascal

<div style="text-align: right;"><h7>Posted: 2018-05-19 12:16</h7></div>

###### .

![alt_text](pascal_-_bg.jpg)

>Az alábbiakban egy még 2005-ben készült jegyzetemet olvashatod (itt-ott kiegészítve, hogy a mai állapotokat jobban tükrözze), amely nagyjából végigvezet a Pascal alapjain a Free Pascal felhasználásával.

## Tartalomjegyzék

1. [Bevezetés](#1)
2. [A fejlesztési környezet kialakítása](#2)
3. [A Pascal programozás alapjai](#3)
4. [Konstansok és változók](#4)
5. [Alapvető vezérlési szerkezetek](#5)
6. [Kifejezések](#6)
7. [Alprogramok és unit-ok](#7)
8. [Strukturált típusok](#8)

<a name="1"></a>
## 1. Bevezetés

<a href="niklaus_wirth.jpg" target="_blank" title="">
  <img src="niklaus_wirth.gif" class="right"></a>
A Pascal programozási nyelvet és az első Pascal nyelvű fordítóprogramot a svájci ***Niklaus Wirth*** (a képen) alkotta meg 1970-ben. Wirth a nyelvet *Blaise Pascalról*, a XII. század neves francia matematikus-filozófus tudósáról nevezte el. A Pascal nyelvet 1973-ban szabványosították, amit 1982-ben átdolgoztak a ma is érvényben lévő Pascal szabvánnyá (ISO 7185). Az ezt a nyelvet megvalósító legnépszerűbb fejlesztői környezet a Borland cég DOS alatt használható **Turbo Pascal**ja volt. Nagy fejlődésen ment át és egészen a 7.0-s verzióig jutott el (1983-1991). A Turbo Pascal termékvonalának megszűnésével jelent meg a **Borland Pascal** 1992-ben, mely még néhány újítást tartalmazott. Már a Turbo Pascal 5.5-ös verziójában megjelent az objektumok használatának lehetősége, mely alapjául szolgált az **Object Pascal** nyelv megjelenésének, amit azóta már önálló programozási nyelvként tartanak számon. A Windows széleskörű elterjedésével a Borland Pascalt is fokozatosan leváltotta az 1995-ben megjelenő **Delphi**, mely egy Object Pascal nyelvre épülő, Windows alatt futó vizuális fejlesztőeszköz.

A **Free Pascal** kidolgozását a német *Florian Klämpfl* kezdte meg, hogy egy olyan Pascal fordítót készítsen, ami működőképes Linux operációs rendszer alatt is, teljesen 32 bites kódot generálva (azóta – természetesen – már 64 bites verzió is létezik). A projekt az FPK (*Free Pascal Kompiler*) nevet kapta. A fordító 1996-ban vált az internetes közösség számára is elérhetővé és hamarosan nagy népszerűségre tett szert. További programozók csatlakoztak a projekthez és az új cél már egy ingyenes és platformfüggetlen Pascal fordító megalkotása lett, ami szinte teljesen kompatibilis a Turbo/Borland Pascallal, illetve a Delphivel. Ma már többféle processzorarchitektúrára (x86, x86_64, PowerPC stb.) és több operációs rendszerre (Linux, AIX, FreeBSD, Mac OS, DOS, Windows stb.) egyaránt elérhető. Forráskódja is bárki által hozzáférhető, terjeszthető, a GNU licence (*GNU General Public Licence – GNU GPL*) alá tartozik. A nyílt forráskód egyik óriási előnye, hogy szabad betekintést kapunk a program bármely részébe, ezáltal megtudhatjuk, hogyan készítették el azt a fejlesztők. Ez a Free Pascal esetében elsősorban a modulok (vagy unitok) forráskódjának böngészésekor van nagy hasznunkra, hiszen bármiféle dokumentáció igénybevétele nélkül is megismerhetjük az implementált eljárásokat, függvényeket és adattípusokat.

<a name="2"></a>
## 2. A fejlesztési környezet kialakítása

<a href="helloworld.png" target="_blank" title="">
  <img src="helloworld.gif" class="right"></a>
A programot letölteni innen tudod: [http://freepascal.org/download.var](http://freepascal.org/download.var). A Free Pascal korábbi (1.0.xx-­es) verziójához még nem tartozott a Turbo Pascal­éhoz hasonló karakteres felületű IDE (Integrált Fejlesztői Környezet), az újhoz (jelenleg 3.0.0-­ás) azonban már igen. Létezik egy **Lazarus** nevű grafikus alkalmazás is, ami a Borland Delphi­hez szokott programozók életét könnyíti meg. A Lazarus a Delphi­hez hasonlóan projektekkel dolgozik, az objektumok tulajdonságait „Object Inspector”-­ban mutatja, miközben a fordításhoz mindvégig a Free Pascal­t használja. Vannak azonban sokkal „kódközelibb” megoldások is: az IDE használata (lásd a jobb oldali képet), vagy ha keresünk egy jó – Pascal­os szintaxiskiemeléssel is ellátott – kódszerkesztő alkalmazást.
Ilyen a grafikus felületen működő Bluefish is. Persze van még rengeteg, a Pascal kód megjelenítésére alkalmas program, melyek között tetszés szerint válogathatunk (pl. nedit, mcedit stb.). A továbbiakban én a Bluefish ­használatát veszem végleges döntésnek, a fordítást pedig konzolos (grafikus terminál, pl. gnome­terminal) felületről végezzük.

### 2.1 A Bluefish­ről néhány szóban

<img src="bluefish.png" class="right">
A program elsősorban HTML/CSS fejlesztésére van kitalálva (erőssége főleg abban rejlik), de megbirkózik más típusú kódok szintaxiskiemelésével is. A megnyitott fájl kiterjesztése alapján határozza meg az adott típusú kódnak megfelelő kiemelést. A Pascal kódot alapbeállításban csak a `.pas` kiterjesztésű fájlokhoz jeleníti meg kiemeléssel, de mi ragaszkodjunk a Free Pascal hagyományaihoz és használjuk a `.pp` végződést.

Ehhez az alábbi beállításra van szükség: Menjünk be az Edit menü → Preferences menüpontjába és a Filetypes fülre kattintva, a megjelenő listát gördítsük a „pascal”­-hoz, és az Extensions oszloppal egyvonalban, dupla kattintást követően egészítsük ki a felsorolást `.pas:.pp`-­re. Innentől kezdve a `.pp`­-t is szépen megjeleníti.

Egy másik beállítani való az lenne, hogy állítsuk be az alapértelmezett karakterkódolást. E sorok írásakor én Fedora Core 3 Linuxot használok, abban a terminál karakterkódolása alapbeállításban UTF­8, így ahhoz, hogy az apró, terminálon futó programokban jók legyenek az ékezetek, a forráskódot is ezzel a kódolással kell elkészíteni. Ezért aztán át szoktam állítani (szintén a Preferences­-en) a Files fülben a Default character set­et UTF­8-­ra.

A többi beállítás már ízlés dolga, nagyjából ennyi lenne a Bluefish beállítása.

### 2.2 A fordítási művelet

Maga a Free Pascal könyvtárszerkezete kissé összetett, így aztán elég hosszú parancssort igényelne egy egyszerű programocska újra­újrafordítása. Kicsit megsportosíthatjuk a könyvtárszerkezetet is, ha a unit­okat tartalmazó könyvtárakat a program főkönyvtárához közelebb hozzuk, például, hogy így nézzen ki:

```
../fpc­2.0.0/bin   ← itt vannak a binárisok (ppc386 is)
/doc              ← dokumentációk, segédletek
/lib              ← a unit­ok (/lib/units könyvtár)
/out              ← a sikeres fordítás eredményei
/src              ← itt történik a kódírás/fordítás
```

Persze mindez, hogy így néz ki a szerkezet, csak az én agyam szüleménye, nem kötelező ezt másnak is betartani. A fenti könyvtárszerkezetre alapozva így néz ki egy `src` könyvtárban található proba.pp nevű Free Pascal forrásállomány binárissá fordításának utasítása:

```
../bin/fpc ­Fu../lib/units/rtl proba.pp
```

Nincs ember, aki ezt állandóan begépelné. A ­`Fu...` bejegyzés a forráskódban megadott unit(ok) helyét adja meg a fordítónak. Ha az rtl (*Run­Time Library*, ezt minden esetben el kell tudnia érnie a fordítónak, itt vannak a legáltalánosabb unit­-ok) könyvtár unit­jain kívül másokat is használunk, azt ugyanúgy hosszadalmasan be kellene pötyögni. Többek között emiatt kell készíteni egy `fpc.cfg` nevű konfigurációs fájlt az `fpc` binárist tartalmazó könyvtárban (`../bin`). Ennek az `fpc.cfg` fájlnak a tartalmára egy egyszerű példa a következőkben látható:

```
# Compiling parameters for fpc
-l                      # Free Pascal logó kiírása fordításkor
-viwn                   # fordítási információk kijelzése
-Fu../lib/units/rtl     # elérési utak néhány unit­csomaghoz
-Fu../lib/units/cdrom
-Fu../lib/units/gtk
-Fu../lib/units/gtk2
-Fu../lib/units/opengl
-Fu../lib/units/unzip
-FE../out               # a lefordult bináris könyvtára az ../out
```

Végezetül még készíthetünk egy rövid scriptet az `src` könyvtárban, amelyet én itt a `run`­ névvel láttam el. Tartalma:

```
# Script for compiling and running

cd ../bin               # belép a bin­-be
./fpc ../src/proba.pp   # elindítja az fpc­-t, a forrás nevét is megadja
echo                    # kihagy egy sort... Csak úgy :)

if [ -f ../out/proba ]; then   # sikeres fordítás esetében valószínűleg
../out/proba                   # készült egy proba nevű bináris; ha igen, 
fi                             # akkor elindítja azt
```

Ezek után mindössze ki kell adni az `src` könyvtárban a `./run` parancsot, és az ott található `proba.pp` lefordul és elindul (feltéve, hogy a kód jó). Természetesen, ha nem proba.pp­nek nevezzük a forrásállományt (és ez általában így is van rendjén), át kell írni az idevonatkozó bejegyzéseket.

A későbbiek során foglalkozunk még egy másik fajta fordítást segítő eszköz, a make program használatával is. Ennek lényege, hogy a forrásállományokat tartalmazó könyvtáron belüli – általunk szerkeszthető – `Makefile` nevű állomány tartalma alapján egy egyszerű make utasítás kiadásával indíthatunk el akár egészen összetett fordítási folyamatokat.

<a name="3"></a>
## 3. A Pascal programozás alapjai

### 3.1 A program felépítése

Hogyan is épül fel egy Pascal program? Alapvetően 3 fő részre oszthatjuk. Az egyes részekben található bejegyzésekből bizonyos dolgok mindenkor kötelezően megadandóak, másokat csak különlegesebb esetekben kell használni, míg akadnak olyanok is, melyek megadása nem kötelező, de a szabványosság miatt ajánlott a feltüntetésük (ilyen pl. a program azonosító).

1. A program fejrésze

```
// globális fordítási direktívák (pl. {$I-} )

program azonosito;  // a program azonosítója
// ...amely modul (unit) esetében: unit azonosito;
```

2. Definíciós és deklarációs rész

```
uses modul1, modul2, ...;  // a program által használt unitok
type ...;   // típusok definíciója
const ...;  // konstansok (állandók) definíciója
var ...;    // változók deklarációja
procedure ...;  // metódusok definíciója, melyek eljárások és/vagy...
function ...;   // ...függvények fejrészei
```

3. Főprogram

```
begin  // egy utasítássor mindig ezzel kezdődik
  utasitas1;
  utasitas2;
  ...
end.  // a legvégén ezzel fejeződik be
```

Fontos tudnivaló, hogy a definíció a kódszegmens, a deklaráció pedig az adatszegmens meghatározására használt fogalom. (Ezeket gyakran összetévesztik egymással.)

### 3.2 Programírási szabályok

A programírás megkezdése előtt segítséget nyújthat, ha tervet készítünk. Ezt megtehetjük egyszerű, mondatszerű leírással (amiből már egyenesen következhet a Pascal­kód), vagy Jackson­terv, ismertebb nevén folyamatábra rajzolásával. Ez utóbbi a legalkalmasabb a program szerkezetének vizuális megértéséhez. Ilyen tervvel még találkozunk a későbbiekben. A programunkat célszerű úgy írni, hogy a későbbi fejlesztések során is könnyen átlátható legyen, akár más programozó számára is. Ehhez meg kell fogadnunk néhány hasznos tanácsot:

- bár a fordító nem tesz különbséget kis­ és nagybetűk között, néha használhatunk nagybetűket a jobb olvashatóság miatt
- az egybefüggő utasítássorokat (ciklusmagok, eljárás­ és függvénytörzsek) 2-­3 szóköznyi helykihagyással beljebb tagoljuk
- a program nagyobb kiterjedésű, különböző részeit foglaljuk önálló modulokba (unitokba).

Az utasítások végét (még a deklarációs és definíciós utasításoknál is) általában pontosvesszővel zárjuk le. A Pascal program mindig a begin utasítással kezdődik és az end. paranccsal fejeződik be. A ciklusmagok, eljárás­ és függvénytörzsek szintén begin­nel kezdődnek, de végződésük itt még csak az end;, hiszen a program még nem érhet véget.
A forráskódban elhelyezhetünk megjegyzéseket is, ezzel is továbbsegítve magunk illetve mások tájékozódását. A Free Pascal háromféle módot ajánl fel megjegyzés beszúrására, melyek mindegyikét teljesen figyelmen kívül hagyja a fordító, mintha ott sem lennének:

```
(* Ez egy régi stílusú megjegyzés *)
{ Ez egy Turbo Pascal szabványú megjegyzés }
// Ez egy Delphi-s megjegyzés; egy teljes sorra vonatkozik
```

### 3.3 A „Helló Világ!” program

Ezek után készítsük el az alábbi, rendkívül egyszerű programot. Írjuk a proba.pp állományba a következőket:

```
program hello;
begin
  writeln('Helló Világ!');
end.
```

Mentsük el, lépjünk át terminálra és ott a `./run` utasítással fordítsuk le és futtassuk. Nem fogunk túlzottan meglepődni: az elkészült program kiírja a terminálra (standard output) a „Helló Világ!” szöveget.

A program fő része a `writeln` eljárás, mely a neki megadott értéket írja ki a szabványos kimenetre, majd új sorra vált.
Az értékmegadási szintaktikát vizsgálva megállapíthatjuk, hogy szöveg típusú értékeket `''` jelek között adhatunk meg. Más a helyzet számérték, konstans vagy változónév megadásánál, ugyanis akkor el kell hagynunk a `''` jeleket. Ekkor így néz ki az utasítás:

```
writeln(5);
```

E két féle értékadási forma kombinálásakor, hogy a program a számot, konstanst vagy változónevet ne szövegként értelmezze, a vessző karaktert kell alkalmazni, pl. így:

```
writeln('Ekkora lábam van: ',42);
```

Itt már egy számértéket is láthatunk, mely a szövegen kívülre került. Jobban belegondolva, ennek ebben az esetben még nem sok értelme van, mivel a programunkban nem is tudnánk kihasználni a „42” számértékként való kezeléséből származó lehetőséget. Hogy ezt megvalósíthassuk, meg kell ismerkednünk a konstansokkal és a változókkal.

<a name="4"></a>
## 4. Konstansok és változók

### 4.1 Konstansok

Nevükből is kikövetkeztethető: a konstansok értéke a program futása során nem változhat. Definiálása a const utasítás után történik konstansnév = tartalom; szintaktikával. Két ugyanolyan nevű konstanst vagy változót nem adhatunk meg, hiszen akkor nem tudnánk a megfelelőre hivatkozni. Miután definiáltunk egy konstanst, a fordító a továbbiakban ezzel a konstansnévvel találkozva már a fordítás alatt annak értékét írja a helyére.
Íme egy példa konstansok használatára:

```
const
  x = 'Településünk lakóinak száma:';
  y = 2542;

begin
  writeln(x);
  writeln(y);
end.
```

### 4.2 Változók

Ahhoz, hogy egy programban változót használjunk, még használata előtt deklarálni kell. Először meg kell adnunk annak típusát és csak azután adhatunk értéket neki. Értéket egy változónak a `:=` jel segítségével adhatunk. Értékként megadhatunk ténylegesen értéket (pl. `x:=5;` ), változót (pl. `x:=y;` ) vagy akár kifejezést is (pl. `x:=(y+1)*z-3;` ).
A változó bármikor új értéket kaphat, de a típusa mindig marad az eredeti.
Vizsgáljuk meg az alábbi példaprogramot:

```
var
  a, b, c : integer;

begin
  a:=3;
  b:=4;
  c:=a+b;
  writeln(c);
end.
```

Több egyforma típusú változót (jelen esetben csupa integer típust) egy vesszőkkel elválasztott listába is foglalhatunk. Az elkészült program mindössze ennyit kell, hogy kiírjon a képernyőre: „7”.

Az előző programban sem éltünk igazán a változók adta lehetőségekkel, hiszen a program külső felhasználója nem képes beavatkozni azok tartalmába; az „a” változó fixen 3, a „b” pedig 4 értéket kap. A felhasználó általi értékadást a következő módon képzelhetjük el:

```
var
  a, b, c : integer;

begin
  write('Kérem a-t: '); readln(a);
  write('Kérem b-t: '); readln(b);
  c:=a+b;
  writeln('a és b összege: ',c);
end.
```

Ezzel máris interaktívvá tettük előző programunkat. Két új eljárással találkoztunk:

- a `write` a `writeln`-­tól annyiban különbözik, hogy nem hoz létre új sort
- a `readln` az enter leütése előtt begépelt adatot adja meg értékként az adott változónak, majd új sorba viszi a kurzort. Amennyiben a kódban nincs megadva hozzá paraméter (`readln;`), egyszerűen egy enter leütésére vár.

Az elkészült programmal játszadozva megfigyelhetjük, hogy hosszabb szám, tizedes tört, ill. bármiféle, nem egész számként értelmezett, esetleg szöveg megadása után `Runtime error 106` hibaüzenettel megszakad a program futása. Ez az integer típusú változó korlátai miatt van, mely csak meghatározott hosszúságú pozitív és negatív egész számok tárolására alkalmas.

Természetesen nemcsak az integer áll rendelkezésünkre, így lehetőségünk van az adott feladathoz legjobban alkalmazható típus kiválasztására.

### 4.3 Elemi adattípusok

A Free Pascal minden alaptípust tartalmaz, amelyeket a hagyományos Turbo Pascal is tartalmazott, kiegészítve ezeket néhány Delphi­ből átvett, extra típussal. Ezek felsorolását láthatjuk a következőkben.

#### 4.3.1 Előre definiált egésztípusok

```
Típus 	Tartomány 	Méret bájtban
Byte 	0 .. 255 	1
Shortint 	-­128 .. 127 	1
Smallint 	-­32768 .. 32767 	2
Word 	0 .. 65535 	2
Integer 	Smallint, Longint vagy Int64 	2, 4 vagy 8
Cardinal 	Word, Longword vagy Qword 	2, 4 vagy 8
Longint 	­-2147483648 .. 2147483647 	4
Longword 	0 .. 4294967295 	4
Int64 	-­9223372036854775808 .. 9223372036854775807 	8
Qword 	0 .. 18446744073709551615 	8
```

Az integer és a cardinal típusok tartománya, ezzel együtt mérete az adott processzor­architektúrától ill. a fordítási paraméterektől függ.

#### 4.3.2 Valós típusok

```
Típus 	Tartomány 	Fontos számjegyek 	Méret
Real 	processzorfüggő 	??? 	4 vagy 8
Single 	1.5E­45 .. 3.4E38 	7 - ­8 	4
Double 	5.0E­324 .. 1.7E308 	15 - ­16 	8
Extended 	1.9E­4951 .. 1.1E4932 	19­ - 20 	10
Comp 	­-2E64+1 .. 2E63­1 	19 - ­20 	8
```

A valós típusú értékek kiíratásakor az általunk is értelmezhető megjelenítéshez a tizedesjegyeket csökkenteni kell, amit a kiíratást végző eljárásban kell paraméterként megadnunk. Ezt láthatjuk a következő programrészletben. Figyeljük meg, hogyan határozunk meg egy real típusú változónak 2 tizedesjegyet:

```
var
  atlag : real;
...
  writeln('Átlag=',atlag:0:2);
```

#### 4.3.3 Logikai típus

Az előzőeken kívül, elemi adattípusnak nevezhetjük még a logikai (Boolean) típust is, melynek értéke kizárólag igaz (True) ill. hamis (False) lehet.

#### 4.3.4 Karaktertípusok

Természetesen a Free Pascal szöveges jellegű adatok bevitelét és tárolását is támogatja. Ezek közül a Char típus a legkisebb, mely egyetlen karakter hosszúságú lehet, így értelemszerűen 1 bájton tárolható.

Ennél kiterjedtebb lehetőségeket kínáló karaktertípus a String típus, amely alapértelmezésben akár 255 karakternyi szöveget is tartalmazhat. Ezt a hosszt befolyásolhatjuk az alábbi módon:

```
var
  nev : string[50];
```

Így a `nev` nevű string változónk legfeljebb 50 karakternyi helyet foglalhat el. Tudnunk kell azonban, hogy a felső határ mindenképpen 255, még akkor is, ha többet adunk meg a változótípus deklarációjánál.

Ezektől az előre definiált típusoktól mentesen saját típusokat is létrehozhatunk a `type` lefoglalt szót követően, erről azonban majd a későbbi fejezetekben foglalkozunk bővebben.

<a name="5"></a>
## 5. Alapvető vezérlési szerkezetek

A Pascal program utasításai minden esetben sorban, egymás után hajtódnak végre. A programok legtöbbje azonban nem ilyen egyszerű, tehát nem csak annyi történik, hogy egy rakás utasítás egymás után lefut, majd a program kilép.
Módunkban áll vezérlőegységeket használni, melyek akár önálló programblokkokat is tartalmazhatnak, sőt egymásba is ágyazhatók. A leggyakrabban használt vezérlési szerkezetek az elágazások.

### 5.1 Elágazások

A program, amikor elágazásos vezérlőegységhez ér, mindig valamilyen feltételt kell, hogy megvizsgáljon. A vizsgálat alapján döntés születik, majd onnantól kezdve e döntés alapján folytatódik tovább a program futása.

Az elágazásoknak három fajtáját különböztetjük meg:

- egyirányú (`if then`)
- kétirányú (`if then else`)
- többirányú (`case of`)

#### 5.1.1 Egy­ és kétirányú elágazás

Az egyirányú elágazás úgy működik, hogy az `if` (ha) utasítással megvizsgáltatunk egy feltételt, és ha az igaz (True) értéket kap (vagyis a feltétel teljesül), akkor (`then`) egy önálló utasítás (vagy programblokk) hajtódik végre.
Ellenkező esetben (tehát, ha a feltétel eredménye hamis (False) értéket kap) a program továbblép az elágazáson, úgy is mondhatjuk: kikerüli azt.

Formája:

```
if feltétel then utasítás;
```

A vizsgálandó feltétel az alábbiak valamelyike lehet:

- logikai változó (Boolean, értékei: True, False)
- egyszerű relációs kifejezés (pl.: `a>5`)
- egyszerű logikai kifejezés (pl.: `szerkeszthető AND mehet`)
- összetett logikai kifejezés (pl: `(a>0) AND (b=1)`)

A kétirányú elágazás az egyirányútól annyiban különbözik, hogy az esetleges hamis ágon is önálló utasítás (vagy programblokk) hajtódik végre. Ekkor az `if .. then` szerkezet egy `else` (egyébként) résszel bővül.

A kétirányú elágazás általános formája:

```
if feltétel then utasítás1 else utasítás2;
```

Egyirányú elágazás: 

![alt_text](egyiranyu_elagazas.png "Egyirányú elágazás")

Kétirányú elágazás: 

![alt_text](ketiranyu_elagazas.png "Kétirányú elágazás")

Példa kétirányú elágazásra:

```
var
  x : integer;

begin
  x:=5;
  if x<4 then
    begin
      writeln('x kisebb 4-nél.');
      writeln('Lehetett volna nagyobb is, de végül nem így lett.');
    end
  else
    begin
      writeln('x nagyobb 4-nél.');
      writeln('Ez aztán a váratlan fordulat.');
    end;
end.
```

A fenti példaprogramunkban mindkét ágban egy­-egy programblokk hajtódik végre, ezért kellett használni a `begin`-`­end` szavakat. Fontos, hogy a `then` (vagyis a True ág) blokkját pontosvessző nélküli `end`-­el zárjuk, mert ott még nincs vége az `if` utasításnak!

#### 5.1.2 Többirányú elágazás

Akkor használunk többirányú elágazást, ha egy olyan kifejezést elemzünk ki, amelynek vizsgálata során több lehetséges eredményre juthatunk. Ez a feladat egymásba ágyazott `if` utasításokkal is megoldható lenne, de a `case` szelekciós változat alkalmazása rövidebb, kényelmesebb. Itt is használhatjuk az `else`-­t, ha a feltétel eredménye az `of` után megadottak közül egyikre sem lenne igaz.

![alt_text](tobbiranyu_elagazas.png "Többirányú elágazás")

Formája:

```
case szelektor of
  érték1 : utasítás1;
  érték2 : utasítás2;
  ...
  értékN : utasításN;
else
  utasítás_egyébként;
end;
```

Íme egy tipikus példaprogram többirányú elágazás használatára:

```
var
  jegy : byte;

begin
  write('Add meg a kapott érdemjegyet: '); readln(jegy);
  case jegy of
    1 : writeln('Karró?! Ejnye!');
    2 : writeln('Görbül!');
    3 : writeln('Ez is valami!');
    4 : writeln('Nem rossz!');
    5 : writeln('Ez az! Csak így tovább!');
  else
    writeln('Ez nem jegy!');
  end;
end.
```

Amennyiben több vizsgálati lehetőséghez szeretnénk ugyanazt az utasítássort megadni, halmazt is kiértékelhetünk a következő példához hasonlóan:

```
...
case jegy of
  1    : writeln('Megbukott.');
  2..4 : writeln('Átment.');
  5    : writeln('Kitűnő.');
...
```

### 5.2 Ciklusok

Akkor alkalmazunk ciklusokat, amikor egyazon programrészt többször kell végrehajtani. Hogy ez az ismétlődés hányszor menjen végbe, azt meghatározhatjuk előre, vagy függhet valamilyen feltétel vizsgálati eredményétől (tesztelésétől).

A ciklus belsejében lévő utasításokat ciklusmagnak nevezzük, mely több utasítás használatakor szintén begin end; közé kerül.

Ciklusfajták:

- növekményes (`for`)
- elöltesztelő (`while`)
- hátultesztelő (`repeat until`)

#### 5.2.1 A `for` ciklus

Akkor használjuk ezt a ciklust, ha pontosan meg tudjuk határozni az ismétlések számát. E ciklus tulajdonképpen kétféle folyamat előállítására képes: növeli vagy csökkenti a neki megadott változó (ciklusváltozó) kezdőértékét egy megadott végértékig.

A ciklikus növelést az alábbi módon adhatjuk meg:

```
for ciklusváltozó:=kezdőérték to végérték do
utasítás;
```

A lefelé számlálást pedig így:

```
for ciklusváltozó:=kezdőérték downto végérték do
utasítás;
```

A `for` ciklus folyamatábrája részletes, és az abból következő egyszerűsített ábrázolásban:

![alt_text](novekmenyes_ciklus.png "A növekményes ciklus")

```
CV = ciklusváltozó
CM = ciklusmag
KÉ = kezdőérték
VÉ = végérték
LK = lépésköz
```

A következő példaprogram a konzolos felületen használható 16 színt jeleníti meg. Itt találkozhatunk először más (nem csak az alap, `system`) unit használatával, melyhez a uses lefoglalt szó és a felhasznált unit nevének megadása szükséges. A `crt` unit egy jókora csokor konzol­ és billentyűzetkezelő eljárást illetve függvényt tartalmaz, többek között a `textcolor` eljárást, mely a konzol szövegszínének megváltoztatását végzi.

```
uses
  crt;

var
  szin : integer;

begin
  for szin:=0 to 15 do
  begin
    textcolor(szin);
    write(szin,' ');
  end;
end.
```

#### 5.2.2 A `while` ciklus

A `while` azért elöltesztelő ciklus, mert még a ciklus lefutása előtt egy feltétel alapján dönt arról, hogy a ciklusmag lefusson­e vagy sem. Tehát lehet, hogy egyszer sem fognak végrehajtódni a benne lévő utasítások, ha már a ciklusfeltételnél hamis eredmény születik.

Szerkezete:

```
while feltétel do
  utasítás;
```

Az elöltesztelő ciklus:

![alt_text](eloltesztelo_ciklus.png "Az elöltesztelő ciklus")

Következő programunk addig kér be egy számot a felhasználótól, amíg az 0 és 10 közé nem esik. Úgy működik, hogy amíg a feltétel True nem lesz, végrehajtja a ciklusmagot. A `= False` rész `<> True`-­val tetszőlegesen kicserélhető.

```
var
  szam : integer;

begin
  szam:=0;
  while (szam>0) and (szam<10) = False do
  begin
    write('Kérek egy számot 0 és 10 között: ');
    readln(szam);
  end;
  writeln('Na végre!');
end.
```

#### 5.2.3 A repeat ciklus

Ebben a ciklusban legalább egyszer mindenképpen lefut a ciklusmag, mert a ciklusfeltétel vizsgálata csak a ciklus végén – az `until` utasítás után – található. Emiatt nevezzük ezt hátultesztelő ciklusnak.
Mivel a repeat és az `until` már önmagukban is a ciklusmag kezdetét és végét jelentik, a több utasításból álló blokkot nem kötelező `begin` és `end;` közé tennünk.

```
repeat
  utasítás;
until feltétel;
```

A hátultesztelő ciklus:

![alt_text](hatultesztelo_ciklus.png "A hátultesztelő ciklus")

Mivel működése nagyon hasonlít az elöltesztelő cikluséhoz, ugyanazt a feladatot általában végrehajthatjuk hátultesztelővel is. Bizonyos műveletek egyikkel, mások a másikkal egyszerűbbek vagy gyorsabbak. Mindig el kell döntenünk, hogy éppen melyiket használjuk. Íme előző programunk `repeat`­-`until`­-os megvalósítása:

```
var
  szam : integer;

begin
  repeat
    write('Kérek egy számot 0 és 10 között: ');
    readln(szam);
  until (szam>0) and (szam<10);
  writeln('Na végre!');
end.
```

Egyébként az adott feladatot tekintve láthatóan a `repeat`­-`until`­-os megoldás az egyszerűbb.

### 5.3 A `goto` utasítás

Segítségével egy előre - ­a `label` utasítással - deklarált címkével jelölt területre ugorhatunk, átlépve a soron következő utasítást. Hátránya, hogy ezáltal programunk elveszti struktúráltságát, ezzel együtt szépségét. A legtöbb esetben `goto` nélkül, más utasításokkal is megoldhatunk szinte bármit.

Használata:

```
goto címke;
```

Ezzel az utasítással egy belső blokkból egy külsőbe ugorhatunk, de fordítva már nem. Önálló eljárásokból vagy függvényekből (vagyis alprogramokból) se ki, se be nem ugrálhatunk. Alapesetben a Free Pascal nem is támogatja a `goto` és label utasítások használatát, de a `{$GOTO ON}` fordítási direktívával "ráerőltethetjük”. Következő példaprogramunkban végigkövethetjük a `goto` működését:

```
{$GOTO ON}
var
  v : byte;

label
  ide, oda, amoda;

begin
  writeln('1-es gomb: "ide"; 2-es: "oda" lépsz.');
  write('Választásod: ');
  readln(v);
  if v=1 then goto ide;
  if v=2 then goto oda else
  begin
    writeln('Se 1-est, se 2-est nem nyomtál!');
    goto amoda;
  end;

  ide:
    writeln('1-est nyomtál, "ide" léptél!');
    goto amoda;

  oda:
    writeln('2-est nyomtál, "oda" léptél!');
    goto amoda;

  amoda:
    readln;

end.
```

Láthatóan már ez a program is sokkal bonyolultabb, mintsem azt az adott feladat megkövetelné. A `goto` használat tehát lehetséges, de bármikor is szükség lenne rá, mindig el lehet, és el is kell kerülni a használatát. A program sokkal kifinomultabb, ha kifejezések vizsgálatával és a vizsgálatból következő utasításokból áll.

A lehetséges kifejezések közül néhánnyal már találkoztunk ebben a fejezetben. A következőben megismerhetjük, pontosan milyen lehetőségeket ad a kezünkbe a Free Pascal.

<a name="6"></a>
## 6. Kifejezések

A kifejezések kétféle összetevőből állnak: operátorokból és azok operandusaiból. Az operátorok tulajdonképpen a műveletek, az operandusok, pedig a számok, értékek, melyekkel a műveleteket végezzük. Nézzünk egy tipikus példát, ahol az egyik értéket összehasonlítjuk a másik értékkel:

```
a>b
```

Ebben a kifejezésben azt vizsgáljuk, hogy az `a` nagyobb-e `b` operandusnál. A nagyobb jelet ábrázoló kacsacsőr egy relációs típusú operátor. A bináris operátorok közé is csoportosíthatjuk, ami azt jelenti, hogy két operandus tartozik hozzá. A legtöbb operátor bináris.

### 6.1 Aritmetikai operátorok

Alapvető számolási műveleteket végezhetünk velük egész vagy valós típusú számokkal. Ezek a következők lehetnek:
- `+`	összeadás
- `-`	kivonás
- `*`	szorzás
- `/`	osztás
- `div`	egész típusba osztás
- `mod`	osztás maradékképzése

Az aritmetikai operátorokhoz tartozik még továbbá két "unáris" operátor a `+` és a `-` előjel.

### 6.2 Logikai operátorok

Logikai (boolean) műveleteket végeznek el logikai típusú operandusok között logikai típusú eredményt hozva. Egyszerű logikai kifejezések, valamint több más típusú kifejezés összekapcsolásával összetett logikai kifejezések alakíthatók ki velük.

Az alábbiak tartoznak ebbe a csoportba:
- `not`	logikai tagadás
- `and`	logikai és
- `or`	logikai vagy
- `xor`	logikai kizáró ­vagy

### 6.3 Relációs operátorok

Egész és valós típusú operandusok között végezhetünk velük vizsgálatokat:
- `=`	egyenlő
- `<>`	nem egyenlő
- `<`	kisebb, mint...
- `>`	nagyobb, mint...
- `<=`	kisebb-­egyenlő, mint...
- `>=`	nagyobb-­egyenlő, mint...
- `in`	egyik eleme­e a másiknak

Példák egyszerű és összetett kifejezésekre:

```
e:=(a+b)/(c-d);
if (a mod b)<>0 then...
if (a+b)=4 and (c-d)<=5 then...
```

### 6.4 Szövegösszefűző operátor

Egymástól elkülönített szövegeket, vagy egy változó­ ill. konstansnév utáni szöveget a `+` operátorral tudunk kiíratáskor összefüggővé tenni az alábbi módokon:

```
writeln('Addig jár a korsó a kútra,'
        +' amíg be nem vezetik a vizet.');
writeln(konyvtarnev+'/');
```

A fentieken kívül még néhány különleges funkciójú operátor is létezik, melyekkel majd a későbbi fejezetekben fogunk találkozni.

<a name="7"></a>
## 7. Alprogramok és unit­ok

Egy hosszabb program esetében mindenképpen szembe találnánk magunkat azzal a problémával, hogy egy-­egy összefüggő utasítássort kénytelenek vagyunk rendszeresen megismételni. Szerencsére lehetőségünk van arra, hogy ezeket az utasításokat még felhasználásuk előtt egységbe foglaljuk, ezt az egységet egy azonosítóval ellássuk, majd azután bármikor meghívhatjuk. Alprogram használatával egy ilyen egység az adott program futási ideje alatt tulajdonképpen kibővíti a Free Pascal utasításkészletét. Unit esetében az alprogramok egy külön fájlban találhatóak meg, amiket akár más program is felhasználhat.

### 7.1 Alprogramok

Az alprogramoknak – mint, ahogy a beépített utasításoknak – két fajtája van:

- eljárások (`procedure`)
- függvények (`function`)

Eljárást használunk, ha egy előre meghatározott programblokknak kell végrehajtódnia, esetleges paraméterektől függően. Miután lefutott, a következő eljárás végrehajtása következik.

A függvények feladata, hogy valamilyen visszatérési értéket produkáljanak az őket igénybe vevő utasítás (eljárás vagy akár értékadás) számára.

#### 7.1.1 Eljárások

Egy eljárásnak nem szükséges paraméterekkel rendelkeznie (pl. `clrscr`), viszont akkor csak egy valamit tud egyféleképpen végrehajtani.

Egy egyszerű eljárás általános meghatározása:

```
procedure eljárás_azonosító;
begin
  utasítás1;
  utasítás2;
  ...
end;
```

Lehetőségünk van azonban olyan eljárásokat alkotni, melyeknek majd felhasználásukkor paraméter(eke)t adhatunk. Mivel ezek valóban teljesen önálló programok (alprogramok), így saját változókat, konstansokat stb. is tartalmazhatnak, melyek lokálisak, tehát kívülről nem láthatók, nem használhatók.

Nézzük, hogyan is néz ki általános formában egy paraméterekkel és lokális változókkal rendelkező eljárás (fp=formális paraméter):

```
procedure eljárás_azonosító(fp1 : típus1; fp2, fp3 : típus2; ...);
  var
    változó1 : típus;
    ...
  begin
    utasítás1;
    utasítás2;
    ...
  end;
```

A következő példán keresztül láthatjuk, hogyan is néz ki egy egyszerű, saját eljárást használó program:

```
uses
  crt;

procedure kekhatter(szoveg : string);
  begin
    textbackground(blue);
    write(szoveg);
    textbackground(black);
    readln;
  end;

begin
  kekhatter('Itt kék a háttér.');
end.
```

A főprogram a saját eljárásnak köszönhetően csak egyetlen utasításból áll.

#### 7.1.2 Függvények

A függvényeknek visszatérési értékkel kell megjelenniük, tehát egy függvény lefutásának eredménye mindenképpen valamilyen típusú érték. A függvény is tartalmazhat az eljáráshoz hasonlóan paraméterlistát, lokális deklarációkat. A törzs részben, egy értékadás során kell meghatároznunk, mit fog végül eredményül hozni alprogramunk.

A függvények általános leírása:

```
function függvény_azonosító(fp1 : típus1; fp2 : típus2; ...) : visszatérési_típus;
  var
    változó1 : típus;
    ...
  begin
    utasítás1;
    utasítás2;
    ...
    függvény_azonosító := kifejezés vagy változó;
  end;
```

Következő példánkban egy egyszerű, két számból átlagot kiszámoló függvényt láthatunk. A főprogramban eljárásként is meghívjuk a függvényt, de ez a felhasználó számára nem jár semmiféle látvánnyal:

```
uses
  crt;

function atlag(x, y : integer) : real;
  begin
    atlag := (x+y)/2;
  end;

begin
  atlag(100,200);
  writeln(atlag(432,123):0:2);
end.
```

### 7.2 Unit­ok

Több, egymással összefüggésben lévő alprogramot egy külön fájlban, úgynevezett modulban (idegen szóval unit­ban) is tárolhatunk. A modulokat a hagyományos programokhoz hasonlóan forráskódból fordítjuk, az elkészült bináris állomány azonban automatikusan egy `.ppu` kiterjesztést kap. Ezt a különálló modult a program fejrészében, a uses modulnév utasítással tudjuk bevonni a program keresési sorába. A Free Pascal­nak a fordítás során a system unit­ot mindenképpen meg kell találnia, ezt a forráskódban nem is kell külön feltüntetni.

Ezen kívül rengeteg gyári unit­ot kapunk kézhez, melyek egy­egy önálló feladatsor ellátásáért felelősek, pl. a crt (`crt.ppu`) unit a karakteres képernyő­ és billentyűzetkezelő alprogramokat tartalmaz, míg a cdrom modul a CD/DVD meghajtó eszköz kezeléséhez használatos.

A unit­ok forrása valamelyest eltér a hagyományos programokétól. Nézzük, hogyan készíthetünk el egy saját unit­ot:

1. rész: a modul fejrésze

```
unit azonosito;  // fontos, hogy a fájl neve megegyezzen a itt szereplő azonosítóval!
```

2. rész: kapcsolódási felület (interfész)

```
interface
uses ...;  // modulok listája
type ...;  // globális (kívülről is elérhető) típusok, konstansok és változók
const ...;
var ...;
procedure ...;  // kívülről elérhető eljárások fejlécei
function ...;   // kívülről elérhető függvények fejlécei
```

3. rész: kifejtés (implementáció)

```
implementation
uses ...;  // modulok listája
type ...;  // lokális (csak a unitban elérhető) típusok, konstansok, változók
const ...;
var ...;
procedure ...;  // lokális eljárások definiálása
function ...;   // lokális függvények definiálása
procedure ...;  // globális eljárások kifejtése
function ...;   // globális függvények kifejtése
```

4. rész: inicializálás (ez a rész el is hagyható)

```
begin
  utasítás1;
  utasítás2;
end.
```

Mindig igyekezzünk betartani a fenti szabályokat és talán nem érhet minket meglepetés!
Természetesen, ha változtatunk valamit a unit forráskódján és újrafordítjuk azt, akkor rögtön a unit­ot használó programot is újra kell fordítanunk.

<a name="8"></a>
## 8. Strukturált típusok

Az alaptípusokkal egy adott változónak adhatunk egy bizonyos típust, mely aztán a típusának megfelelő értéket kaphat. Egy strukturált típusnak azonban több, egymástól eltérő értékű eleme is lehet, melyek mégis szoros kapcsolatban vannak egymással, belül egy struktúrát létrehozva ezzel.

A Free Pascal az alábbi strukturált típusokat támogatja:

- `array` (tömb) típus
- `record` (rekord) típus
- `object` (objektum) típus
- `class` (osztály) típus
- `class reference` (osztály referencia) típus
- `set` típus
- `file` típus

### 8.1 Tömbök

Akkor használunk tömböket, ha nagyobb számú, azonos típusú változóra (elemre) van szükségünk. A tömbben tárolt elemek mindegyikének azonosítása azok indexe alapján történik. Egy tömb egy adott elemére az elemet tároló tömb azonosítója és az elem indexének segítségével hivatkozhatunk.

#### 8.1.1 A vektor

A legegyszerűbb tömb a vektor (egydimenziós tömb), amelynek deklarációja így történik:

```
var
  tömb_azonosító : array[alsó_index..felső_index] of elemek_típusa;
```

A tömb elemeinek számát tehát meghatározhatjuk így is:

```
felső_index - alsó_index + 1, a tömb méretét pedig így: elemszám * elemtípus_helyfoglalása.
```

A program belsejében a `tömb_azonosító[index]` szintaktikával hivatkozunk az elemre.

A következő programrészletben egy 100 elemű tömböt deklarálunk, majd az 5. elem felhasználását is láthatjuk:

```
var
t : array[1..100] of byte;
begin
t[5]:=123;
writeln(t[5]);
...
```

#### 8.1.2 A mátrix

A tömb egyes elemeinek mindegyike is tartalmazhat önálló tömböt, melynek eredményeképpen kétdimenziós, más néven mátrix tömböt kapunk. Gondoljuk csak a torpedó játékra, ahol a játékmező pontjait sorok és oszlopok címei alapján azonosítjuk. Itt is valami hasonló történik. Egy ilyen kétdimenziós tömböt a következőképpen deklarálhatunk:

```
var
  pontok : array[1..10] of array[1..10] of byte;

ami egyenértékű az alábbi, egyszerűsített deklarációval:

var
  pontok : array[1..10,1..10] of byte;
```

Tovább fokozva a dolgot kettőnél több, maximálisan nyolcdimenziós mélységű mátrix tömb is létrehozható, bár olyanra valószínűleg sohasem lesz szükségünk.
A tömb összes elemét, illetve nagyobb terjedelmű, de egybefüggő halmazát könnyen bejárhatjuk a már megismert for ciklussal. Ehhez egy újabb változót kell deklarálnunk, ami a tömb elemeinek indexeit tartalmazza a for ciklus futása alatt. Egy 5 elemet tartalmazó tömb bejárását láthatjuk a következő kódrészletben:

```
var
     i : byte;
  tomb : array[1..5] of string;
...
begin
for i:=1 to 5 do
... tomb[i] ...
```

Segítséget nyújthatnak a `low` és a `high` függvények is, melyekkel a tömb legkisebb illetve legnagyobb indexét kaphatjuk meg. Használatukkor lehetőségünk van a tömbelemek számának utólagos változtatására, hiszen mindig a legelső és legutolsó indexet fogják eredményül adni. Az előbbi példából kiindulva ezek az 1 és az 5 indexű elemek.

Lássunk egy teljes értékű példát a tömbök használatára a `low` és a `high` függvények felhasználásával. A program egy eddig nem tárgyalt, crt unitban fellelhető utasítást is tartalmaz, a `clrscr­`-t, mely a képernyő törlését végzi el.

```
uses
  crt;

var
  i    : byte;
  tomb : array[1..5] of string;

begin
  clrscr;
  for i:=low(tomb) to high(tomb) do
  begin
    write('Kérem a(z) ',i,'. elemet: ');
    readln(tomb[i]);
  end;
  writeln('Az elemek fordított sorrendben:');
  for i:=high(tomb) downto low(tomb) do
    write(tomb[i],' ');
  readln;
end.
```

Típusdeklaráció használatával is megadhatunk tömböket. Ebben az esetben az eljárások és függvények képesekké válnak paraméterezésként elfogadni ezeket a típusokat. Ezt mutatja be az alábbi példa:

```
type
  nevek = array[1..40] of string[30];

var
  tanulok : nevek;
```

Mindezen túl létrehozhatunk tömböt úgynevezett típusos konstansban is, melyben azonnal értéket is kell, hogy kapjanak a tömb elemei:

```
const
szamok : array[1..5] of integer = (14, 423, 2, 8754, 29);
```

### 8.2 Rekordok

A rekord egy rendkívül rugalmas adatszerkezet, mivel nemcsak tetszőleges számú, hanem akár teljesen különböző típusú adatot is tartalmazhat. A rekord fogalmát egész egyszerűen így szoktuk meghatározni: összetartozó adatok halmaza. A rekord elemei a mezők, melyek nevét és típusát több utasításban, több sorban végezzük. A rekorddeklarációt az end; szóval zárjuk le. Miután a type résszel végeztünk, még meg kell adnunk a mezők hozzáféréséhez egy változót:

```
type
  rekordnév = record
    mező1 : típus1;
    mező2 : típus2;
    ...
  end;

var
  rekordváltozó : rekordnév;
```

A rekord mezőihez kétféle módon férhetünk hozzá:

- a `with` utasítással:

```
    with rekordváltozó do
      begin
        mező1:=érték1;
        mező2:=érték2;
        ...
      end;
```

- vagy a `.` (pont) alkalmazásával:


```
    rekordváltozó.mező1:=érték1;
    rekordváltozó.mező2:=érték2;
    ...
```

Hogy éppen mikor, melyik módot vesszük igénybe, az az adott helyzettől függ.

A következő példaprogramban mindkét lehetőség felhasználásra kerül. A programban létrehozunk egy rekordtípust, abban 4 mezőt, majd a felhasználó által megadott adatokkal 3 rekordot viszünk be. A bevitel után a program sorban visszaolvassa nekünk a bevitt adatok egy részét. Figyeljük meg, milyen jól alkalmazható itt egy vektortömb és a körbejárásához használt for ciklus:

```
uses
  crt;

type
  adat = record
    nev       : string[30];
    szul_hely : string[20];
    szul_ev   : word;
    adoszam   : int64;
  end;

var
  szemely : array[1..3] of adat;
  i       : byte;

begin
  clrscr;
  for i:=1 to 3 do
  begin
    with szemely[i] do
    begin
      writeln(i,'. rekord:');
      write('Név: '); readln(nev);
      write('Születési hely: '); readln(szul_hely);
      write('Születési év: '); readln(szul_ev);
      write('Adószám: '); readln(adoszam);
    end;
  end;
  clrscr;
  for i:=1 to 3 do
  begin
    writeln(szemely[i].nev,' születési helye: '
           +szemely[i].szul_hely,'.');
  end;
  readln;
end.
```
A Free Pascal támogatja továbbá a case szelekciós elágazás használatát a rekorddeklaráción belül, sőt az elágazások tovább is ágaztathatók. Ezekre láthatunk példákat a következőkben:

```
type
  sajatrekord1 = record
    case boolean of
      false : (x, y, z : real);
      true : (r, theta, phi : real);
  end;
  sajatrekord2 = record
    case pontok : boolean of
      false : (x, y, z : real);
      true : (r, theta, phi : real);
  end;
  sajatrekord3 = record
    x : longint;
    case byte of
    2 : (y : longint;
      case byte of
        3 : (z : longint);
      );
  end;
```

>Az anyag szerkesztését végül soha nem fejeztem be, pedig innentől következnének csak az igazán izgi dolgok: az objektumok, osztályok és megannyi más. Talán majd egyszer...