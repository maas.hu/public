# ![alt text](/_ui/img/icons/sp_m.svg) IBM Spectrum Protect

###### .

>Írogatás alatt...

## Tartalomjegyzék

1. [Bevezető](#1)
2. [User adminisztráció](#2)
3. [Alapkonfiguráció ellenőrzése](#3)
4. [Az IBM SP Server telepítése](#4)
5. [Az IBM SP Server konfigurálása](#5)
6. [Az IBM SP Server frissítése](#6)
7. [Backup schedules konfigurálása](#7)
8. [IBM SP Server DB restore](#8)
9. [Egyéb karbantartási feladatok](#9)
10. [Hibakeresés](#10)
11. [Hasznos SQL Query-k](#11)

<a name="1"></a>
## 1. Bevezető

Az IBM Spectrum Protect – korábbi nevén Tivoli Storage Manager, azaz TSM – átfogó adatállóságot biztosít a fizikai fájlszerverek, a virtuális környezetek és az alkalmazások széles skálája számára. A kapcsolódó kliensek akár több milliárd objektumot is kezelhetnek biztonsági mentési kiszolgálónként. Az ügyfelek csökkenthetik a biztonsági mentési infrastruktúra költségeit a beépített adathatékonysági képességekkel és az adatok migrálásával vagy másolásával szalagra, nyilvános felhőszolgáltatásokba és helyszíni objektumtárolóba. - Ez lenne a marketingszöveg. Évekig ez, az IBM backupmegoldása volt magasan piacvezető világszerte és még napjainkra is a folyamatosan az elsők között szerepel a különféle felmérések szerint. 

Íme egy tipikus, ugyanakkor egyszerű, IBM Spectrum Protect által "védett" környezet az egymással összeköttetésben lévő gépek szempontjából, ahol az IBM SP szerver épp AIX-en fut, a saját storage pool-ja LVM mirrorral biztosított és közvetlen kapcsolatban áll egy IBM Physical Tape Library-vel is: 

![alt_text](sp-hosts.png "IBM Spectrum Protect - kapcsolódó gépek")


<a name="2"></a>
## 2. User adminisztráció

Belépés lokálisan, már üzemben lévő szerveren: 

```ksh
$ dsmadmc
IBM Spectrum Protect
Command Line Administrative Interface - Version 8, Release 1, Level 10.0
(c) Copyright by IBM Corporation and other(s) 1990, 2020. All Rights Reserved.

Enter your user id:  gjakab

Enter your password:

Session established with server TSMSRV1: Linux/x86_64
  Server Version 8, Release 1, Level 12.100
  Server date/time: 20/10/21   07:27:24  Last access: 21/09/21   10:14:57
```

### 2.1 Admin userek listázása

```
Protect: TSMSRV1>q admin

Administrator       Days Since       Days Since       Locked?       Privilege Classes
Name                Last Access     Password Set
--------------     ------------     ------------     ----------     -----------------------
ADMIN                       517              413         No         System
GJAKAB                       <1              413         No         System
SERVER_CONSOLE                                           No         System
SUPPORT                      <1              314         No         System
```

### 2.2 Jelszóváltoztatás

```
Protect: TSMSRV1>update admin gjakab <your_password_goes_here>
ANR2071I Administrator GJAKAB updated.
Protect: TSMSRV1>quit
ANS8002I Highest return code was 3.
```

Az alapértelmezés szerint a global server paraméternek 90 napos jelszólejárat van beállítva. Ezt az alábbi módon változtathatjuk meg soha le nem járóra: 

```
Protect: TSMSRV1>q status
IBM Spectrum Protect Server for Linux/x86_64 - Version 8, Release 1, Level 12.100
...
                   Password Expiration Period: 90 Day(s)
...
Protect: TSMSRV1>set passexp 9999
ANR2092I Password expiration period set to 9999 days.
```

### 2.3 Admin user létrehozása és a "system" role-hoz rendelése

```
Protect: TSMSRV1>reg admin gjakab <there_is_a_password_here>
Session established with server TSMSRV1: Linux/x86_64
IBM Spectrum Protect Server for Linux/x86_64 - Version 8, Release 1, Level 12.100
Server date/time: 20/05/20 14:28:32 Last access: 20/05/20 14:08:20
ANR2068I Administrator GJAKAB registered.

Protect: TSMSRV1>q admin

Administrator       Days Since       Days Since       Locked?       Privilege Classes
Name                Last Access     Password Set
--------------     ------------     ------------     ----------     -----------------------
ADMIN                       517              413         No         System
GJAKAB                       <1              <1          No
SERVER_CONSOLE                                           No         System
SUPPORT                      <1              314         No         System

Protect: TSMSRV1>grant auth gjakab cl=system
ANR2076I System privilege granted to administrator GJAKAB.

Protect: TSMSRV1>q admin

Administrator       Days Since       Days Since       Locked?       Privilege Classes
Name                Last Access     Password Set
--------------     ------------     ------------     ----------     -----------------------
ADMIN                       517              413         No         System
GJAKAB                       <1              <1          No         System
SERVER_CONSOLE                                           No         System
SUPPORT                      <1              314         No         System
```

<a name="3"></a>
## 3. Alapkonfiguráció ellenőrzése

Egy meglévő, egyetlen node-ot tartalmazó szervert veszünk alapul (hibák akadhatnak) a továbbiakban, legalábbis ebben a fejezetben. 

### 3.1 Domains, Device Classes, Storage Pools, Copy Groups, Policy Sets and Management Classes

A [hivatalos](https://publib.boulder.ibm.com/tividd/td/TSMCW/GC32-0784-00/en_US/HTML/anrwqs07.htm) áttekintő ábra, hogy hogyan is kapcsolódnak egymáshoz az egyes objektumok az IBM SP szerver konfigurációjában: 

![alt_text](sp-components.gif "IBM Spectrum Protect - objektumok")

#### 3.1.1 Domain-ek lekérdezése

```
Protect: TSMSRV1>q dom

Policy        Activate-     Activate-     Number of      Description
Domain        d Policy      d Defaul-     Registere-
Name          Set           t Mgmt           d Nodes
                            Class
---------     ---------     ---------     ----------     ------------------------
MYDOM         STANDARD      NOLIMIT                1     Installed default
                                                          policy domain.
STANDARD      STANDARD      STANDARD               0     Installed default
                                                          policy domain.

Protect: TSMSRV1>q dom MYDOM f=d

Policy    Activate- Activatio- Days Sinc- Activate- Number of  Description                Backup   Archive  Last Update by  Last Upda- Managing profile      Changes   Active      Domain
Domain    d Policy  n Date/Ti- e Activat- d Defaul- Registere-                          Retentio- Retentio- (administrator) te Date/T-                       Pending   Data Pool   Type
Name      Set           me            ion t Mgmt       d Nodes                          n (Grace  n (Grace                     ime                                     List
                                          Class                                           Period)   Period)
--------- --------- ---------- ---------- --------- ---------- ------------------------ --------- --------- --------------- ---------- -------------------- ---------- ----------- ----------
MYDOM     STANDARD  19/05/20          519 NOLIMIT            1 Installed default               30       365 ADMIN           19/05/20                            No                 Standard
                      16:03:20                                  policy domain.                                                16:03:20
```

Szintaxis (`h q dom`): 

```
Syntax

                 .-*-----------.  .-Format--=--Standard-----.
>>-Query DOmain--+-------------+--+-------------------------+--><
                 '-domain_name-'  '-Format--=--+-Standard-+-'
                                               '-Detailed-'
```

#### 3.1.2 Device Class-ok lekérdezése

```
Protect: TSMSRV1>q devc

Device        Device         Storag-     Device        Format     Est/Max      Mount
Class         Access         e Pool      Type                     Capacit-      Limit
Name          Strategy         Count                                y (MB)
---------     ----------     -------     ---------     ------     --------     ------
DBBACKUP      Sequential           0     FILE          DRIVE      10,240.0         20
DISK          Random               4
MYDEVC_COPY   Sequential           1     FILE          DRIVE      10,240.0         20

Protect: TSMSRV1>q devc DBBACKUP f=d

             Device Class Name: DBBACKUP
        Device Access Strategy: Sequential
            Storage Pool Count: 0
                   Device Type: FILE
                        Format: DRIVE
         Est/Max Capacity (MB): 10,240.0
                   Mount Limit: 20
              Mount Wait (min):
         Mount Retention (min):
                  Label Prefix:
                       Library:
                     Directory: /tsm/dbbackup
                   Server Name:
                  Retry Period:
                Retry Interval:
                        Shared: No
            High-level Address:
              Minimum Capacity:
                          WORM: No
              Drive Encryption:
               Scaled Capacity:
       Primary Allocation (MB):
     Secondary Allocation (MB):
                   Compression:
                     Retention:
                    Protection:
               Expiration Date:
                          Unit:
      Logical Block Protection:
               Connection Name:
Last Update by (administrator): ADMIN
         Last Update Date/Time: 19/05/20   15:29:03

```

#### 3.1.3 Storage Pool-ok lekérdezése

```
Protect: TSMSRV1>q stgp

Storage         Device         Storage       Estimated       Pct       Pct      Hig-     Lo-     Next Stora-
Pool Name       Class Name     Type            Capacity      Util      Migr     h M-      w      ge Pool
                                                                                 ig      Mi-
                                                                                 Pct      g
                                                                                         Pct
-----------     ----------     ---------     ----------     -----     -----     ----     ---     -----------
ARCHIVEPOOL     DISK           DEVCLASS           0.0 M       0.0       0.0       90      70
BACKUPPOOL      DISK           DEVCLASS           0.0 M       0.0       0.0       90      70
MYDEVC_COPY     MYDEVC_COPY    DEVCLASS            14 G      39.8
MYDEVC_DSK      DISK           DEVCLASS            10 G      54.5      54.5       90      70
SPACEMGPOOL     DISK           DEVCLASS           0.0 M       0.0       0.0       90      70

Protect: TSMSRV1>q stgp MYDEVC_DSK f=d

                     Storage Pool Name: MYDEVC_DSK
                     Storage Pool Type: Primary
                     Device Class Name: DISK
                          Storage Type: DEVCLASS
                            Cloud Type:
                             Cloud URL:
                        Cloud Identity:
                        Cloud Location:
                    Estimated Capacity: 10 G
                    Space Trigger Util: 54.5
                              Pct Util: 54.5
                              Pct Migr: 54.5
                           Pct Logical: 100.0
                          High Mig Pct: 90
                           Low Mig Pct: 70
                       Migration Delay: 0
                    Migration Continue: Yes
                   Migration Processes: 1
                 Reclamation Processes:
                     Next Storage Pool:
                  Reclaim Storage Pool:
                Maximum Size Threshold: No Limit
                                Access: Read/Write
...
```

#### 3.1.4 Copy Group-ok lekérdezése

```
Protect: TSMSRV1>q copyg

Policy        Policy        Mgmt          Copy          Version-     Version-      Retain      Retain
Domain        Set Name      Class         Group          s Data       s Data        Extra        Only
Name                        Name          Name            Exists      Deleted     Versions     Version
---------     ---------     ---------     ---------     --------     --------     --------     -------
MYDOM         ACTIVE        NOLIMIT       STANDARD      No Limit     No Limit     No Limit     No Lim-
                                                                                                    it
MYDOM         ACTIVE        TAPE          STANDARD      No Limit     No Limit     No Limit     No Lim-
                                                                                                    it
MYDOM         STANDARD      NOLIMIT       STANDARD      No Limit     No Limit     No Limit     No Lim-
                                                                                                    it
MYDOM         STANDARD      TAPE          STANDARD      No Limit     No Limit     No Limit     No Lim-
                                                                                                    it
STANDARD      ACTIVE        STANDARD      STANDARD             2            1           30          60
STANDARD      STANDARD      STANDARD      STANDARD             2            1           30          60

Protect: TSMSRV1>q copyg MYDOM f=d

                 Policy Domain Name: MYDOM
                    Policy Set Name: ACTIVE
                    Mgmt Class Name: NOLIMIT
                    Copy Group Name: STANDARD
                    Copy Group Type: Backup
               Versions Data Exists: No Limit
              Versions Data Deleted: No Limit
              Retain Extra Versions: No Limit
                Retain Only Version: No Limit
                          Copy Mode: Modified
                 Copy Serialization: Shared Static
                     Copy Frequency: 0
                   Copy Destination: MYDEVC_DSK
Table of Contents (TOC) Destination:
     Last Update by (administrator): ADMIN
              Last Update Date/Time: 19/05/20   16:00:37
                   Managing profile:
                    Changes Pending: No

                 Policy Domain Name: MYDOM
                    Policy Set Name: ACTIVE
                    Mgmt Class Name: TAPE
                    Copy Group Name: STANDARD
                    Copy Group Type: Backup
               Versions Data Exists: No Limit
              Versions Data Deleted: No Limit
              Retain Extra Versions: No Limit
                Retain Only Version: No Limit
                          Copy Mode: Modified
                 Copy Serialization: Shared Static
                     Copy Frequency: 0
                   Copy Destination: MYDEVC_DSK
Table of Contents (TOC) Destination:
...
```

#### 3.1.5 Policy Set-ek lekérdezése

A Policy Set-ek és a hozzá kapcsolódó további objektumok hierarchiája [a hivatalos doksi szerint](https://www.meduniwien.ac.at/itsc/services/backup/docs/pubs52/aix/html/guide/anragd51121.htm) így néz ki: 

![alt_text](sp-policy_mgmt.gif "IBM Spectrum Protect - policy management") 

```
Protect: TSMSRV1>q pol

Policy        Policy        Default       Description
Domain        Set Name      Mgmt
Name                        Class
                            Name
---------     ---------     ---------     ------------------------
MYDOM         ACTIVE        NOLIMIT       Installed default
                                           policy set.
MYDOM         STANDARD      NOLIMIT       Installed default
                                           policy set.
STANDARD      ACTIVE        STANDARD      Installed default
                                           policy set.
STANDARD      STANDARD      STANDARD      Installed default
                                           policy set.
```

#### 3.1.6 Management Class-ok lekérdezése

```
Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        NOLIMIT       Yes            Installed default
                                                          management class.
MYDOM         ACTIVE        TAPE          No             Installed default
                                                          management class.
MYDOM         STANDARD      NOLIMIT       Yes            Installed default
                                                          management class.
MYDOM         STANDARD      TAPE          No             Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.
```

### 3.2 Backup Script-ek és Schedule-ök

#### 3.2.1 Backup Script-ek

```
Protect: TSMSRV1>q scr

Name                Description                                            Managing profile
---------------     --------------------------------------------------     --------------------
BACKUP_STGP
DBBACKUP
DBINCR

Protect: TSMSRV1>q scr DBBACKUP f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBBACKUP       1          backup db devc=dbbackup t=f w=y
               5          backup volh
               10         backup devconfig
               15         delete volh t=all todate=-7
               20         delete volhistory type=dbbackup todate=today-3

Protect: TSMSRV1>q scr DBBACKUP f=r
backup db devc=dbbackup t=f w=y
backup volh
backup devconfig
delete volh t=all todate=-7
delete volhistory type=dbbackup todate=today-3
```

Szintaxis (`h q scr`): 

```
                 .-*-----------.
>>-Query SCRipt--+-------------+-------------------------------->
                 '-script_name-'

   .-FORMAT--=--Standard------------------------------.
>--+--------------------------------------------------+--------><
   '-FORMAT--=--+-Standard--------------------------+-'
                +-Detailed--------------------------+
                +-Lines-----------------------------+
                '-Raw--+--------------------------+-'
                       '-Outputfile--=--file_name-'
```

#### 3.2.2 Schedule-ök

A **client schedule**-ök lekérdezése:

```
Protect: TSMSRV1>q sch
ANR2034E QUERY SCHEDULE: No match found using this criteria.
ANS8001I Return code 11.
```

Egy másik szerveren, ahol vannak is valós kliensek: 

```
Protect: TSMSRV2>q sch

Domain           *     Schedule Name        Action     Start Date/Time          Duration     Period     Day
------------     -     ----------------     ------     --------------------     --------     ------     ---
AB_W2K                 CALLC_ARC_ONCE       ImgBck     08/18/15   22:00:00          1 H        Once     Any
AB_W2K                 IMAGE9_CCENTER       ImgBck     05/28/11   05:00:00          5 H                 (*)
AB_W2K                 IMAGESNAP_CCENT-     ImgBck     05/28/11   05:00:00          5 H                 (*)
                        ER
AB_W2K           *     PLAGER_INCR          Inc Bk     04/07/11   22:00:00          5 H        1 D      Any
AB_W2K           *     W2K_2WEEKLY_2300     Inc Bk     08/25/06   23:00:00          1 H        2 W      Sun
...
```

Az **administrative schedule**-ök lekérdezése:

```
Protect: TSMSRV1>q sch t=a

*     Schedule Name        Start Date/Time          Duration     Period     Day
-     ----------------     --------------------     --------     ------     ---
      BACKUP_STGP          05/06/20   11:30:00         50 M        1 H      Any
      DBBACKUP             19/05/20   18:00:00          1 H        1 D      Any
      DBINCR               05/06/20   13:00:00          1 H        2 H      Any
```

### 3.3 Eventek és Activity Logok

#### 3.3.1 Eventek lekérdezése

A legutóbbi és a hamarosan következő admin eventek: 

```
Protect: TSMSRV1>q event * t=a
Session established with server TSMSRV1: Linux/x86_64
  Server Version 8, Release 1, Level 12.100
  Server date/time: 20/10/21   08:33:20  Last access: 20/10/21   07:27:26


Scheduled Start          Actual Start             Schedule Name     Status
--------------------     --------------------     -------------     ---------
20/10/21   00:30:00      20/10/21   00:30:11      BACKUP_STGP       Completed
20/10/21   01:00:00      20/10/21   01:00:12      DBINCR            Completed
20/10/21   01:30:00      20/10/21   01:30:14      BACKUP_STGP       Completed
20/10/21   02:30:00      20/10/21   02:30:17      BACKUP_STGP       Completed
20/10/21   03:00:00      20/10/21   03:00:18      DBINCR            Completed
20/10/21   03:30:00      20/10/21   03:30:19      BACKUP_STGP       Completed
20/10/21   04:30:00      20/10/21   04:30:22      BACKUP_STGP       Completed
20/10/21   05:00:00      20/10/21   05:00:24      DBINCR            Completed
20/10/21   05:30:00      20/10/21   05:30:25      BACKUP_STGP       Completed
20/10/21   06:30:00      20/10/21   06:30:28      BACKUP_STGP       Completed
20/10/21   07:00:00      20/10/21   07:00:00      DBINCR            Completed
20/10/21   07:30:00      20/10/21   07:30:01      BACKUP_STGP       Completed
20/10/21   08:30:00      20/10/21   08:30:04      BACKUP_STGP       Completed
20/10/21   09:00:00                               DBINCR            Future
...
```

Kliens-eventek utáni keresés szintaxisa (`h 3.49.29.1`): 

```
>>-Query EVent--domain_name--schedule_name---------------------->

   .-Type--=--Client-.
>--+-----------------+--+-------------------------+------------->
                        |           .-,---------. |
                        |           V           | |
                        '-Nodes--=----node_name-+-'

   .-BEGINDate--=--current_date-.  .-BEGINTime--=--00:00-.
>--+----------------------------+--+---------------------+------>
   '-BEGINDate--=--date---------'  '-BEGINTime--=--time--'

   .-ENDDate--=--end_date-.  .-ENDTime--=--23:59-.
>--+----------------------+--+-------------------+-------------->
   '-ENDDate--=--date-----'  '-ENDTime--=--time--'

   .-EXceptionsonly--=--No------.
>--+----------------------------+------------------------------->
   '-EXceptionsonly--=--+-No--+-'
                        '-Yes-'

   .-Format--=--Standard-----.
>--+-------------------------+---------------------------------><
   '-Format--=--+-Standard-+-'
                '-Detailed-'
```

Admin eventek utáni keresés szintaxisa (`h 3.49.29.2`): 

```
>>-Query EVent--schedule_name--Type--=--Administrative---------->

   .-BEGINDate--=--current_date-.  .-BEGINTime--=--00:00-.
>--+----------------------------+--+---------------------+------>
   '-BEGINDate--=--date---------'  '-BEGINTime--=--time--'

   .-ENDDate--=--begin_date-.  .-ENDTime--=--23:59-.
>--+------------------------+--+-------------------+------------>
   '-ENDDate--=--date-------'  '-ENDTime--=--time--'

   .-EXceptionsonly--=--No------.
>--+----------------------------+------------------------------->
   '-EXceptionsonly--=--+-No--+-'
                        '-Yes-'

   .-Format--=--Standard-----.
>--+-------------------------+---------------------------------><
   '-Format--=--+-Standard-+-'
                '-Detailed-'
```

#### 3.3.2 Az activity log ellenőrzése

Warningok (`ANR????W`) és errorok (`ANR????E`) utáni keresgetés: 

```
Protect: TSMSRV1>q actlog search="anr????w" begind=-1

Date/Time                Message
--------------------     ----------------------------------------------------------
19/10/21   16:05:57      ANR3692W A client backup anomaly was detected for node
                          MYNODE, session number 1756453. The average number of
                          backed up bytes is 239413, the actual number of backed
                          up bytes was 401014, the average data deduplication is 0
                          percent, and the actual data deduplication was 0
                          percent. (SESSION: 1756453)
20/10/21   00:00:07      ANR1794W IBM Spectrum Protect SAN discovery is disabled
                          by options.
20/10/21   08:14:34      ANR0482W Session 1855431 for node GJAKAB (Linux x86-64)
                          terminated - idle for more than 15 minutes. (SESSION:
                          1855431)
20/10/21   08:41:51      ANR2017I Administrator GJAKAB issued command: QUERY
                          ACTLOG search=anr????w begind=-1  (SESSION: 1860683)
...

Protect: TSMSRV1>q actlog search="anr????e" begind=-1

Date/Time                Message
--------------------     ----------------------------------------------------------
19/10/21   07:49:09      ANR0944E QUERY PROCESS: No active processes found.
19/10/21   07:49:09      ANR2034E QUERY STGPOOLDIRECTORY: No match found using
                          this criteria.
19/10/21   07:49:09      ANR2034E QUERY REPLICATION: No match found using this
                          criteria.
```

Minden actlog az utolsó 10 percből: 

```
Protect: TSMSRV1>q actlog begint=-00:10

Date/Time                Message
--------------------     ----------------------------------------------------------
20/10/21   08:36:57      ANR8592I Session 1861042 connection is using protocol
                          TLSV13, cipher specification TLS_AES_256_GCM_SHA384,
                          certificate TSM Self-Signed Certificate.  (SESSION:
                          1861042)
20/10/21   08:36:57      ANR0406I Session 1861042 started for node MYNODE (DOCSTORE)
                          (SSL tsmcln1.mydomain[10.20.30.40]:36190). (SESSION:
                          1861042)
...
```

<a name="4"></a>
## 4. Az IBM SP Server telepítése

### 4.1 Előkészületek

Az ingyenesen, publikusan elérhető [public.dhe.ibm.com](https://public.dhe.ibm.com/storage/tivoli-storage-management/maintenance/) oldalról letölthető csomagok akár telepítésre is használhatók, viszont licenszt így nem kapunk. Így tehát ezek a csomagok inkább update-hez ajánlottak.  
A licenszelt telepítők az **IBM Passport Advantage** oldalról tölthetőek le. 

- Töltsd le a telepítőt valahová, ahol van min. 6 GB szabad hely, adj futtatási jogot a `.bin` fájlnak, majd futtasd (bontsd ki)!  
- Ellenőrizd, hogy van-e elegendő hely az alaptelepítéshez! A `/var` alatt legalább **3 GB**-nak, a `/tmp` alatt legalább **5 GB**-nak, az `/opt` alatt pedig legalább **9 GB**-nak kell szabadon lennie:  
```ksh
# df -hPT /var /tmp /opt
Filesystem               Type  Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-var   xfs   4.0G 1021M  3.0G  25% /var
/dev/mapper/rootvg-tmplv ext4  5.9G   61M  5.6G   2% /tmp
/dev/mapper/rootvg-optlv ext4   11G  740M  9.7G   7% /opt
```

- Készítsd el a TSM számára szükséges, előre megtervezett és alaposan átgondolt fájlrendszereket! Legalább a pool-nak és a copypool-nak ajánlott RAW LUN-on elhelyezkednie, amik közvetlenül a SAN-ról (SVC-ről) érkeznek. Az ajánlás és sok-sok teszt alapján a TSM DB-t és a log és archlog-ot tartalmazó VG-t is külön-külön LUN-on kell tartani.  
Példa a fájlrendszerekre:  
```ksh
# df -hP | grep /tsm/ | sort
/dev/mapper/dbbackupvg-tsm_dbbackuplv      118G   11G  102G  10% /tsm/dbbackup
/dev/mapper/pool1vg-tsm_mynode_poollv      542G   73M  514G   1% /tsm/mynode/pool
/dev/mapper/pool2vg-tsm_mynode_copypoollv  542G   73M  514G   1% /tsm/mynode/copypool
/dev/mapper/tsmvg-tsm_db_db1lv             4.8G  381M  4.2G   9% /tsm/db/db1
/dev/mapper/tsmvg-tsm_db_db2lv             4.8G  381M  4.2G   9% /tsm/db/db2
/dev/mapper/tsmvg-tsm_db_db3lv             4.8G  381M  4.2G   9% /tsm/db/db3
/dev/mapper/tsmvg-tsm_db_db4lv             4.8G  381M  4.2G   9% /tsm/db/db4
/dev/mapper/tsmvg-tsm_mynode_archloglv      30G  3.3G   25G  12% /tsm/mynode/archlog
/dev/mapper/tsmvg-tsm_mynode_loglv          30G  8.1G   20G  29% /tsm/mynode/log
/dev/mapper/tsmvg-tsm_mynode_tsminst1lv    2.0G   75M  1.8G   5% /tsm/mynode/tsminst1
/dev/mapper/tsmvg-tsm_installlv            9.8G  4.1G  5.2G  45% /tsm/install
```

### 4.2 A telepítés menete

Lépj be a könyvtárba, ahol a kibontott `.bin` fájl található, indítsd el a telepítőt és kövesd az utasításokat:

```ksh
# ./install.sh
GUI failed to start. For more information see /tmp/install.15212.log.
Launching console mode...
Preprocessing the input.
Loading repositories...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...


=====> IBM Installation Manager> Install

Select packages to install:
     1. [X] IBM® Installation Manager 1.9.1
     2. [X] IBM Spectrum Protect server 8.1.10.20200521_1523
     3. [X] IBM Spectrum Protect languages 8.1.10.20200521_1521
     4. [X] IBM Spectrum Protect storage agent 8.1.10.20200521_1521
     5. [X] IBM Spectrum Protect device driver 8.1.10.20200521_1522
     6. [X] IBM Spectrum Protect Operations Center 8.1.10000.20200519_1120

     O. Check for Other Versions, Fixes, and Extensions

     N. Next, C. Cancel
-----> [N]
```

Először csak az "IBM Installation Manager"-t telepítsd fel, így kapcsolj ki minden mást, majd telepítsd, végül lépj is ki a telepítőből: 

```ksh
-----> [N] 2

...

=====> IBM Installation Manager> Install
Select packages to install:
     1. [X] IBM® Installation Manager 1.9.1
     2. [ ] IBM Spectrum Protect server 8.1.10.20200521_1523
     3. [ ] IBM Spectrum Protect languages 8.1.10.20200521_1521
     4. [ ] IBM Spectrum Protect storage agent 8.1.10.20200521_1521
     5. [ ] IBM Spectrum Protect device driver 8.1.10.20200521_1522
     6. [ ] IBM Spectrum Protect Operations Center 8.1.10000.20200519_1120

     O. Check for Other Versions, Fixes, and Extensions

     N. Next, C. Cancel
-----> [N]
Validating package prerequisites...


=====> IBM Installation Manager> Install> Licenses

Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Installation Manager - License Agreement

Options:
     A. [ ] I accept the terms in the license agreement
     D. [ ] I do not accept the terms in the license agreement

     B. Back, C. Cancel
-----> [C] A


=====> IBM Installation Manager> Install> Licenses

Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Installation Manager - License Agreement

Options:
     A. [X] I accept the terms in the license agreement
     D. [ ] I do not accept the terms in the license agreement

     B. Back, N. Next, C. Cancel
-----> [N]


=====> IBM Installation Manager> Install> Licenses> Location

Installation Manager installation location:
      /opt/IBM/InstallationManager/eclipse

Options:
     L. Change Installation Manager Installation Location

     B. Back, N. Next, C. Cancel
-----> [N]


=====> IBM Installation Manager> Install> Licenses> Location> Summary

Target Location:
  Package Group Name        : IBM Installation Manager
  Installation Directory    : /opt/IBM/InstallationManager/eclipse

Packages to be installed:
     IBM® Installation Manager 1.9.1

Options:
     G. Generate an Installation Response File

     B. Back, I. Install, C. Cancel
-----> [I]
25% 50% 75% 100%
------------------|------------------|------------------|------------------|
............................................................................
=====> IBM Installation Manager> Install> Licenses> Location> Summary>
Completion
The install completed successfully.

Options:
     R. Restart Installation Manager
-----> [R]
Preprocessing the input.

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> X
```

Indulhat a Server telepítése: 

```ksh
# ./install.sh
GUI failed to start. For more information see /tmp/install.15711.log.
Launching console mode...
Preprocessing the input.

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager
-----> 1
Checking repositories...
Loading repositories...
Checking availability of packages...
Validating package prerequisites...


=====> IBM Installation Manager> Install
Select packages to install:
     1. [ ] IBM Spectrum Protect server 8.1.10.20200521_1523
     2. [ ] IBM Spectrum Protect languages 8.1.10.20200521_1521
     3. [ ] IBM Spectrum Protect storage agent 8.1.10.20200521_1521
     4. [ ] IBM Spectrum Protect device driver 8.1.10.20200521_1522
     5. [ ] IBM Spectrum Protect Operations Center 8.1.10000.20200519_1120

     C. Cancel
-----> [C] 1

=====> IBM Installation Manager> Install> Select

IBM Spectrum Protect server 8.1.10.20200521_1523

Options:
     1. Choose version 8.1.10.20200521_1523 for installation.
     2. Show all available versions of the package.

     C. Cancel
-----> [1]
Preparing and resolving the selected packages...


=====> IBM Installation Manager> Install

Select packages to install:
     1. [X] IBM Spectrum Protect server 8.1.10.20200521_1523
     2. [ ] IBM Spectrum Protect languages 8.1.10.20200521_1521
     3. [ ] IBM Spectrum Protect storage agent 8.1.10.20200521_1521
     4. [ ] IBM Spectrum Protect device driver 8.1.10.20200521_1522
     5. [ ] IBM Spectrum Protect Operations Center 8.1.10000.20200519_1120

     O. Check for Other Versions, Fixes, and Extensions

     N. Next, C. Cancel
-----> [N]
Validating package prerequisites...
.


=====> IBM Installation Manager> Install> Prerequisites

Validation results:

* [WARNING] IBM Spectrum Protect server 8.1.10.20200521_1523 contains validation warning.
     1. WARNING: The system does not meet the recommended memory requirement of 16 GB.

Enter the number of the error or warning message above to view more details.

Options:
     R. Recheck status.

     B. Back, N. Next, C. Cancel
-----> [N]


=====> IBM Installation Manager> Install> Prerequisites> Shared Directory

Shared Resources Directory:
     /opt/IBM/IBMIMShared

Options:
     M. Change Shared Resources Directory

     B. Back, N. Next, C. Cancel
-----> [N]
Finding compatible package groups...


=====> IBM Installation Manager> Install> Prerequisites> Shared Directory> Location

New package group:
     1. [X] IBM Spectrum Protect

Selected group id: "IBM Spectrum Protect"
Selected location: "/opt/tivoli/tsm"
Selected architecture: 64-bit

Options:
     M. Change Location

     B. Back, N. Next, C. Cancel
-----> [N]

=====> IBM Installation Manager> Install> Prerequisites> Shared Directory> Location> Custom panels


---- Configuration for IBM Spectrum Protect server 8.1.10.20200521_1523


Select the product that you purchased:
     1. IBM Spectrum Protect
     2. IBM Spectrum Protect Extended Edition
     3. IBM Spectrum Protect for Data Retention

-----> 2

Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Spectrum Protect Extended Edition - Software License Agreement
     2. IBM Spectrum Protect Extended Edition - Non-IBM Terms
Options:
     A. [ ] I accept the terms in the license agreements.
     D. [ ] I do not accept the terms in the license agreements.

-----> A
Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Spectrum Protect Extended Edition - Software License Agreement
     2. IBM Spectrum Protect Extended Edition - Non-IBM Terms
Options:
     A. [X] I accept the terms in the license agreements.
     D. [ ] I do not accept the terms in the license agreements.

     B. Back, N. Next, C. Cancel
-----> [N]


=====> IBM Installation Manager> Install> Prerequisites> Shared Directory> Location> Custom panels> Summary

Target Location:
  Package Group Name         :  IBM Spectrum Protect
  Installation Directory     :  /opt/tivoli/tsm
  Shared Resources Directory :  /opt/IBM/IBMIMShared

Translations:
     English

Packages to be installed:
     IBM Spectrum Protect server 8.1.10.20200521_1523

Options:
     G. Generate an Installation Response File

     B. Back, I. Install, C. Cancel
-----> [I]
25% 50% 75% 100%
------------------|------------------|------------------|------------------|
............................................................................
=====> IBM Installation Manager> Install> Prerequisites> Shared Directory> Location> Custom panels> Summary> Completion

There were problems during the installation.
WARNING: Multiple warnings occurred.

     V. View Message Details

Options:
     F. Finish
-----> [F] V

=====> IBM Installation Manager> Warning

WARNING: Multiple warnings occurred.

Details:
ANRI1015W: The system does not meet the recommended memory requirement of 16 GB.

Explanation: The system where you are installing the product should have a recommended 16 GB of memory.

User Action: Install on a system that meets the recommended memory requirement.

INFORMATION: To learn about best practices for configuring, monitoring, and operating an IBM Spectrum
Protect solution, go to IBM Knowledge Center: ... Search for IBM Spectrum Protect data protection solutions.

     O. OK, D. Hide Details
-----> [O]

=====> IBM Installation Manager> Install> Prerequisites> Shared Directory> Location> Custom panels> Summary> Completion

There were problems during the installation.
WARNING: Multiple warnings occurred.

     V. View Message Details

Options:
     F. Finish
-----> [F]

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> X
```

### 4.2 Telepítés utáni tennivalók

1. Készíts egy `tsminst1` felhasználót és állíts be mindent a `/tsm` alatt, hogy ő legyen a tulajdonosa (az alábbi természetesen csak egy példa; nem kötelezően ez a home-ja és az ID-ja): 

```ksh
# useradd -u 1001 -d /home80/tsminst1 -m tsminst1

# id tsminst1
uid=1001(tsminst1) gid=1001(tsminst1) groups=1001(tsminst1)

# su - tsminst1

$ id
uid=1001(tsminst1) gid=1001(tsminst1) groups=1001(tsminst1)

$ pwd
/home80/tsminst1

$ logout

# ls -la /tsm
total 24
drwxr-xr-x   6 root root 4096 Jun  3 15:57 .
dr-xr-xr-x. 20 root root 4096 Jun  3 15:55 ..
drwxr-xr-x   6 root root 4096 Jun  3 16:03 db
drwxr-xr-x   3 root root 4096 Jun  3 16:24 dbbackup
drwxr-xr-x   7 root root 4096 Jun  3 15:58 mynode
drwxr-xr-x   5 root root 4096 Jun 19 11:44 install

# chown -R tsminst1:tsminst1 /tsm
```

2. Inicializáld a DB2-t:

```ksh
# /opt/tivoli/tsm/db2/instance/db2icrt -a SERVER -s ese -u tsminst1 tsminst1
DBI1446I  The db2icrt command is running.


DB2 installation is being initialized.

 Total number of tasks to be performed: 4
Total estimated time for all tasks to be performed: 309 second(s)

Task #1 start
Description: Setting default global profile registry variables
Estimated time 1 second(s)
Task #1 end

Task #2 start
Description: Initializing instance list
Estimated time 5 second(s)
Task #2 end

Task #3 start
Description: Configuring DB2 instances
Estimated time 300 second(s)
Task #3 end

Task #4 start
Description: Updating global profile registry
Estimated time 3 second(s)
Task #4 end

The execution completed successfully.

For more information see the DB2 installation log at "/tmp/db2icrt.log.1071".
DBI1070I  Program db2icrt completed successfully.
```

3. Inicializáld a TSM Server instance-t:

```ksh
# su - tsminst1 -c "echo export DSMI_CONFIG=/tsm/mynode/tsminst1/tsmdbmgr.opt >>~/sqllib/userprofile"

# su - tsminst1 -c "echo export DSMI_DIR=/opt/tivoli/tsm/client/api/bin64 >>~/sqllib/userprofile"

# su - tsminst1 -c "echo export DSMI_LOG=/tsm/mynode/tsminst1 >>~/sqllib/userprofile"

# su - tsminst1 -c "db2set -i tsminst1 DB2CODEPAGE=819"

# su - tsminst1 -c "db2 update dbm cfg using dftdbpath /tsm/mynode/tsminst1"
DB20000I  The UPDATE DATABASE MANAGER CONFIGURATION command completed
successfully.

# su - tsminst1 -c "db2stop; db2start"
02/07/2020 16:38:03     0   0   SQL1032N  No start database manager command was issued.
SQL1032N  No start database manager command was issued.  SQLSTATE=57019
07/02/2020 16:38:07     0   0   SQL1063N  DB2START processing was successful.
SQL1063N  DB2START processing was successful.

# cd /tsm/mynode/tsminst1

# ls -la
total 24
drwxr-xr-x 3 tsminst1 tsminst1  4096 Jun  3 16:02 .
drwxr-xr-x 7 tsminst1 tsminst1  4096 Jun  3 15:58 ..
drwx------ 2 tsminst1 tsminst1 16384 Jun  3 16:02 lost+found

# /opt/tivoli/tsm/server/bin/dsmputil TSMDBMGR_tsminst1
Created file TSM.PWD.

# ls -la
total 28
drwxr-xr-x 3 tsminst1 tsminst1  4096 Jul  2 16:38 .
drwxr-xr-x 7 tsminst1 tsminst1  4096 Jun  3 15:58 ..
drwx------ 2 tsminst1 tsminst1 16384 Jun  3 16:02 lost+found
-rw------- 1 root     root       151 Jul  2 16:38 TSM.PWD

# chown tsminst1:tsminst1 TSM.PWD

# su - tsminst1 -c "cd /tsm/mynode/tsminst1; /opt/tivoli/tsm/server/bin/dsmserv format dbdir=/tsm/db/db1,/tsm/db/db2,/tsm/db/db3,/tsm/db/db4 activelogdir=/tsm/mynode/log archlogdir=/tsm/mynode/archlog ACTIVELOGSize=8000"
ANR7800I DSMSERV generated at 13:19:05 on May 21 2020.

IBM Spectrum Protect for Linux/x86_64
Version 8, Release 1, Level 10.000

Licensed Materials - Property of IBM

(C) Copyright IBM Corporation 1990, 2020.
All rights reserved.
U.S. Government Users Restricted Rights - Use, duplication or disclosure
restricted by GSA ADP Schedule Contract with IBM Corporation.

ANR7801I Subsystem process ID is 16262.
ANR0905W Options file /tsm/mynode/tsminst1/dsmserv.opt not found.
ANR7814I Using instance directory /tsm/mynode/tsminst1.
ANR3339I Default Label in key data base is TSM Server SelfSigned SHA Key.
ANR4726I The ICC support module has been loaded.
ANR1547W The server failed to update the DBDIAGLOGSIZE server option due to insufficient available space. Required space: 1024 megabytes;  available space: 185 megabytes. The current value:2
megabytes.
ANR0152I Database manager successfully started.

ANR2976I Offline DB backup for database TSMDB1 started.
ANR2974I Offline DB backup for database TSMDB1 completed successfully.
ANR0992I Server's database formatting complete.
ANR0369I Stopping the database manager because of a server shutdown.

# su - tsminst1
Last login: Thu Jul  2 16:39:50 CEST 2020 on pts/0

$ ls -l ~/sqllib/userprofile
-rwxr-xr-x 1 tsminst1 tsminst1 125 Jul  2 16:37 /home80/tsminst1/sqllib/userprofile

$ cat /home80/tsminst1/sqllib/userprofile
export DSMI_CONFIG=/tsm/tsminst1/tsmdbmgr.opt
export DSMI_DIR=/opt/tivoli/tsm/client/api/bin64
export DSMI_LOG=/tsm/tsminst1

$ logout

# echo "" >> /tsm/mynode/tsminst1/dsmserv.opt

# echo "VOLUMEHISTORY /tsm/mynode/tsminst1/volhist.dat" >> /tsm/mynode/tsminst1/dsmserv.opt

# echo "DEVCONFIG    /tsm/mynode/tsminst1/devcon.dat" >> /tsm/mynode/tsminst1/dsmserv.opt

# echo "SERVERNAME       TSMDBMGR_tsminst1" >> /tsm/mynode/tsminst1/tsmdbmgr.opt

# su - tsminst1 -c "db2stop; db2start"
02/07/2020 16:58:59     0   0   SQL1032N  No start database manager command was issued.
SQL1032N  No start database manager command was issued.  SQLSTATE=57019
07/02/2020 16:59:02     0   0   SQL5043N  Support for one or more communications protocols specified in the DB2COMM environment variable failed to start successfully. However, core database manager functionality started successfully.
SQL1063N  DB2START processing was successful.

# ps -ef | grep db2
root      2718     1  0 15:45 ?        00:00:00 /opt/tivoli/tsm/db2/bin/db2fmcd
root     18338     1  0 16:59 ?        00:00:00 db2wdog 0 [tsminst1]
tsminst1 18340 18338  0 16:59 ?        00:00:00 db2sysc 0
root     18346 18338  0 16:59 ?        00:00:00 db2ckpwd 0
root     18347 18338  0 16:59 ?        00:00:00 db2ckpwd 0
root     18348 18338  0 16:59 ?        00:00:00 db2ckpwd 0
tsminst1 18350 18338  0 16:59 ?        00:00:00 db2vend (PD Vendor Process - 1) 0
tsminst1 18355 18338  0 16:59 ?        00:00:00 db2acd ...

# chown -R tsminst1.tsminst1 /tsm

# su - tsminst1
Last login: Thu Jul  2 16:58:58 CEST 2020 on pts/0

$ cd /tsm/mynode/tsminst1/

$ dsmserv
ANR7800I DSMSERV generated at 13:19:05 on May 21 2020.

IBM Spectrum Protect for Linux/x86_64
Version 8, Release 1, Level 10.000

Licensed Materials - Property of IBM

(C) Copyright IBM Corporation 1990, 2020.
All rights reserved.
U.S. Government Users Restricted Rights - Use, duplication or disclosure
restricted by GSA ADP Schedule Contract with IBM Corporation.

ANR7801I Subsystem process ID is 18859.
ANR0900I Processing options file /tsm/mynode/tsminst1/dsmserv.opt.
ANR7814I Using instance directory /tsm/mynode/tsminst1.
ANR3339I Default Label in key data base is TSM Server SelfSigned SHA Key.
ANR4726I The ICC support module has been loaded.
ANR0990I Server restart-recovery in progress.
ANR1547W The server failed to update the DBDIAGLOGSIZE server option due to insufficient available space. Required space: 1024 megabytes;  available space: 179 megabytes. The current value:2
megabytes.
ANR0152I Database manager successfully started.
ANR1628I The database manager is using port 51500 for server connections.
ANR2277W The server master encryption key was not found. A new master encryption key will be created.
ANR1636W The server machine GUID changed: old value (), new value (52.b4.e0.a0.65.bc.ea.11.97.f0.00.50.56.97.3a.59).
ANR2100I Activity log process has started.
ANR4726I The NAS-NDMP support module has been loaded.
ANR1794W IBM Spectrum Protect SAN discovery is disabled by options.
ANR2200I Storage pool BACKUPPOOL defined (device class DISK).
ANR2200I Storage pool ARCHIVEPOOL defined (device class DISK).
ANR2200I Storage pool SPACEMGPOOL defined (device class DISK).
ANR2803I License manager started.
ANR9639W Unable to load Shared License File dsmreg.sl.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect for Data Retention will expire on 08/02/2020.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect Basic Edition will expire on 08/02/2020.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect Extended Edition will expire on 08/02/2020.
ANR2828I Server is licensed to support IBM Spectrum Protect for Data Retention.
ANR2828I Server is licensed to support IBM Spectrum Protect Basic Edition.
ANR2828I Server is licensed to support IBM Spectrum Protect Extended Edition.
ANR8598I Outbound SSL Services were loaded.
ANR0984I Process 1 for AUDIT LICENSE started in the BACKGROUND at 05:05:10 PM.
ANR2820I Automatic license audit started as process 1.
ANR8201E Unable to initialize TCP/IP driver - socket creation failed; error 97.
ANR8200I TCP/IP Version 4 driver ready for connection with clients on port 1500.
ANR2560I Schedule manager started.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect for Data Retention will expire on 08/02/2020.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect Basic Edition will expire on 08/02/2020.
ANR9652I An EVALUATION LICENSE for IBM Spectrum Protect Extended Edition will expire on 08/02/2020.
ANR2825I License audit process 1 completed successfully - 0 nodes audited.
ANR0985I Process 1 for AUDIT LICENSE running in the BACKGROUND completed with completion state SUCCESS at 05:05:12 PM.
ANR0984I Process 2 for EXPIRE INVENTORY (Automatic) started in the BACKGROUND at 05:05:19 PM.
ANR0811I Inventory client file expiration started as process 2.
ANR0167I Inventory file expiration process 2 processed for 0 minutes.
ANR0812I Inventory expiration process 2 is completed: processed 0 nodes, examined 0 objects, retained 0 objects, deleted 0 backup objects, 0 archive objects, 0 database backup volumes, and 0
recovery plan files. 0 objects were retried, 0 errors were detected, and 0 objects were skipped. 0 retention bitfiles were deleted from retention pools.
ANR0985I Process 2 for EXPIRE INVENTORY (Automatic) running in the BACKGROUND completed with completion state SUCCESS at 05:05:19 PM.
ANR0281I Servermon successfully started during initialization, using process 18894.
ANR0098W This system does not meet the minimum memory requirements.
ANR0993I Server initialization complete.
ANR0916I IBM Spectrum Protect distributed by International Business Machines is now ready for use.
IBM Spectrum Protect:SERVER1>
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: QUERY PROCESS
ANR0944E QUERY PROCESS: No active processes found.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW LOCKS ONLYWAITERS=YES
ANR2017I Administrator SERVER_CONSOLE issued command: INSTRUMENTATION END
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW ALLOC
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW REPLICATION
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: QUERY REPLICATION * STATUS=RUNNING FORMAT=DETAIL
ANR2679E QUERY REPLICATION: All of the nodes provided are invalid.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW DBCONN
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2034E QUERY STGPOOLDIRECTORY: No match found using this criteria.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2679E QUERY REPLICATION: All of the nodes provided are invalid.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW DEDUPTHREAD
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW BANNER
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW RESQUEUE
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TXNTABLE LOCKDETAIL=NO
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2034E QUERY MOUNT: No match found using this criteria.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2034E QUERY SESSION: No match found using this criteria.
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW SESSION FORMAT=DETAILED
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW THREADS
ANR2017I Administrator SERVER_CONSOLE issued command: SHOW TIME
q sess
ANR2017I Administrator SERVER_CONSOLE issued command: QUERY SESSION
ANR2034E QUERY SESSION: No match found using this criteria.
IBM Spectrum Protect:SERVER1>
reg admin admin <ide_jön_a_szupertitkos_jelszavad>
ANR2017I Administrator SERVER_CONSOLE issued command: REGISTER ADMIN admin ?***?
ANR2068I Administrator ADMIN registered.
IBM Spectrum Protect:SERVER1>
grant auth admin cl=system
ANR2017I Administrator SERVER_CONSOLE issued command: GRANT AUTHORITY admin cl=system
ANR2076I System privilege granted to administrator ADMIN.
IBM Spectrum Protect:SERVER1>
halt
ANR2017I Administrator SERVER_CONSOLE issued command: HALT
ANR1912I Stopping the activity log because of a server shutdown.
ANR0369I Stopping the database manager because of a server shutdown.
ANR0991I Server shutdown complete.

$ logout

# ps -ef | grep -E "dsm|db2"
root      2718     1  0 15:45 ?        00:00:00 /opt/tivoli/tsm/db2/bin/db2fmcd
root     19838 14259  0 17:06 pts/0    00:00:00 grep --color=auto -E dsm|db2
```

4. Készítsd el az init script-et: 

```ksh
# cd /opt/tivoli/tsm/server/bin

# ls -l dsmserv*
-rwxr-xr-x 1 root root 121161152 May 21 13:24 dsmserv
-rw-r--r-- 1 root root     61960 May 21 13:24 dsmserv.opt.smp
-rwxr-xr-x 1 root root      8974 May 21 13:24 dsmserv.rc

# cp -p dsmserv.rc /etc/init.d/tsminst1

# vi /etc/init.d/tsminst1
...

# diff dsmserv.rc /etc/init.d/tsminst1
84c84
< instance_dir="${instance_home}/tsminst1"
---
> instance_dir="/tsm/mynode/tsminst1"
```

5. Indítsd el a TSM Servert:

```ksh
# /etc/init.d/tsminst1 start
Starting dsmserv instance tsminst1 ... Succeeded

# ps -ef | grep dsm
root     20689     1  0 17:17 pts/0    00:00:00 su - tsminst1 -c nohup /opt/tivoli/tsm/server/bin/dsmserv  -i /tsm/mynode/tsminst1 -q
tsminst1 20772 20689  2 17:17 ?        00:00:01 /opt/tivoli/tsm/server/bin/dsmserv -i /tsm/mynode/tsminst1 -q
root     20850 20772  0 17:17 ?        00:00:00 dsmckpwd tsminst1 TSMDB1
tsminst1 20853 20772  0 17:17 ?        00:00:00 /opt/tivoli/tsm/server/bin/servermon/servermon -path=/tsm/mynode/tsminst1/ -dbalias=TSMDB1 -schema=TSMDB1 -instance=tsminst1 -optFile=/tsm/mynode/tsminst1/dsmserv.opt -installdir=/opt/tivoli/tsm/server/bin/servermon -dsmservId=20772
root     21188 14259  0 17:18 pts/0    00:00:00 grep --color=auto dsm
```

6. Telepíts és konfigurálj egy TSM klienst (az alábbi példában már volt egy 7.1-es telepítve, de ez nem számít; a folyamat ugyanaz): 

```ksh
# rpm -Uvh TIVsm-API64.x86_64.rpm TIVsm-BA.x86_64.rpm
warning: TIVsm-API64.x86_64.rpm: Header V4 RSA/SHA1 Signature, key ID be1d0ddb: NOKEY
Preparing...                          ################################# [100%]
Updating / installing...
   1:TIVsm-API64-8.1.10-0             ################################# [ 25%]
   2:TIVsm-BA-8.1.10-0                ################################# [ 50%]
Cleaning up / removing...
   3:TIVsm-BA-7.1.8-0                 ################################# [ 75%]
   4:TIVsm-API64-7.1.8-0              ################################# [100%]

# rpm -qa | grep -i tiv
TIVsm-BA-8.1.10-0.x86_64
TIVsm-API64-8.1.10-0.x86_64

# cd /opt/tivoli/tsm/client/ba/bin/

# cp -p dsm.sys.smp dsm.sys

# ln -s dsm.sys /opt/tivoli/tsm/client/api/bin64/dsm.sys

# vi dsm.sys
...
# sync
# cat dsm.sys
...
SErvername  server_a
   COMMMethod         TCPip
   TCPPort            1500
   TCPServeraddress   localhost

SERVERNAME       TSMDBMGR_tsminst1
   COMMMETHOD       TCPIP
   TCPPORT          1500
   TCPSERVERADDRESS localhost
*  PASSWORDACCESS   generate
   NODENAME         $$_TSMDBMGR_$$
   PASSWORDDIR      /tsm/mynode/tsminst1/
   ERRORLOGNAME     /tsm/mynode/tsminst1/tsmdbmgr.log

# cp -p dsm.opt.smp dsm.opt
Ensure the existing “server_a” also has the “localhost” set as TCPServeraddress.  
```

7. Lépj be a szerverbe `admin`-ként és ellenőrizd ill. állítsd be a licensz-t: 

```ksh
# dsmadmc
ANS0102W Unable to open the message repository /opt/tivoli/tsm/client/ba/bin/EN_GB/dsmclientV3.cat. The American English repository will be used instead.
IBM Spectrum Protect
Command Line Administrative Interface - Version 8, Release 1, Level 10.0
(c) Copyright by IBM Corporation and other(s) 1990, 2020. All Rights Reserved.

Enter your user id:  admin

Enter your password:

Session established with server SERVER1: Linux/x86_64
  Server Version 8, Release 1, Level 10.000
  Server date/time: 02/07/20   17:59:00  Last access: 02/07/20   17:05:51
...
```

```
Protect: SERVER1>q lic

                                              Last License Audit: 02/07/20   17:05:12
...
             Is IBM Spectrum Protect for Data Retention in use ?: No
           Is IBM Spectrum Protect for Data Retention licensed ?: Yes
                    Is IBM Spectrum Protect Basic Edition in use: Yes
                  Is IBM Spectrum Protect Basic Edition licensed: Yes
                 Is IBM Spectrum Protect Extended Edition in use: No
               Is IBM Spectrum Protect Extended Edition licensed: Yes
                                       Server License Compliance: Valid
```

Amennyiben a `Server License Compliance` nem `Valid`, úgy validálnod kell:

```
Protect: SERVER1>reg lic file=*.lic
ANR2852I Current license information:
ANR2853I New license information:
ANR2828I Server is licensed to support IBM Spectrum Protect for Data Retention.
ANR2828I Server is licensed to support IBM Spectrum Protect Basic Edition.
ANR2828I Server is licensed to support IBM Spectrum Protect Extended Edition.
```

Mindezeket követően készíthetsz is magadnak egy saját admin felhasználót és folytathatod az alapszintű konfigurálást. 

<a name="5"></a>
## 5. Az IBM SP Server konfigurálása

### 4.1 Telepítés utáni alapbeállítások

Az alábbi leírás egy dokumentumkezelő-rendszer mögötti háttértárként használja a TSM szervert, de az alapkoncepció így is megérthető, de tudni érdemes, hogy ez kissé eltér egy "normális" TSM backup környezettől. Már csak azért is, mert mindössze egyetlen node-ot definiálunk, amihez nincsenek is schedule-ök. 

#### 4.1.1 Servername beállítása

```
Protect: SERVER1>set servername TSMSRV1
ANR2586W Setting the server name to TSMSRV1 can adversely affect or stop operations that depend on communications such as for the following purposes: Operations Center administration, backup-archive
client operations, library sharing with the library manager or library clients, or other server to server functions such as virtual volumes, event logging, or enterprise configuration.

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR2094I Server name set to TSMSRV1.
ANR4865W The server name has been changed. Windows clients that use "passwordaccess generate" may be unable to authenticate with the server.

Protect: SERVER1>q status
IBM Spectrum Protect Server for Linux/x86_64 - Version 8, Release 1, Level 9.000


                                  Server Name: TSMSRV1
               Server host name or IP address:
                    Server TCP/IP port number: 1500
...
```

#### 5.1.2 Licensze ellenőrzése és beállítása

Amennyiben nem valid, próbáljuk beregisztrálni majd kérdezzük le újra: 

```
Protect: SERVER1>q lic
...
                                       Server License Compliance: FAILED

Protect: SERVER1>reg lic file=*.lic
ANR2852I Current license information:
ANR2853I New license information:
ANR2828I Server is licensed to support IBM Spectrum Protect for Data Retention.
ANR2828I Server is licensed to support IBM Spectrum Protect Basic Edition.
ANR2828I Server is licensed to support IBM Spectrum Protect Extended Edition.

Protect: SERVER1>q lic
...
                                       Server License Compliance: Valid
```

Lehetséges, hogy maga a licensz fájl hiányzik (pl. nem egy teljes csomagból való telepítés miatt). Ekkor kaphatjuk az alábbi hibaüzenetet: 

```
Protect: TETR>reg lic file=*.lic
ANR2852I Current license information:
ANR9638W License registration is not supported on this server.
ANS8001I Return code 3.
```

A fenti esetben a `dsmreg.sl` fájlt manuálisan kell idemásolni és megfelelő jogosultságokkal ellátni. Ha ez megtörtént, lehet újrapróbálni a licensz regisztrációját: 

```ksh
# pwd
/opt/tivoli/tsm/server/bin

# ls -l dsmreg.sl
-rwxr-xr-x 1 root root 25128 Oct  5 11:07 dsmreg.sl
```

### 5.2 Domain, Device Class, Storage Pool, Copy Group, Policy Set, Management Class definiálása

#### 5.2.1 Domain definiálása

```
Protect: SERVER1>q dom

Policy        Activate-     Activate-     Number of      Description
Domain        d Policy      d Defaul-     Registere-
Name          Set           t Mgmt           d Nodes
                            Class
---------     ---------     ---------     ----------     ------------------------
STANDARD      STANDARD      STANDARD               0     Installed default
                                                          policy domain.

Protect: SERVER1>copy dom standard MYDOM
ANR1503I Policy domain STANDARD copied to domain MYDOM.

Protect: SERVER1>upd dom MYDOM description='My Document Management System'
ANR1502I Policy domain MYDOM updated.
```

#### 5.2.2 Device Class definiálása

```
Device class for TSM's own DB backup: 
Protect: SERVER1>def devc dbbackup devt=file directory=/tsm/dbbackup
ANR2203I Device class DBBACKUP defined.

Protect: SERVER1>set dbrecovery dbbackup protectkeys=no
ANR2784W Specifying PROTECTKEYS=NO requires the server's encryption keys to be backed up manually.

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR2782I SET DBRECOVERY completed successfully and device class for automatic DB backup is set to DBBACKUP.

Protect: SERVER1>backup db devc=dbbackup t=f
ANR2784W Specifying PROTECTKEYS=NO requires the server's encryption keys to be backed up manually.

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR2280I Full database backup started as process 2.
ANS8003I Process number 2 started.

Protect: SERVER1>q proc

Process      Process Description      Process Status
  Number
--------     --------------------     -------------------------------------------------
       2     Database Backup          TYPE=FULL in progress. Bytes backed up: 736 MB.
                                       Current output volume(s): .

Protect: SERVER1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.
Device classes for MYDOM: 
Protect: TSMSRV1>def devc MYDEVC_FILE devt=file mountlim=500 maxcap=20G directory=/tsm/mynode/pool
ANR2203I Device class MYDEVC_FILE defined.

Protect: TSMSRV1>def devc MYDEVC_COPY devt=file maxcap=20G directory=/tsm/mynode/copypool
ANR2203I Device class MYDEVC_COPY defined.
```

#### 5.2.3 Storage Pool-ok definiálása

```
Protect: TSMSRV1>def stgp mydevc_dsk disk
ANR2200I Storage pool mydevc_dsk defined (device class DISK).

Protect: TSMSRV1>def stgp MY_FILE MYDEVC_FILE collocate=no maxscr=99 dataformat=nonblock
ANR2200I Storage pool MYDEVC_FILE defined (device class MYDEVC_FILE).

Protect: TSMSRV1>def stgp MYDEVC_COPY MYDEVC_COPY pooltype=copy maxscr=99 dataformat=nonblock
ANR2200I Storage pool MYDEVC_COPY defined (device class MYDEVC_COPY).
```

Megjegyés: a példában egy dokumentumkezelő rendszer háttértáraként üzemel a TSM, az ahhoz való IBM-es ajánlás szerint file típusú (sequential) Storage Pool-t kell készítenünk. 

Ellenőrizd ezt a beállítást utólag is: 

```
Protect: TSMSRV1>q stgp MYDEVC_FILE f=d

...
              Storage Pool Data Format: Native without Block Headers
...
```

#### 5.2.4 Copy Group konfigurálása

```
Protect: TSMSRV1>upd copyg MYDOM standard standard vere=nolimit verd=nolimit rete=nolimit reto=nolimit
ANR1532I Backup copy group STANDARD updated in policy domain MYDOM, set STANDARD, management class STANDARD.

Protect: TSMSRV1>upd copyg MYDOM standard standard standard dest=MYDEVC_FILE
ANR1532I Backup copy group STANDARD updated in policy domain MYDOM, set STANDARD, management class STANDARD.
```

#### 5.2.5 Policy Set aktiválása

```
Protect: TSMSRV1>act pol MYDOM standard

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR1514I Policy set STANDARD activated in policy domain MYDOM.
```

#### 5.2.6 Management Class konfigurálása

```
Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
MYDOM         STANDARD      STANDARD      Yes            Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.

Protect: TSMSRV1>copy mgmt MYDOM standard standard nolimit
ANR1523I Management class STANDARD copied to class NOLIMIT in policy domain MYDOM, set STANDARD.

Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
MYDOM         STANDARD      NOLIMIT       No             Installed default
                                                          management class.
MYDOM         STANDARD      STANDARD      Yes            Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.

Protect: TSMSRV1>del mgmt MYDOM standard standard

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR1521I Management class STANDARD deleted from policy domain MYDOM, set STANDARD.

Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
MYDOM         STANDARD      NOLIMIT       No             Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.

Protect: TSMSRV1>assign defmgmt MYDOM standard nolimit
ANR1538I Default management class set to NOLIMIT for policy domain MYDOM, set STANDARD.

Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
MYDOM         STANDARD      NOLIMIT       Yes            Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.

Protect: TSMSRV1>act pol MYDOM standard
ANR1550W Management class STANDARD is NOT defined in policy set STANDARD but IS defined in the ACTIVE policy set for domain MYDOM:  files bound to this management class will be REBOUND to the
default management class if/when this set is activated.

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR1550W Management class STANDARD is NOT defined in policy set STANDARD but IS defined in the ACTIVE policy set for domain MYDOM:  files bound to this management class will be REBOUND to the
default management class if/when this set is activated.
ANR1514I Policy set STANDARD activated in policy domain MYDOM.

Protect: TSMSRV1>q mgmt

Policy        Policy        Mgmt          Default        Description
Domain        Set Name      Class         Mgmt Clas-
Name                        Name          s ?
---------     ---------     ---------     ----------     ------------------------
MYDOM         ACTIVE        NOLIMIT       Yes            Installed default
                                                          management class.
MYDOM         STANDARD      NOLIMIT       Yes            Installed default
                                                          management class.
STANDARD      ACTIVE        STANDARD      Yes            Installed default
                                                          management class.
STANDARD      STANDARD      STANDARD      Yes            Installed default
                                                          management class.
```

### 5.3 Backup Script és Schedule definiálása

#### 5.3.1 Backup Script definiálása

```
Protect: TSMSRV1>q scr
ANR2034E QUERY SCRIPT: No match found using this criteria.
ANS8001I Return code 11.

Protect: TSMSRV1>def scr dbbackup 'backup db devc=dbbackup t=f w=y'
ANR1454I DEFINE SCRIPT: Command script DBBACKUP defined.

Protect: TSMSRV1>upd scr dbbackup 'backup volh'
ANR1456I UPDATE SCRIPT: Command script DBBACKUP updated.

Protect: TSMSRV1>upd scr dbbackup 'backup devconfig'
ANR1456I UPDATE SCRIPT: Command script DBBACKUP updated.

Protect: TSMSRV1>upd scr dbbackup 'delete volh t=all todate=-7'
ANR1456I UPDATE SCRIPT: Command script DBBACKUP updated.

Protect: TSMSRV1>upd scr dbbackup 'delete volhistory type=dbbackup todate=today-3'
ANR1456I UPDATE SCRIPT: Command script DBBACKUP updated.

Protect: TSMSRV1>def scr dbincr 'backup db devc=dbbackup t=incr w=y'
ANR1454I DEFINE SCRIPT: Command script DBINCR defined.

Protect: TSMSRV1>upd scr dbincr 'backup volhistory'
ANR1456I UPDATE SCRIPT: Command script DBINCR updated.

Protect: TSMSRV1>upd scr dbincr 'backup devconfig'
ANR1456I UPDATE SCRIPT: Command script DBINCR updated.

Protect: TSMSRV1>q scr dbincr f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBINCR         1          backup db devc=dbbackup t=incr w=y
               5          backup volhistory
               10         backup devconfig

Protect: TSMSRV1>def scr BACKUP_STGP 'backup stgp MYDEVC_FILE MYDEVC_COPY'
ANR1454I DEFINE SCRIPT: Command script BACKUP_STGP defined.

Protect: TSMSRV1>q scr

Name                Description                                            Managing profile
---------------     --------------------------------------------------     --------------------
BACKUP_STGP
DBBACKUP
DBINCR
```

Szintaxisa: 

```
>>-DEFine SCRipt--script_name----------------------------------->

                   .-Line--=--001-----.
>--+-command_line--+------------------+-+----------------------->
   |               '-Line --=--number-' |
   '-File--=--file_name-----------------'

>--+-----------------------------+-----------------------------><
   '-DESCription--=--description-'
```

Létrehozás után futtasd a szkriptet manuálisan! (Ebben a példában ez itt most előtérben történik, így egy darabig nem kapod vissza a promptot.)

```
Protect: TSMSRV1>run DBBACKUP
ANR0984I Process 44 for Database Backup started in the FOREGROUND at 02:08:12 PM.
ANR4559I Backup DB is in progress.
ANR2280I Full database backup started as process 44.
ANR4626I Database backup will use 1 streams for processing with the number originally requested 1.
ANR4550I Full database backup (process 44) completed. Total bytes backed up: 940,048,384.
ANR0985I Process 44 for Database Backup running in the FOREGROUND completed with completion state SUCCESS at 02:09:44 PM.
ANR2394I BACKUP DB: Server device configuration information was written to all device configuration files.
ANR2463I BACKUP VOLHISTORY: Server sequential volume history information was written to all configured history files.
ANR2394I BACKUP DEVCONFIG: Server device configuration information was written to all device configuration files.
ANR2467I DELETE VOLHISTORY: 0 sequential volume history entries were successfully deleted.
ANR2467I DELETE VOLHISTORY: 4 sequential volume history entries were successfully deleted.
ANR1462I RUN: Command script DBBACKUP completed successfully.
```

#### 5.3.2 Schedule definiálása

Példa egy full és egy incremental admin schedule definiálására: 

```
Protect: TSMSRV1>def sch DBBACKUP t=a cmd='run DBBACKUP' active=yes startt=18:00 per=1 peru=Days dur=1 duru=Hours
ANR2577I Schedule DBBACKUP defined.

Protect: TSMSRV1>def sch DBINCR t=a cmd='run DBINCR' active=yes startt=13:00 per=2 peru=Hours dur=1 duru=Hours
ANR2577I Schedule DBINCR defined.
```

Szintaxisa: 

```
Protect: TSMSRV1>h 3.17.33.2
...
Figure 6. Classic administrative schedule

>>-DEFine SCHedule--schedule_name------------------------------->

>--+-------------------------+--CMD--=--command----------------->
   '-Type--=--Administrative-'

   .-ACTIVE--=--No--.
>--+----------------+--+-----------------------------+---------->
   '-ACTIVE--=--Yes-'  '-DESCription--=--description-'

   .-PRIority--=--5------.  .-STARTDate--=--current_date-.
>--+---------------------+--+----------------------------+------>
   '-PRIority--=--number-'  '-STARTDate--=--date---------'

   .-STARTTime--=--current_time-.  .-DURation--=--1------.
>--+----------------------------+--+---------------------+------>
   '-STARTTime--=--time---------'  '-DURation--=--number-'

   .-DURUnits--=--Hours----------.  .-MAXRUNtime--=--0------.
>--+-----------------------------+--+-----------------------+--->
   '-DURUnits--=--+-Minutes----+-'  '-MAXRUNtime--=--number-'
                  +-Hours------+
                  +-Days-------+
                  '-INDefinite-'

   .-SCHEDStyle--=--Classic-.  .-PERiod--=--1------.
>--+------------------------+--+-------------------+------------>
   '-SCHEDStyle--=--Classic-'  '-PERiod--=--number-'

more...   (<ENTER> to continue, 'C' to cancel)

   .-PERUnits--=--Days--------.
>--+--------------------------+--------------------------------->
   '-PERUnits--=--+-Hours---+-'
                  +-Days----+
                  +-Weeks---+
                  +-Months--+
                  +-Years---+
                  '-Onetime-'

   .-DAYofweek--=--ANY-----------.
>--+-----------------------------+------------------------------>
   '-DAYofweek--=--+-ANY-------+-'
                   +-WEEKDay---+
                   +-WEEKEnd---+
                   +-SUnday----+
                   +-Monday----+
                   +-TUesday---+
                   +-Wednesday-+
                   +-THursday--+
                   +-Friday----+
                   '-SAturday--'

   .-EXPiration--=--Never-----.
>--+--------------------------+--------------------------------><
   '-EXPiration--=--+-Never-+-'
                    '-date--'
...
```

#### 5.3.3 Node definiálása (regisztrálása)

```
Protect: TSMSRV1>reg node mynode password dom=MYDOM
ANR2060I Node MYNODE registered in policy domain MYDOM.
```

<a name="6"></a>
## 6. Az IBM SP Server frissítése

### 6.1 Előkészületek

- A frissítési csomag letöltése: az ingyenesen, publikusan elérhető [public.dhe.ibm.com](https://public.dhe.ibm.com/storage/tivoli-storage-management/maintenance/) oldalról beszerezhető.  
- A telepítő `.bin` fájlt másoljuk egy legalább 6 GB-nyi szabad helyet tartalmazó FS alá, adjunk futtatási jogot neki, futtassuk (bontsuk ki).  
- Bizonyosodjunk meg róla, hogy van elég szabad helyünk az alábbiak alatt:  
    - `/tmp`: 4 GB  
    - `/var`: 3 GB  
    - `/opt`: 7 GB  
- Ne felejtsük majd el az egész `/opt/tivoli/tsm` mappát egy .tar arhívumba menteni (szintén legalább 3-4 GB szabad hely kell ehhez) miután leállítottuk a TSM Servert.  
- Olvassuk el azért a hivatalos IBM-es intrukciót is a telepíteni kívánt kiadásról!

### 6.1 A frissítési/upgrade folyamat

1. Jelentkezz be.

2. Ellenőrizd, hogy jelenleg nem-e fut schedule-ált job: 

```
Protect: TSMSRV1>q sched
ANR2034E QUERY SCHEDULE: No match found using this criteria.
ANS8001I Return code 11.
```

3. Kérdezd le a full DB backup parancsát: 

```
Protect: TSMSRV1>q sched * t=a

*     Schedule Name        Start Date/Time          Duration     Period     Day
-     ----------------     --------------------     --------     ------     ---
      BACKUP_STGP          05/06/20   11:30:00         50 M        1 H      Any
      DBBACKUP             19/05/20   18:00:00          1 H        1 D      Any
      DBINCR               05/06/20   13:00:00          1 H        2 H      Any

Protect: TSMSRV1>q sched DBBACKUP t=a f=d

                 Schedule Name: DBBACKUP
                   Description:
                       Command: run dbbackup
                      Priority: 5
               Start Date/Time: 19/05/20   18:00:00
                      Duration: 1 Hour(s)
    Maximum Run Time (Minutes): 0
                Schedule Style: Classic
                        Period: 1 Day(s)
                   Day of Week: Any
                         Month:
                  Day of Month:
                 Week of Month:
                    Expiration:
                       Active?: Yes
Last Update by (administrator): ADMIN
         Last Update Date/Time: 19/05/20   15:58:15
              Managing profile:


Protect: TSMSRV1>q scr dbbackup f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBBACKUP       1          backup db devc=dbbackup t=f w=y
               5          backup volh
               10         backup devconfig
               15         delete volh t=all todate=-7
               20         delete volhistory type=dbbackup todate=today-3

```

4. A fenti lekérdezés kimenetét felhasználva, indítsd el a DB backupot és ellenőrizd a státuszát: 

```
Protect: TSMSRV1>backup db devc=DBBACKUP type=full
ANR2784W Specifying PROTECTKEYS=NO requires the server's encryption keys to be backed up manually.

Do you wish to proceed? (Yes (Y)/No (N)) y
ANR2280I Full database backup started as process 103.
ANS8003I Process number 103 started.

Protect: TSMSRV1>q dbs

Location                           Total Space of       Used Space on       Free Space(MB)
                                      File System         File System
                                              (MB)                (MB)
------------------------------     ---------------     ---------------     ---------------
/tsm/mynode/db/db1                         20,480.00            3,348.37           17,131.63
/tsm/mynode/db/db2                         20,480.00            3,348.37           17,131.63
/tsm/mynode/db/db3                         20,480.00            3,348.37           17,131.63
/tsm/mynode/db/db4                         20,480.00            3,348.37           17,131.63

Protect: TSMSRV1>q proc

Process      Process Description      Process Status
  Number
--------     --------------------     -------------------------------------------------
     103     Database Backup          TYPE=FULL in progress. Bytes backed up: 2,256
                                       MB. Current output volume(s):
                                       /tsm/mynode/pool/dbbackup/87554389.DBV.

Protect: TSMSRV1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.
```

5. Készíts mentést a volhistory és devconfig objektumokról is: 

```
Protect: TSMSRV1>q scr dbb f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBB            1
               5          backup db devc=DBBACKUP type=full  w=y
               10         delete volhistory type=dbbackup todate=today-2
               15         backup volhistory
               20         backup volhistory filename=/tsm/mynode/dbincr/config/volhisto-
                           ry.out
               25         backup devconfig
               30         backup devconfig filename=/tsm/mynode/dbincr/config/devconfig-
                           .out

Protect: TSMSRV1>backup volhistory

Do you wish to proceed? (Yes (Y)/No (N)) y
ANR2463I BACKUP VOLHISTORY: Server sequential volume history information was written to all configured history files.

Protect: TSMSRV1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.

Protect: TSMSRV1>backup devconfig

Do you wish to proceed? (Yes (Y)/No (N)) y
ANR2394I BACKUP DEVCONFIG: Server device configuration information was written to all device configuration files.

Protect: TSMSRV1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.
```

6. Ellenőrizd, hogy ezek hová lettek mentve: 

```
Protect: TSMSRV1>q opt

Server Option         Option Setting           Server Option         Option Setting
-----------------     --------------------     -----------------     --------------------
...
VolumeHistory         /tsm/mynode/tsminst1/-     Devconfig             /tsm/mynode/tsminst1/-
                       volhist.dat                                    devcon.dat
...
```

7. Umountolj minden volue-ot, ha esetleg lenne ilyen: 

```
Protect: TSMSRV1>q mount
ANR8329I 3592 volume ABC152 is mounted R/W in drive RMT_F1C2R3 (/dev/rmt_F1C2R3_0), status: IDLE.
ANR8334I         1 matches found.

Protect: TSMSRV1>dism vol ABC152
ANR8499I Command accepted.

Protect: TSMSRV1>q mount
ANR2034E QUERY MOUNT: No match found using this criteria.
ANS8001I Return code 11.
```

8. Ellenőrizd a processzeket és session-öket, hogy már nincs semmi, ami használná a szervert: 

```
Protect: TSMSRV1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.

Protect: TSMSRV1>q sess

 Sess      Comm.      Sess        Wait       Bytes       Bytes      Sess        Platform     Client Name
Number     Method     State        Time        Sent       Recvd     Type
------     ------     ------     ------     -------     -------     -------     --------     ------------------
 1,501     Tcp/Ip     Run          0 S        5.5 K       1.3 K     Admin       AIX          GJAKAB
 1,519     Tcp/Ip     Run          0 S          246         324     Server      AIX          ABCLIBMGR

Protect: TSMSRV1>q sess

 Sess      Comm.      Sess        Wait       Bytes       Bytes      Sess        Platform     Client Name
Number     Method     State        Time        Sent       Recvd     Type
------     ------     ------     ------     -------     -------     -------     --------     ------------------
 1,501     Tcp/Ip     Run          0 S       10.0 K       1.5 K     Admin       AIX          GJAKAB
```

9. Állítsd le az IBM SP szervert (ez közvetlen belépéskor vissza kell dobjon a shell-be): 

```
Protect: TSMSRV1>halt
ANR2234W This command will halt the server; if the command is issued from a remote client, it may not be possible to restart the server from the remote location.

Do you wish to proceed? (Yes (Y)/No (N)) y

ANS8002I Highest return code was 11.
```

10. Mentsd le a jelenlegi telepítést: 

```ksh
# cd /opt/tivoli

# tar -cf tsm-working.tar tsm

# mv tsm-working.tar <folder_with_at_least_3GB_of_free_space>
```

11. Indítsd el, majd vidd végig az update/upgrade folyamatot: 

Go to the directory, where you extracted the installer to, and issue ./install.sh. 
It is possible that the Installation Manager does not start at once but you should update the Installation Manager itself first, so e.g. the installation of IBM Installation Manager 1.8.5 welcomes you. In that case, you should install it first, accept its license agreement and so, then you can reload the Installation Manager when finished. 
If the version of Installation Manager is fine, the procedure is the same (or very similar) as written below: 

```ksh
# ./install.sh
GUI failed to start. For more information see /tmp/install.6815870.log.
Launching console mode...
Preprocessing the input.

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> 2
Checking repositories...
Loading repositories...


=====> IBM Installation Manager> Update

Select a package group to update:
     1. [X] IBM Tivoli Storage Manager

Details of package group IBM Tivoli Storage Manager:
  Package Group Name         :  IBM Tivoli Storage Manager
  Shared Resources Directory :  /opt/IBM/IBMIMShared
  Installation Directory     :  /opt/tivoli/tsm
  Translations               :  English
  Architecture               :  64-bit

Other Options:
     U. Update All
     A. Unselect All

     N. Next,      C. Cancel
-----> [N]
Preparing installed packages in the selected package groups...
Validating package group locations...
Searching updates...
Preparing and resolving the selected packages...
Preparing and resolving the selected packages...
Validating the selected updates...


=====> IBM Installation Manager> Update> Packages

Package group:  IBM Tivoli Storage Manager

Update packages:
     1-. [X] IBM Tivoli Storage Manager server 7.1.10.20200325_1212
       2. [X] Version 7.1.10.20200414_1750
     3. [ ] IBM Tivoli Storage Manager languages 7.1.10.20200325_1211
     4. [ ] IBM Tivoli Storage Manager license 7.1.1.20141123_0818
     5-. [X] IBM Tivoli Storage Manager device driver  7.1.10.20200325_1211
       6. [X] Version 7.1.10.20200414_1747

Other Options:
     A. Show All
     R. Select Recommended

     B. Back,      N. Next,      C. Cancel
-----> [N]
Validating package prerequisites...
.


=====> IBM Installation Manager> Update> Packages> Prerequisites

Validation results:

* [WARNING] IBM Tivoli Storage Manager server 7.1.10.20200414_1750 contains validation warning.
     1. WARNING: The system does not meet the recommended memory requirement of 16 GB.

Enter the number of the error or warning message above to view more details.

Options:
     R. Recheck status.

     B. Back,      N. Next,      C. Cancel
-----> [N]


=====> IBM Installation Manager> Update> Packages> Prerequisites> Summary

Target Location:
  Shared Resources Directory :  /opt/IBM/IBMIMShared
Update packages:
     1-. IBM Tivoli Storage Manager (/opt/tivoli/tsm)
       2. IBM Tivoli Storage Manager server 7.1.10.20200325_1212
       3. IBM Tivoli Storage Manager device driver  7.1.10.20200325_1211

Options:
     G. Generate an Update Response File

     B. Back,      U. Update,      C. Cancel
-----> [U]
                 25%                50%                75%                100%
------------------|------------------|------------------|------------------|
............................................................................

=====> IBM Installation Manager> Update> Packages> Prerequisites> Summary>
  Completion

There were problems during the update.
WARNING: Multiple warnings occurred.

     V. View Message Details

Options:
     F. Finish
-----> [F] V

=====> IBM Installation Manager> Warning

WARNING: Multiple warnings occurred.

Details:
ANRI1015W: The system does not meet the recommended memory requirement of 16 GB.

Explanation: The system where you are installing the product should have a recommended 16 GB of memory.

User Action: Install on a system that meets the recommended memory requirement.

INFORMATION: To learn about best practices for configuring, monitoring, and operating a Tivoli Storage Manager solution, go to IBM Knowledge Center: ... Search for Tivoli Storage Manager data protection solutions.

     O. OK,      D. Hide Details
-----> [O]

=====> IBM Installation Manager> Update> Packages> Prerequisites> Summary>
  Completion

There were problems during the update.
WARNING: Multiple warnings occurred.

     V. View Message Details

Options:
     F. Finish
-----> [F]

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> X
```

12. Amennyiben a /opt/tivoli/tsm/server/bin alatt voltak, javítsd ki az indító/leállító scripteket: 

```ksh
# cd /opt/tivoli/tsm/server/bin

# vi startserver
...
INST_USER=tsminst1
...
INST_DIR=/tsm/mynode/tsminst1
...

# vi stopserver
...
INSTANCE_DIR=/tsm/mynode/tsminst1
...
```

(Mégjobb, ha ezeknek a scripteknek a biztonsági mentéséről már előzetesen gondoskodtál.)

13. Indítsd el az IBM SP szervert: 

```ksh
# ./startserver
Verifying that offline storage devices are available...
04/22/2020 13:33:07     0   0   SQL1063N  DB2START processing was successful.
SQL1063N  DB2START processing was successful.
SQL1005N  The database alias "TSMDB1" already exists in either the local
database directory or system database directory.
SQL1005N  The database alias "TSMDB1" already exists in either the local
database directory or system database directory.
04/22/2020 13:33:10     0   0   SQL1064N  DB2STOP processing was successful.
SQL1064N  DB2STOP processing was successful.
Starting TSM with command: /opt/tivoli/tsm/server/bin/rc.dsmserv -u tsminst1 -i /tsm/mynode/tsminst1 -q
root@gabi:/opt/tivoli/tsm/server/bin                 :ido:1587555190 epoch mp
# stty: tcgetattr: Not a typewriter
```

14. Ellenőrizd, hogy a fő `dsmserv` processz fut-e, várj legalább fél percet, és próbálj bejelentkezni: 

```ksh
# ps -ef | grep dsm
tsminst1  8257784        1   7 13:33:10  pts/2  0:00 /opt/tivoli/tsm/server/bin/dsmserv -i /tsm/mynode/tsminst1 -q
    root  9568430 12320854   1 13:33:21  pts/2  0:00 grep dsm

# dsmadmc
...
```

15. Ellenőrizd a szerver státuszát, verzióját: 

```
Protect: TSMSRV1>q status
Storage Management Server for AIX - Version 7, Release 1, Level 10.100


                                  Server Name: PARmynodeT
               Server host name or IP address: 10.161.167.41
                    Server TCP/IP port number: 1500
                                  Crossdefine: On
                          Server Password Set: Yes
                Server Installation Date/Time: 04/19/16   13:33:53
                     Server Restart Date/Time: 04/22/20   13:33:28
                               Authentication: On
                   Password Expiration Period: 90 Day(s)
                Invalid Sign-on Attempt Limit: 0
                      Minimum Password Length: 0
                                 Registration: Closed
                               Subfile Backup: No
                                 Availability: Enabled
...
```

16. Ellenőrizd az action log-okat, hogy nincs-e valami igazán félelmetes köztük (mint pl. az alábbi ideiglenes kapcsolódási hiba): 

```
Protect: TSMSRV1>q act

Date/Time                Message
--------------------     ----------------------------------------------------------
...
04/22/20   13:33:45      ANR0454E Session rejected by server PARLIBMGR, reason:
                          201 - Communication Failure.
...
```

17. Ellenőrizd a kapcsolódó szervereket: 

```
Protect: TSMSRV1>q server

Server       Comm.      High-level        Low-leve-      Days      Server         Virtual        Allow
Name         Method     Address           l Address     Since      Password       Volume         Replacement
                                                         Last      Set            Password
                                                        Access                    Set
--------     ------     -------------     ---------     ------     ----------     ----------     -----------
MYCLN1       TCPIP      10.20.30.40       1500              <1     Yes            No             No
ABCLIBMGR    TCPIP      10.20.30.50       1500              <1     Yes            No             No
```

18. Indíts megint egy DB backupot és ellenőrizd annak előrehaladását: 

```
Protect: TSMSRV1>q proc

Process      Process Description          Job Id     Process Status                                         Parent
  Number                                                                                                    Process
--------     --------------------     ----------     -------------------------------------------------     --------
   1,137     Database Backup                         TYPE=FULL in progress. Bytes backed up: 368 MB.
                                                      Current output volume(s):
                                                      /tsm/dbbackup/34730281.DBV.

Protect: TSMSRV1>q proc
ANR0944E QUERY PROCESS: No active processes found.
ANS8001I Return code 11.

Protect: TSMSRV1>quit
```

19. Amennyiben voltak ilyenek, ellenőrizd a drive-okat és path-okat is:

```
Protect: TSMSRV1>q drive
...
Protect: TSMSRV1>q path
...
```

### 6.3 Kliens update

Soha ne telepíts túl új kliensverziót, hanem ellenőrizd előtte a kompatibilitást! Akár olyan eset is történhet, hogy az új klienssel érkező `dsmadmc` nem tud kapcsolódni a szerverhez a túl új GSKit8 csomag miatt. 


<a name="7"></a>
## 7. Kliens backup konfigurálása

### 7.1 Egy példa image backup beállítása

```
Protect: TSMSRV1>copy sched ABC_WINSERV IMAGESNAP_VALAMI ABC_WINSERV IMAGESNAP_ABCDE1
ANR2503I Schedule IMAGESNAP_VALAMI in domain ABC_WINSERV copied to schedule IMAGESNAP_ABCDE1 in domain ABC_WINSERV.

Protect: TSMSRV1>q sched ABC_WINSERV IMAGESNAP_ABCDE1 f=d

            Policy Domain Name: ABC_WINSERV
                 Schedule Name: IMAGESNAP_ABCDE1
                   Description:
                        Action: ImageBackup
                     Subaction:
                       Options:
                       Objects: d: c:
                      Priority: 5
               Start Date/Time: 03/09/09   21:05:00
                      Duration: 5 Hour(s)
    Maximum Run Time (Minutes): 0
                Schedule Style: Enhanced
                        Period:
                   Day of Week: Any
                         Month: Any
                  Day of Month: Any
                 Week of Month: Any
                    Expiration: 09/20/13   23:59:59
Last Update by (administrator): GJAKAB
         Last Update Date/Time: 06/17/20   12:51:17
              Managing profile:

Protect: TSMSRV1>reg node ABCDE1 ABCDE1 do=ABC_WINSERV
ANR2060I Node ABCDE1 registered in policy domain ABC_WINSERV.

Protect: TSMSRV1>def assoc ABC_WINSERV IMAGESNAP_ABCDE1 ABCDE1
ANR2510I Node ABCDE1 associated with schedule IMAGESNAP_ABCDE1 in policy domain ABC_WINSERV.

Protect: TSMSRV1>q assoc ABC_WINSERV IMAGESNAP_ABCDE1

Policy Domain Name                 Schedule Name                      Associated Nodes
------------------------------     ------------------------------     ----------------------------------------------------------------
ABC_WINSERV                        IMAGESNAP_ABCDE1                   ABCDE1

Protect: TSMSRV1>q dom ABC_WINSERV f=d
...
```

### 7.2 Egy node törlése

```
Protect: TSMSRV1>q sched nodes=HUNFS2

Domain           *     Schedule Name        Action     Start Date/Time          Duration     Period     Day
------------     -     ----------------     ------     --------------------     --------     ------     ---
ABC_WINSERV            IMAGESNAP_HUNFS2     ImgBck     01/05/18   04:00:00          5 H                 (*)
ABC_WINSERV            INCR_DAILY_2000-     Inc Bk     11/05/02   20:00:00          4 H        1 D      Any
                        _NEW
ANR2662I (*) "Query schedule format=standard" displays an asterisk in the day of week column for enhanced schedules.  The period column is blank.  Issue "query schedule format=detailed" to display complete
information about an enhanced schedule.

Protect: TSMSRV1>q assoc ABC_WINSERV IMAGESNAP_HUNFS2

Policy Domain Name                 Schedule Name                      Associated Nodes
------------------------------     ------------------------------     ----------------------------------------------------------------
ABC_WINSERV                        IMAGESNAP_HUNFS2                   HUNFS2

Protect: TSMSRV1>q assoc ABC_WINSERV INCR_DAILY_2000_NEW

Policy Domain Name                 Schedule Name                      Associated Nodes
------------------------------     ------------------------------     ----------------------------------------------------------------
ABC_WINSERV                        INCR_DAILY_2000_NEW                AS0082 HUNFS2

Protect: TSMSRV1>del assoc ABC_WINSERV INCR_DAILY_2000_NEW HUNFS2

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR2511I Node HUNFS2 disassociated from schedule INCR_DAILY_2000_NEW in policy domain ABC_WINSERV.

Protect: TSMSRV1>q assoc ABC_WINSERV INCR_DAILY_2000_NEW

Policy Domain Name                 Schedule Name                      Associated Nodes
------------------------------     ------------------------------     ----------------------------------------------------------------
ABC_WINSERV                        INCR_DAILY_2000_NEW                AS0082
```

<a name="8"></a>
## 8. IBM SP Server DB restore

Az egyes objektumnevek természetesen az alábbi példától eltérhetnek. Itt most szalagról történő visszatörtés történik, a Device Class neve pedig nem az előzőekben példaként felhozott `DBBACKUP`, hanem `DBB`. Ezekkel mindenek előtt tisztában kell lenni. 

1. Ellenőrizd az utolsó full backup pontos dátumát és idejét: 

```ksh
# su - tsminst1
> cd /tsm/mynode/tsminst1
> dsmserv
...
Protect: TSMSRV1> q volh t=dbb
                                 Date/Time: 06/19/2020 19:31:37
                               Volume Type: BACKUPFULL
                             Backup Series: 1,435
                          Backup Operation: 0
                                Volume Seq: 100,001
                              Device Class: TS4500_ABC
                               Volume Name: ABC157
...
```

A dátum és idő tehát a fenti példa alapján: "06/19/2020 19:31:37"

A szerver leállítása: 

```ksh
...
Protect: TSMSRV1> q proc

Protect: TSMSRV1> q sess

Protect: TSMSRV1> halt
...
```

2. Csinálj egy preview-t: 

```ksh
> /opt/tivoli/tsm/server/bin/dsmserv RESTORE DB todate=06/19/2020 totime=19:31:37 source=DBB preview=yes
```

3. Távolítsd el a DB-t: 

```ksh
> /opt/tivoli/tsm/server/bin/dsmserv REMOVEDB TSMDB1
```

4. Restore-old a DB-t (ugyanaz a parancs, mint az előbb, csak a `preview=yes` nélkül): 

```ksh
> /opt/tivoli/tsm/server/bin/dsmserv RESTORE DB todate=06/19/2020 totime=19:31:37 source=DBB
...
ANR4912I Database  Restore DB in progress and total bytes transferred
130,392,522,752.
...
ANR3096I The volumes used to perform this restore operation were successfully
recorded in the server volume history.
ANR8468I 3592 volume ABC157 dismounted from drive RMT_F1C2R3 (/dev/rmt_F1C2R3_-
0) in library ABCLIB1.
ANR0409I Session 38 ended for server TSMSRV1 (AIX).
```

Szalagos visszatöltés esetén ez a folyamat akár 1-1,5 óráig is eltarthat. 

5. Indítsd el a szervert, pl.:

```ksh
# /opt/tivoli/tsm/server/bin/startserver
```

<a name="9"></a>
## 9. Egyéb karbantartási feladatok

### 9.1 Drive-ok és/vagy path-ok ki-/bekapcsolása

```
Protect: TSMSRV1>q drive

Library Name     Drive Name       Device Type     On-Line
------------     ------------     -----------     -------------------
PTLABC1          RMT_F1C4R1_0     3592            Yes
PTLABC1          RMT_F1C4R2_0     3592            Yes
PTLABC1          RMT_F1C4R3_0     3592            Yes
PTLABC1          RMT_F1C4R4_0     3592            Yes

Protect: TSMSRV1>q path

Source Name     Source Type     Destinatio-     Destinatio-     On-Line
                                n Name          n Type
-----------     -----------     -----------     -----------     ----------
TSMSRV1           SERVER          PTLABC1         LIBRARY         Yes
TSMSRV1           SERVER          RMT_F1C4R1-     DRIVE           Yes
                                 _0
TSMSRV1           SERVER          RMT_F1C4R2-     DRIVE           Yes
                                 _0
TSMSRV1           SERVER          RMT_F1C4R3-     DRIVE           Yes
                                 _0
TSMSRV1           SERVER          RMT_F1C4R4-     DRIVE           Yes

Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R1_0 online=no
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R2_0 online=no
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R3_0 online=no
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R4_0 online=no
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R1_0 srctype=server desttype=drive library=PTLABC1 online=no
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R2_0 srctype=server desttype=drive library=PTLABC1 online=no
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R3_0 srctype=server desttype=drive library=PTLABC1 online=no
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R4_0 srctype=server desttype=drive library=PTLABC1 online=no
Protect: TSMSRV1>upd path TSMSRV1 PTLABC1 srctype=server desttype=LIBRARY online=no

Protect: TSMSRV1>q path

Source Name     Source Type     Destinatio-     Destinatio-     On-Line
                                n Name          n Type
-----------     -----------     -----------     -----------     ----------
TSMSRV1           SERVER          PTLABC1         LIBRARY         No
TSMSRV1           SERVER          RMT_F1C4R1-     DRIVE           No
                                 _0
TSMSRV1           SERVER          RMT_F1C4R2-     DRIVE           No
                                 _0
TSMSRV1           SERVER          RMT_F1C4R3-     DRIVE           No
                                 _0
TSMSRV1           SERVER          RMT_F1C4R4-     DRIVE           No
                                 _0

Protect: TSMSRV1>q drive

Library Name     Drive Name       Device Type     On-Line
------------     ------------     -----------     -------------------
PTLABC1          RMT_F1C4R1_0     3592            No
PTLABC1          RMT_F1C4R2_0     3592            No
PTLABC1          RMT_F1C4R3_0     3592            No
PTLABC1          RMT_F1C4R4_0     3592            No
Protect: TSMSRV1>upd path TSMSRV1 PTLABC1 srctype=server desttype=LIBRARY online=yes
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R1_0 srctype=server desttype=drive library=PTLABC1 online=yes
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R2_0 srctype=server desttype=drive library=PTLABC1 online=yes
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R3_0 srctype=server desttype=drive library=PTLABC1 online=yes
Protect: TSMSRV1>upd path TSMSRV1 RMT_F1C4R4_0 srctype=server desttype=drive library=PTLABC1 online=yes
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R1_0 online=yes
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R2_0 online=yes
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R3_0 online=yes
Protect: TSMSRV1>upd drive PTLABC1 RMT_F1C4R4_0 online=yes
```

### 9.2 Egy file storage pool kibővítése egy újabb FS path-jával: 

Először is, készítsd el az FS-t, mount-old fel, állítsd be a jogosultságokat: 

```ksh
# lsvgfs filepoolvg | xargs df -m
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/filep14lv 2457600.00      0.00  100%       41    65% /tsm/mynode/file/14
/dev/filep01lv 2457600.00      0.00  100%       28    88% /tsm/mynode/file/01
/dev/filep02lv 2457600.00      0.00  100%       29    91% /tsm/mynode/file/02
/dev/filep03lv 2457600.00      0.00  100%       33    52% /tsm/mynode/file/03
/dev/filep04lv 2457600.00      0.00  100%       28    88% /tsm/mynode/file/04
/dev/filep05lv 2457600.00      0.00  100%       34    54% /tsm/mynode/file/05
/dev/filep06lv 2457600.00      0.00  100%       34    54% /tsm/mynode/file/06
/dev/filep07lv 2457600.00      0.00  100%       31    97% /tsm/mynode/file/07
/dev/filep08lv 2457600.00      0.00  100%       36    57% /tsm/mynode/file/08
/dev/filep09lv 2457600.00      0.00  100%       29    91% /tsm/mynode/file/09
/dev/filep10lv 2457600.00      0.00  100%       33    52% /tsm/mynode/file/10
/dev/filep11lv 2457600.00      0.00  100%       37    58% /tsm/mynode/file/11
/dev/filep12lv 2457600.00      0.00  100%       29    91% /tsm/mynode/file/12
/dev/filep13lv 2457600.00      0.00  100%       42    66% /tsm/mynode/file/13
/dev/filep15lv 2457600.00      0.00  100%       46    72% /tsm/mynode/file/15
/dev/filep16lv 2457600.00      0.00  100%       47    74% /tsm/mynode/file/16
/dev/filep17lv 2457600.00      0.00  100%       56    88% /tsm/mynode/file/17
/dev/filep18lv 2457600.00 1000978.60   60%       37     1% /tsm/mynode/file/18

# lsfs -q /tsm/mynode/file/18
Name            Nodename   Mount Pt               VFS   Size    Options    Auto Accounting
/dev/filep18lv  --         /tsm/mynode/file/18      jfs2  5033164800 rw         yes  no
  (lv size: 5033164800, fs size: 5033164800, block size: 4096, sparse files: yes, inline log: yes, inline log size: 2047, EAformat: v1, Quota: no, DMAPI: no, VIX: yes, EFS: no, ISNAPSHOT: no, MAXEXT: 0, MountGuard: no)

# lsvg filepoolvg
VOLUME GROUP:       filepoolvg               VG IDENTIFIER:  00c59a1700004c0000000167790a4c1a
VG STATE:           active                   PP SIZE:        2048 megabyte(s)
VG PERMISSION:      read/write               TOTAL PPs:      21952 (44957696 megabytes)
MAX LVs:            256                      FREE PPs:       352 (720896 megabytes)
LVs:                18                       USED PPs:       21600 (44236800 megabytes)
...

# mklv -t jfs2 -y filep19lv filepoolvg 352
filep19lv

# crfs -v jfs2 -m /tsm/mynode/file/19 -d filep19lv -A yes -a logname=INLINE
File system created successfully.
737453896 kilobytes total disk space.
New File System size is 1476395008

# mount /tsm/mynode/file/19

# ls -ld /tsm/mynode/file/17 /tsm/mynode/file/18 /tsm/mynode/file/19
drwxr-xr-x    3 tsminst1 tsmsvrs        4096 Jul 14 09:44 /tsm/mynode/file/17
drwxr-xr-x    3 tsminst1 tsmsvrs        4096 Sep 17 14:42 /tsm/mynode/file/18
drwxr-xr-x    3 root     system          256 Sep 21 15:46 /tsm/mynode/file/19

# chown tsminst1:tsmsvrs /tsm/mynode/file/19
```

Állítsd be ezt az új FS-t is a device class számára: 

```
Protect: TSMSRV1>q stgp

Storage         Device         Storage       Estimated       Pct       Pct      Hig-     Lo-     Next Stora-
Pool Name       Class Name     Type            Capacity      Util      Migr     h M-      w      ge Pool
                                                                                 ig      Mi-
                                                                                 Pct      g
                                                                                         Pct
-----------     ----------     ---------     ----------     -----     -----     ----     ---     -----------
MYDEVC_FILE     MYDEVC_FILE    DEVCLASS        43,157 G      97.7      97.7       90      70
MYDEVC_BKP      TS4500_FRA     DEVCLASS      5,814,574        0.7

Protect: TSMSRV1>q devc MYDEVC_FILE f=d
...
Directory: /tsm/mynode/file/01, <felsorolás> ,/tsm/mynode/file/18
...
Protect: TSMSRV1>upd devc MYDEVC_FILE dir=/tsm/mynode/file/01, <felsorolás> ,/tsm/mynode/file/18,/tsm/mynode/file/19
ANR2205I Device class MYDEVC_FILE updated.

Protect: TSMSRV1>q devc MYDEVC_FILE f=d
...
Directory: /tsm/mynode/file/01, <felsorolás> ,/tsm/mynode/file/19
...
Protect: TSMSRV1>q stgp MYDEVC_FILE

Storage         Device         Storage       Estimated       Pct       Pct      Hig-     Lo-     Next Stora-
Pool Name       Class Name     Type            Capacity      Util      Migr     h M-      w      ge Pool
                                                                                 ig      Mi-
                                                                                 Pct      g
                                                                                         Pct
-----------     ----------     ---------     ----------     -----     -----     ----     ---     -----------
MYDEVC_FILE        MYDEVC_FILE       DEVCLASS        43,860 G      96.2      96.2       90      70
```

### 9.3 Library Media Charger (SMC) újrakonfigurálása AIX-on

Amennyiben az `smc*` eszközök valamelyike nincs megfelelően konfigurálva, vagy elment `Defined` állásba, akkor ilyesmi errort láthatsz: 

```
10/06/20   16:42:04      ANR8840E Unable to open device /dev/smc0 with error
                          number 6 and PVRRC 2843. (PROCESS: 478)
```

Here is the way to get the devices working again: 

```ksh
# lsdev -Cc tape
rmt_F1C2R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C2R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
smc0         Defined   13-T1-01     IBM 3584 Library Medium Changer (FCP)
smc1         Defined   13-T1-01     IBM 3584 Library Medium Changer (FCP)

# rmdev -dl smc0
smc0 deleted

# rmdev -dl smc1
smc1 deleted

# cfgmgr

# lsdev -Cc tape
rmt_F1C2R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C2R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
smc0         Available 13-T1-01     IBM 3584 Library Medium Changer (FCP)
smc1         Available 13-T1-01     IBM 3584 Library Medium Changer (FCP)

# /usr/lpp/Atape/instAtape -a
Setting alternate pathing support on smc0...
smc0 changed
Setting alternate pathing support on smc1...
smc1 changed

# lsdev -Cc tape
rmt_F1C2R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C2R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_0 Available 13-T1-01-PRI IBM 3592 Tape Drive (FCP)
rmt_F1C4R4_1 Available 13-T1-01-ALT IBM 3592 Tape Drive (FCP)
smc0         Available 13-T1-01-PRI IBM 3584 Library Medium Changer (FCP)
smc1         Available 13-T1-01-ALT IBM 3584 Library Medium Changer (FCP)
```

Mindezek után ki is kell próbálnod a backup-ot szalagra: 

```
Protect: ABCLIBMGR01>q devc

Device        Device         Storag-     Device        Format     Est/Max      Mount
Class         Access         e Pool      Type                     Capacit-      Limit
Name          Strategy         Count                                y (MB)
---------     ----------     -------     ---------     ------     --------     ------
DB_BACKUP     Sequential           0     3592          DRIVE                   DRIVES
DISK          Random               3
TS4500_T-     Sequential           2     3592          DRIVE                   DRIVES
 APE

Protect: ABCLIBMGR01>backup db devc=TS4500_TAPE t=f
ANR4976W The device class TS4500_TAPE is not the same as device class DB_BACKUP defined for the serverbackup node $$_TSMDBMGR_$$.
ANR2784W Specifying PROTECTKEYS=NO requires the server's encryption keys to be backed up manually.

Do you wish to proceed? (Yes (Y)/No (N)) Y
ANR4976W The device class TS4500_TAPE is not the same as device class DB_BACKUP defined for the serverbackup node $$_TSMDBMGR_$$.
ANR2280I Full database backup started as process 480.
ANS8003I Process number 480 started.

Protect: ABCLIBMGR01>q mount
ANR8330I 3592 volume FR0013 is mounted R/W in drive RMT_F1C4R4 (/dev/rmt_F1C4R4_0), status: IN USE.
ANR8334I         1 matches found.

Protect: ABCLIBMGR01>q proc

Process      Process Description      Process Status
  Number
--------     --------------------     -------------------------------------------------
     480     Database Backup          TYPE=FULL in progress. Bytes backed up: 61 MB.
                                       Current output volume(s): ABC013.
```

<a name="10"></a>
## 10. Hibakeresés

### 10.1 ANR????W: warningok az Activity Logban

10.1.1 ANR2292W The PROTECTKEYS parameter in SET DBRECOVERY is set to NO

```
Protect: TSMSRV1>q actlog search="PROTECTKEYS" begint=-24

Date/Time                Message
--------------------     ----------------------------------------------------------
06/29/20   09:38:13      ANR2292W The PROTECTKEYS parameter in SET DBRECOVERY is
                          set to NO. (SESSION: 46)
06/29/20   10:38:14      ANR2292W The PROTECTKEYS parameter in SET DBRECOVERY is
                          set to NO. (SESSION: 46)
06/29/20   11:38:59      ANR2292W The PROTECTKEYS parameter in SET DBRECOVERY is
                          set to NO. (SESSION: 46)
06/29/20   12:39:00      ANR2292W The PROTECTKEYS parameter in SET DBRECOVERY is
                          set to NO. (SESSION: 46)

Protect: TSMSRV1>q scr

Name                Description                                            Managing profile
---------------     --------------------------------------------------     --------------------
DAILYJOBSTART
DBB
DBINCR
RECLCSTART
RECLCSTOP
STGBKP_TAPE

Protect: TSMSRV1>q scr DBB f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBB            1
               5          backup db devc=DBBACKUP type=full  w=y
               10         delete volhistory type=dbbackup todate=today-2
               15         backup volhistory
               20         backup volhistory filename=/tsm/mynode/dbincr/config/volhisto-
                           ry.out
               25         backup devconfig
               30         backup devconfig filename=/tsm/mynode/dbincr/config/devconfig-
                           .out

Protect: TSMSRV1>q scr DBINCR f=l

Name           Line       Command
               Number
----------     ------     ------------------------------------------------------------
DBINCR         1
               5          backup db devc=DBINCR type=i w=y
               10         backup volhistory
               15         backup volhistory filename=/tsm/mynode/dbincr/config/volhisto-
                           ry.out
               20         backup devconfig
               25         backup devconfig filename=/tsm/mynode/dbincr/config/devconfig-
                           .out

Protect: TSMSRV1>q devc

Device        Device         Storag-     Device        Format     Est/Max      Mount
Class         Access         e Pool      Type                     Capacit-      Limit
Name          Strategy         Count                                y (MB)
---------     ----------     -------     ---------     ------     --------     ------
DBBACKUP      Sequential           0     FILE          DRIVE      51,200.0         20
DBINCR        Sequential           0     FILE          DRIVE      30,720.0         20
DISK          Random               3
REPLPOOL      Sequential           1     FILE          DRIVE      102,400.        512
                                                                         0
TS4500_ABC    Sequential           1     3592          DRIVE                   DRIVES

Protect: TSMSRV1>SET DBRECOVERY DBBACKUP PROTECTKEYS=YES PASSWORD=<serverpassword>
ANR2782I SET DBRECOVERY completed successfully and device class for automatic DB backup is set to DBBACKUP.

Protect: TSMSRV1>SET DBRECOVERY DBINCR PROTECTKEYS=YES PASSWORD=<serverpassword>
ANR2782I SET DBRECOVERY completed successfully and device class for automatic DB backup is set to DBINCR.
```

### 10.2 ANR????E: errorok az Activity Logban

#### 10.2.1 ANR2984E Database backup terminated due to environment or setup issue

```
Protect: TSMSRV1>run DBBACKUP
ANR0984I Process 2 for Database Backup started in the FOREGROUND at 02:29:21 PM.
ANR4559I Backup DB is in progress.
ANR2280I Full database backup started as process 2.
ANR4626I Database backup will use 1 streams for processing with the number originally requested 1.
ANR2984E Database backup terminated due to environment or setup issue related to DSMI_CONFIG - Db2 sqlcode -2033 sqlerrmc 406       .
ANR0985I Process 2 for Database Backup running in the FOREGROUND completed with completion state FAILURE at 02:29:23 PM.
ANR1893E Process 2 for Database Backup completed with a completion state of FAILURE.
...
```

Bizonyosodj meg róla, hogy a `userprofile` fájl megfelelően ki van-e töltve: 

```ksh
$ cat ~/sqllib/userprofile
export DSMI_CONFIG=/tsm/mynode/tsminst1/tsmdbmgr.opt
export DSMI_DIR=/opt/tivoli/tsm/client/api/bin64
export DSMI_LOG=/tsm/mynode/tsminst1
```

#### 10.2.2 ANR8471E Server no longer polling drive

```
Protect: ABCLIBMGR01>q drive

Library Name     Drive Name       Device Type     On-Line
------------     ------------     -----------     -------------------
PTLABC1          RMT_F1C2R4       3592            Unavailable Since
                                                  07/03/20   13:43:07
PTLABC1          RMT_F1C4R4       3592            Unavailable Since
                                                  07/03/20   14:13:13

Protect: ABCLIBMGR01>q path

Source Name     Source Type     Destinatio-     Destinatio-     On-Line
                                n Name          n Type
-----------     -----------     -----------     -----------     ----------
ABCLIBMGR01     SERVER          PTLABC1         LIBRARY         Yes
ABCLIBMGR01     SERVER          RMT_F1C2R4      DRIVE           No
ABCLIBMGR01     SERVER          RMT_F1C4R4      DRIVE           No
MYNODE          SERVER          RMT_F1C2R4      DRIVE           Yes
MYNODE          SERVER          RMT_F1C4R4      DRIVE           Yes

Protect: ABCLIBMGR01>upd drive PTLABC1 RMT_F1C2R4 online=yes
ANR8467I Drive RMT_F1C2R4 in library PTLABC1 updated.

Protect: ABCLIBMGR01>upd path ABCLIBMGR01 RMT_F1C2R4 srctype=server desttype=drive library=PTLABC1 online=yes
ANR1722I A path from ABCLIBMGR01 to PTLABC1 RMT_F1C2R4 has been updated.

Protect: ABCLIBMGR01>upd drive PTLABC1 RMT_F1C4R4 online=yes
ANR8467I Drive RMT_F1C4R4 in library PTLABC1 updated.

Protect: ABCLIBMGR01>upd path ABCLIBMGR01 RMT_F1C4R4 srctype=server desttype=drive library=PTLABC1 online=yes
ANR1722I A path from ABCLIBMGR01 to PTLABC1 RMT_F1C4R4 has been updated.

Protect: ABCLIBMGR01>q drive

Library Name     Drive Name       Device Type     On-Line
------------     ------------     -----------     -------------------
PTLABC1          RMT_F1C2R4       3592            Yes
PTLABC1          RMT_F1C4R4       3592            Yes

Protect: ABCLIBMGR01>q path

Source Name     Source Type     Destinatio-     Destinatio-     On-Line
                                n Name          n Type
-----------     -----------     -----------     -----------     ----------
ABCLIBMGR01     SERVER          PTLABC1         LIBRARY         Yes
ABCLIBMGR01     SERVER          RMT_F1C2R4      DRIVE           Yes
ABCLIBMGR01     SERVER          RMT_F1C4R4      DRIVE           Yes
MYNODE          SERVER          RMT_F1C2R4      DRIVE           Yes
MYNODE          SERVER          RMT_F1C4R4      DRIVE           Yes
```

#### 10.2.3 Check barcodes after forced restart

```
Protect: ABCLIBMGR01>audit library PTLABC1 CHECKLabel=Barcode
```

<a name="11"></a>
## 11. Hasznos SQL query-k

Van, amit csak ilyen query-vel lehet megkapni. 

Íme egy példa: minden lementett adat GB-ban, legalábbis egy adott hónap elsejétől:

```
Protect: TSMSRV1>SELECT CAST(FLOAT(SUM(bytes))/1024/1024/1024 AS DEC(8,2)) AS "Backed up data in GB" FROM summary WHERE activity='BACKUP' AND start_time >{'2021-10-01 00:00:00'} AND start_time <{'2021-10-21 00:00:00'}

 Backed up data in GB
---------------------
               570.14
```

### 11.1 Riportkészítési alapok

Nodelista: 

```
Protect: TSMSRV1>q node

Node Name                     Platform     Policy Domain      Days Sinc-     Days Sinc-     Locked?
                                           Name                  e Last      e Passwor-
                                                                  Access          d Set
-------------------------     --------     --------------     ----------     ----------     -------
...
ABCLIN01                      Linux        ABC_LINUX                  16            688       No
                               x86-64
...
```

Egy bizonyos node részletei szöveges jellemzőkkel (tehát NEM mezőnevekkel): 

```
Protect: TSMSRV1>q node ABCLIN01 f=d

                            Node Name: ABCLIN01
                             Platform: Linux x86-64
                      Client OS Level: 3.10.0-1160.24.1.el
                       Client Version: Version 8, release 1, level 9.0
                  Application Version: Version 0, release 0, level 0.0
                   Policy Domain Name: ABC_LINUX
                Last Access Date/Time: 10/04/21   20:30:20
               Days Since Last Access: 16
...
```

SQL lekérdezés a mezőnevek megismeréséhez: 

```
Protect: TSMSRV1>select * from nodes where node_name='ABCLIN01'

                 NODE_NAME: ABCLIN01
                   NODE_ID: 243
             PLATFORM_NAME: Linux x86-64
               DOMAIN_NAME: ABC_LINUX
                PWSET_TIME: 2019-12-02 14:44:46.000000
          INVALID_PW_COUNT: 0
                   CONTACT:
               COMPRESSION: CLIENT
                ARCHDELETE: YES
                BACKDELETE: NO
                    LOCKED: NO
              LASTACC_TIME: 2021-10-04 20:30:20.000000
                  REG_TIME: 2019-12-02 14:44:46.000000
                 REG_ADMIN: GJAKAB
         LASTSESS_COMMMETH: SSL
            LASTSESS_RECVD: 695713188
             LASTSESS_SENT: 11986061
         LASTSESS_DURATION: 1.27580000000000E+001
         LASTSESS_IDLEWAIT: 2.76800000000000E+000
         LASTSESS_COMMWAIT: 2.56000000000000E-001
        LASTSESS_MEDIAWAIT: 0.00000000000000E+000
            CLIENT_VERSION: 8
            CLIENT_RELEASE: 1
              CLIENT_LEVEL: 9
           CLIENT_SUBLEVEL: 0
...
```

SQL lekérdezés bizonyos mezőnevekre való szelektálással, az eredeti mezőnevek megtartásával: 

```
Protect: TSMSRV1>select substr(node_name,1,30) as NODE_NAME, CLIENT_VERSION, CLIENT_RELEASE, CLIENT_LEVEL, CLIENT_SUBLEVEL from NODES where NODE_NAME='ABCLIN01'

NODE_NAME                            CLIENT_VERSION      CLIENT_RELEASE      CLIENT_LEVEL      CLIENT_SUBLEVEL
-------------------------------     ---------------     ---------------     -------------     ----------------
ABCLIN01                                          8                   1                 9                    0
```

Ugyanez, de egyedi mezőnevekkel: 

```
>select substr(NODE_NAME,1,25) as "Node Name",substr(CLIENT_VERSION,1,2) as "V",substr(CLIENT_RELEASE,1,2) as "R",substr(CLIENT_LEVEL,1,2) as "L",substr(CLIENT_SUBLEVEL,1,2) as "S" from NODES where NODE_NAME='ABCLIN01'

Node Name                      V       R       L       S
--------------------------     ---     ---     ---     ---
ABCLIN01                       8       1       9       0
```

Az alapértelmezett oszlopokba rendezés helyett a `dsmadmc`-t kell paramétereznünk, tehát már alapból így belépnünk: 

```ksh
$ dsmadmc -commadel
...
```

A kimenet így már meg is változik: 

```
tsm: CRAIG>select substr(NODE_NAME,1,25) as "Node Name",substr(CLIENT_VERSION,1,2) as "V",substr(CLIENT_RELEASE,1,2) as "R",substr(CLIENT_LEVEL,1,2) as "L",substr(CLIENT_SUBLEVEL,1,2) as "S" from NODES where NODE_NAME='ABCLIN01'
ABCLIN01                ,8 ,1 ,9 ,0
```

Teljes nodelista választott mezőkkel, azokat `,` karakterekkel elválasztva, egy külső fájlba irányítva (a UNIX szintű jogosultságnak jónak kell lennie a megadott fájlba való íráshoz): 

```
Protect: TSMSRV1>select NODE_NAME, TCP_NAME, TCP_ADDRESS, LASTACC_TIME, PLATFORM_NAME, CLIENT_OS_NAME, CLIENT_VERSION, CLIENT_RELEASE, CLIENT_LEVEL, CLIENT_SUBLEVEL from NODES > /tmp/nodelist.txt
Output of command redirected to file '/tmp/nodelist.txt'
```

Script-ből való bekérdezésre példa: 

```ksh
$ ENPW="AbcDefGhiJkl0123="

$ dsmadmc -id=support -pa=$(echo $ENPW | base64 -d) "select substr(stgpool_name,1,15) as Storage_Pool_Name,cast(float(EST_CAPACITY_MB/1024) as DEC(8,2)) as Storage_Pool_size_GB,cast(float(EST_CAPACITY_MB/1024)*PCT_UTILIZED/100 as DEC(8,2)) as Storage_Pool_Util_GB,substr(PCT_UTILIZED,1,5) as Utilization_Percentage from stgpools where stgpool_name='MYSTGP_DSK'"
IBM Spectrum Protect
Command Line Administrative Interface - Version 8, Release 1, Level 10.0
(c) Copyright by IBM Corporation and other(s) 1990, 2020. All Rights Reserved.

Session established with server TSMSRV1: Linux/x86_64
  Server Version 8, Release 1, Level 12.100
  Server date/time: 20/10/21   15:48:10  Last access: 20/10/21   15:47:52

ANS8000I Server command: 'select substr(stgpool_name,1,15) as Storage_Pool_Name,cast(float(EST_CAPACITY_MB/1024) as DEC(8,2)) as Storage_Pool_size_GB,cast(float(EST_CAPACITY_MB/1024)*PCT_UTILIZED/100 as DEC(8,2)) as Storage_Pool_Util_GB,substr(PCT_UTILIZED,1,5) as Utilization_Percentage from stgpools where stgpool_name='MYSTGP_DSK''.

STORAGE_POOL_NAME       STORAGE_POOL_SIZE_GB      STORAGE_POOL_UTIL_GB     UTILIZATION_PERCENTAGE
------------------     ---------------------     ---------------------     -----------------------
MYSTGP_DSK                             10.00                      5.45     54.5

ANS8002I Highest return code was 0.
```

Ugyanez, de kiszűrve a "sallangot": 

```ksh
$ dsmadmc -id=support -pa=$(echo $ENPW | base64 -d) -dataonly=yes "select substr(stgpool_name,1,15) as Storage_Pool_Name,cast(float(EST_CAPACITY_MB/1024) as DEC(8,2)) as Storage_Pool_size_GB,cast(float(EST_CAPACITY_MB/1024)*PCT_UTILIZED/100 as DEC(8,2)) as Storage_Pool_Util_GB,substr(PCT_UTILIZED,1,5) as Utilization_Percentage from stgpools where stgpool_name='MYSTGP_DSK'"
MYSTGP_DSK                             10.00                      5.45     54.5
```

### 11.2 Táblanevek

```
Protect: TSMSRV1>select TABNAME from TABLES order by 1

TABNAME: ACTIVITY_SUMMARY

TABNAME: ACTLOG

TABNAME: ADMINS
...
```

### 11.3 Felszabadítható szalagok listázása

Melyek azok a szalagok, amiknek a device class-a "DC_JAG50", írhatók/olvashatók, a visszanyerhető mennyiség legalább 1%, a kihasznált terület legalább 80%, a szalag státusza pedig `FULL` vagy `FILLING`: 

```
tsm: CRAIG>q scr WIN_JAG_C_MOVEABLE f=r
select substr(VOLUME_NAME,1,10) as VOLUME,substr(STGPOOL_NAME,1,10) as POOL,substr(PCT_UTILIZED,1,3) as UTIL,substr(PCT_RECLAIM,1,3) as RECLAIM,substr(ACCESS,1,10) as ACCESS,substr(status,1,10) as STATUS from volumes where DEVCLASS_NAME LIKE '%DC_JAG50%' and ACCESS='READWRITE' and PCT_RECLAIM>1 and PCT_UTILIZED<80 and (status='FILLING' or status='FULL') order by STGPOOL_NAME, pct_reclaim desc
```

Kimenete: 

```
Protect: TSMSRV1>run WIN_JAG_C_MOVEABLE

VOLUME          POOL            UTIL      RECLAIM      ACCESS          STATUS
-----------     -----------     -----     --------     -----------     -----------
T00957          WIN_JAG_C       11.       90.          READWRITE       FULL
D01078          WIN_JAG_C       13.       88.          READWRITE       FULL
T00910          WIN_JAG_C       12.       88.          READWRITE       FULL
...
```

...