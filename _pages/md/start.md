<div style="display: flex; align-items: center; justify-content: flex-end; flex-wrap: wrap; margin-bottom: 20px; padding: 5px;">
  <aside class="big">
    <a href="/prog/python"><img src="/prog/python/python_-_bg.jpg"></a>
    <h2><a href="/prog">Programming</a></h2>
    <a href="/prog/python"><h3>Python</h3>
    <p>Python for beginners</p></a>
  </aside>
  <aside>
    <a href="/sysadmin/linux/storage"><img src="/sysadmin/linux/storage/linux_-_storage_-_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/linux/storage"><h3>Linux Storage</h3>
    <p>Comprehensive guide to Linux storage management.</p></a>
  </aside>
  <aside>
    <a href="/sysadmin/linux/network"><img src="/sysadmin/linux/network/linux_-_network_-_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/linux/network"><h3>Linux Network and Security Basics</h3>
    <p>Linux networking basics, SSH, NFS, Samba, firewalls, xinetd, and troubleshooting tools</p></a>
  </aside>
  <aside>
    <a href="/prog/bash"><img src="/prog/bash/bash_-_bg.jpg"></a>
    <h2><a href="/prog">Programming</a></h2>
    <a href="/prog/bash"><h3>Bash, KSH</h3>
    <p>Shell scripting from the basics</p></a>
  </aside>
  <!-- <aside>
    <a href="/sysadmin/ansible"><img src="/sysadmin/ansible/ansible_-_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/ansible"><h3>Ansible</h3>
    <p>Ansible from basics to an intermediate level</p></a>
  </aside>
  <aside>
    <a href="/sysadmin/terraform"><img src="/sysadmin/terraform/terraform_-_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/terraform"><h3>Terraform</h3>
    <p>Establishing Infrastructure as Code on Azure</p></a> -->
  <aside>
    <a href="/electronics/catalogue/transistors"><img src="/electronics/catalogue/transistors/transistors_-_bg.jpg"></a>
    <h2><a href="/electronics">Electronics</a></h2>
    <a href="/electronics/catalogue/transistors"><h3>Transistor Catalogue</h3>
    <p>Listing some discrete transistor types, sorted by general series</p></a>
  </aside>
  <aside>
    <a href="/electronics/circuits/gates"><img src="/electronics/circuits/gates/gates_bg.jpg"></a>
    <h2><a href="/electronics">Electronics</a></h2>
    <a href="/electronics/circuits/gates"><h3>Prototype Logic Gates</h3>
    <p>Building and testing some fundamental logic gate circuits</p></a>
  </aside>
  <aside>
    <a href="/sysadmin/qemu"><img src="/sysadmin/qemu/qemu_-_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/qemu"><h3>QEMU</h3>
    <p>Introducing QEMU, installing and running AIX on Linux x86_64</p></a>
  </aside>
  <aside>
    <a href="/sysadmin/xen"><img src="/sysadmin/xen/xen_bg.jpg"></a>
    <h2><a href="/sysadmin">System Administration</a></h2>
    <a href="/sysadmin/xen"><h3>Xen Project</h3>
    <p>HVM, PV and PVH, installation, manging VMs with libvirt or xl</p></a>
  </aside>
  <aside>
    <a href="/linguistics/finnish"><img src="/linguistics/finnish/finnish_-_bg.jpg"></a>
    <h2><a href="/linguistics">Linguistics</a></h2>
    <a href="/linguistics/finnish"><h3>Finnish</h3>
    <p>Some history behind and basics of Finnish</p></a>
  </aside>
</div>