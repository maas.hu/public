# 🪄 ImageMagick

<div style="text-align: right;"><h7>Posted: 2024-05-07 22:23</h7></div>

###### .

ImageMagick is a set of useful CLI tools to manipulate and investigate image data. 

## Fix timestamp of image

I don't know how frequent anomaly it is, but it's possible that our picture file is stored even on the camera's SD card with a different timestamp than it was actually captured. Luckily, the EXIF data contains the proper information, so let's use that one with a nice script. 

The general details can be extracted even with an image viewer like Eye of Gnome, but that's not scriptable. Here's how the same can be extracted on CLI with ImageMagick's `identify` program, compared with the filesystem level timestamp:

```ksh
$ identify -verbose IMG_0389.JPG
Image:
  Filename: IMG_0389.JPG
  Permissions: rw-r--r--
  Format: JPEG (Joint Photographic Experts Group JFIF format)
[...]
  Properties:
    date:create: 2024-05-07T17:06:20+00:00
    date:modify: 2024-05-02T10:33:38+00:00
    date:timestamp: 2024-05-07T17:53:59+00:00
[...]

$ ls -l IMG_0389.JPG
-rw-r--r-- 1 kmihaly users 6284405 May  2 13:33 IMG_0389.JPG
```

Adjusting filesystem level timestamp of hundreds of images: 

```ksh
$ for i in *JPG ; do echo $i ; TS=$(identify -verbose $i | awk '/exif:DateTime:/{print $2$3}' | awk 'BEGIN{FS=":"}{print $1$2$3$4"."$5}') ; touch -t $TS $i ; done
IMG_0389.JPG
IMG_0390.JPG
[...]
```
