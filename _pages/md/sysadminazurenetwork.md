# ![alt text](/_ui/img/icons/azure_network_m.svg) Azure Networking

<div style="text-align: right;"><h7>Posted: 2024-10-20 18:34</h7></div>

###### .

![alt_text](azure_-_network_-_bg.jpg)


## Sisällysluettelo

1. [Johdanto](#1)
2. [VNetit ja Subnetit](#2)
3. [Network Security Groups (NSGs)](#3)
4. [VNet Peering](#4)
5. [Public IP](#5)
6. [Azure DNS](#6)
7. [Routing ja User Defined Routes (UDR)](#7)
8. [Network Watcher](#8)


<a name="1"></a>
## 1. Johdanto

**Azure Networking** on tärkeä osa **Infrastructure as a Service** (**IaaS**) -ratkaisuja, joissa hallitaan ja ohjataan pilvi-infrastruktuurin verkkoja. Azure-verkkojen avulla voidaan rakentaa ja hallita virtuaaliverkkoja (VNets), palomuureja, reitityksiä ja muita verkkokomponentteja, jotka ovat keskeisiä pilvi-infrastruktuurissa. Tässä artikkelissa käsitellään Azure Networkingin keskeisiä komponentteja ja niiden roolia verkkoarkkitehtuurissa.

Tämä artikkeli tarjoaa kattavan katsauksen Azure Networking -ratkaisuihin, joilla hallitaan ja suojataan verkkoja Azure-ympäristössä. Järjestelmällinen verkkorakenteen suunnittelu ja tietoturvakonfiguraatio ovat keskeisiä osia pilvi-infrastruktuurien menestyksellisessä käyttöönotossa. Lisää tietoa löytyy osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/networking/).


<a name="2"></a>
## 2. VNetit ja Subnetit

**Azure-näennäisverkkot** (**VNet**) ovat Azuren tarjoamia loogisia verkkoja, jotka mahdollistavat resurssien välisten yhteyksien hallinnan turvallisesti ja skaalautuvasti. VNet tarjoaa eristetyn ympäristön, jossa Azure-resurssit voivat kommunikoida toistensa kanssa yksityisillä IP-osoitteilla. Jokainen VNet määrittää oman IP-osoiteavaruuden, jonka puitteissa resursseja voidaan sijoittaa ja jakaa eri osiin.

VNet voi olla eristetty, mutta se voidaan myös yhdistää muihin Azure-palveluihin, kuten **Storage Accounts**, sekä on-premises-infrastruktuureihin VPN-yhteyksien tai ExpressRoute-ratkaisujen kautta. VNet mahdollistaa myös julkisen internet-yhteyden esimerkiksi julkisten IP-osoitteiden avulla, mutta pääsääntöisesti se toimii yksityisenä ja turvallisena verkkoalueena.

### 2.1 VNetin IP-osoiteavaruus

Jokainen VNet määrittää oman IP-osoiteavaruutensa, joka ilmoitetaan CIDR-muodossa (esim. `10.0.0.0/16`). Tämä osoiteavaruus jaetaan edelleen aliverkkoihin, ja sitä käytetään yksityisten IP-osoitteiden jakamiseen resursseille. On tärkeää suunnitella osoiteavaruus huolellisesti, jotta vältetään osoitekonflikteja ja mahdollistetaan verkon laajennukset tulevaisuudessa.

- **CIDR (Classless Inter-Domain Routing)**: CIDR-muoto mahdollistaa joustavamman IP-osoitteiden hallinnan. Esimerkiksi CIDR `10.0.0.0/16` tarjoaa 65 536 IP-osoitetta, ja se voidaan jakaa useisiin pienempiin aliverkkoihin (subnetit).
- **IP-osoitealueiden hallinta**: VNetin sisällä hallitaan useita resursseja, kuten virtuaalikoneita, tietokantoja ja sovelluksia, jotka saavat yksityisiä IP-osoitteita.

### 2.2 Aluekohtaisuus ja VNet-yhteydet

VNet on aina sidottu tiettyyn Azure-alueeseen (**region**), mikä tarkoittaa, että se ei voi kattaa useampia alueita. Jos haluat yhdistää resursseja eri alueilla olevien VNetien välillä, on käytettävä joko **VNet Peering** -ratkaisua tai VPN-yhteyttä. VNet voi kuitenkin olla yhteydessä muihin VNet-verkkoihin saman alueen sisällä ilman julkista internetyhteyttä.

- **VNet Peering**: VNet-verkot voidaan yhdistää toisiinsa peering-yhteyksillä, mikä mahdollistaa resurssien välisen yhteydenpidon ilman julkista internetyhteyttä.
- **VPN ja ExpressRoute**: VNet voidaan liittää on-premises-ympäristöihin VPN-yhteyden tai ExpressRouten kautta, jolloin paikalliset resurssit voivat kommunikoida pilvessä sijaitsevien resurssien kanssa.

### 2.3 Subnetit

VNetin IP-osoiteavaruus jaetaan **subneteihin**, jotka toimivat VNetin sisäisinä **aliverkkoina**. Subnettien avulla resursseja voidaan segmentoida, mikä helpottaa liikenteen hallintaa ja mahdollistaa eri verkkoturvallisuuskäytännöt.

- Jokaisella subnetillä on oma IP-osoiteavaruus, joka määritetään osana VNetin suurempaa osoiteavaruutta.
- Subnettien avulla voidaan jakaa sovelluskerrokset, tietokannat ja hallintapalvelut omiin aliverkkoihinsa, mikä parantaa turvallisuutta ja skaalautuvuutta.

### 2.4 Subnettien IP-osoitteet ja segmentointi

Jokaisella subnetillä on oma IP-osoiteavaruus, ja sen koko määritetään myös CIDR-muodossa (esim. `10.0.0.0/24`, joka tarjoaa 256 osoitetta). Azuren sisäiseen käyttöön varataan aina neljä ensimmäistä ja viimeinen IP-osoite jokaisesta subnetistä erityistarkoituksiin.

Esimerkiksi, kun VNetin CIDR on `10.0.0.0/16`, ja luot `subnet1`-nimisen subnetin, jonka osoitealue on `10.0.0.0/24`, subnetin varatut IP-osoitteet ovat seuraavat:

- `.0`: verkko-osoite
- `.1`: yhdyskäytävä
- `.2` ja `.3`: Azure DNS -osoitteiden kartoittamiseksi VNet-tilaan
- `.255`: subnetin lähetysosoite (broadcast address)

Subnettien segmentoinnilla voidaan jakaa esimerkiksi sovellus- ja tietokantapalvelut omiin aliverkkoihinsa, mikä parantaa resurssien hallintaa ja turvallisuutta. Tämä mahdollistaa selkeämmän jaottelun eri palveluista ja vähentää riskejä.

Subnettien välinen liikenne on oletuksena sallittua, mikä helpottaa resurssien välistä kommunikointia. Kuitenkin liikennettä voidaan hallita tarkemmin käyttämällä **Network Security Group** (**NSG**), jonka avulla voit määritellä, millainen liikenne sallitaan tai estetään kunkin subnetin välillä.

Yleiskuvan saaminen jo määritellyistä verkkoalueista tai niin kutsutuista "prefikseistä" on haastavaa Azure Portalissa, mutta onneksi voit saada tällaisen listan käyttämällä `az`-komentoa jokaisessa Tilauksessa, johon sinulla on käyttöoikeus. Esim.:

<pre class="con">
<span class="psu">$</span> <span class="cmd">az account set --subscription befa9012-a1b2-c3d4-e5f6-a1b2c3d4e5f6</span>

<span class="psu">$</span> <span class="cmd">az network vnet list --output table</span>
Name                 ResourceGroup        Location    NumSubnets    Prefixes          DnsServers    DDOSProtection    VMProtection
-------------------  -------------------  ----------  ------------  ----------------  ------------  ----------------  --------------
gitlab-services-dev  gitlab-services-dev  westeurope  1             10.236.72.0/23                  False
common-emea-vnet     common               westeurope  2             10.224.4.0/26                   False
common-us-vnet       common               eastus2     1             10.225.2.0/27                   False
</pre>

### 2.5 Subnettien segmentoinnin suunnittelu

Resurssien segmentointi subnetteihin mahdollistaa liikenteen tarkemman hallinnan ja resurssien eriyttämisen. Esimerkiksi voit luoda erillisiä subnettejä eri palveluille, kuten sovelluksille, tietokannoille ja hallintapalveluille. Tämä mahdollistaa erilaisten liikenteenhallinta- ja turvallisuuskäytäntöjen soveltamisen kullekin segmentille.

- **Verkkoturva**: Subnettien välistä liikennettä voidaan hallita erillisten NSG-sääntöjen avulla, jolloin voit tarkasti määrittää, mikä liikenne on sallittua ja mikä estetään.
- **Liikenteen ohjaus**: Subnettien välillä voidaan käyttää **User Defined Routes** (UDR) -reitityssääntöjä, jotka mahdollistavat liikenteen ohjaamisen halutun reitittimen tai palomuurin kautta.

Tämä rakenne tarjoaa joustavan ja turvallisen tavan hallita Azure-verkkoja ja resursseja, mahdollistaa eri sovelluskerrosten ja palveluiden erottelun sekä tehokkaan liikenteen ohjauksen ja segmentoinnin.


<a name="3"></a>
## 3. Network Security Groups (NSG)

**Network Security Groupit** (**NSG:t**) ovat olennainen osa Azure-verkkoturvallisuutta. Niiden avulla voit määrittää säännöt, jotka sallivat tai estävät verkkoliikenteen saapumisen ja poistumisen resursseista. NSG:t voivat sisältää säännöt sekä sisään- että ulosmenevälle liikenteelle.

NSG:n avulla voit määrittää säännöt, jotka hallitsevat liikennettä yksittäisten resurssien tai aliverkkojen tasolla.

### 3.1 NSG:n oletussäännöt

Kun NSG luodaan, siihen sisältyy kuusi oletussääntöä, joita ei voi poistaa tai muokata. Nämä säännöt varmistavat perusliikenteen sujuvuuden ja estävät ei-toivotun liikenteen.

- Kolme sääntöä koskee **Inbound**-liikennettä ja kolme **Outbound**-liikennettä.
- **Deny All** -säännöt estävät kaiken muun liikenteen, jota ei ole erikseen sallittu.

### 3.2 Sääntöjen prioriteetti ja toteutus

Säännöillä on **prioriteettiarvo**, joka määrittää, missä järjestyksessä säännöt arvioidaan. 

<div class="info">
<p>Pienempi numero tarkoittaa korkeampaa prioriteettia, ja ensimmäinen osuva sääntö määrittää liikenteen kohtalon.</p>
</div>

Esimerkiksi prioriteetilla `100` oleva sääntö arvioidaan ennen sääntöä, jonka prioriteetti on `200`.

### 3.3 NSG:n liittäminen resursseihin

NSG voidaan liittää joko yksittäisiin **Network Interface Card** (**NIC**) -kortteihin tai kokonaisiin subnetteihin. Tämä mahdollistaa liikenteen hallinnan joko yksittäisen resurssin tai koko aliverkon tasolla.

- NSG:n liittäminen **NIC:iin** hallitsee liikennettä vain kyseisen resurssin tasolla.
- NSG:n liittäminen **subnettiin** hallitsee liikennettä kaikille aliverkon resursseille.

Seuraava esimerkki näyttää NSG:n, johon sisältyy kaksi mukautettua saapuvan liikenteen sääntöä, ja NSG itse on liitetty verkkoliitäntään.

![alt_text](azure_-_portal_-_nsg.png)


<a name="4"></a>
## 4. VNet Peering

**VNet Peering** -toiminnon avulla voit yhdistää kaksi tai useampia virtuaaliverkkoja, jotka voivat sijaita jopa eri alueilla. Tämä mahdollistaa resurssien välisen suoran yhteyden ilman, että liikenne kulkee julkisen internetin kautta.

### 4.1 Alueellinen ja globaali Peering

- **Alueellinen Peering**: Peering, jossa kaksi VNetiä sijaitsevat samassa Azure-alueessa, on edullisempaa kuin globaali peering.
- **Globaali Peering**: Jos VNetit sijaitsevat eri alueilla, peering on globaali, ja sen kustannukset ovat korkeammat.

### 4.2 Peeringin transitiivisuus

Peering-yhteydet eivät ole **transitiivisia**, mikä tarkoittaa, että jos VNet1 on peering-yhteydessä VNet2:een ja VNet2 on yhteydessä VNet3:een, VNet1 ei voi kommunikoida suoraan VNet3:n kanssa ilman erillistä peering-yhteyttä.

### 4.3 Peeringin kustannukset

Peering-yhteyksistä laskutetaan tiedonsiirron mukaan, eli mitä enemmän dataa siirretään verkkojen välillä, sitä korkeammat kustannukset ovat. Globaali peering on aina kalliimpaa kuin alueellinen peering, koska data siirretään eri alueiden välillä.

<div class="info">
<p>Alueellinen peering on halvempi vaihtoehto, kun molemmat VNetit sijaitsevat samassa Azure-alueessa.</p>
</div>


<a name="5"></a>
## 5. Public IP

Julkiset IP-osoitteet ovat resursseihin liitettäviä osoitteita, joiden avulla nämä voivat olla yhteydessä internetiin. Niitä ei voi liittää suoraan virtuaalikoneisiin, vaan ne liitetään esimerkiksi **load balancerin** tai **Application Gatewayn** kautta.

- Julkiset IP-osoitteet voivat olla **dynaamisia** tai **staattisia**. Dynaaminen osoite voi muuttua esimerkiksi palvelimen uudelleenkäynnistyksen yhteydessä.
- **Kustannukset**: Julkisten IP-osoitteiden käyttö on maksullista, ja ne ovat aluekohtaisia resursseja, eli julkinen IP-osoite on sidottu tiettyyn alueeseen.


<a name="6"></a>
## 6. Azure DNS

Azure DNS on täysin hallittu nimipalvelu, joka mahdollistaa oman DNS-vyöhykkeen isännöinnin ja DNS-tietueiden hallinnan Azuren ympäristössä. Se tarjoaa korkean suorituskyvyn, skaalautuvuuden ja integroitumisen muihin Azure-palveluihin. Azure DNS mahdollistaa DNS-vyöhykkeiden hallinnan samalla tavalla kuin perinteiset DNS-palvelut, mutta ilman infrastruktuurin hallintaan liittyviä huolia.

Azure DNS eroaa muista DNS-ratkaisuista tarjoamalla täysin hallitun DNS-palvelun, joka on integroitu Azuren muihin palveluihin ja tarjoaa sisäänrakennettuja ratkaisuja korkean käytettävyyden ja skaalautuvuuden tarpeisiin. Azure DNS tukee sekä julkisia että yksityisiä DNS-vyöhykkeitä, mikä tekee siitä joustavan ratkaisun useisiin eri käyttötapauksiin, kuten verkkopalveluiden julkaisujen ja sisäisten verkkojen hallintaan.

### 6.1 Miksi Azure DNS?

Azure DNS on hyödyllinen useista syistä. Ensinnäkin se poistaa tarpeen hallita fyysisiä DNS-palvelimia tai huolehtia niiden skaalautuvuudesta. Koska palvelu on täysin hallittu, Azure huolehtii nimipalvelun toiminnasta ja ylläpidosta, mikä vähentää hallinnointitarvetta ja infrastruktuurikustannuksia. 

Toinen merkittävä etu on, että Azure DNS integroituu saumattomasti muihin Azure-resursseihin, kuten **Azure Virtual Network** -ratkaisuihin ja **Azure Load Balancer** -palveluun. Tämä mahdollistaa helpon DNS-tietueiden hallinnan ja automaattisen integroinnin osana laajempaa pilvi-infrastruktuuria.

Lisäksi Azure DNS tarjoaa tuen **DNSSEC**-tietueiden hallintaan, mikä auttaa parantamaan DNS-tietueiden turvallisuutta, sillä DNSSEC varmistaa tietueiden aitouden ja suojaa niitä peukaloinnilta.

### 6.2 Julkiset ja yksityiset DNS-vyöhykkeet

Azure DNS tukee sekä julkisia että yksityisiä DNS-vyöhykkeitä. Julkisia DNS-vyöhykkeitä käytetään, kun halutaan tarjota nimipalvelu resursseille, joihin pääsee julkisen internetin kautta, kuten verkkosivustoille. 

Yksityiset DNS-vyöhykkeet taas mahdollistavat nimipalvelun sisäisille resursseille, kuten virtuaalikoneille, jotka toimivat **Azure-näennäisverkoissa**. Tämä on erityisen hyödyllistä, kun halutaan tarjota nimipalvelua resursseille, jotka ovat eristettyjä julkisesta internetistä, mutta joiden täytyy kommunikoida toistensa kanssa Azure-ympäristön sisällä.

Julkisten DNS-vyöhykkeiden hallinta tapahtuu täysin Azuren kautta, jolloin rekisteröintipalvelut täytyy edelleen hoitaa erikseen. Yksityisten DNS-vyöhykkeiden avulla Azure DNS tarjoaa mahdollisuuden käyttää omia nimipalveluita **VNet**-verkkojen sisällä ilman tarvetta julkisille DNS-palvelimille.

### 6.3 Azure DNS:n avainkäsitteet

Azure DNS:ään liittyy useita tärkeitä käsitteitä, jotka ovat välttämättömiä ymmärtääksesi sen toimintaa:

1. **DNS-vyöhyke**: Vyöhyke on kokonaisuus, joka sisältää joukon DNS-tietueita. Jokainen vyöhyke edustaa tiettyä verkkotunnusta, kuten `esimerkki.com`, ja kaikki siihen liittyvät tietueet hallitaan yhden vyöhykkeen kautta.
   
2. **DNS-tietueet**: DNS-tietueet, kuten `A`, `AAAA`, `MX`, `CNAME`, ja `TXT`, määrittävät, miten verkkotunnukseen liittyvät resurssit löytyvät ja miten ne voidaan tavoittaa. Azure DNS:ssä voit hallita kaikkia perinteisiä DNS-tietuetyyppejä.
   
3. **Name Server (NS)**: Kun luot uuden DNS-vyöhykkeen, Azure luo automaattisesti neljä nimeämispalvelinta (name server) kyseistä vyöhykettä varten. Nämä palvelevat tietueita julkiseen tai yksityiseen käyttöön vyöhykkeen tyypin mukaan.
   
4. **TTL (Time to Live)**: TTL-arvo määrittää, kuinka kauan tietue tallennetaan välimuistiin ennen kuin sitä täytyy päivittää. Azure DNS mahdollistaa TTL-arvojen hallinnan, mikä auttaa optimoimaan DNS-vastauksien suorituskykyä ja päivityksiä.

### 6.4 Yhteensopivuus ja suorituskyky

Azure DNS hyödyntää Microsoftin globaalia datakeskusverkkoa, mikä tarkoittaa, että DNS-kyselyt vastaavat nopeasti ja luotettavasti riippumatta siitä, mistä päin maailmaa ne tulevat. Tämä tekee siitä erityisen hyödyllisen verkkosivustoille ja sovelluksille, jotka tarvitsevat matalat vasteajat ja globaalin kattavuuden.

Toisin kuin muut DNS-ratkaisut, Azure DNS:ää ei tarvitse ylläpitää manuaalisesti tai asentaa palvelimille, vaan kaikki hallinta tapahtuu suoraan Azure-portaalin tai **Azure Resource Manager** -rajapinnan kautta. Tämä integroitu hallintamalli tekee DNS-tietueiden hallinnasta suoraviivaista ja osana organisaation kokonaisinfrastruktuuria.

Azure DNS on siis kustannustehokas, suorituskykyinen ja helposti skaalautuva ratkaisu, joka on erityisen hyödyllinen, kun halutaan hallita DNS-tietueita pilvessä muiden Azure-resurssien rinnalla. Lisää tietoa ja ajankohtaisia ohjeita löydät aina osoitteesta [learn.microsoft.com](https://learn.microsoft.com/en-us/azure/dns/).


<a name="7"></a>
## 7. Routing ja User Defined Routes (UDR)

Routing Azure-ympäristössä määrittää, miten liikenne ohjautuu eri resurssien välillä. Azure määrittää oletusreitit virtuaaliverkkojen sisällä, mutta käyttäjät voivat lisätä **User Defined Route** (UDR) -sääntöjä mukautetun liikenteen ohjaamiseksi.

- **Oletusreitit**: Azure määrittää automaattisesti reitit VNetin sisäiselle liikenteelle.
- **User Defined Routes (UDR)**: Käyttäjät voivat luoda mukautettuja reittejä ohjatakseen liikennettä haluamallaan tavalla.

### 7.1 UDR:n määrittäminen

User Defined Route -säännöillä voit muokata liikenteen oletuskäyttäytymistä. Voit esimerkiksi määrittää liikenteen kulkemaan tietyn reitittimen tai palomuurin kautta.


<a name="8"></a>
## 8. Network Watcher

**Network Watcher** on Azuren tarjoama työkalu, jolla voidaan seurata ja diagnosoida verkko-ongelmia. Se tarjoaa monipuolisia työkaluja, kuten **Topology**, **IP flow verify**, **Next hop** ja **Packet capture**.

### 8.1 Network Watcherin työkalut

Network Watcher sisältää useita työkaluja, jotka auttavat seuraamaan ja diagnosoimaan Azure-verkon toimintaa:

- **Topology**: Näyttää koko verkon topologian ja auttaa visualisoimaan verkon rakenteen.
- **IP flow verify**: Tarkistaa, voivatko tietyt VNetit tai virtuaalikoneet kommunikoida tietyillä porteilla.
- **Next hop**: Laskee seuraavan reitityspisteen liikenteen kohteelle.
- **Packet capture**: Taltioi paketteja syvälliseen analysointiin, mikä on hyödyllistä vianetsinnässä.

Seuraava esimerkki näyttää yksinkertaisen **NSG-diagnostiikan**, joka on tehty Network Watcher -työkalulla tarkistaaksesi, onko tietylle IP-osoitteelle sallittu pääsy valitun verkkoliitännän IP-osoitteeseen.

![alt_text](azure_-_portal_-_network_watcher_-_nsg_diagnostics.png)
