# ![alt text](/_ui/img/icons/babytux_m.svg) Linux

![alt_text](linux_-_bg.jpg)

## Linux in General

<!-- - ![alt text](/_ui/img/icons/babytux_s.svg) [Basics](/sysadmin/linux/basics) -->
- ![alt text](/_ui/img/icons/babytux_s.svg) [Storage](/sysadmin/linux/storage)
- ![alt text](/_ui/img/icons/babytux_s.svg) [Network and Security Basics](/sysadmin/linux/network)

## Distributions

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/rocky_s.svg) [Rocky](/sysadmin/linux/rocky) -->
<!-- - ![alt text](/_ui/img/icons/suse_s.svg) [openSUSE](/sysadmin/linux/opensuse) -->
<!-- - ![alt text](/_ui/img/icons/mageia_s.svg) [Mageia](/sysadmin/linux/mageia) -->

## Window Managers

<div class="info">
<p>Later...</p>
</div>

<!-- - ![alt text](/_ui/img/icons/gnome_s.svg) [GNOME](/sysadmin/linux/gnome) -->
