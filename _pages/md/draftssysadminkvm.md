# ![alt text](/_ui/img/icons/babytux_m.svg) KVM

<div style="text-align: right;"><h7>Posted: 2022-09-21 17:21</h7></div>

###### .


## Table of contents

1. [Introduction](#1)


<a name="1"></a>
## 1. Introduction

![alt_text](kvm_bg.jpg)

KVM stands for **Kernel-based Virtual Machine**, which is a virtualization module in the Linux kernel that allows the kernel to function as a hypervisor.

There are arguments all around the internet that KVM is not a true Type-1 hypervisor, but rather a Type-2 (or a combination of the two). That's not entirely true. 

KVM was first integrated into the mainline Linux kernel at the beginning of 2007 with the 2.6.20 kernel. You can have a standard Linux distribution e.g. for Desktop or Server use, but once you install the necessary package dependencies for administering KVM, it "awakens" the KVM capabilities of Linux and that converts Linux into a Type-1 hypervisor. 

However, if you keep other applications that "compete" for resources on that machine, and you still use e.g. your laptop as a Desktop OS, you lower the value of Type-1-ness. This is why there are still debates about KVM being a true Type-1 hypervisor. 

### 1.1 PowerKVM

IBM Power Systems servers have traditionally been virtualized with **PowerVM**, and this continues to be an option on standard scale-out servers. 

With the introduction of the Linux-only scale-out systems with POWER8 technology, a new virtualization mechanism with the name **PowerKVM** is supported on Power Systems. **PowerKVM** is simply the port of KVM, the de facto open source virtualization mechanism. 

You are not able to run both PowerVM and PowerKVM hypervisors at the same machine.

...