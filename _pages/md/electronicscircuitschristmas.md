# 🎄 Karácsonyi dísz

<div style="text-align: right;"><h7>Posted: 2020-12-23 17:32</h7></div>

###### .

## 1. Bevezető

Az alábbi kis villogót még 2013-ban készítettem, de már akkor is azt terveztem, hogy majd jegyzetelek róla valamit. Most jött el az ideje. 

A 4017-es (vagy CD4017-es) integrált áramkör egy CMOS számláló, tíz kimeneti terminállal. Direkt ilyen LED-es apróságok elkészítésére találták ki. Az IC 13-as, 14-es és 15-ös lábán vezérelhetjük a számláló üzemmódját. Jelen esetben egy véletlenszerű villogtatás megvalósítására van szükség, amihez a 14-es láb kell, hogy kapja a négyszögjelet, a 13 és 15-ös lábak pedig a földre csatlakoznak. 

Az eredeti kapcsolási rajz, 5V-os feszültségstabilizátorral, négyszögjelgenerátorral: 

![alt_text](4017_christmas_circuit.png)

C1 = 72,15 µF; R1 = 0 Ω; R2 = 10 kΩ csinál szinte tökéletesen 1 Hz frenkvenciát
és teljesen egyforma L és H állapotokat az 555-ös IC 3-as lábán. 

Mivel 5V-os kapcsolóüzemű tápegységet nem nagy kunszt találni, így a fenti cucc feszültségstabilizáló része igazán elhagyható, ha készítünk egy egyik felén USB-s, másik felén bármilyen jack dugós csatlakozóval ellátott kábelt. 

## 2. Lehetséges megvalósítás

Az alkalmazott 555-ös és 4017-es IC-k ebben az esetben SMD-sek, az ízlés szerinti színű (nálam 5 zöld, 5 piros) LED-ek viszont hagymányos, fúrt lyukakhoz való kialakításúak, ezáltal tartós, viszonylag ütésálló készüléket alkothatunk meg. 

A C1-nek 22 µF-os kondenzátort alkalmaztam, az R1 pedig maradt a fenti ajánlás szerinti 10 kΩ-os. 

### 2.1 A panelterv

A panel magassága: 17 cm (ezt majd még pontosítom)

![alt_text](4017_christmas_panel.png)

### 2.2 A kész mű

![alt_text](4017_christmas_done.jpg)
