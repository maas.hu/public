# ![alt text](/_ui/img/icons/python_m.svg) Python

<div style="text-align: right;"><h7>Posted: 2023-02-27 22:28</h7></div>

###### .

![alt_text](python_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Environment Setup](#2)
3. [Python Scripting Basics](#3)
4. [Common Data Types](#4)

<a name="1"></a>
## 1. Introduction

Python is consistently ranked as one of the most popular programming languages. It first entered the **Top 10 in Q2 2002**, climbed into the **Top 3 by Q1 2014**, and has held the position of **Most Popular Programming Language since Q4 2018**, at least as of March 2025. [Here](https://www.youtube.com/watch?v=u8FgXP2z_KE) is one reference supporting this.

<img src="guido.jpg" class="right">

The language was originally created by the Dutch programmer **Guido van Rossum**. As a successor to the **ABC programming language**, the first `0.9.0` version of Python was released on **February 20, 1991**.

This article focuses on **Python 3**, commonly referred to as **Python3**.

Python3 is an **object-oriented scripting language**. As a **scripting language**, it uses an **interpreter**, meaning it executes code **immediately** rather than compiling it into binary form. Python3 supports **both dynamic and strong typing**, includes functional programming concepts such as *map*, *reduce*, and *filter*, and features a **clean, pseudo-code-like syntax**, where **whitespace is used for code blocks** instead of braces or keywords.

Its rapid rise in popularity can be attributed to several factors:

- While its primary platform remains **Unix-like systems** (especially Linux), Python is **open-source**, **cross-platform**, and **architecture-independent**.
- Its **readable, pseudo-code-like syntax** enforces proper **indentation**, making it easy to write and maintain.
- It is an excellent **teaching language**, originally designed for learning—similar to Pascal.
- It is widely used in **high-demand fields** such as **Data Science, DevOps, and Web Development**, thanks to its efficiency and flexibility.

### 1.1 Legacy vs. Present vs. Future

It is common for a system to have multiple Python versions installed, which can sometimes be confusing for users. For example:

<pre class="con">
<span class="psu">$</span> <span class="cmd">ls -l $(which python python2 python3 python3.{1..99} 2>/dev/null)</span>
-rwxr-xr-x 1 root root 28643224 Sep 20 09:56 /opt/Python3/bin/python3.12
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python -> python2.7
lrwxrwxrwx 1 root root        9 Nov 13 15:25 /usr/bin/python2 -> python2.7
lrwxrwxrwx 1 root root       10 Dec  4 23:40 /usr/bin/python3 -> python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 23:23 /usr/bin/python3.10
-rwxr-xr-x 1 root root    14448 Dec  4 23:40 /usr/bin/python3.11
-rwxr-xr-x 1 root root    14448 Dec  4 21:51 /usr/bin/python3.9
</pre>

In short, **Python 2.x is legacy**, while **Python 3.x is the standard and future of the language**. Many legacy applications still rely on Python 2.x, which is why its final release, **Python 2.7.18 (April 20, 2020)**, continues to receive **occasional security updates** (e.g., `2.7.18-28.1` in January 2023). However, no new features will be added.

For details on the transition from Python 2 to Python 3, refer to the official documentation: [What’s New In Python 3.0](https://docs.python.org/3/whatsnew/3.0.html). It's also worth noting that since Python **3.8.0**, new **minor versions** are released every **October**.

### 1.2 Leaving the System-Default Python Alone

Python can be installed in various ways. The most straightforward method is to use the official installers provided by the Python developers or, in the case of Linux distributions, install it via the package manager.

On Linux, a **default system-wide Python installation** is always present, as many core system components **depend on it**. For example, on **RHEL 9**, the `dnf` package manager (written in Python) has a **hardcoded shebang** pointing to `/usr/bin/python3.9`.

As documented in [Red Hat’s official guide](https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/installing_and_using_dynamic_programming_languages/assembly_introduction-to-python_installing-and-using-dynamic-programming-languages#assembly_introduction-to-python_installing-and-using-dynamic-programming-languages):

>Python 3.9 is the default Python implementation in RHEL 9. Python 3.9 is distributed in a non-modular `python3` RPM package in the BaseOS repository and is usually installed by default. Python 3.9 will be supported for the whole life cycle of RHEL 9.

>In RHEL 9, the **unversioned** form of the `python` command points to the default Python 3.9 version and it is an equivalent to the `python3` and `python3.9` commands. In RHEL 9, you cannot configure the unversioned command to point to a different version than Python 3.9.

Fortunately, major Linux distributions provide **multiple minor versions** of Python in their repositories, ensuring users have access to different versions. On RHEL 9-based systems, for example, users can install additional Python **3.9 components** as well as newer versions such as **Python 3.11 and Python 3.12**.


<a name="2"></a>
## 2. Environment Setup

Installing a relatively up-to-date version of Python3 is straightforward using the package manager. For example, on **openSUSE**:

<pre class="con">
<span class="psu">$</span> <span class="cmd">sudo zypper install python312</span>
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following 4 recommended packages were automatically selected:
  python312 python312-curses python312-dbm python312-pip

The following 12 NEW packages are going to be installed:
  libpython3_12-1_0 libpython3_12-1_0-x86-64-v3 python312 python312-base python312-base-x86-64-v3 python312-curses python312-dbm python312-gobject python312-gobject-cairo
  python312-pip python312-pycairo python312-x86-64-v3

12 new packages to install.

Package download size:    18.5 MiB

Package install size change:
              |      70.5 MiB  required by packages that will be installed
    70.5 MiB  |  -      0 B    released by packages that will be removed

Backend:  classic_rpmtrans
<span class="psu">Continue? [y/n/v/...? shows all options] (y):</span> <span class="cmd">y</span>
[...]
</pre>

To provide **more flexibility**, this article will guide you through **compiling the latest Python3 version from source**.

### 2.1 Installing Prerequisites for Compiling Python3 from Source

When compiling Python3 on **Red Hat** or its derivatives like **Rocky Linux**, you need to install the **"Development Tools"** package group. This includes essential build tools such as `gcc` and `make`, along with other necessary dependencies.

Below is the size of this package group on **Rocky Linux 9.5**:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf groupinstall -y "development tools"</span>
[...]
Install  364 Packages
Upgrade    3 Packages

Total download size: 274 M
[...]
Complete!
</pre>

In addition to the core development tools, the following **libraries and headers** are required for compiling Python3:

<pre class="con">
<span class="psr">#</span> <span class="cmd">dnf install -y bzip2-devel expat-devel libffi-devel ncurses-devel openssl-devel readline-devel sqlite-devel tk-devel atk-devel xz-devel zlib-devel</span>
[...]
Install  44 Packages
Upgrade   4 Packages

Total download size: 21 M
[...]
Complete!
</pre>

### 2.2 Downloading the Source Code

The official Python source code can be downloaded from [python.org](https://www.python.org). As of **March 2025**, the latest release is **Python 3.13.2**.

![alt_text](python_-_src_download.jpg)

Navigate to **[Downloads → Source Code](https://www.python.org/downloads/source/)**, **copy the link** of the *"Gzipped source tarball"*, and use `wget` to download it to your system:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cd /usr/src</span>

<span class="psr">#</span> <span class="cmd">wget https://www.python.org/ftp/python/3.13.2/Python-3.13.2.tgz</span>

<span class="psr">#</span> <span class="cmd">tar -xzf Python-3.13.2.tgz</span>

<span class="psr">#</span> <span class="cmd">cd Python-3.13.2</span>
</pre>

### 2.4 Configuring and Compiling Python3

There are multiple ways to install Python3 from source. The most common approach is using `altinstall`, which installs Python under `/usr/local`. However, this method does not allow precise control over the installation path.

A **more structured approach** is to specify a custom **installation prefix** (`--prefix`), ensuring that Python is installed in a dedicated directory like `/opt/Python3`. This approach provides **portability** and **does not interfere** with system-wide Python installations.

#### 2.4.1 Using a Custom `--prefix` Directory (Preferred)

This method installs Python into `/opt/Python3`, creating a **standalone Python environment**.

1. **Ensure `/opt/Python3` is free to use** (rename an existing instance if necessary). Below, `/opt` is empty:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">ls -la /opt</span>
    total 8
    drwxr-xr-x.  2 root root 4096 Nov  3 03:29 .
    dr-xr-xr-x. 19 root root 4096 Jan 30 16:26 ..
    </pre>
2. **Run the configuration script** with optimizations enabled and specify `/opt/Python3` as the target directory:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">./configure --enable-optimizations --prefix /opt/Python3</span>
    [...]
    config.status: creating pyconfig.h
    configure: creating Modules/Setup.local
    configure: creating Makefile
    </pre>
3. **Compile the source code** (this step can take several minutes):  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">make</span>
    Running code to generate profile data (this can take a while):
    [...]
    Checked 112 modules (33 built-in, 75 shared, 1 n/a on linux-x86_64, 0 disabled, 3 missing, 0 failed on import)
    make[1]: Leaving directory '/usr/src/Python-3.13.2'
    </pre>
4. **Install Python into `/opt/Python3`**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">make install</span>
    Creating directory /opt/Python3/bin
    Creating directory /opt/Python3/lib
    [...]
    Successfully installed pip-24.3.1
    </pre>
5. **Verify the installation**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">du -sh /opt/Python3</span>
    390M	/opt/Python3

    <span class="psr">#</span> <span class="cmd">/opt/Python3/bin/python3 --version</span>
    Python 3.13.2
    </pre>

To integrate `/opt/Python3` into your environment, add it to your `PATH`:

<pre class="con">
<span class="psu">$</span> <span class="cmd">grep PATH .bashrc</span>
export PATH=$HOME/bin:$PATH:/opt/Python3/bin
</pre>

#### 2.4.2 Using `altinstall` (Simpler Approach)

If you prefer a simpler method, `altinstall` ensures that Python is installed under `/usr/local`, keeping system-installed Python versions untouched.

<pre class="con">
<span class="psr">#</span> <span class="cmd">./configure --enable-optimizations</span>
[...]
config.status: creating pyconfig.h
configure: creating Modules/Setup.local
configure: creating Makefile

<span class="psr">#</span> <span class="cmd">make altinstall</span>
[...]

<span class="psr">#</span> <span class="cmd">python3.13 --version</span>
Python 3.13.2
</pre>

### 2.5 Configuring PIP for Your Regular User

To ensure PIP works properly, update your system's **secure_path** for `sudo` to include either your custom directory (`/opt/Python3/bin`) or `/usr/local/bin` (if you used `altinstall`).

1. **Modify `secure_path` in `/etc/sudoers`** (example for `/opt/Python3`):  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">vi /etc/sudoers</span>
    [...]
    Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/opt/Python3/bin
    [...]
    </pre>
2. **Upgrade PIP globally**:  
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">pip3.13 install --upgrade pip</span>
    Requirement already satisfied: pip in /opt/Python3/lib/python3.13/site-packages (24.3.1)
    Collecting pip
      Downloading pip-25.0.1-py3-none-any.whl.metadata (3.7 kB)
    Downloading pip-25.0.1-py3-none-any.whl (1.8 MB)
      ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.8/1.8 MB 9.9 MB/s eta 0:00:00
    Installing collected packages: pip
    Successfully installed pip-25.0.1
    </pre>

### 2.6 Packaging for Portability

If you used `--prefix`, you might want to **package the entire installation** (`/opt/Python3`) into a `.tar.gz` archive, making it portable across different systems:

<pre class="con">
<span class="psr">#</span> <span class="cmd">PKG="python-$(/opt/Python3/bin/python3 --version | awk '{print $2}')-lin-x64" ; \
  tar -cf $PKG.tar Python3 ; \
  sync ; \
  gzip $PKG.tar ; \
  du -h $PKG* | awk '{print $2" is ready, "$1"B"}'</span>
python-3.13.2-lin-x64.tar.gz is ready, 113MB
</pre>


<a name="3"></a>
## 3. Python Scripting Basics

![alt_text](python_-_baby.jpg)

Before diving too deep into writing programs, it's essential to get comfortable with the different ways you can interact with Python.

### 3.1 REPL for Rapid Experimentation

**REPL** stands for **Read, Evaluate, Print, Loop**, a concept shared with other scripting languages like Raku. When you invoke Python3 using any of its names (`python`, if properly linked, `python3`, or explicitly `python3.13`), you enter an interactive shell where you can execute Python commands directly:

<pre class="con">
<span class="psu">$</span> <span class="cmd">python3
Python 3.11.2 (main, Feb 28 2023, 18:30:49) [GCC 8.5.0 20210514 (Red Hat 8.5.0-16)] on linux
Type "help", "copyright", "credits" or "license" for more information.
<span class="psu">>>></span>
</pre>

Some initial tests, including the *mandatory* "Hello World," followed by exiting REPL:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">1 + 1</span>
2
<span class="psu">>>></span> <span class="cmd">None</span>
<span class="psu">>>></span> <span class="cmd">print("Hello World!")</span>
Hello World!
<span class="psu">>>></span> <span class="cmd">exit()</span>
</pre>

You can also use the `quit()` function or `Ctrl+D` to exit.

<div class="warn">
<p>In REPL, simply entering a string, number, or variable name prints its value to the screen. However, in actual source code, you must use the <code>print()</code> function explicitly.</p>
</div>

Another useful REPL shortcut is `Ctrl+L`, which clears the screen.

### 3.2 Python as an Invoked One-Liner

Similar to Perl and Raku, Python allows you to execute commands directly from the shell using the `-c` option:

<pre class="con">
<span class="psu">$</span> <span class="cmd">python3 -c 'print("Hello World!")'</span>
Hello World!

<span class="psu">$</span> <span class="cmd">python3 -c 'print(1+1)'</span>
2
</pre>

This is particularly useful for quick calculations or running simple scripts without creating a separate file.

### 3.3 Source Files as Python Programs

For anything beyond basic one-liners, Python code should be stored in source files.

Like any other script, if the interpreter is not specified via a shebang (`#!`), you must explicitly invoke Python to execute the script:

<pre class="con">
<span class="psu">$</span> <span class="cmd">vim hello.py</span>

<span class="psu">$</span> <span class="cmd">cat hello.py</span>
print("Hello World!")

<span class="psu">$</span> <span class="cmd">python3 hello.py</span>
Hello World!
</pre>

A more common approach is to include a **shebang** at the top of the file. You can specify the full path to `python3` (e.g., `/usr/bin/python3`), but a better choice is to use `env`, which dynamically resolves the interpreter:

```
#!/usr/bin/env python3

print("Hello World!")
```

After adding a shebang, make the script executable:

<pre class="con">
<span class="psu">$</span> <span class="cmd">chmod +x hello.py</span>

<span class="psu">$</span> <span class="cmd">./hello.py</span>
Hello World!
</pre>

At this point, your script functions like a standalone program. You can even remove the `.py` extension if desired.

<div class="info">
<p>As your learning progresses to conditionals and loops, it's recommended to use standalone Python scripts and execute them within an integrated terminal, such as in <b>Visual Studio Code</b>, for better organization and debugging.</p>
</div>

### 3.4 Comments in Python Code

Python uses the `#` symbol for comments, just like Unix shells.

Unlike some languages, Python does not have built-in block comments. While unassigned multi-line strings (triple quotes) are sometimes used as pseudo-comments, they are technically string literals and consume memory. Avoid using them as block comments:

```
# This is a proper comment
print("Hello World!")

"""
This is an unassigned multi-line string,
which is not so nice thing to use as block comment.
Don't do this!
"""
```

So far, we've primarily used the `print()` function.

<div class="info">
<p>For a complete list of built-in functions in Python3, refer to the official documentation: <a href="https://docs.python.org/3/library/functions.html">Built-in Functions</a>.</p>
</div>


<a name="4"></a>
## 4. Common Data Types

![alt_text](python_-_data_types.svg)

There are multiple ways to classify, compare, and categorize Python 3's data types. This section presents one such interpretation. For a more detailed explanation, along with additional examples, refer to the [official Python documentation](https://docs.python.org/3/tutorial/datastructures.html#), which is regularly updated.

### 4.1 Strings

"Python does not differentiate between single (`'`) and double (`"`) quotation marks when defining strings. For example, `"Hello World!"` and `'Hello World!'` are treated identically. This applies to multi-line strings as well. The only requirement is that the opening and closing quotes must match.

#### 4.1.1 Concatenating Strings

Strings in Python can be **concatenated** using the `+` operator, which **joins** two or more strings together into a single string. This is useful when constructing dynamic messages, formatting output, or handling user input.

For example, in the Python REPL:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">'pass' + 'word'</span>
'password'
</pre>

Here, the two separate string literals `'pass'` and `'word'` are combined into a single string `'password'`. This operation does not modify either of the original strings but instead creates a **new** string that holds the concatenated result.

#### 4.1.2 Multiplying Strings

Python provides a convenient way to repeat strings using the `*` operator. For example, here's how you can multiply a string by `4`, which results in the string being repeated four times consecutively:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">'Ha' * 4</span>
'HaHaHaHa'
</pre>

This feature is particularly useful for formatting, generating repeated patterns, or initializing strings dynamically in loops, without requiring explicit loops for the string multiplication itself.

#### 4.1.3 Methods of Strings

Like everything in Python, a string is an **object**, meaning it has **attributes** (such as its value) and **methods** (functions that operate on the object). Python provides many built-in methods for working with strings.  

One such method is `find()`, which searches for a specified substring within a string and returns the **index** (position) of the first occurrence. If the substring is **not found**, it returns `-1`:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">'parakeet'.find('r')</span>
2
<span class="psu">>>></span> <span class="cmd">'parakeet'.find('s')</span>
-1
</pre>

- In the first case, the letter `'r'` appears at **index 2** (Python uses **zero-based indexing**, meaning the first character is at position `0`).
- In the second case, `'s'` is not present in the string, so `find()` returns `-1`.  

Another commonly used method is `lower()`, which **converts all uppercase letters in a string to lowercase**:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">'PaRaKeEt'.lower()</span>
'parakeet'
</pre>

This transformation is useful for **case-insensitive** comparisons, text normalization, and standardizing input data before processing. Since `lower()` does not modify the original string but instead returns a **new** lowercase version, it must be used in an assignment if the change needs to be stored.

#### 4.1.4 Control Characters  

Control characters are special characters that do not represent visible symbols but instead control how text is displayed or formatted. In Python, these characters are commonly used for **formatting output**, **structuring text**, or **handling special cases in strings**.  

One such control character is `\n`, which represents a **newline (line break)**. When included in a string, it moves the cursor to the next line when printed:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print('Hello,\nWorld!')</span>
Hello,
World!
</pre>

Here, the `\n` forces the text following it to appear on a **new line**, breaking `"Hello, World!"` into two separate lines.  

Another useful control character is `\t`, which represents a **tab (horizontal indentation)**. This is often used for formatting text in columns:  

<pre>
#!/usr/bin/env python3

print('Name\t\tPosition')
print('Jakab Gipsz\tDirector')
print('John Doe\tOperator')
</pre>

After saving this script as `hello` and making it executable, running it produces output where names and positions are **aligned into columns**:  

<pre class="con">
<span class="psu">$</span> <span class="cmd">hello</span>
Name        Position
Jakab Gipsz Director
John Doe    Operator
</pre>

The **escape character** (`\`) is another important feature in Python strings. It is used to include special characters that would otherwise be interpreted **literally** or cause syntax errors. For example, to include a **single quote** inside a single-quoted string, the quote must be **escaped** using `\`:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print('Your\'s is better')</span>
Your's is better
</pre>

Without the backslash (`\`), Python would interpret the second **single quote** as the end of the string, causing a **syntax error**. Using `\` ensures that the apostrophe is treated as a **literal character** within the string.  

Control characters like `\n`, `\t`, and `\` are essential for **text formatting**, **data structuring**, and **avoiding syntax conflicts** in Python string handling.

### 4.2 Numbers (`int` and `float`)

Python3 supports **numeric data types** such as **integers (`int`)** and **floating-point numbers (`float`)**, which are used for mathematical operations and computations. Integers represent whole numbers without decimals, while floats handle numbers with fractional parts. 

Python provides various **arithmetic operators** and **functions** to perform **calculations**, making numerical operations straightforward and flexible.

#### 4.2.1 Arithmetic Operators  

Python provides a range of arithmetic operators for performing basic mathematical calculations on integers and floating-point numbers. Unlike some languages, Python allows whitespace around operators, making expressions more readable. It also automatically preserves floating-point precision when performing operations involving decimals.

- **Addition (`+`)** adds numbers together.
- **Subtraction (`-`)** subtracts the second number from the first.
- **Multiplication (`*`)** multiplies numbers.
- **Division (`/`)** returns the quotient as a floating-point number, even if both operands are integers.
- **Floor division (`//`)** returns the quotient rounded down to the nearest integer.
- **Modulus (`%`)** returns the remainder of a division operation.
- **Exponentiation (`**`)** raises a number to the power of another.

Python ensures that operations involving floats maintain their decimal places in the result. For example, `2.3 + 2.7` correctly returns `5.0` instead of `5`. Similarly, floor division (`//`) provides an integer result, while modulus (`%`) helps determine divisibility.

Some examples:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">2+2</span>
4
<span class="psu">>>></span> <span class="cmd">2 + 2</span>
4
<span class="psu">>>></span> <span class="cmd">2 * -5</span>
-10
<span class="psu">>>></span> <span class="cmd">2.3 + 2.7</span>
5.0
<span class="psu">>>></span> <span class="cmd">2.35 + 2.65</span>
5.0
<span class="psu">>>></span> <span class="cmd">10 - 3</span>
7
<span class="psu">>>></span> <span class="cmd">4 * 7</span>
28
<span class="psu">>>></span> <span class="cmd">9 / 2</span>
4.5
<span class="psu">>>></span> <span class="cmd">9 // 2</span>
4
<span class="psu">>>></span> <span class="cmd">9 % 2</span>
1
<span class="psu">>>></span> <span class="cmd">2 ** 3</span>
8
</pre>

#### 4.2.2 Converting Between String and Number Data Types  

Python provides built-in functions to easily convert between strings and numeric data types. This is useful when dealing with user input (which is always received as a string), performing mathematical operations, or formatting output.

- **`str()`**: Converts a given value to a string. This is useful for displaying numbers as text.
- **`int()`**: Converts a given value to an integer. It can accept a string containing a numeric value or a floating-point number (truncating the decimal part).
- **`float()`**: Converts a given value to a floating-point number. It can take a string representation of a float or an integer.
- **`type()`**: Returns the type of a given value, allowing verification before or after conversions.

In the example, `int("1")` correctly converts a numeric string to an integer, while `float("1.1")` converts a string to a float. Similarly, `str(1.1)` converts a number to a string, which can then be manipulated as text.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">type(3)</span>
&lt;class 'int'&gt;
<span class="psu">>>></span> <span class="cmd">type("parakeet")</span>
&lt;class 'str'&gt;
<span class="psu">>>></span> <span class="cmd">int</span>
&lt;class 'int'&gt;
<span class="psu">>>></span> <span class="cmd">int("1")</span>
1
<span class="psu">>>></span> <span class="cmd">int(1.0)</span>
1
<span class="psu">>>></span> <span class="cmd">float("1.1")</span>
1.1
<span class="psu">>>></span> <span class="cmd">float(1)</span>
1.0
<span class="psu">>>></span> <span class="cmd">str(1.1)</span>
'1.1'
<span class="psu">>>></span> <span class="cmd">1.1 * 3</span>
3.3000000000000003
<span class="psu">>>></span> <span class="cmd">str(1.1) * 3</span>
'1.11.11.1'
</pre>

When performing operations after conversion, note that `1.1 * 3` results in `3.3000000000000003` due to floating-point precision limitations. Meanwhile, `str(1.1) * 3` repeats the string representation (`"1.1"`) three times, producing `"1.11.11.1"`. This behavior highlights the importance of ensuring correct data types before performing operations.

### 4.3 Booleans and `None`

Booleans and `None` are fundamental data types in Python that represent truth values and the absence of a value, respectively. The Boolean type (`bool`) has only two possible values: `True` and `False`, which are commonly used in conditions and logical operations.

The special `None` value, belonging to the `NoneType`, signifies "nothing" or "no value," often used as a placeholder or default return value in functions.

#### 4.3.1 Booleans

In Python, the Boolean data type, `bool`, can have only two values: `True` or `False`:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">True</span>
True
<span class="psu">>>></span> <span class="cmd">False</span>
False
<span class="psu">>>></span> <span class="cmd">type(True)</span>
&lt;class 'bool'&gt;
</pre>

The `bool()` function determines whether a given value is considered **"truthy"** (`True`) or **"falsy"** (`False`), where **non-empty values** evaluate to `True`, and **empty values** (such as `0`, `None`, `""`, `[]`, `{}`, etc.) evaluate to `False`:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">bool()</span>
False
<span class="psu">>>></span> <span class="cmd">bool("")</span>
False
<span class="psu">>>></span> <span class="cmd">bool([])</span>
False
<span class="psu">>>></span> <span class="cmd">bool({})</span>
False
<span class="psu">>>></span> <span class="cmd">bool("anything")</span>
True
<span class="psu">>>></span> <span class="cmd">bool([1])</span>
True
</pre>

#### 4.3.2 The `NoneType`

Python's `None` is equivalent to `null` in languages like JavaScript or `nil` in Ruby—it represents the absence of a value or "nothingness." It belongs to the unique type `NoneType` and is often used to indicate missing or undefined values. 

Notably, when `None` is entered in the REPL, it doesn't produce any visible output, emphasizing its role as a placeholder for "nothing":

<pre class="con">
<span class="psu">>>></span> <span class="cmd">type(None)</span>
&lt;class 'NoneType'&gt;
<span class="psu">>>></span> <span class="cmd">None</span>
</pre>

### 4.4 Variables and Constants

Variables act as **placeholders** for values, functions, and comparisons. In Python, like everything else, variables are objects with their own state and, in some cases, associated methods.

Assigning a value to a variable for the first time or updating it is called **assignment**. Both the programmer and functions within the code can perform assignments and reassignments as needed.

#### 4.4.1 Assignments

To store a value in a variable, use the **assignment** operator (`=`). This binds the **value** on the right side of `=` to the **variable** on the left.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = "Hello World!"</span>

<span class="psu">>>></span> <span class="cmd">myVar</span>
'Hello World!'
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
Hello World!
</pre>

Variables in Python are **dynamic**, meaning they can be reassigned to values of different types at any time:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = "Hello World!"</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
Hello World!
<span class="psu">>>></span> <span class="cmd">myVar = 1</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
1
</pre>

You can also **assign one variable to another**. When this happens, the assigned value is copied at the moment of assignment. Changes to the original variable afterward do not affect the copied value:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = 123</span>
<span class="psu">>>></span> <span class="cmd">myVar</span>
123
<span class="psu">>>></span> <span class="cmd">myInt = myVar</span>
<span class="psu">>>></span> <span class="cmd">myVar = "Hello"</span>
<span class="psu">>>></span> <span class="cmd">myInt</span>
123
</pre>

As shown above, even though `myVar` was reassigned from `123` to `"Hello"`, the variable `myInt` retained its original value of `123` because it was assigned before the change.

#### 4.4.2 Mixing Strings and Variables in the `print()` Function

Python provides multiple ways to insert variables into a string when using the `print()` function. One approach is to separate variables and string literals with a `,`, which automatically adds a space between them. Another, more modern and flexible method is **f-string formatting**, which allows variables to be embedded directly within a string using `{}`.

Both of the following examples produce the same output:

<pre>
[...]
    print(name, 'is', age, 'years old.')
[...]
    print(f'{name} is {age} years old.')
[...]
</pre>

The first example separates the variables with commas, ensuring proper spacing between elements. The second example uses an **f-string**, which provides a more readable and concise way to format text dynamically.

#### 4.4.3 Extra Operations with Variables

Python allows **augmented assignment operators**, which combine an operation with an assignment. The `+=` operator adds a value to an existing variable and updates it in place. It works as a shorthand for combining the standard assignment (`=`) with an arithmetic or concatenation operation.

For strings, `+=` is used for **concatenation**, appending the new string to the existing one:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = "Hello World!"</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
Hello World!
<span class="psu">>>></span> <span class="cmd">myVar += " Bye!"</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
Hello World! Bye!
<span class="psu">>>></span> <span class="cmd">myVar = myVar + " Bye!"</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
Hello World! Bye! Bye!
</pre>

For numbers, `+=` increases the numeric value, just as you would expect:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = 1</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
1
<span class="psu">>>></span> <span class="cmd">myVar += 1</span>
<span class="psu">>>></span> <span class="cmd">print(myVar)</span>
2
</pre>

Similarly, Python supports other augmented assignment operators for numbers, such as `*=`, `-=`, and `/=`, which perform multiplication, subtraction, and division **while updating the variable in place**:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myVar = 123</span>
<span class="psu">>>></span> <span class="cmd">myVar *= 123</span>
<span class="psu">>>></span> <span class="cmd">myVar</span>
15129
<span class="psu">>>></span> <span class="cmd">myVar -= 123</span>
<span class="psu">>>></span> <span class="cmd">myVar</span>
15006
<span class="psu">>>></span> <span class="cmd">myVar /= 123</span>
<span class="psu">>>></span> <span class="cmd">myVar</span>
122.0
</pre>

These operators provide a more concise and readable way to update variables while performing arithmetic or concatenation operations.

#### 4.4.4 Constants

A **constant** is similar to a variable, but its assigned value is **intended to remain unchanged** throughout the execution of a program. In many programming languages, constants are explicitly enforced, preventing reassignment after their initial definition. However, the necessity of constants has been debated throughout the history of programming, which is why some languages provide explicit support for them, while others, like Python, **do not enforce them at all**.

##### Benefits of Using Constants

Using constants can improve code quality in several ways:

- They prevent accidental changes to critical values.
- They improve readability by signaling to other developers that a value is **meant to stay the same** throughout the program.
- They can reduce debugging effort and minimize the risk of introducing errors due to unintended value modifications.

##### Constants in Python

Unlike some other languages, **Python does not have a built-in mechanism to define true constants**. Instead, the convention in the Python community is to use **uppercase names** for variables that are meant to be treated as constants. However, this is purely a naming convention; the Python interpreter does not enforce immutability, meaning a "constant" can still be reassigned.

Python does, however, include several [built-in constants](https://docs.python.org/3/library/constants.html), such as `False`, `True`, and the special placeholder object `Ellipsis`.

##### Defining Constants in Python

A common approach to defining constants in Python is to place them in a **separate module** and import them into the main program. This helps organize constants separately from the main logic, making them easier to manage.

Example: Creating a `const.py` module to store constants:

<pre class="con">
<span class="psu">$</span> <span class="cmd">cat > const.py << EOF</span>
<span class="psu">></span> <span class="cmd"># declare constants </span>
<span class="psu">></span> <span class="cmd">PI = 3.14</span>
<span class="psu">></span> <span class="cmd">EOF</span>

<span class="psu">$</span> <span class="cmd">python3</span>
Python 3.13.2 (main, Mar  7 2025, 18:44:03) [GCC 11.5.0 20240719 (Red Hat 11.5.0-5)] on linux
Type "help", "copyright", "credits" or "license" for more information.
<span class="psu">>>></span> <span class="cmd">import const</span>
<span class="psu">>>></span> <span class="cmd">print("The value of Pi is", const.PI)</span>
The value of Pi is 3.14
</pre>

By storing constants in a separate file, they are logically separated from the main program, making it **clearer and easier to manage**.

##### Environment Variables as Constants

Another way to define constants is by **storing them as environment variables**. This approach is particularly useful when constants are dependent on the system configuration or need to be changed without modifying the source code.

Example: Using an environment variable in Python:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">import os</span>
<span class="psu">>>></span> <span class="cmd">os.getenv('USER')</span>
'gjakab'
</pre>

This method is especially useful for **storing configuration values**, such as API keys or file paths, that should not be hardcoded in the script.

### 4.5 Lists

A **list** is the most commonly used **sequence type** in Python. It contains multiple elements, enclosed within square brackets `[]` and separated by commas `,`.

In many other programming languages, similar data structures are called **arrays**. However, unlike traditional arrays, Python lists are **not restricted to a single data type**—they can store elements of different types. This flexibility is one of the reasons they are referred to as **lists** rather than arrays in Python.

#### 4.5.1 Basic List Operations

Creating a simple list and referencing it as a whole:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList = [1, 2, 3, 4, 5]
<span class="psu">>>></span> <span class="cmd">myList
[1, 2, 3, 4, 5]
</pre>

Lists in Python are indexed, meaning each element has a unique position, starting from `0`. Since this `myList` contains **five elements**, the last element is at index `4`.

The `len` function can be used to determine the length of the list. Additionally, Python allows **negative indexing**, where `-1` refers to the last element, `-2` to the second-to-last, and so on:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList[0]
1
<span class="psu">>>></span> <span class="cmd">len(myList)
5
<span class="psu">>>></span> <span class="cmd">myList[4]
5
<span class="psu">>>></span> <span class="cmd">myList[-1]
5
<span class="psu">>>></span> <span class="cmd">myList[-3]
3
</pre>

Negative indexing is particularly useful when working with lists dynamically, allowing easy access to elements from the end without needing to know the exact length.

#### 4.5.2 List Slicing

Slicing allows extracting portions of a list using the syntax:  

```
list[start:stop:step]
```

- `start` (optional): The index to begin slicing (inclusive). Defaults to `0`.  
- `stop` (required): The index to stop slicing (exclusive).  
- `step` (optional): The interval between elements. Defaults to `1`.  

Examples of list slicing:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList[0:3]</span>     <span class="cmt"># Elements from index 0 to 2 (exclusive)</span>
[1, 2, 3]
<span class="psu">>>></span> <span class="cmd">myList[2:]</span>      <span class="cmt"># Elements from index 2 to the end</span>
[3, 4, 5]
<span class="psu">>>></span> <span class="cmd">myList[:3]</span>      <span class="cmt"># First three elements (same as myList[0:3])</span>
[1, 2, 3]
<span class="psu">>>></span> <span class="cmd">myList[0:-1:2]</span>  <span class="cmt"># Every second element, excluding the last</span>
[1, 3]
<span class="psu">>>></span> <span class="cmd">myList[0::2]</span>    <span class="cmt"># Every second element, including the last</span>
[1, 3, 5]
</pre>

Slicing is a powerful feature that simplifies extracting and manipulating subsets of a list without using loops.

#### 4.5.3 Modifying Elements in a List

Changing an **element's value in a list** is straightforward; it works just like variable assignment:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList[0] = "a"</span>  <span class="cmt"># Modify the first element</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 2, 3, 4, 5]
</pre>

You can also modify **multiple elements at once** using slicing:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList[1:3] = ["b", "c"]</span>  <span class="cmt"># Replace elements at index 1 and 2</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 'b', 'c', 4, 5, 6, 7, 8, 9, 10]
</pre>

If the assigned list has more elements than the slice references, the original list expands to accommodate them. Similarly, if fewer elements are assigned, the list shrinks accordingly.

#### 4.5.4 Adding and Removing Elements in a List

The `append()` method **adds a single element** to the end of a list:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList.append(6)</span>
<span class="psu">>>></span> <span class="cmd">myList.append(7)</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 2, 3, 4, 5, 6, 7]
</pre>

Alternatively, you can use **concatenation (`+=`)** to extend a list with multiple elements:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList += [8, 9, 10]</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 2, 3, 4, 5, 6, 7, 8, 9, 10]
</pre>

If you need to **insert elements** at a specific position rather than appending them to the end, you can use slicing. For example, inserting at the beginning:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList</span>
[2, 3, 4, 5]
<span class="psu">>>></span> <span class="cmd">myList[:0] += [1]</span>  <span class="cmt"># Insert 1 at the start</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 3, 4, 5]
</pre>

The `pop()` method **removes the last element** by default and **returns** the removed value:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 3, 4, 5]
<span class="psu">>>></span> <span class="cmd">myList.pop()</span>
5
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 3, 4]
</pre>

You can also **remove an element from a specific index** using `pop(index)`, as long as the index is within range:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 3, 4]
<span class="psu">>>></span> <span class="cmd">myList.pop(0)</span>  <span class="cmt"># Remove first element</span>
1
<span class="psu">>>></span> <span class="cmd">myList</span>
[2, 3, 4]
</pre>

The `remove()` method **deletes an element by value** instead of index. If the specified value does not exist, a `ValueError` is raised:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 3, 4, 5]
<span class="psu">>>></span> <span class="cmd">myList.remove(3)</span>  <span class="cmt"># Remove the element with value 3</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
[1, 2, 4, 5]

<span class="psu">>>></span> <span class="cmd">myList.remove(6)</span>  <span class="cmt"># Trying to remove a non-existent value</span>
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: list.remove(x): x not in list
</pre>

Another way to **remove multiple elements** is by **assigning an empty list to a slice** of the original list:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 'b', 'c', 4, 5, 6, 7, 8, 9, 10]
<span class="psu">>>></span> <span class="cmd">myList[3:] = []</span>  <span class="cmt"># Remove all elements from index 3 onward</span>
<span class="psu">>>></span> <span class="cmd">myList</span>
['a', 'b', 'c']
</pre>

#### 4.5.5 List Comprehensions

The [official documentation](https://docs.python.org/3/tutorial/datastructures.html?highlight=data%20structures#list-comprehensions) describes how to populate a list efficiently, not only using a standard `for` loop but also by embedding the logic directly within the list declaration. This technique is called **list comprehension**.

The general syntax for list comprehension is:

```
[new_item for item in iterable]
```

It allows creating lists in a more concise and readable manner. Let's compare the traditional approach with list comprehension using an example.

### Creating a List of Squares

Using a **standard `for` loop**, elements are appended to a list one by one:  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">squares = []</span>
<span class="psu">>>></span> <span class="cmd">for x in range(10):</span>
<span class="psu">...</span> <span class="cmd">    squares.append(x**2)</span>
<span class="psu">...</span> <span class="cmd"></span>
<span class="psu">>>></span> <span class="cmd">print(squares)</span>
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
</pre>

Using **list comprehension**, the same result is achieved in a single, more readable line:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">squares = [x**2 for x in range(10)]</span>
<span class="psu">>>></span> <span class="cmd">print(squares)</span>
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
</pre>

List comprehensions make code more **concise**, **efficient**, and **easier to read**, especially when dealing with **transformations or filtering elements** in a list.

#### 4.5.6 Sorting Lists

Sorting elements in a list is a common operation in Python. The built-in `sorted()` function allows sorting both **strings (alphabetically)** and **numbers (numerically)**.  

By default, `sorted()` sorts elements in **ascending order**, but it can also be used to **reverse the order** by setting the `reverse=True` parameter.

##### Examples

Sorting a list of words **alphabetically** and a list of numbers **numerically**:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">fruits = ['nut', 'apple', 'plum']</span>
<span class="psu">>>></span> <span class="cmd">ids = [3, 8, 1]</span>

<span class="psu">>>></span> <span class="cmd">print(sorted(fruits))</span>
['apple', 'nut', 'plum']

<span class="psu">>>></span> <span class="cmd">print(sorted(ids))</span>
[1, 3, 8]
</pre>

Sorting in **descending order**:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print(sorted(ids, reverse=True))</span>
[8, 3, 1]
</pre>

The `sorted()` function **does not modify the original list**; instead, it returns a new sorted list. If you want to sort a list in place, you can use the `sort()` method:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">ids.sort()</span>
<span class="psu">>>></span> <span class="cmd">print(ids)</span>
[1, 3, 8]
</pre>

### 4.6 Tuples  

A **tuple** is an **immutable sequence type**, meaning its contents cannot be modified after creation. It is similar to a **list**, but unlike lists, tuples **do not provide methods for adding, removing, or changing elements**.

Tuples are often used when you need a **fixed collection of items** or when data should remain **unchanged throughout the program**.

#### 4.6.1 Declaring and Unpacking Tuples  

Tuples are created by enclosing elements in **parentheses `()`**, and elements are separated by **commas**.

Tuples also support **unpacking**, allowing their values to be assigned to multiple variables in a single step.

##### Example: Creating and unpacking tuples  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">point = (2.0, 3.0)</span>
<span class="psu">>>></span> <span class="cmd">point</span>
(2.0, 3.0)
<span class="psu">>>></span> <span class="cmd">point_3d = point + (4.0,)</span>  <span class="cmt"># Adding a new element to a tuple</span>
<span class="psu">>>></span> <span class="cmd">point_3d</span>
(2.0, 3.0, 4.0)
<span class="psu">>>></span> <span class="cmd">x, y, z = point_3d</span>         <span class="cmt"># Unpacking the tuple into separate variables</span>
<span class="psu">>>></span> <span class="cmd">x</span>
2.0
<span class="psu">>>></span> <span class="cmd">y</span>
3.0
<span class="psu">>>></span> <span class="cmd">z</span>
4.0
</pre>

#### 4.6.2 Using Tuples for String Formatting  

One common use of tuples is **formatting strings**, where they ensure a specific number of values appear in a given format.  

##### Example: String formatting with tuples  

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print ("My name is: %s %s" % ("Jakab", "Gipsz"))</span>
My name is: Jakab Gipsz
<span class="psu">>>></span> <span class="cmd">print ("My first name, last name and weight are: %s %s, %f kg" % ("Jakab", "Gipsz", 77))</span>
My first name, last name and weight are: Jakab Gipsz, 77.000000 kg
</pre>
<p></p>
<div class="info">
<p>While the <code>%</code> operator is still used, the <strong><code>.format()</code> method</strong> and <strong>f-strings</strong> (introduced in Python 3.6) are often preferred for better readability and flexibility.</p>
</div>

### 4.7 Sets

Python provides a built-in **set** data type, which represents an **unordered collection of unique elements**.

Unlike lists and tuples, sets do not allow **duplicate values**, making them useful for **membership testing** and **removing duplicate entries**.

In addition to basic usage, sets support **mathematical operations**, such as **union, intersection, difference, and symmetric difference**, which makes them highly useful for **handling distinct elements in datasets**.

#### 4.7.1 Creating Sets

Sets can be created using either **curly braces `{}`** or the `set()` function. Unlike lists or tuples, the order of elements in a set is **not guaranteed**. Any duplicate values are **automatically removed** upon creation.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}</span>
<span class="psu">>>></span> <span class="cmd">basket</span>
{'orange', 'banana', 'pear', 'apple'}  <span class="cmt"># Duplicates are removed</span>
</pre>

#### 4.7.2 Set Behavior Compared to Lists

While sets share some similarities with lists, they have key differences:

- **Unordered**: Sets do not maintain the order of elements.
- **Unique elements**: Duplicate values are automatically removed.
- **No indexing**: Unlike lists, elements in a set cannot be accessed by index (e.g., `set[0]` is invalid).

For cases where **order and duplicates matter**, a **list** or **tuple** should be used instead.

Here’s a **short demonstration** of essential **set operations** in Python. These examples showcase **union, intersection, difference, and symmetric difference**, which are some of the most commonly used operations when working with sets.

#### 4.7.3 Set Operations

Python sets support **various mathematical operations** that help compare and manipulate distinct elements efficiently.

##### **Union (`|` or `.union()`)**

Combines elements from both sets, removing duplicates.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">A = {'apple', 'banana', 'cherry'}</span>
<span class="psu">>>></span> <span class="cmd">B = {'banana', 'kiwi', 'grape'}</span>
<span class="psu">>>></span> <span class="cmd">print(A | B)</span>
{'apple', 'banana', 'cherry', 'kiwi', 'grape'}
<span class="psu">>>></span> <span class="cmd">print(A.union(B))</span>
{'apple', 'banana', 'cherry', 'kiwi', 'grape'}
</pre>

##### **Intersection (`&` or `.intersection()`)**  

Finds common elements between sets.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print(A & B)</span>
{'banana'}
<span class="psu">>>></span> <span class="cmd">print(A.intersection(B))</span>
{'banana'}
</pre>

##### **Difference (`-` or `.difference()`)**  

Finds elements in **A** that are **not** in **B**.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print(A - B)</span>
{'apple', 'cherry'}
<span class="psu">>>></span> <span class="cmd">print(A.difference(B))</span>
{'apple', 'cherry'}
</pre>

##### **Symmetric Difference (`^` or `.symmetric_difference()`)**  

Finds elements **not** present in **both** sets.

<pre class="con">
<span class="psu">>>></span> <span class="cmd">print(A ^ B)</span>
{'apple', 'cherry', 'kiwi', 'grape'}
<span class="psu">>>></span> <span class="cmd">print(A.symmetric_difference(B))</span>
{'apple', 'cherry', 'kiwi', 'grape'}
</pre>

##### **Why Use Set Operations?**

- Efficient way to **compare large datasets**
- **Removes duplicates** automatically
- Provides **mathematical-like operations** on collections

### 4.8 Dictionaries

A **dictionary** (`dict`) is another sequence-like type, but it is specifically used for storing **key-value pairs**, where **keys must be unique**. It closely resembles the standard **JSON** format.

One key difference between Python 2 and Python 3 is that **in Python 3, dictionaries maintain the insertion order of key-value pairs**, whereas in Python 2, the order was not guaranteed.

#### 4.8.1 Basic Usage of Dictionaries

Dictionaries allow storing and retrieving values using unique keys. They also provide **methods for modifying and deleting elements**.

Below is an example of basic dictionary operations, including the **dangerous** `del` function, which should be used cautiously, and the safer alternative, the `pop()` method:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">ages = { 'Michael': 43, 'George': 40, 'John': 37 }</span>
<span class="psu">>>></span> <span class="cmd">ages</span>
{'Michael': 43, 'George': 40, 'John': 37}
<span class="psu">>>></span> <span class="cmd">ages['George']</span>
40
<span class="psu">>>></span> <span class="cmd">ages['Kevin']</span>
Traceback (most recent call last):
  File "&lt;stdin&gt;", line 1, in <module>
KeyError: 'Kevin'
<span class="psu">>>></span> <span class="cmd">ages['Kevin'] = 34</span>
<span class="psu">>>></span> <span class="cmd">ages</span>
{'Michael': 43, 'George': 40, 'John': 37, 'Kevin': 34}
<span class="psu">>>></span> <span class="cmd">del ages['Kevin']</span>
<span class="psu">>>></span> <span class="cmd">ages</span>
{'Michael': 43, 'George': 40, 'John': 37}
<span class="psu">>>></span> <span class="cmd">ages.pop('John')</span>
37
<span class="psu">>>></span> <span class="cmd">ages</span>
{'Michael': 43, 'George': 40}
</pre>

#### 4.8.2 Advanced Queries on Elements of a Dictionary

Dictionaries provide methods to retrieve only the keys or values. The `keys()` method returns a **dict_keys** object, while the `values()` method returns a **dict_values** object. These can be easily converted into lists using the `list()` function if needed:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">ages.keys()</span>
dict_keys(['Michael', 'George'])
<span class="psu">>>></span> <span class="cmd">list(ages.keys())</span>
['Michael', 'George']
<span class="psu">>>></span> <span class="cmd">ages.values()</span>
dict_values([43, 40])
<span class="psu">>>></span> <span class="cmd">list(ages.values())</span>
[43, 40]
</pre>

#### 4.8.3 Using the `dict` Function

The `dict()` function provides an alternative way to create dictionaries. 

A basic example:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">weights = dict(Michael=77, George=78, John=79)</span>
<span class="psu">>>></span> <span class="cmd">weights</span>
{'Michael': 77, 'George': 78, 'John': 79}
</pre>

A more advanced approach allows defining a dictionary from a list of key-value tuples. This is particularly useful when working with dynamically generated data or when iterating over structured datasets:

<pre class="con">
<span class="psu">>>></span> <span class="cmd">colors = dict([('Michael', 'blue'), ('George', 'green'), ('John', 'brown')])</span>
<span class="psu">>>></span> <span class="cmd">colors</span>
{'Michael': 'blue', 'George': 'green', 'John': 'brown'}
</pre>
<p></p>
<div class="warn">
<p>Dictionary keys must be <strong>immutable</strong>, meaning lists (e.g., <code>[1, 2, 3]</code>) <strong>cannot</strong> be used as keys. Instead, <strong>use tuples if you need a compound key</strong>.
</div>

*To be continued...*

<!-- continue the review from here -->
