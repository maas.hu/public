# ![alt text](/_ui/img/icons/kubernetes_m.svg) Kubernetes

<div style="text-align: right;"><h7>Posted: 2021-11-02 17:34</h7></div>
###### .

## Table of Contents

1. [Introduction](#1)
2. [Components of Kubernetes](#2)
3. [Installation](#3)
4. [Application Deployment Basics](#4)
5. [Pods and Services Under the Cluster's Key Components](#5)
6. [Labels, Selectors, Probes and Secrets](#7)
7. [Kubernetes Dashboard (Web UI)](#7)
8. [Installing and Testing K8s Enterprise Components](#8)


<a name="1"></a>
## 1. Introduction

![alt_text](barbossa_with_helm.jpg)

**Kubernetes**, or as it's mostly referred shortly, **K8s**, was originally developed by **Google**, written in **Go**, mostly influenced by Google's **Borg** cluster manager solution and initially announced by Google in **mid-2014**. Shortly after in 2015, the **The Cloud Native Computing Foundation ([CNCF](https://cncf.io))** was founded, and this organization is now the maintainer of the software. 

From its main functionality's point of view, in short, Kubernetes is a complex **container orchastration tool** which fits into many modern expectations, supports all those IT industry standards when...

- the server side infrastructure involves **containers**
- **high availabilty** is a key requirement
- quick and preferably automatic **scalability** is highly recommended

Kubernetes is very flexible when choosing the type of the actual **servers** that we want to integrate into our Kubernetes cluster. The set of servers can include **physical servers** (either enterprise-level, x86 servers or cheap SBCs on aarch64 platform or some other types), **VMs** under our **on-premise** hypervisors, or VMs, running on **cloud providers**, or we can even **mix** all of these.

When the cluster is configured properly, it **oversees** the set of servers, and decides where to **deploy** containerized applications, when to **scale** up and down the number of application replicas, and what to do when an application or server stops working. 

⚠️ **This article assumes that you use a Linux distribution as OS for all nodes, on both physical and/or virtual hosts.**


<a name="2"></a>
## 2. Components of Kubernetes

We can define "components" as such from different points of view: 

- software components that **implement** a working Kubernetes environment
- architectural components to define and understand the **relationship and dependencies** within a Kubernetes environment

### 2.1 Software Components of a Lab Environment

In order to **start learning about and experimenting** with Kubernetes, you need to have the following components installed: 

- a container service, implementing a **Container Runtime**, preferably **Docker** (for Windows or Mac, you need to use **Docker Desktop** of course, which virtualizes a native-ish Docker environment)
- `minikube` - a CLI tool that runs a single-node Kubernetes **cluster itself** on your laptop or in a VM. It helps you **to create a cluster** from scratch.
- `kubectl` - a CLI tool to deploy and manage **applications** on a Kubernetes cluster. It lets you **to interact with your cluster**.

⚠️ **The advanced level of Docker management is not part of this article.**

### 2.2 Components of Kubernetes' Architecture

![alt_text](https://isitobservable.io/assets/images/blog-img-01@2x.png)

- **Control Plane**: the control or **orchestration layer** of the whole cluster, that exposes the API and interfaces to define, deploy, and manage the lifecycle of containers. The subcomponents of the cluster all run on a separated **Master Node**.
- **Worker Nodes**, Worker Machines or sometimes simply referred only as **Nodes**: these physical or virtual servers are mostly identified as the ones, hosting container runtime, each

#### 2.2.1 Components of the Control Plane

- **kube-apiserver**: it represents the front end for controlling the cluster
- **etcd**: it contains the cluster configuration
- **kube-scheduler**: it watches for newly created Pods with no assigned node, and selects a node for them to run on
- **kube-controller-manager**: a single process to supervise all the sub-controlling tasks for sub-components
- **cloud-controller-manager**: it embeds cloud-specific control logic into the cluster

#### 2.2.2 Components of the Nodes

- **kubelet**: an **agent** that runs on each node in the cluster. It makes sure that containers are running in a Pod.
- **kube-proxy**: it implements the Kubernetes **networking** inside and between nodes
- **container runtime**: the software that is responsible for running containers (e.g. `containerd`)
- **pods**: a set of running containers (can be 1 or more); **a logical representation** of a worker unit

For the **official definitions** of all of these architectural components, please refer to the [official documentation](https://kubernetes.io/docs/concepts/overview/components/).

### 2.3 Components of a K8s Enterprise Environment

Even though all the above is true in case of a real enterprise environment, when you are not simply "playing around" with Kubernetes on your laptop and on VMs running on it but you are ready to leverage the solution's true power, then you need to take other other components too into consideration, like using `containerd` as your Container Runtime and Calico networking add-on to provide networking for the cluster. 

This article attempts to provide more details on these enterprise-level considerations in a separate chapter.


<a name="3"></a>
## 3. Installation

When we use **VMs** for worker nodes, it is a prerequisite to have at least 2 vCPU for our VM. Otherwise, we will get this message during starting our `minikube`-based environment: 

```ksh
X Exiting due to RSRC_INSUFFICIENT_CORES: Requested cpu count 2 is greater than the available cpus of 1
```

### 3.1 Gathering and Testing `kubectl` and `minikube`

Many learning materials suggest that these are complex software solutions, and you need to use `apt-get`, `dnf` or similar package managers, but in reality, these are single binaries. 

Preparing the binaries for our lab: 

1. Fetching the binaries (into a custom path):

```ksh
# mkdir -p /opt/kubernetes && cd /opt/kubernetes

# curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   154  100   154    0     0    939      0 --:--:-- --:--:-- --:--:--   939
100 44.7M  100 44.7M    0     0  6694k      0  0:00:06  0:00:06 --:--:-- 7436k

# curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 65.8M  100 65.8M    0     0  5180k      0  0:00:13  0:00:13 --:--:-- 7926k

# mv minikube-linux-amd64 minikube

# chmod +x *

# ls -la
total 113240
drwxr-xr-x. 2 root root     4096 Oct 21 17:20 .
drwxr-xr-x. 6 root root     4096 Oct 21 17:19 ..
-rwxr-xr-x. 1 root root 46907392 Oct 21 17:20 kubectl
-rwxr-xr-x. 1 root root 69041843 Oct 21 17:20 minikube
```

2. As we used a custom path, we need to set up the user's profile of the elevated user for controlling the cluster (this is `root` in this example, **temporarily**): 

```ksh
# vi ~/.bash_profile
[...]
export PATH=$PATH:$HOME/bin:/opt/Node.js/bin:/opt/docker:/opt/kubernetes
[...]
^D
```

3. Relogin, then checking the versions (old versions, when this article was created): 

```ksh
# kubectl version --client
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:38:50Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}

# minikube version
minikube version: v1.23.2
commit: 0a0ad764652082477c00d51d2475284b5d39ceed
```

And now we are **almost done**.

### 3.2 Setting up a Kubernetes administrator user

By default, `minikube` doesn't recommend to run the services under `root` user. It warns the user for this with the following message: 

```ksh
# minikube start
* minikube v1.23.2 on Rocky 8.4
* Using the docker driver based on user configuration
* The "docker" driver should not be used with root privileges.
...
X Exiting due to DRV_AS_ROOT: The "docker" driver should not be used with root privileges.
```

So, the safe way is to **create a regular user** or use an existing one (this example uses a `kube` named user for it), then set its `$PATH` variable. Also ensure the Docker daemon is running: 

```ksh
# useradd -g users -G docker -m kube

# vi /home/kube/.bash_profile
...
export PATH=$PATH:/opt/docker:/opt/kubernetes
...

# su - kube

$ ps -ef | grep docker
root        1427    1408  0 17:29 pts/0    00:00:00 dockerd
root        1433    1427  0 17:29 ?        00:00:01 containerd --config /var/run/docker/containerd/containerd.toml --log-level info
kube        1965    1941  0 17:38 pts/0    00:00:00 grep --color=auto docker
```

### 3.3 Starting the cluster and check its components

When you start an empty cluster for the first time, you will notice that it downloads the `k8s-minikube` image prior starting, so it takes some minutes.  

In this example, I started it on a laptop directly, with the latest version, as of January 2023: 

```ksh
$ minikube start
😄  minikube v1.28.0 on Opensuse-Tumbleweed 
✨  Automatically selected the docker driver. Other choices: virtualbox, ssh
📌  Using Docker driver with root privileges
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.25.3 preload ...
    > preloaded-images-k8s-v18-v1...:  385.44 MiB / 385.44 MiB  100.00% 2.82 Mi
    > gcr.io/k8s-minikube/kicbase:  386.27 MiB / 386.27 MiB  100.00% 1.70 MiB p
    > gcr.io/k8s-minikube/kicbase:  0 B [_______________________] ?% ? p/s 2m4s
🔥  Creating docker container (CPUs=2, Memory=3900MB) ...
🐳  Preparing Kubernetes v1.25.3 on Docker 20.10.20 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Now let's get an **overview** of our first (default) cluster!

- Check the **main status info** of the cluster: 

```ksh
$ kubectl cluster-info
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
[...]
To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

It also shows the **IP address of the Kubernetes Control Plane**. 

- Check **nodes**:

```ksh
$ kubectl get nodes
NAME       STATUS   ROLES           AGE     VERSION
minikube   Ready    control-plane   8m24s   v1.25.3
```

In this initial/default stage, only the Control Plane has nodes.

- Check **namespaces**: 

```ksh
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   8m54s
kube-node-lease   Active   8m55s
kube-public       Active   8m55s
kube-system       Active   8m55s
```

Namespaces are a way to **isolate and manage** applications and services from each other. 

- Check **pods**, associated to **any namespaces**: 

```ksh
$ kubectl get pods -A
NAMESPACE     NAME                               READY   STATUS    RESTARTS   AGE
kube-system   coredns-565d847f94-tfbm4           1/1     Running   0          9m16s
kube-system   etcd-minikube                      1/1     Running   0          9m29s
kube-system   kube-apiserver-minikube            1/1     Running   0          9m29s
kube-system   kube-controller-manager-minikube   1/1     Running   0          9m29s
kube-system   kube-proxy-sj9dn                   1/1     Running   0          9m16s
kube-system   kube-scheduler-minikube            1/1     Running   0          9m29s
kube-system   storage-provisioner                1/1     Running   0          9m28s
```

These pods, again, are the ones that the Kubernetes cluster itself requires, nothing more at this stage.

- Check the cluster's **services**: 

```ksh
$ kubectl get services -A
NAMESPACE     NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  10m
kube-system   kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   10m
```
Services act as **load balancers** within the cluster, and **direct traffic to pods**. 

- **Stop** the cluster: 

```ksh
$ minikube stop
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
🛑  1 node stopped.
```

### 3.4 Setting Docker as default for `minikube`

By default, `minikube` has a priority list for its driver to be loaded during start, so without any parameters, it assumes the user maybe wanted the Docker driver (also notifies us as "Automatically selected the docker driver. Other choices: virtualbox, ssh"). 

You can also **store this definition** in a config file. Some time ago, the command for it also revealed the path of the file for such settings: 

```ksh
$ minikube config set driver docker
! These changes will take effect upon a minikube delete and then a minikube start

$ cat ~/.minikube/config/config.json
{
    "driver": "docker"
}
```

Now it looks like this (also purging everything, as suggested by it): 

```ksh
$ minikube config set driver docker
❗  These changes will take effect upon a minikube delete and then a minikube start

$ minikube delete
🔥  Deleting "minikube" in docker ...
🔥  Deleting container "minikube" ...
🔥  Removing /home/kube/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
```


<a name="4"></a>
## 4. Application Deployment Basics

The title of this chapter is obviously a too general term, and even can be meaningless sometimes, but today's modern development practices approach the end product as an application, regardless of the hosting machine, and not as an application, running on an OS, running on a platform. The reason is developers didn't want to give a damn about the OS, its general resources, the server and the platform. They just wanted to get the infrastructure ready superfast, preferably based on a single and supersimple configfile. Therefore, the term **Infrastructure as Code (IaC)** was also constructed. This led directly to the foundation of another one, to **GitOps**. 

### 4.1 The YAML manifests

YAML is a "Data Serialization Language", often used for configuration files, where it's heavily important to transfer the configuration easily between servers and/or locations. The other possibilities for the same nowadays are JSON and XML, but definitely YAML is the most simplest and most popular. 

#### 4.1.1 General syntax of a YAML file

YAML files' extension can be either `.yml` or `.yaml`. The contents are easy to follow but easy to mess too, because of strict identations. Identation means **two more spaces**, comparing to the higher level key. 

1. `---` <-- for marking the beginning of the "document" (you can store multiple documents in a single file)
2. `# blabla` <-- comments
3. `name: Jakab Gipsz` <-- `key`/`value` pairs, without even requiring double or single quotes
4. sequence (list of array of items), idented under a key, e.g.:
```
machines:
  - r7425
  - r7525
```

When in doubt, use a YAML Validator, e.g. [yamlling.com](https://www.yamllint.com/). 

### 4.2 Namespaces

As mentioned above already, these are to **isolate and manage** applications and (micro)services from each other. As also listed already, four namespaces are created automatically by default. 

#### 4.2.1 Creating, modifying, deleting namespaces

To create a custom, new namespace with using YAML manifest, **create** a new `namespace.yml` file with this basic contents: 

```
---
apiVersion: v1
kind: Namespace
metadata:
  name: development
```

...then **activate** and **check** the namespace: 

```ksh
$ kubectl apply -f namespace.yml 
namespace/development created

$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   70s
development       Active   17s
kube-node-lease   Active   71s
kube-public       Active   71s
kube-system       Active   71s
```

(You can use `kubectl get ns` too for the same checking.)

The same `.yml` file can be **extended** for further namespaces, e.g.:

```
---
apiVersion: v1
kind: Namespace
metadata:
  name: development
---
apiVersion: v1
kind: Namespace
metadata:
  name: test
```

...then applying the file nicely recognizes what should be done: 

```ksh
$ kubectl apply -f namespace.yml 
namespace/development unchanged
namespace/test created
```

Deleting namespaces based on their manifest file is similarily easy: 

```ksh
$ kubectl delete -f namespace.yml 
namespace "development" deleted
namespace "test" deleted
```

### 4.3 Applcation Deployments

Similarily to the previous namespace creation, you can also define it through a new `.yml` file. 

#### 4.3.1 Definition of the Application Deployment

Let's call our manifest as `deployment-nginx.yml`: 

```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: development
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        command: ["/bin/sh","-c"]
        args: ["sed -i 's/listen  .*/listen 3000;/g' /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"]
        ports:
        - containerPort: 3000
```

Notice number of `replicas`: it instructs the Control Plane to create **3** replicas of the same application. 

The `command`, `args` and `ports` parts were needed here only to enforce the "abnormal" port `3000` for testing. By default, it runs with the default HTTP port `80`, and even if the configuration accepts the `- containerPort: 3000` definition, it still listens on port `80`. 

#### 4.3.2 Creating the Application

**Create** the namespace (assuming the previous `namespace.yml` restored to have `development` definition only), then lunch the application deployment: 

```ksh
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   8m30s
kube-node-lease   Active   8m31s
kube-public       Active   8m31s
kube-system       Active   8m31s

$ kubectl apply -f namespace.yml 
namespace/development created

$ kubectl apply -f deployment-nginx.yml 
deployment.apps/nginx-deployment created
```

**Check** the deployment and the status of pods: 

```ksh
$ kubectl get deployments -n development
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           84s

$ kubectl get pods -n development
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-77ddfb9b4d-45spk   1/1     Running   0          98s
nginx-deployment-77ddfb9b4d-pl4n8   1/1     Running   0          98s
nginx-deployment-77ddfb9b4d-sgd74   1/1     Running   0          98s
```

#### 4.3.3 Short HA testing

Testing the application's **high availability** through its pods can be done e.g. by **deleting one of the pods**, then ensuring that the cluster still keeps always three pods, as three running replicas for that application, making a new one in this case: 

```ksh
$ kubectl get pods -n development
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-77ddfb9b4d-45spk   1/1     Running   0          98s
nginx-deployment-77ddfb9b4d-pl4n8   1/1     Running   0          98s
nginx-deployment-77ddfb9b4d-sgd74   1/1     Running   0          98s

$ kubectl delete pod nginx-deployment-77ddfb9b4d-45spk -n development
pod "nginx-deployment-77ddfb9b4d-45spk" deleted

$ kubectl get pods -n development
NAME                                READY   STATUS        RESTARTS   AGE
nginx-deployment-77ddfb9b4d-45spk   1/1     Terminating   0          2m12s
nginx-deployment-77ddfb9b4d-dbfmr   1/1     Running       0          4s
nginx-deployment-77ddfb9b4d-pl4n8   1/1     Running       0          2m12s
nginx-deployment-77ddfb9b4d-sgd74   1/1     Running       0          2m12s
```

#### 4.3.4 Details of Pods and Event Logs

The `-o wide` shows the IPs and some other info additionally: 

```ksh
$ kubectl get pods -n development
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-7f9b6b5d8f-6h5w8   1/1     Running   0          6m30s
nginx-deployment-7f9b6b5d8f-fbtzf   1/1     Running   0          6m30s
nginx-deployment-7f9b6b5d8f-h6dlh   1/1     Running   0          6m30s

$ kubectl get pods -n development -o wide
NAME                                READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
nginx-deployment-7f9b6b5d8f-6h5w8   1/1     Running   0          6m39s   172.17.0.3   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-fbtzf   1/1     Running   0          6m39s   172.17.0.5   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-h6dlh   1/1     Running   0          6m39s   172.17.0.4   minikube   <none>           <none>
```

The `describe pod ...` command shows everything we might need, including also the IP and any related event logs at the end: 

```ksh
$ kubectl describe pod nginx-deployment-7f9b6b5d8f-6h5w8 -n development
Name:             nginx-deployment-7f9b6b5d8f-6h5w8
Namespace:        development
Priority:         0
Service Account:  default
Node:             minikube/192.168.49.2
Start Time:       Mon, 02 Jan 2023 17:42:36 +0200
Labels:           app=nginx
                  pod-template-hash=7f9b6b5d8f
Annotations:      <none>
Status:           Running
IP:               172.17.0.3
IPs:
  IP:           172.17.0.3
Controlled By:  ReplicaSet/nginx-deployment-7f9b6b5d8f
[...]
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  7m18s  default-scheduler  Successfully assigned development/nginx-deployment-7f9b6b5d8f-6h5w8 to minikube
  Normal  Pulling    7m17s  kubelet            Pulling image "nginx:latest"
  Normal  Pulled     7m14s  kubelet            Successfully pulled image "nginx:latest" in 2.343098424s
  Normal  Created    7m14s  kubelet            Created container nginx
  Normal  Started    7m14s  kubelet            Started container nginx
```

There is a specific `logs` command to see logs related to the specific pod, e.g.:

```ksh
$ kubectl logs nginx-deployment-7f9b6b5d8f-bnstv -n development
2023/01/02 15:54:19 [notice] 1#1: using the "epoll" event method
2023/01/02 15:54:19 [notice] 1#1: nginx/1.23.3
2023/01/02 15:54:19 [notice] 1#1: built by gcc 10.2.1 20210110 (Debian 10.2.1-6) 
2023/01/02 15:54:19 [notice] 1#1: OS: Linux 6.0.10-1-default
2023/01/02 15:54:19 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
[...]
```

### 4.4 Application Testing with BusyBox

Using a BusyBox is a common practice to log into an additional container, created from the respective BusyBox image to run some tests against other containers to make sure our application's availability. 

First, let's deploy the BusyBox pod quickly in our default namespace: 

```ksh
$ cat <<EOF >busybox.yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: busybox
  namespace: default
  labels:
    app: busybox
spec:
  replicas: 1
  selector:
    matchLabels:
      app: busybox
  template:
    metadata:
      labels:
        app: busybox
    spec:
      containers:
      - name: busybox-container
        image: busybox:latest
        # Keep the container running
        command: [ "/bin/sh", "-c", "--" ]
        args: [ "while true; do sleep 30; done;" ]
        resources:
          requests:
            cpu: 30m
            memory: 64Mi
          limits:
            cpu: 100m
            memory: 128Mi
EOF

$ kubectl apply -f busybox.yml
deployment.apps/busybox created

$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
busybox-6f55ffd656-qlcl5   1/1     Running   0          15m
```

**Note**: the long `kubectl get pods -n default` command is not needed here as it belongs to the `default` namespace.

Login to our BusyBox container, using its name: 

```ksh
$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
busybox-6f55ffd656-qlcl5   1/1     Running   0          39m

$ kubectl get pods -n development -o wide
NAME                                READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
nginx-deployment-7f9b6b5d8f-6h5w8   1/1     Running   0          2m18s   172.17.0.3   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-fbtzf   1/1     Running   0          2m18s   172.17.0.5   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-h6dlh   1/1     Running   0          2m18s   172.17.0.4   minikube   <none>           <none>

$ kubectl exec -it busybox-6f55ffd656-qlcl5 -- /bin/sh

/ # wget 172.17.0.3:3000
Connecting to 172.17.0.3:3000 (172.17.0.3:3000)
saving to 'index.html'
index.html           100% |*********************************************************************************|   615  0:00:00 ETA
'index.html' saved
 
/ # cat index.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]
```

Fetching, creating and logging in to a BusyBox container in one step: 

```ksh
$ kubectl run busybox --image=busybox --rm -it --restart=Never -- sh
[...]
```

### 4.5 The Kubernetes Service

This term is about **exposing** your application with a **LoadBalancer** solution to the internet. 

Example for defining a LoadBalancer through YAML manifest (save it to a `service.yml` file): 

```
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: development
  labels:
    app: nginx
spec:
  selector:
    app: nginx
  ports:
    - port: 80
      targetPort: 3000
  type: LoadBalancer
```

- It's important to assign the `app` selector to the previously defined deployment's `label` selector. If they don't match (in this example, it is `nginx`), then the service won't be assigned. 
- The `port` defines the public port (so you won't need to specify the port in your browser), while the `targetPort` is the port, used **within** the container/pod. 

Once the above config is saved...

1. start a **tunnel** in a separated terminal: 

```ksh
$ minikube tunnel
[...]
[sudo] password for root: 
Status:	
	machine: minikube
	pid: 23960
	route: 10.96.0.0/12 -> 192.168.49.2
	minikube: Running
	services: []
    errors: 
		minikube: no errors
		router: no errors
		loadbalancer emulator: no errors
```

2. Make sure the application's pods are running, then **start and check** the service: 

```ksh
$ kubectl get pods -n development -o wide
NAME                                READY   STATUS    RESTARTS        AGE   IP           NODE       NOMINATED NODE   READINESS GATES
nginx-deployment-7f9b6b5d8f-bnstv   1/1     Running   1 (4m50s ago)   18h   172.17.0.2   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-fdhlx   1/1     Running   1 (4m50s ago)   18h   172.17.0.4   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-vfchz   1/1     Running   1 (4m50s ago)   18h   172.17.0.6   minikube   <none>           <none>

$ kubectl apply -f service.yml 
service/nginx-service created

$ kubectl get services -n development
NAME            TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)        AGE
nginx-service   LoadBalancer   10.108.36.17   10.108.36.17   80:31808/TCP   33s
```

3. **Test** the service in your browser:  
  ![alt_text](kubernetes_-_service_-_welcome.png)

Notice that the tunnel showed the tunneled service: 

```ksh
[...]
Status:	
	machine: minikube
	pid: 23960
	route: 10.96.0.0/12 -> 192.168.49.2
	minikube: Running
	services: [nginx-service]
    errors: 
		minikube: no errors
		router: no errors
		loadbalancer emulator: no errors
```

### 4.6 Summing the Above Up

Once you decide your solution is close to perfect, you can **merge** all your namespace, deployment and service descriptor `.yml` files into one, as mentioned above, separated the "documents" by `---` lines. 

If you are done with it, the starting up looks like this: 

```ksh
$ kubectl apply -f infra-nginx.yml 
namespace/development created
deployment.apps/nginx-deployment created
service/nginx-service created
```

Even if you kept your YAML manifest files separated or merged, here are some more commands as a summary of...

1. all namespaces: 

```ksh
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   22h
development       Active   21h
kube-node-lease   Active   22h
kube-public       Active   22h
kube-system       Active   22h
```

2. **all objects** under `default` namespace: 

```ksh
$ kubectl get all
NAME                           READY   STATUS    RESTARTS      AGE
pod/busybox-6f55ffd656-qlcl5   1/1     Running   1 (49m ago)   19h

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   22h

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/busybox   1/1     1            1           19h

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/busybox-6f55ffd656   1         1         1       19h
```

3. **all objects** under your custom `development` namespace: 

```ksh
$ kubectl get all -n development
NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-54845cf56b-d8jdr   1/1     Running   0          14m
pod/nginx-deployment-54845cf56b-q7mfv   1/1     Running   0          13m
pod/nginx-deployment-54845cf56b-qcv4f   1/1     Running   0          13m

NAME                    TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)        AGE
service/nginx-service   LoadBalancer   10.108.36.17   10.108.36.17   80:31808/TCP   44m

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   3/3     3            3           18h

NAME                                          DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-deployment-54845cf56b   3         3         3       14m
replicaset.apps/nginx-deployment-7f9b6b5d8f   0         0         0       18h
```

### 4.7 Complete Cleanup

For a complete cleanup, you need to **delete your objects first** one-by-one. 

You can do it either by `kubectl delete service ...`, `...delete deployment ...` etc. commands, or you can still use your YAML manifest files with commands like `kubectl delete -f <respective_yaml_manifest>`, as already mentioned above. 

```ksh
$ ls -l *yml
-rw-r--r-- 1 kube users 616 Jan  2 15:44 busybox.yml
-rw-r--r-- 1 kube users 658 Jan  3 12:26 deployment-nginx.yml
-rw-r--r-- 1 kube users  65 Jan  2 14:40 namespace.yml
-rw-r--r-- 1 kube users 191 Jan  3 11:49 service.yml

$ kubectl delete -f service.yml
service "nginx-service" deleted

$ kubectl delete -f deployment-nginx.yml
deployment.apps "nginx-deployment" deleted

$ kubectl delete -f busybox.yml
deployment.apps "busybox" deleted

$ kubectl delete -f namespace.yml
namespace "development" deleted
```

The other quick solution is to use labels (more on that later), only if everything is properly labeled: 

```ksh
$ kubectl delete pod,service -n development -l 'app=nginx'
pod "nginx-deployment-54845cf56b-28c44" deleted
pod "nginx-deployment-54845cf56b-cm95w" deleted
pod "nginx-deployment-54845cf56b-k8vr7" deleted
service "nginx-service" deleted
```

Additionally, you may want to **delete the initial Minikube environment** too. 

```ksh
$ minikube delete
🔥  Deleting "minikube" in docker ...
🔥  Deleting container "minikube" ...
🔥  Removing /home/kube/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
```

### 4.7 Testing an app without YAML manifests

In this additional sub-chapter, we will test another "Hello World" app. 

Right after the initial Minikube environment is started by `minikube start`, you can download it right away, and turning it "on". 

What we will do here is roughly described in previous sub-chapters already: 

1. create a **deployment** to the `default` namespace, by downloading the tiny `echoserver` image and start it into a single pod
2. show a **summary**
3. show the latest **event logs**
4. print out the **configuration** for a potential YAML manifest
5. assign a **service**, a LoadBalancer
6. **reveal** the service IP and port

```ksh
$ kubectl create deployment hello-node --image=k8s.gcr.io/echoserver:1.7
deployment.apps/hello-node created

$ kubectl get all
NAME                              READY   STATUS              RESTARTS   AGE
pod/hello-node-775ff9967c-h8vgj   0/1     ContainerCreating   0          8s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   3m46s

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello-node   0/1     1            0           8s

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-node-775ff9967c   1         1         0       8s

$ kubectl get events
LAST SEEN   TYPE     REASON                    OBJECT                             MESSAGE
23s         Normal   Scheduled                 pod/hello-node-775ff9967c-h8vgj    Successfully assigned default/hello-node-775ff9967c-h8vgj to minikube
23s         Normal   Pulling                   pod/hello-node-775ff9967c-h8vgj    Pulling image "k8s.gcr.io/echoserver:1.7"
23s         Normal   SuccessfulCreate          replicaset/hello-node-775ff9967c   Created pod: hello-node-775ff9967c-h8vgj
23s         Normal   ScalingReplicaSet         deployment/hello-node              Scaled up replica set hello-node-775ff9967c to 1
4m13s       Normal   NodeHasSufficientMemory   node/minikube                      Node minikube status is now: NodeHasSufficientMemory
4m13s       Normal   NodeHasNoDiskPressure     node/minikube                      Node minikube status is now: NodeHasNoDiskPressure
4m13s       Normal   NodeHasSufficientPID      node/minikube                      Node minikube status is now: NodeHasSufficientPID
3m59s       Normal   Starting                  node/minikube                      Starting kubelet.
3m59s       Normal   NodeAllocatableEnforced   node/minikube                      Updated Node Allocatable limit across pods
3m59s       Normal   NodeHasSufficientMemory   node/minikube                      Node minikube status is now: NodeHasSufficientMemory
3m59s       Normal   NodeHasNoDiskPressure     node/minikube                      Node minikube status is now: NodeHasNoDiskPressure
3m59s       Normal   NodeHasSufficientPID      node/minikube                      Node minikube status is now: NodeHasSufficientPID
3m59s       Normal   NodeReady                 node/minikube                      Node minikube status is now: NodeReady
3m48s       Normal   RegisteredNode            node/minikube                      Node minikube event: Registered Node minikube in Controller
3m46s       Normal   Starting                  node/minikube                      

$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/kube/.minikube/ca.crt
    extensions:
    - extension:
        last-update: Tue, 03 Jan 2023 12:49:40 EET
        provider: minikube.sigs.k8s.io
        version: v1.28.0
      name: cluster_info
    server: https://192.168.49.2:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    extensions:
    - extension:
        last-update: Tue, 03 Jan 2023 12:49:40 EET
        provider: minikube.sigs.k8s.io
        version: v1.28.0
      name: context_info
    namespace: default
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/kube/.minikube/profiles/minikube/client.crt
    client-key: /home/kube/.minikube/profiles/minikube/client.key

$ kubectl expose deployment hello-node --type=LoadBalancer --port=8080
service/hello-node exposed
 
$ minikube service hello-node
|-----------|------------|-------------|---------------------------|
| NAMESPACE |    NAME    | TARGET PORT |            URL            |
|-----------|------------|-------------|---------------------------|
| default   | hello-node |        8080 | http://192.168.49.2:32083 |
|-----------|------------|-------------|---------------------------|
🎉  Opening service default/hello-node in default browser...
[...]
```

Example for cleaning everything up (this time, we don't have YAML manifest files, so we use the standard commands): 

```ksh
$ kubectl get all
NAME                              READY   STATUS    RESTARTS   AGE
pod/hello-node-775ff9967c-h8vgj   1/1     Running   0          14m

NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
service/hello-node   LoadBalancer   10.97.245.96   <pending>     8080:32083/TCP   12m
service/kubernetes   ClusterIP      10.96.0.1      <none>        443/TCP          17m

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello-node   1/1     1            1           14m

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-node-775ff9967c   1         1         1       14m

$ kubectl delete service hello-node
service "hello-node" deleted

$ kubectl delete deployment hello-node
deployment.apps "hello-node" deleted

$  kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   18m
```

### 4.8. Resource Management

You can limit and control the CPU and memory resources to be consumed by the containers by defining them in your deployment YAML manifest. 

The values here are `requests` and `limits`: 

- `requests` mean the **minimum** guaranteed amount of a resource that is reserved for a container.
- by `limits`, we can manipulate the **maximum** amount of a resource to be used by a container. This means that the container can never consume more than the memory amount or CPU amount indicated.

They can be applied under the `image` definition, e.g.:

```
[...]
        image: nginx:latest
        resources:
          requests:
            memory: "64Mi"
            cpu: "250m"
          limits:
            memory: "128Mi"
            cpu: "500m"
[...]
```

As always, be aware of the indentation! It's better to have a safe copy of your original `.yml` file before doing any alteration. 

You **don't need to restart your whole cluster** to take it in effect. You simply need to re-apply the configuration, then witness how the pods are re-created automatically: 

```ksh
$ vi deployment-nginx.yml

$ kubectl apply -f deployment-nginx.yml

$ kubectl get pods -n development -o wide
NAME                                READY   STATUS              RESTARTS      AGE   IP           NODE       NOMINATED NODE   READINESS GATES
nginx-deployment-54845cf56b-d8jdr   1/1     Running             0             13s   172.17.0.7   minikube   <none>           <none>
nginx-deployment-54845cf56b-q7mfv   0/1     ContainerCreating   0             4s    <none>       minikube   <none>           <none>
nginx-deployment-54845cf56b-qcv4f   1/1     Running             0             8s    172.17.0.6   minikube   <none>           <none>
nginx-deployment-7f9b6b5d8f-bnstv   1/1     Running             1 (37m ago)   18h   172.17.0.2   minikube   <none>           <none>
```

You can also check the `kubectl describe pod <pod_name> -n development`, where these resource control settings are visible too.

Look for more details and further examples at [kubernetes.io](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/). 

### 4.9 Security

It worth limiting the priviliges within the pods to increase security. Example lines for enhancing security: 

```
[...]
        securityContext:
          allowPrivilegeEscalation: false
          runAsNonRoot: true
          capabilities:
            drop:
              - ALL
          readOnlyRootFilesystem: true
[...]
```


<a name="5"></a>
## 5. Pods and Services Under the Cluster's Key Components

This chapter focuses on **getting logs and details** about these key cluster components. 

### 5.1 The Control Plane

The Control Plane's components can be recognized when we list the running pods under `kube-system` namespace, created by default in a Minikube environment: 

```ksh
$ kubectl get pods -n kube-system
NAME                               READY   STATUS    RESTARTS       AGE
coredns-565d847f94-7sgbj           1/1     Running   0              110m
etcd-minikube                      1/1     Running   0              110m
kube-apiserver-minikube            1/1     Running   0              110m
kube-controller-manager-minikube   1/1     Running   0              110m
kube-proxy-bwt78                   1/1     Running   0              110m
kube-scheduler-minikube            1/1     Running   0              110m
metrics-server-769cd898cd-77lgg    1/1     Running   0              10m
storage-provisioner                1/1     Running   1 (109m ago)   110m
```

#### 5.1.1 The API Server

As mentioned above already, the commands `kubectl` and `kubeadm` are the ones that we can use to control and communicate with the cluster through its `apiserver`. 

Listing all the Control Plane **API resources**: 

```ksh
$ kubectl api-resources
NAME                              SHORTNAMES   APIVERSION                             NAMESPACED   KIND
bindings                                       v1                                     true         Binding
componentstatuses                 cs           v1                                     false        ComponentStatus
configmaps                        cm           v1                                     true         ConfigMap
endpoints                         ep           v1                                     true         Endpoints
events                            ev           v1                                     true         Event
limitranges                       limits       v1                                     true         LimitRange
namespaces                        ns           v1                                     false        Namespace
nodes                             no           v1                                     false        Node
persistentvolumeclaims            pvc          v1                                     true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                     false        PersistentVolume
pods                              po           v1                                     true         Pod
podtemplates                                   v1                                     true         PodTemplate
replicationcontrollers            rc           v1                                     true         ReplicationController
resourcequotas                    quota        v1                                     true         ResourceQuota
secrets                                        v1                                     true         Secret
serviceaccounts                   sa           v1                                     true         ServiceAccount
services                          svc          v1                                     true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1        false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1        false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1                false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1              false        APIService
controllerrevisions                            apps/v1                                true         ControllerRevision
daemonsets                        ds           apps/v1                                true         DaemonSet
deployments                       deploy       apps/v1                                true         Deployment
replicasets                       rs           apps/v1                                true         ReplicaSet
statefulsets                      sts          apps/v1                                true         StatefulSet
tokenreviews                                   authentication.k8s.io/v1               false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1                true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1                false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1                false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1                false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v2                         true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1                               true         CronJob
jobs                                           batch/v1                               true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1                 false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1                 true         Lease
endpointslices                                 discovery.k8s.io/v1                    true         EndpointSlice
events                            ev           events.k8s.io/v1                       true         Event
flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta2   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1beta2   false        PriorityLevelConfiguration
nodes                                          metrics.k8s.io/v1beta1                 false        NodeMetrics
pods                                           metrics.k8s.io/v1beta1                 true         PodMetrics
ingressclasses                                 networking.k8s.io/v1                   false        IngressClass
ingresses                         ing          networking.k8s.io/v1                   true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1                   true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                         false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1                              true         PodDisruptionBudget
clusterrolebindings                            rbac.authorization.k8s.io/v1           false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1           false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1           true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1           true         Role
priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass
csidrivers                                     storage.k8s.io/v1                      false        CSIDriver
csinodes                                       storage.k8s.io/v1                      false        CSINode
csistoragecapacities                           storage.k8s.io/v1                      true         CSIStorageCapacity
storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass
volumeattachments                              storage.k8s.io/v1                      false        VolumeAttachment
```

#### 5.1.2 The `etcd`

The `etcd`'s most recent operations can be listed in JSON format, so it's better to use `jq` for converting it to be human readable format: 

```ksh
$ kubectl logs etcd-minikube -n kube-system | jq .
{
  "level": "info",
  "ts": "2023-01-03T10:49:34.847Z",
  "caller": "etcdmain/etcd.go:73",
  "msg": "Running: ",
  "args": [
    "etcd",
    "--advertise-client-urls=https://192.168.49.2:2379",
[...]
    "--trusted-ca-file=/var/lib/minikube/certs/etcd/ca.crt"
  ]
}
{
  "level": "info",
  "ts": "2023-01-03T10:49:34.847Z",
  "caller": "embed/etcd.go:131",
  "msg": "configuring peer listeners",
  "listen-peer-urls": [
    "https://192.168.49.2:2380"
  ]
}
[...]
```

#### 5.1.3 The Kube Scheduler

It identifies the newly created pods that haven't been assigned to a worker node yet, and it chooses a node for the pod to run on.

Taking a look at its logs: 

```ksh
$ kubectl logs kube-scheduler-minikube -n kube-system
I0103 10:49:35.414745       1 serving.go:348] Generated self-signed cert in-memory
W0103 10:49:36.715436       1 requestheader_controller.go:193] Unable to get configmap/extension-apiserver-authentication in kube-system.  Usually fixed by 'kubectl create rolebinding -n kube-system ROLEBINDING_NAME --role=extension-apiserver-authentication-reader --serviceaccount=YOUR_NS:YOUR_SA'
W0103 10:49:36.715458       1 authentication.go:346] Error looking up in-cluster authentication configuration: configmaps "extension-apiserver-authentication" is forbidden: User "system:kube-scheduler" cannot get resource "configmaps" in API group "" in the namespace "kube-system"
W0103 10:49:36.715468       1 authentication.go:347] Continuing without authentication configuration. This may treat all requests as anonymous.
W0103 10:49:36.715477       1 authentication.go:348] To require authentication configuration lookup to succeed, set --authentication-tolerate-lookup-failure=false
I0103 10:49:36.720995       1 server.go:148] "Starting Kubernetes Scheduler" version="v1.25.3"
[...]
```

#### 5.1.4 The Kube Controller Manager

It runs as a loop and constantly checks the status of the cluster to make sure things are running properly. E.g., it replaces worker nodes if necessary. 

#### 5.1.5 The Cloud Controller Manager

It simply lets your cluster to connect with a cloud provider's API, so you can use cloud resources. 

### 5.2 The Worker Nodes

As of today, most of the Kubernetes clusters are running with a minimum of three worker nodes. It's not a minimum requirement but a common practice. 

#### 5.2.1 Kubelet

It's an agent that runs on every worker node, and it ensures that the containers in a pod are running and healthy. 

It communicates directly with the Control Plane's API Server. 

#### 5.2.2 The Container Runtime

It provides a Container Runtime Interface (CRI) for the Kubelet to create containers with either of these engines: 

- Containerd
- CRI-O
- Kata Containers
- AWS Firecracker

Kubernetes **doesn't support Dockershim** since its version `1.24`, but still supports the standard Docker containers, that's why we can still use them to construct containers.

#### 5.2.3 Kube-proxy

It makes sure the node's pods can communicate with other pods on other nodes and with the Control Plane. 

Each Kube-proxy communicate directly with the API Server. 


<a name="6"></a>
## 6. Labels, Selectors, Probes and Secrets

>These are old notes only; I need to renew them with better explanation.

### 6.1 Labels

Playing around with the **labels** (not only pods can possess labels): 

```ksh
$ kubectl get pods --show-labels
[...]

$ kubectl label po/helloworld app=helloworldapp --overwrite
pod/helloworld labeled

$ kubectl label po/helloworld app-
pod/helloworld labeled
```

### 6.2 Selectors

Preparing a `default` namespace to have more "environments" by a sample YAML manifest: 

```ksh
$ kubectl create -f sample-infrastructure-with-labels.yml
pod/homepage-dev created
pod/homepage-staging created
pod/homepage-prod created
pod/login-dev created
pod/login-staging created
pod/login-prod created
pod/cart-dev created
[...]

$ kubectl get pods
NAME               READY   STATUS              RESTARTS   AGE
cart-dev           1/1     Running             0          70s
cart-prod          1/1     Running             0          70s
cart-staging       1/1     Running             0          70s
catalog-dev        0/1     ContainerCreating   0          70s
catalog-prod       0/1     ContainerCreating   0          69s
[...]
```

Examples for using selectors by `--selector` or `-l`: 

```ksh
$ kubectl get pods --selector env=production
NAME            READY   STATUS    RESTARTS   AGE
cart-prod       1/1     Running   0          8m56s
catalog-prod    1/1     Running   0          8m55s
helloworld      1/1     Running   0          16m
homepage-prod   1/1     Running   0          8m56s
login-prod      1/1     Running   0          8m56s
ordering-prod   1/1     Running   0          8m55s
quote-prod      1/1     Running   0          8m55s
social-prod     1/1     Running   0          8m56s

$ kubectl get pods -l dev-lead=jim
NAME            READY   STATUS    RESTARTS        AGE
login-dev       1/1     Running   1 (5m21s ago)   16m
login-prod      1/1     Running   1 (5m13s ago)   16m
login-staging   1/1     Running   1 (5m10s ago)   16m

$ kubectl get pods -l dev-lead!=jim,env=staging
NAME               READY   STATUS    RESTARTS        AGE
cart-staging       1/1     Running   1 (5m23s ago)   16m
catalog-staging    1/1     Running   1 (4m40s ago)   16m
homepage-staging   1/1     Running   0               16m
ordering-staging   1/1     Running   1 (4m34s ago)   16m
quote-staging      1/1     Running   1 (4m49s ago)   16m
social-staging     1/1     Running   1 (5m2s ago)    16m

$ kubectl get pods -l 'release-version in (4.0,5.0)' --show-labels
NAME              READY   STATUS    RESTARTS        AGE   LABELS
catalog-dev       1/1     Running   2 (2m53s ago)   24m   application_type=api,dev-lead=daniel,env=development,release-version=4.0,team=ecommerce
catalog-prod      1/1     Running   2 (3m2s ago)    24m   application_type=api,dev-lead=daniel,env=production,release-version=4.0,team=ecommerce
catalog-staging   1/1     Running   2 (2m51s ago)   24m   application_type=api,dev-lead=daniel,env=staging,release-version=4.0,team=ecommerce

$ kubectl get pods -l 'release-version notin (1.0,2.0)'
NAME               READY   STATUS    RESTARTS        AGE
catalog-dev        1/1     Running   2 (4m32s ago)   26m
catalog-prod       1/1     Running   2 (4m41s ago)   26m
catalog-staging    1/1     Running   2 (4m30s ago)   26m
homepage-dev       1/1     Running   0               26m
homepage-prod      1/1     Running   0               26m
homepage-staging   1/1     Running   0               26m
```

Deleting, using selectors: 

```ksh
$ kubectl delete pods -l dev-lead=jim
pod "login-dev" deleted
pod "login-prod" deleted
pod "login-staging" deleted
```

Detailing everything: 

```ksh
$ kubectl describe po/helloworld
[...]
```

Logs: 

```ksh
$ kubectl logs catalog-dev
[...]
```

### 6.3 Probes

Probes are used for automatic checkings. These probes are `readinessProbe`, `livenessProbe` etc.

...

Login into a pod (it was already explained through BusyBox): 

```ksh
$ kubectl exec -it catalog-dev /bin/bash
[...]
```

## 6.4. Application Secrets

...


<a name="7"></a>
## 7. Kubernetes Dashboard (Web UI)

It has several really useful features. Let's see only four of them: 

- making and reviewing statistics
- performing settings, where we can see even the generated YAML file and the invoked commands
- opening terminal
- creating new deployments with filling in a form

The default installation already contains the `dashboard` addon, but we need to enable it: 

```ksh
$ minikube addons list
|-----------------------------|----------|--------------|-----------------------|
|         ADDON NAME          | PROFILE  |    STATUS    |      MAINTAINER       |
|-----------------------------|----------|--------------|-----------------------|
| ambassador                  | minikube | disabled     | unknown (third-party) |
| auto-pause                  | minikube | disabled     | google                |
| csi-hostpath-driver         | minikube | disabled     | kubernetes            |
| dashboard                   | minikube | disabled     | kubernetes            |
[...]
```

Starting the Kubernetes Dashboard webapp **temporarily**: 

```ksh
$ minikube dashboard
* Enabling dashboard ...
  - Using image kubernetesui/dashboard:v2.3.1
  - Using image kubernetesui/metrics-scraper:v1.0.7
* Verifying dashboard health ...
* Launching proxy ...
* Verifying proxy health ...
* Opening http://127.0.0.1:44879/...kubernetes-dashboard:/proxy/ in your default browser...
  http://127.0.0.1:44879/...kubernetes-dashboard:/proxy/
```

In this case, you can open a browser right away (if you have the cluster running on your Linux Desktop directly) or open a tunneled SSH connection to 127.0.0.1:44879 port (in PuTTY, e.g. `L44879 127.0.0.1:44879`), then you can watch the Dashboard right away. 

The process can be stopped with Ctrl+C. 

Enabling it permanently, then starting together with the recommended `metrics-server` addon (for CPU/RAM, and other, useful statistics): 

```ksh
$ minikube addons enable dashboard
  - Using image kubernetesui/metrics-scraper:v1.0.7
  - Using image kubernetesui/dashboard:v2.3.1
* Some dashboard features require the metrics-server addon. To enable all features please run:
[...]
* The 'dashboard' addon is enabled

$ minikube addons enable metrics-server
  - Using image k8s.gcr.io/metrics-server/metrics-server:v0.4.2
* The 'metrics-server' addon is enabled

$ minikube addons list
|-----------------------------|----------|--------------|-----------------------|
|         ADDON NAME          | PROFILE  |    STATUS    |      MAINTAINER       |
|-----------------------------|----------|--------------|-----------------------|
[...]
| dashboard                   | minikube | enabled ✅   | kubernetes            |
[...]
| metrics-server              | minikube | enabled ✅   | kubernetes            |
[...]

$ minikube dashboard
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:45743/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

Example for browsing the Dashboard: 

![alt_text](kubernetes_-_dashboard.png)

You may want to set a fix port for it instead of the randomly picked one, and also send the service into the background: 

```ksh
$ minikube dashboard --port=30000 &
[1] 22013
[kube@rockysrv1 ~]$ * Verifying dashboard health ...
* Launching proxy ...
* Verifying proxy health ...
* Opening http://127.0.0.1:30000/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
  http://127.0.0.1:30000/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
```

<a name="8"></a>
## 8. Installing and Testing K8s Enterprise Components

![alt_text](kubernetes_-_enterprise.jpg)

This topic and any related considerations are collected into this chapter just because Kubernetes, as a solution, is the same (or at least, must be) on both in our learning/sandbox environments and on enterprise.

First of all, on any enterprise environments, we use the same `kubectl` and `kubeadm` binaries, usually with strict, stable version of them, but **we never work with** Minikube. It's a bit *sad*, because the `minikube` binary provides all the components of an "all-in-one" K8s Cluster. Instead, we build the Cluster from other components, from other binaries and/or packages.

The other difference is that nowadays people don't reference the **Container Runtime** as "Docker" in case of K8s Enterprise Clusters anymore but they use the industry standard `containerd`, which is based on Docker. This article keeps referencing `dockerd` for playing around in a `minikube` environment, while uses `containerd` for Enterprise. When you just dowload the latest static binary package of Docker from [download.docker.com](https://download.docker.com/linux/static/stable/x86_64/), you will find both `dockerd` and `containerd` shipped with it. 
 
### 8.1 Different Installation and Initialization Scenarios

The following scenarios will be detailed in this subchapter: 
- install on Ubuntu 20.04 LTS from repo packages with `containerd`
- install on CentOS 7.9 from repo packages with `CRIO`
- install on Rocky 9 from repo packages with `CRIO`
- install attempt on Rocky 9 from static binary packages with `containerd`

When we are working with multiple nodes, it's always a good practice to use a clear indication where you are in your terminals, so you may want to re-define your `PS1` environmental variable, e.g. `PS1="master$ "` for your Control Plane, and `PS1="node0$ "`, `PS1="node1$ "` and so on for the Worker Nodes.

#### 8.1.1 Installation and Initialization on Ubuntu 20.04 LTS

<div class="warn">
<p>This procedure uses <code>containerd</code> as Container Runtime, but everything happens on an old distro, so it's here in this article for reference only.</p>
</div>

##### 8.1.1.1 Preparing a K8s Enterprise with distro-specific packages

The example environment in this chapter contains **Ubuntu 20.04 LTS** VMs (for now), but the same activities can be done on any other major distros, but obviously with other package management commands and there might be some other minor differences around enabling kernel modules, location of config files etc., but our end goal is the same in each step. 

Also, here you will see heavy dependency on package manager repositories and package installations but getting the binaries can be just as easy as previously discussed: you can just download and extract them if you find that approach more symphatic. However, working with packages made for the distros are always the official way, just like described below. 

Preparing the K8s Control Node

1. Prepare a simple configuration for `containerd`. They are instruct the server to load the following two **kernel modules** during booting up, but you should also **load** these kernel modules for the current session already: 

```ksh
$ sudo cat > /etc/modules-load.d/containerd.conf
overlay
br_netfilter
^D

$ sudo modprobe overlay

$ sudo modprobe br_netfilter
```

By the way, the other, probably more "entertaining" and recommended method for creating these files is a combination of using `EOF` marker and `tee` command, but we will keep using the above, simple method for faster deployment. Here is the oher method anyway: 

```ksh
$ cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
> overlay
> br_netfilter
> EOF
```

2. Prepare a **configuration** for `kbernetes-cri` service for **Kubernetes networking**, then activate it: 

```ksh
$ sudo cat > /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
^D

$ sudo sysctl --system
[...]
* Applying /etc/sysctl.d/99-kubernetes-cri.conf ...
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
[...]
```

3. **Install** the `containerd` package, **create the folder** for your `containerd` configuration, **save the defaults** into it, then **restart** `containerd.service` to make sure the configuration is used: 

```ksh
$ sudo apt-get update && sudo apt-get install -y containerd
[...]
Setting up runc (1.1.7-0ubuntu1~20.04.1) ...
Setting up containerd (1.7.2-0ubuntu1~20.04.1) ...
[...]

$ sudo mkdir -p /etc/containerd

$ sudo containerd config default | sudo tee /etc/containerd/config.toml
disabled_plugins = []
imports = []
[...]

$ sudo systemctl restart containerd

$ sudo systemctl status containerd
● containerd.service - containerd container runtime
     Loaded: loaded (/lib/systemd/system/containerd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-09-05 11:45:21 UTC; 56s ago
       Docs: https://containerd.io
    Process: 4061 ExecStartPre=/sbin/modprobe overlay (code=exited, status=0/SUCCESS)
[...]
```

4. Kubernetes' prerequisite is to **have swap deactivated**, so make sure it is so, then **install** the **prerequisite packages** for Kubernetes: 

```ksh
$ sudo swapoff -a

$ sudo apt-get update && sudo apt-get install -y apt-transport-https curl
[...]
Setting up apt-transport-https (2.0.9) ...
Setting up libcurl4:amd64 (7.68.0-1ubuntu2.19) ...
Setting up curl (7.68.0-1ubuntu2.19) ...
[...]
```

5. Once the packages are installed, download and install the **GPG key** for the K8s package repository, set up the **repo configuration**, then finally, **install** `kubelet`, `kubeadm` and `kubectl` packages with specifying their version are the same. Also, set up **version lock** for these packages:

```ksh
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
OK

$ cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

$ sudo apt-get update
[...]

$ sudo apt-get install -y kubelet=1.27.0-00 kubeadm=1.27.0-00 kubectl=1.27.0-00
[...]
Setting up conntrack (1:1.4.5-2) ...
Setting up kubectl (1.27.0-00) ...
Setting up ebtables (2.0.11-3build1) ...
Setting up socat (1.7.3.3-2) ...
Setting up cri-tools (1.26.0-00) ...
Setting up kubernetes-cni (1.2.0-00) ...
Setting up kubelet (1.27.0-00) ...
Created symlink /etc/systemd/system/multi-user.target.wants/kubelet.service → /lib/systemd/system/kubelet.service.
Setting up kubeadm (1.27.0-00) ...
[...]

$ sudo apt-mark hold kubelet kubeadm kubectl
kubelet set on hold.
kubeadm set on hold.
kubectl set on hold.
```

Go through on the same kernel module enablement, package repository configurations and package installations as above **on K8s Worker Nodes too**. 

##### 8.1.1.2 Initialize and configure the Cluster

It can be done on the Control Node and the first step takes some minutes to complete by `kubeadm`. 

1. **Initialize** the Cluster: 

```ksh
$ sudo kubeadm init --pod-network-cidr 192.168.0.0/16 --kubernetes-version 1.27.0
[...]
Your Kubernetes control-plane has initialized successfully!
[...]
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
[...]
```

2. Issue the **three commands just like suggested above**, then you will see the Control Plane is defined but in `NotReady` status yet. It's because we need to install the networking plug-in: 

```ksh
$ kubectl get nodes
NAME          STATUS     ROLES           AGE   VERSION
k8s-control   NotReady   control-plane   17m   v1.27.0
```

3. Install (apply) the **Calico Networking Add-on** through `kubectl` (it contains all the pre-configurations we need), then wait about 2-5 minutes, then check the Control Plane again to ensure it's `Ready`: 

```ksh
$ kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
[...]
daemonset.apps/calico-node created
deployment.apps/calico-kube-controllers created

$ kubectl get nodes
NAME          STATUS   ROLES           AGE   VERSION
k8s-control   Ready    control-plane   23m   v1.27.0
```

##### 8.1.1.3 Join the Worker Nodes to the Cluster

1. On the **Control Plane** server, you need to just issue the following **helper command** to get the actual join command for the Worker Nodes, by also generating the token for the connection: 

```ksh
$ kubeadm token create --print-join-command
kubeadm join [...]
```

2. Issue the generated command on both **Worker Nodes**, e.g.:

```ksh
$ sudo kubeadm join 10.0.1.101:6443 --token 5ag3ht.15yo01malq1sosjj --discovery-token-ca-cert-hash sha256:8657b0ff676951b698dde5f21a9f92cb3415708272a2f6f1c24bd24b60252f3f
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[...]
This node has joined the cluster:
[...]
```

3. Wait 1-2 minutes and check the **node list** again on the Control Plane: 

```ksh
$ kubectl get nodes
NAME          STATUS   ROLES           AGE     VERSION
k8s-control   Ready    control-plane   7m53s   v1.27.0
k8s-worker1   Ready    <none>          2m2s    v1.27.0
k8s-worker2   Ready    <none>          119s    v1.27.0
```

#### 8.1.2 Installation and Initialization on CentOS 7.9 Hosts

<div class="warn">
<p>This is just another half-official walkthrough (as done by a LinuxAcademy instructor.)</p>
</div>

Most of these commands need to be run on each of the nodes. Pay attention though. Down at Step 10, we are going to do a little bit on just the master, and down at Step 15 we'll run something on just the nodes. There are notes down there, just be watching for them.

1. Once we have logged in, we need to elevate privileges using sudo:

```ksh
$ sudo su -  
```

2. Disable SELinux:

```ksh
# setenforce 0

# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

3. Enable the br_netfilter module for cluster communication:

```ksh
# modprobe br_netfilter

# echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

4. Ensure that the Docker dependencies are satisfied:

```ksh
# yum install -y yum-utils device-mapper-persistent-data lvm2
[...]
```

5. Add the Docker repo and install Docker:

```ksh
# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
[...]

# yum install -y docker-ce
[...]
```

6. Set the cgroup driver for Docker to systemd, reload systemd, then enable and start Docker:

```ksh
# sed -i '/^ExecStart/ s/$/ --exec-opt native.cgroupdriver=systemd/' /usr/lib/systemd/system/docker.service

# systemctl daemon-reload

# systemctl enable docker --now
```

7. Add the Kubernetes repo:

```ksh
# cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
  https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

8. Install Kubernetes v1.14.0:

```ksh
# yum install -y kubelet-1.14.0-0 kubeadm-1.14.0-0 kubectl-1.14.0-0 kubernetes-cni-0.7.5
[...]
```

9. Enable the kubelet service. The kubelet service will fail to start until the cluster is initialized, this is expected:

```ksh
# systemctl enable kubelet
```

**Note: Complete the following section on the MASTER ONLY!**

10. Initialize the cluster using the IP range for Flannel:

```ksh
# kubeadm init --pod-network-cidr=10.244.0.0/16
[...]
```

11. Copy the kubeadmn join command that is in the output. We will need this later.

12. Exit sudo, copy the admin.conf to your home directory, and take ownership.

```ksh
$ mkdir -p $HOME/.kube

$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

13. Deploy Flannel:

```ksh
$ kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel-old.yaml
clusterrole.rbac.authorization.k8s.io/flannel created
clusterrolebinding.rbac.authorization.k8s.io/flannel created
serviceaccount/flannel created
configmap/kube-flannel-cfg created
daemonset.extensions/kube-flannel-ds-amd64 created
daemonset.extensions/kube-flannel-ds-arm64 created
daemonset.extensions/kube-flannel-ds-arm created
daemonset.extensions/kube-flannel-ds-ppc64le created
daemonset.extensions/kube-flannel-ds-s390x created
```

14. Check the cluster state:

```ksh
$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                READY   STATUS            RESTARTS   AGE
kube-system   coredns-584795fc57-9g9jh            0/1     Pending           0          6m23s
kube-system   coredns-584795fc57-thzdw            0/1     Pending           0          6m23s
kube-system   etcd-k8smaster                      1/1     Running           0          5m43s
kube-system   kube-apiserver-k8smaster            1/1     Running           0          5m20s
kube-system   kube-controller-manager-k8smaster   1/1     Running           0          5m26s
kube-system   kube-flannel-ds-amd64-ff75s         0/1     PodInitializing   0          4s
kube-system   kube-proxy-2xbqp                    1/1     Running           0          6m22s
kube-system   kube-scheduler-k8smaster            1/1     Running           0          5m16s
```

**Note: Complete the following steps on the NODES ONLY!**

15. Run the join command that you copied earlier, this requires running the command prefaced with sudo on the nodes (if we hadn't run sudo su to begin with). Then we'll check the nodes from the master.

```ksh
$ kubectl get nodes
```

Looking around on the Master Node after the other two nodes joined: 

```ksh
[cloud_user@k8smaster ~]$ kubectl get nodes
NAME        STATUS   ROLES    AGE     VERSION
k8smaster   Ready    master   10m     v1.14.0
k8snode1    Ready    <none>   3m2s    v1.14.0
k8snode2    Ready    <none>   2m50s   v1.14.0

[cloud_user@k8smaster ~]$ ip -4 a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc mq state UP group default qlen 1000
    inet 10.0.1.100/24 brd 10.0.1.255 scope global dynamic eth0
       valid_lft 2180sec preferred_lft 2180sec
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
4: flannel.1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue state UNKNOWN group default 
    inet 10.244.0.0/32 scope global flannel.1
       valid_lft forever preferred_lft forever
5: cni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue state UP group default qlen 1000
    inet 10.244.0.1/24 scope global cni0
       valid_lft forever preferred_lft forever

[cloud_user@k8smaster ~]$ ps -ef | grep -E -i "docker|container|kube"
root      8218     1  0 08:24 ?        00:00:00 /usr/bin/containerd
root      8227     1  2 08:24 ?        00:00:21 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --exec-opt native.cgroupdriver=systemd
root      8695     1  2 08:26 ?        00:00:14 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf --config=/var/lib/kubelet/config.yaml --cgroup-driver=systemd --network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.1
[...]

[cloud_user@k8smaster ~]$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                READY   STATUS    RESTARTS   AGE
kube-system   coredns-584795fc57-9g9jh            1/1     Running   0          11m
kube-system   coredns-584795fc57-thzdw            1/1     Running   0          11m
kube-system   etcd-k8smaster                      1/1     Running   0          10m
kube-system   kube-apiserver-k8smaster            1/1     Running   0          10m
kube-system   kube-controller-manager-k8smaster   1/1     Running   0          10m
kube-system   kube-flannel-ds-amd64-62bxw         1/1     Running   1          3m34s
kube-system   kube-flannel-ds-amd64-ff75s         1/1     Running   0          4m45s
kube-system   kube-flannel-ds-amd64-t9qwp         1/1     Running   1          3m22s
kube-system   kube-proxy-2xbqp                    1/1     Running   0          11m
kube-system   kube-proxy-774xm                    1/1     Running   0          3m22s
kube-system   kube-proxy-zwkhz                    1/1     Running   0          3m34s
kube-system   kube-scheduler-k8smaster            1/1     Running   0          9m57s
```

#### 8.1.3 Installation on Rocky 9 VMs

##### 8.1.3.1 Preparations of Nodes

1. Kernel module and `sysctl` preparations: 

```ksh
# cat > /etc/modules-load.d/k8s.conf
overlay
br_netfilter
^D

# modprobe overlay

# modprobe br_netfilter

# cat > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1

# sysctl --system
[...]
* Applying /etc/sysctl.d/k8s.conf ...
[...]
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
[...]

# lsmod | grep -E "br_netfilter|overlay"
br_netfilter           32768  0
bridge                323584  1 br_netfilter
overlay               172032  0

# sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
```

2. Disable firewall and SELinux: 

```ksh
# systemctl disable firewalld --now
Removed "/etc/systemd/system/multi-user.target.wants/firewalld.service".
Removed "/etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service".

# setenforce 0

# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

3. Install CRI-O from its repo: 

```ksh
# export VERSION=1.25

# export OS=CentOS_8

# curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/devel:kubic:libcontainers:stable.repo

# curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/devel:kubic:libcontainers:stable:cri-o:$VERSION.repo

# yum install crio
[...]
Is this ok [y/N]: y
[...]
Installed:
  conmon-2:2.1.7-1.el9_2.x86_64          container-selinux-3:2.205.0-1.el9_2.noarch     containers-common-4:1-19.el8.28.18.noarch     cri-o-1.25.4-2.1.el8.x86_64      
  criu-3.17-5.el9_2.x86_64               fuse-common-3.10.2-5.el9.0.1.x86_64            fuse-overlayfs-1.11-1.el9_2.x86_64            fuse3-3.10.2-5.el9.0.1.x86_64    
  fuse3-libs-3.10.2-5.el9.0.1.x86_64     libnet-1.2-6.el9.x86_64                        libslirp-4.4.0-7.el9.x86_64                   runc-4:1.1.4-1.el9_1.x86_64      
  slirp4netns-1.2.0-3.el9.x86_64         socat-1.7.4.1-5.el9.x86_64                    
[...]

# systemctl start crio

# systemctl status crio
● crio.service - Container Runtime Interface for OCI (CRI-O)
     Loaded: loaded (/usr/lib/systemd/system/crio.service; disabled; preset: disabled)
     Active: active (running) since Sun 2023-09-10 19:25:06 EEST; 3s ago
       Docs: https://github.com/cri-o/cri-o
[...]
```

4. Get `kube*` binaries from repo: 

```ksh
# cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
  https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# dnf clean all
35 files removed

# dnf repolist
[...]

# dnf install kubelet
[...]
Is this ok [y/N]: y
[...]
Installed:
  containernetworking-plugins-1.1.1-1.el8.2.16.x86_64
  kubelet-1.28.1-0.x86_64
[...]

# dnf install kubeadm
[...]
Is this ok [y/N]: y
[...]
Installed:
  cri-tools-1.26.0-0.x86_64
  kubeadm-1.28.1-0.x86_64
  kubectl-1.28.1-0.x86_64
[...]

# systemctl enable kubelet

# systemctl status kubelet
○ kubelet.service - kubelet: The Kubernetes Node Agent
     Loaded: loaded (/usr/lib/systemd/system/kubelet.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/kubelet.service.d
             └─10-kubeadm.conf
[...]
```

##### 8.1.3.2 `init` on Control Plane and `join` on Worker Nodes

5. Init the Control Plane. This time, I had to define the IP to be advertised (because the host's primary IP is a VirtualBox NAT interface and it was the primary one)

```ksh
# kubeadm init --pod-network-cidr=10.32.0.0/24 --apiserver-advertise-address=192.168.56.111
[...]
Your Kubernetes control-plane has initialized successfully!
[...]
```

6. Perform all the steps #1 to #4, then join the Worker Nodes to the Cluster, e.g.:

```ksh
# kubeadm join 192.168.56.111:6443 --token luhch7.ee3rahc3jjrex9l9 --discovery-token-ca-cert-hash sha256:ef174219cac51e751090faec08e26342eb84d09beb042318905a20f788a2b95b --v=5
[...]
This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.
[...]
```

If any time these initial configurations failed, you should consider cleaning up the invalid configuration: 

```ksh
# kubeadm reset
W0910 20:33:26.381982    4714 preflight.go:56] [reset] WARNING: Changes made to this host by 'kubeadm init' or 'kubeadm join' will be reverted.
[...]
[reset] Are you sure you want to proceed? [y/N]: y

# rm -rf /etc/kubernetes/*
```

#### 8.1.4 Attempt of Installation and Configuration Using Static Packages

<div class="warn">
<p>These are truly for future improvements only, just in case I will ever find out how to prepare K8s Enterprise without any extra repo addition and 3rd party package installations!!!</p>
</div>

1. Kernel module and `sysctl` preparations: 

```ksh
# cat > /etc/modules-load.d/k8s.conf
overlay
br_netfilter
^D

# modprobe overlay

# modprobe br_netfilter

# cat > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1

# sysctl --system
[...]
* Applying /etc/sysctl.d/k8s.conf ...
[...]
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
[...]

# lsmod | grep -E "br_netfilter|overlay"
br_netfilter           32768  0
bridge                323584  1 br_netfilter
overlay               172032  0

# sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
```

2. Disable firewall and SELinux: 

```ksh
# systemctl disable firewalld --now
Removed "/etc/systemd/system/multi-user.target.wants/firewalld.service".
Removed "/etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service".

# setenforce 0

# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

3. Get the **Container Runtime**

Not sure for how long this hacky way works, but here is how you can gather the latest stable version of the official `containerd` package by a single command (see the site [containerd.io](https://containerd.io/downloads/) for more details if it doesn't work): 

```ksh
# mkdir /opt/containerd && cd /opt/containerd

# $(curl -L -s https://containerd.io/downloads/ | awk -F'>' '/wget https/{print $NF}')
[...]
2023-09-08 12:30:39 (21.1 MB/s) - ‘containerd-1.7.5-linux-amd64.tar.gz’ saved [46863863/46863863]
[...]

# tar -xzf containerd-1.7.5-linux-amd64.tar.gz

# rm -f containerd-1.7.5-linux-amd64.tar.gz

# find . -ls
   262166      4 drwxr-xr-x   3 root     root         4096 Sep  8 14:19 .
   262169      4 drwxr-xr-x   2 root     root         4096 Aug 25 17:00 ./bin
   262173   6444 -rwxr-xr-x   1 root     root      6598656 Aug 25 17:00 ./bin/containerd-shim
   262171  25036 -rwxr-xr-x   1 root     root     25633432 Aug 25 17:00 ./bin/containerd-stress
   262170  27120 -rwxr-xr-x   1 root     root     27769976 Aug 25 17:00 ./bin/ctr
   262174   8108 -rwxr-xr-x   1 root     root      8302592 Aug 25 17:00 ./bin/containerd-shim-runc-v1
   262175  11776 -rwxr-xr-x   1 root     root     12058624 Aug 25 17:00 ./bin/containerd-shim-runc-v2
   262172  53516 -rwxr-xr-x   1 root     root     54798496 Aug 25 17:00 ./bin/containerd

# bin/containerd --version
containerd github.com/containerd/containerd v1.7.5 fe457eb99ac0e27b3ce638175ef8e68a7d2bc373
```

4. Get the **K8s binaries**

```ksh
# mkdir -p /opt/kubernetes && cd /opt/kubernetes

# for i in 'adm' 'ctl' 'let' '-proxy' ; do echo kube$i: ; curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kube$i" ; echo ----- ; sleep 5 ; done
[...]

# chmod +x *

# for i in 'adm' 'ctl' ; do echo "kube${i}: $(./kube${i} version 2>/dev/null)" ; done
kubeadm: kubeadm version: &version.Info{Major:"1", Minor:"28", GitVersion:"v1.28.1", GitCommit:"8dc49c4b984b897d423aab4971090e1879eb4f23", GitTreeState:"clean", BuildDate:"2023-08-24T11:21:51Z", GoVersion:"go1.20.7", Compiler:"gc", Platform:"linux/amd64"}
kubectl: Client Version: v1.28.1
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3

# for i in 'let' '-proxy' ; do echo "kube${i}: $(./kube${i} --version)" ; done
kubelet: Kubernetes v1.28.1
kube-proxy: Kubernetes v1.28.1
```

5. Get the `crictl` binary

It's now kind of a replacement of `docker` commands, explicitely designed to work with K8s, so it's both important to have it and the Cluster cannot start without it either.

```ksh
# cd /opt/kubernetes

# VER=$(curl -L -s https://github.com/kubernetes-sigs/cri-tools | awk 'BEGIN{FS="&quot;"}/VERSION/{print $2}' | head -1)

# curl -LO https://github.com/kubernetes-sigs/cri-tools/releases/download/${VER}/crictl-${VER}-linux-amd64.tar.gz

# tar -xzf crictl-${VER}-linux-amd64.tar.gz && rm -f crictl-${VER}-linux-amd64.tar.gz
```

6. Get the `runc` binary

```ksh
# cd /opt/kubernetes

# wget https://github.com/opencontainers/runc/releases/download/v1.1.9/runc.amd64
[...]

# mv runc.amd64 runc && chmod +x runc

# ./runc --version
runc version 1.1.9
commit: v1.1.9-0-gccaecfcb
spec: 1.0.2-dev
go: go1.20.3
libseccomp: 2.5.4
```

7. Get the `cni` binaries

```ksh
# mkdir -p /opt/cni/bin && cd /opt/cni/bin

# wget https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz
[...]

# tar -xzf cni-plugins-linux-amd64-v1.3.0.tgz && rm -f cni-plugins-linux-amd64-v1.3.0.tgz

# ./bridge --version
CNI bridge plugin v1.3.0
CNI protocol versions supported: 0.1.0, 0.2.0, 0.3.0, 0.3.1, 0.4.0, 1.0.0

# echo $PATH
/root/.local/bin:/root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin

# export PATH=$PATH:/opt/cni/bin
```

8. Install and configure `conntrack`

This tool is a command line interface for connection tracking system, used by HA solutions like K8s. As it's a general tool, it can be installed from regular repo of your distribution, e.g.:

```ksh
# dnf install conntrack-tools.x86_64
[...]
Is this ok [y/N]: y
[...]
Installed:
  conntrack-tools-1.4.7-2.el9.x86_64   libnetfilter_cthelper-1.0.0-22.el9.x86_64   libnetfilter_cttimeout-1.0.0-19.el9.x86_64   libnetfilter_queue-1.0.5-1.el9.x86_64  
[...]
```

It's default configuration is usually wrong, so edit `/etc/conntrackd/conntrackd.conf` and change the `IPv4_interface` and `Interface` options in your `Multicast` block to match with your environment, using that NIC which you can use to access other nodes in the Cluster. E.g. like below, then start and check the service:

```ksh
# cat /etc/conntrackd/conntrackd.conf | grep -E "Multicast|IPv4_interface|Interface" | grep -v "#"
	Multicast {
		IPv4_interface 192.168.56.111
		Interface enp0s8

# systemctl start conntrackd

# systemctl status conntrackd
● conntrackd.service - connection tracking daemon for debugging and High Availablity
     Loaded: loaded (/usr/lib/systemd/system/conntrackd.service; disabled; preset: disabled)
     Active: active (running) since Fri 2023-09-08 17:15:45 EEST; 2s ago
    Process: 2007 ExecStartPre=/bin/rm -f /var/lock/conntrack.lock (code=exited, status=0/SUCCESS)
   Main PID: 2008 (conntrackd)
[...]
```

9. Create symlinks and `.service` files

Creating symlinks are needed to make sure the files are in `PATH`: 

```ksh
# find /opt/kubernetes -type f -exec ln -s {} /usr/local/bin/ \;

# ln -s /opt/containerd/bin/containerd /usr/local/bin/containerd

# ls -la /usr/local/bin
total 8
drwxr-xr-x.  2 root root 4096 Sep  8 14:20 .
drwxr-xr-x. 12 root root 4096 Jul 31 16:11 ..
lrwxrwxrwx.  1 root root   30 Sep  8 14:20 containerd -> /opt/containerd/bin/containerd
lrwxrwxrwx.  1 root root   22 Sep  8 14:16 crictl -> /opt/kubernetes/crictl
lrwxrwxrwx.  1 root root   23 Sep  8 14:16 kubeadm -> /opt/kubernetes/kubeadm
lrwxrwxrwx.  1 root root   23 Sep  8 14:16 kubectl -> /opt/kubernetes/kubectl
lrwxrwxrwx.  1 root root   23 Sep  8 14:16 kubelet -> /opt/kubernetes/kubelet
```

Example for `/lib/systemd/system/containerd.service` (you can just create it with `cat > [...]`):

```
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target local-fs.target

[Service]
ExecStartPre=-/sbin/modprobe overlay
ExecStart=/opt/containerd/containerd

Type=notify
Delegate=yes
KillMode=process
Restart=always
RestartSec=5
LimitNPROC=infinity
LimitCORE=infinity
LimitNOFILE=infinity
TasksMax=infinity
OOMScoreAdjust=-999

[Install]
WantedBy=multi-user.target
```

Example for `/lib/systemd/system/kubelet.service`: 

```
[Unit]
Description=kubelet: The Kubernetes Node Agent
Documentation=https://kubernetes.io/docs/home/
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/local/bin/kubelet
Restart=on-failure
RestartSec=10
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
```

Issue a `systemctl daemon-reload`, ensure swap is also disabled in `/etc/fstab` and also: 

```ksh
# systemctl enable containerd
Created symlink /etc/systemd/system/multi-user.target.wants/containerd.service → /usr/lib/systemd/system/containerd.service.

# systemctl enable kubelet
Created symlink /etc/systemd/system/multi-user.target.wants/kubelet.service → /usr/lib/systemd/system/kubelet.service.
```

The `containerd.service` can be started already but the `kubelet.service`, for now, must be stopped, because that holds port `10250` which would be written as an error for the next step, for initialization. 

**Some debugging:** 

All the configuration of the Container Runtime side (including warnings/errors, if any): 

```ksh
# crictl -r unix:///var/run/containerd/containerd.sock info
[...]
```

The supported parameters for the current version of the `kubelet` binary: 

```ksh
# kubelet --help
[...]
```

### 8.2 Deploying and Scaling Services

#### 8.2.1 Deploying a Simple Service

This example service simply provides a list of dummy products on a simple website (similar to the above examples demonstrated on `minikube` environment), but now we have an enterprise environment for it. 

##### 8.2.1.1 The Deployment

1. Login to the Master Node, ensure the cluster is `Ready`, then generate the YAML manifest file for our app (see above for details what means what): 

```ksh
$ kubectl get nodes
NAME            STATUS   ROLES    AGE     VERSION
ip-10-0-1-101   Ready    master   4h17m   v1.12.2
ip-10-0-1-102   Ready    <none>   4h17m   v1.12.2
ip-10-0-1-103   Ready    <none>   4h17m   v1.12.2

$ cat > app-storeproducts-deployment.yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: store-products
  labels:
    app: store-products
spec:
  replicas: 4
  selector:
    matchLabels:
      app: store-products
  template:
    metadata:
      labels:
        app: store-products
    spec:
      containers:
      - name: store-products
        image: linuxacademycontent/store-products:1.0.0
        ports:
        - containerPort: 80
^D
```

2. Deploy with `kubeadm` and check the results: 

```ksh
$ kubectl apply -f app-storeproducts-deployment.yml
deployment.apps/store-products created

$ kubectl get deployments
NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
store-products   4         4         4            4           91s

$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
busybox                           1/1     Running   0          88m
store-products-576bb96d6d-4q84d   1/1     Running   0          107s
store-products-576bb96d6d-96brd   1/1     Running   0          107s
store-products-576bb96d6d-mzdpt   1/1     Running   0          108s
store-products-576bb96d6d-vjwsm   1/1     Running   0          107s
```

##### 8.2.1.2 Creating the Service for the app

Create an another YAML manifest for the Service and apply it with `kubectl`: 

```ksh
$ cat > app-storeproducts-service.yml
---
kind: Service
apiVersion: v1
metadata:
  name: store-products
spec:
  selector:
    app: store-products
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80

$ kubectl apply -f app-storeproducts-service.yml 
service/store-products created

$ kubectl get svc
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP   96m
store-products   ClusterIP   10.108.94.165   <none>        80/TCP    2m53s
```

This service results creating other pods to access the previously created deployment and makes sure it load-balances the dynamic usage of the 4 replicas of them. 

##### 8.2.1.3 Testing the Service

It can be done simply again through a BusyBox container: 

```ksh
$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
busybox                           1/1     Running   1          96m
store-products-576bb96d6d-4q84d   1/1     Running   0          10m
store-products-576bb96d6d-96brd   1/1     Running   0          10m
store-products-576bb96d6d-mzdpt   1/1     Running   0          10m
store-products-576bb96d6d-vjwsm   1/1     Running   0          10m

$ kubectl exec busybox -- curl -s store-products
{
	"Products":[
		{
			"Name":"Apple",
			"Price":1000.00,
		},
		{
			"Name":"Banana",
			"Price":5.00,
		},
		{
			"Name":"Orange",
			"Price":1.00,
		},
		{
			"Name":"Pear",
			"Price":0.50,
		}
	]
}
```

#### 8.2.2 Deploying a Microservice

This time you've been given a task to deploy multiple applications that serve a more complex webservice, which, in this example, is a small shop selling robots. It has an instance with a MongoDB as the DB behind the service to store the data and another one to serve the frontend, the webservice itself. 

##### 8.2.2.1 Preparations for the Deployment

This example app is maintained by Instana, and you can just fetch the latest release of it from GitHub. 

1. Clone the repo:  

```ksh
$ pwd
/home/cloud_user

$ git clone https://github.com/linuxacademy/robot-shop.git
Cloning into 'robot-shop'...
remote: Enumerating objects: 1100, done.
remote: Total 1100 (delta 0), reused 0 (delta 0), pack-reused 1100
Receiving objects: 100% (1100/1100), 80.21 MiB | 42.71 MiB/s, done.
Resolving deltas: 100% (610/610), done.
```

2. Create the app's own namespace: 

```ksh
$ kubectl create namespace robot-shop
namespace/robot-shop created
```

##### 8.2.2.2 The Deployment in one go

The repo's `.K8s/descriptors` directory contains all the YAML manifests (or "descriptors") for us to create all the components of this app, so let's use them all at once, letting K8s to create them one-by-one in proper order: 

```ksh
$ kubectl -n robot-shop create -f ~/robot-shop/K8s/descriptors/
deployment.extensions/cart created
service/cart created
deployment.extensions/catalogue created
service/catalogue created
deployment.extensions/dispatch created
service/dispatch created
deployment.extensions/mongodb created
service/mongodb created
deployment.extensions/mysql created
service/mysql created
deployment.extensions/payment created
service/payment created
deployment.extensions/rabbitmq created
service/rabbitmq created
deployment.extensions/ratings created
service/ratings created
deployment.extensions/redis created
service/redis created
deployment.extensions/shipping created
service/shipping created
deployment.extensions/user created
service/user created
deployment.extensions/web created
service/web created

$ kubectl get pods -n robot-shop
NAME                         READY   STATUS    RESTARTS   AGE
cart-59b7dfbdcc-8jr76        1/1     Running   0          2m16s
catalogue-796444cf68-nlhwc   1/1     Running   0          2m16s
dispatch-76cf85ddc6-vqxp6    1/1     Running   0          2m16s
mongodb-5969679ff7-6qhdj     1/1     Running   0          2m16s
mysql-7958997695-l9tmm       1/1     Running   0          2m16s
payment-6fd7c8ff75-hk6zc     1/1     Running   0          2m16s
rabbitmq-6ddb6485bd-7q59t    1/1     Running   0          2m15s
ratings-5fd67676b4-f994q     1/1     Running   0          2m15s
redis-dff4dbc55-qbkqz        1/1     Running   0          2m15s
shipping-8695b6c8-n5qcn      1/1     Running   0          2m15s
user-59d45d4db6-2b2wv        1/1     Running   0          2m15s
web-757b94795b-rsjf2         1/1     Running   0          2m15s
```

(The `-w` parameter for `kubectl get` allows you to be notified about the changes, especially if any of these pods are not in `Running` status yet, so you will know when they change.)

The website is available on the Master node's IP on port `30080`. 

##### 8.2.2.3 Scaling up the MongoDB Deployment

By `kubectl edit deployment ...` command, you can directly edit the descriptor of the **running deployment** by the directly invoked `vim` editor, and upon saving, the changes immediately take effect: 

```ksh
$ kubectl get pods -n robot-shop
NAME                         READY   STATUS    RESTARTS   AGE
[...]
mongodb-5969679ff7-6qhdj     1/1     Running   0          27m
[...]

$ kubectl edit deployment mongodb -n robot-shop
[...]
spec:
  progressDeadlineSeconds: 2147483647
  replicas: 2
[...]
:wq

[...]
deployment.extensions/mongodb edited

$ kubectl get pods -n robot-shop | grep mongodb
mongodb-5969679ff7-6qhdj     1/1     Running   0          30m
mongodb-5969679ff7-r8xg8     1/1     Running   0          45s

$ kubectl get deployment mongodb -n robot-shop
NAME      DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
mongodb   2         2         2            2           32m
```

The original descriptor files don't change by activities like this. 

You can easily catch the **running containers on the participating Worker Nodes**, e.g.:

```ksh
$ sudo docker ps
CONTAINER ID        IMAGE                              COMMAND                  CREATED             STATUS              PORTS               NAMES
f0cd70b4290e        linuxacademycontent/rs-cart        "node server.js"         About an hour ago   Up About an hour                        k8s_cart_cart-59b7dfbdcc-8jr76_robot-shop_24508c8c-4caf-11ee-9faf-0e40d9595b05_0
681be558cc1d        linuxacademycontent/rs-mongodb     "docker-entrypoint.s…"   About an hour ago   Up About an hour                        k8s_mongodb_mongodb-5969679ff7-6qhdj_robot-shop_2451c068-4caf-11ee-9faf-0e40d9595b05_0
[...]
```


## ???. Some More Topics Left to Discuss

### Advanced Ways to Manage Pods

...

- **Replicas** (the most common way)
- **DaemonSet** (one pod per node; background processes)
- **Job** (runs until completion; e.g. testing cluster for testing framework)

...

### Stateful  Workloads

... `StatefulSet` ...

