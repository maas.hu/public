# ![alt text](/_ui/img/icons/qemu_m.svg) QEMU

<div style="text-align: right;"><h7>Posted: 2022-10-18 11:43</h7></div>

###### .

![alt_text](qemu_-_bg.jpg)


## Table of contents

1. [Introduction](#1)
2. [Installing AIX on Linux QEMU](#2)
3. [Network Setup](#3)
4. [Further Storage Considerations](#4)
5. [Different CPU Types and SMP Emulation](#5)
6. [Additional Observations on PPC Emulation](#6)


<a name="1"></a>
## 1. Introduction

**QEMU** is an abbreviation for **Quick Emulator**, and it is an open-source virtualization software that can emulate CPUs and other hardware components. 

Normally, QEMU is used by other Type-1 or Type-2 virtualization solutions as a layer that emulates that particular CPU type or other hardware that the particular VM desires, so QEMU is quite rarely used on its own. Usually, it is associated with a close cooperation with **KVM**, but **VirtualBox** also should be mentioned as a strong user of QEMU's set of services. **Xen Project** may have such setup that is fully independent from QEMU, but usually Xen requires it too in its **Hardware-assisted Virtual Machine** (HVM) modes. 

This subpage guides you through on a case, when we use QEMU **on its own**, to create and run a QEMU VM for **IBM AIX** operating system, which is available for only one platform, for the amazing **IBM Power**, the **PPC64**. 

Since QEMU emulates a full system, you can use it to run different operating systems without having to reboot your host computer. Because of its architecture, using QEMU on its own makes it a **Type-2 hypervisor**. 

<a name="2"></a>
## 2. Installing AIX on Linux QEMU

Source of the concept: [aix4admins.blogspot.com](http://aix4admins.blogspot.com/2020/04/qemu-aix-on-x86-qemu-quick-emulator-is.html)

The below is tried out on `x86_64` for now with an AIX 7.2, but further tests are coming. The created disk image with the installed system works fine on `aarch64` too. 

### 2.1 Preparation

Install **QEMU**, its dependencies, and also the **PPC support** for QEMU. In this example, the host OS is an openSUSE Tumbleweed: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">zypper install qemu qemu-ppc</span>
Loading repository data...
Reading installed packages...
Resolving package dependencies...
[...]
The following 18 recommended packages were automatically selected:
  kvm_stat ovmf qemu-block-curl qemu-hw-display-qxl qemu-hw-display-virtio-gpu
  qemu-hw-display-virtio-gpu-pci qemu-hw-display-virtio-vga qemu-hw-usb-host qemu-hw-usb-redirect 
  qemu-hw-usb-smartcard qemu-ksm qemu-microvm qemu-ovmf-x86_64 qemu-tools qemu-ui-curses
  qemu-ui-gtk qemu-ui-spice-app rdma-ndd
[...]
The following 14 packages are suggested, but will not be installed:
  qemu-accel-qtest qemu-arm qemu-block-dmg qemu-block-gluster qemu-block-iscsi qemu-block-nfs
  qemu-block-ssh qemu-chardev-baum qemu-extra qemu-lang qemu-ppc qemu-s390x qemu-skiboot
  qemu-vhost-user-gpu
[...]
The following 54 NEW packages are going to be installed:
  kvm_stat libcapstone4 libefa1 libfdt1 libfmt9 libibverbs libibverbs1 liblttng-ust1 libmlx4-1 libmlx5-1
  libndctl6 libpmem1 librados2 librbd1 librdmacm1 libslirp0 libspice-server1 liburing2 libvdeplug3
  libvirglrenderer1 ovmf pmdk qemu qemu-accel-tcg-x86 qemu-audio-spice qemu-block-curl qemu-block-rbd 
  qemu-chardev-spice qemu-hw-display-qxl qemu-hw-display-virtio-gpu qemu-hw-display-virtio-gpu-pci 
  qemu-hw-display-virtio-vga qemu-hw-usb-host qemu-hw-usb-redirect qemu-hw-usb-smartcard qemu-ipxe
  qemu-ksm qemu-microvm qemu-ovmf-x86_64 qemu-seabios qemu-sgabios qemu-tools qemu-ui-curses
  qemu-ui-gtk qemu-ui-opengl qemu-ui-spice-app qemu-ui-spice-core qemu-vgabios qemu-x86
  rdma-core rdma-ndd system-user-qemu qemu-SLOF qemu-ppc
[...]
54 new packages to install.
Overall download size: 50.8 MiB. Already cached: 0 B. After the operation, additional 210.9 MiB will be used.
Continue? [y/n/v/...? shows all options] (y): y
[...]
</pre>

### 2.2 Installing from official AIX install DVD

At least AIX 7.2 TL4 SP2 (2020 w27) and TL5 SP4 (2022 w19) install DVDs tested and found to be working fine. 

📌 **During the installation and initial configuration phases, be constantly careful avoiding `Ctrl+C`, because when you press it, your QEMU session itself becomes terminated, so you need to boot the AIX system up again.** 

1. **Create a disk**:  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">qemu-img create -f qcow2 hdisk0.qcow2 20G</span>
    Formatting 'hdisk0.qcow2', fmt=qcow2 cluster_size=65536 extended_l2=off compression_type=zlib size=21474836480 lazy_refcounts=off refcount_bits=16
    </pre>
    Of course, if you keep multiple disk images under the same directory, then it's better to use filenames like `aix7254srv1-hdisk0.qcow2` or something like that. 

2. **Start the VM** and start the **AIX installation** with that:  
  (assuming that both the disk image and the AIX installer DVD image are located under your current working directory)  
    <pre class="con">
    <span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
      -cpu power8 -machine pseries -m 2048 \
      -serial stdio \
      -device virtio-scsi-pci,id=scsi \
      -device scsi-hd,drive=drive-virtio-disk0 \
      -drive file=aix7254srv2-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
      -cdrom aix-7.2-tl5sp4-2219-1of2.iso \
      -prom-env "boot-command=boot cdrom:"</span>
    [...]
      Welcome to Open Firmware
    [...]
    </pre>
    By default, a QEMU window also appears as below: 

![alt_text](qemu_-_dvd_boot.png)

You need to wait until you can use your "normal" terminal, where you started the VM from. You will see some 4 digit numbers slowly showing up, then (about 3 minutes later), you should reach the following stage.  

3. **Start the actual installation** by pressing `1` and Enter first to select terminal:  
    <pre class="con">
    [...]
    ******* Please define the System Console. *******

    Type a 1 and press Enter to use this terminal as the
      system console.
    Pour definir ce terminal comme console systeme, appuyez
      sur 1 puis sur Entree.
    Taste 1 und anschliessend die Eingabetaste druecken, um
      diese Datenstation als Systemkonsole zu verwenden.
    Premere il tasto 1 ed Invio per usare questo terminal
      come console.
    Escriba 1 y pulse Intro para utilizar esta terminal como
      consola del sistema.
    Escriviu 1 1 i premeu Intro per utilitzar aquest
      terminal com a consola del sistema.
    Digite um 1 e pressione Enter para utilizar este terminal
      como console do sistema.
    [...]
    </pre>

4. Proceed with **standard AIX installation** (refer e.g. for the [subpage](/learning/it/sysadmin/aix) here about it)

5. **Wait patiently.** Depending on your above choices regarding additional software, the installation takes 60 to 140 minutes to complete, and it may show some failures like these for some detected, but driverless devices:  
    <pre class="con">
    [...]
    FAILURES
    --------
      Filesets listed in this section failed pre-installation verification
      and will not be installed.

      Missing Filesets
      ----------------
      The following filesets could not be found on the installation media.
      If you feel these filesets really are on the media, check for typographical
      errors in the name specified or, if installing from directory, check for
      discrepancies between the Table of Contents file (.toc) and the images that
      reside in the directory.

      devices.pci.f41a0011
      devices.pci.qemu.std-vga
      devices.pci.scsi
      devices.pci.usb-xhci
      devices.pci.vga
      devices.vdevice.l-lan
      devices.vdevice.nvram
      devices.vdevice.qemu.spapr-nvram
      devices.vdevice.v-scsi
      devices.vdevice.vty
      devices.vrtscsi
      devices.vscsi.disk

      &lt;&lt; End of Failure Section &gt;&gt;
    [...]
    Filesets processed:  1 of 647
    System Installation Time: 9 minutes       Tasks Complete: 18%

    installp: APPLYING software for:
            xlC.aix61.rte 16.1.0.3
    [...]
    Filesets processed:  496 of 647
    System Installation Time: 1 hr 3 mins    Tasks Complete: 75%

    installp: APPLYING software for:
            devices.chrp.IBM.lhea.diag 7.2.0.0
    [...]
              96               92      Creating boot image.                        
    [...]
    </pre>

6. Because of the QEMU's virtual environment, there will be an extra task on **setting up bootlist** at the end (just pressed `1`):  
    <pre class="con">
    [...]
    Set_Bootlist: Could not set the bootlist to:                                   
    hdisk0    .
    The boot device must be set in the host KVM.
    Press 1 to continue: 1
    [...]
    </pre>

Once the install is complete, AIX tries to restart the system. Whenever it succeeds or not, terminate it with `Ctrl+C`. 

7. Now you need to **boot from disk** first time. To do this, you need to specify the `boot-command=boot disk:` parameter for `-prom-env` for this.  
  You may still want to **keep the DVD loaded** just in case something must be installed.  
  Also, now you may start it **without that extra QEMU window**, so now it's safe to use `-display none`.  
  Let's put all these requirements into one starting command: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
  -cpu power8 -machine pseries -m 2048 \
  -serial stdio \
  -display none \
  -device virtio-scsi-pci,id=scsi \
  -device scsi-hd,drive=drive-virtio-disk0 \
  -drive file=aix7254srv2-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
  -cdrom aix-7.2-tl5sp4-2219-1of2.iso \
  -prom-env "boot-command=boot disk:"</span>
[...]
</pre>

After successful boot, but right before getting the login prompt, you may get some errors like these (booting up from a fresh AIX 7.2 TL5 SP4 installation): 

<pre class="con">
[...]
The current volume is: /dev/hd10opt
Primary superblock is valid.
 Performing all automatic mounts 
Multi-user initialization completed
Checking for srcmstr active...complete
Starting tcpip daemons:
0513-059 The syslogd Subsystem has been started. Subsystem PID is 3539314.
0513-059 The sendmail Subsystem has been started. Subsystem PID is 3735928.
0513-059 The portmap Subsystem has been started. Subsystem PID is 3867002.
0513-059 The inetd Subsystem has been started. Subsystem PID is 3998078.
0513-059 The snmpd Subsystem has been started. Subsystem PID is 4129154.
0513-059 The hostmibd Subsystem has been started. Subsystem PID is 4260230.
0513-059 The snmpmibd Subsystem has been started. Subsystem PID is 3473818.
0513-059 The aixmibd Subsystem has been started. Subsystem PID is 4063620.
Finished starting tcpip daemons.
Starting NFS services:
0513-059 The biod Subsystem has been started. Subsystem PID is 3801474.
[STOP] DSO is not supported on KVM systems
[STOP] DSO is not supported on KVM systems
[STOP] DSO is not supported on KVM systems
0513-059 The rpc.lockd Subsystem has been started. Subsystem PID is 4522282.
Completed NFS services.
Unable to disable probevue : A system call received a parameter that is not valid.
Unable to enable probevue : Reserved errno was encountered
[...]
</pre>

The first time boot takes at least 15-20 minutes because several services will fail. E.g. this `probevue` messages just look hanging but be patient, they will be passed. 

8. Finally, you can **select the terminal type**, then `Accept License Agreements` and `Accept Software Maintenance Terms and Conditions`, set **Date and Time** (including **Time Zone**), set `root` pw and install additional SW if you want, then select `Exit to Login` (again, this is not an AIX manual, so look for somewhere else for help if needed)

9. Look around on the booted system, **be happy** a bit, but **do not attempt to list the RPM packages or check the OSLevel**, so **no `rpm -qa` or `oslevel -s`**, because that may hang!  
  You may also **Disable the non-working services** in `inittab`, e.g.: 
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">rmitab rcnfs</span>

    <span class="psr">#</span> <span class="cmd">rmitab piobe</span>

    <span class="psr">#</span> <span class="cmd">rmitab qdaemon</span>

    <span class="psr">#</span> <span class="cmd">rmitab naudio2</span>

    <span class="psr">#</span> <span class="cmd">rmitab naudio</span>

    <span class="psr">#</span> <span class="cmd">rmitab aso</span>

    <span class="psr">#</span> <span class="cmd">chrctcp -S -d tftpd</span>
    </pre>

10. **Shut the system down** gracefully:   
    <pre class="con">
    <span class="psr">#</span> <span class="cmd">shutdown -F</span>
    SHUTDOWN PROGRAM
    Sat Oct 15 13:31:19 EEST 2022
    Running /etc/rc.d/rc2.d/Ksshd stop
    [...]
    Oct 22 15:53:32 portmap: terminating on signal.
    ....Halt completed....
    </pre>

You may consider saving the state of the disk image now, as it took so much time to reach this phase.


<a name="3"></a>
## 3. Network Setup

Any Unix-like hosts without network, and without at least providing an SSH service is just a joke. Here is a quick (and dirty) solution for getting it working on our AIX VM. 

1. Configure a `tap0` interface on the host: 

The scenario in this example: 

- the **host** machine has `192.168.101.100/24` on its `wlp0s20f3` interface
- the **AIX** gets `192.168.101.103/24` on its `en0`

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip a</span>
[...]
2: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether cc:15:31:1a:0c:48 brd ff:ff:ff:ff:ff:ff
    inet 192.168.101.100/24 brd 192.168.101.255 scope global dynamic noprefixroute wlp0s20f3
[...]

<span class="psr">#</span> <span class="cmd">ip tuntap add tap0 mode tap</span>

<span class="psr">#</span> <span class="cmd">ip link set tap0 up</span>

<span class="psr">#</span> <span class="cmd">echo 1 > /proc/sys/net/ipv4/conf/tap0/proxy_arp</span>

<span class="psr">#</span> <span class="cmd">ip route add 192.168.101.103 dev tap0</span>

<span class="psr">#</span> <span class="cmd">ip neigh add proxy 192.168.101.103 dev tap0</span>

<span class="psr">#</span> <span class="cmd">ip neigh show proxy</span>
192.168.101.103 dev tap0 proxy

<span class="psr">#</span> <span class="cmd">echo 1 > /proc/sys/net/ipv4/ip_forward</span>

<span class="psr">#</span> <span class="cmd">iptables -t nat -A POSTROUTING -o wlp0s20f3 -j MASQUERADE</span>

<span class="psr">#</span> <span class="cmd">iptables -I FORWARD 1 -i tap0 -j ACCEPT</span>

<span class="psr">#</span> <span class="cmd">iptables -I FORWARD 1 -o tap0 -m state --state RELATED,ESTABLISHED -j ACCEPT</span>
</pre>

📌 **It's important to note that this `tap0` proxy keeps disappearing if not used because of the `arp_evict_nocarrier` event/policy. You need to ensure it still exists before starting the VM up.**

2. Start the AIX with this network setup:  

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
  -cpu power8 -machine pseries -m 2048 \
  -serial stdio \
  -display none \
  -device virtio-scsi-pci,id=scsi \
  -device scsi-hd,drive=drive-virtio-disk0 \
  -drive file=aix7254srv2-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
  -net nic,macaddr=56:44:45:30:31:32 \
  -net tap,script=no,ifname=tap0
  -prom-env "boot-command=boot disk:"</span>
[...]
</pre>

3. Configure the networking on AIX, even with the mighty `smitty mktcpip`. Don't forget to set the `default` gateway (normally your home router's IP). 

A quick final check on a working AIX VM: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">date</span>
Sun Oct 23 11:40:24 CDT 2022

<span class="psr">#</span> <span class="cmd">oslevel -s</span>
7200-05-04-2219

<span class="psr">#</span> <span class="cmd">df -m</span>
Filesystem    MB blocks      Free %Used    Iused %Iused Mounted on
/dev/hd4          64.00     24.64   62%     1898    25% /
/dev/hd2        1280.00    146.83   89%    20568    37% /usr
/dev/hd9var      128.00    100.25   22%      486     3% /var
/dev/hd3         128.00    126.04    2%       40     1% /tmp
/dev/hd1          32.00     31.66    2%        5     1% /home
/dev/hd11admin    128.00    127.63    1%        5     1% /admin
/proc                 -         -    -        -      - /proc
/dev/hd10opt      32.00     13.92   57%      195     6% /opt
/dev/livedump    256.00    255.64    1%        4     1% /var/adm/ras/livedump

<span class="psr">#</span> <span class="cmd">prtconf</span>
System Model: IBM pSeries (emulated by qemu)
Machine Serial Number: Not Available
Processor Type: PowerPC_POWER8
Processor Implementation Mode: POWER 8
Processor Version: PV_8_Compat
Number Of Processors: 1
Processor Clock Speed: 1000 MHz
CPU Type: 64-bit
Kernel Type: 64-bit
LPAR Info: 0 aix_on_kvm
Memory Size: 2048 MB
Good Memory Size: 2048 MB
Platform Firmware level: Not Available
Firmware Version: SLOF,HEAD
Console Login: enable
Auto Restart: true
Full Core: false
NX Crypto Acceleration: Not Capable
In-Core Crypto Acceleration: Capable, but not Enabled
 
Network Information
        Host Name: aix72srv1
        IP Address: 192.168.101.103
        Sub Netmask: 255.255.255.0
        Gateway: 192.168.101.1
        Name Server: 
        Domain Name: 
 
Paging Space Information
        Total Paging Space: 512MB
        Percent Used: 1%
 
Volume Groups Information
============================================================================== 
Active VGs
============================================================================== 
rootvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk0            active            639         557         127..104..70..128..128
============================================================================== 
 
INSTALLED RESOURCE LIST

The following resources are installed on the machine.
+/- = Added or deleted from Resource List.
*   = Diagnostic support not available.
        
  Model Architecture: chrp
  Model Implementation: Uni-Processor, PCI bus
        
+ sys0                                                     System Object
+ sysplanar0                                               System Planar
* vio0                                                     Virtual I/O Bus
* ent0                                                     Virtual I/O Ethernet Adapter (l-lan)
* vsa0                                                     LPAR Virtual Serial Adapter
* vty0                                                     Asynchronous Terminal
* pci0                                                     PCI Bus
* scsi0            qemu_virtio-scsi-pci:0000:00:02.0       Virtio SCSI Client Adapter (f41a0800)
* hdisk0           qemu_virtio-scsi-pci:0000:00:02.0-LW_0  MPIO Other Virtio SCSI Disk Drive
+ L2cache0                                                 L2 Cache
+ mem0                                                     Memory
+ proc0                                                    Processor
</pre>


<a name="4"></a>
## 4. Further Storage Considerations

Adding **more disks** is easy. First you just need to create it with `qemu-img` (e.g. `qemu-img create -f qcow2 hdisk1.qcow2 20G`), then assign it to the existing VM starting command: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
  -cpu power8 -machine pseries -m 2048 \
  -serial stdio \
  -display none \
  -device virtio-scsi-pci,id=scsi \
  -device scsi-hd,drive=drive-virtio-disk0 \
  -drive file=aix7254srv1-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
  -device scsi-hd,drive=drive-virtio-disk1 \
  -drive file=aix7254srv1-hdisk1.qcow2,if=none,id=drive-virtio-disk1 \
  -prom-env "boot-command=boot disk:" \
  -net nic,macaddr=56:44:45:30:31:32 \
  -net tap,script=no,ifname=tap0</span>
[...]
</pre>


<a name="5"></a>
## 5. Different CPU Types and SMP Emulation Modes

### 5.1 CPU Types

The command `qemu-system-ppc64 -cpu ?` lists all the possible values for `-cpu` option, however, you will get the following error with most of them: 

<pre class="con">
[...]
qemu-system-ppc64: Unable to find sPAPR CPU Core definition
qemu-system-ppc64: network script /etc/qemu-ifdown failed with status 256
[...]
</pre>

For now, the originally recommended `-cpu power8 -machine pseries` set of parameters, without any other exotic (smp) config is the one that surely works. Also, this gives us `Processor Clock Speed: 1000 MHz` when checked with `prtconf`. Just because of this, I thought it worth trying other settings too. The ones `power9` and `power10` have the CPU Core definition in place, but they also fail. 

Here is a list of working `cpu` values for AIX 7.2 TL4 image (as of October 2022, with QEMU version `7.1.0`): 

- `power7_v2.3` (it shows `PowerPC_POWER7` as Type and `PV_7_Compat` as Processor Version)
- `power7` (same as previous)
- `power7+_v2.1` (same as previous)
- `power7+` (same as previous)
- `power8e_v2.1` (it shows `PowerPC_POWER8` as Type and `PV_8_Compat` as Processor Version)
- `power8e` (same as previous)
- `power8nvl_v1.0` (same as previous)
- `power8nvl` (same as previous)
- `power8_v2.0` (same as previous)
- `power8` (same as previous)

### 5.2 SMP Emulation Modes

The `-smp 2,cores=2,sockets=2,maxcpus=4` SMP configuration is accepted, it boots up, then immediately crashes with this message: 

<pre class="con">
[...]
**
ERROR:../accel/tcg/tcg-accel-ops-mttcg.c:112:mttcg_cpu_thread_fn: assertion failed: (cpu->halted)
Bail out! ERROR:../accel/tcg/tcg-accel-ops-mttcg.c:112:mttcg_cpu_thread_fn: assertion failed: (cpu->halted)
Aborted (core dumped)
</pre>

At least these `smp` settings **worked**: 

- `-cpu power8 -machine pseries -smp 1,cores=2,sockets=2,maxcpus=4`: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">lparstat -i | grep CPU</span>
Online Virtual CPUs                        : 1
Maximum Virtual CPUs                       : 4
Minimum Virtual CPUs                       : 1
Maximum Physical CPUs in system            : 4
Active Physical CPUs in system             : 4
Active CPUs in Pool                        : 4
Shared Physical CPUs in system             : -
Physical CPU Percentage                    : 100.00%
Desired Virtual CPUs                       : 1
</pre>

According to the above, my latest command to fire up the VM and to achieve as good performance as possible with one disk, with one network is this: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
  -cpu power8 -machine pseries -smp 1,cores=2,sockets=2,maxcpus=4 -m 2048 \
  -serial stdio \
  -display none \
  -device virtio-scsi-pci,id=scsi \
  -device scsi-hd,drive=drive-virtio-disk0 \
  -drive file=aix7254srv2-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
  -net nic,macaddr=56:44:45:30:31:32 \
  -net tap,script=no,ifname=tap0
  -prom-env "boot-command=boot disk:"</span>
[...]
</pre>

<a name="6"></a>
## 6. Additional Observations on PPC Emulation

### 6.1 RPM DB rebuild

According to my latest tests, this happens only if the very first, long boot did not finish successfully, and the the system was not shut down gracefully: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">oslevel -s</span>
error: db4 error(22) from dbenv->open: Invalid argument
error: cannot open Packages index using db4 - Invalid argument (22)
error: cannot open Packages database in /opt/freeware/packages
error: db4 error(22) from dbenv->open: Invalid argument
error: cannot open Packages index using db4 - Invalid argument (22)
error: cannot open Packages database in /opt/freeware/packages
7200-04-02-2027

<span class="psr">#</span> <span class="cmd">rpm -qa</span>
error: db4 error(22) from dbenv->open: A system call received a parameter that is not valid.
error: cannot open Packages index using db4 - A system call received a parameter that is not valid. (22)
error: cannot open Packages database in /opt/freeware/packages
error: db4 error(22) from dbenv->open: A system call received a parameter that is not valid.
error: cannot open Packages index using db4 - A system call received a parameter that is not valid. (22)
error: cannot open Packages database in /opt/freeware/packages
</pre>

The temporary solution is to rebuild the RPM DB (until next reboot(?)): 

<pre class="con">
<span class="psr">#</span> <span class="cmd">cd /opt/freeware</span>

<span class="psr">#</span> <span class="cmd">tar -chvf `date +"%d%m%Y"`.rpm.packages.tar packages</span>
[...]

<span class="psr">#</span> <span class="cmd">rm -f /opt/freeware/packages/__*</span>

<span class="psr">#</span> <span class="cmd">/usr/bin/rpm --rebuilddb</span>

<span class="psr">#</span> <span class="cmd">rpm -qa</span>
AIX-rpm-7.2.4.1-1.ppc
</pre>

If it is really happening on each boot, then better to create e.g. an `/etc/rc.startup` script for it (not the best solution as the issue itself is still present). 

[Source and more details](https://www.ibm.com/support/pages/how-resolve-rpm-dbrunrecovery-errors)

Even though this rebuild was done, an `rpm` process kept the CPU extremely busy. I just realized it only in the great `nmon` (with `c`, `m`, `t`): 

<pre class="con">
┌─topas_nmon──A=Async-I/O────────Host=aix72srv1──────Refresh=3 secs───11:47.23─────────┐
│ CPU-Utilisation-Small-View ───────────EntitledCPU=  1.00 UsedCPU=  1.000─────────────│
│Logical  CPUs              0----------25-----------50----------75----------100        │
│CPU User%  Sys% Wait% Idle%|           |            |           |            |        │
│  0  99.5   0.5   0.0   0.0|UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU>        │
│EntitleCapacity/VirtualCPU +-----------|------------|-----------|------------+        │
│ EC  98.5   1.5   0.0   0.0|UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU|        │
│ VP  98.5   1.5   0.0   0.0|UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU| sssssss│
│EC= 100.0%  VP= 100.0%     +--Capped---|------------|-----------100% VP=1 CPU+        │
│ Memory ──────────────────────────────────────────────────────────────────────────────│
│          Physical  PageSpace |        pages/sec  In     Out | FileSystemCache        │
│% Used       30.4%      0.9%  | to Paging Space   0.0    0.0 | (numperm)  4.7%        │
│% Free       69.6%     99.1%  | to File System    0.0    0.0 | Process    8.4%        │
│MB Used     622.2MB     4.5MB | Page Scans        0.0        | System    17.3%        │
│MB Free    1425.8MB   507.5MB | Page Cycles       0.0        | Free      69.6%        │
│Total(MB)  2048.0MB   512.0MB | Page Steals       0.0        |           ------       │
│                              | Page Faults       0.0        | Total    100.0%        │
│------------------------------------------------------------ | numclient  4.7%        │
│Min/Maxperm      59MB(  3%)  1756MB( 90%) <--% of RAM        | maxclient 90.0%        │
│Min/Maxfree     960   1088       Total Virtual    2.5GB      | User      10.0%        │
│Min/Maxpgahead    2      8    Accessed Virtual    0.5GB 19.9%| Pinned    23.1%        │
│                                                             | lruable pages   499536.│
│ Top-Processes-(79) ──────Mode=3  [1=Basic 2=CPU 3=Perf 4=Size 5=I/O 6=Cmds]──────────│
│  PID     %CPU  Size   Res   Res   Res  Char RAM      Paging       Command            │
│          Used    KB   Set  Text  Data   I/O  Use io other repage                     │
│ 4718870  98.3  1140  1160    28  1132     0   0%    0    0    0 rpm                  │
│ 4784572   0.2  4516  4280   396  3884     0   1%    0    0    0 topas_nmon           │
│       0   0.1    68    68     0    68     0   0%    0    0    0 Swapper              │
│  786716   0.1   124   124     0   124     0   0%    0    0    0 gil = TCP/IP         │
│ 3539356   0.0  1292  1088   332   756     0   0%    0    0    0 snmpmibd             │
│ 3998082   0.0  1500  1308   184  1124     0   0%    0    0    0 aixmibd              │
│ 5767428   0.0  2044  2212   968  1244     0   0%    0    0    0 sshd                 │
│ 4325768   0.0  1048  1012   128   884     0   0%    0    0    0 hostmibd             │
│ 3867002   0.0  2748  2176   712  1464     0   0%    0    0    0 sendmail             │
│  983330   0.0    60    60     0    60     0   0%    0    0    0 kvm_dr_kproc         │
└──────────────────────────────────────────────────────────────────────────────────────┘
</pre>

The process was `/usr/opt/rpm/bin/rpm -q --queryformat %{NAME}? ...`. Killed it, then rebuilt the RPM DB once again, then rebooted the system. It worked!


### 6.2 Possible `fsck` issues during first boot

If you experience the `fsck` issue just like that's described in the original article about this concept, boot it again with the installation DVD, go into the **Maintenance Mode**, then fix it: 

When the "Welcome to Base Operating System" menu appears:  choose `3` (Start Maintenance Mode) --> `1` (Access a Root VG) -->  `0` Continue  --> `1` (VG on hdisk0) --> `1` (Access this VG and start a shell). 

After that, we will get a prompt with an environment where filesystems are already mounted:

<pre class="con">
<span class="psr">#</span> <span class="cmd">cd /sbin/helpers/jfs2</span>

<span class="psr">#</span> <span class="cmd">cat > fsck64</span>
#!/bin/ksh
exit 0

<span class="psr">#</span> <span class="cmd">cat fsck64</span>
[...]

<span class="psr">#</span> <span class="cmd">sync</span>

<span class="psr">#</span> <span class="cmd">shutdown -F</span>
[...]
</pre>

### 6.3 Replacing `arp` by `ip neigh`

The original article mentioned the `arp` command, but that's not available on many distributions anymore (for commands like `arp -Ds 10.0.2.16 enp0s3 pub`), so here is the equivalent with `ip neigh`: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">ip neigh add proxy 192.168.101.103 dev tap0</span>

<span class="psr">#</span> <span class="cmd">ip neigh show proxy</span>
192.168.101.103 dev tap0 proxy
</pre> 

Found the solution [here](https://lwn.net/Articles/710863/). 

Note that when the guest goes down for some minutes, then the `arp_evict_nocarrier` event **clears up the proxy**, so you need to check it by `ip neigh show proxy` every time before you stat the guest, and restore it if needed, otherwise you get this error even if you used one of the proper `cpu` values: 

<pre class="con">
[...]
qemu-system-ppc64: OS terminated: 888 102 700 C20 
qemu-system-ppc64: network script /etc/qemu-ifdown failed with status 256
</pre>

A lame script for monitoring/restoring it automatically: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">while true ; do if [ $(ip neigh show proxy | wc -l) -lt 1 ]; then echo restoring... ; sudo ip neigh add proxy 192.168.101.103 dev tap0 ; else echo "OK ($(date))" ; fi ; sleep 5 ; done</span>
</pre>

### 6.4 An alternative networking solution, using `br0` interface

It is not a proven solution, just put some commands together with some comments that I've found under the original article (referenced in the beginning). 

1. Prepare host:

<pre class="con">
<span class="psr">#</span> <span class="cmd">nmcli con show</span>
NAME                UUID                                  TYPE      DEVICE 
wlp3s0              4a79f300-0bcf-3b09-835f-37ba3bd81b15  wifi      wlp3s0 
Wired connection 1  0df75304-e7d4-3fa4-93eb-a0ec2c155e3e  ethernet  --     

<span class="psr">#</span> <span class="cmd">nmcli con add type bridge ifname br0</span>
Connection 'bridge-br0' (c63d64ce-8571-4437-b699-e5fbe3f24ef0) successfully added.

<span class="psr">#</span> <span class="cmd">nmcli con add type bridge-slave ifname wlp3s0 master br0</span>
Connection 'bridge-slave-wlp3s0' (7a42b734-2d51-436e-a602-02d59fc46daa) successfully added.

<span class="psr">#</span> <span class="cmd">nmcli con show</span>
NAME                 UUID                                  TYPE      DEVICE 
bridge-br0           c63d64ce-8571-4437-b699-e5fbe3f24ef0  bridge    br0    
wlp3s0               4a79f300-0bcf-3b09-835f-37ba3bd81b15  wifi      wlp3s0 
Wired connection 1   0df75304-e7d4-3fa4-93eb-a0ec2c155e3e  ethernet  --     
bridge-slave-wlp3s0  7a42b734-2d51-436e-a602-02d59fc46daa  ethernet  --     
</pre>

2. Enable using this `br0` interface for QEMU: 

<pre class="con">
<span class="psr">#</span> <span class="cmd">echo "allow br0" >> /etc/qemu/bridge.conf</span>
</pre>

3. Start AIX with this new interface as `root` with the `-net bridge,id=n1,br=br0` parameter.


Even booting with debug mode is possible if needed. It uses the optional feature of the AIX DVD's `bootfile.exe` that is trying to load the installer:

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 \
  -cpu power8 -machine pseries -m 2048 \
  -serial stdio \
  -device virtio-scsi-pci,id=scsi \
  -device scsi-hd,drive=drive-virtio-disk0 \
  -drive file=aix7254srv2-hdisk0.qcow2,if=none,id=drive-virtio-disk0 \
  -cdrom aix-7.2-tl5sp4-2219-1of2.iso \
  -prom-env "boot-command=boot cdrom:\ppc\chrp\bootfile.exe -s verbose"</span>
[...]
</pre>

### 6.5 Other Linux distros and even other host architectures

Tried starting the prebuilt image up under an Orange Pi 4 LTS, using the official Debian and Ubuntu distros. I failed to set up the above described networking on Debian 11 "Bullseye", so moved on to the bit more modern Ubuntu and that was all right. 

Both on Debian and Ubuntu, you need `qemu-utils` and `qemu-system-ppc` packages, or you may also compile the software **from source**. That should work also, and maybe it gives you better performance, more cpu settings, higher clock speed etc. 

Also, `vgabios` package must be installed and its "romfile" symlinked before starting the VM like this: 

<pre class="con">
<span class="psr">root@orangepi4-lts:~#</span> <span class="cmd">qemu-system-ppc64 -cpu power8 -machine pseries -m 2048 -serial stdio -display none -drive file=hdisk0.qcow2,if=none,id=drive-virtio-disk0 -device virtio-scsi-pci,id=scsi -device scsi-hd,drive=drive-virtio-disk0 -prom-env "boot-command=boot disk:" -net nic,macaddr=56:44:45:30:31:32 -net tap,script=no,ifname=tap0
qemu-system-ppc64: failed to find romfile "vgabios-stdvga.bin"</span>

<span class="psr">root@orangepi4-lts:~#</span> <span class="cmd">apt-get install vgabios</span>

<span class="psr">root@orangepi4-lts:~#</span> <span class="cmd">ls -l /usr/share/qemu</span>
[...]

<span class="psr">root@orangepi4-lts:~#</span> <span class="cmd">ln -s /usr/share/vgabios/vgabios-stdvga.bin /usr/share/qemu/vgabios-stdvga.bin</span>

<span class="psr">root@orangepi4-lts:~#</span> <span class="cmd">qemu-system-ppc64 -cpu power8 -machine pseries -m 2048 -serial stdio -display none -drive file=hdisk0.qcow2,if=none,id=drive-virtio-disk0 -device virtio-scsi-pci,id=scsi -device scsi-hd,drive=drive-virtio-disk0 -prom-env "boot-command=boot disk:" -net nic,macaddr=56:44:45:30:31:32 -net tap,script=no,ifname=tap0</span>
[...]
  Welcome to Open Firmware
[...]
</pre>

It took 15 minutes to boot it up on this device but well worth it! 😅

### 6.6 Other AIX install DVDs

At least the latest (as of Oct. 2022) **AIX 7.3 TL0 SP2** install DVDs do not have the trick that gets it booted: 

<pre class="con">
[...]
Trying to load:  from: /vdevice/v-scsi@71000003/disk@8200000000000000: ...   Successfully loaded

This is an unsupported configuration.


W3411: Client application returned.

E3406: Client application returned an error.

  Type 'boot' and press return to continue booting the system.
  Type 'reset-all' and press return to reboot the system.


Ready! 
0 > 
[...]
</pre>

According to IBM's [AIX 7.3 Release Notes Document](https://www.ibm.com/docs/en/aix/7.3?topic=notes-aix-73-release#SC23662906-gen10), only the "PAPR" implemented environments are supported, so probably a QEMU environment is not enough yet, and probably that's not PAPR-compliant: 

>Only 64-bit Common Hardware Reference Platform (CHRP) machines that are running selected POWER8®, POWER9™, and Power10 processors in POWER8, or later, processor compatibility mode that implement the Power Architecture® Platform Requirements (PAPR) are supported.  
>IBM Hyperconverged Systems, powered by Nutanix, with POWER8 processor-based servers (CS821 and CS822 models) do not support AIX 7.3.

The current `-machine pseries` refers to `pseries-7.1` and that can be the reason: 

<pre class="con">
<span class="psu">$</span> <span class="cmd">qemu-system-ppc64 -machine ?</span>
Supported machines are:
[...]
pseries              pSeries Logical Partition (PAPR compliant) (alias of pseries-7.1)
pseries-7.1          pSeries Logical Partition (PAPR compliant) (default)
[...]
</pre>

So, there is no so much to do for AIX 7.3 for now. Either building QEMU from source or simply waiting for more recent `pseries` types to be implemented in QEMU's codebase might be needed. (To be continued on that...)

### 6.7 CPU usage of the host

My testing environment was a Dell laptop with a 4 cores / 8 threads 11th Gen i7-1185G7 CPU, and it was cooling itself heavily sometimes. By default SMP settings, having one emulated CPU cores only, always only one of the physical threads were used heavily, and this load was bouncing between them in every 5-15 seconds: 

![alt_text](qemu_-_host_cpu_usage.png)
