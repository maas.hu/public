# ![alt text](/_ui/img/icons/blender_m.svg) Blender - Video Editing

<div style="text-align: right;"><h7>Posted: 2023-09-26 00:13</h7></div>

###### .


## Table of Contents

1. [Introduction](#1)
2. [The User Interface](#2)
3. [Rendering the Output](#3)
4. [...](#4)
5. [...](#5)


<a name="1"></a>
## 1. Introduction

Besides all the great 3D and 2D scene, animation and even computer game development features, Blender can be considered also as a complete **Video Editing suite**. 

A great, 4 episodes tutorial YouTube video series can be found [here](https://www.youtube.com/watch?v=yh7YPabqeDk&list=PLsGl9GczcgBs0r5xcKL7KvrmGoRuro8RV). In this article, I'm just summarizing the essencials that I had to use for kind of an "everyday usage". 

<a name="2"></a>
## 2. The User Interface

### 2.1 Entering to Video Editing Mode

There are at least three different ways to enter into this mode of the program. 

1. The quickest is when you choose it right away when it's started or whenever you open its Splash Screen (click on the Blender icon in the top left corner → **Splash Screen** menu): 

![alt_text](blender_-_video_editing_-_splash.png)

2. The other option can be chosen even after you entered to the program's General view already. Just click on the + sign of the main menu and choose **Video Editing** → **Video Editing**: 

![alt_text](blender_-_video_editing_-_add_workspace.png)

3. The third option is when you have your own, customized interface already that you can just load. 

### 2.2 The Sequencer

Here you can find the bottom area of the whole Blender window to overview the general controls in and around the Sequencer: 

![alt_text](blender_-_video_editing_-_sequencer.png)

By default, the video to be rendered is only a "demo" one, so it lasts 250 frames only. When you insert a long video and you want to adjust to it, you can just select the video, copy it's **Duration** from the **Strip** tab of the **Side Panel**, then paste the number as an **End Frame**. 

### 2.3 Adding Strips to Sequencer

You can just drag & drop either from Blender's internal file browser or from your desktop environment's file manager. You can add a **Movie clip**, **Sound**, **Image** or **Image Sequence** files as new objects. 

For **positioning** your inserted object, keep in mind that the precise positioning, by frame, can be done when you press the `G` key (**Move**). Further transformations and their keys are under **Strip** → **Transform** menu of the Sequencer. 


<a name="3"></a>
## 3. Rendering the Output

Once you are satisfied with the scene, the length of it is set, all the unnecessary audio is silenced etc., you can now save it to a video file by **rendering**. 

Before you just start rendering, you need to set some **Output settings**. These can be found in the **Output** tab of the **Side Panel**: 

1. Under **Format** section, set the **Resolution** and **Frame Rate** if needed. The settings here are fetched from the first video you attached to the scene, so probably these are already fine:  
![alt_text](blender_-_video_editing_-_output_-_1.png)
2. Under **Output** section, set the folder you want to have your video file instead of the default `/tmp` (on Linux). Also ensure the **File Format** is a video format, and not the default `PNG` one. This **FFmpeg Video** is usually perfect:  
![alt_text](blender_-_video_editing_-_output_-_2.png)
3. Ensure the **Color Management** remained this **Standard** with **None** for **Lock**. **Override** if necessary:  
![alt_text](blender_-_video_editing_-_output_-_3.png)
4. Set the preferred **Container** for **Encoding** and the **Video Codec**. The below settings with `H.264` usually result great output:  
![alt_text](blender_-_video_editing_-_output_-_4.png)
5. Change the default **No Audio** to a proper **Audio Codec**, such as `AAC`:  
![alt_text](blender_-_video_editing_-_output_-_5.png)
6. Then just press `[Ctrl+F12]` for rendering the scene, meaning, saving it to a video file. 


<a name="4"></a>
## 4. ...

...


<a name="5"></a>
## 5. ...

...
