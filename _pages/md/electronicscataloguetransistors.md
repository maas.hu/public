# 🪤 Transistors

<div style="text-align: right;"><h7>Posted: 2022-11-07 21:49</h7></div>

###### .

![alt text](transistors_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [2N... Series](#2)
3. [2S... Series](#3)
4. [B... Series](#4)
5. [F... Series](#5)
6. [IR..., IS... Series](#6)
7. [J... Series](#7)
8. [LND... Series](#8)
9. [N... Series](#9)
10. [STS... Series](#10)
11. [TIP... Series](#11)
12. [U... Series](#12)


<a name="1"></a>
## 1. Introduction

The below tables contain **only a tiny, selected portion** of the incredibly wide range of transistor types from all around the world. I selected those, which I...
- found at least a little bit interesting or
- found as relatively popular among hobbysts or
- found in multiple devices to be desoldered

I also tried collecting **complementary pairs**, whenever it was possible. 

⚠️ Please note that the **test conditions** for the officially measured electrical characteristics (e.g. *"Emitter Base Breakdown Voltage"*) almost **always differ**! Refer for the test conditions in the **linked datasheets**!


<a name="2"></a>
## 2. 2N... Series

These parts were originally numbered by **JEDEC**, the **Joint Electron Device Engineering Council**, and their numbering system dates back to '60s, when the first "1N..." diodes appeared. The "1" stood for "No filament/heater" and the "N" stood for "crystal rectifier". Later on, the meaning of the first digit became "p-n junction count", so the first BJTs under JEDEC's supervision all started using the "2N..." numbering. The 2N2222 NPN BJT appeared in 1962, and it's still a classic; probably the most successful transistor type of all time. 

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N2222.pdf">2N2222</a></b></td>
    <td>BJT, NPN</td>
    <td><b>60 V</b> (V<sub>CBO</sub>)<br><b>600 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>250 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2N2907</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N2608-2N2609.pdf">2N2608</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>-10 mA</b> (I<sub>GF</sub>)<br><b>300 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>1 V</b><br>max. C<sub>iss</sub> = <b>17 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N2608-2N2609.pdf">2N2609</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>-10 mA</b> (I<sub>GF</sub>)<br><b>300 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>1 V</b><br>max. C<sub>iss</sub> = <b>30 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N2907.pdf">2N2907</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-60 V</b> (V<sub>CBO</sub>)<br><b>-600 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>200 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>2N2222</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N3773-2N6609.pdf">2N3773</a></b></td>
    <td>BJT, NPN</td>
    <td><b>160 V</b> (V<sub>CBO</sub>)<br><b>16000 mA</b> (I<sub>C</sub>)<br><b>150000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>(BR)EBO</sub> = <b>7 V</b></td>
    <td>TO-204<br><img src="to-204_-_bec.svg"></td>
    <td>2N6609</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N3819.pdf">2N3819</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>25 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>300 mW</b> (P<sub>D</sub>)</td>
    <td>max. V<sub>(GS)OFF</sub> = <b>8 V</b><br>max. C<sub>iss</sub> = <b>8 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N5460-2N5461-2N5462.pdf">2N5460</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>40 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>0.75 V</b><br>max. C<sub>iss</sub> = <b>7 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N5460-2N5461-2N5462.pdf">2N5461</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>40 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>1 V</b><br>max. C<sub>iss</sub> = <b>7 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N5460-2N5461-2N5462.pdf">2N5462</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>40 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>1.8 V</b><br>max. C<sub>iss</sub> = <b>7 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N3773-2N6609.pdf">2N6609</a></b></td>
    <td>BJT, NPN</td>
    <td><b>-160 V</b> (V<sub>CBO</sub>)<br><b>-16000 mA</b> (I<sub>C</sub>)<br><b>150000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>(BR)EBO</sub> = <b>-7 V</b></td>
    <td>TO-204<br><img src="to-204_-_bec.svg"></td>
    <td>2N3773</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2N7000.pdf">2N7000</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>200 mA</b> (I<sub>D</sub>)<br><b>625 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>0.8 V</b><br>typ. C<sub>iss</sub> = <b>60 pF</b></td>
    <td>TO-92<br><img src="to-92_-_sgd.svg"></td>
    <td>-</td>
  </tr>
</table>


<a name="3"></a>
## 3. 2S... Series

The originally **Japanese** "2S" prefix is similar to the American "2N" one in that it also identifies a 2-junction device, while the a letter "S" in this case after the first digit simply follows the standard for "tubes".

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SA1538.pdf">2SA1538</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-120 V</b> (V<sub>CBO</sub>)<br><b>-200 mA</b> (I<sub>C</sub>)<br><b>1300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-3 V</b><br>typ. f<sub>T</sub> = <b>400 MHz</b></td>
    <td>TO-126<br><img src="to-126_-_ecb.svg"></td>
    <td>2SC3953</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SA1797.pdf">2SA1797</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>-2000 mA</b> (I<sub>C</sub>)<br><b>600 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-6 V</b><br>typ. f<sub>T</sub> = <b>200 MHz</b></td>
    <td>SOT-89<br><img src="sot-89_-_bce.svg"></td>
    <td>2SC4672</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SA1943.pdf">2SA1943</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-230 V</b> (V<sub>CBO</sub>)<br><b>-15000 mA</b> (I<sub>C</sub>)<br><b>150000 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>typ. f<sub>T</sub> = <b>30 MHz</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_bce.svg"></td>
    <td>2SC5200</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SB772.pdf">2SB772</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-40 V</b> (V<sub>CBO</sub>)<br><b>3000 mA</b> (I<sub>C</sub>)<br><b>500 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>typ. f<sub>T</sub> = <b>80 MHz</b></td>
    <td>TO-252<br><img src="to-252_-_bce.svg"></td>
    <td>2SD882</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SB772T.pdf">2SB772T</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-40 V</b> (V<sub>CBO</sub>)<br><b>3000 mA</b> (I<sub>C</sub>)<br><b>500 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>typ. f<sub>T</sub> = <b>80 MHz</b></td>
    <td>SOT-89 ("B772")<br><img src="sot-89_-_bce.svg"></td>
    <td>2SD882T</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC2625.pdf">2SC2625</a></b></td>
    <td>BJT, NPN</td>
    <td><b>450 V</b> (V<sub>CBO</sub>)<br><b>10000 mA</b> (I<sub>C</sub>)<br><b>80000 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>7 V</b><br>max. t<sub>ON</sub> = <b>1 μs</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_bce.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC3950.pdf">2SC3950</a></b></td>
    <td>BJT, NPN</td>
    <td><b>30 V</b> (V<sub>CBO</sub>)<br><b>500 mA</b> (I<sub>C</sub>)<br><b>1300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>3 V</b><br>typ. f<sub>T</sub> = <b>2 GHz</b></td>
    <td>TO-126<br><img src="to-126_-_ecb.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC3953.pdf">2SC3953</a></b></td>
    <td>BJT, NPN</td>
    <td><b>120 V</b> (V<sub>CBO</sub>)<br><b>200 mA</b> (I<sub>C</sub>)<br><b>1300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>3 V</b><br>typ. f<sub>T</sub> = <b>400 MHz</b></td>
    <td>TO-126<br><img src="to-126_-_ecb.svg"></td>
    <td>2SA1538</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC4672.pdf">2SC4672</a></b></td>
    <td>BJT, NPN</td>
    <td><b>60 V</b> (V<sub>CBO</sub>)<br><b>3000 mA</b> (I<sub>C</sub>)<br><b>900 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>6 V</b><br>typ. f<sub>T</sub> = <b>210 MHz</b></td>
    <td>SOT-89<br><img src="sot-89_-_bce.svg"></td>
    <td>2SA1797</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC5200.pdf">2SC5200</a></b></td>
    <td>BJT, NPN</td>
    <td><b>230 V</b> (V<sub>CBO</sub>)<br><b>15000 mA</b> (I<sub>C</sub>)<br><b>150000 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>30 MHz</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_bce.svg"></td>
    <td>2SA1943</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC9012.pdf">2SC9012</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-40 V</b> (V<sub>CBO</sub>)<br><b>-500 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2SC9013</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC9013.pdf">2SC9013</a></b></td>
    <td>BJT, NPN</td>
    <td><b>40 V</b> (V<sub>CBO</sub>)<br><b>500 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2SC9012</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC9014.pdf">2SC9014</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>450 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2SC9015</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SC9015.pdf">2SC9015</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>450 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2SC9014</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SD882.pdf">2SD882</a></b></td>
    <td>BJT, NPN</td>
    <td><b>40 V</b> (V<sub>CBO</sub>)<br><b>3000 mA</b> (I<sub>C</sub>)<br><b>500 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>80 MHz</b></td>
    <td>TO-252<br><img src="to-252_-_bce.svg"></td>
    <td>2SB772</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SD882T.pdf">2SD882T</a></b></td>
    <td>BJT, NPN</td>
    <td><b>40 V</b> (V<sub>CBO</sub>)<br><b>3000 mA</b> (I<sub>C</sub>)<br><b>500 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>80 MHz</b></td>
    <td>SOT-89 ("D882")<br><img src="sot-89_-_bce.svg"></td>
    <td>2SB772T</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ186.pdf">2SJ186</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-200 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>500 mA</b> (I<sub>D</sub>)<br><b>1000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>75 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK1334</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ317.pdf">2SJ317</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-12 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-2000 mA</b> (I<sub>D</sub>)<br><b>1000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-0.4 V</b><br>typ. C<sub>iss</sub> = <b>63 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK2549</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ360.pdf">2SJ360</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-1000 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>0.8 V</b><br>typ. C<sub>iss</sub> = <b>155 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK3065</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ363.pdf">2SJ363</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-30 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-2000 mA</b> (I<sub>D</sub>)<br><b>1000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>2.1 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK2103</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ377.pdf">2SJ377</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>5000 mA</b> (I<sub>D</sub>)<br><b>20000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-0.8 V</b><br>typ. C<sub>iss</sub> = <b>630 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>2SK2231</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ518.pdf">2SJ518</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-2000 mA</b> (I<sub>D</sub>)<br><b>1000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>220 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK2615</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ580.pdf">2SJ580</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-1800 mA</b> (I<sub>D</sub>)<br><b>1500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>270 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SK2615</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ598.pdf">2SJ598</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>-12000 mA</b> (I<sub>D</sub>)<br><b>23000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>-1.5 V</b><br>typ. C<sub>iss</sub> = <b>720 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>2SK3377</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SJ74.pdf">2SJ74</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>25 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>-10 mA</b> (I<sub>GF</sub>)<br><b>400 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>0.15 V</b><br>typ. C<sub>iss</sub> = <b>105 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>2SK170</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK1334.pdf">2SK1334</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>200 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>1000 mA</b> (I<sub>D</sub>)<br><b>1000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>2 V</b><br>typ. C<sub>iss</sub> = <b>80 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SJ186</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK170.pdf">2SK170</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>-40 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>400 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>0.2 V</b><br>typ. C<sub>iss</sub> = <b>30 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>2SJ74</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK2103.pdf">2SK2103</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>30 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>2000 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>230 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SJ363</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK2231.pdf">2SK2231</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>5000 mA</b> (I<sub>D</sub>)<br><b>20000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>0.8 V</b><br>typ. C<sub>iss</sub> = <b>370 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>2SJ377</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK2549.pdf">2SK2549</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>16 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>2000 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>0.5 V</b><br>typ. C<sub>iss</sub> = <b>260 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SJ317</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK2615.pdf">2SK2615</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>2000 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>0.8 V</b><br>typ. C<sub>iss</sub> = <b>150 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SJ518, 2SJ580</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK3065.pdf">2SK3065</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>2000 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>0.8 V</b><br>typ. C<sub>iss</sub> = <b>160 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>2SJ360</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/2SK3377.pdf">2SK3377</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>, V<sub>GS</sub>)<br><b>20000 mA</b> (I<sub>D</sub>)<br><b>30000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(off)</sub> = <b>1.5 V</b><br>typ. C<sub>iss</sub> = <b>760 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>2SJ598</td>
  </tr>
</table>


<a name="4"></a>
## 4. B... Series

This series was always the most popular one in Europe and it follows the most traditional letters too probably, such as: 

```
2 letters + 3 numbers (+1 letter for optional subtypes)
```

Based on this, for **the first letter**, we can instantly notice, e.g.: 

- `A`: germanium crystalline
- `B`: silicon crystalline
- `C`: a special material (e.g. gallaium-arsenid)

For the **second letter**, e.g.:

- `C`: audio frequency, small signal transistor
- `D`: audio frequency, large signal transistor
- `F`: high frequency, small signal
- `L`: high frequency, large signal
- `S`: switching transistor, small signal
- `U`: switching transistor, large signal

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC107-BC108-BC109.pdf">BC107</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>200 mA</b> (I<sub>C</sub>)<br><b>600 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>6 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-18<br><img src="to-18_-_ebc.svg"></td>
    <td>BC177</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC107-BC108-BC109.pdf">BC109</a></b></td>
    <td>BJT, NPN</td>
    <td><b>30 V</b> (V<sub>CBO</sub>)<br><b>200 mA</b> (I<sub>C</sub>)<br><b>600 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-18<br><img src="to-18_-_ebc.svg"></td>
    <td>BC179</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC140-BC-141.pdf">BC141</a></b></td>
    <td>BJT, NPN</td>
    <td><b>100 V</b> (V<sub>CBO</sub>)<br><b>1000 mA</b> (I<sub>C</sub>)<br><b>800 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>7 V</b><br>min. f<sub>T</sub> = <b>50 MHz</b></td>
    <td>TO-39<br><img src="to-39_-_ebc.svg"></td>
    <td>BC161</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC160-BC161.pdf">BC161</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-60 V</b> (V<sub>CBO</sub>)<br><b>-1000 mA</b> (I<sub>C</sub>)<br><b>800 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5</b><br>min. f<sub>T</sub> = <b>50 MHz</b></td>
    <td>TO-39<br><img src="to-39_-_ebc.svg"></td>
    <td>BC141</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC177-BC178-BC179.pdf">BC177</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CES</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5</b><br>min. f<sub>T</sub> = <b>130 MHz</b></td>
    <td>TO-18<br><img src="to-18_-_ebc.svg"></td>
    <td>BC107</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC177-BC178-BC179.pdf">BC179</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-25 V</b> (V<sub>CES</sub>)<br><b>-50 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5</b><br>min. f<sub>T</sub> = <b>130 MHz</b></td>
    <td>TO-18<br><img src="to-18_-_ebc.svg"></td>
    <td>BC109</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC182.pdf">BC182</a></b></td>
    <td>BJT, NPN</td>
    <td><b>60 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>350 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>6 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC212</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC212.pdf">BC212</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-60 V</b> (V<sub>CBO</sub>)<br><b>-200 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>200 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC182</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC337.pdf">BC337</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>800 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>210 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC327</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC327.pdf">BC327</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>-800 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>260 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC337</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC413-BC414.pdf">BC413</a></b></td>
    <td>BJT, NPN</td>
    <td><b>45 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>250 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC415</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC413-BC414.pdf">BC414</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>typ. f<sub>T</sub> = <b>250 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC416</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC415-BC416.pdf">BC415</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-45 V</b> (V<sub>CBO</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>typ. f<sub>T</sub> = <b>200 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC413</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC415-BC416.pdf">BC416</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>300 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>typ. f<sub>T</sub> = <b>200 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC416</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC546-BC547-BC548.pdf">BC546</a></b></td>
    <td>BJT, NPN</td>
    <td><b>80 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>6 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b><br>(typ.: 300 MHz)</td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC556</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC546-BC547-BC548.pdf">BC547</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>6 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b><br>(typ.: 300 MHz)</td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC557</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC556-BC557-BC558.pdf">BC556</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-80 V</b> (V<sub>CBO</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b><br>(typ.: 300 MHz)</td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC546</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BC556-BC557-BC558.pdf">BC557</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>-100 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b><br>(typ.: 300 MHz)</td>
    <td>TO-92<br><img src="to-92_-_cbe.svg"></td>
    <td>BC547</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BD249.pdf">BD249C</a></b></td>
    <td>BJT, NPN</td>
    <td><b>100 V</b> (V<sub>CBO</sub>)<br><b>25000 mA</b> (I<sub>C</sub>)<br><b>125000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)-2</sub> = <b>4 V</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_bce.svg"></td>
    <td>BC547</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BD250.pdf">BD250C</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-100 V</b> (V<sub>CBO</sub>)<br><b>-25000 mA</b> (I<sub>C</sub>)<br><b>125000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)-2</sub> = <b>-3 V</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_bce.svg"></td>
    <td>BC547</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF244.pdf">BF244</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-0.5 V</b><br>typ. C<sub>iss</sub> = <b>3 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF245.pdf">BF245</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>100 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-0.5 V</b><br>typ. C<sub>iss</sub> = <b>3 pF</b></td>
    <td>TO-92<br><img src="to-92_-_gsd.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF256.pdf">BF256</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>10 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-0.5 V</b></td>
    <td>TO-92<br><img src="to-92_-_gsd.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF960.pdf">BF960</a></b></td>
    <td>D-MOSFET,<br>N-Channel<br>Dual Gate</td>
    <td><b>20 V</b> (V<sub>DS</sub>)<br><b>30 mA</b> (I<sub>GF</sub>)<br><b>200 mW</b> (P<sub>D</sub>)</td>
    <td>max. V<sub>(G1/2S)OFF</sub> = <b>2.7 V</b><br>typ. C<sub>iss</sub> = <b><br>1.8 (G1) / 1.0 (G2) pF</b></td>
    <td>SOT-103<br><img src="sot-103_-_dsg1g2.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF961.pdf">BF961</a></b></td>
    <td>D-MOSFET,<br>N-Channel<br>Dual Gate</td>
    <td><b>20 V</b> (V<sub>DS</sub>)<br><b>30 mA</b> (I<sub>GF</sub>)<br><b>200 mW</b> (P<sub>D</sub>)</td>
    <td>max. V<sub>(G1/2S)OFF</sub> = <b>3.5 V</b><br>typ. C<sub>iss</sub> = <b><br>3.7 (G1) / 1.6 (G2) pF</b></td>
    <td>SOT-103<br><img src="sot-103_-_dsg1g2.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF980.pdf">BF980</a></b></td>
    <td>D-MOSFET,<br>N-Channel<br>Dual Gate</td>
    <td><b>18 V</b> (V<sub>DS</sub>)<br><b>30 mA</b> (I<sub>GF</sub>)<br><b>225 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(G1/2S)OFF</sub> = <b>0.2 V</b><br>typ. C<sub>ig1-s</sub> = <b><br>3 pF</b></td>
    <td>SOT-103<br><img src="sot-103_-_dsg1g2.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BF981.pdf">BF981</a></b></td>
    <td>D-MOSFET,<br>N-Channel<br>Dual Gate</td>
    <td><b>20 V</b> (V<sub>DS</sub>)<br><b>20 mA</b> (I<sub>GF</sub>)<br><b>225 mW</b> (P<sub>D</sub>)</td>
    <td>max. V<sub>(G1/2S)OFF</sub> = <b>2.5 V</b><br>typ. C<sub>iss</sub> = <b><br>2.1 (G1) / 1.0 (G2) pF</b></td>
    <td>SOT-103<br><img src="sot-103_-_dsg1g2.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BS107.pdf">BS107</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>200 V</b> (V<sub>DS</sub>)<br><b>250 mA</b> (I<sub>D</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>60 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>BS208</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BS170.pdf">BS170</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>)<br><b>-500 mA</b> (I<sub>D</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>0.8 V</b><br>max. C<sub>iss</sub> = <b>60 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>BS250</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BS208.pdf">BS208</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-240 V</b> (V<sub>DS</sub>)<br><b>-200 mA</b> (I<sub>D</sub>)<br><b>830 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-0.8 V</b><br>typ. C<sub>iss</sub> = <b>200 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>BS107</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BS250.pdf">BS250</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>)<br><b>-250 mA</b> (I<sub>D</sub>)<br><b>830 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>60 pF</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>BS170</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSP129.pdf">BSP129</a></b></td>
    <td>D-MOSFET,<br>N-Channel</td>
    <td><b>240 V</b> (V<sub>DS</sub>)<br><b>350 mA</b> (I<sub>D</sub>)<br><b>1800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2.1 V</b><br>typ. C<sub>iss</sub> = <b>82 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSP135.pdf">BSP135</a></b></td>
    <td>D-MOSFET,<br>N-Channel</td>
    <td><b>600 V</b> (V<sub>DS</sub>)<br><b>120 mA</b> (I<sub>D</sub>)<br><b>1800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2.1 V</b><br>typ. C<sub>iss</sub> = <b>98 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSP149.pdf">BSP149</a></b></td>
    <td>D-MOSFET,<br>N-Channel</td>
    <td><b>200 V</b> (V<sub>DS</sub>)<br><b>480 mA</b> (I<sub>D</sub>)<br><b>1800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1.8 V</b><br>typ. C<sub>iss</sub> = <b>500 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSP315P.pdf">BSP315P</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>)<br><b>-1170 mA</b> (I<sub>D</sub>)<br><b>1800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>130 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>BSP318S</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSP318S.pdf">BSP318S</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>)<br><b>2600 mA</b> (I<sub>D</sub>)<br><b>1800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1.2 V</b><br>typ. C<sub>iss</sub> = <b>300 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>BSP315P</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/BSS169.pdf">BSS169</a></b></td>
    <td>D-MOSFET,<br>N-Channel</td>
    <td><b>100 V</b> (V<sub>DS</sub>)<br><b>170 mA</b> (I<sub>D</sub>)<br><b>360 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>28 pF</b></td>
    <td>SOT-89<br><img src="sot-89_-_gds.svg"></td>
    <td>-</td>
  </tr>
</table>


<a name="5"></a>
## 5. F... Series

From **ON Semiconductor** (originally from **Fairchild Semiconductor**).

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/FQA46N15.pdf">FQA46N15</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>150 V</b> (V<sub>DS</sub>)<br><b>50000 mA</b> (I<sub>D</sub>)<br><b>250000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>2 V</b><br>typ. C<sub>iss</sub> = <b>2500 pF</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_gds.svg"></td>
    <td>FQA36P15</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/FQA36P15.pdf">FQA36P15</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-150 V</b> (V<sub>DS</sub>)<br><b>-36000 mA</b> (I<sub>D</sub>)<br><b>294000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>2550 pF</b></td>
    <td>TO-3PN<br><img src="to-3pn_-_gds.svg"></td>
    <td>FQA46N15</td>
  </tr>
</table>


<a name="6"></a>
## 6. IR..., IS... Series

These are "High Power, Fast Switching, Quality MOSFET Transistors" from **Infineon** (former **International Rectifier**) and **Vishay**.

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRF4905.pdf">IRF4905</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-55 V</b> (V<sub>DS</sub>)<br><b>-74000 mA</b> (I<sub>D</sub>)<br><b>200000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>3400 pF</b></td>
    <td>TO-220<br><img src="to-220_-_gds.svg"></td>
    <td>IRFZ44N</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRFR120N.pdf">IRFR120N</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>100 V</b> (V<sub>DS</sub>)<br><b>9400 mA</b> (I<sub>D</sub>)<br><b>48000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>2 V</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>IRFR9120</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRFR4105.pdf">IRFR4105</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>55 V</b> (V<sub>DS</sub>)<br><b>27000 mA</b> (I<sub>D</sub>)<br><b>68000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>IRFR5305</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRFR5305.pdf">IRFR5305</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-55 V</b> (V<sub>DS</sub>)<br><b>-31000 mA</b> (I<sub>D</sub>)<br><b>110000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>1200 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>IRFR4105</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRFR9120.pdf">IRFR9120</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-100 V</b> (V<sub>DS</sub>)<br><b>-5600 mA</b> (I<sub>D</sub>)<br><b>42000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>390 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>IRFR120N</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRFZ44N.pdf">IRFZ44N</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>55 V</b> (V<sub>DS</sub>)<br><b>49000 mA</b> (I<sub>D</sub>)<br><b>110000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>2 V</b><br>typ. C<sub>iss</sub> = <b>1350 pF</b></td>
    <td>TO-220<br><img src="to-220_-_gds.svg"></td>
    <td>IRF4905</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/IRLL014N.pdf">IRLL014N</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>55 V</b> (V<sub>DS</sub>)<br><b>2000 mA</b> (I<sub>D</sub>)<br><b>2100 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>230 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>ISP25DP06LM</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/ISP25DP06LM.pdf">ISP25DP06LM</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>)<br><b>-1900 mA</b> (I<sub>D</sub>)<br><b>5000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>420 pF</b></td>
    <td>SOT-252<br><img src="sot-223_-_gds.svg"></td>
    <td>IRLL014N</td>
  </tr>
</table>


<a name="7"></a>
## 7. J... Series

It provides a wide range of JFETs.

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J105-J106-J107.pdf">J105</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>–25 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-4.5 V</b><br>max. r<sub>DS(on)</sub> = <b>3 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dsg.svg"></td>
    <td>J174</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J105-J106-J107.pdf">J106</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>–25 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-2 V</b><br>max. r<sub>DS(on)</sub> = <b>6 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dsg.svg"></td>
    <td></td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J105-J106-J107.pdf">J107</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>–25 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-0.5 V</b><br>max. r<sub>DS(on)</sub> = <b>8 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dsg.svg"></td>
    <td>J270</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J111-J112.pdf">J111</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>-35 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-3 V</b><br>max. r<sub>DS(on)</sub> = <b>30 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dsg.svg"></td>
    <td>J175</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J111-J112.pdf">J112</a></b></td>
    <td>JFET, N-Channel</td>
    <td><b>–35 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-1 V</b><br>max. r<sub>DS(on)</sub> = <b>50 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dsg.svg"></td>
    <td>J176</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J174-J175-J176-J177.pdf">J174</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>5 V</b><br>max. r<sub>DS(on)</sub> = <b>85 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>J105</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J174-J175-J176-J177.pdf">J175</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>3 V</b><br>max. r<sub>DS(on)</sub> = <b>125 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>J111</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J174-J175-J176-J177.pdf">J176</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>1 V</b><br>max. r<sub>DS(on)</sub> = <b>250 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>J112</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J174-J175-J176-J177.pdf">J177</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>0.8 V</b><br>max. r<sub>DS(on)</sub> = <b>300 Ω</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td></td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/J270.pdf">J270</a></b></td>
    <td>JFET, P-Channel</td>
    <td><b>30 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>50 mA</b> (I<sub>GF</sub>)<br><b>350 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>0.5 V</b></td>
    <td>TO-92<br><img src="to-92_-_dgs.svg"></td>
    <td>J107</td>
  </tr>
</table>


<a name="8"></a>
## 8. LND... Series

This series was interesting only because it has at least one Depletion-Mode MOSFET in TO-92 package.

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/LND150.pdf">LND150</a></b></td>
    <td>D-MOSFET<br>N-Channel</td>
    <td><b>500 V</b> (V<sub>DG</sub>, V<sub>GS</sub>)<br><b>30 mA</b> (I<sub>GF</sub>)<br><b>740 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>(GS)OFF</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>7.5 pF</b></td>
    <td>TO-92<br><img src="to-92_-_sgd.svg"></td>
    <td>-</td>
  </tr>
</table>


<a name="9"></a>
## 9. FDT..., NDT... Series

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/NDT2955.pdf">NDT2955</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-60 V</b> (V<sub>DS</sub>)<br><b>-2500 mA</b> (I<sub>D</sub>)<br><b>3000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>601 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>NDT3055</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/NDT3055.pdf">NDT3055</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>60 V</b> (V<sub>DS</sub>)<br><b>4000 mA</b> (I<sub>D</sub>)<br><b>3000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>250 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>NDT2955</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/NDT451AN.pdf">NDT451AN</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>30 V</b> (V<sub>DS</sub>)<br><b>7200 mA</b> (I<sub>D</sub>)<br><b>3000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>720 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>NDT456P</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/NDT456P.pdf">NDT456P</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-30 V</b> (V<sub>DS</sub>)<br><b>-7500 mA</b> (I<sub>D</sub>)<br><b>3000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>1440 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>NDT451AN</td>
  </tr>
</table>


<a name="10"></a>
## 10. STS... Series

...

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/STS9012.pdf">STS9012</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-40 V</b> (V<sub>CBO</sub>)<br><b>600 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>STS9012</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/STS9013.pdf">STS9013</a></b></td>
    <td>BJT, NPN</td>
    <td><b>40 V</b> (V<sub>CBO</sub>)<br><b>500 mA</b> (I<sub>C</sub>)<br><b>625 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>140 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>2N2907</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/STS9014.pdf">STS9014</a></b></td>
    <td>BJT, NPN</td>
    <td><b>50 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>400 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>5 V</b><br>min. f<sub>T</sub> = <b>150 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>STS9015</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/STS9015.pdf">STS9015</a></b></td>
    <td>BJT, PNP</td>
    <td><b>-50 V</b> (V<sub>CBO</sub>)<br><b>100 mA</b> (I<sub>C</sub>)<br><b>450 mW</b> (P<sub>tot</sub>)</td>
    <td>min. V<sub>(BR)EBO</sub> = <b>-5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-92<br><img src="to-92_-_ebc.svg"></td>
    <td>STS9014</td>
  </tr>
</table>


<a name="11"></a>
## 11. TIP... Series

...

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/TIP120-TIP121-TIP122-TIP123-TIP124-TIP125-TIP126-TIP127.pdf">TIP120</a></b></td>
    <td>BJT, NPN<br>Darlington</td>
    <td><b>60 V</b> (V<sub>CBO</sub>)<br><b>5000 mA</b> (I<sub>C</sub>)<br><b>65000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)</sub> = <b>2.5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-220<br><img src="to-220_-_bce.svg"></td>
    <td>TIP125</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/TIP120-TIP121-TIP122-TIP123-TIP124-TIP125-TIP126-TIP127.pdf">TIP122</a></b></td>
    <td>BJT, NPN<br>Darlington</td>
    <td><b>100 V</b> (V<sub>CBO</sub>)<br><b>5000 mA</b> (I<sub>C</sub>)<br><b>65000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)</sub> = <b>2.5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-220<br><img src="to-220_-_bce.svg"></td>
    <td>TIP127</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/TIP120-TIP121-TIP122-TIP123-TIP124-TIP125-TIP126-TIP127.pdf">TIP125</a></b></td>
    <td>BJT, PNP<br>Darlington</td>
    <td><b>-60 V</b> (V<sub>CBO</sub>)<br><b>5000 mA</b> (I<sub>C</sub>)<br><b>65000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)</sub> = <b>-2.5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-220<br><img src="to-220_-_bce.svg"></td>
    <td>TIP120</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/TIP120-TIP121-TIP122-TIP123-TIP124-TIP125-TIP126-TIP127.pdf">TIP127</a></b></td>
    <td>BJT, PNP<br>Darlington</td>
    <td><b>-100 V</b> (V<sub>CBO</sub>)<br><b>5000 mA</b> (I<sub>C</sub>)<br><b>65000 mW</b> (P<sub>tot</sub>)</td>
    <td>max. V<sub>BE(on)</sub> = <b>-2.5 V</b><br>min. f<sub>T</sub> = <b>100 MHz</b></td>
    <td>TO-220<br><img src="to-220_-_bce.svg"></td>
    <td>TIP122</td>
  </tr>
</table>


<a name="12"></a>
## 12. U... Series

Here is a short list from the wide selection of **Unisonic Technologies**, Taiwan. Their website provides a quite useful [catalogue](http://www.unisonic.com.tw/english/product-new.asp) for browsing components.

<table class="tan">
  <tr>
    <th>Label</th>
    <th>Type and Polarity</th>
    <th>Maximum Ratings</th>
    <th>Electrical Characteristics</th>
    <th>Typical Packaging<br>and Pin Layout</th>
    <th>Complementary<br>type of</th>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UF601.pdf">UF601</a></b></td>
    <td>D-MOSFET,<br>N-Channel</td>
    <td><b>600 V</b> (V<sub>DS</sub>)<br><b>185 mA</b> (I<sub>D</sub>)<br><b>800 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>53 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>-</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UF9Z34.pdf">UF9Z34</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-55 V</b> (V<sub>DS</sub>)<br><b>-17000 mA</b> (I<sub>D</sub>)<br><b>38000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-2 V</b><br>typ. C<sub>iss</sub> = <b>620 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>UFZ24N</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UFZ24N.pdf">UFZ24N</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>55 V</b> (V<sub>DS</sub>)<br><b>17000 mA</b> (I<sub>D</sub>)<br><b>30000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>2 V</b><br>typ. C<sub>iss</sub> = <b>770 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>UF9Z34</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UT2316.pdf">UT2316</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>30 V</b> (V<sub>DS</sub>)<br><b>3600 mA</b> (I<sub>D</sub>)<br><b>500 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>145 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>UT3P03</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UT3P03.pdf">UT3P03</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-30 V</b> (V<sub>DS</sub>)<br><b>-3000 mA</b> (I<sub>D</sub>)<br><b>2000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>371 pF</b></td>
    <td>SOT-223<br><img src="sot-223_-_gds.svg"></td>
    <td>UT2316</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UTD413.pdf">UTD413</a></b></td>
    <td>E-MOSFET,<br>P-Channel</td>
    <td><b>-40 V</b> (V<sub>DS</sub>)<br><b>-12000 mA</b> (I<sub>D</sub>)<br><b>46000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>-1 V</b><br>typ. C<sub>iss</sub> = <b>780 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>UTD454</td>
  </tr>
  <tr>
    <td><b><a href="/downloads/datasheets/UTD454.pdf">UTD454</a></b></td>
    <td>E-MOSFET,<br>N-Channel</td>
    <td><b>40 V</b> (V<sub>DS</sub>)<br><b>12000 mA</b> (I<sub>D</sub>)<br><b>44000 mW</b> (P<sub>D</sub>)</td>
    <td>min. V<sub>GS(th)</sub> = <b>1 V</b><br>typ. C<sub>iss</sub> = <b>380 pF</b></td>
    <td>TO-252<br><img src="to-252_-_gds.svg"></td>
    <td>UTD413</td>
  </tr>
</table>
