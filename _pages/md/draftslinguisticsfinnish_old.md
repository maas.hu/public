# ![alt text](/_ui/img/flags/fin_m.svg) Finn nyelv / Suomen kieli

<div style="text-align: right;"><h7>Posted: 2019-10-25 17:41</h7></div>

###### .

>Az alábbi jegyzet még erősen hiányos, de ahogy haladok, úgy írogatom be a fontosabb tudnivalókat. 

- ![alt text](/_ui/img/flags/fin-hun_s.svg) [Finn-Magyar Szótár](dictionary)
- ![alt text](/_ui/img/flags/fin-hun_s.svg) [Finn igék, típusaik, ragozásuk](verbs)

## Tartalomjegyzék

1. [Bevezető - Johdanto](#1)
2. [Az ábécé - Aakkoset](#2)
3. [Alapmondatok, kifejezések - Peruslauseet, lausekkeet](#3)
4. [Főnév - Substantiivi](#4)
5. [Melléknév - Adjektiivi](#5)
6. [Számnév - Numeraali](#6)
7. [Névmás - Pronomini](#7)
8. [Ige - Verbi](#8)
9. [Határozószó - ](#9)
10. [Névelő - Artikkeli](#10)
11. [Elöljárószók - ](#11)
12. [Kötőszavak, módosítószók és egyéb határozószók - ](#12)
13. [Állítás, tagadás, kérdés - Lausuma, kieltäminen, kysymys](#13)
14. [Felszólító és feltételes mód - ](#14)
15. [Rövid dialógusok - ](#15)
16. [Mesék, versek - ](#16)
17. [Kiadványok, linkek](#17)


<a name="1"></a>
## 1. Bevezető - Johdanto

A finn nyelv (*suomi* vagy *suomen kieli*) a finnugor nyelvcsalád balti-finn ágának legjelentősebb nyelve. Anyanyelvként 4,8 millióan beszélik, ill. még további félmillió beszéli második nyelvként. A finn rendkívül közeli rokonságban van az észt nyelvvel: olyannyira, hogy a kettő beszélői kölcsönösen megértik egymást. 

A finnugor nyelvek az ősi uráli nyelvből fejlődtek ki, és egyes vélekedések szerint a napjainkban még élő nyelvek közül a finn áll a legközelebb az ősi urálihoz. A tudomány jelen állása szerint az uráli alapnyelvet nagyjából 6000 évvel ezelőtt beszélték, majd ahogy vándoroltak a népek, évszázadok alatt vált szét az alábbi ábrán is feltűntetett Ugor alapnyelvre és a Balti-finn nyelvek őseire, amelyeket 3000 évvel ezelőtt használhattak. 

![alt_text](/learning/language/mansi/uralic_lang.png "Az uráli nyelvek egymáshoz való viszonya és a ma élő finnugor nyelvek")

Bár egymás számára nem érthetőek, a közös eredet miatt a finn a magyar nyelvnek is rokona. Amellett, hogy számos nyelvtani szabály is hasonlít, körülbelül 800 olyan szó van a finnben és a magyarban, amelynek közös a gyökere. Ugyan sok szó esetén ezt a közös gyökeret csak a nyelvészek segítségével fedezhetjük fel, mégis az ilyen szavakat általában könnyebb megtanulnunk. 

A viszonylag sok közös eredetű és hasonló szó mellett fontos megjegyezni az egyéb hasonlóságokat a nyelvek mélyebb szintű logikájában: 
- a főneveknek nincsenek nemeik, 
- a hangsúly mindig a szavak első szótagjára esik,
- mindkettő a lehető legerősebben *agglutinatív*, azaz *ragasztó* nyelv (más néven *ragozó*, vagy *toldalékoló* nyelv),
- a szavak ragozott alakjaiban a magánhangzó-harmónia elengedhetetlen fontosságú,
- ugyanaz a logika még új szavak alkotásakor is (pl. *repülőgép*, *esernyő*, *örömlány*). 

Ezek a közös jellemzők legalább annyira fontosak a nyelvrokonság felismerésében és elismerésében, mint a közös ősi szavaink. Annak ellenére, hogy a finn legalább olyan bonyolult és nehezen elsajátítható nyelv, mint a magyar, fenntartások nélkül kijelenthető, hogy a fentieknek köszönhetően könnyebb finnül megtanulnia egy magyarnak, mint pl. egy angol anyanyelvűnek.


<a name="2"></a>
## 2. Az ábécé - Aakkoset

### 2.1 A finn ábécé betűi

<table class="tan">
  <tr>
    <td><b>A a</b></td><td><b>B b</b></td><td><b>C c</b></td><td><b>D d</b></td><td><b>E e</b></td><td><b>F f</b></td><td><b>G g</b></td><td><b>H h</b></td>
  </tr>
  <tr>
    <td><b>I i</b></td><td><b>J j</b></td><td><b>K k</b></td><td><b>L l</b></td><td><b>M m</b></td><td><b>N n</b></td><td><b>O o</b></td><td><b>P p</b></td>
  </tr>
  <tr>
    <td><b>Q q</b></td><td><b>R r</b></td><td><b>S s</b></td><td><b>T t</b></td><td><b>U u</b></td><td><b>V v</b></td><td><b>W w</b></td><td><b>X x</b></td>
  </tr>
  <tr>
    <td><b>Y y</b></td><td><b>Z z</b></td><td><b>Å å</b></td><td><b>Ä ä</b></td><td><b>Ö ö</b></td>
  </tr>
</table>

### 2.2 Kiejtés

**y** [ y ] - "ü"  
**s** [ s ] - "sz"  
**e** [ e ] - a magyar "e"-nél zártabb, a tájnyelvi "ë"-nek felel meg  
**ä** [ æ ] - az "e"-nél jóval nyitottabb, de még nem "a". Mint az angol *cat*, *fat*, *hat* szavakban.  
**å** [ o ] - a magyar "o"-nak felel meg, csak svéd eredetű szavakban, nevekben fordul elő. 

<a name="3"></a>
## 3. Alapmondatok, kifejezések - Peruslauseet, lausekkeet

**kyllä** (nagyon formális) / **joo** - igen  
**ei** - nem  

**oikein** - helyes  
**väärin** - rossz  

**Olet oikeassa.** - Igazad van.  
**Olet väärässä.** - Tévedsz.  

**Kiitos!** - Köszönöm!  
**Kiitos paljon!** - Nagyon köszönöm! (Köszönöm szépen!)  
**Ole hyvä!** - Szívesen!  

**Apua!** - Segítség!  
**Varo!** - Óvatosan!  

### 3.1 Köszönések, köszöntések, bemutatkozások

<img src="/_ui/img/imgfx/conversation.png" title="" align="right" class="right">

**Moi!** / **Hei!** - Szia!  
**Terve!** - Hello!  
**Tervetuloa!** - Üdvözlet!  
**Hei hei!** / **Moi moi!** - Szia! *(elköszönésként)*  

**Hyvää huomenta!** - Jó reggelt!  
**Hyvää päivää!** - Jó napot!  
**Hyvää iltaa!** - Jó estét!  
**Hyvää yötä!** - Jó éjszakát!  

**Nähdään!** - Viszlát!  
**Nähdään pian! Hei!** - Viszlát hamarosan! Szia!  
**Nähdään myöhemmin!** - Viszlát később!  

**Hyvää päivänjatkoa!** - Kellemes napot!  
**Tsemppiä!** - Sok szerencsét!  
**Hyvää joulua!** - Boldog karácsonyt!  
**Hyvää uutta vuotta!** - Boldog új évet!  
**Kiitos samoin!** - Viszont kívánom!  

**Ystävällisin terveisin** - Szívélyes üdvözlettel *(levél alján)*  

**Mitä kuuluu?** - Hogy vagy?  
**Ihan hyvää, kai.** - Egész jól, azt hiszem.  
**Oletko kunnossa?** - Jól vagy?  
**No, ihan hyvää, kiitos.** - Nos, egész jól, köszi.  
**No, ei mitään.** - Nos, semmi különös.  
**Kaikki on ihan hyvin.** - Minden a legnagyobb rendben. ("Minden egészen rendben.")  
**No, ei se mitään.** - Nos, megesik.  

**Anteeksi, kuka sina olet?** - Elnézést, ki vagy te?  
**Anteeksi, oletko sinä Pöllönen?** - Elnézést, te vagy Pöllönen?  

**Minä olen Elsa.** - Engem Elsa-nak hívnak. *(nő)*  
**Minä olen Otso.** - Engem Otso-nak hívnak. *(férfi)*  
**Sinä olet Joni.** - Téged Joni-nak hívnak.  

**Olen pahoillani.** - Sajnálom.  
**Olemme pahoillamme.** - Sajnáljuk.  

### 3.2 Nyelvismeret közlése

**En ymmärrä.** - Nem értem.  
**En puhu suomea.** - Nem beszélek finnül.  
**(Minä) puhun huonosti suomea.** - Rosszul beszélek finnül.  
**(Minä) puhun englantia.** - Beszélek angolul.  
**Puhun vähän viroa.** - Beszélek egy kicsit észtül.  
**Puhun myös englantia.** - Beszélek angolul is.  
**Anteeksi, mutta puhutko suomea?** - Elnézést, de beszélsz finnül?  
**Anteeksi, puhutteko te englantia.** - Elnézést, ti beszéltek angolul?  
**Anteeksi, mutta puhun vain englantia.** - Elnézést, de csak angolul beszélek.  
**Miksi puhutte suomea?** - Miért beszélsz finnül?  
**Osaan puhua ja lukea japania.** - Tudok beszélni és írni japánul.  
**Sinä puhut suomea todella hyvin!** - Te igazán jól beszélsz finnül!  

**– Puhutko venäjää?** - Beszélsz oroszul?  
**– Puhun, mutta vain vähän.** - Beszélek, de csak egy kicsit.  

### 3.3 Útbaigazítás

**Voinko auttaa?** - Segíthetek? (Tudok segíteni?)  
**Kuinka voin auttaa?** - Miben segíthetek? (Hogyan segíthetek?)  
**Voisitteko auttaa minua?** - Todna segíteni nekem?  
**Hei! Osaatko sanoa missä oopperatalo on?** - Szia! Meg tudnád mondani, hol van az operaház?  
**Terve! Osaatteko te sanoa missä lähin kirkko on?** - Hello! Meg tudnátok mondani, hol van a legközelebbi templom?  

...

### 3.4 Vásárlás, vendéglátás

**Hyvää päivää. Mitä saisi olla?** - Jó napot. Mit hozhatok?  
**Haluatko jotain syötävää?** - Akarsz valamit enni?  
**Kolme kuppia kahvia, kaksi palaa piirakkaa ja yksi pala kakkua, kiitos.** - Két csésze kávét, két szelet pitét és egy szelet tortát kérek.  
**Kaksi pulloa olutta, kiitos.** - Két üveg sört kérek.  
**On aika juoda olutta.** - Ideje sört inni.  
**Saisimmeko kaksi annosta kalaa, kiitos?** - Kaphatnánk két adag halat, kérem?  

**– Anteeksi, meillä ei ole ruokalistaa.** - Elnézést, nekünk nincs menünk.  
**– Olen todella pahoillani. Pieni hetki.** - Végtelenül sajnálom. Egy pillanat.  

**On aika syödä päivällistä.** - Ideje vacsorázni. *("Idő van enni vacsorát.")*  
**Me haluaisimme syödä päivällistä ravintolassa mutta isi haluaa olla kotona.** - Szerenténk vacsorázni az étteremben de apa otthon akar lenni.  
**Täällä on kallista.** - Itt drágaság van.  
**Haluan paljon kahvia ja vähän sokeria.** - Sok kávét és egy kevés cukrot akarok.  
**Onko kilo karkkia liika vai liian vähän?** - Egy kiló cukorka az túl sok vagy túl kevés?  

### 3.5 Egyéb mondatok

**Tyyne, sinä olet mukava.** - Tyyne, te kedves vagy.  
**Miikka, onko sinulla suomalainen nimi?** - Miikka, neked finn neved van?  
**Veinö, onko sinulla suomalainen sauna?** - Veinö, neked finn szaunád van?  
**Minulla on ruotsalainen nimi.** - Nekem svéd nevem van.  
**Minulla on tanskalainen ystävä.** - Nekem dán barátom van.  
**Hänellä on ruotsalainen auto.** - Neki svéd autója van.  
**Missä tanskalainen on?** - Hol van a dán?  
**Suomalainen on velho, ja ruotsalainen on viikinki.** - A finn egy varázsló, és a svéd egy viking.  
**Hänellä on suomalainen kännykkä.** - Neki (egy) finn telefonkészüléke van.  
**Unkari on niin kaunis kieli.** - A magyar nagyon szép nyelv.  
**Tuo on hyvä kysymys!** - Ez egy jó kérdés!  
**Ole kiltti ja ole varovainen, Pöllölä.** - Kérlek, légy óvatos, Pöllölä.  

...

**Jään alla talvella elävät kalat uiskentelevat.** - <r>Jég alatt télen eleven halak úszkálnak.</r>

### 3.6 Alap bemutatkozás

...

<a name="4"></a>
## 4. Főnév - Substantiivi

...

### kuukaudet - a hónapok

<table class="tan">
  <tr>
    <th>tammikuu</th>
    <th>helmikuu</th>
    <th>maaliskuu</th>
    <th>huhtikuu</th>
    <th>toukokuu</th>
    <th>kesäkuu</th>
  </tr>
  <tr>
    <td>January</td>
    <td>February</td>
    <td>March</td>
    <td>April</td>
    <td>May</td>
    <td>June</td>
  </tr>
</table>
<table class="tan">
  <tr>
    <th>heinäkuu</th>
    <th>elokuu</th>
    <th>syyskuu</th>
    <th>lokakuu</th>
    <th>marraskuu</th>
    <th>joulukuu</th>
  </tr>
  <tr>
    <td>July</td>
    <td>August</td>
    <td>September</td>
    <td>October</td>
    <td>November</td>
    <td>December</td>
  </tr>
</table>

### 4.1 Többes szám

A többesszám egyszerűen a "t" raggal képződik: 

**kissa → kissat** = macska → macskák  
**orava → oravat** = mókus → mókusok  

Mondatba helyezve a többesszámú alanyokhoz tartozó igék szintén többesszámba kerülnek: 

**Nämä karhut tanssivat.** - Ezek a medvék táncolnak.  

### 4.2 A -ban/-ben és -ba/-be határozóragok

**Haluan asua Suomessa.** - Finnországban akarok élni.  
**Haluan asua tässä maassa.** - Ebben az országban akarok élni.  

**Tori on Berliinissä.** - A tér Berlinben van.  
**Isä asuu tässä maassa.** - Apa ebben az országban él.  

...

Amiket még nem is értek: 

**tie** - út  
**tietä** - az út (???)  


<a name="5"></a>
## 5. Melléknév - Adjektiivi

Melléknevek a Duolingóból, amiket állandóan összekeverek egymással: 

**hauska** - vicces  
**kaunis** - gyönyörű  
**komea** - jóképű  
**mukava** - kedves  
**rehellinen** - őszinte  

**Englanti niin kaunis mutta vaikea kieli.** - Angol - oly' szép, de bonyolult nyelv.  
**Tämä tomaatti on punainen ja tuo vihreä.** - Ez a paradicsom piros, és az pedig zöld.  
**Pieni sininen auto on hyvä lelu.** - A kicsi, kék autó egy jó játék.  
**Minulla on jano.** - Szomjas vagyok.  
**Minulla on nälkä.** - Éhes vagyok.  
**Onko teillä nälkä?** - Éhes vagy?  
**Meillä on vielä jano** - Mi még mindig szomjasak vagyunk.  

...

Ragozáskor nem csak a főnevet ragozzuk, hanem a melléknevet is. 

<table class="tan">
  <tr>
    <td><b>surullinen</b> - szomorú</td>
    <th>egyes sz.</th>
    <th>többes sz.</th>
  </tr>
  <tr>
    <th>alanyeset (-)</th>
    <td><b>surullinen</b></td>
    <td><b>surulliset</b></td>
  </tr>
  <tr>
    <th>tárgyeset (-t)</th>
    <td><b>surullista</b></td>
    <td><b>surullisia</b></td>
  </tr>
  <tr>
    <th>birtokos eset (-é)</th>
    <td><b>surullisen</b></td>
    <td><b>surullisten</b> / <b>surullisien</b></td>
  </tr>
  <tr>
    <th>illatív eset (-ba / -be)</th>
    <td><b>surulliseen</b></td>
    <td><b>surullisiin</b></td>
  </tr>
  <tr>
    <th>inesszív eset (-ban / -ben)</th>
    <td><b>???</b></td>
    <td><b>???</b></td>
  </tr>
  <tr>
    <th>választható eset (ki-)</th>
    <td><b>???</b></td>
    <td><b>???</b></td>
  </tr>
  <tr>
    <th>adresszív eset (be-)</th>
    <td><b>???</b></td>
    <td><b>???</b></td>
  </tr>
  <tr>
    <th>allatív eset (rá-)</th>
    <td><b>???</b></td>
    <td><b>???</b></td>
  </tr>
  <tr>
    <th>ablatív eset (-ról / -ről)</th>
    <td><b>???</b></td>
    <td><b>???</b></td>
  </tr>
</table>

**Mummo halaa surullista miestä.** - Nagymama öleli a szomorú(t) férfit.  
**Halaan ujoa naista, vaikka myös minä olen ujo.** - Megölelem a félénk nőt, habár én is félénk vagyok.  

<a name="6"></a>
## 6. Számnév - Numeraali

**yksi, kaksi, kolme, neljä, viisi, kuusi, seitsemän, kahdeksan, yhdeksän, kymmenen, yksitoista, kaksitoista, kaksikymmentä, kolmekymmentä, neljäkymmentä, viisikymmentä, sata, tuhat** - 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 20, 30, 40, 50, 100, 1000  

A számneveknek tulajdonképpen három formája van: a fenti hivatalos, a beszélt nyelvben "ráérősebb", kicsit rövidebb változat és a sokkal rövidebb, amit a számoláskor használnak.  
Mindez így néz ki 21-től 29-ig:

**kaksikymmentäyksi → kakskytyks → kaayy** - huszonegy  
**kaksikymmentäkaksi → kakskytkaks → kaakaa** - huszonkettő  
**kaksikymmentäkolme → kakskytkolme → kaakoo** - huszonhárom  
**kaksikymmentäneljä → kakskytneljä → kaanee** - huszonnégy  
**kaksikymmentäviisi → kakskytviis → kaavii** - huszonöt  
**kaksikymmentäkuusi → kakskytkuus → kaakuu** - huszonhat  
**kaksikymmentäseitsemän → kakskytseitsemän → kaasee** - huszonhét  
**kaksikymmentäkahdeksan → kakskytkaheksan → kaakasi** - huszonnyolc  
**kaksikymmentäyhdeksän → kakskytyheksän → kaaysi** - huszonkilenc  

...

**liikaa** - túl sok  

...

**yksi sekunti, kaksi sekuntia** - egy másodperc, két másodperc  

**Pieni hetki!** - Csak egy pillanat!  

**Paljonko kello on?** - Mennyi az idő?  

**Kello on kolme.** - Három óra van.  
**Kello on puoli yksitoista.** - Fél tizenegy. (Fél órája múlt 10.)

**– Anteeksi, mitä kello on?** - Elnézést, mennyi az idő?  
**– Se on yksitoista.** - Tizenegy óra van.  


<a name="7"></a>
## 7. Névmás - Pronomini

...

### 7.1 Személyes névmások - Persoonapronominit

(pl. én, téged)

**minä** - én  
**sinä** - te  
**hän** - ő  
**me** - mi  
**te** - ti  
**he** - ők  

Fontos megjegyezni, hogy a beszélt finn nyelvben a fentiek általában így fordulnak elő: 

**mä** - én  
**sä** - te  
**se** - ő (a *se* használatával vigyázzunk, mert illetlen lehet egyes szituációkban)  
**me** - mi (tehát ugyanaz, mint az írt/irodalmi finnben)  
**te** - ti (szintén egyezik)  
**ne** - ők  

**(minä) puhun** - (én) beszélek  
**(sinä) puhut** - (te) beszélsz  
**hän puhuu** - ő beszél  
**(me) puhumme** - (mi) beszélünk  
**(te) puhutte** - (ti) beszéltek  
**he puhuvat** - ők beszélnek  

- Partitiiviobjekti:  
**minua** - engem  
**sinua** - téged  
**häntä** - őt  
**meitä** - minket  
**teitä** - titeket  
**heitä** - őket  

- Totaaliobjekti:  
**minut** - engem  
**sinut** - téged  
**hänet** - őt  
**meidät** - minket  
**teidät** - titeket  
**heidät** - őket  

### 7.2 Birtokos névmások

(pl. enyém, tieid)

**minulla** - nekem  
**sinulla** - neked  
**hänellä** - neki  
**meillä** - nekünk  
**teillä** - nektek  
**heillä** - nekik  

**Meillä on vanha talo.** - Van egy régi házunk.  
**Meillä on ystävä, joka asuu Suomessa.** - Van egy barátunk, aki Finnországban él.  

**oma** - saját  

**Teillä on oma huone.** - Neked saját szobád van.  

...

**sama** - ugyanaz, azonos  

...

### 7.3 Visszaható névmások

(pl. magam, magatok)

...

### 7.4 Kölcsönös névmások

(pl. egymást, egymással)

...

### 7.5 Mutató névmások

(pl. ez, amolyan)

**tämä** - ez  
**tuo** - az  
**nämä** - ezek a...  
**nuo** - azok a...  

**tässä** - itt  
**siellä** - ott  

**Tämä veitsi on tässä ja tuo veitsi on tuossa.** - Ez a kés itt van, és az a kés ott van.   

### 7.6 Kérdő névmások

(pl. ki, mekkora)

**kuka** - ki  
**ketkä** - kik  
**ketä** - <r>kit</r>  
**mitä** - <r>mit</r>  
**kuinka moni** - hány darab  
**kuinka paljon** - hány ()  
**millainen** - milyen, miféle  

**Ketä sinä rakastat?** - Kit szeretsz?  

**Kuinka monta jäsentä kotitaloudessasi on?** - Hányan laknak egy háztartásban?  

**Kuinka moni suomalainen osaa ruotsia?** - Hány finn tud svédül?  

...

### 7.7 Vonatkozó névmások

(pl. ami, amikor)

**joka** - aki, ami, amelyik  
**jolla** - ami, amely  

**jota** - akit  

...

**Minulla on ystävä jolla on kolme kissaa.** - Van egy barátom, akinek van három macskája.  
**Tyttö, jota ajattelen on ujo.** - A lány, akire gondolok, félénk.  
**Poika, joka juoksee tuolla, rakastaa tyttöä jota sinä ihailet.** - A fiú, aki ott fut, szereti a lányt, akit te csodálsz.  
**Lasi, jossa on olutta, on rikki.** - A pohár, amiben sör van, eltört.  

### 7.8 Határozatlan névmások

(valami, olykor)

...

### 7.9 Általános névmások

(mindenki, bárhol)

...

### 7.10 Tagadó névmások

(sehogyan, soha)

**koskaan** - soha  

...

<a name="8"></a>
## 8. Ige - Verbi

<img src="/_ui/img/imgfx/teaching.png" title="" align="right" class="right">

A finnben egyféle jelen idő, háromféle múlt idő van és nincs jövő idő. A jövőre vonatkozó cselekvést a szövegkörnyezetből állapíthatjuk meg. 

### 8.1 Jelen idejű igék kijelentő módú ragozása

A finn **hat** igegípust (**verbityyppi**) különböztet meg, amelyeket azok főnévi igeneves formájában ismerhetünk fel. 
Az **olla** (lenni) ige viszont rögtön egy kivétel, amelynek jelen idejű, kijelentő módú ragozása az alábbi:    
**minä olen** - én vagyok  
**sinä olet** - te vagy  
**hän on** - ő van  
**me olemme** - mi vagyunk  
**te olette** - ti vagytok  
**he ovat** - ők vannak

A legtöbb ige viszont könnyen beazonosítható és besorolható a hat típusba. 

A finnben ugyanúgy nem kell a személyes névmást is kiemelni, ahogyan a magyarban: 

**Maalaan taloa.** - Festem a házat.  

Egyébként nagyon hasznos oldal még a témában a [fi.bab.la](https://fi.bab.la/taivutus/suomi/nauraa). 

#### 8.1.1 Tyyppi 1: `a/ä`

A leggyakoribb csoportot alkotják a ***kettős magánhangzóval végződő*** *igék*, pl. **asua** (élni), **istua** (ülni), **puhua** (beszélni), **katsoa** (nézni), **lukea** (olvasni):  
    **minä asun** - én élek  
    **sinä asut** - te élsz  
    **hän asuu** - ő él  
    **me asumme** - mi élünk  
    **te asutte** - ti éltek  
    **he asuvat** - ők élnek  

#### 8.1.2 Tyyppi 2: `da/dä`

A ***"-da"*** vagy ***"-dä" szótaggal végződő*** *igék* már jóval kevesebben vannak, de többnyire kifejezetten fontosak, pl. **syödä** (enni), **juoda** (inni), **tehdä** (tenni), **myydä** (eladni):  
    **minä syön** - én eszek  
    **sinä syöt** - te eszel  
    **hän syö** - ő eszik  
    **me syömme** - mi eszünk  
    **te syötte** - ti esztek  
    **he syövät** - ők esznek  

#### 8.1.3 Tyyppi 3: `la/lä`; `s + ta/tä`; `na/nä`

A ***kettős mássalhangzóval, majd "a" vagy "ä" magánhangzóval végződő*** *igék*, pl. **mennä** (menni), **tulla** (jönni), **purra** (harapni), **pestä** (mosni).  
Ezeknél a leggyakoribb végződés az **"-lla"** és **"-llä"**, pl. **opiskella** (tanulni), **kävellä** (sétálni).  
A *menni* ragozása:  
    **minä menen** - én megyek  
    **sinä menet** - te mész  
    **hän menee** - ő megy  
    **me menemme** - mi megyünk  
    **te menette** - ti mentek  
    **he menevät** - ők mennek  

#### 8.1.4 Tyyppi 4: `vokaali + ta/tä`

A ***"-ta" vagy "-tä" szótaggal végződő, de a szótag előtt magánhangzóval végződő*** *igék*, pl. **pelata** (játszani), **osata** (tudni), **haluta** (akarni), **avata** (nyitni), **herätä** (felkelni).  
Ezeknek az igéknek a ragozásánál az az utolsó **"-t-"** kiesik:  
    **minä pelaan** - én játszom  
    **sinä pelaat** - te játszol  
    **hän pelaa** - ő játszik  
    **me pelaamme** - mi játszunk  
    **te pelaatte** - ti játszotok  
    **he pelaavat** - ők játszanak  

#### 8.1.5 Tyyppi 5: `i + ta/tä`

A ***"-ta" vagy "-tä" szótaggal végződő, de a szótag előtt "-i-"-t tartalmazó*** *igék*, pl. **tarvita** (igényelni (szükségnek lenni valamire)), **valita** (választani), **häiritä** (zavarni).  
Ragozásukkor az utolsó **"-t-"** szintén kiesik, ám a helyére **"-tse-"** kerül:  
    **minä tarvitsen** - én igénylem  
    **sinä tarvitset** - te igényled  
    **hän tarvitsee** - ő igényli  
    **me tarvitsemme** - mi igényeljük  
    **te tarvitsette** - ti igénylitek  
    **he tarvitsevat** - ők igénylik  

#### 8.1.6 Tyyppi 6: `e + ta/tä`

A ***"-ta" vagy "-tä" szótaggal végződő, de a szótag előtt "-e-"-t tartalmazó*** *igék*, pl. **vanheta** (öregedni).  
Ezeknél az igéknél az utolsó szótag leesik és a helyére **"-ne"** kerül:  
    **minä vanhenen** - én öregszem  
    **sinä vanhenet** - te öregszel  
    **hän vanhenee** - ő öregszik  
    **me vanhenemme** - mi öregszünk  
    **te vanhenette** - ti öregszetek  
    **he vanhenevat** - ők öregszenek  

### 8.2 Igéből képzett főnév (deverbális főnév)

Ez egy meglehetősen fontos alakja az igének, pl. naplóíráshoz (pl. *aludni* --> *alvás*). A finn nyelvtan szerint ez a "**neljäs infinitiivi**", vagyis az ige negyedik igenévi alakja. Angol változata a **gerund** (finnül: **gerundi**). 

Pl.:  
**Nukkuminen on tärkeää keholle.** - Az alvás fontos a szervezetnek.

A finnben néhány igének van egy másodlagos formája is, ami pontosan ugyanazt a szerepet tölti be és ugyanazt is jelenti, pl. **juokseminen** = **juoksu**; **uiminen** = **uinti**).  
Nem minden igének van második verziója, de minden egyes igét el lehet látni a **-minen** toldalékkal. 

Bővebb infó és beszélgetés erről a témáról: 
- [uusikielemme.fi](https://uusikielemme.fi/finnish-grammar/verbs/infinitives/the-fourth-infinitive-minen-neljas-infinitiivi)
- [finlandforum.org](https://www.finlandforum.org/viewtopic.php?t=50292)
- [thisisfinnish.tumblr.com](https://thisisfinnish.tumblr.com/post/138335650065/how-do-you-turn-a-verb-into-a-gerund-i-like)
  
### 8.3 Múlt idő

...

**oli** - volt  
**ohi** - vége  

**Toivon, että sinulla oli hauskaa.** - Remélem, hogy volt örömöd. (Remélem, hogy jól érezted magad.)  
**Oli hauska tutustua.** - Öröm volt a találkozás.  

...

**loppu** - elfogyott  

...


<a name="9"></a>
## 9. Határozószó - Adverbi

...

**Se on auki.** - Ez nyitva van.  
**Leo, museo on auki!** - Leo, a múzeum nyitva van!  
**Tuo teatteri on usein kiinni.** - Az a színház gyakran van zárva.  
**Voi ei, linna on kiinni.** - Jajj ne, a kastély zárva van.  
**Te olette liian kaukana.** - Túlságosan messze vagytok.  

...

<a name="10"></a>
## 10. Névelő - Artikkeli

A finn nyelv az uráli (a magyar kivételével) és a szláv nyelvekhez hasonlóan nem tartalmaz sem határozott, sem határozatlan névelőt. A finn irodalmi nyelv kialakításának korai korszakában még tettek kísérletet a nyelvészek egy névelőcsoport létrehozására az indoeurópai nyelvek modelljét alapul véve, amelyet az Új Testamentum *(Se Wsi Testamenti)* első finn fordításában érhetünk tetten, amelyet 1548-ban nyomtattak. 

Az "egy", "bizonyos" és "kevés" szavaknak természetesen vannak finn megfelelőik, de ezek már számnevek: 

**yksi** - egy  
**joitakin** - néhány (bizonyos, egyes)  
**muutamat** - néhány  

**kirja** - a könyv  
**kirja** - egy könyv  
**kirjat** - a könyvek  
**yksi kirja** - egy darab könyv  
**joitakin kirjoja** - egyes könyvek  
**muutamia kirjoja** - néhány könyv  

<a name="11"></a>
## 11. Elöljárószók - 

...

<a name="12"></a>
## 12. Kötőszavak, módosítószók és egyéb határozószók - 

**ja** - és  
**vai**, **tai** - vagy  
**mutta** - de  

**jo** - már  
**vielä** - még  
**että** - hogy  
**koska** - mert  
**vain** - csak  
**ehkä** - talán  
**kuin** - mint  
**yleensä** - általában  
**sekä** - is, egyaránt  

**Meillä on jo nälkä.** - Mi már éhesek vagyunk.  
**Tajuan, että se on halpaa** - Észreveszem, hogy ez olcsó.  
**Isä grillaa, koska meillä on nälkä.** - Apa grillezik, mert éhesek vagyunk.  

**Voi ei, oopperatalo on tuolla ja isä laulaa taas.** - Jajj ne, az operaház arra van és apa énekel megint.  
**Oho, me olemme taas täällä.** - Ó, mi már megint itt vagyunk.  

**Ehkä hän puhuu myös suomea.** - Ő talán beszél finnül is.  

**Tuo kukka on punainen kuin veri.** - Az a virág piros, mint a vér.  

**Pöllönen puhuu sekä tanskaa että norjaa.** - Pöllönen beszél dául és norvégül egyaránt.  

<a name="13"></a>
## 13. Állítás, tagadás, kérdés - Lausuma, kieltäminen, kysymys

...

### 13.1 Állítás

#### 13.1.1 Egyszerű állítások

A szórend nem túl kötött, tehát &ndash; akárcsak a magyarban &ndash; a helyhatározó is lehet mondat elején:  
**Metsässä jouksee hirvi.** - Az erdőben jávorszarvas fut.  
**Kylmässä metsässä juoksee harmaa susi.** - A hideg erdőben farkas fut.  
**Minä kuulen että karhut ovat lähellä.** - Hallom, hogy medvék vannak a közelben.  

**Rakastan sinua.** - Szeretlek.  
**Syön banaania.** - Banánt eszek.  

**Siivoamme isoa asuntoa.** - Takarítjuk a nagy lakást.  
**Siivoamme taloa, koska se on sotkuinen.** - Takarítjuk a házat, mert az piszkos.  
**Mummo korjaa vanhaa tuolia.** - A nagymama javítja a régi széket.  

#### 13.1.2 Tudás kifejezése

**Osaan sekä tanssia että laulaa.** - Tudok egyaránt táncolni és énekelni. 

#### 13.1.3 Akarat kifejezése

**Minä haluan mämmiä.** - Mämmit akarok.  
**Haluan jotain syötävää. Mutta mitä?** - Valamit akarok enni. De mit?  
**Haluan asua tässä maassa.** - Ebben az országban akarok élni.  
**Kulta, haluan, että olet onnellinen.** - Drágám, azt akarom, hogy boldog légy.  

### 13.2 Tagadás

#### 13.2.1 Egyszerű tagadás

- Egyes számban: az **en** (én nem), **et** (te nem), **ei** (ő nem) szavakkal:  

**Tuo ei ole vadelma.** - Az nem málna.  
**Japani ei ole pieni maa.** - Japán nem kicsi ország.  
**Australialainen mies ei ole kotona.** - Az ausztrál férfi nincs otthon.  
**Anteeksi, minä en puhu hindiä.** - Elnézést, nem beszélek hindiül.  
**Sinä et puhu englantia.** - Te nem beszélsz angolul.  
**Ruotsi on Skandinaviassa mutta Suomi ei.** - Svédország Skandináviában van, de Finnország nem.  

- Többes számban: az **emme** (mi nem), **ette** (ti nem), **eivät** (ők nem) szavakkal:  

**Me emme puhu saksaa.** - Mi nem beszélünk németül.  
**Te ette puhu koreaa.** - Ti nem beszéltek koreaiul.  
**Nämä opettajat eivät puhu suomea.** - Ezek a tanárok nem beszélnek finnül.  

A tagadó szó kerülhet a mondat végére is:  
**Ruotsi on Skandinaviassa, mutta Suomi ei.** - Svédország Skandináviában van, de Finnország nem.  

#### 13.2.2 Képesség tagadása

 pl. **emme osaa**

**Anteeksi, emme osaa puhua englantia.** - Elnézést, nem beszélhetünk angolul.  
**Emme osaa unkaria vaikka asumme Unkarissa.** - Nem beszélünk magyarul, habár Magyarországon élünk.  

#### 13.2.3 Nem, hanem

**En ole Kanadassa vaan Suomessa.** - Nem Kanadában, hanem Finnországban vagyok.  


#### 13.2.4 Sem... sem...

**En ole kalassa enkä marjassa.** - Se nem halászom, se nem szüretelek bogyókat.  
**Sinä et ole marjassa etkä kalassa.** - Se bogyókat nem szüretelsz, se nem halászol.  
**He eivät ole kotona eivätkä metsässä.** - Ők nincsenek sem otthon, sem az erdőben.  

### 13.3 Kérdés

...

#### 13.1 Kérdőszó nélküli kérdések

- eldöntendő kérdések:  

**Onko tuo kysymys vai vastaus?** - Az egy kérdés vagy egy válasz?  
**Onko se omena?** - Ez egy alma?  
**Roosa, onko tuo oopperatalo?** - Róza, az ott az operaház?  
**Opettaja, onko tämä lause oikein?** - Tanár(úr|nő), ez a mondat helyes?  
**Ovatko Matti ja Liisa pari?** - Matti és Liisa egy pár?  
**Korjaatteko te lamppua?** - Javítod a lámpát?  
**Haluatko jotain syötävää?** - Kérsz valamit enni?  
**Onko teillä pirtelöä tai jäätelöä?** - Neked tejturmixod vagy jégkrémed van?  

- képesség felől érdeklődő kérdések:  

**Osaatko sinä piirtää tai maalata?** - Tudsz rajzolni vagy festeni?  
**Osaatteko te laulaa?** - Ti tudtok énekelni?  

- tagadással kérdezés:  

**Eikö Otso asu einää Ruotsissa?** - Otso már nem lakik Svédországban?  
**Etkö osaa ajaa?** - Nem tudsz vezetni?  

#### 13.2 Kérdőszavas kérdések

- mi:  

**Mikä on taskussa?** - Mi van a zsebedben?

- mit:  

**Mitä te syötte?** - Mit esztek ti?  
**Mitä koira sanoo?** - Mit mond a kutya?  

- ki:  

**Kuka haluaa lisää kakkua?** - Ki akar még tortát?  

- kik:  

**Ketkä etsivät lintua?** - Kik keresik a madarat?  
**Ketkä ovat oikeassa ja ketkä väärässä?** - Kiknek van igazuk és kik tévednek?  

- kit:  

**Ketä näet?** - Kit látsz?  

- kinek:  

**Kenellä on lasi?** - Kinek van pohara?  

- miért:  

**Miksi tämä vastaus on väärin?** - Miért rossz ez a válasz?  
**Miksi tämä ruoka on oranssia?** - Miért narancssárga ez az étel?  
**Miksi sinä maalaat autoa?** - Miért fested az autót?  

- miért nem:  

**Miksi sinä et ole kotona?** - Miért nem vagy otthon?  
**Miksi te ette ole kotona?** - Miért nem vagytok otthon?  

- hogyan ("hogyan gyakran" - milyen gyakran):  

**Kuinka usein haluat tanssia?** - Milyen gyakran akarsz táncolni?  

- hol, merre:  

**Missä sinä asut?** - Hol laksz?  
**Missä kala on?** - Merre van a hal?  
**Missä kamelit elävät?** - Hol élnek a tevék?  
**Missä puisto on?** - Merre van a park?  
**Missä ketsuppi ja sinappi ovat?** - Merre van(nak) a ketchup és a mustár?  
**Anteeksi, mutta missä tori on?** - Elnézést, de merre van a piac?  
**Iltaa. Osaatko sanoa, missä vanha linna on?** - Estét! Meg tudja mondani, merre van a kastély?  

- milyen:  

**Mikä silta tuo on?** - Milyen híd az?  

- miféle:  

**Millainen kieli unkari on?** - Miféle nyelv a magyar?  

- mennyi:  

**Paljonko nämä omenat maksavat?** - Mennyibe kerülnek ezek az almák?  
**Paljonko tämä omena maksaa?** - Mennyibe kerül az az alma?  
**Paljonko kello on?** - Mennyi az idő?  

<a name="14"></a>
## 14. Felszólító és feltételes mód - 

**Ole varovainen; tee on kuumaa.** - Légy óvatos; a tea forró.  

...

<a name="15"></a>
## 15. Rövid dialógusok - 

...

**- Anteeksi, että olen myöhässä.** - Elnézést, el vagyok késve.  
**- No, ei se mitään.** - Nos, semmi gond.  

**- Yksi kahvi, kiitos.** - Egy kávét kérek.  
**- Ole hyvä.** - Tessék. *(leteszi az asztalra)*  
**- Kiitos.** - Köszönöm!

**– Onko kahvila auki?** - Zárva van a kávézó?  
**– On.** - Úgy van.

**- Korjaavatko he lamppua?** - Ők javítják a lámpát?  
**- Korjaavat.** - Igen, ők.

**- Siivoaako hän asuntoa?** - Ő takarítja a lakást?  
**- Siivoaa.** - Igen, ő.

**- Maalaatko taulua?** - Te festesz (egy festményt)?  
**- Maalaan.** - Igen, én.  

<a name="16"></a>
## 16. Mesék, versek - 

...

<a name="17"></a>
## 17. Kiadványok, linkek - 

...

- Egy nagyon jó, hatalmas tudásanyaggal rendelkező finn nyelvtanulással foglalkozó weboldal: [Uusi kielemme](https://uusikielemme.fi/)
- Egy anyanyelvi finn nyelvtanár YouTube csatornája rengeteg hasznos anyaggal kezdőknek, középhaladóknak: [Sinä osaat! Suomen kieltä kaikille](https://www.youtube.com/channel/UC9tDAXEWnBL5neFwZwaFtpA)
- Egy teljes tanfolyami anyag angolul: [A taste of Finnish](http://tasteoffinnish.fi/)
- Nagyon jó nyelvi tesztek különféle szinteken: [Osaan suomea](https://www.osaansuomea.fi/)


## Finnish Short Texts I Like

OP:n palvelut toimivat normaalisti. Op.fi:ssä oli aiemmin palvelunestohyökkäyksestä johtunut häiriö. Hyökkäys on saatu pääosin estettyä ja tilanne on normaali. Ajoittaista häiriötä saattaa kuitenkin esiintyä. Asiakkaidemme tiedot ja rahat eivät ole vaarantuneet. Seuraamme tilannetta tehostetusti ja pahoittelemme aiheutunutta haittaa.

>Az OP szolgáltatásai normálisan működnek. Az Op.fi-n korábban egy szolgáltatásmegtagadási támadás okozta fennakadást. A támadást többnyire sikerült megakadályozni, a helyzet normális. Időnkénti zavarok azonban előfordulhatnak. Ügyfeleink információi és pénze nem került veszélybe. Fokozottan figyelemmel kísérjük a helyzetet és elnézést kérünk az okozott kellemetlenségekért.

---


Example sentences to spread out on this page (temporary; not all of them remain): 

Simple statements:
- **Me kävelemme keskellä katua.** - We are walking in the middle of the street.
- **Auto seisoo keskellä katua.** - The car is standing in the middle of the street.
- **Pöytä on keskellä ravintolaa.** - The table is in the middle of the restaurant.
- **Nuo suot ovat keskellä Suomea.** - Those bogs are in the middle of Finland.
- **Hän etsii toista sauvaa.** - She is looking for the other ski pole.
- **Läppärissä on outo peli.** - There is a strange game on the laptop.
- **Ilves haistelee pientä puuta.** - The lynx is sniffing the small tree.
- **Mummo harrastaa matkailua.** - Grandma has traveling as a hobby.
- **Moni suomalainen harrastaa pesäpalloa.** - Many Finns have nestball (Finnish baseball) as a hobby.
- **Matkailu on nyt vaikeaa, koska olemme karanteenissa.** - Traveling is now difficult, because we are in quarantene. 
- **Voi ei! Uikkarit ovat kotona!**
- **Nämä housut maksavat liikaa.** - Those pants cost too much.
- **Se pitää korjata.** - It needs to be fixed.
- **Kai sillä on jano.** - I guess it is thirsty.
- **Minulla on kiire, mutta ehkä myöhemmin.** - I am busy, but maybe later.
- **Onneksi tässä kaupungissa on metro.** - Good thing this city has a metro.
- **Minä katselen ja katselen kahta tummaa pilveä.** - I keep on watching two dark clouds.
- **Minulla on ikävä tuota kännykkää.** - I miss that cell phone.
- **Nuo housut maksavat sata euroa.** - Those pants cost one hundred euros.
- **Kaikki on ihan hyvin.** - Everything is quite OK.
- **Isi etsii täydellistä kuusta.** - Daddy is looking for a perfect spruce.
- **Ihailen kaunista vihreää lehteä.** - I'm admiring the beautiful green leaf.
- **Mummo ja vaari kuuntelevat viittä eri lintua.** - Grandma and grandpa are listening to five different birds.
- **He juoksevat alas mäkeä.** - They are running down the hill.
- **Olemme pahoillamme, mutta olemme kiinni.** - Our apologies, but we are closed.
- **Rakastan tätä pörröistä koiraa.** - I love this fluffy dog.
- **Pöllö on piilossa isossa puussa.** - The owl is hiding in a big tree.
- **Tarjoilija on väsynyt.** - The waiter is tired.
- **Tuo joki on lähellä Oulua.** - That river is close to Oulu.
- **Heillä on kolme viikkoa aikaa myydä tuo rakennus.** - They have three weeks to sell that building.
- **Otso kuulee, että puro on lähellä.** - Otso can hear that the stream is near.
- **Vesi on syvää.** - The water is deep.
- **Punainen lintu on vihainen.** - The red bird is angry.
- **Salmiakki on yleensä mustaa.** - Salmiakki is usually black.
- **He myyvät nykyään myös karkkia.** - They sell candy too these days.

Statements with denial: 
- **En halua hidasta tablettia.** - I do not want a slow tablet.
- **Tässä ei ole mikrofonia.** - There is no microphone here.
- **Onneksi me emme tarvitse autoa.** - Good thing we do not need a car.
- **Tuo ei ole järvi eikä meri.** - That is neither a lake nor a sea.

Simple questions:
- **Saisimmeko kalaa ja riisiä?** - Can we get fish and rice?
- **Miksi tuo vyö on niin halpa?** - Why is that belt so cheap?
- **Miksi harrastat urheilua?** - Why do you have sport as a hobby?
- **Onko sinulla tuota sovellusta?** - Do you have that app?
- **Oletko sinä hyvä pesäpallossa?** - Are you good in nestball?
- **Onko tuossa ravintolassa terassia?** - Is there a terrace in that restaurant?
- **Apua! Missä lääkkeet ovat?** - Help! Where are the medicines?
- **Millaiset lasit ne ovat?** - What kind of glasses are they?
- **Oletko varma, että pyyhe on minulla?** - Are you sure that I have the towel?
- **Millaiset korut ovat muotia?** - What kind of jewelry is in fashion?
- **Paljonko nämä ruskeat lasit maksavat?** - How much do these brown glasses cost?
- **Osaatko sinä käyttää Duolingoa?** - Do you know how to use Duolingo?
- **Osaavatko kaikki pingviinit pelata jääkiekkoa?** - Can all penguins play ice hockey?
- **Onko tämä silta turvallinen?** - Is this bridge safe?
- **- Mitä herkkua tämä on? – Se on kiisseliä.** - What delicacy is this? – It is kissel.
- **– Miksi sinä hymyilet? – Koska tämä metsä on niin kaunis.** - – Why are you smiling? – Because this forest is so beautiful.
- **Montako minuuttia metro on myöhässä?** - By how many minutes is the metro late?
- **Onko maalaus tässä museossa?** - Is the painting in this museum?

Questions that involve denial: 
- **Miksi sinulla ei ole pipoa?** - Why do you not have a beanie?
- **Eivätkö vesipullot ole teillä?** - Don't you have water bottles?
- **Miksi tämä ei ole päällä?** - Why isn't it on?
- **Miksi sinä et halua nettiä etkä kännykkää?** - Why do you want neither internet nor a cell phone?
- **Syötkö sinä pullaa?** - Do you eat pulla?

Simple exclamations: 
- **Huh, mikä kiire!** - Whoa, so busy! / Whoa, what a hurry!
- **Huh, mikä ruuhka!** - Whoa, what a traffic jam!

Remains unsorted for now (beyond temporary):
- **Anteeksi, mutta minulla ei ole lusikkaa.** - Excuse me, but I do not have a spoon.
- **Paljonko tuo täytekakku maksaa?** - How much does that layered cake cost?
- **korkea puu; pitkä ihminen** - a tall tree; a tall person
- **Syön banaania.** - I'm eating a banana.
- **Mitä he pelaavat?** - What are they playing?
- **Vahva mies juo vahvaa kahvia.** - The strong man is drinking strong coffee.
- **Mummo halaa onnellista morsianta.** - Grandma is hugging the happy bride.
- **Korkeassa puussa elää viisas pöllö.** - There is a wise owl living in the tall tree.
- **Tämä suklaa on herkullista!** - This chocolate is delicious!
- **Paljonko kaksi karjalanpiirakkaa maksaa?** - How much do two Karelian pasties cost?
- **Suo on märkä paikka.** - A bog is a wet place.
- **– Kuinka usein maalaat? – Joka päivä.** - – How often do you paint? – Every day.
- **– Paljonko kello on? – Kaksitoista.** - – What time is it? – Twelve o'clock.
- **Onko tämä liha paikallista?** - Is this meat local?
- **Halaan ujoa naista, vaikka minä olen myös ujo.** - I hug the shy woman, even though I am also shy.
- **Tämä ei ole kiisseliä vaan hilloa.** - This is not kissel but jam.
- **Haluamme, että tämä video on täydellinen.** - We want this video to be perfect.
- **Tuo leivos on pieni ja halpa.** - That pastry is small and cheap.
- **Etkö asu enää Suomessa? / Ettekö te asu enää Suomessa?** - Do you not live in Finland anymore?
- **Tämä ei ole edes päällä!** - This is not even on!
- **Tuolla on muutama pilvi.** - There are a few clouds over there.
- **Tässä on muutama vadelma.** - It has a few raspberries.
- **Ketä te halaatte?** - Who are you hugging?
- **Paljonko yksi glögi maksaa?** - How much does one glögi cost?
- **Tuo hirvi on vihainen.** - That moose is angry.
- **Omistan kahdeksan sinistä undulaattia.** - I own eight blue parakeets.
- **Minä en osaa käyttää tuota radiota.** - I do not know how to use that radio.
- **Sillä on kolme kaunista pentua.** - It has three beautiful kittens.
- **Anteeksi, emme osaa puhua englantia.** - Sorry, we cannot speak English.
- **Olen todella pahoillani.** - My deepest apologies.
- **Meillä ei ole yhtään suklaata.** - We do not have any chocolate.
- **Meri on suolainen.** - The sea is salty.
- **Koira on surullinen. Sillä on ikävä heitä.** - The dog is sad. It misses them.
- **Rakastan sinua niin paljon!** - I love you so much!
- **kymmenen lemmikkiä** - ten pets
- **Mikä eläin tuo on?** - What animal is that?
- **– Paljonko kello on? – Yksitoista.** - – What time is it? – Eleven o'clock.
- **Oho, tämä viini on melko kallista.** - Oh wow, this wine is pretty expensive.
- **He kuuntelevat, kun lintu laulaa, koska sillä on todella kaunis ääni.** - They listen when the bird sings because it has a really beautiful voice.
- **Huomaatko? Tuo orava on jo harmaa.** - Do you notice? That squirrel is already gray.
- **Me emme ole saunassa emmekä metsässä.** - We are neither in the sauna nor in the forest.
- **Luuletko, että tuo marja on myrkyllinen?** - Do you reckon that that berry is poisonous?
- **Kaksi salaattia ja yksi annos kalaa kiitos** - Two salads and one portion of fish, please.
- **Tyyne käy melko harvoin Lontoossa.** - Tyyne visits London pretty rarely.
- **Missä hotellissa sinä asut?** - In which hotel are you staying?
- **Mies toivoo, että saa olla rauhassa.** - The man hopes that he gets to have some peace and quiet.
- **Can you hear? The cook is singing.** - Can you hear? The cook is singing.
- **Käykö Elsa usein Oslossa?** - Does Elsa visit Oslo often?
- **– Mitä ja missä te opiskelette? – Minä opiskelen historiaa Roomassa ja Anna opiskelee kemiaa Tallinnassa.** - – What and where do you study? – I study history in Rome and Anna studies chemistry in Tallinn.
- **Onko sinulla uusi puhelin?** - Do you have a new phone?
- **Morsian hymyilee.** - The bride is smiling.
- **Kuinka korkea tuo tammi on?** - How tall is that oak?
- **Tuo väri on melko kirkas.** - That color is pretty bright.
- **Korussa on kaunis helmi.** - There is a beautiful pearl in the piece of jewelry.
- **Poro seuraa minua.** - The reindeer is following me.
- **Nämä vanhat karhut elävät Suomessa.** - These old bears live in Finland.
- **Berliinissä on melkein neljä miljoonaa asukasta.** - There are almost four million inhabitants in Berlin.
- **Kuinka monta korvapuustia sinä haluat?** - How many cinnamon rolls do you want?
- **Tämä riisi on melko suolaista.** - This rice is pretty salty.
- **Pöytä on keskellä ravintolaa.** - The table is in the middle of the restaurant.
- **Montako vyötä sinä omistat?** - How many belts do you own?
- **Korvapuusti on pullaa.** - The cinnamon roll is pulla.
- **Sveitsiläiset kellot ovat tuolla.** - The Swiss watches are over there.
- **Pöytä on keskellä ravintolaa.** - The table is in the middle of the restaurant.
- **Onko tuo voi suomalaista?** - Is that butter Finnish?
- **Sveitsiläiset korut ovat tuossa.** - The Swiss jewelry is right there.
- **Sulavatko ne?** - Are they melting?
- **Onko uusi tulostin tarpeeksi nopea?** - Is the new printer fast enough?
- **Te olette nyt mies ja vaimo.** - You are now husband and wife.
- **Tuossa sushissa on riisiä ja kalaa.** - There is rice and fish in that sushi.
- **Valkoinen lintu seuraa pilveä.** - The white bird is following the cloud.
- **Millainen leivos se on?** - What kind of pastry is it?
- **Äiti haluaa olla lähellä vauvaa.** - The mother wants to be near the baby.
- **Meri ei ole tyyni mutta Tyyne on.** - The sea is not calm, but Tyyne is.
- **Pieni hetki, meillä on kiire.** - Just a moment! We are busy.
- **Nainen, jota rakastan, asuu kaukana Suomessa.** - The woman whom I love lives far away in Finland.
- **On yö; heillä ei ole kiire.** - It is night; they are not busy.
- **Missä olet ostoksilla?** - Where are you shopping?
- **Mitä kieltä pöllöt puhuvat?** - What language do owls speak?
- **Kaikki on ihan hyvin.** - Everything is quite OK.
- **Koko puro on jäässä.** - The whole stream is frozen.
- **Haluan olla lähellä sinua.** - I want to be near you.
- **Ovatko he oikeassa vai väärässä?** - Are they right or wrong?
- **Tuolla on muutama pilvi.** - There are a few clouds over there.
- **Voi voi, onko hän taas myöhässä?** - Uh-oh, is he late again?
- **Sillä on pieni kivi.** - It has a small rock.
- **Apua! Tämä on liian painava!** - Help! This is too heavy!
- **Täällä on söpö siili.** - There is a cute hedgehog over here.
- **Pari on marjassa keskellä metsää.** - The couple is picking berries in the middle of the forest.
- **Tuossa on muutama sieni.** - There are a few mushrooms right there.
- **Olen myöhässä!** - I am late!
- **Pieni hetki! Meillä on kiire.** - Just a moment! We are busy.
- **Poika, joka juoksee tuolla rakastaa tyttöä jota sinä ihailet.** - The boy, who is running over there, loves the girl whom you admire.
- **Minulla on ikävä tuota kännykkää.** - I miss that cell phone.
- **Lumi on kylmää.** - The snow is cold.
- **Nuo lasit maksavat liikaa. / Nuo lasit maksavat liian paljon.** - Those glasses cost too much.
- **Isi etsii täydellistä kuusta.** - Daddy is looking for a perfect spruce.
- **Maassa on uutta lunta. / Maassa on tuoretta lunta.** - There is fresh snow on the ground.
- **Meillä on ikävä heitä.** - We miss them.
- **Haluamme maistaa viittä eri marjaa.** - We want to taste five different berries.
- **Kasvissa on vain yksi ruskea lehti.** - The plant only has one brown leaf.
- **Kolme pientä hiirtä juoksee nopeasti pois.** - Three small mice run away quickly.
- **Juon kahvia ja kuuntelen viittä pientä lintua jotka laulavat puussa.** - I am drinking coffee and listening to the five small birds, which are singing in the tree.
- **Minä olen väsynyt koska on myöhä.** - I am tired, because it is late.
- **Me kävelemme alas mäkeä.** - We are walking down the hill.
- **Paljonko nämä omenat maksavat?** - How much do these apples cost?
- **Yksi kahvi. Mustana, kiitos.** - One coffee. Black, please.
- **Ihailen kaunista vihreää lehteä.** - I am admiring a beautiful, green leaf.
- **Purossa on kirkasta vettä.** - There is clear water in the creek.
- **Me haluaisimme miettiä vielä vähän aikaa.** - We would like to think for a while longer.
- **Olen todella pahoillani.** - My deepest apologies.
- **Kaksi sutta juoksee ja juoksee.** - Two wolves keep on running.
- **Hän kävelee hiljaa kuin hiiri.** - He walks as silently as a mouse.
- **Lintu katselee kahta korkeaa puuta.** - The bird is looking at two tall trees.
- **– Anteeksi, minulla on kiire. – No ei se mitään. Nähdään myöhemmin.** - – Sorry, I am in a hurry. – Well, that is OK. See you later.
- **Anteeksi, tarjoilija. Lasku, kiitos.** - Excuse me, waiter. Check, please.
- **Kuuletko? Lintu on lähellä.** - Can you hear? The bird is near.
- **Meillä on kaksi viikkoa aikaa ostaa uusi asunto.** - We have two weeks to buy a new apartment.
- **Koko suo on oranssi.** - The whole bog is orange.
- **saada** - to get
- **Äiti ja isä rakastavat minua.** - Mother and father love me.
- **Minä ihailen taivasta.** - I am admiring the sky.
- **kuulla** - to hear
- **Miksi se ei ole seinässä?** - Why is it not plugged in?
- **Venäläinen näytelmä on suosittu.** - The Russian play is popular.
- **Nuo suot ovat keskellä Suomea.** - Those bogs are in the middle of Finland.
- **Minä en omista yhtään sormusta.** - I do not own a single ring.
- **Minulla on nälkä. Missä lörtsyt ovat?** - I am hungry. Where are the lörtsy?
- **Vadelmat kasvavat nopeasti.** - The raspberries are growing fast.
- **Hän juo vain vahvaa kahvia.** - She drinks only strong coffee.
- **Voi ei, seisooko kello?** - Oh no! Has the clock stopped?
- **Me emme myy glögiä.** - We do not sell glögi.
- **Kahvilat ovat vielä kiinni.** - The cafés are still closed.
- **– Miksi sinä hymyilet? – Koska tämä metsä on niin kaunis.** - – Why are you smiling? – Because this forest is so beautiful.
- **Tämä kiisseli on sinistä.** - This kissel is blue.
- **Suuri kenguru potkaisee autoa.** - The large kangaroo kicks the car.
- **Leo, haluatko lisää täytekakkua?** - Leo, do you want more layered cake?
- **Isi, minä haluaisin käydä vessassa.** - Daddy, I would like to go to the restroom.
- **Mitä sarjaa te katselette?** - What series are you watching?
- **Mitä peliä te haluatte pelata?** - What game do you want to play?
- **Netti pätkii taas.** - The net is cutting in and out again.
- **Korkeassa puussa elää viisas pöllö.** - There is a wise owl living in the tall tree.
- **Poro kuulee että ahma on lähellä.** - The reindeer can hear that the wolverine is near.
- **Montako asukasta Oulussa on?** - How many inhabitants does Oulu have?
- **Onko teillä tätä paitaa harmaana?** - Do you have this shirt in gray?
- **Tämä pasta on herkullista.** - This pasta is delicious.
- **Tässä rahkassa on appelsiinia ja banaania.** - There is orange and banana in this quark.
- **Kaksi berliiniläistä miestä katselee yhdessä ranskalaista elokuvaa.** - Two men from Berlin are watching a French movie together.
- **Vyössä on musta sydän.** - There is a black heart in the belt.
- **Mummo halaa surullista miestä.** - Grandma hugs the sad man.
- **haistella** - to sniff
- **- Oletko amerikkalainen? – En. Olen kanadalainen.** - – Are you American? – No. I am Canadian.
- **Tuo paikka on lähellä Helsinkiä.** - That place is close to Helsinki.
- **Tuo ahma elää Australiassa.** - That wolverine lives in Australia.
- **Nämä vanhat karhut elävät Suomessa.** - These old bears live in Finland.
- **Riisissä on liian vähän suolaa.** - There is too little salt in the rice.
- **Ehkä he eivät puhu venäjää.** - Maybe they do not speak any Russian.
- **Turistit ihailevat sinivalkoista taivasta.** - The tourists are admiring the blue and white sky.
- **Minkäkokoista paitaa etsit?** - What size shirt are you looking for?
- **– Montako sekuntia meillä on? – Kymmenen.** - – How many seconds do we have? – Ten.
- **Voi voi, jäätelöt sulavat.** - Uh-oh, the ice creams are melting.
- **Millainen sovellus se on?** - What kind of app is it?
- **Karhut haistelevat kasvia.** - The bears are sniffing the plant.
- **Miksi te ette asu enää Berliinissä? / Miksi et asu enää Berliinissä?** - Why do you not live in Berlin anymore?
- **Onko se tarpeeksi nopea?** - Is it fast enough?
- **- Yksi lörtsy, kiitos.** - Suolainen vai makea?** - – One lörtsy, please. – Savory or sweet?
- **Paljonko nuo isot korvapuustit maksavat?** - How much do those big cinnamon rolls cost?
- **Miksi tuo vyö on niin halpa?** - Why is that belt so cheap?
- **Saisimmeko me sushia, kiitos?** - Could we have some sushi, please?
- **Mitä kokoa nämä housut ovat?** - What size are these pants?
- **Tuo kasvi on outo.** - That plant is weird.
- **Me haluamme halata sinua. / Haluamme halata teitä.** - We want to hug you.
- **Kuppi kahvia ja munkki, kiitos.** - A cup of coffee and a jelly doughnut, please.
- **Italialaiset kengät ovat tuolla.** - The Italian shoes are over there.
- **En halua kirjavaa kravattia.** - I do not want to have a colorful tie.
- **Kulta, minä en halua, että sinä itket.** - Darling, I do not want you to cry.
- **Ilves elää yleensä metsässä.** - The lynx usually lives in a forest.
- **Onko tämä liha paikallista?** - Is this meat local?
- **Tämä ei ole kiisseliä vaan hilloa.** - This is not kissel but jam.
- **Oho, tuossa on poro.** - Oh! There is a reindeer right there.
- **Emme voi ostaa tuota kännykkää, koska meillä ei ole tarpeeksi rahaa.** - We cannot buy that cell phone, because we do not have enough money.
- **Haluaisitko vielä jotain? Ehkä kuppi kahvia?** - Would you like to have something else? Perhaps a cup of coffee?
- **Tässä on kaksi kukkaa.** - It has two flowers.
- **Joki on pitkä ja syvä.** - The river is long and deep.
- **Pieni hetki, meillä on kiire.** - Just a moment! We are busy.
- **En halua nousta ylös vaikka on aamu.** - I do not want to get up, although it is morning.
- **Me emme halua tuota kameraa.** - We do not want that camera.
- **On kylmä päivä, mutta minulla on hiki.** - It is a cold day, but I am sweating.
- **Millaiset vyöt ovat nyt muotia?** - What kind of belts are now in fashion?
- **Viineri on tanskalainen leivos.** - The Danish pastry is a Danish pastry.
- **– Se on ihan hyvä. – Oletko varma?** - – It is just fine. – Are you sure?
- **Äiti rakastaa meitä.** - Mother loves us.
- **Ilves kävelee pois.** - The lynx walks away.
- **Miksi et asu enää Kanadassa?** - Why do you not live in Canada anymore?
- **Tarjoilija, voisitko suositella jotain?** - Waiter, could you recommend something?
- **Kaksi pirtelöä, ole hyvä.** - Here you are, two milkshakes.
- **Olet väärässä; tuo kaupunki on Puolassa.** - You are wrong; that city is in Poland.
- **Paljonko nuo harmaat lasit maksavat?** - How much do those gray glasses cost?
- **Paljonko kello on? Se on yksi.** - – What time is it? – It is one.
- **Tuolla on muutama pilvi.** - There are a few clouds over there.
- **Hän todella rakastaa tätä hevosta.** - She really loves this horse.
- **Paljonko nuo pirtelöt maksavat?** - How much do those milkshakes cost?
- **Sillä on kolme kaunista pentua (/kissanpentua).** - It has three beautiful kittens.
- **Olen pahoillani, meillä ei ole yhtään teetä.** - My apologies, we do not have any tea.
- **Metsässä elää hirvi.** - There is a moose living in the forest.
- **Koira on surullinen. Sillä on ikävä heitä.** - The dog is sad. It misses them.
- **Nuo Viikingit käyvät usein Helsingissä.** - Those Vikings visit Helsinki often.
- **Saisimmeko lisää viiniä?** - Could we have more wine?
- **Mekko on valkoinen kuin lumi.** - The dress is as white as snow.
- **Puu on kasvi.** - A tree is a plant.
- **Se on seinässä.** - It is plugged in.
- **Sinä et ole marjassa etkä kalassa.** - You are neither picking berries nor fishing.
- **Eikö sinulla ole läppäriä?** - Do you not have a laptop?
- **Minulla on viikko aikaa siivota tämä asunto.** - I have a week to clean this apartment.
- **Miksi minä en saa lisää pullaa?** - Why can I not get more pulla?
- **Hymyilemme, koska kävelemme metsässä.** - We are smiling, because we are walking in the forest.
- **Käykö Elsa usein Oslossa?** - Does Elsa visit Oslo often?
- **Minkäväristä hattua etsit?** - What color hat are you looking for?
- **Hän syö viineriä kahvilassa.** - How many universities are there in Finland?
- **Netti ei toimi.** - The Net is not working.
- **Hyi, vessa on likainen.** - Yuck, the restroom is dirty.
- **Mitä peliä te pelaatte?** - What game are you playing?
- **Mitä sarjaa te katselette?** - What series are you watching?
- **Vahva mies juo vahvaa kahvia.** - The strong man is drinking strong coffee.
- **Kuinka korkea tuo tammi on?** - How tall is that oak?
- **Ajatteletko häntä usein?** - Do you think of him often?
- **Anteeksi, osaatko sanoa, missä lähin hotelli on?** - Excuse me, can you tell me where the closest hotel is?
- **Tämä pasta on herkullista.** - This pasta is delicious.
- **Oululainen perhe katselee kotona elokuvaa.** - The family from Oulu is at home watching a movie.
- **Meri on vaarallinen paikka.** - The sea is a dangerous place.
- **Pelissä on monta prinsessaa.** - There are many princesses in the game.
- **Minkäkokoista paitaa etsit? / Minkä kokoista paitaa sinä etsit?** - What size shirt are you looking for?
- **Montako korvapuustia haluatte?** - How many cinnamon rolls do you want?
- **– Yksi lörtsy, kiitos. – Suolainen vai makea?** - – One lörtsy, please. – Savory or sweet?
- **Riisissä on liikaa pippuria** - There is too much pepper in the rice.
- **Voi voi, jäätelöt sulavat.** - Uh-oh, the ice creams are melting.
- **Kylmässä metsässä juoksee harmaa susi.** - There is a gray wolf running in the cold forest.
- **Pöytä on keskellä ravintolaa.** - The table is in the middle of the restaurant.
- **Tuossa on marja.** - There is a berry right there.
- **Suomalaiset korut ovat täällä.** - The Finnish jewelry is over here. (the "jewerly" is in plural)
- **Täällä on tyhjää ja hiljaista.** - It is empty and quiet here.
- **Saksalainen tyttö juoksee, koska hänellä on kiire** - The German girl is running, because she is in a hurry.
- **Joki on pitkä ja syvä.** - The river is long and deep.
- **Se on rikki ja pitää korjata.** - It is broken and needs to be fixed.
- **On yö; heillä ei ole kiire.** - It is night; they are not busy.
- **Tuolla on muutama pilvi.** - There are a few clouds over there.
- **Täällä on muutama vadelma.** - There are a few raspberries over here.
- **lumi ja jää** - snow and ice
- **Kaikki ihailevat teitä.** - Everyone admires you. ("kaikki" is plural)
- **Miksi te ette asu enää Berliinissä? / Miksi sinä et asu enää Berliinissä?** - Why do you not live in Berlin anymore?
- **Tuolla kasvaa tammi. / Tuolla on tammi.** - There is an oak growing over there.
