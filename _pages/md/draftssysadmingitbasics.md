# ![alt_text](/_ui/img/icons/git_m.svg) Git Basics

<div style="text-align: right;"><h7>First article here about the subject posted: 2019-07-30 13:17</h7></div>

###### .

![alt_text](git_basics_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Version Control Basics](#2)
3. [Working with Remote Repositories](#3)
4. [Advanced Techniques for Daily Use](#4)
5. [VSCode Integration](#5)


<a name="1"></a>
## 1. Introduction

Git's primary purpose is **version control**, which means it automatically keeps tracking your changes under the protected directory. This protected directory is called **repository**.e

The secondary purpose is syncing these changes to or from a Git server, so it can be considered also as a **backup**.


<a name="2"></a>
## 2. Version Control Basics

Let's see first how you can create a new Git repository first on your local machine once `git` is installed. 

1. Initializing your repository by a new `/path/to/my/codebase/.git` directory (the command `git init` creates a new `master` repository, but let's follow the latest trends and create it as `main` instead): 

```ksh
$ cd /path/to/my/codebase

$ ls -la
total 12
drwxr-xr-x. 2 gjakab users 4096 Oct  3 22:21 .
drwxr-xr-x. 3 gjakab users 4096 Oct  3 22:03 ..
-rwxr-xr-x. 1 gjakab users  510 Oct  3 22:04 zombietrack.sh

$ git init --initial-branch=main
Initialized empty Git repository in /path/to/my/codebase/.git/
```

2. You can already check the `status` and notice how nicely `git` recommends adding the existing files/directories if any: 

```ksh
$ git status
On branch main
[...]
No commits yet
[...]
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	zombietrack.sh
[...]
nothing added to commit but untracked files present (use "git add" to track)
```

3. Add all existing files to the index:  
  (Note that, in this example, there is only one file, so even `git add zombietrack.sh` would do instead of adding everything with `git add .`.)

```ksh
$ git add .

$ git status
On branch main
[...]
No commits yet
[...]
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   zombietrack.sh
```

4. Ensure your identity (your `user.name` and your `user.email`) is either already configured in `git`'s global settings or at least locally defined for this particular repository. You will get something like the following if nothing has been set yet: 

```ksh
$ git config --list
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
```
- Specifying the identity **locally** for this repo: 

```ksh
$ git config user.email "gjakab@example.com"
$ git config user.name "Jakab Gipsz"
```

- Specifying the identity **globally** (for any further repositories too, as a default set of value): 

```ksh
$ git config --global user.email "gjakab@example.com"
$ git config --global user.name "Jakab Gipsz"
```

5. Record the pristine state as the first commit in the history (you will get a "fatal" error, if your identity hasn't been set yet in the previous step): 

```ksh
$ git commit -m "my first commit"
[main (root-commit) f2f4da3] my first commit
 1 file changed, 20 insertions(+)
 create mode 100755 zombietrack.sh
```

And now you are done with your **initial steps**. If you're just creating a new repo, then you can move on to **chapter 2.2.** 

The next steps are just a representation of `git` basic functionality: 

6. Change something on your existing script and check the `git status`: 

```ksh
$ vi zombietrack.sh 

$ git status
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   zombietrack.sh
[...]
no changes added to commit (use "git add" and/or "git commit -a")
```

7. "Stage" the file by `git add` and `commit` again: 

```ksh
$ git add .

$ git commit -m "docs: add comment to zombietrack.sh"
[main f07a0d5] docs: add comment to zombietrack.sh
 1 file changed, 1 insertion(+)
```

Note: **Staging** in the Web IDE of GitLab is the same as the `git add` CLI command.

<a name="3"></a>
## 3. Working with Remote Repositories

The remote repo is what we can call also as **push destination**. Specifying it is the point, when you need to have an account either at [github.com](https://github.com), or at [gitlab.com](https://gitlab.com), or on your own (self-managed) GitLab CE server, or somewhere else. 

Besides the account, it is heavily advised to have your **public SSH key** uploaded to the remote site. 

- on **GitHub**, it can be done at your user's **Settings** &rarr; **SSH and GPG keys**
- on **GitLab**, it can be done at your user's **Preferences** &rarr; **SSH Keys**

(Refer to other articles if you don't know how to create SSH key pair.)

### 3.1 Initializing the remote repository

Once the public SSH key is populated remotely, you need to **create your new repo even on server side**. On GitHub, it is called simply as **repository**, but on GitLab, it is a **project**.  

On **GitLab SaaS**, it looks like this: 

![alt_text](gitlab_-_saas_-_new_project.png)

Once created, you can define the already provided `git@...` link locally as `remote`, then `push` all your previously added contents to the remote site. The `-u` makes sure that the remote site is set as "upstream" or "tracking" reference for your codebase: 

```ksh
$ git remote add origin git@gitlab.com:gipszjakab/codebase.git

$ git push -u origin main
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
[...]
 * [new branch]      main -> main
Branch 'main' set up to track remote branch 'main' from 'origin'.
```

### 3.2 Cloning an existing remote repository

The easier way for all the above is assuming that the codebase is already on remote location, it is up-to-date, and you just want to fetch everything with `git clone` to yourself as a local clone. It already defines also the initial/main branch: 

```ksh
$ git clone git@gitlab.com:gipszjakab/codebase.git
Cloning into 'codebase'...
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 6 (delta 1), reused 6 (delta 1), pack-reused 0
Receiving objects: 100% (6/6), done.
Resolving deltas: 100% (1/1), done.

$ cd codebase/

$ git status
On branch main
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean

$ ls -la
total 8
drwxr-xr-x. 3 gjakab users 1024 Oct  3 23:00 .
drwx------. 4 gjakab users 1024 Oct  3 23:00 ..
drwxr-xr-x. 8 gjakab users 1024 Oct  3 23:00 .git
-rwxr-xr-x. 1 gjakab users  538 Oct  3 23:00 zombietrack.sh
```

### 3.3 Pulling to ensure your local copy follows the remote repository

If you didn't do any changes and it's OK you just download any changes if any, to ensure you are up-to-date with your team's recent changes, then maybe ensuring you're in the primary/main branch, then issue a `git pull`. E.g.: 

```ksh
$ cd /path/to/my/codebase

$ git status
On branch main
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean

$ git pull
Already up to date.
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 265 bytes | 265.00 KiB/s, done.
From gitlab.com:gipszjakab/codebase
   0772b09..03abbaf  main       -> origin/main
Updating 0772b09..03abbaf
Fast-forward
 zombietrack.sh | 1 -
 1 file changed, 1 deletion(-)

$ git status
On branch main
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean
```

- if only the **remote** contents changed, then it will be the above case, so only your local copy becomes up-to-date
- if you've modified/added any files on your **local** folder, it won't be deleted but it remains untracked, so you need to `git add` it first, then `git commit`, then `git push`
- if **both your local copy and the remote "origin" changed**, `git pull` will fail, because `git` tries to avoid loosing any of your local data. In that case, you need to ensure you either added your changes somehow (we will discuss it later), or just used `git restore <file>` for that file. The `restore` operation reverts the contents of your local copy to the last known local version. After that, you can use `git pull` normally, as in the first case. 

<div class="warn">
For now, there are only slight differences between using either GitHub or GitLab. And it remains the same when using GitLab only for Source Control.
</div>

### 3.4 Simple Source Control in Action with `git` CLI Commands Only

This below is a possible walkthrough how you can perform your everyday work when it concerns these tasks only: 

1. Ensure you have the upstream branch (usually `main`) in sync on your computer:  
```ksh
$ git status
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
$ git pull
Already up to date.
```
2. Create a new branch for your changes:  
```ksh
$ git branch sre-7383 main

$ git -P branch
* main
  sre-6876
  sre-7337
  sre-7383

$ git checkout sre-7383
Switched to branch 'sre-7383'

$ git status
On branch sre-7383
nothing to commit, working tree clean
```
3. Perform your changes
4. Stage your changed file for committing:  
```ksh
$ git status
On branch sre-7383
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   vault.kdbx

no changes added to commit (use "git add" and/or "git commit -a")

$ git add vault.kdbx
```
5. Commit your changes:  
```ksh
$ git commit -m "chore(sre-7383): remove my-example-host's obsolete luks passphrase from vault"
[sre-7383 30ac289] chore(sre-7383): remove my-example-host's obsolete luks passphrase from vault
 1 file changed, 0 insertions(+), 0 deletions(-)
```
6. Push your changes as a preparation for MR on Git server (GitLab SaaS, GitLab CE, GitLab EE, GitHub etc.) side:  
```ksh
$ git push origin HEAD
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 235.33 KiB | 18.10 MiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
[...]
 * [new branch]      HEAD -> sre-7383
```

### 3.5 Semantic Commit Messages

The term *"semantic"* in version control includes several general ideas. In summary: 

- the version follows a `major.minor.patch` version, e.g. `1.2.3`
- the version `v1.2.3` is not a semantic version but still the part `1.2.3` of it is
- the `alpha*`, `-beta*`, `-rc*` pre-release versions after the version numbers always have lower priority, e.g. `1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0`.

More on **Semantic Versioning** can be read on [semver.org](https://semver.org/).

The term *"semantic commit message* is rather about expecting categorizing your commits. It's just a very good practice to show **how a minor change to your commit message style can make you a better programmer**.

**Format**: `<type>(<scope>): <subject>`

`<scope>` is optional

The **concept**: 

```
feat: add hat wobble
^--^  ^------------^
|     |
|     +-> Summary in present tense.
|
+-------> Type: chore, docs, feat, fix, refactor, style, or test.
```

In general, you need to try to be short but clear in the same time, and use unconjugated verbs for describing the action. 

**General rules** for these commit types:

- `chore`: (updating grunt tasks etc; no production code change)
- `docs`: (changes to the documentation)
- `feat`: (new feature for the user, not a new feature for build script)
- `fix`: (bug fix for the user, not a fix to a build script)
- `refactor`: (refactoring production code, eg. renaming a variable)
- `style`: (formatting, missing semi colons, etc; no production code change)
- `test`: (adding missing tests, refactoring tests; no production code change)

**Examples**: 

```ksh
$ git commit -m "chore(sre-1234): add example-server1 to webservers group"
[...]
$ git commit -m "feat(pdreq-4321): switch to node18 for xyx bot"
```


<a name="4"></a>
## 4. Advanced Techniques for Daily Use

This chapter is intended to store some knowledge on tasks that are both the practical nature and the slightly more advanced scope compared to the basics.

Further useful daily practicalities, that are rather related to GitLab than Git in general, can be found in the article about [GitLab Basics](/learning/it/sysadmin/git/gitlab/).

### 4.1 Git Stash

If you are on the `main` branch, but you were not "thinking in advance" and you have made some changes already, and now you want to "move" those changes to a new branch, you can follow these steps:

1. **Stash your changes** (initialize your Git Stash) to to save them temporarily:

```ksh
$ git stash
Saved working directory and index state WIP on main: [...]
```

This will store your changes in a special stash area, and it will clean your working directory (remove the changes from the tracked files).

2. Create and switch to the **new branch**:

```ksh
$ git checkout -b csre-799
Switched to a new branch 'csre-799'
```

This command will create a new branch named `csre-799` and check it out.

3. Now, you can **apply the stashed changes** to the new branch:

```ksh
$ git stash pop
On branch csre-799
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   ansible-hosts

no changes added to commit (use "git add" and/or "git commit -a")
Dropped refs/stash@{0} (bacce397a4bc655aea6e6e04629613658e19ee30)
```

This will reapply the changes that were saved in the stash to your new branch. The pop command removes the changes from the stash and applies them.

4. After applying the changes to the new branch, you can now **commit** them:

```ksh
$ git add .

$ git commit -m "chore(csre-799): add xyz server to maintenance"
[...]
```

**Optional**: if you want to check what’s in the stash before applying it:

```ksh
$ git stash list
[...]

$ git stash show -p
[...]
```


<a name="5"></a>
## 5. VSCode Integration

The MS Visual Studio Code integrates the standard Git functionalities in a very nice way, without any additional extensions. 

While the above procedure is fine for simple changes, VSCode provides a still quite convenient way of working for complicated ones. Here is an example: 

1. First make sure the default/protected branch is up-to-date (pulled), e.g.: 

```ksh
$ cd common-cicd-config-private-cloud

$ git pull
Already up to date.

$ git status
On branch main
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean
```

2. Browse the directory with VSCode too, then go to its **Source Control** tab and click on the "**...**" icon (**Views and More Actions...**), and select **Branch** &rarr; **Create Branch From...**:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_1.png)  
  (Originally, you had to specify the new branch name first, then selected the original branch. It changed around `v1.78.0` version of VSCode to the opposite approach: original first then new second.)

<div class="warn">
<p>If you cannot see options for this, you need to open a new <b>Terminal</b> in VSCode, change to the directory of the repo there, and issue the above <code>git pull</code> command there. The options should appear in this tab right after you did it.</p>
</div>

3. Select the original branch:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_2.png)

4. Specify the new branch's name to create locally:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_3.png)

5. Optionally, you can ensure that you are in your new branch now: 

```ksh
$ git status
On branch sre-4583
nothing to commit, working tree clean
```

6. Make your changes. VSCode will perfectly highlight the modifications on file level with `M` (*modified*) and/or `U` (*untracked*) letters:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_4.png)

7. The actual comparison is also perfectly visible in **Source Control** tab when you click on the files to be committed.  
  When you are done editing, write a commit message and **Commit**:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_5.png)

9. Say **Yes** to the warning about absence of stages:  
	![alt_text](gitlab_-_vscode_-_new_branch_-_6.png)

10. **Publish Branch** (push it to server with the same branch name as you did locally):  
	![alt_text](gitlab_-_vscode_-_new_branch_-_7.png)

11. Find the new branch on GitLab, make your tests if needed, and create the **Merge request** on GitLab. 

### 5.1 Further VSCode Extensions

If you search for the keyword "git" in **Extensions** (`Ctrl+Shift+X`), you may notice there are dozens of VSCode Extensions. Some of them are really useful, while some of them are just wasting your time (as always).

It worth mentioning the one [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens), which might be super useful with its slightly highlighted commit histories during editing:

![alt_text](gitlab_-_vscode_-_gitlense.png)
