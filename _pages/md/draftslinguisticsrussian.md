# ![alt text](/_ui/img/flags/rus_m.svg) Orosz nyelv / Русский язык

###### .

<div class="info">
<p>Ez a bejegyzés még rendkívül hiányos, szerkesztés alatt áll.</p>
</div>

<img src="russia.jpg" class="right">

## Tartalomjegyzék

1. [Bevezető - Вступление](#1)
2. [Az ábécé - Алфавит](#2)
3. [Alapmondatok, kifejezések - Основные предложения, выражения](#3)
4. [Főnév - Существительное](#4)
5. [Melléknév - Прилагательное](#5)
6. [Számnév - Имя числительное](#6)
7. [Névmás - Местоимение](#7)
8. [Ige - Глагол](#8)
9. [Határozószó - Наречие](#9)
10. [Névelő - Артикль](#10)
11. [Elöljárószók - Предлоги](#11)
12. [Kötőszavak, módosítószók és egyéb határozószók - конъюнкции, модификаторы и другие наречия](#12)
13. [Állítás és tagadás - Высказывание и отрицание](#13)
14. [Rövid dialógusok - Короткие диалоги](#14)
15. [Mesék - Сказки](#15)

<a name="1"></a>
## 1. Bevezető - Вступление

...

<a name="2"></a>
## 2. Az ábécé - Алфавит

Az orosz ábécé 33 betűt, ebből 21 mássalhangzót, 10 magánhangzót tartalmaz, kettőnek (keményjel és lágyságjel) pedig nincs önálló hangértéke. 

![alt_text](alphabet_ru.png "Az orosz ábécé")

### 2.1 A betűk kiejtése

| **а** - "á" | **б** - "be" | **в** - "ve" | **г** - "ge" | **д** - "de" | **е** - "je" | **ё** - "jo" | **ж** - "zse" | **з** - "ze" | **и** - "i" | **й** - "jot" | **к** - "ká" | **л** - "el" | **м** - "em" | **н** - "en" | **о** - "o" | **п** - "pe" | **р** - "er" | **с** - "ec" | **т** - "te" | **у** - "u" | **ф** - "ef" | **х** - "há" | **ц** - "ce" | **ч** - "cse" | **ш** - "sá" | **щ** - "scsá" | **ъ** - *keményjel* (твёрдый знак) | **ы** - *jeri* (az "ü" és az "i" közötti hang) | **ь** - *lágyságjel* (мягкий знак) | **э** - "e" | **ю** - "ju" | **я** - "já" |

### 2.2 Az orosz billentyűzetkiosztás

![alt_text](keyboard_ru.jpg "Az orosz billentyűzetkiosztás")

<a name="3"></a>
## 2. Alapmondatok, kifejezések - Основные предложения, выражения

**Здравствуйте!** - Üdvözlet! Üdvözöljük! (általános köszönés)  
**Привет!** - Szia!  
**Доброе утро!** - Jó reggelt!  
**Добрый день!** - Jó napot!  
**Добрый вечер!** - Jó estét!  
**Спокойной ночи!** - Jó éjszakát!  
**Добро пожаловать!** - Isten hozott!  
**Очень приятно!** - Örülök a találkozásnak!  
**До свидания!** - Viszont látásra!  
**Пока!** - Szevasz! / Viszlát!

**Cпасибо!** - Köszönöm!  
**Большое спасибо!** - Nagyon szépen köszönöm!  
**Пожалуйста!** - Kérem! Szívesen!  
**Не за что!** - Nincs mit! Szóra sem érdemes!

**Извините!** - Bocsánat! / Elnézést!  
**Простите!** - Elnézést kérek! (Bocsi, hogy közbevágok, de...)  
**Без проблем! Ничего! Ничего пожалуйста! Ничего страшного!** - Semmi gond!

**Вы говорите по-английски?** - Beszél ön angolul?  
**Я плохо говорю по-русски.** - Rosszul beszélek oroszul.  
**Говорите, пожалуйста, медленнее?** - Beszélne kérem lassabban?  
**Я не понимаю.** - Nem értem.  
**Я понимаю.** - Értem.

**Как дела?** - Hogy vagy?  
**Хорошо, спасибо! А у тебя?** - Jól, köszönöm! És te?  
**Так себе...** - Megvagyok...  
**Вы очень добры!** - Nagyon kedves öntől!

**Удачи!** - Sok szerencsét!  
**С днём рождения!** - Boldog szülinapot!  
**С Новым Годом!** - Boldog Újévet!  
**С Рождеством!** - Boldog Karácsonyt!  
**Поздравляю!** - Gratulálok!  
**На здоровье!** - Egészségedre! (evés, ivás előtt)  
**За здоровье!** - Egészségre! (koccintáskor)  
**Будьте здоровы!** - Egészségedre! (tüsszentéskor)

**Хороший** / **Плохой** / **Так себе** - jól / rosszul / úgy ahogy  
**Маленький** {**-ая**} {**-ое**} / **Большой** {**-ая**} {**-ое**} - nagy / kicsi  
**Сегодня** / **Сейчас** - ma / most  
**Завтра** / **Вчера** - holnap / tegnap

**Да, я хочу́. Cпасибо!** - Igen, kérek. Köszönöm!  
**Cпасибо, я нe хочу!** - Köszönöm, nem kérek!  
**Я голодный.** (**Я хочу́ есть.** / **Я хочу́ кушать.**) - Éhes vagyok. 
**Я нe голодный.** - Nem vagyok éhes.  
**Я хочу́ пить.** - Szomjas vagyok.  
**Я нe хочу́ пить.** - Nem vagyok szomjas.  
**Я наелся.** (hn) / **Я наелась.** (nn) - Tele vagyok.  
**Дайте мне стакан воды, пожалуйста?** - Kaphatok egy pohár vizet?  
**У меня нету чайная ложка. Дайте мне?** - Nincs kiskanalam. Kaphatnék egyet?  
**Это очень вкусно!** - Ez nagyon finom!  
**Это мое любимое блюдо.** - Ez a kedvenc ételem.  
**Где находится?** - Hol van a fürdőszoba?  
**Где туалет?** - Hol van a budi?  
**Идите прямо, потом налево**/**направо!** - Menj egyenesen, aztán fordulj balra/jobbra!  
**Я немного устал.** - Egy kicsit fáradt vagyok.  
**Я сонный.** - Álmos vagyok.

**Вы говорите по-английски** / **по-русски?** - Beszél ön angolul / oroszul?  
**Да, немного.** - Igen, egy kicsit.

**Украина-замечательная страна!** - Ukrajna egy csodálatos ország!  
**Чем вы занимаетесь?** - Mit csinál? (Mi a munkája önnek?)  
**Я работаю с компьютерами.** - Számítógépekkel dolgozom.

<a name="4"></a>
## 4. Főnév - Существительное

Az orosz nyelvben a főnevet ragozzuk, így azok a ragozás során különböző esetekben más-más végződést kapnak. Nézzük az eseteket!

<table class="tan">
  <tr>
    <th>Eset / Падеж</th>
    <th>Funkció</th>
    <th>Kérdés / Вопрос</th>
  </tr>
  <tr>
    <td><b>Alanyeset</b> / Именительный падеж</td>
    <td>a kifejezés alanyát fejezzük ki</td>
    <td>Ki? Mi? / Кто? Что?</td>
  </tr>
  <tr>
    <td><b>Tárgyeset</b> / Винительный падеж</td>
    <td>a kifejezés tárgyát fejezzük ki</td>
    <td>Kit? Mit? / Кого? Что?</td>
  </tr>
  <tr>
    <td><b>Birtokos eset</b> / Родительный падеж</td>
    <td>a kifejezés birtokviszonyát fejezzük ki</td>
    <td>Kinek a? Minek a? / Кого? Чого?</td>
  </tr>
  <tr>
    <td><b>Részeshatározó eset</b> / Дательный падеж</td>
    <td>az igék részes vonzatait fejezzük ki</td>
    <td>Kinek? Minek? / Кому? Чому?</td>
  </tr>
  <tr>
    <td><b>Eszközhatározó eset</b> / Творительный падеж</td>
    <td>egy cselekvés eszközét, végrehajtóját vagy okozóját fejezzük ki</td>
    <td>Ki által? Mivel? / Кем? Чем?</td>
  </tr>
  <tr>
    <td><b>Elöljárós eset</b> / Предложной падеж</td>
    <td>bizonyos elöljárókkal együtt használjuk</td>
    <td>Kiről? Miről? / О ком? О чём?</td>
  </tr>
</table>

Példák:

- Alanyeset: **Это Алексей.** - Ő Alekszej.
- Tárgyeset: **Анна ждёт Алексея.** - Anna várja Alekszejt.
- Birtokos eset: **Мы изучаем таблицу Алексея.** - Alekszej táblázatát tanulmányozzuk.
- Részeshatározó eset: **Ученик дал Алексею таблицу.** - Egy diák adott egy táblázatot Alekszejnek.
- Eszközhatározó eset: **Таблица написана Алексеем.** - A táblázat Alekszej által lett megírva.
- Elöljárós eset: **Друзья говорят об Алексее.** - A barátok Alekszejről beszélnek.

Az orosz főnevek túlnyomó többsége három ragozási típusba sorolható. Az alábbi vázlatos ragozási segédlet kiinduló pontja nem a ragozási típus, hanem az egyes esetek.

- Az **I. ragozási típushoz** tartoznak a kemény vagy lágy mássalhangzókra végződő hímnemű (стол, класс, автомобиль), és az **-о**, **-е**, **-ё**, **-ие** végződésű semlegesnemű főnevek (**окно́** / **зада́ние**)
- A **II. ragozási típushoz** tartozik az összes, szótári alakban **-a**, **-я** végződéssel rendelkező főnév. Ezek a főnevek többségükben nőneműek, de akadnak köztük hímneműek is (ezek mindig férfit jelentő főnevek: **па́па**, **де́душка**, **мальчи́шка**, **дя́дя**, illetve számtalan **–a**-ra végződő férfi becenév: **Ва́ня**, **Алёша**, **Серёжа**), sőt, ún. közös neműek is, melyeknek nembeli egyeztetése az adott személy valóságos biológiai nemétől függ (**э́тот пья́ница** [ez a részeges férfi] ↔ **э́та пья́ница** [ez a részeges nő]).
- A **III. ragozási típushoz** tartozik a hímnemű путь szó kivételével az összes olyan nőnemű főnév, melyeknek írott szótári alakja lágyságjelre végződik.

Összesítő táblázat csak a végződésekkel

Az orosz nyelvben a tárgyesetben az élőlényt és élettelen dolgot kifejező főnevek végződései eltérnek. Az élettelent kifejezőké az egyes szám nőnem kivételével megegyeznek az alanyeset végződéseivel. Az "**а**"-ra/"**я**"-ra végződő nőnemű főnevek **-у**,/**-ю** végződést kapnak, a lágyságjelre végződők pedig nem változnak tárgyesetben.

A következő táblázat egy egyszerűsített változat mindenféle tekintetben, de az esetek nagy részére nagyon jól alkalmazható:

<table class="tan">
  <tr>
    <th colspan="2" rowspan="3"></th>
    <th colspan="6">I. ragozás</th>
    <th colspan="3">II. ragozás</th>
    <th>III. rag.</th>
  </tr>
  <tr>
    <td colspan="3"><b>hn</b></td>
    <td colspan="3"><b>sn</b></td>
    <td colspan="3"><b>nn</b></td>
    <td><b>nn</b></td>
  </tr>
  <tr>
    <td>msh.</td>
    <td>-й *</td>
    <td>-ь</td>
    <td>-o</td>
    <td>-е</td>
    <td>-ие</td>
    <td>-a</td>
    <td>-я</td>
    <td>-ия</td>
    <td>-ь</td>
  </tr>
  <tr>
    <th rowspan="6">E. sz.</th>
    <td><b>A</b></td>
	<td>-</td><td>-й</td><td>-ь</td><td>-o</td><td>-е</td><td>-ие</td><td>-a</td><td>-я</td><td>-ия</td><td>-ь</td>
  </tr>
  <tr>
	<td><b>T</b></td>
	<td>- -a *</td><td>-й -я *</td><td>-ь -я *</td><td>-o</td><td>-e</td><td>-ие</td><td>-y</td><td>-ю</td><td>-ию</td><td>-ь</td>
  </tr>
  <tr>
	<td><b>B</b></td>
	<td>-a</td><td>-я</td><td>-я</td><td>-a</td><td>-я</td><td>-ия</td><td>-ы</td><td>-и</td><td>-ии</td><td>-и</td>
  </tr>
  <tr>
	<td><b>R</b></td>
	<td>-y</td><td>-ю</td><td>-ю</td><td>-y</td><td>-ю</td><td>-ию</td><td>-e</td><td>-e</td><td>-ии</td><td>-и</td>
  </tr>
  <tr>
	<td><b>Eszk.</b></td>
	<td>-ом</td><td>-eм</td><td>-eм</td><td>-oм</td><td>-eм</td><td>-иeм</td><td>-oй</td><td>-eй</td><td>-иeй</td><td>-ю</td>
  </tr>
  <tr>
	<td><b>El.</b></td>
    <td>-e</td><td>-e</td><td>-e</td><td>-e</td><td>-e</td><td>-ии</td><td>-e</td><td>-e</td><td>-ии</td><td>-и</td>
  </tr>
  <tr>
    <th rowspan="6">T. sz.</th>
    <td><b>A</b></td>
	<td>-ы</td><td>-и</td><td>-и</td><td>-a</td><td>-я</td><td>-ия</td><td>-ы</td><td>-и</td><td>-ии</td><td>-и</td>
  </tr>
  <tr>
	<td><b>T</b></td>
	<td>-ы -oв *</td><td>-и -eв *</td><td>-и -eй *</td><td>-a</td><td>-я</td><td>-ия</td><td>-ы</td><td>-и</td><td>-ии</td><td>-и</td>
  </tr>
  <tr>
	<td><b>B</b></td>
	<td>-oв</td><td>-eв</td><td>-eй</td><td>-</td><td>-eй</td><td>-ий</td><td>-</td><td>-ь</td><td>-ий</td><td>-eй</td>
  </tr>
  <tr>
	<td><b>R</b></td>
	<td>-aм</td><td>-ям</td><td>-ям</td><td>-aм</td><td>-ям</td><td>-иям</td><td>-aм</td><td>-ям</td><td>-иям</td><td>-ям</td>
  </tr>
  <tr>
	<td><b>Eszk.</b></td>
	<td>-aми</td><td>-ями</td><td>-ями</td><td>-aми</td><td>-ями</td><td>-иями</td><td>-aми</td><td>-ями</td><td>-иями</td><td>-ями</td>
  </tr>
  <tr>
	<td><b>El.</b></td>
    <td>-ax</td><td>-яx</td><td>-яx</td><td>-ax</td><td>-яx</td><td>-ияx</td><td>-ax</td><td>-яx</td><td>-ияx</td><td>-яx</td>
  </tr>
</table>

\* Az I. ragozásnál hn-ben az élőlény tárgyesete azonos a birtokos esettel. A többi tárgyesete az alanyesettel egyezik meg.

#### Főnévragozás példákkal

Mivel a fenti táblázat mellett még számos ritkábban alkalmazott szabály, kivétel létezik, és ezek felsorolása az orosz nyelvtannal foglalkozó tankönyvekben is hosszú oldalakon keresztül bonyolódik, így most jöjjön egy olyan táblázat, amelyben az egyes eseteket példákon keresztül figyelhetjük meg.

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td>стол, музе́й, рубль, окно́, зада́ние<br>
	карти́на, неде́ля, тетра́дь<br>
	вре́мя, и́мя<br></td>
    <td>стол<red>ы́</red>, музе́<red>й</red>, рубл<red>и́</red>, о́кн<red>а</red>, зада́ни<red>я</red><br>
	карти́н<red>ы</red>, неде́л<red>ь</red>, тетра́д<red>и</red><br>
	врем<red>ена́</red>, им<red>ена́</red></td>
  </tr>
  <tr>
    <th>T - élettelen</th>
    <td>стол, музе́й, рубль, окно́, зада́ние<br>
	карти́н<red>у</red>, неде́л<red>ю</red>, тетра́дь<br>
	вре́мя</td>
    <td>стол<red>ы́</red>, музе́<red>й</red>, рубл<red>и́</red>, о́кн<red>а</red>, зада́ни<red>я</red><br>
	карти́н<red>ы</red>, неде́л<red>ь</red>, тетра́д<red>и</red><br>
	врем<red>ена́</red></td>
  </tr>
  <tr>
    <th>T - élő</th>
    <td>ма́льчик<red>a</red>, учи́тел<red>я</red>, попуга́<red>я</red>, живо́тн<red>oe</red><br>
	ма́м<red>у</red>, мать<br>
	и́мя</td>
    <td>ма́льчик<red>ов</red>, учител<red>е́й</red>, попуга́<red>ев</red>, живо́тн<red>ых</red><br>
	мам, мат<red>ере́й</red><br>
	им<red>ена́</red></td>
  </tr>
  <tr>
    <th>B</th>
    <td>стол<red>а́</red>, музе́<red>я</red>, рубл<red>я́</red>, окн<red>а</red>́, зада́ни<red>я</red><br>
	карти́н<red>ы</red>, неде́л<red>и</red>, тетра́д<red>и</red><br>
	вре́м<red>ени</red>, и́м<red>ени</red></td>
    <td>стол<red>о́в</red>, музе́<red>ев</red>, рубл<red>е́й</red>, о́кон, зада́ни<red>й</red><br>
	карти́н, неде́л<red>ь</red>, тетра́д<red>ей</red><br>
	врем<red>ён</red>, им<red>ён</red></td>
  </tr>
  <tr>
    <th>R</th>
    <td>стол<red>у́</red>, музе́<red>ю</red>, рубл<red>ю́</red>, окн<red>у́</red>, зада́ни<red>ю</red><br>
	карти́н<red>е</red>, неде́л<red>е</red>, тетра́д<red>и</red><br>
	вре́м<red>ени</red>, и́м<red>ени</red></td>
    <td>стол<red>а́м</red>, музе́<red>ям</red>, рубл<red>я́м</red>, о́кн<red>ам</red>, зада́ни<red>ям</red><br>
	карти́н<red>ам</red>, неде́л<red>ям</red>, тетра́д<red>ям</red><br>
	врем<red>ена́м</red>, им<red>ена́м</red></td>
  </tr>
  <tr>
    <th>Eszk.</th>
    <td>стол<red>о́м</red>, музе́<red>ем</red>, рубл<red>ём</red>, окн<red>о́м</red>, зада́ни<red>ём</red><br>
	карти́н<red>ой</red>, неде́л<red>ей</red>, тетра́дь<red>ю</red><br>
	вре́м<red>енем</red>, и́м<red>енем</red></td>
    <td>стол<red>а́ми</red>, музе́<red>ями</red>, рубл<red>я́ми</red>, о́кн<red>ами</red>, зада́ни<red>ями</red><br>
	карти́н<red>ами</red>, неде́л<red>ями</red>, тетра́д<red>ями</red><br>
	врем<red>ена́ми</red>, им<red>ена́ми</red></td>
  </tr>
  <tr>
    <th>El.</th>
    <td>стол<red>е́</red>, музе́<red>е</red>, рубл<red>е́</red>, окн<red>е́</red>, зада́ни<red>и</red><br>
	картин<red>е</red>, недел<red>е</red>, тетра́д<red>и</red><br>
	вре́м<red>ени</red>, и́м<red>ени</red></td>
    <td>стол<red>а́х</red>, музе́<red>ях</red>, рубл<red>я́х</red>, о́кн<red>ах</red>, зада́н<red>ях</red><br>
	карти́н<red>ах</red>, неде́л<red>ях</red>, тетра́д<red>ях</red><br>
	врем<red>ена́х</red>, им<red>ена́х</red></td>
  </tr>
</table>

<a name="5"></a>
## 5. Melléknév - Прилагательное

Milyen, milyet, milyené, milyennek, milyennel, milyenről
milyenek, milyeneket, milyeneké, milyeneknek, milyenekkel, milyenekről

<table class="tan">
  <tr>
    <td colspan="2"></td>
    <th width="100px">hímnem</th>
    <th width="100px">semleges nem</th>
    <th width="100px">nőnem</th>
    <th width="110px">példa</th>
  </tr>
  <tr>
    <th rowspan ="6">E. sz.</th>
    <th>A</th>
    <td>-ый</td>
    <td>-oe</td>
    <td>-aя</td>
    <td>нов<red>ый</red></td>
  </tr>
  <tr>
    <th>T</th>
    <td>-ый/-ого *</td>
    <td>-oe</td>
    <td>-ую</td>
    <td>нов<red>ый</red>/нов<red>ого</red> *</td>
  </tr>
  <tr>
    <th>B</th>
    <td colspan="2">-ого</td>
    <td>-oй</td>
    <td>нов<red>ого</red></td>
  </tr>
  <tr>
    <th>R</th>
    <td colspan="2">-омy</td>
    <td>-oй</td>
    <td>нов<red>омy</red></td>
  </tr>
  <tr>
    <th>Eszk.</th>
    <td colspan="2">-ым</td>
    <td>-oй</td>
    <td>нов<red>ым</red></td>
  </tr>
  <tr>
    <th>El.</th>
    <td colspan="2">-ом</td>
    <td>-oй</td>
    <td>o нов<red>ом</red></td>
  </tr>
  <tr>
    <th rowspan ="6">T. sz.</th>
    <th>A</th>
    <td colspan="3">-ыe</td>
    <td>нов<red>ыe</red></td>
  </tr>
  <tr>
    <th>T</th>
    <td colspan="3">-ыe/-ыx *</td>
    <td>нов<red>ыe</red>/нов<red>ыx</red> *</td>
  </tr>
  <tr>
    <th>B</th>
    <td colspan="3">-ыx</td>
    <td>нов<red>ыx</red></td>
  </tr>
  <tr>
    <th>R</th>
    <td colspan="3">-ым</td>
    <td>нов<red>ым</red></td>
  </tr>
  <tr>
    <th>Eszk.</th>
    <td colspan="3">-ыми</td>
    <td>нов<red>ыми</red></td>
  </tr>
  <tr>
    <th>El.</th>
    <td colspan="3">-ыx</td>
    <td>o нов<red>ыx</red></td>
  </tr>
</table>

#### Példák melléknevek és főnevek párosításával

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td><b>нов<red>ый</red> друг - új barát</b></td>
    <td>нов<red>ые</red> друзь<red>я</red> - új barátok</td>
  </tr>
  <tr>
    <th>T</th>
    <td>нов<red>ого</red> друг<red>а</red> - új barátot</td>
    <td>нов<red>ых</red> друз<red>ей</red> - új barátokat</td>
  </tr>
  <tr>
    <th>B</th>
    <td>нов<red>ого</red> друг<red>а</red> - új baráté</td>
    <td>нов<red>ых</red> друз<red>ей</red> - új barátoké</td>
  </tr>
  <tr>
    <th>R</th>
    <td>нов<red>ому</red> друг<red>у</red> - új barátnak</td>
    <td>нов<red>ым</red> друзь<red>ям</red> - új barátoknak</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td>нов<red>ым</red> друг<red>ам</red> - új baráttal</td>
    <td>нов<red>ыми</red> друзь<red>ями</red> - új barátokkal</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>о нов<red>ом</red> друг<red>е</red> - új barátról</td>
    <td>о нов<red>ых</red> друзь<red>ях</red> - új barátokról</td>
  </tr>
</table>

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td><b>хорош<red>ий</red> студент	- jó tanuló</b></td>
    <td>хорош<red>ие</red> студент<red>ы</red> - jó tanulók</td>
  </tr>
  <tr>
    <th>T</th>
    <td>хорош<red>его</red> студент<red>а</red> - jó tanulót</td>
    <td>хорош<red>их</red> студент<red>ов</red> - jó tanulókat</td>
  </tr>
  <tr>
    <th>B</th>
    <td>хорош<red>его</red> студент<red>а</red> - jó tanulóé</td>
    <td>хорош<red>их</red> студент<red>ов</red> - jó tanulóké</td>
  </tr>
  <tr>
    <th>R</th>
    <td>хорош<red>ему</red> студент<red>у</red> - jó tanulónak</td>
    <td>хорош<red>им</red> студент<red>ам</red> - jó tanulóknak</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td>хорош<red>им</red> студент<red>ом</red> - jó tanulóval</td>
    <td>хороши<red>ми</red> студент<red>ами</red> - jó tanulókkal</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>о хорош<red>ем</red> студент<red>е</red> - jó tanulóról</td>
    <td>о хорош<red>их</red> студент<red>ах</red> - jó tanulókról</td>
  </tr>
</table>

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td><b>дорог<red>ая</red> книга - kedves könyv</b></td>
    <td>дорог<red>ие</red> книг<red>и</red> - kedves könyvek</td>
  </tr>
  <tr>
    <th>T</th>
    <td>дорог<red>ую</red> книг<red>у</red> - kedves könyvet</td>
    <td>дорог<red>ие</red> книг<red>и</red> - kedves könyveket</td>
  </tr>
  <tr>
    <th>B</th>
    <td>дорог<red>ой</red> книг<red>и</red> - kedves könyvé</td>
    <td>дорог<red>их</red> книг - kedves könyveké</td>
  </tr>
  <tr>
    <th>R</th>
    <td>дорог<red>ой</red> книг<red>е</red> - kedves könyvnek</td>
    <td>дорог<red>им</red> книг<red>ам</red> - kedves könyveknek</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td>дорог<red>ой</red> книг<red>ой</red> - kedves könyvvel</td>
    <td>дорог<red>ими</red> книг<red>ами</red> - kedves könyvekkel</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>о дорог<red>ой</red> книг<red>e</red> - kedves könyvről</td>
    <td>о дорог<red>их</red> книг<red>ах</red> - kedves könyvekről</td>
  </tr>
</table>

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td><b>последн<red>ий</red> день - utolsó nap</b></td>
    <td>последн<red>ие</red> дн<red>и</red> - utolsó napok</td>
  </tr>
  <tr>
    <th>T</th>
    <td>последн<red>ий</red> день * - utolsó napot</td>
    <td>последн<red>ие</red> дн<red>и</red> * - utolsó napokat</td>
  </tr>
  <tr>
    <th>B</th>
    <td>последн<red>его</red> дн<red>я</red> - utolsó napé</td>
    <td>последн<red>их</red> дн<red>ей</red> - utolsó napoké</td>
  </tr>
  <tr>
    <th>R</th>
    <td>последн<red>ему</red> дн<red>ю</red> - utolsó napnak</td>
    <td>последн<red>им</red> дн<red>ям</red> - utolsó napoknak</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td>последн<red>им</red> дн<red>ем</red> - utolsó nappal</td>
    <td>последн<red>ими</red> дн<red>ями</red> - utolsó napokkal</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>о последн<red>ем</red> дн<red>е</red> - utolsó napról</td>
    <td>о последн<red>их</red> дн<red>ех</red> - utolsó napokról</td>
  </tr>
</table>

* élettelen dolog miatt

A **который** vonatkozó névmást is a melléknévhez hasonlóan ragozzuk:

<table class="tan">
  <tr>
    <th></th>
    <th>Egyes szám</th>
    <th>Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td>котор<red>ый</red>, котор<red>ое</red>, котор<red>ая</red> - aki, ami, amelyik</td>
    <td>котор<red>ые</red> - akik, amik, amelyek</td>
  </tr>
  <tr>
    <th>T</th>
    <td>котор<red>ый</red>, котор<red>ое</red>, котор<red>ую</red> - akit, amit, amelyiket</td>
    <td>котор<red>ые</red> - akiket, amiket, amelyeket</td>
  </tr>
  <tr>
    <th>B</th>
    <td>котор<red>ому</red>, котор<red>ой</red> - akié, amié, amelyiké</td>
    <td>котор<red>ых</red> - akiké, amiké, amelyeké</td>
  </tr>
  <tr>
    <th>R</th>
    <td>котор<red>ому</red>, котор<red>ой</red> - akinek, aminek, amelyiknek</td>
    <td>котор<red>ым</red> - akiknek, amiknek, amelyeknek</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td>котор<red>ым</red>, котор<red>ой</red> - akivel, amivel, amelyikkel</td>
    <td>котор<red>ыми</red> - akikkel, amikkel, amelyekkel</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>котор<red>ом</red>, котор<red>ой</red> - akiről, amiről, amelyikről</td>
    <td>котор<red>ых</red> - akikről, amikről, amelyekről</td>
  </tr>
</table>

<a name="6"></a>
## 6. Számnév - Имя числительное

egy, kettő, három, négy, öt, hat, hét, nyolc, kilenc, tíz  
→ **один, два, три, четыре, пять, шесть, семь, восемь, девять, десять**

tizenegy, tizenkettő, tizenhárom, húsz, huszonegy, huszonkettő, harminc  
→ **одиннадцать, двенадцать, тринадцать, двадцать, двадцать один, двадцать два, тридцать**

negyven, ötven, hatvan, hetven, nyolcvan, kilencven, száz  
→ **сорок, пятьдесят, шестьдесят, семьдесят, восемьдесят, девяносто, сто**

százharmincöt, kettőszáznegyvenhárom, ezer, kettőezertizenhét  
→ **сто тридцать пять, Двести сорок три, тысяча, две тысячи семнадцать**

első, második, harmadik, negyedik, ötödik, tizedik, huszonharmadik, ezerötszázadik  
→ **первый, второй, третий, четвертый, пятый, десятый, двадцать третья, одна тысяча пятисотый**

<a name="7"></a>
## 7. Névmás - Местоимение

### 7.1 A személyes névmások

<table class="tan">
  <tr>
    <th></th>
    <th colspan="4">Egyes szám</th>
    <th colspan="3">Többes szám</th>
  </tr>
  <tr>
    <th>A</th>
    <td><b>я</b></td>
    <td><b>ты</b></td>
    <td><b>он, оно</b></td>
    <td><b>она</b></td>
    <td><b>мы</b></td>
    <td><b>вы</b></td>
    <td><b>они</b></td>
  </tr>
  <tr>
    <th>T</th>
    <td><b>меня</b></td>
    <td><b>тебя</b></td>
    <td><b>его</b> (на него)</td>
    <td><b>её</b> (на неё)</td>
    <td><b>нас</b></td>
    <td><b>вас</b></td>
    <td><b>нх</b> (на них)</td>
  </tr>
  <tr>
    <th>B</th>
    <td><b>меня</b></td>
    <td><b>тебя</b></td>
    <td><b>его</b> (у него)</td>
    <td><b>её</b> (у неё)</td>
    <td><b>нас</b></td>
    <td><b>вас</b></td>
    <td><b>нх</b> (у них)</td>
  </tr>
  <tr>
    <th>R</th>
    <td><b>мне</b></td>
    <td><b>тебе</b></td>
    <td><b>ему</b> (к нему)</td>
    <td><b>ей</b> (к ней)</td>
    <td><b>нам</b></td>
    <td><b>вам</b></td>
    <td><b>им</b> (к ним)</td>
  </tr>
  <tr>
    <th>Esz.</th>
    <td><b>мной</b></td>
    <td><b>товой</b></td>
    <td><b>им</b> (с ним)</td>
    <td><b>ей, ею</b> (с ней)</td>
    <td><b>нами</b></td>
    <td><b>вами</b></td>
    <td><b>ими</b> (с ними)</td>
  </tr>
  <tr>
    <th>El.</th>
    <td>(обо) <b>мне</b></td>
    <td>(о) <b>тебе</b></td>
    <td>(о) <b>нём</b></td>
    <td>(о) <b>ней</b></td>
    <td>(о) <b>нас</b></td>
    <td>(о) <b>вас</b></td>
    <td>(о) <b>них</b></td>
  </tr>
</table>


<a name="8"></a>
## 8. Ige - Глагол

Az orosz ige szótári alakja a főnévi igenév, amely tipikusan így néz ki: [előtag][magánhangzó]+ТЬ (чита́ть, говори́ть, знать, понима́ть).
A főnévi igenév ismeretében az esetek többségében "könnyedén" képezhetőek a múlt idejű alakok: a –ть-től megfosztott tőhöz a múlt idő képzője, az л csatlakozik. A múlt idejű alakok tükrözik az alany nyelvtani nemét és számát: hímnem= -Ø nőnem= -а semleges nem= -о többes szám= -и. Tehát: читалØ, читала, читало, читали // говорилØ, говорила, говорило, говорили.

A jelen idejű alakok azonban gyakorlatilag mindig a főnévi igenév, illetve a múlt idő tövétől eltérő más tőből képződnek. Az előbbi tövek utolsó magánhangzójával két dolog történhet:

- eltűnik, kiesik (ez írásban is azonnal látszik)
- a tő kibővül egy mássalhangzóval (leggyakrabban [j] hanggal, ez azonban csak alaposabb vizsgálódás eredményeként olvasható ki az írott alakból).

Ha sikerült megtalálni a jelen idő tövét, ehhez általában 2 fő végződés sor csatlakozhat, az "**е**"-soros és az "**и**"-soros:

<table class="tan">
  <tr>
    <td></td>
    <th width="250px">1. ragozás («е»)</th>
    <th width="250px">2. ragozás («и»)</th>
  </tr>
  <tr>
    <th>я</th>
    <td><b>-у (-ю)</b>: читаю, живу</td>
    <td><b>-у (-ю)</b>: говорю, смотрю</td>
  </tr>
  <tr>
    <th>ты</th>
    <td><b>-ешь /-ёшь</b>: читаешь, живёшь</td>
    <td><b>-ишь</b>: говоришь, смотришь</td>
  </tr>
  <tr>
    <th>он, она, оно</th>
    <td><b>-ет / -ёт</b>: читает, живёт</td>
    <td><b>-ит</b>: говорит, смотрит</td>
  </tr>
  <tr>
    <th>мы</th>
    <td><b>-ем / -ём</b>: читаем, живём</td>
    <td><b>-им</b>: говорим, смотрим</td>
  </tr>
  <tr>
    <th>вы</th>
    <td><b>-ете / -ёте</b>: чита́ете, живёте</td>
    <td><b>-ите</b>: говорите, смотрите</td>
  </tr>
  <tr>
    <th>они</th>
    <td><b>-ут (-ют)</b>: читают, живут</td>
    <td><b>-ат (-ят)</b>: говорят, смотрят</td>
  </tr>
</table>

#### Néhány különleges igetípus ragozása

- kiesik a "**ва**":  
    **вставать** (felkelni) → **встаю**, **встаёшь**, **встаёт**, ... **встают**
    **давать** (adni) → **даю**, **даёшь**, **даёт**, ... **дают**
    **продавать** (eladni) → **продаю**, **продаёшь**, **продаёт**, ... **продают**
- kiesik az "**ова**" vagy "**eвa**":  
    **рисовать** (rajzolni) → **рисую**, **рисуешь**, **рисует**, ... **рисуют**
    **танцeвать** (táncolni) → **танцую**, **танцуешь**, **танцует**, ... **танцуют**
- a "**с**" "**ш**"-re cserélődik, és csak E/1-ben jelenik meg az "**у**", míg a többi "**и**"-soros lesz:  
    **просить** (kérni) → **прошу**, **просишь**, **проcит**, ... **просят**

#### Igék folyamatos és befejezett alakjai

A legalapvetőbb különbség a magyar igékhez képest az az, hogy általában egy igének 2 változata van: **folyamatos** (**несовершенный**) és **befejezett** (**совершенный**) szemléletű alak, amelyek többnyire párt alkotnak. Mindkettő cselekvést vagy történést fejez ki, a jelentésükben sincs túl sok különbség, és az esetek nagy részében elegendő a folyamatos alakkal tisztában lennünk.  
A folyamatos akkor használandó, ha csak a cselekvés tényét szeretnénk hagsúlyozni, hogy éppen folyamatban, vagy többször ismétlődően történik valami, illetve, amikor általánosságot mondunk el. Fontos, hogy konkrét, kézzel fogható eredménye nem születik. Ennek az alaknak lehet múlt, jelen és jövő ideje.  
A befejezett alakú ige a cselekvés egy konkrét szakaszát, a lezárultságát fejezi ki. A folyamat vagy csak egyszer történt meg, vagy történhet akár máskor is, de az éppen elmesélt alkalom biztosan lezárult egy eredménnyel. A született eredmény miatt ennek a típusnak csak múlt és jövő ideje lehet.

Az egyes igepárok egy része könnyedén fordítható magyarra (pl. **есть**: enni; **cъесть**: megenni, elfogyasztani), ám másokat nem hogy le nem fordíthatunk magyarra, de a különbség megértése is nehézkes lehet számunkra.

#### Befejezett múltidő

Néhány példa:

**Я выпип пиво.** - Megittam a sört.  
**Я прочитал книгу.** - Elolvastam a könyvet.  
**Я ходил на почту.** - Voltam a postán (megjártam a postát.)

### 7.2 Mozgást jelentő igék

#### Gyakoribb elöljárók

<table class="tan">
  <tr>
    <th>при~</th>
	<td><img src="mozg_01.gif" alt=""><br>ide-, megjönni</td>
	<td>Он <red>при</red>шёл домой.<br>Каждый год на день рождения ко мне <red>приходят</red> гости.</td>
  </tr>
  <tr>
    <th>у~</th>
	<td><img src="mozg_02.gif" alt=""><br>elmenni (sok időre)</td>
	<td>Он <red>у</red>шёл в кино.<br>Мы <red>у</red>шли из дома и забыли взять зонтик.</td>
  </tr>
  <tr>
    <th>в(о)~</th>
	<td><img src="mozg_03.gif" alt=""><br>bemenni, bejönni (pl. ha behívunk v.kit v.va)</td>
	<td>Он <red>во</red>шёл в комнату.<br><red>В</red>ходите, мы давно вас ждём!</td>
  </tr>
  <tr>
    <th>вы~</th>
	<td><img src="mozg_04.gif" alt=""><br>kimenni</td>
	<td>Он <red>вы</red>шел из комнаты.<br><red>Вы</red>ходите на следующей остановке?</td>
  </tr>
  <tr>
    <th>под~</th>
	<td><img src="mozg_05.gif" alt=""><br>elérni v.mit</td>
	<td>Он <red>под</red>ошёл к дому.</td>
  </tr>
  <tr>
    <th>от~</th>
	<td><img src="mozg_06.gif" alt=""><br>elhagyni v.mit</td>
	<td>Он <red>от</red>ошёл от дома.</td>
  </tr>
  <tr>
    <th>пере~</th>
	<td><img src="mozg_07.gif" alt=""><br>átmenni egyik helyről a másikra</td>
	<td>Он <red>пере</red>ошёл улицу.<br>Здесь можно <red>пере</red>йти улицу?</td>
  </tr>
  <tr>
    <th>про~</th>
	<td><img src="mozg_08.gif" alt=""><br>megtenni v.mennyi utat,<br>
    elmenni v.mi mellet (мимо...),<br>keresztülmenni egy területen</td>
    <td>Мы <red>про</red>бежали 5 километров и очень устали.<br>Он <red>про</red>шёл <b>мимо</b> дома.</td>
  </tr>
  <tr>
    <th>о(б)~</th>
	<td><img src="mozg_09.gif" alt=""><br>körbe menni,<br>egy területet átjárni, körbejárni</td>
	<td>Я <red>об</red>ошёл дом.</td>
  </tr>
  <tr>
    <th>вз~</th>
	<td><img src="mozg_10.gif" alt=""><br>felmenni v.hova<br></td>
	<td>Туристы <red>вз</red>ошли на гору.</td>
  </tr>
  <tr>
    <th>с~</th>
	<td><img src="mozg_11.gif" alt=""><br>lejönni v.honnan<br></td>
	<td>Туристы <red>с</red>ошли с горы.</td>
  </tr>
  <tr>
    <th>по~</th>
	<td><img src="mozg_06.gif" alt=""><br>el-, nekiindulni, elmenni</td>
	<td><red>По</red>ехали на рынок!</td>
  </tr>
</table>

### 7.2. Felszólító mód - 

A felszólító módnak külön igealakja csak egyes, és többes szám 2. személyben van. A többi személyben más alakokkal (körülírással) fejezzük ki a felszólítást.

Képzése: A jelen (egyszerű jövő) idő többes szám 3. személyű alakjának végződését (**-ют**, **-ут**, **-ят**, **-ат**) elhagyjuk, és helyébe **-й**-t, **-и**-t vagy **-ь**-t teszünk.
Az **-авать** végű igéknél, az amúgy kieső **-ва**-t itt megtartjuk: **давать** → **давай**, **давайте**.

### 7.2 Feltételes mód - 

<a name="9"></a>
## 9. Határozószó - Наречие

<a name="10"></a>
## 10. Névelő - Артикль

<a name="11"></a>
## 11. Elöljárószók - Предлоги

<a name="12"></a>
## 12. Kötőszavak, módosítószók és egyéb határozószók - конъюнкции, модификаторы и другие наречия

<a name="13"></a>
## 13. Állítás és tagadás - Высказывание и отрицание

### 13.1 ...

...

### 13.2 Is..is és sem..sem

**Здесь и наша книга и ваша.** - A mi és a ti könyvetek is itt van.  
**Ни у мальчика, ни у меня нет карандаша.** - Sem a kisfiúnak, sem nekem nincs ceruzám. 

...

<a name="14"></a>
## 14. Rövid dialógusok - Короткие диалоги

<a name="15"></a>
## 15. Mesék - Сказки

**Евгений Пермяк**  
**(1902-1982)**

### Как Маша стала болшой

![alt_text](tale-01a.png)

Маленькая Маша очень хотела вырасти. Очень. А как это сделать, она не знала. Всё перепробовала. И в маминых туфлях ходила. И в бабушкином капоте сидела. И причёску, как у тети Кати, делала. И бусы примерила. И часы на руку надевала.  
Ничего не получалось. Только смеялись над ней да подшучивали.  
Один раз как-то Маша вздумала пол подметать. И подмела. Да так хорошо подмела, что даже мама удивилась:  
‒ Машенька! Да неужели ты у нас большая становишься?  
А когда Маша чисто-начисто вымыла посуду да сухо-насухо вытерла её, тогда не только мама, но и отец удивился. Удивился и при всех за столом сказал:  
‒ Мы и не заметили, как у нас Мария выросла. Не только пол метёт, но и посуду моет.  
Теперь все маленькую Машу называют большой. И она себя взрослой чувствует, хотя и ходит в своих крошечных туфельках и в коротеньком платьице. Без причёски. Без бус. Без часов.  
Не они, видно, маленьких большими делают. 

![alt_text](tale-01b.png)

> **Hogyan nőtt meg Mása**  
A kicsi Mása nagyon fel akart nőni. Nagyon. De hogy hogyan csinálja, azt nem tudta. Mindent megpróbált. Anyukájának a cipőjében járkált. Meg a nagymamájának a ruhájában is ült. És olyan frizurát is csinált, amilyen a nagynénjének, Katinak is van. Nyakláncot is hordott. És órát is viselt a kezén.  
Semmi nem jött össze. Csak nevettek és viccelődtek rajta.  
Egyszer aztán Mása azt találta ki, hogy felsöpri a padlót. És fel is mosott. De annyira jól felmosott, hogy még az anyukája is meglepődött:  
‒ Másácskánk! Csak nem felnőtté válsz lassan itt nekünk?  
Aztán amikor Mása szép tisztára mosta el az edényeket, de még nagyon szárazra is törölte őket, akkor már nem csak anyukája, hanem még apja is meglepődött. Meglepődött, majd amikor mindenki körülötte az asztalnál ült, azt mondta:  
‒ Nekünk itt fel sem tűnt, hogy a mi Máriánk hogy megnőtt. Nem csak a padlót mossa fel, de még el is mosogat.  
Most már a kis Mására mindenki nagyként tekint. És most már ő is nagynak érzi magát, pedig a iciri-piciri cipőcskéjében és rövid ruhácskájában jár. Frizura nélkül. Nyaklánc nélkül. Óra nélkül.  
Nem ezek teszik felnőtté az embert. 

#### Торопливый ножик

Строгал Митя палочку, строгал да бросил. Косая палочка получилась. Неровная. Некрасивая.  
‒ Как же это так? ‒ спрашивает Митю отец.  
‒ Ножик плохой,‒ отвечает Митя,‒ косо строгает.  
‒ Да нет,‒ говорит отец,‒ ножик хороший. Он только торопливый. Его нужно терпению выучить.  
‒ А как? ‒ спрашивает Митя.  
‒ А вот так,‒ сказал отец.  
Взял палочку да принялся её строгать потихонечку, полегонечку, осторожно. 
Понял Митя, как нужно ножик терпению учить, и тоже стал строгать потихонечку, полегонечку, осторожно. 
Долго торопливый ножик не хотел слушаться. Торопился: то вкривь, то вкось норовил вильнуть, да не вышло. Заставил его Митя терпеливым быть. 
Хорошо стал строгать ножик. Ровно. Красиво. Послушно. 

![alt_text](tale-02.png)

>**A kapkodó kés**  
Hegyezte Mitya a botot, hegyezte, de aztán eldobta. Kacska bot jött össze. Nem egyenes. Nem szép.  
‒ Hát ezt meg hogy? ‒ kérdezi Mitya apja.  
‒ Rossz a kés,‒ válaszolja Mitya,‒ ferdén vág.  
‒ De nem,‒ mondja apja,‒ a kés az jó. Ő csak kapkodós.   Türelemre kell őt tanítani.  
‒ És hogyan? ‒ kérdezi Mitya.  
‒ Hát így,‒ mondta apja.  
Elvette a botot, de megmutatta neki a hegyezést lassacskán, könnyedecskén, óvatosan. Megértette Mitya, hogy a kést türelemre kell tanítani, és nekiállt faragni lassacskán, könnyedecskén, óvatosan. A kés sokáig nem akart szót fogadni. Sietett: össze-vissza igyekezett gördülni, de nem jött ki. Mitya ráparancsolt, hogy legyen türelmes. Aztán jól kezdett faragni a kés. Egyenesen. Szépen. Szófogadóan. 

#### Первая рыбка

![alt_text](tale-03.png)

Юра жил в большой и дружной семье. Все в этой семье работали. Только один Юра не работал. Ему всего пять лет было.  
Один раз поехала Юрина семья рыбу ловить и уху варить. Много рыбы поймали и всю бабушке отдали. Юра тоже одну рыбку поймал. Ерша. И тоже бабушке отдал. Для ухи. 
Сварила бабушка уху. Вся семья на берегу вокруг котелка уселась и давай уху нахваливать:  
‒ Оттого наша уха вкусна, что Юра большущего ерша поймал. Потому наша уха жирна да навариста, что ершище жирнее сома.  
А Юра хоть и маленький был, а понимал, что взрослые шутят. Велик ли навар от крохотного ёршишки? Но он всё равно радовался. Радовался потому, что в большой семейной ухе была и его маленькая рыбка. 

>**Az első halacska**  
Jura (Gyuri) egy nagy és barátságos családban élt. Ebben a családban mindenki dolgozott. Egyedül csak Jura nem dolgozott. Ő mindössze öt éves volt.  
Egyszer aztán ...
