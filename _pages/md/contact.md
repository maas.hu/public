# 📬 Contact

Several subpages have the embedded Disqus solution to discuss, but every decent, traditional website require a dedicated subpage for contacting, so here it is. 

You can leave any of your observations, comments, ideas regarding any of the topics you found on this site. 

In case, you prefer sending an even more traditional private email, you can do it <a href="mailto:korodimi that_worm gmail that_small_sign com">here</a>. 