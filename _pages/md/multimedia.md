# 🖌️ Computer Graphics and Multimedia

![alt_text](multimedia_-_bg.jpg)

## Audio Editing and Converting

- 🎵 [OGG Vorbis](/multimedia/ogg)

## Video Editing and Converting

- 🎬 [Avidemux / ffmpeg](/multimedia/avidemux)
