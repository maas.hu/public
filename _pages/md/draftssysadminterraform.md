# ![alt text](/_ui/img/icons/terraform_m.svg) Terraform

<div style="text-align: right;"><h7>Posted: 2023-04-28 11:28</h7></div>

###### .

![alt_text](terraform_-_bg.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Workstation Setup](#2)
3. [Getting Started with Terraforming on Azure](#3)
4. [Effectively Managing Terraform Code](#4)
5. [Terraform State](#5)
6. [A Real Life Deployment of an Azure VM from Scratch](#6)
7. [Manipulating Virtual Networks](#7)
8. [Destroying Resources](#8)
9. [Azure Backup and Restore](#9)
10. [Continuous Delivery](#10)
11. [Additional Azure-related Considerations](#11)


<a name="1"></a>
## 1. Introduction

**Terraform** is an open-source **Infrastructure-as-Code** (IaC) software tool created by **HashiCorp**, first released in **28 July 2014**.  

The idea behind Terraform (or "TF") is to store all the necessary information for creating or modifying a server infrastucture in plain text description (code) level in an as simple and human-readable way as possible. The approach that TF represents makes it a **declaritive programming tool**. The servers that we manage with TF are mostly **virtual machines** or **containers** and their additional components like **network** and **storage** are all created in a **cloud environment**, usually, in a public cloud infrastructure. 

The TF code can be considered as a **recipe**, where instead of floor, cocoa powder, sugar etc., the "**ingredients**" are the **resource group**, **virtual network**, **public IP**, **network interface**, **VM** etc. Of course, the **method** for how to combine these ingredients together is also defined. 

TF also manages the **State** of the environment, so it always assumes its actual status and behavior of the resources. 

Some of the most common **advantages** are:

- **idempotency** through state, meaning you can deploy the same code over again and again, and you will get the same result
- automatic **dependencies**
- **version control** with Git
- **reusability**, through modules

The tool's official website, the [registry.terraform.io](https://registry.terraform.io) provides good and well-detailed documentation too with lots of examples. [Here is an example for them for adding a custom disk attachment](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine_data_disk_attachment). 

<div class="info">
This article focuses on <b>Terraform on Azure</b>. The TF provider for AWS can be somewhat similar though but that isn't discussed here at all.
</div>

### 1.1 Steps for Deployment

In theory, without any technical details yet, these are the main steps, at least when working with **Microsoft Azure**: 

1. **Author** our configuration
2. **Initialize** the working directory
3. **Import** existing resources
4. **Create** an execution plan
5. **Execute** our plan

### 1.2 The Terraform Workflow

![alt_text](terraform_-_components.jpg)

From another perspective and involving at least the general technical details, you need to...

1. have a **Cloud Provider** (in our case, it's Microsoft Azure, but it can be even your Private Cloud)
2. have the **Terraform Executable** on our laptop
3. prepare the **Configuration Files** (with **Hashicorp** configuration language, but TF supports **JSON** too):
4. issue `terraform init`:  
  a. TF reads the configuration and downloads any specified **TF Provider(s)** from a **Registry**. Typically, it's the **TF Public Registry**, but any other ones can be used
  b. initializes the Backend which is where the current State of our infrastructure is stored (if it isn't specified otherwise, TF uses a local State File)
5. issue `terraform plan`: TF reads the State File and uses it to determine which changes, if any, need to be made
6. issue `terraform apply`: the TF Executable:  
  a. uses the TF Provider to deploy our infrastructure to the chosen Cloud Provider
  b. updates the State with the latest changes


<a name="2"></a>
## 2. Workstation Setup

Before even starting working with any Terraform code, we need to have the required "**toolkit**" on our desktop computer or laptop: 
- the `terraform` executable: validate, initialize, plan and deploy your infrastructure
- `git` for version control and collaboration
- Azure CLI (`az`) to login to Azure and generate "credentials for Terraform Cloud" (I'm not sure about this part yet.)
- an IDE, preferably Visual Studio Code and extensions for it for working with TF code better

Some lazy engineers use **Chocolatey** to automate the installation of packages on Windows. You may also try it out. 

Getting confirmations for each components mentioned above on a Linux workstation (with custom installation of VSCode): 

```ksh
$ terraform --version
Terraform v1.4.6
on linux_amd64

$ git --version
git version 2.40.0

$ az --version
azure-cli                         2.47.0 *
[...]
core                              2.47.0 *
telemetry                          1.0.8
[...]
Dependencies:
msal                              1.22.0
azure-mgmt-resource               23.0.0
[...]

$ /opt/VSCode/bin/code --version
1.77.3
704ed70d4fd1c6bd6342c436f1ede30d1cff4710
x64
```

Installing the `hashicorp.terraform` extension for VSCode in CLI. This is only **optional but highly recommended**, because having it, VSCode will be able to search for the resources when the Init is done: 

```ksh
$ /opt/VSCode/bin/code --install-extension hashicorp.terraform
[...]
```

It's also always useful when you sometimes use the right button &rarr; Format Document feature in VSCode, so the identations will be fixed and formatted nicely. 


<a name="3"></a>
## 3. Getting Started with Terraforming on Azure

In order to follow only one Cloud Provider's requirements and steps for them, we will use **Microsoft Azure** only in this chapter. 

Let's see an example for specifying an initial TF configuration block. This all can be started by creating a new, empty directory (e.g. `terraform`), then starting editing a new `main.tf` file. 

As a result of this exercise, you need to have a **Storage Account on Azure**.

### 3.1 Log in to Azure Portal webUI

First of all, you need to be able to access your organizations' or your private Azure Portal with your Microsoft account. Here is also the place where you create your resource group manually first. 

### 3.2 Initialize the Working Directory and Author Your TF Configuration

The details to be defined in the code in this initial phase: 

1. The required **TF version** (`required_version`). The below definition for `1.3.8` means it is the required minimum.
2. The required **providers** (`required_providers`) and the version of them one-by-one. We will use `azurerm` here, which is **Hashicorp's official provider for Azure** (so not the Cloud Provider itself, but the internal driver for Terraform to work with Azure). The `~>` in the `version` definition means the last digit can be incremented but nothing else.

```
terraform {
  required_version = ">=1.3.8"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.43.0"
    }
  }
}
```

Once these very basic settings are saved to the file, we already may issue a `terraform init` in that directory, so TF will download the necessary provider, its drivers: 

```ksh
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/azurerm versions matching "~> 3.43.0"...
- Installing hashicorp/azurerm v3.43.0...
- Installed hashicorp/azurerm v3.43.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### 3.3 Declare Your Provider Configuration

The next thing is the **configuration of the provider(s)**. It can be added to the same, main `.tf` file as above, continuing the previous block: 

```
[...]
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
}

[...]
```

The `features` block is mandatory, even if it's empty like above. 

The additional `skip_provider_registration` property ensures TF stops registering the given provider in Azure Resource Manager.

### 3.4 Import the existing Resource Group Into Your TF State

These are the resources you want to exist and what arguments and/or properties you want them to have. It also can be continued in the same `.tf` file. 

The challenge here is dealing with existing resources. In the below example, the Resource Group is already "provisioned" in a Cloud playground. 

This is already the phase when it's highly beneficial if you have the Hashicorp Terraform VSCode extension installed since it shows you hints for any possible options: 

![alt_text](terraform_-_vscode_-_hints.png)

#### 3.4.1 The Resource Block

Let's just set the required properties empty at first: 

```
[...]
}

resource "azurerm_resource_group" "rg" {
  name = ""
  location = ""
}
```

...and look for possible values at **Azure Portal** (maybe you will need to create a new Resource Group, just like I did for this one): 

![alt_text](terraform_-_azure_-_rg_properties.png)

According to the above example, the possible setup for this: 

```
[...]
}

resource "azurerm_resource_group" "rg" {
  name     = "809-13a598e5-deploy-to-azure-using-the-terraform-c"
  location = "South Central US"
}
```

#### 3.4.2 Login with Azure CLI and import RG

Now it's time to **login** with your Azure CLI: 

```ksh
$ az login
A web browser has been opened at https://login.microsoftonline.com/organizations/oauth2/v2.0/authorize. Please continue the login in the web browser. If no web browser is available or if the web browser fails to open, use device code flow with `az login --use-device-code`.
[
  {
    "cloudName": "AzureCloud",
[...]
      "type": "user"
    }
  }
]
```

If it was successful, then you need to **import** the above specified Resource Group, still using the values that you can see on Azure Portal's Properties view: 

```ksh
$ terraform import azurerm_resource_group.rg /subscriptions/0cfe2870-d256-4119-b0a3-16293ac11bdc/resourceGroups/809-13a598e5-deploy-to-azure-using-the-terraform-c
azurerm_resource_group.rg: Importing from ID "/subscriptions/0cfe2870-d256-4119-b0a3-16293ac11bdc/resourceGroups/809-13a598e5-deploy-to-azure-using-the-terraform-c"...
azurerm_resource_group.rg: Import prepared!
  Prepared azurerm_resource_group for import
azurerm_resource_group.rg: Refreshing state... [id=/subscriptions/0cfe2870-d256-4119-b0a3-16293ac11bdc/resourceGroups/809-13a598e5-deploy-to-azure-using-the-terraform-c]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.
```

<div class="warn">
<p>As a result of the above command, a <code>terraform.tfstate</code> file must be generated.</p>
</div>

#### 3.4.3 Possible errors at this point

It's possible that you get an error for the previous command. The following Azure CLI command's output also complains about the same, but there's a solution for that: 

```ksh
$ az ad signed-in-user show
Continuous access evaluation resulted in challenge with result: InteractionRequired and code: LocationConditionEvaluationSatisfied

$ export AZURE_IDENTITY_DISABLE_CP1=1

$ az logout

$ az account clear

$ az login
[...]

$ az ad signed-in-user show
{
  "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#users/$entity",
  "businessPhones": [],
[...]
}
```

However, the `terraform import ...` command still might fail partially with error like this: 

```ksh
$ terraform import azurerm_resource_group.rg /subscriptions/[...]
[...]
azurerm_resource_group.rg: Import prepared!
  Prepared azurerm_resource_group for import
[...]
│ Error: reading resource group: resources.GroupsClient#Get: Failure responding to request: StatusCode=403 -- Original Error: autorest/azure: Service returned an error. Status=403 Code="AuthorizationFailed" Message="The client [...] with object id [...] does not have authorization to perform action 'Microsoft.Resources/subscriptions/resourcegroups/read' over scope '/subscriptions/[...]/resourcegroups/[...]' or the scope is invalid. If access was recently granted, please refresh your credentials."
[...]
```

Notice that the mentioned ID in `scope` in the error differs from your desired ID, specified by you originally. To solve this, you need to switch to the proper subscription, then try it again: 

```ksh
$ az account set --subscription [...]
$
```

### 3.5 Declare the Storage Account Resource

Continue the same file with creating a Storage Account with a unique `name`. 

We don't know the `account_tier` at this phase yet. For their possible values, you should refer to the official documentation on Terraform Registry. Besides that the [documentation](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account) provides an example for this block of code, you can see the valid options: 

- `account_tier` - (Required) Defines the Tier to use for this storage account. Valid options are `Standard` and `Premium`. For `BlockBlobStorage` and `FileStorage` accounts only `Premium` is valid. Changing this forces a new resource to be created.
- `account_replication_type` - (Required) Defines the type of replication to use for this storage account. Valid options are `LRS`, `GRS`, `RAGRS` ("Read-access geo-redundant storage"), `ZRS`, `GZRS` and `RAGZRS`.

```
[...]
}

resource "azurerm_storage_account" "storage" {
  name                     = "strgacc20230517001"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
```

Also, don't forget to use the Format Document feature of VSCode. 

### 3.6 Validate, Plan and Apply Your Configuration

Don't forget to fix the indentations by VSCode's "Format Document" feature. **The whole code** by this phase, including all the above, is still quite simple but there's already something to be applied: 

```
terraform {
  required_version = ">=1.3.8"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.43.0"
    }
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
}

resource "azurerm_resource_group" "rg" {
  name     = "809-13a598e5-deploy-to-azure-using-the-terraform-c"
  location = "South Central US"
}

resource "azurerm_storage_account" "storage" {
  name                     = "strgacc20230517001"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
```

And the **validation** on your terminal: 

```ksh
$ terraform validate
Success! The configuration is valid.
```

#### 3.6.1 The first `terraform plan`

Now that the configuration is valid, you're still logged in to your Cloud Provider, you may issue `terraform plan`. 

Issuing the command with also saving the results to a file (optional): 

```ksh
$ terraform plan -out tfplan
azurerm_resource_group.rg: Refreshing state... [id=/subscriptions/0cfe2870-d256-4119-b0a3-16293ac11bdc/resourceGroups/809-13a598e5-deploy-to-azure-using-the-terraform-c]
[...]
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create
[...]
Terraform will perform the following actions:
[...]
  # azurerm_storage_account.storage will be created
  + resource "azurerm_storage_account" "storage" {
      + access_tier                       = (known after apply)
      + account_kind                      = "StorageV2"
      + account_replication_type          = "LRS"
      + account_tier                      = "Standard"
      + allow_nested_items_to_be_public   = true
      + cross_tenant_replication_enabled  = true
      + default_to_oauth_authentication   = false
      + enable_https_traffic_only         = true
      + id                                = (known after apply)
      + infrastructure_encryption_enabled = false
      + is_hns_enabled                    = false
      + large_file_share_enabled          = (known after apply)
      + location                          = "southcentralus"
      + min_tls_version                   = "TLS1_2"
      + name                              = "strgacc20230517001"
      + nfsv3_enabled                     = false
      + primary_access_key                = (sensitive value)
      + primary_blob_connection_string    = (sensitive value)
      + primary_blob_endpoint             = (known after apply)
      + primary_blob_host                 = (known after apply)
      + primary_connection_string         = (sensitive value)
      + primary_dfs_endpoint              = (known after apply)
      + primary_dfs_host                  = (known after apply)
      + primary_file_endpoint             = (known after apply)
[...]
Plan: 1 to add, 0 to change, 0 to destroy.
[...]
Saved the plan to: tfplan
[...]
To perform exactly these actions, run the following command to apply:
    terraform apply "tfplan"
```

#### 3.6.2 The first `terraform apply`

Once you're satisfied with the `plan`, you may issue your first `terraform apply` command. You can speed it up with using the already saved `tfplan` file, so the full command is `terraform apply tfplan` in this case. 

```ksh
$ terraform apply tfplan
azurerm_storage_account.storage: Creating...
azurerm_storage_account.storage: Still creating... [10s elapsed]
azurerm_storage_account.storage: Still creating... [20s elapsed]
azurerm_storage_account.storage: Still creating... [30s elapsed]
azurerm_storage_account.storage: Creation complete after 32s [id=/subscriptions/0cfe2870-d256-4119-b0a3-16293ac11bdc/resourceGroups/809-13a598e5-deploy-to-azure-using-the-terraform-c/providers/Microsoft.Storage/storageAccounts/strgacc20230517001]
[...]
Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```

Once the deployment was reported as successful, like above, you can go to the Azure Portal and ensure the desired Storage Account was really created with the specified type. 

Additionally, as an optional future consideration, when no actual resource changes are expected, you can just apply with storing the changes in TF State by `terraform apply --refresh-only`.


<a name="4"></a>
## 4. Effectively Managing Terraform Code

In real life TF repositories or directories, you will see multiple files, separated by their purpose. Thanks to TF's own programming scheme, we are not forced to write the same details over and over again for reuse, but we can do it much more dynamically by input and output variables, locals, conditionals and leveraging modules. 

As a bare minimum, these files are created in a new repo: 

- `README.md` for some documentation
- `.gitignore` for ignoring the TF generated `.terraform*` files and directories
- `main.tf` for our main code, for our "module" (see below)
- `variables.tf` for reusable input variables

Optionally, you may have also `outputs.tf` for reusable output variables. 

When you have all these files filled, you can use the `terraform fmt` command (where this `fmt` stands for *"Reformat your configuration in the standard style"*) to get a confirmation from TF which ones of these `.tf` files are **just taken into account**, so they will be actually used during our deployments. 

### 4.1 TF Input Variables

To have your code more redundant, you can define **input variables**  for commonly changed resource arguments. 

Example for using it, using the `var` keyword: 

```
variable "storage_account_replication_type" {
  type    = string
  default = "LRS"
}

resource "azurerm_storage_account" "storageaccount" {
  name    = "stmodeldataprod001"
[...]
  account_replication_type = var.storage_account_replication_type
}
```

As mentioned above already, it's a good practice to keep the `variable` blocks in a separate `variables.tf` file. 

### 4.2 TF `locals`

It works very similarly as variables, but you can declare them within the `locals` block and invoke them with the `local` keyword. 

E.g. this way you can "generate" the same `stmodeldataprod001` Storage Account `name`:

```
locals {
  workload_name = "modeldata"
  environment   = "prod"
  instance      = "001"
}

resource "azurerm_storage_account" "storageaccount" {
  name    = "st{local.workload_name}${local.environment}${local.instance}"
[...]
}
```

### 4.3 TF Output Variables

Output variables allow you to export data from one TF configuration to another. 

E.g.:

```
output "storageaccount_primary_blob_host" {
  value = azurerm_storage_account.storageaccount.primary_blob_host
}
```

As mentioned above already, it's a good practice to keep the `output` blocks in a separate `outputs.tf` file. 

### 4.4 TF Modules

Any collection of TF configuration files in a single directory is a **module**. Even a single `main.tf` file in your new, uninitialized directory is already a module. Modules can drastically decrease unnecessary development work and increase consistency. They can be deployed directly or reused by other modules. They can be referenced to a **local** module (directory) or in a **registry**. 

#### 4.4.1 Local reference to module

E.g., this small block references to another directory as module: 

```
module "storageaccount" {
  source                           = "./storage-account"
  storage_account_replication_type = "GRS"
}
```

#### 4.4.2 Module in a registry

It speeds up consistency even further. E.g.: 

```
module "storageaccount" {
  source                           = "Azure/storage/account"
  storage_account_replication_type = "GRS"
}
```

### 4.5 Conditionals

We can dynamically define parameters for our resources, e.g.:

```
[...]
}

resource "azurerm_storage_account" "securestorage" {
  name                     = var.storage_account_name
  location                 = var.location
  resource_group_name      = var.resource_group_name
  account_tier             = "Standard"
  account_replication_type = var.environment == "Production" ? "GRS" : "LRS"
}
```

In this case, if the value in the `var.environment` happens to be the "Production" one, then the `account_replication_type` is "GRS", otherwise (`else`) it will be "LRS". 

### 4.6 Loops

You can control your series of mostly similar resources by well prepared, mapped variables, and using the `for_each` loop control feature of Terraform.

E.g., you might have this in your `variables.tf`:

```
[...]
variable "location" {
  description = "A map of locations with identifiers for regions"
  type        = map(string)
  default = {
    EU = "West Europe",
    US = "East US 2"
  }
}

variable "vm" {
  description = "Predefinitions for specifying VMs and their network"
  type = map(object({
    prefix        = string
    subnet_id     = string
    address_space = string
    private_ip    = string
    ome_ip        = string
    lxca_ip       = string
  }))
  default = {
    EU = {
      prefix        = "cockpitvirt-eu",
      subnet_id     = "/subscriptions/[...]/resourceGroups/common/providers/Microsoft.Network/virtualNetworks/common-emea-vnet/subnets/sre-emea-vnet",
      address_space = "10.20.30.0/27",
      private_ip    = "10.20.30.10",
      ome_ip        = "10.20.30.11",
      lxca_ip       = "10.20.30.12"
    },
    US = {
      prefix        = "cockpitvirt-us",
      subnet_id     = "/subscriptions/[...]/resourceGroups/common/providers/Microsoft.Network/virtualNetworks/common-us-vnet/subnets/sre-us-vnet",
      address_space = "10.30.30.0/27",
      private_ip    = "10.30.30.10",
      ome_ip        = "10.30.30.11",
      lxca_ip       = "10.30.30.12"
    }
  }
}
[...]
```

And now you can just leverage using the `EU` and `US`, higher level indexing of variables when referencing the variables for actual resource creations, e.g.:

```
resource "azurerm_managed_disk" "data_disk" {
  for_each             = var.vm
  name                 = "${each.value.prefix}_disk_data"
  disk_size_gb         = 1024
  location             = var.location[each.key]
  resource_group_name  = var.generic_name
  storage_account_type = "StandardSSD_LRS"
  create_option        = "Empty"
  tags                 = var.tags
}
```

Notice that you didn't only reference to the nested variable `prefix`, but also to the `location`, indexed with the same `EU` and `US` mapping option.


<a name="5"></a>
## 5. Terraform State

Facilitating team work is always important, so it worth storing not only code in a source controlled repository but also you can keep Terraform's key component, the Terraform State on a remote location. Let's see the basics of TF State first. 

### 5.1 Managing TF State

Let's summarize why the TF State as a feature is useful for us: 

- it **maps** configuration to **"real world" resources**
- it enables **dependency mapping**
- it improves **performance**

#### 5.1.1 Verifying TF State

It's already discussed above that when you successfully imported the Resource Group to your local TF State, then a `terraform.tfstate` file will be generated. Even though it's also in JSON format, there is the `terraform show` TF command to give us a summary based on that file: 

```ksh
$ terraform show
# azurerm_resource_group.rg:
resource "azurerm_resource_group" "rg" {
    id       = "/subscriptions/80ea84e8-afce-4851-928a-9e2219724c69/resourceGroups/810-eddf718c-migrate-terraform-state-to-terraform"
    location = "southcentralus"
    name     = "810-eddf718c-migrate-terraform-state-to-terraform"
    tags     = {}

    timeouts {}
}
```

Note that you can also apply `ignore_changes` for the RG's tags, so the tags are not enforced to be updated, even though the TF State confirms no change on them is needed: 

```
[...]
  lifecycle {
    ignore_changes = [tags]
  }
[...]
```

You can also also restrict ignoring changes on some of the tags only, e.g.:

```
[...]
  lifecycle {
    ignore_changes = [
      tags["CostCenter"]
    ]  
  }
[...]
```

### 5.2 Working with Terraform Cloud

HashiCorp has its own webUI at [app.terraform.io](https://app.terraform.io) called **Terraform Cloud** or **TFC** for everyday use, where we can remotely store our **secrets** (any sensitive data) instead of local plain textfiles, our **TF State** files instead of local `terraform.tfstate` and so on. The registration on TFC site is completely free. 

Configuring a backend on TFC also drastically improves **team collaboration**. 

Basic tasks, associated with TFC: 

1. Import Existing Resources into Terraform State
2. Inspect the Local State File
3. Configure a Terraform Cloud Workspace
4. Migrate State to Terraform Cloud
5. Delete the Local State File


#### 5.2.1 Preparing a Workspace on TFC

Upon registration on TFC, you need to create an **Organization** first (e.g. `jakabworkspace`), then a new **Workspace** under that. 

Choose the **CLI-driven workflow** type, so you will manage it through your CLI commands: 

![alt_text](terraform_-_cloud_-_new_workspace.png)

The name of the Workspace can be e.g. `remotestate`, and then you can already click on **Create Workspace**. 

The site already shows further instructions how to migrate your local State to TFC. 

#### 5.2.2 Moving the TF State to TFC

First you just need to copy the `cloud` creation-related code block to your TF file, e.g.:

```
terraform {
  required_version = ">=1.3.8"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.43.0"
    }
  }
  cloud {
    organization = "jakabworkspace"
    workspaces {
      name = "remotestate"
    }
  }
}
[...]
```

Upon saving the changes, you can issue the `terraform login` command, which will open your browser: 

```ksh
$ terraform login
Terraform will request an API token for app.terraform.io using your browser.
[...]
If login is successful, Terraform will store the token in plain text in
the following file for use by subsequent commands:
    /home/gjakab/.terraform.d/credentials.tfrc.json
[...]
Do you want to proceed?
  Only 'yes' will be accepted to confirm.
[...]
  Enter a value: yes
[...]
```

The process enfoces you to login if not logged in yet, then you need to **Create token** by clicking the button for that. The new TF versions automatically paste the token, so a Welcome text should welcome you in your terminal. 

Once you ensured the State is present on TFC site, you can delete your local State file and its backups if any: 

```ksh
$ rm terraform.tfstate*
```

### 5.3 Terraform State in Azure Storage Container

This is what rather happens when working with Terraform for Azure resources. 

First, you need to have a Storage Account, then a Storage Container inside that Storage Account. After that, you can specify your Backend, preferably in your `backend.tf` file, where you also specify a Key, inside that Storage Container. Once your code is ready, and you issue a new `terraform init`, TF will ask you whether you want to migrate your TF State over to this new Azure resource. 

You will find a real world example for all of this in the next chapter.

### 5.4 Re-importing Resources for TF State

It might happen by various reasons. Here's a list of re-import commands if you need to do it for a RG which has the following, usual types of components/resources: 

- the Resource Group itself [1]
- a Storage Account [2] with a Container [3] for storing the TF state
- a NIC [4], an NSG [5], and association [6] for them
- a VM [7], an extra data disk [8], and associating this extra disk as attachment [9] of the VM
- a Recovery Services Vault [10], a Backup Policy [11] of it, and the list of protected items [12]

And the 12 `terraform import` commands for these:

```ksh
$ terraform import azurerm_resource_group.rg /subscriptions/[...]/resourceGroups/NessusRG

$ terraform import azurerm_storage_account.storage /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Storage/storageAccounts/nessusstgacc001

$ terraform import azurerm_storage_container.container https://nessusstgacc001.blob.core.windows.net/nessuscnt

$ terraform import azurerm_network_interface.prod_nic /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Network/networkInterfaces/nessusprod_nic

$ terraform import azurerm_network_security_group.nsg /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Network/networkSecurityGroups/nessusnsg

$ terraform import azurerm_network_interface_security_group_association.prod_network_security_group "/subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Network/networkInterfaces/nessusprod_nic|/subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Network/networkSecurityGroups/nessusnsg"

$ terraform import azurerm_linux_virtual_machine.prod_vm /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Compute/virtualMachines/nessusprod_vm

$ terraform import azurerm_managed_disk.prod_disk_data /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Compute/disks/nessusprod_disk_data

$ terraform import azurerm_virtual_machine_data_disk_attachment.prod_data_disk_attachment /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.Compute/virtualMachines/nessusprod_vm/dataDisks/nessusprod_disk_data

$ terraform import azurerm_recovery_services_vault.recovery_vault /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.RecoveryServices/vaults/nessus-recovery-vault

$ terraform import azurerm_backup_policy_vm.prod_vm_backup_policy /subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.RecoveryServices/vaults/nessus-recovery-vault/backupPolicies/nessusprod_vm_backup_policy

$ terraform import azurerm_backup_protected_vm.prod_vm_backup_assignment "/subscriptions/[...]/resourceGroups/NessusRG/providers/Microsoft.RecoveryServices/vaults/nessus-recovery-vault/backupFabrics/Azure/protectionContainers/IaasVMContainer;iaasvmcontainerv2;NessusRG;nessusprod_vm/protectedItems/VM;iaasvmcontainerv2;NessusRG;nessusprod_vm"
```

Whenever in doubt, the HashiCorp Terraform documentation always provides an example for importing any kind of resources, e.g. [this is the one](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/backup_protected_vm#import) for `azurerm_backup_protected_vm`.


<a name="6"></a>
## 6. A Real Life Deployment of an Azure VM from Scratch

In this case, the `backend.tf` will be populated **only when** the initial resources are deployed already in order to fix the subscription etc. 

As mentioned in the previous chapter, the Storage Container has key importancy: it stores the Terraform State, so you no longer need to keep locally and to "version control" your <code>terraform.tfstate</code> file through Git. Instead, its contents will be stored also in the Cloud.

We will do it in two sets of steps: 

1. Create the **core resources**, such as: 
    - Resource Group
    - Storage Account and Storage Container under it
2. Create **VM, networking and additional resources** for the VM:
    - Network Interface and the Network Security Group
    - the Virtual Machine itself
    - the OS disk and an additional one for the VM

In this short walthrough, you will have the following files in your TF module (ignore the filesizes; I had two VMs by the end): 

```ksh
$ ls -l
total 28
-rw-r--r-- 1 gjakab users  264 Jul 19 10:49 backend.tf
-rw-r--r-- 1 gjakab users  152 Jul 19 10:49 main.tf
-rw-r--r-- 1 gjakab users 2536 Jul 19 10:49 network.tf
-rw-r--r-- 1 gjakab users  230 Jul 19 10:49 providers.tf
-rw-r--r-- 1 gjakab users 1387 Jul 19 10:49 storage.tf
-rw-r--r-- 1 gjakab users 2346 Jul 19 10:49 variables.tf
-rw-r--r-- 1 gjakab users 3009 Jul 19 10:49 virtual_machine.tf
```

### 6.1 Creating the Core Resources

1. create the **Resource Group** on **Azure Portal** under the proper subscription
2. create the new directory, and create the following files: 
    - `providers.tf`: have it with the *usual* contents, defining `required_version` in a `terraform` block, then the `provider "azurerm"`
    - `variables.tf`: populate the initial main variables
    - `main.tf`: define the desired `azurerm_resource_group` only, and nothing else
    - `backend.tf`: **it must be empty** in the beginning or have everything you predict in advance commented out
3. issue `terraform init`
4. issue an `az login`, then the proper `az account set --subscription <YOUR_AZURE_SUBSCRIPTION_ID>` 
5. issue `terraform import azurerm_resource_group.rg /subscriptions/[...]/resourceGroups/[...]` (see above for details what's needed here)
6. define the `azurerm_storage_account` in a new `storage.tf` using your `variables.tf`, then `terraform validate`, then `terraform plan`, then `terraform apply`
7. check the results on Azure Portal
8. define the `azurerm_storage_container` in `storage.tf` using your `variables.tf`, then `terraform validate`, then `terraform plan`, then `terraform apply`
9. check the results on Azure Portal
10. define all the created resources in `backend.tf` (or uncomment its contents), then re-init it with `terraform init`. Say `yes` to question *"Do you want to copy existing state to the new backend?"*. 
11. do `terraform validate`, then `terraform plan`, then `terraform apply` again if needed, then check the results on Azure Portal

#### 6.1.1 Summary on TF files' contents level

- contents of `providers.tf` until this stage, which will be **permanent** from the very beginning: 

```
terraform {
  required_version = ">=1.3.8"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.20.0"
    }
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
}
```

- contents of `variables.tf` until this stage (with removing sensitive information): 

```
variable "location" {
  description = "The location where resources are created"
  default     = "West Europe"
}

variable "subscription" {
  description = "SRE DevTest"
  default     = "<YOUR_AZURE_SUBSCRIPTION_ID>"
}

variable "resource_group_name" {
  description = "The name of the resource group in which the resources are created"
  default     = "NessusRG"
}

variable "tags" {
  description = "Tags to categorize resources"
  default = {
    Product     = "Plan",
    Environment = "Development",
    CostCenter  = "ResearchAndDevelopment",
    Customer    = "<CUSTOMER_NAME>",
    AppGroup    = "ContinuousIntegrationAndDelivery",
    Team        = "SiteReliabilityEngineering"
  }
}

variable "storage_account" {
  description = "Details of the Storage Account"
  default = {
    name             = "nessusstgacc001",
    tier             = "Standard",
    kind             = "StorageV2",
    replication_type = "LRS"
  }
}

variable "storage_container" {
  description = "Details of the Storage Container"
  default = {
    name        = "nessuscnt",
    access_type = "private",
  }
}
```

- contents of `main.tf`, which will be **permanent** too once ready: 

```
resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.location
  lifecycle {
    ignore_changes = [tags]
  }
}
```

- contents of `storage.tf` until this stage (this is an optional file; the contents could be simply added to `main.tf`, but it's a good practice to keep similar resources together): 

```
resource "azurerm_storage_account" "storage" {
  name                            = var.storage_account.name
  account_tier                    = var.storage_account.tier
  account_kind                    = var.storage_account.kind
  account_replication_type        = var.storage_account.replication_type
  resource_group_name             = var.resource_group_name
  location                        = var.location
  tags                            = var.tags
  allow_nested_items_to_be_public = false
}

resource "azurerm_storage_container" "container" {
  storage_account_name  = var.storage_account.name
  name                  = var.storage_container.name
  container_access_type = var.storage_container.access_type
}
```

**Optional**: if you're making your own network and subnet instead of using an existing one, the relevant blocks also can be added here, to `main.tf`'s end, e.g. (note the tricky part about `address_space` and `address_prefixes`):

```
resource "azurerm_virtual_network" "network" {
  name                = "${var.vm.prefix}_network"
  address_space       = [ "${var.vm.net_addr}" ]
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.vm.prefix}_subnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [ "${var.vm.net_addr}" ]
}
```

- contents of `backend.tf` until this stage (with removing sensitive information; also, **it cannot have dynamic values** (variables or locals)) which will be **permanent** too once ready: 

```
terraform {
  backend "azurerm" {
    storage_account_name = "nessusstgacc001"
    container_name       = "nessuscnt"
    subscription_id      = "<YOUR_AZURE_SUBSCRIPTION_ID>"
    resource_group_name  = "NessusRG"
    key                  = "nessus"
  }
}
```

### 6.2 Create Networking and Additional Resources for the VM

First create two **new, empty** files for:
- storing any networking related resouces: `network.tf`
- storing strictly the directly VM-related resources and associations: `virtual_machine.tf`

#### 6.2.1 Improve `variables.tf`

<div class="warn">
<p>I've removed <b>any sensitive or half-sensitive</b> information here and in next sub-chapters, like company name and Azure subscription ID but you need to know them in advance. Also, you need to take notes on the used <b>network and static IP</b> to be reserved.</p>
</div>

Extend the existing `variables.tf` by necessary details for VM and Network Security Group creations: 

```
[...]
variable "vm_tags" {
  type        = map(string)
  description = "These tags are specific for Virtual Machines."
  default = {
    updatewindow = "W0500"
  }
}

variable "vm" {
  description = "Predefinitions for specifying Network, Subnet, NICs and VMs"
  default = {
    prefix     = "nessus"
    subnet_id  = "/subscriptions/<AN_AZURE_SUBSCRIPTION_ID>/resourceGroups/rg-misc-projects-common/providers/Microsoft.Network/virtualNetworks/vnet-misc-projects-common/subnets/sn-misc-projects-common-1"
    net_addr   = "10.224.26.8/29"
  }
}

variable "jumphost_ip" {
  type    = list(string)
  default = ["10.30.30.13/32", "10.30.40.13/32"]
}

variable "<COMPANY_NAME>_vpn_cidrs" {
  type    = list(string)
  default = ["172.30.16.0/23", "172.30.20.0/23", "172.30.22.0/23"]
}

variable "all_<COMPANY_NAME>_dc_cidrs" {
  type    = list(string)
  default = ["10.20.0.0/16", "10.21.0.0/16", "10.22.0.0/16", "10.23.0.0/16"]
}

variable "ports_tcp_ssh" {
  type    = string
  default = "22"
}

variable "ports_tcp_web" {
  type    = list(string)
  default = [80, 443]
}
```

Issue `terraform validate` when your changes are done. 

#### 6.2.2 Fill the `network.tf`

This TF file defines: 
- the virtual **network card** resource (NIC)
- the **Network Security Group** (NSG)
- the **association** between the NIC and the NSG

```
# The common NSG
resource "azurerm_network_security_group" "nsg" {
  name                = "${var.vm.prefix}nsg"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_address_prefixes    = concat(var.jumphost_ip, var.<COMPANY_NAME>_vpn_cidrs, var.all_<COMPANY_NAME>_dc_cidrs)
    source_port_range          = "*"
    destination_address_prefix = var.vm.net_addr
    destination_port_range     = var.ports_tcp_ssh
  }

  security_rule {
    name                       = "WEB"
    priority                   = 1011
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_address_prefixes    = var.<COMPANY_NAME>_vpn_cidrs
    source_port_range          = "*"
    destination_address_prefix = var.vm.net_addr
    destination_port_ranges    = var.ports_tcp_web
  }

}

# The NICs and their associations

resource "azurerm_network_interface" "dev_nic" {
  name                = "${var.vm.prefix}dev_nic"
  location            = var.location
  resource_group_name = var.resource_group_name
  ip_configuration {
    name                          = "${var.vm.prefix}dev_nic_config"
    subnet_id                     = var.vm.subnet_id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.224.26.12"
  }
  tags = var.tags
}

resource "azurerm_network_interface_security_group_association" "dev_network_security_group" {
  network_interface_id      = azurerm_network_interface.dev_nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
```

#### 6.2.3 Adjust the `storage.tf`

Improve the existing file by adding an extra (optional, but let's have it for the example) disk for our future VM: 

```
[...]
resource "azurerm_managed_disk" "dev_disk_data" {
  name                 = "${var.vm.prefix}dev_disk_data"
  disk_size_gb         = 64
  location             = var.location
  resource_group_name  = var.resource_group_name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  tags                 = var.tags
}
```

#### 6.2.4 Fill the `virtual_machines.tf`

Finally, fill up all the necessary details for this file for creating the prerequisites, then activating our VM. As you can see, only the environment's name (used as the object's name in Azure) and the private IP are the static values here. 

This TF file defines: 
- the **Virtual Machine** (VM) resource, including
  - the association to the NIC
  - the VM's size (refer to Azure's complete offering for more options vs prices)
  - an initial admin user and its SSH key
  - the source image type and version
  - the type and size of the OS disk
- the **association** between the additional disk and the VM

```
# The Dev VM and its associations

resource "azurerm_linux_virtual_machine" "dev_vm" {
  name                = "${var.vm.prefix}dev_vm"
  computer_name       = "${var.vm.prefix}dev"
  location            = var.location
  resource_group_name = var.resource_group_name
  network_interface_ids = [
    azurerm_network_interface.dev_nic.id,
  ]
  size           = "Standard_B2s"
  admin_username = "<A_USERNAME>"
  tags           = merge(var.tags, var.vm_tags)

  admin_ssh_key {
    username   = "<A_USERNAME>"
    public_key = "ssh-rsa <LONG_SSH_KEY_HERE>"
  }

  source_image_reference {
    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "8-LVM"
    version   = "latest"
  }

  os_disk {
    name                 = "${var.vm.prefix}dev_disk_os"
    disk_size_gb         = 64
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}

resource "azurerm_virtual_machine_data_disk_attachment" "dev_data_disk_attachment" {
  managed_disk_id    = azurerm_managed_disk.dev_disk_data.id
  virtual_machine_id = azurerm_linux_virtual_machine.dev_vm.id
  lun                = "02"
  caching            = "ReadWrite"
}
```

Issue `terraform validate` when your changes in the TF code are done: 

```ksh
$ terraform validate
Success! The configuration is valid.
```

If it's successful, you can finally run your `terraform plan -out tfplan` and `terraform apply tfplan` again. 

Keep an eye **on the progress, then on the results** on Azure, then enjoy the results: 

![alt_text](terraform_-_azure_-_vm.png)

Of course, if creating more VMs is needed, you just need to improve `network.tf`, `storage.tf` and `virtual_machines.tf` respectively.


<a name="7"></a>
## 7. Manipulating Virtual Networks

Sometimes it's better to keep VNets under one or more Resource Groups, dedicated only to VNets, their Subnets and VNet Peerings. This chapter provides some example for them.

It's practical to keep these definitions in `network.tf` in our code. If the RG already exists, you may need to import it to TF State by `terraform import azurerm_resource_group.[...]` first. 

### 7.1 Virtual Network with Multiple Subnets

Example for defining it: 

```
locals {
  common_address_space = "10.20.30.0/24"
  [...]
}

resource "azurerm_virtual_network" "vnet" {
  name                = "vnet-misc-projects-common"
  address_space       = [local.common_address_space]
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

resource "azurerm_subnet" "subnet" {
  count                = 32
  address_prefixes     = [cidrsubnet(local.common_address_space, 5, count.index)]
  name                 = "sn-misc-projects-common-${count.index}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
}
```

Importing them to TF State if they already exist, e.g.: 

- importing the `azurerm_virtual_network`: 

```ksh
$ terraform import azurerm_virtual_network.vnet /subscriptions/abcd1234-aaaa-bbbb-cccc-abcdef123456/resourceGroups/rg-misc-projects-common/providers/Microsoft.Network/virtualNetworks/vnet-misc-projects-common
[...]
Import successful!
[...]
```

- importing all the `azurerm_subnet`s from the above example code: 

```ksh
$ for i in {0..31} ; do echo $i ; terraform import azurerm_subnet.subnet[${i}] "/subscriptions/abcd1234-aaaa-bbbb-cccc-abcdef123456/resourceGroups/rg-misc-projects-common/providers/Microsoft.Network/virtualNetworks/vnet-misc-projects-common/subnets/sn-misc-projects-common-${i}" ; echo ----- ; done
[...]
azurerm_subnet.subnet[31]: Import prepared!
[...]
Import successful!
[...]
```

### 7.2 Security Group Association to a Specific Subnet

```
locals {
  [...]
  sredevtestprefix     = "/subscriptions/${var.subscription}/resourceGroups/"
  [...]
  vnetsuffix           = "/providers/Microsoft.Network/virtualNetworks/"
}

[...]

resource "azurerm_subnet_network_security_group_association" "sn-misc-projects-common-0" {
  subnet_id                 = "${local.sredevtestprefix}${var.resource_group_name}${local.vnetsuffix}${azurerm_virtual_network.vnet.name}/subnets/sn-misc-projects-common-0"
  network_security_group_id = "${local.sredevtestprefix}backup-internals/providers/Microsoft.Network/networkSecurityGroups/backup-internals-sg"
}
```

Importing this resource to TF State if this resource already exists, e.g.:

```ksh
$ terraform import azurerm_subnet_network_security_group_association.sn-misc-projects-common-0 "/subscriptions/abcd1234-aaaa-bbbb-cccc-abcdef123456/resourceGroups/rg-misc-projects-common/providers/Microsoft.Network/virtualNetworks/vnet-misc-projects-common/subnets/sn-misc-projects-common-0"
[...]
Import successful!
[...]
```

### 7.3 Network Peering

Example in the code (with relevant `locals` definitions): 

```
locals {
  [...]
  sreprodprefix        = "/subscriptions/6ffde7cb-112e-414c-a1b7-4ae3db31dfb3/resourceGroups/"
  [...]
  vnetsuffix           = "/providers/Microsoft.Network/virtualNetworks/"
}

[...]

resource "azurerm_virtual_network_peering" "peering_to_idm_eu_network" {
  name                         = "${azurerm_virtual_network.vnet.name}-to-idm_eu_network"
  resource_group_name          = var.resource_group_name
  virtual_network_name         = azurerm_virtual_network.vnet.name
  remote_virtual_network_id    = "${local.sreprodprefix}idm${local.vnetsuffix}idm_eu_network"
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
}
```

Then if it exists, you need to import it to TF State, e.g.: 

```ksh
$ terraform import azurerm_virtual_network_peering.peering_to_idm_us_network "/subscriptions/abcd1234-aaaa-bbbb-cccc-abcdef123456/resourceGroups/rg-misc-projects-common/providers/Microsoft.Network/virtualNetworks/vnet-misc-projects-common/virtualNetworkPeerings/vnet-misc-projects-common-to-idm_us_network"
[...]
Import successful!
[...]
```


<a name="8"></a>
## 8. Destroying Resources

The most simple way for destroying any resources is just removing their TF code, then issue a `terraform apply`. However, we might want to keep their definition in the code for further potential re-usage. There are at least three possible ways to do it: 

- commenting the resources and associations in TF code out (it's probably a very lame solution but works)
- using the `count = 0` feature, then also add indexed names for these resources (a bit more sophisticated one)
- keep everything as is in the code, then `terraform apply -destroy -target=...`, so it's a matter of storing the TF State

### 8.1 Using the `count` Feature for Destroy

Here is a quick example for changing the above "real life" example to support indexed resources for not only destroying, simply by `count = 0`, but also possibilities to extend.

In `variable.tf`:

```
[...]
variable "vm" {
  description = "Predefinitions for specifying Network, Subnet, NICs and VMs"
  default = {
    [...]
    dev_count = 0
  }
}
[...]
```

In `network.tf`:

```
[...]
resource "azurerm_network_interface" "dev_nic" {
  name                = "${var.vm.prefix}dev_nic"
  location            = var.location
  [...]
  tags  = var.tags
  count = var.vm.dev_count
}
[...]
resource "azurerm_network_interface_security_group_association" "dev_network_security_group" {
  network_interface_id      = azurerm_network_interface.dev_nic[count.index].id
  network_security_group_id = azurerm_network_security_group.nsg.id
  count                     = var.vm.dev_count
}
[...]
```

In `storage.tf`: 

```
[...]
resource "azurerm_managed_disk" "dev_disk_data" {
  [...]
  tags                 = var.tags
  count                = var.vm.dev_count
}
[...]
```

In `virtual_machines.tf`:

```
[...]
resource "azurerm_linux_virtual_machine" "dev_vm" {
  [...]
  network_interface_ids = [
    azurerm_network_interface.dev_nic[count.index].id,
  ]
  [...]
  tags           = merge(var.tags, var.vm_tags)
  count          = var.vm.dev_count
[...]
resource "azurerm_virtual_machine_data_disk_attachment" "dev_data_disk_attachment" {
  [...]
  count              = var.vm.dev_count
}
[...]
```

Notice you had to use `[count.index]` each time you referenced to the NIC.


<a name="9"></a>
## 9. Azure Backup and Restore

There are multiple possibilities to establish Backup on Azure. Here are three examples which we will go through with some quick examples: 

- Azure PostgreSQL Flexible Server Backup
- Azure Backup for the whole VM
- Azure Backup Protected File Share

### 9.1 Azure PostgreSQL Flexible Server Backup

If you have e.g. a PostgreSQL Flex DB resource, then the only thing you need to define in TF code is its built-in backup offering, e.g.:

```
resource "azurerm_postgresql_flexible_server" "app_prod_flx" {
  name                         = "app-prod-flx"
[...]
  backup_retention_days        = 7
  geo_redundant_backup_enabled = true
[...]
}
```

### 9.2 Azure Backup for the whole VM

Assuming all the other resources like the Resource Group and VM are defined already, here is an example for defining it. The required resources to be created: 

1. a Recovery Services vault (`azurerm_recovery_services_vault`)
2. a Backup Policy for the VM (`azurerm_backup_policy_vm`), including backup frequency, schedule and retention
3. assignment(s) by VM (`azurerm_backup_policy_vm`)

```
resource "azurerm_recovery_services_vault" "recovery_vault" {
  name                = "${var.vm.prefix}-recovery-vault"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = "Standard"
  tags                = var.tags
}

resource "azurerm_backup_policy_vm" "prod_vm_backup_policy" {
  name                = "${var.vm.prefix}prod_vm_backup_policy"
  resource_group_name = var.resource_group_name
  recovery_vault_name = azurerm_recovery_services_vault.recovery_vault.name

  backup {
    frequency = "Weekly"
    time      = "02:00"
    weekdays  = ["Sunday"]
  }

  retention_weekly {
    weekdays = ["Sunday"]
    count    = "2"
  }
}

resource "azurerm_backup_protected_vm" "prod_vm_backup_assignment" {
  resource_group_name = var.resource_group_name
  recovery_vault_name = azurerm_recovery_services_vault.recovery_vault.name
  source_vm_id        = azurerm_linux_virtual_machine.prod_vm.id
  backup_policy_id    = azurerm_backup_policy_vm.prod_vm_backup_policy.id
}
```

You can even specify multiple retentions. In this below case, it keeps 7 pieces of daily backups and 4 pieces of weekly ones: 

```
[...]
  retention_daily {
    count = 7
  }

  retention_weekly {
    count    = 4
    weekdays = ["Sunday"]
  }
[...]
```

Prior the first backup, an automatic RG per region for recovery points will be created, where you can browse and check the backups. 

Example for contents of the automatic **RG per region**, for storing Restore Points:

![alt_text](terraform_-_azure_-_backup_-_rp_-_01.png)

Example for contents under a **Restore Point**: 

![alt_text](terraform_-_azure_-_backup_-_rp_-_02.png)

More on this can be read in [Hashicorp's documentation](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/data_protection_backup_vault).

### 9.3 Azure Backup Protected File Share

...

More on this can be read in [Hashicorp's documentation](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/backup_protected_file_share).




<a name="10"></a>
## 10. Continuous Delivery

For working within a team truly with an **Infrastucture as Code mindset**, you need to integrate your TF codes with **Source Control**. This article is not about Git or GitHub or GitLab in general, but overall, the first steps for it are to initialize your local folder with `git` commands, then you `git push` your initial version to the Source Control provider, to either to GitHub or GitLab server. 

When creating a Git repository, make sure you create it with a `.gitignore` file to **ignore tracking** any `.terraform*` and similar, TF-generated files and directories that are generated by TF during initialization and further activities. Even GitHub and GitLab offers a **template** for creating it in a second. Usually, it contains these: 

```
.terraform/
*.tfstate
*.terraform
*.tfstate.*
*.lock.*
```

Depending on your favored Source Control provider, even a set of automatic actions can be configured, and that makes the whole thing **Continuous Delivery** (CD). It requires several configurations and preparations, but after all, the results make the development convenient. In GitHub, the required configuration is under **Actions**, while it's a fully integrated in GitLab called **CI/CD**. 

If you chose to use TFC, then you will need to create another Workflow type, the **API-driven workflow**. 

From "helicopter view", these are the required steps for **CD integration**: 

1. Set up your environment
2. Create a Git repository
3. Configure a Workspace in TFC
4. Configure CD
5. Configure branch protection
6. Author the configuration
7. Create and complete a pull request
8. Review deployment results

#### 10.3.1 A Possible Workflow for CD

A possible workflow for IaC development, integrated with CD, to describe it in a bit more specific way: 

1. clone the remote repository's default/protected `main` (or sometimes `master`) branch to your local computer --> nothing is deployed at this point
2. create a separate branch to keep `main` untouched --> still nothing is happening
3. `terraform init` for preparing the providers and so on, then make your changes/improvements locally, test it with `terraform plan`
4. `git commit ...`, then `git push` your changes over to the server side --> if it's configured properly, it already **triggers a pipeline** for verifying the syntax, even issuing a `terraform plan` to confirm what would change
5. create a **Merge Request** for mergint your custom branch to the protected `main`, then you ask your colleagues for approval
6. upon merging, it **triggers another pipeline** that automatically does the `terraform apply` for actually changing the environment

#### 10.3.2 A Possible Pipeline Configuration

Here is a possible example `.gitlab-ci.yml` file for GitLab CI/CD, which does nothing but initializes and validates our TF code some specific circumstances, e.g. when it's merged into `main`: 

```
default:
  image: registry.example.com/system-management/common-cicd-config-private-cloud/main/k8s-ci-image

stages:
  - lint
  - format-check
  - scan

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
    - if: $CI_COMMIT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH == "main"
      when: always
    - when: never

validate:
  stage: lint
  script:
    - cd modules/
    - terraform init -input=false
    - terraform validate
  tags:
    - sre-runner
  interruptible: true

terraform-format-check:
  stage: format-check
  script:
    - terraform fmt -check -recursive --diff
  tags:
    - sre-runner
  interruptible: true

tfsec-scan:
  stage: scan
  script:
    - tfsec modules
  tags:
    - sre-runner
  interruptible: true
```


<a name="11"></a>
## 11. Additional Azure-related Considerations

As the whole article focuses on managing Azure resources through Terraform, it's important to have some extra notes especially about Azure-focused considerations. 

### 11.1 Issues with Picking a Static Private IP

You might have issues with the static private IP you figured out for the VM because Azure reserves the first 3 IPs. You can ensure what's allowed when you start creating the NIC manually (without finishing it on webUI). This is how e.g. this `...12` IP can be figured out: 

![alt_text](terraform_-_azure_-_create_nic.png)

### 11.2 Network Security Group Rules Overview

If you want to know all the effective Network Security Group rules, assigned to your NIC in the RG: 

```ksh
$ az network nic list-effective-nsg --name nessusdev_nic --resource-group NessusRG
{
  "value": [
    {
      "association": {
        "networkInterface": {
[...]
```

### 11.3 Provisioned vs. Billed Disk Size

It doesn't really worth configuring a 10 GiB sized disk like above because M$ charges you based on pre-defined sizes anyway, so the 10 GiB disk is paid as a 32 GiB one: 

![alt_text](terraform_-_azure_-_disk_size.png)

### 11.4 Using "Bring-Your-Own-Subscription" (BYOS) Images

In the previous examples above, we used the standard RHEL images, offered by MS, but that already includes RH Subscription. If you have your own, it's better not to double-pay for the same thing, so you should consider using BYOS images. 

First of all, it requires an Agreement, also as a resource, e.g.: 

```
resource "azurerm_marketplace_agreement" "RedHat" {
  publisher = "redhat"
  offer     = "rhel-byos"
  plan      = "rhel-lvm88"
}
```

If it already exists under that specific Azure Subscription, you need to import it first. E.g.:

```ksh
$ terraform import azurerm_marketplace_agreement.RedHat "/subscriptions/abcd1234-aaaa-bbbb-cccc-abcdef123456/providers/Microsoft.MarketplaceOrdering/agreements/redhat/offers/rhel-byos/plans/rhel-lvm88"
```

Then use the proper Source Image References, e.g.:

```
  source_image_reference {
    publisher = "RedHat"
    offer     = "rhel-byos"
    sku       = "rhel-lvm88"
    version   = "latest"
  }

  plan {
    publisher = "redhat"
    product   = "rhel-byos"
    name      = "rhel-lvm88"
  }
```

Getting the list of any RHEL images:

```ksh
> az vm image list --publisher RedHat --all --output table
Architecture    Offer                     Publisher       Sku                        Urn                                                                    Version
--------------  ------------------------  --------------  -------------------------  ---------------------------------------------------------------------  -----------------
x64             rh-ansible-self-managed   redhat-limited  rh-aap2                    redhat-limited:rh-ansible-self-managed:rh-aap2:9.0.20231213            9.0.20231213
[...]
x64             rh_rhel_8_main_1          RedHat          rhel_8_main_a1_gen1        RedHat:rh_rhel_8_main_1:rhel_8_main_a1_gen1:8.4.2021110400             8.4.2021110400
x64             test-rhel-byos            RedHat          another-test-plan          RedHat:test-rhel-byos:another-test-plan:9.2.2023060509                 9.2.2023060509
x64             test-rhel-byos            RedHat          test-private               RedHat:test-rhel-byos:test-private:9.2.2023060509                      9.2.2023060509
x64             test-rhel-byos            RedHat          test-private-plan          RedHat:test-rhel-byos:test-private-plan:9.2.2023060509                 9.2.2023060509
```

Filtering for LVM8-type, BYOS images only:

```ksh
> az vm image list --output table --all --publisher "RedHat" --offer "byos" --sku rhel-lvm8
Architecture    Offer      Publisher    Sku               Urn                                                Version
--------------  ---------  -----------  ----------------  -------------------------------------------------  ---------------
x64             rhel-byos  RedHat       rhel-lvm8         RedHat:rhel-byos:rhel-lvm8:8.0.20200402            8.0.20200402
[...]
x64             rhel-byos  RedHat       rhel-lvm89        RedHat:rhel-byos:rhel-lvm89:8.9.2023122211         8.9.2023122211
x64             rhel-byos  RedHat       rhel-lvm89-gen2   RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023112310    8.9.2023112310
x64             rhel-byos  RedHat       rhel-lvm89-gen2   RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023122211    8.9.2023122211
```

You need to accept the terms by an `az` command with this syntax: 

```
az vm image accept-terms [--offer]
                         [--plan]
                         [--publisher]
                         [--urn publisher:offer:sku:version]
```

It's best to simply use the above listed `Urn` format to specify the offer, publisher, SKU and version, e.g.:

```ksh
> az vm image accept-terms --urn RedHat:rhel-byos:rhel-lvm89-gen2:8.9.2023122211
This command has been deprecated and will be removed in version '3.0.0'. Use 'az vm image terms accept' instead.
{
  "accepted": true,
  "id": "/subscriptions/[...]
[...]
}
```

Upon accepting the terms, the next `terraform apply` will warn you that the resource already exists since the `az vm image accept-terms` command created it. You need to import it:

```ksh
> terraform apply
[...]
│ Error: A resource with the ID "/subscriptions/[...]/providers/Microsoft.MarketplaceOrdering/agreements/redhat/offers/rhel-byos/plans/rhel-lvm89-gen2" already exists - to be managed via Terraform this resource needs to be imported into the State. Please see the resource documentation for "azurerm_marketplace_agreement" for more information.
│ 
│   with azurerm_marketplace_agreement.RedHat,
│   on virtual_machines.tf line 1, in resource "azurerm_marketplace_agreement" "RedHat":
│    1: resource "azurerm_marketplace_agreement" "RedHat" {
[...]

> terraform import azurerm_marketplace_agreement.RedHat /subscriptions/[...]/providers/Microsoft.MarketplaceOrdering/agreements/redhat/offers/rhel-byos/plans/rhel-lvm89-gen2

> terraform apply
[...]
```

It's still possible that the Azure Marketplace Extension still has restricted list of images for "redhat.rhel-byos". In case, you need to contact with your organization's cloud engineering to extend it.

You can read more about possible RHEL images [here](https://learn.microsoft.com/en-us/azure/virtual-machines/workloads/redhat/redhat-imagelist) and about the `az vm image accept-terms` command [here](https://learn.microsoft.com/en-us/cli/azure/vm/image?view=azure-cli-latest#az-vm-image-accept-terms). 

### 11.5 Locked Container

It's possible that the last `terraform apply` got terminated which caused locking the Storage Account's Container resource. In this case, TF shows this error when next time you try to issue either `terraform plan` or `... apply`: 

```ksh
$ terraform plan
╷
│ Error: Error acquiring the state lock
│ 
│ Error message: state blob is already locked
│ Lock Info:
│   ID:        03fdc4e0-1374-1234-432e-a926b955e570
│   Path:      nessuscnt/nessus
│   Operation: OperationTypeApply
│   Who:       gjakab@mylaptop
│   Version:   1.5.2
│   Created:   2023-07-10 14:16:30.320864265 +0000 UTC
│   Info:      
│ 
│ 
│ Terraform acquires a state lock to protect the state from being written
│ by multiple users at the same time. Please resolve the issue above and try
│ again. For most commands, you can disable locking with the "-lock=false"
│ flag, but this is not recommended.
```

The explanation is quite clear: the previous operation was never finished, so Azure tries to avoid any data loss by locking the access by multiple users. You can notice this locking on Azure too if you browse for that Container resource under the Storage Account: 

![alt_text](terraform_-_azure_-_locked_cnt.png)

You can try using the `-lock=false` parameter, but that doesn't solve this problem. You need to break the lease of this object by **Break lease**: 

![alt_text](terraform_-_azure_-_unlocking_cnt.png)

After that, TF commands will work as normal. 
