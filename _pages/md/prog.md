# 👨‍💻 Programming

![alt_text](prog_-_bg.jpg)

## Programming Languages

- ![alt text](/_ui/img/icons/bash_s.svg) [Bash, KSH](/prog/bash)
- ![alt text](/_ui/img/icons/python_s.svg) [Python](/prog/python)

## Programming Tools

<div class="info">
<p>Later...</p>
</div>
