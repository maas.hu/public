# 🛠️ Webes segédeszközök

###### .

1. [Keresőmotorok](#1)
2. [Fórumok](#2)


<a name="1"></a>
## 1. Keresőmotorok

Amennyiben egy több oldalból álló weboldallal rendelkezel, és szeretnéd az oldal látogatójának dolgát a tartalmak közötti keresés lehetőségével jelentősen megkönnyíteni (nem mellesleg, ezzel az oldalad fejlettségi szintjén jelentősen feldobni), elég sok lehetőséged van. Itt most csak azokat sorolom fel, amikkel már volt valamilyen dolgom valaha is: 

- **a tartalomkezelőd saját search engine-je**: ha már eleve egy tartalomkezelő rendszerre alapul az egész weboldalad (pl. Drupal, WordPress), akkor annak a dokumentációja segítséget nyújt a keresési funkció kialakításához. 
- **TipueSearch**: JQuery-t használó, szupergyors kereső, amit teljesen statikus weboldalaknál is használhatsz, ám ennek a rendszernek az index állományát fáradtságos munkával, magadnak kell szerkesztgetned.  
Ezt [itt](https://www.jqueryscript.net/demo/jQuery-Site-Search-Engine-Plugin-Tipue-Search/demos/static/) próbálhatod ki, valamint töltheted le. 
- **DuckDuckGo Search Box**: a sokak szerint nagyon biztonságos DuckDuckGo kereső saját keresőeszköze.  
Használata a lehető legegyszerűbb, ugyanis csak egy felparaméterezett `iframe` beszúrására van szükség, ám cserében csak kevés dolgot tudunk rajta testre szabni, továbbá a keresési eredményekhez már a saját oldalára ugrik. A [kód](https://duckduckgo.com/search_box) innen szerezhető be. 
- **Google Search Engine**: a Google saját szolgáltatása, amely gyakorlatilag úgy működik, mintha a Google-t beszúrnád egy mini ablakba a keresés megnyomása után.  
Sokkal jobban hozzáilleszthető a weboldalad kinézetéhez, mint a DuckDuckGo. Hátránya, hogy időnként reklámok linkjeit is kihozza "találatként" (ráadásul azokat a lista elejére), nem csak a saját aloldalaidat, továbbá használatához Google regisztráció is szükséges.  
A keresőmotor beállítópanele [itt](https://cse.google.com/cse/all) található.  

A további lehetőségek felsorolása egyelőre felesleges, így is van miből válogatni. Természetesen saját magad is belevághatsz egy keresőmotor megalkotásába, de ha már ilyeneket tudsz, akkor igazából ennek a "cikknek" nem kifejezetten te vagy a fő célközönsége. 😃

### 1.1 Google Search Engine

Itt nyomj a *"New search engine"* gombra, add meg a weboldalad címét (minden példánál ott van a www, de az természetesen felesleges), majd add meg a többi mezőt, ha szükséges. 

A színösszeállítások finomhangolásához a *"Look and feel"* menüt kell választanod. 


<a name="2"></a>
## 2. Fórumok

Itt is olyanokat sorolok csak fel, amikkel már volt dolgom. 

- **Önálló fórum**: ezek kifejezetten fórumok készítéséhez fejlesztett fórummotorok, amilyen pl. a [phpBB](https://www.phpbb.com/).  
Az ilyen  az egész oldal (vagy a weboldalad egy aloldala) egy hatalmas fórum, témakörökkel, részletes admin felülettel stb.
- **Vendégkönyv**: PHP alapú, önálló oldalon belüli fórummotorok, mint pl. a még DB-t sem igénylő [hnGuestBook](http://www.hnscripts.com).  
Ezek nagyon gyorsak, az oldal betöltését csak minimálisan befolyásolják, de képességeik erősen korlátozottak. 
- **Blogkommentelő rendszer**: ezek elsősorban blogok, cikkek, publikációk kommentelésére fejlesztett, központilag menedzselhető fórummotorok. A legelterjedtebb ezek közül a [Disqus](http://disqus.com).  
Segítségével a látogatók különféle bejelentkeztetést is választhatnak, hogy megkapják a kommentelés lehetőségét, ami biztonságos, jól áttekinthető. 

### 2.1 Disqus

Akár önálló felhasználó létrehozásával, akár meglévő Facebook/Twitter/Google account-al regisztrálhat bárki ingyenesen az oldalon. A bejelentkezés után akár több site-ot is létrehozhatunk, majd a kapott kódot a weboldalunk kívánt aloldalainak bármelyik részére (általában az aljára) illeszthetjük. A Disqus ezeket az aloldalakat külön-külön képes kezelni. A kommentek moderálását akár helyben bejelentkezve, akár a Disqus weboldalán, központilag is elvégezhetjük. 

>Egyelőre ennyi. Ez a jegyzet még bővül illetve rendszerezettebb lesz várhatóan. 