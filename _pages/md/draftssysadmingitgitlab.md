# ![alt_text](/_ui/img/icons/gitlab_m.svg) GitLab Basics

<div style="text-align: right;"><h7>First article here about the subject posted: 2019-07-30 13:17</h7></div>
###### .

<div class="info">
<p>This subpage is still a mess; sorry</p>
</div>

![alt_text](gitlab_-_wallpaper.jpg)


## Table of Contents

1. [Introduction](#1)
2. [Source Control with GitLab](#2)
3. [Self-managed GitLab](#3)
4. [GitLab API](#4)


<a name="1"></a>
## 1. Introduction

**GitLab** is a centralized tool for not only storing and controlling teams of developers' codebase with the **Git Standard**, but also provides a set of built-in solutions for testing and deploying it. 

From history point of view, **GitHub** was the first. It was originally developed by *Chris Wanstrath*, *P. J. Hyett*, *Tom Preston-Werner*, and *Scott Chacon* using Ruby on Rails since **2007**. The GitHub.com site was launched in April 2008, and since its first-mover advantage, GitHub became the home base of many open-source code repositories. 

**GitLab** came a bit later. Ukrainian developers *Dmitriy Zaporozhets* and *Valery Sizov* created it originally, and the first initial release came out in **2011**. From day one, GitLab was designed to be a set of collaboration tools as well as a code repository service, so this combination should allow teams to develop, secure, and operate software in a single application. 

### 1.1 Key Differences between GitHub and GitLab 

Both GitHub and GitLab handle the **low level version controlling features** exactly the same way, as standards must be followed. So if you're already familiar with any of them, you won't have troubles with using the `git` commands locally, the IDE integrations, like the built-in one in Microsoft's Visual Studio Code will work the same way too, and server side also will be somewhat similar when working with version control features only, or just reading `.md` files on the Web GUI. **It is the vision of the two solutions what makes them different.**

The **core difference** is GitLab has **Continuous Integration/Continuous Delivery (CI/CD)** and **DevOps** workflows built-in. GitHub lets you work with the CI/CD tools **of your choice**, but you'll need to integrate them yourself. GitHub users typically work with a third-party CI program such as Jenkins, CircleCI, or TravisCI, and GitHub offers ways to integrate with such tools on its own. 

**Branches** shortly are your new and unique changes on your codebase. In handling branches, GitHub advocates merging new branches with the default/protected `master` or `main` branch as soon as possible. In GitLab's workflow, you create multiple yet separate stable branches beyond that of the default/protected branch for each set of changes you make. At a minimum, you'll have production and pre-production stable branches. The multiple branch approach does require a multiple-step testing process. A single code review upon the merge request isn't enough. From this point of view, **GitHub prioritizes speed, while GitLab focuses on reliability**. 

If you want to host your private repositories and the whole environment behind them on your own, GitLab is also a better choice because it offers a great **self-managed** option. 

### 1.2 GitLab SaaS vs. self-managed

When using **GitLab SaaS**, you don't need to install anything to use it. You only need to sign up on [gitlab.com](https://gitlab.com), and use GitLab straight away since it is hosted, managed, and administered by GitLab, Inc., with free and paid options for individuals and teams. These offerings are **Free**, **Premium** and **Ultimate**.

**GitLab self-managed** is when you install, administer, and maintain your own GitLab instance yourself, on your own server. That server can be running on several platforms, under any bare metal, virtualization or even Kubernetes. GitLab self-managed also has both free (**Community Edition** (CE)) and paid (**Enterprise Edition** (EE))options. 


<a name="2"></a>
## 2. Source Control with GitLab

In this chapter, we will see the **general usage of Git from GitLab GUI's perspective**. This means, we won't take advantage of the real power of GitLab in this chapter yet, covered by CI/CD, but we will already create and maintain a centralized repository for quick and efficient teamwork. 

### 2.1 The GitLab Project

GitLab recommends the administration and development of larger but closely related sets of tasks separated into **Projects**.

#### 2.1.1 Creating a Project

It was discussed already above for Git Basics chapter, but let's see it again, how it's done (with ): 

**Create a project** &rarr; **Create a blank project** &rarr; **Project name** &rarr; ez lehet most pl. "Hello World": 

Once it's created, copy the URL of the project for cloning with **Clone** &rarr; **Clone with SSH**. 

#### 2.1.2 The default/protected branch

In Settings &rarr; Repository, you can see the basic repository settings for your Project. By default, GitLab sets the one and only branch (which is `main` in this example) both as default and protected. 

- **default** means that whenever you visit the the Project, GitLab Web GUI will list the contents of that branch. Also, all **merge requests** and **commits** are made against this branch by *default*, unless you specify a different one.
- **protected** means the ability to restrict who can modify the branch

The protection is not so restrictive by default, so it is recommended to be enhanced. By setting Allowed to push to No one, you can ensure nobody is ever, not even accidentaly push changes into the default/protected branch. It means, saving any changes into that one is possible only through merge requests: 

![alt_text](gitlab_-_protection_-_basic.png)

#### 2.1.3 Project Properties

Selecting **Menu** &rarr; **Projects** &rarr; **View all projects** (SaaS) or **Explore projects** (Self-managed), we can browse our and others' Projects under the **View all projects** tab. 

Here, we also can see their most important properties, like **Visibility**-je (**Private**, **Internal** or **Public**), what permissions we have for them and so on. 

These low level properties can be modified in **Settings** &rarr; **General** &rarr; **Visibility, project features, permissions**. 

#### 2.1.4 Deleting a Project

We can do it in the same page, so in **Settings** &rarr; **General** &rarr; **Advanced** &rarr; **Expand** &rarr; **Delete this project**. 

### 2.2 GitLab Groups

Git itself has already proven itself in teamwork, it was mainly invented for this reason. In practice, we usually do not work with the previously described Projects running under our own name, but within a **Group**, to share the belonging Projects with team members. From the Project's point of view, the foundations of the Projects that belong to the same Group are the same.

So let's create a new Group. For the sake of simplicity, call it that is somehow related to the belonging Projects: 

![alt_text](gitlab_-_new_group.png)

From this part, we can go through with the same steps like before: create a new Project under this Group, then `clone`, edit, `commit`, `push`. 

### 2.3 Merge Requests

Working in a team that keeps the common codebase in this type of centralized, controlled way require continuous supervision and reviewal what should be and can be kept in the default, protected branch. Meaning, what are the changes and improvements that are allowed to be merged into the most commonly used branch. 

In practice, as an individual person, it means following this short procedure: 

1. **ensure** you use the up-to-date version of `main` (or `master` if you still work with that)
2. create a **new branch** (which is `branchA` in the below figure)
3. make your **changes, tests** and so on, and when you feel it's ready to be merged with the `main` branch, then...
4. create a **Merge Request** (MR), provide a short description for it (why was it needed, what does it fix etc.)
5. ask the team or a team mate to **review** it and **approve** the MR
6. **merge** it

If we want to draw this on a figure, it works something like this: 

![alt_text](git_-_branches_-_simple.png)

The Merge Request and the approval, of course, is not mandatory, but it is always a good practice to follow this as a policy for auditable source control. 

#### 2.3.1 Merge Request in practice, using GitLab's GUI only for code changes

For **simple modifications**, it is always a good and quick practice: 

1. In GitLab GUI, go to the main page of the Project, and create a **New branch**: 

![alt_text](gitlab_-_new_branch_-_creation.png)

2. For **Branch name**, you should specify something **short and self-explanatory**. The **Create from** should stay the default/protected branch (so `main` or `master`, then &rarr; **Create branch** gomb):  

3. GitLab puts the user **under the new branch** automatically but **ensure you are really there**

4. Edit the file or files you want, then, as a *Save*, write a nice **Commit message**, e.g. `chore(001): make headline h1 in README.md`, then click on **Commit changes** button when ready

5. When no more Commits are expected, click on **Create merge request** (or if you are lost somewhere, you can do the same under **Repository** &rarr; **Branches** &rarr; **Merge request** button on the proper row)

6. Summarize your changes in the **Description** field, assign the MR into yourself at the **Assignee** section, you may leave the **Delete source branch when merge request is accepted.** parameter as is, then click on **Create merge request** button

7. Send the link of the MR to a colleague, who reviews it, then she/he approves it

8. Merge it: 

![alt_text](gitlab_-_new_branch_-_merge.png)

Under **Settings** &rarr; **General** &rarr; **Merge requests**, there are several further security settings. Further users can be invited e.g. here: 

![alt_text](gitlab_-_project_in_admin_area.png)

### 2.5 Migrating the default/protected branch

In this scenario, let's assume you still want to use `master` as default/protected instead of `main`, and when everybody else is agreed on that and being notified, delete the original branch. 

1. Copy the URL with **Copy URL** button on GitLab GUI for cloning the repo, then prepare locally: 

```ksh
$ pwd
/path/to/my/codebase

$ ls -la
total 16
drwxr-xr-x. 3 gjakab users 4096 Oct  3 23:26 .
drwxr-xr-x. 3 gjakab users 4096 Oct  3 22:03 ..
drwxr-xr-x. 8 gjakab users 4096 Oct  5 19:35 .git
-rwxr-xr-x. 1 gjakab users  538 Oct  3 22:25 zombietrack.sh

$ cd ..

$ ls -la
total 12
drwxr-xr-x. 3 gjakab users 4096 Oct  3 22:03 .
drwxr-xr-x. 3 root   root  4096 Oct  3 22:03 ..
drwxr-xr-x. 3 gjakab users 4096 Oct  3 23:26 codebase

$ rm -rf codebase

$ git clone git@gitlab.com:gipszjakab/codebase.git
Cloning into 'codebase'...
remote: Enumerating objects: 9, done.
remote: Counting objects: 100% (9/9), done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 9 (delta 2), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (9/9), done.
Resolving deltas: 100% (2/2), done.

$ cd codebase/

$ git status
On branch main
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean

$ git branch -m main master

$ git status
On branch master
Your branch is up to date with 'origin/main'.
[...]
nothing to commit, working tree clean

$ git push -u origin master
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for master, visit:
remote:   https://gitlab.com/gipszjakab/codebase/-/merge_requests/new?merge_request%5Bsource_branch%5D=master
remote: 
To gitlab.com:gipszjakab/codebase.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

2. On GitLab GUI, check the new branch: 

![alt_text](gitlab_-_migrate_default_branch_-_1.png)

3. Go to **Settings** &rarr; **Repository**. Change the **Default branch** and **Save changes**: 

![alt_text](gitlab_-_migrate_default_branch_-_2.png)

4. On the same page, go to **Protected branches** and set the protection the same as `main` had or agree with others how it should be. The below one is a quite common set of permissions. Click on **Protect** when you are done: 

![alt_text](gitlab_-_migrate_default_branch_-_3.png)

5. **Unprotect** the old one: 

![alt_text](gitlab_-_migrate_default_branch_-_4.png)

6. Finally, you can either **rename** (see below) or **delete** the old branch even on command line. Example for deleting it: 

```ksh
$ git status
On branch master
Your branch is up to date with 'origin/master'.
[...]
nothing to commit, working tree clean

$ git push origin --delete main
To gitlab.com:gipszjakab/codebase.git
 - [deleted]         main
```

Source: [bud agency](https://bud.agency/rename-master-branch-main-git-gitlab/)

#### 2.5.1 Rename the old branch

If you want to **keep** the old branch for a while to be sure that's really not needed instead of **deleting** it (as above), here is the way (**Still assuming that you have `master` as default/protected and `main` as obsolete, to-be-deleted**. In case your desire is just the opposite or you use other branch names, compose the commands accordingly.): 

```ksh
$ cd codebase

$ git status
On branch master
Your branch is up to date with 'origin/master'.
[...]

$ git pull
[...]

$ git checkout main
Switched to branch 'main'
Your branch is up to date with 'origin/main'.

$ git branch -m main main-to-be-deleted

$ git status
On branch main-to-be-deleted
Your branch is up to date with 'origin/main'.
[...]

$ git branch -a
[...]

$ git push origin --delete main
To gitlab.com:gipszjakab/codebase.git
 - [deleted]         main

$ git push --set-upstream origin main-to-be-deleted
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
[...]
branch 'main-to-be-deleted' set up to track 'origin/main-to-be-deleted'.

$ git status
On branch main-to-be-deleted
Your branch is up to date with 'origin/main-to-be-deleted'.
[...]

$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
```

### 2.6 Soft Reset: Splitting existing commits for trackable changes

This practice is useful if you had multiple tasks done on your downstream branch but you pushed all the changes at once, either accidentaly or intentionally, but you're mandated to present a better Merge Request to the reviewer by splitting the changes for clear tracking. 

This is the stage where you already had the undesired "all-in-one" commit. Meaning in this case: you added both `blabla1.md` and `blabla2.md` files, committed and pushed to origin already: 

```ksh
$ git add .

$ git status
On branch newbranch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   blabla1.md
	new file:   blabla2.md

$ git commit -m "docs: add everything"
[...]
 * [new branch]      HEAD -> newbranch

$ git -P log
commit d29bf04f790315f3b1b72dedc7223c1eacac80b5 (HEAD -> newbranch, origin/newbranch)
Author: gjakab <gjakab@example.com>
Date:   Tue Nov 28 10:35:29 2023 +0200

    docs: add everything

commit 3e6c33c68e4784802cc6d1cc49363c603b95875e (origin/main, origin/HEAD, main)
Author: gjakab <gjakab@example.com>
Date:   Fri Jan 20 10:37:24 2023 +0000
[...]
```

After quickly creating a Merge Request, the list of commits isn't really trackable:

![alt_text](gitlab_-_softreset_-_01.png)

And this is a potential solution for just removing that all-in-one commit, by returning back to the previous commit hash, and then spliting it into two ones: 

```ksh
$ git reset --soft 3e6c33c68e4784802cc6d1cc49363c603b95875e

$ git status
On branch newbranch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   blabla1.md
	new file:   blabla2.md

$ git restore --staged .

$ git status
On branch newbranch
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	blabla1.md
	blabla2.md
[...]

$ git add blabla1.md

$ git status
On branch newbranch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   blabla1.md
[...]
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	blabla2.md

$ git commit -m "docs: add blabla1.md"
[newbranch c8c8b8a] docs: add blabla1.md
 1 file changed, 3 insertions(+)
 create mode 100644 blabla1.md

$ git add blabla2.md

$ git commit -m "docs: add blabla2.md"
[newbranch 9b28315] docs: add blabla2.md
 1 file changed, 3 insertions(+)
 create mode 100644 blabla2.md

$ git status
On branch newbranch
nothing to commit, working tree clean

$ git push origin HEAD --force
[...]
 + d29bf04...9b28315 HEAD -> newbranch (forced update)
```

Now it looks much nicer for the reviewer: 

![alt_text](gitlab_-_softreset_-_02.png)

### 2.7 Dropping commits with hard reset

This is not the nicest solution, but if `git revert` is not an option, then we may probably use this way. 

Before doing anything, ensure **you have the latest version** on your local copy. In this case, the branch was not the default/protected `main` one, so forced pushing was allowed. 

1. Pulling the latest changes and switching to the custom branch: 

```ksh
$ git pull
Already up to date.

$ git checkout k8s_rhel
Switched to branch 'k8s_rhel'
Your branch is up to date with 'origin/k8s_rhel'.

$ git pull
Already up to date.
```

2. Get the ID of the commit that you want **to keep** as the last one (it was `f0037bed515f34e3e8daa4877cc9fe43025db063` in this case), and anything above deleted: 

```ksh
$ git log | head -50
commit 14e26538cbd547b6aa942ab8c245925ec0e51c1e
Author: gipsz.jakab <gipsz.jakab@example.com>
Date:   Fri Oct 21 09:12:25 2022 +0300

    Revert "fix: use the proper column in lsblk output for zklog"
    
    This reverts commit 0f14a25c29bad805a754208cbbe6089037bd6721.

commit 0f14a25c29bad805a754208cbbe6089037bd6721
Author: gipsz.jakab <gipsz.jakab@example.com>
Date:   Thu Oct 20 15:40:50 2022 +0000

    fix: use the proper column in lsblk output for zklog

commit f0037bed515f34e3e8daa4877cc9fe43025db063
Author: KMikorka <mikorka.kalman@example.com>
Date:   Sun Sep 18 22:26:07 2022 +0300

    RHEL Installation and k8s bootstrap for k8sop worker nodes
[...]
```

3. Hard reset to that commit: 

```ksh
$ git reset --hard f0037bed515f34e3e8daa4877cc9fe43025db063
HEAD is now at f0037be RHEL Installation and k8s bootstrap for k8sop worker nodes
```

4. And forced `push`: 

```ksh
$ git push origin HEAD --force
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: View merge request for k8s_rhel:
[...]
 + 0f14a25...f0037be HEAD -> k8s_rhel (forced update)
```

### 2.8 Rebasing branches

If your local changes in your alternate branch were pushed into remote already, and you're satisfied with it, you can create your Merge Request, ask for approvals, and merge. However, if that codebase is relatively frequently changed by other team members, then it's easy to get into a situation when the source branch of your alternate branch is ahead. Luckily, GitLab is so smart it can solve this issue even remotely if you can see and you can use this button for this feature: 

![alt_text](gitlab_-_rebase.png)

If only local rebase is the only solution, you need to try it. Here is an example for this set of activities:

1. Rebase locally: 

```ksh
$ git status
On branch sre-5193
[...]

$ git checkout main
Switched to branch 'main'
Your branch is up to date with 'origin/main'.

$ git pull
[...]
 1 file changed, 4 deletions(-)

$ git rebase main sre-5193
Successfully rebased and updated refs/heads/sre-5193.

$ git status
On branch sre-5193
Your branch and 'origin/sre-5193' have diverged,
and have 11 and 4 different commits each, respectively.
[...]
```

2. Delete the branch (it is called `sre-5193` in this example) on GitLab GUI. 

3. Push your local copy as it is the only source of truth: 

```ksh
$ git push origin sre-5193
[...]
```

### 2.9 The `.gitignore` file

If you don't want any of the files or folders from being tracked and source-controlled, then you can just have this simple config file to instruct `git` not to deal with them at all. E.g.:

```ksh
$ cat .gitignore
# ignore the whole .obsidian folder from being tracked
.obsidian/
```

### 2.10 GitLab's Issues feature

The purpose of it is to track the work to be done on projects, and for users to report any bugs they find. 


<a name="3"></a>
## 3. Self-managed GitLab

Now let's see how you can build **your own GitLab Server from scratch**. 

Regarding licensing, it's possible that GitLab changes the plans/offerings in time, so it's good to visit [their site](https://docs.gitlab.com/ee/subscriptions/self_managed/) about them in advance.  
Currently, there are two Self-managed plans:

- GitLab **Community** Edition (CE)
- GitLab **Enterprise** Edition (EE)

### 3.1 Preparing the Lab Setup

It's worth checking the official [list of Supported OSes in GitLab Docs site](https://docs.gitlab.com/ee/administration/package_information/supported_os.html) and ensure your OS and arthitecture is supported. 

<div class="info">
<p>Even though it shows <code>arm64</code> e.g. for Debian, <code>aarch64</code> is also supported and working without any issues, so ARM64 SBCs are basically supported.</p>
</div>

A **potential Lab Setup**, involving VMs only: 

![alt_text](gitlab_-_self-managed_-_lab_env_1.png)

It worth preparing well, because it can be quite **resource consuming**. Here is an example, when the environment is just shutting down: 

![alt_text](gitlab_-_self-managed_-_lab_env_stopped.png)

### 3.2 Prerequisites

The required packages before installing: 

- `curl`
- `openssh-server`
- `ca-certificates`

These are usually installed by default already, but it worth ensuring about their presence. 

Further HW and disk space requirements: 

- at least **2 CPUs** (or vCPUs) and at least **3 GB of RAM** is highly recommended
- the free space under `/opt` must be **at least 2.5 GB**

### 3.3 Getting the GitLab CE repo

As a wide selection of OSes and architectures are supported, the method for this differs. Let's see the most commonly used `x86_64` on a **Rocky**, and also try it on a currently less popular (at least, from GitLab CE's point of view), but rapidly emerging `aarch64`, on a **Debian** Linux. 

#### 3.3.1 GitLab CE repo preparation for systems with RPM Package Management

On **Red Hat**, **CentOS**, **Rocky**, **openSUSE** systems, you can proceed like this: 

```ksh
# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6928  100  6928    0     0  12665      0 --:--:-- --:--:-- --:--:-- 12642
Detected operating system as rocky/8.
Checking for curl...
[...]
Installing yum-utils...
gitlab_gitlab-ce-source                              323  B/s | 862  B     00:02    
gitlab_gitlab-ce-source                              2.3 kB/s | 3.1 kB     00:01    
[...]

# dnf repolist
repo id                                              repo name
appstream                                            Rocky Linux 8 - AppStream
baseos                                               Rocky Linux 8 - BaseOS
extras                                               Rocky Linux 8 - Extras
gitlab_gitlab-ce                                     gitlab_gitlab-ce
gitlab_gitlab-ce-source                              gitlab_gitlab-ce-source
```

On **Rocky 9**, currently (as of September 2022), the above downloader is refusing to set the repositories up, but they officially offer the solution for it, which is enforcing the `os` and `dist` variables: 

```ksh
# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh > script.rpm.sh

# chmod +x script.rpm.sh 

# os=rocky dist=8 ./script.rpm.sh 
Detected operating system as rocky/8.
Checking for curl...
[...]
The repository is setup! You can now install packages.
```

#### 3.3.2 GitLab CE repo preparation for systems with DEB Package Management

Tested on `aarch64`, with Debian 11 *"Bullseye"*: 

```ksh
# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6865  100  6865    0     0  24003      0 --:--:-- --:--:-- --:--:-- 24087
Detected operating system as debian/bullseye.
Checking for curl...
Detected curl...
Checking for gpg...
Detected gpg...
Running apt-get update... done.
Installing debian-archive-keyring which is needed for installing 
apt-transport-https on many Debian systems.
Installing apt-transport-https... done.
Installing /etc/apt/sources.list.d/gitlab_gitlab-ce.list...done.
Importing packagecloud gpg key... done.
Running apt-get update... done.
[...]
The repository is setup! You can now install packages.
```

### 3.4 Installing GitLab CE

1️. Specifying the initial `root` GitLab user's password is a good idea to be done in the beginning. Otherwise, it must be reset separately, afterwards. So, let's specify it now, e.g.: 

```ksh
# export GITLAB_ROOT_PASSWORD="q1w2e3r4"
```

2. Proceed with the installation.  
  On RPM systems, it is `dnf install gitlab-ce`, on DEB ones, it is `apt-get install gitlab-ce`. 

3. Accept all the GPG keys and any other questions/warnings during the installation. It should end like this:

```ksh
[...]
       *.                  *.
      ***                 ***
     *****               *****
    .******             *******
    ********            ********
   ,,,,,,,,,***********,,,,,,,,,
  ,,,,,,,,,,,*********,,,,,,,,,,,
  .,,,,,,,,,,,*******,,,,,,,,,,,,
      ,,,,,,,,,*****,,,,,,,,,.
         ,,,,,,,****,,,,,,
            .,,,***,,,,
                ,*,.
[...]
     _______ __  __          __
    / ____(_) /_/ /   ____ _/ /_
   / / __/ / __/ /   / __ `/ __ \
  / /_/ / / /_/ /___/ /_/ / /_/ /
  \____/_/\__/_____/\__,_/_.___/
[...]
Thank you for installing GitLab!
GitLab was unable to detect a valid hostname for your instance.
Please configure a URL for your GitLab instance by setting `external_url`
configuration in /etc/gitlab/gitlab.rb file.
Then, you can start your GitLab instance by running the following command:
  sudo gitlab-ctl reconfigure
[...]
```

### 3.5 Minimal configuration and starting it up

Set the `external_url` parameter in `/etc/gitlab/gitlab.rb` as suggested above, then reconfigure it, then start: 

```ksh
# vi /etc/gitlab/gitlab.rb
[...]
external_url 'http://192.168.56.101'
[...]

# gitlab-ctl reconfigure
Starting Chef Infra Client, version 15.17.4
resolving cookbooks for run list: ["gitlab"]
Synchronizing Cookbooks:
  - gitlab (0.0.1)
[...]
gitlab Reconfigured!

# ss -tulpn | grep 80
tcp   LISTEN 0      1024       127.0.0.1:8080      0.0.0.0:*    users:(("bundle",pid=4553,fd=16),("bundle",pid=4552,fd=16),("bundle",pid=4315,fd=16))
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=4445,fd=10),("nginx",pid=4444,fd=10),("nginx",pid=4443,fd=10))
tcp   LISTEN 0      128        127.0.0.1:8082      0.0.0.0:*    users:(("bundle",pid=4353,fd=20))
tcp   LISTEN 0      511          0.0.0.0:8060      0.0.0.0:*    users:(("nginx",pid=4445,fd=11),("nginx",pid=4444,fd=11),("nginx",pid=4443,fd=11))
```

### 3.6 Accessing the Web GUI and login

1. Ensure **no firewall** is blocking accessing the site. E.g., here is the simple way to disable `firewalld.service` completely on a Rocky 8: 

```ksh
# systemctl disable firewalld
Removed /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service.
Removed /etc/systemd/system/multi-user.target.wants/firewalld.service.

# systemctl stop firewalld
```

2. Open a web browser and type the `external_url` IP after `http://`. It is also a good practice to save the hostname to your remote host or VM, e.g. `rockysrv1` in this example: 

![alt_text](gitlab_-_self-managed_-_welcome.png)

3. By default, **anyone can initiate registration** on the login page with **Register now** (as shown above). This can be considered as safe, because the approval of an admin user is required anyway to finalize it. Otherwise, you can login with your `root` account initially, but it's advised to use your own individual ID once created. 

In case the initially set `root` **password doesn't work**, or we forgot to set it, or we just want to reset it, here is the way to do it: 

```ksh
gitlab-rake "gitlab:password:reset[root]"
Enter password: 
Confirm password: 
Password successfully updated for user with username root.
```

<div class="warn">
You need to <b>wait at least 7-10 minutes</b> before attempting to use the updated password. 
</div>

### 3.7 Initial settings

The self-registration service can be completely turned off on the **Admin Area** &rarr; **Settings** &rarr; **General** page, at the **Sign-up restrictions** section (**Expand**), by turning off the **Sign-up enabled** option. 

In case such registration happened already, you can approve it at **Overview** &rarr; **Users** &rarr; **Pending approval**.

Under the **Overview** &rarr; **Users** page, the new user should also be emerged as an admin. You can do it on the panel after the Edit button, at the **Access** &rarr; **Access level** &rarr; **Administrator** section. Try re-login with that user after that. 

After new login, the *"Welcome to GitLab"* welcomes us. 

It also worth changing the Navigation theme too to differentiate our HomeLab from our working environment visually. You can do it under the **User Settings** &rarr; **Preferences** page. Accessing it is easy from the top right corner, by clicking on our avatar. 

That's all. Your self-managed GitLab Server is ready!


<a name="4"></a>
## 4. GitLab API

In this chapter, you will get a basic understanding about...

- **REST API** calls in general
- basic and advanced usage of `curl` and **Postman** for making GitLab API calls
- simple and advanced `GET` requests for queries, `PUT` requests for changing existing attributes, `POST` requests for creating new objects and `DELETE` for deleting objects (complete example projects in this lab)
- advanced GitLab API calls with Postman, using its **Collections** and **Environments** features
- installing, configuring and getting comfortable with `python-gitlab` tool

GitLab allows users to issue **REST API** requests besides using the normal user interface: the web GUI and `git` data flow for almost anything that can be done with mass amount of clicks on the GUI. 

We use the API mostly with `GET` requests (or **methods**) for simple but very quick or advanced queries, but even `POST`, `PUT` and `DELETE` requests, maybe even more, are possible to alter some settings. The following figure summarizes it how it goes:

![alt_text](gitlab_-_api_requests.png)

Besides the formatted response from server side, the client also always gets a HTTP status code too, which are the standard ones, but let's see them: 

- `100 - 100`: **informational** responses
- `200 - 299`: **successful** responses
- `300 - 399`: **redirection** messages
- `400 - 499`: **client error** responses
- `500 - 599`: **server error** responses

The main GitLab Docs page about using [APIs](https://docs.gitlab.com/ee/api/) can be the starting point for digging for ducumentation. Under that page, probably these are the most frequently used subpages:

- GitLab API calls and examples about [Projects](https://docs.gitlab.com/ee/api/projects.html)
- GitLab API calls and examples about [Groups](https://docs.gitlab.com/ee/api/groups.html)

The HTTP API is versioned with a single number, which is currently `4`. That's why we form our GitLab URLs in this form: 

```
https://<gitlab_server_url>/api/v4/<resource>[?<attributes>]
```

## 4.1 Tools for API requests

For `GET` requests, simply entering the URL to your browser might be enough sometimes, but it is advised to start experiencing using `curl` from the beginning.

### 4.1.1 `curl`

The commonly used options for `curl`: 

- `-X` or `--request` - to specify the type of the request, which is `GET` by default
- `-H` or `--header` - to speficy the `header` for extra parameters to pass over, like the access token
- `--progress-bar` - to show a terminal-width long progress bar instead of statistics at the beginning of the printed results
- `-s` or `--silent` - to disable the statistics and any warnings from the beginning of the printed results
- `-i` or `--include` - to include the response headers, the HTTP status codes, which can be useful (but you need to know that the output this way is incompatible with `jq`)
- `--data` - passing through further data (for `POST` requests mainly)
- `--url` - required only for advanced requests, when using `--data`

You can also explicitly define what responds you accept by specifying it with the header option, e.g. `-H 'accept: application/json;charset=utf-8'`

Instead of immediately trying out GitLab's API, you can try it out with [Dog API](https://dog.ceo/dog-api/) first, to see how nicely it works (you will get a link to a picture of a random dog each time you send your `GET` request): 

```ksh
$ curl https://dog.ceo/api/breeds/image/random | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   101    0   101    0     0   1153      0 --:--:-- --:--:-- --:--:--  1160
{
  "message": "https://images.dog.ceo/breeds/terrier-border/n02093754_5192.jpg",
  "status": "success"
}
```

We will mostly use `curl` for our experiments. 

### 4.1.2 Postman

This is the most popular GUI tool for such API requests. 

It can be downloaded as a local GUI app from [postman.com/downloads](https://www.postman.com/downloads/) or you can also use the tool's web GUI at [web.postman.co](https://web.postman.co/). Any way you choose, the tool is free but you need to register and login. 

Using it against the same random dog picture "service" provider: 

![alt_text](postman_-_dog_api.png)

### 4.2 Authentication for API requests

The `public`, GitLab SaaS stored projects don't require any authentication. Mostly it is needed, and even if you get successful response without authentication, the list of accessible fields are limited. E.g., after specifying the authentication for your simple `.../project/<project_id>` GitLab API request, you will get **137** lines instead of **30** at the time of writing this article. 

There are several ways for authenticating with the GitLab API:

- OAuth2 tokens
- **Personal access tokens**
- Project access tokens
- Group access tokens
- Session cookie
- GitLab CI/CD job token (Specific endpoints only)

#### 4.2.1 Requesting and using Personal Access Token

It can be generated and checked under **User Settings** (or **Preferences**) &rarr; **Access Tokens**: 

![alt_text](gitlab_-_personal_access_tokens.png)

After clicking on **Create personal access token** button, it reveals your token only once, when created, so worth storing it somewhere if you don't want to re-re-recreate it when you try it out on some different days. 

- for requests, done by `curl`, you need to use the `-H 'PRIVATE-TOKEN: <token>'` option.
- for `postman`, you can do it in the **Authorization** tab, by selecting **Bearer Token** as **Type**, and add the token there. 
- If the query would require authentication, but that's totally **missing**, you get response like this: 

```ksh
$ curl -X 'GET' 'https://mygitlabserver/api/v4/groups/221/projects?include_subgroups=true' | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    33  100    33    0     0    265      0 --:--:-- --:--:-- --:--:--   266
{
  "message": "404 Group Not Found"
}
```

- If you provided an **invalid** (e.g. because it's been revoked) token, you get response like this: 

```ksh
$ curl -X 'GET' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://mygitlabserver/api/v4/groups/221/projects?include_subgroups=true' | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    30  100    30    0     0    164      0 --:--:-- --:--:-- --:--:--   163
{
  "message": "401 Unauthorized"
}
```

### 4.3 GitLab API `GET` requests

#### 4.3.1 Simple `GET` requests

The **most simple query** is probably this one, which results with the recent 20 active `public` projects: 

```ksh
$ curl https://gitlab.com/api/v4/projects
[...]
```

Practically, when we use `curl` for queries, we use it with `jq` to convert the output to **human readable JSON query format**. 

Query for a very simple, `public` project: 

```ksh
$ curl https://gitlab.com/api/v4/projects/39922333 | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   810  100   810    0     0   2004      0 --:--:-- --:--:-- --:--:--  2004
{
  "id": 39922333,
  "description": null,
  "name": "Codebase",
  "name_with_namespace": "Jakab Gipsz / Codebase",
  "path": "codebase",
  "path_with_namespace": "gipszjakab/codebase",
  "created_at": "2022-10-03T20:19:14.806Z",
  "default_branch": "main",
  "tag_list": [],
  "topics": [],
  "ssh_url_to_repo": "git@gitlab.com:gipszjakab/codebase.git",
  "http_url_to_repo": "https://gitlab.com/gipszjakab/codebase.git",
  "web_url": "https://gitlab.com/gipszjakab/codebase",
  "readme_url": null,
  "avatar_url": null,
  "forks_count": 0,
  "star_count": 1,
  "last_activity_at": "2022-10-03T20:19:14.806Z",
  "namespace": {
    "id": 55781549,
    "name": "Jakab Gipsz",
    "path": "gipszjakab",
    "kind": "user",
    "full_path": "gipszjakab",
    "parent_id": null,
    "avatar_url": "https://secure.gravatar.com/avatar/d033f8db36459d7d16585b5452bbfa41?s=80&d=identicon",
    "web_url": "https://gitlab.com/gipszjakab"
  }
}
```

The same with explicitly specifying the request type too: 

```ksh
$ curl -X 'GET' 'https://gitlab.com/api/v4/projects/39922333' | jq
[...]
```

Searching for a particular attribute, for `last_activity_at`, and using `silent` mode for skipping statistics: 

```ksh
$ curl -s -X 'GET' 'https://gitlab.com/api/v4/projects/39922333' | jq .last_activity_at
"2022-10-05T17:47:24.111Z"
```

#### 4.3.2 Advanced `GET` requests

Applying the authentication and searching for a "hidden" attribute (can be seen only with authentication), e.g. `restrict_user_defined_variables`: 

```ksh
$ curl -s -X 'GET' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/39922333' | jq .restrict_user_defined_variables
false
```

You can request for the same attribute explicitly by improving the URL in the previous command with the `?<attribute>` closing tag. Even though it is a filter, it also gives the long output, not only the referenced attribute (the syntax is important anyway, because this is the standard way of doing such queries, and this is useful when working with `PUT` requests): 

```ksh
$ curl -s -X 'GET' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/39922333?restrict_user_defined_variables' | jq
```

Of course, the `GET` methods can be considered much more advanced when you combine queries on multiple groups/projects automatically, depending one on another. (Discussed maybe later.)

### 4.4 GitLab API `PUT` requests

Changing an attribute, then check the results, e.g.:

```ksh
$ curl -s -X 'PUT' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/39922333?restrict_user_defined_variables=true'
{"id":39922333 [...]

$ curl -s -X 'GET' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/39922333' | jq .restrict_user_defined_variables
true
```

Doing the same in Postman is much more convenient because that fills up the key in the URL for us automatically: 

![alt_text](postman_-_put_request.png)

### 4.4 GitLab API `POST` requests

You can even create projects from scratch. With `curl`, it can be done like this (formatted with `\` line merging separators in order to follow it better): 

```ksh
$ curl -X 'POST' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' \
    -H "Content-Type: application/json" \
    --data '{"name": "My Brand New Project",
             "path": "my-brand-new-project",
             "initialize_with_readme": "true"}' \
    --url 'https://gitlab.com/api/v4/projects/'
```

Note that the JSON data in the `--data` section doesn't require separators!

Doing the same in Postman is very similar, but you need to rather use the `application/x-www-form-urlencoded` `Content-Type` selected in the **Headers** tab... 

![alt_text](postman_-_post_request_-_1.png)

...and add any of the `--data` parts in the **Body** tab: 

![alt_text](postman_-_post_request_-_2.png)

The result should give a HTTP `201` response: 

![alt_text](postman_-_post_request_-_3.png)

### 4.5 GitLab API `DELETE` requests

E.g., deleting a Project by its ID: 

```ksh
$ curl -X 'DELETE' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/40093080'
{"message":"202 Accepted"}
```

In GitLab's **Premium tiers**, the deleted Projects can be **restored** by their IDs, through `POST` request: 

```ksh
$ curl -X 'POST' -H 'PRIVATE-TOKEN: glpat-AaBbCcDdEeFfGgHh' 'https://gitlab.com/api/v4/projects/40093080/restore'
```

### 4.6 Advanced GitLab API calls with Postman

Postman can be configured as a handy development tool as it supports Collections and Environments too. 

Setting up **Environments**, and populating some basic variables (you need to also select Set as active environment in the properties menu, represented by the `...` in the right top corner): 

![alt_text](postman_-_environment_variables.png)

Setting up **Collections**, saving some basic requests, and using the previously defined variables: 

![alt_text](postman_-_collection.png)

#### 4.6.1 Mass query with page limit

Assuming you still have your GitLab instance's API url in `base_url` in `https://gitlab.xyzcompany.com/api/v4` format and you have a valid token in `private_token`, below is a tested query for **fetching all the projects, including subgroups (so recursive) under a specific group**. In our case, `system-management`'s group ID is `239`, so set this for `id` in **Path Variables** in advance. Then you can **Send** this `GET` request: 

```
{{base_url}}/groups/:id/projects?archived=false&visibility=internal&order_by=created_at&sort=desc&simple=false&include_subgroups=true&page=1&per_page=500
```

The results will show that no matter you set `500` as `per_page` parameter, the maximum is `100`, and you also already received the info that there are 3 pages in total, so you need to issue two more queries with `page=2`, then `page=3` for all the chunks. You can use **Save Response** menu for saving the JSON data: 

![alt_text](postman_-_page_limit.png)

Assuming you have all the 3 `.json` files, now you can get all the `.git` links for the projects. Note that there are 228 projects at the moment of writing. Let's save it into a file (`.csv` or `.txt`, it doesn't really matter here):

```ksh
$ cat *json | jq | awk '/ssh_url_to_repo/{print $2}' | sed 's/[\"|,]//g' | head
git@gitlab.xyzcompany.com:system-management/ansible-inventory/infra-inventory.git
git@gitlab.xyzcompany.com:system-management/ansible-playbooks/software/infrabooks.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/java.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/ssh-server.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/postgresql.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/postfix.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/nfs-server.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/newhost.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/network.git
git@gitlab.xyzcompany.com:system-management/ansible-roles/nessusd.git

$ cat *json | jq | awk '/ssh_url_to_repo/{print $2}' | sed 's/[\"|,]//g' | sort -u | wc -l
228

$ cat *json | jq | awk '/ssh_url_to_repo/{print $2}' | sed 's/[\"|,]//g' | sort -u > ~/Documents/Manuals_and_Scripts/SRE/configs/gitlab_repolist_-_20230711.csv
```

### 4.7 External tools for advanced GitLab API calls

There are plenty of them, the most known one is probably `python-gitlab`. The tool can be used both for your own CLI solutions for asn an API. The latest version and the documentation for the tool can be found at [python-gitlab.readthedocs.io](https://python-gitlab.readthedocs.io/en/stable/) site.

#### 4.7.1 Introduction to `python-gitlab`

The tool can be even gathered from some Linux distro's standard repository. E.g. on openSUSE: 

```ksh
$ sudo zypper search python-gitlab
[...]
S | Name                    | Summary                                           | Type
--+-------------------------+---------------------------------------------------+--------
  | python38-python-gitlab  | Python module for interacting with the GitLab API | package
  | python39-python-gitlab  | Python module for interacting with the GitLab API | package
  | python310-python-gitlab | Python module for interacting with the GitLab API | package
```

The most recommended way to install it is using `pip`. Let's do it: 

1. Install `python-gitlab` with `pip`: 

```ksh
$ pip --version
pip 22.0.4 from /usr/lib/python3.10/site-packages/pip (python 3.10)

$ pip install --upgrade python-gitlab
Defaulting to user installation because normal site-packages is not writeable
Collecting python-gitlab
  Downloading python_gitlab-3.10.0-py3-none-any.whl (127 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 127.9/127.9 KB 8.5 MB/s eta 0:00:00
[...]
Installing collected packages: python-gitlab
Successfully installed python-gitlab-3.10.0
```

2. Check if it's installed: 

```ksh
$ pip list | grep gitlab
python-gitlab                           3.10.0

$ pip show python-gitlab
Name: python-gitlab
Version: 3.10.0
Summary: Interact with GitLab API
Home-page: https://github.com/python-gitlab/python-gitlab
Author: Gauvain Pocentek
Author-email: gauvain@pocentek.net
License: LGPLv3
Location: /home/gjakab/.local/lib/python3.10/site-packages
Requires: requests, requests-toolbelt
Required-by: 
```

3. Activate the PIP Virtual Environment and test `python-gitlab`'s CLI functionality: 

```ksh
$ sudo find /usr/*/*/venv/*/* -name 'activate*' -ls
  1443572      4 -rw-r--r--   1 root     root         2009 Sep 21 16:28 /usr/lib64/python3.10/venv/scripts/common/activate
  1443574      4 -rw-r--r--   1 root     root          935 Sep 21 16:28 /usr/lib64/python3.10/venv/scripts/posix/activate.csh
  1443575      4 -rw-r--r--   1 root     root         2077 Sep 21 16:28 /usr/lib64/python3.10/venv/scripts/posix/activate.fish
  1574425      4 -rw-r--r--   1 root     root         2201 Sep 17 23:28 /usr/lib64/python3.8/venv/scripts/common/activate
  1574427      4 -rw-r--r--   1 root     root         1255 Sep 17 23:28 /usr/lib64/python3.8/venv/scripts/posix/activate.csh
  1574428      4 -rw-r--r--   1 root     root         2405 Sep 17 23:28 /usr/lib64/python3.8/venv/scripts/posix/activate.fish

$ source /usr/lib64/python3.10/venv/scripts/common/activate

~> gitlab project get --id 39922333
id: 39922333
path_with_namespace: gipszjakab/codebase
```

#### 4.7.2 `python-gitlab`'s configuration files

By default, the tool uses https://gitlab.com as base URL without any tokens. When you use the same GitLab server constantly and the same token, it worth storing them in a config file. 

The tool looks up 3 configuration files by default:

- The `PYTHON_GITLAB_CFG` environment variable, which is an environment variable that contains the path to a configuration file.
- `/etc/python-gitlab.cfg` - system-wide configuration file
- `~/.python-gitlab.cfg` - the current user's configuration file

```
[global]
default = somewhere
ssl_verify = true
timeout = 5

[somewhere]
url = https://gitlab.example.com
private_token = glpat-AaBbCcDdEeFfGgHh
api_version = 4
```

Example for an advanced `GET` request (number of all repos under a particular group): 

```ksh
~> gitlab -v group-project list --get-all --include-subgroups true --group-id 239 | grep ssh-url-to-repo | wc -l
236
```

Further documentation can be found on the [python-gitlab.readthedocs.io](https://python-gitlab.readthedocs.io/en/stable/cli-objects.html#) site.
