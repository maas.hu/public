# ![alt text](/_ui/img/icons/mageia_m.svg) Mageia

###### .

>Szerkesztés alatt...

## Tartalomjegyzék

1. [Bevezető](#1)
2. [Telepítés, újratelepítés](#2)

<a name="1"></a>
## 1. Bevezető

...

<a name="2"></a>
## 2. Telepítés, újratelepítés

Az alábbiakban a Mageia 8 klasszikus telepítési folyamatát nézzük végig egy teljesen üres diszkre, de természetesen meglévő rendszert is felülírhatunk vele.

### 2.1 Letöltés

A rendszer klasszikus telepítős és Live DVD-s változata is letölthető a [mageia.org/en/downloads/](https://www.mageia.org/en/downloads/) oldalról. 

Az oldalon a Checksumok közül is érdemes megnézni legalább az egyiket, miután letöltöttük. 

### 2.2 Bootolható pendrive előkészítése

Windows alatt a [Rufust](https://rufus.ie), Linux alatt egyszerűen a `dd` parancsot tudom ajánlani bootolható pendrive készítéséhez. Ez utóbbinak ismertetése az [általános linuxos témakörök](/learning/it/sysadmin/linux/general/) kategóriában található. 

Ezután már indíthatjuk is újra a laptopunkat/PC-nket, az első másodpercekben nyomkodjuk a boot menüjét előhozó billentyűt (pl. F12-t), majd indulhat is a telepítés. 

### 2.3 A telepítés

Mivel egy klasszikus telepítővel van dolgunk, már a DVD-ről vagy bootolható pendrive-ról való rendszerinduláskor kezdhetjük meg a telepítési folyamatot. 

Ez Live DVD esetén – hasonlóképpen az openSUSE Tumbleweed Live GNOME-ról való telepítéshez – a már elinduló Live rendszerben az Install lehetőségre való kereséssel indítható el, azonban a csomagok kiválasztásakor ott jóval kevesebb lehetőségünk van a válogatásra, mint itt. 

Bebootolt tehát a telepítőnk, válasszuk itt az **Install Mageia** menüpontot. 

![alt_text](m8-ins-01.png)

A telepítő és a rendszer nyelve már itt kiválasztható, de maradhatunk az angolnál is: 

![alt_text](m8-ins-02.png)

Olvassuk el gondosan a licenszszerződést (haha), majd **Accept** és **Next**: 

![alt_text](m8-ins-03.png)

A billentyűzet nyelvének azért mégiscsak jó lenne a magyar. Válasszuk ki, majd **Next**: 

![alt_text](m8-ins-04.png)

Elérkeztünk a partícionáláshoz. Üres diszk és különösebb igények nélkül használhatjuk akár a **Use free space** megoldást, amelynek segítségével a Mageia telepítője maga végzi el a partíciók szétosztását, de jelen példában mi most el fogunk egy kis időt tölteni a gondos partícionálással, így válasszuk a **Custom disk partitioning** opciót: 

![alt_text](m8-ins-05.png)

...

![alt_text](m8-ins-06.png)

...


![alt_text](m8-ins-07.png)

...

![alt_text](m8-ins-08.png)

...

![alt_text](m8-ins-09.png)

...

![alt_text](m8-ins-10.png)

...

![alt_text](m8-ins-11.png)

...

![alt_text](m8-ins-12.png)

...

![alt_text](m8-ins-13.png)

...

![alt_text](m8-ins-14.png)

...

![alt_text](m8-ins-15.png)

...

![alt_text](m8-ins-16.png)

...

![alt_text](m8-ins-17.png)

...

![alt_text](m8-ins-18.png)

...

![alt_text](m8-ins-19.png)

...

![alt_text](m8-ins-20.png)

...

![alt_text](m8-ins-21.png)

...

![alt_text](m8-ins-22.png)

...

![alt_text](m8-ins-23.png)

...

![alt_text](m8-ins-24.png)

...

![alt_text](m8-ins-25.png)

...

![alt_text](m8-ins-26.png)

...

![alt_text](m8-ins-27.png)

...

![alt_text](m8-ins-28.png)

...

![alt_text](m8-ins-29.png)

...

![alt_text](m8-ins-30.png)

...

![alt_text](m8-ins-31.png)

...
