# ![alt_text](/_ui/img/icons/gitlab_m.svg) GitLab

<div style="text-align: right;"><h7>First article here about the subject posted: 2019-07-30 13:17</h7></div>
###### .

<div class="info">
<p>This subpage is still a mess; sorry</p>
</div>

![alt_text](gitlab_-_cicd_-_bg.png)


## Table of Contents

1. [Introduction](#1)
2. [GitLab CI/CD](#2) <-- will change
3. [Release Management](#3)


<a name="1"></a>
## 1. Introduction

<a name="2"></a>
## 2. GitLab CI/CD

Giving too much details about even the title of this chapter would inevitably bring us to loads of bullshit, and it would be too far from the actual implementation of the goal. However, if you are not familiar with the subject, then maybe it's worth to go through on some thoughts. 

### 5.1 Continuous Integration

**CI** refers to this. It is *Integration*, because using this solution, the changes immediately become integrated to our codebase. It is *Continuous*, because if everything is configured properly, then everything is done automatically. 

Comparing CI with some traditional solutions, CI assumes small pieces and multiple Merge Requests. Traditionally, it was rather done in less and larger chunks making QA and testing sometimes more difficult. In case of CI, it is expected that the errors, if any, will show up immediately, and their origin is almost always obvious. 

Testing phases: 

- syntax checking and linting
- unit and integration testing
- testing acceptance

As mentioned already above, these tests are done automatically. And they are always done through a **Pipeline**. Oh, and what is a Pipeline? It is nothing more and nothing less than simply **a series of tasks to be performed automatically one after the other**. 


### 5.2 Continuous Delivery

A "CD" mozaikszóból a "D"-t hagyományosan "Delivery"-nek hívták, de sokan inkább "Deployment"-nek nevezik, ugyanis ez már a valós szerverekre történő telepítések és konfigurálások automatizálásának összefoglaló neve. 

A terminológia maga magasról nézve azzal áll szoros kapcsolatban, hogy az ú.n. kódbázist mikor, milyen környezeteken (dev, test, staging, prod) alkalmazzuk. Magában foglalja viszont a részleteket is. 

A pipeline `.gitlab-ci.yml` konfigurációs fájljában az alapbeállítás szerint a sorrend mindig:

`build` &rarr; `test` &rarr; `deploy`

Amennyiben szükségét érezzük, hogy akár a sorrendet, akár további fázisokat iktassunk be, ezt még a legelső `stage` bejegyzés előtt tudjuk megtenni, pl.:

```
[...]
stages:
- test
- build
- deploy
[...]
```

...

### 5.3 GitLab Runner

A Runnerek programok, amelyek lehetővé teszik a kódok futtatását távoli gépeken. Jelen esetben ezek futtatják az automatizált teszteket. Ezt lehetővé tehetjük Docker-rel vagy akár Kubernetes-el is, tehát olyan konténerekkel, amik már tartalmazzák a `gitlab-runner` programot. 

#### 5.3.1 GitLab Runner típusok

A GitLab GUI-n beállítható felhasználhatóság szempontjából különböztetjük csak meg az egyes típusokat: 

- **Shared**: elérhető minden egyes projekthez a GitLab instance-on belül
- **Group**: elérhető egy group minden projektjén belül
- **Project**: egy bizonyos projekthez vagy projektek halmazához bizonyos előfeltételek megléte esetén

#### 5.3.2 A GitLab Runner Executor

Ez a program határozza meg, hogy a job milyen környezetben fusson le. Ez lehet akár...

- egy VM egy hypervisor-on belül, pl. VirtualBox-on
- Shell
- Remote SSH
- Docker
- Kubernetes
- Egyedi executor

A `gitlab-runner` program nem csak mindenféle csomagkezelőkhöz vagy akár Windowsra is letölthető, hanem még egy nagyon egyszerű bináris formájában is beszerezhető a [GitLab oldaláról](https://docs.gitlab.com/runner/install/linux-manually.html). 

A következőkben először Docker-es környezetet fogunk előkészíteni, beállítani, majd használatba venni. 

### 5.4 Docker környezet előkészítése

Először az egyszerűség kedvéért ugyanazon a VM-en telepítettem a Dockert, mint amin a GitLab szolgáltatás is fut, de ez így elég meredek (hihetetlen, de még az egészet hosztoló laptop is lefagyott), így aztán a második nekifutásnak már egy külön VM-et alkottam ehhez. 

Magát a Dockert telepíthetjük akár a [másik fejezetben](/learning/it/sysadmin/docker/) megismerhető módon, teljesen a statikus fájlból is, ezért azt itt most nem ismertetném külön. A lényeg, hogy a végén a Dockert konténereknek otthont adó VM-ünkön a `docker` parancs működik és a `dockerd` daemon is elindítható:  

```ksh
# docker -v
Docker version 20.10.9, build c2ea9bc

# dockerd
INFO[2022-05-01T12:05:20.222546685+02:00] Starting up                                  
INFO[2022-05-01T12:05:20.226660784+02:00] libcontainerd: started new containerd process  pid=3442
INFO[2022-05-01T12:05:20.226710996+02:00] parsed scheme: "unix"                         module=grpc
[...]
INFO[2022-05-01T12:05:22.001137512+02:00] Daemon has completed initialization          
[...]
```

A GitLab hivatalos doksijában az áll, hogy...

>GitLab Runner uses Docker Engine API v1.25 to talk to the Docker Engine. This means the minimum supported version of Docker on a Linux server is 1.13.0, on Windows Server it needs to be more recent to identify the Windows Server version.

Kétféle Docker környezettel volt eddig lehetőségem babrálni: 

- a Docker és GitLab Runner egy VM-en kerül telepítésre és azok service-ei futnak
- a Dockert futtató VM-en egy `gitlab-runner`-nek előkészített Docker konténer fut és azon belül fut a `gitlab-runner` service. 

Az általam eredetileg követett leírás szerint, és amennyiben jól értem, a 2. megoldás volt a preferált, tehát a GitLab Runner egy Docker konténeren belül hívódik meg, hogy azon belül indítson el futtatási környezete(ke)t. Értelmét viszont én inkább úgy látom mindennek, ha a Docker daemont futtató VM-en, tehát a Docker daemon **mellett** fog futni a `gitlab-runner` program is. De nézzük meg így is és úgy is. 

#### 5.4.1 A `gitlab-runner` program telepítése Dockert futtató VM-re

Tulajdonképpen [ennek a leírásnak az alapján](https://docs.gitlab.com/runner/install/linux-manually.html#using-binary-file) kell végigcsinálnunk ezt. 

1. Töltsük le az egy szem binárist: 

```ksh
# mkdir gitlab-runner

# curl -L --output /opt/gitlab-runner/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 47.4M  100 47.4M    0     0  2288k      0  0:00:21  0:00:21 --:--:-- 5186k

# chmod +x gitlab-runner/gitlab-runner

# ln -s /opt/gitlab-runner/gitlab-runner /usr/bin/gitlab-runner
```

2. Hozzunk létre egy `gitlab-runner` nevű felhasználót:

```ksh
# useradd -c 'GitLab Runner' -m gitlab-runner
```

3. Beállítás és indítás:

```ksh
# gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

# gitlab-runner start
```

#### 5.4.2 A `gitlab-runner` Docker konténer beállítása

A legegyszerűbb mód, ha a hivatalos `gitlab-runner` nevű Docker image-et töltjük le és indítjuk el: 

```ksh
# docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
Unable to find image 'gitlab/gitlab-runner:latest' locally
latest: Pulling from gitlab/gitlab-runner
e0b25ef51634: Pull complete 
a4f898b1037a: Pull complete 
4dbda7899ac1: Pull complete 
Digest: sha256:ebb12390ee4e9521c9122d90e8910c735f76d960dcce57c04a179cc747b9ee78
Status: Downloaded newer image for gitlab/gitlab-runner:latest
c045d4f37d5f19d381ccd003d59d5dee5a35ce63e236c884d10bab10dfb94b76
```

A konténerünk most már fut és készen áll a regisztrációra: 

```ksh
# docker ps
CONTAINER ID   IMAGE                         COMMAND                  CREATED         STATUS         PORTS     NAMES
c045d4f37d5f   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   3 minutes ago   Up 3 minutes             gitlab-runner
```

#### 5.4.3 Docker runner regisztrálása

Az alábbiak egy **Project** típusú runner regisztrálását fedik le. Ha **Shared** vagy **Group** típusút szeretnénk, azt máshol tehetjük meg. Azokkal is fogunk foglalkozni. 

1. A GitLab GUI-n válasszuk ki a projektünket, majd annak **Settings** &rarr; **CI/CD** &rarr; **Runners** oldalán keressük ki a regisztrációs tokent: 

![alt_text](gitlab_-_runner_-_registration_-_token.png)

2. Attól függően, hogy az előzőekben a `gitlab-runner`-t futtató VM-et vagy a `gitlab-runner`-el előkészített konténert csináltuk meg, hívjuk meg a `gitlab-runner register` parancsot: 

- Ha a `gitlab-runner` közvetlenül a VM-en fut: 

```ksh
# gitlab-runner register
[...]
```

- Ha `gitlab-runner`-el előkészített konténerünk van: 

```ksh
# docker exec -it gitlab-runner gitlab-runner register
[...]
```

3. A többi vonatkozó információ mellett adjuk meg a tokent: 

```ksh
Runtime platform                                    arch=amd64 os=linux pid=28 revision=c6bb62f6 version=14.10.0
Running in system-mode.                            
[...]
Enter the GitLab instance URL (for example, https://gitlab.com/):
http://192.168.56.101
Enter the registration token:
GR1348941r61NpHo4aPgaH6su67As
Enter a description for the runner:
[c045d4f37d5f]: docker-runner
Enter tags for the runner (comma-separated):
docker
Enter optional maintenance note for the runner:
[...]
Registering runner... succeeded                     runner=GR1348941r61NpHo4
Enter an executor: parallels, shell, ssh, docker+machine, custom, docker, docker-ssh, virtualbox, docker-ssh+machine, kubernetes:
docker
Enter the default Docker image (for example, ruby:2.7):
alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Ellenőrizhetjük is művünket a konténerben futtatott `gitlab-runner verify` paranccsal. 

**Megjegyzés**: ha korábban mégis a portátirányítással vesződtünk és a tűzfal továbbra is fut a VM(ek)-en, akkor ennél a lépésnél könnyen lehet, hogy `No route to host` hibát kapunk. Állítsuk be megfelelően vagy egész egyszerűen tiltsuk le a tűzfalat, pl.:

```ksh
# systemctl disable firewalld
Removed /etc/systemd/system/multi-user.target.wants/firewalld.service.
Removed /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service.

# systemctl stop firewalld

# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
```

3. Frissítsük a GUI-t és már láthatjuk is a runnerünket és annak online státuszát: 

![alt_text](gitlab_-_runner_-_registration_-_online.png)

#### 5.4.4 GitLab Runner eltakarítása

Egy, már nem használt runner eltávolítása, amennyiben a kapcsolat rendben van: 

```ksh
# gitlab-runner unregister --name rockylab-inventory 
Runtime platform                                    arch=amd64 os=linux pid=1600 revision=f761588f version=14.10.1
Running in system-mode.                            
[...]
Unregistering runner from GitLab succeeded          runner=V_xJB3yz
Updated /etc/gitlab-runner/config.toml             
```

Ha valami félrement volna, a runnert a GUI-n törölve még nem törlődik minden. Itt egy példa (ahogy látható, Docker konténeren belüli `gitlab-runner` esetében) egy kisebb szenvedésre, ahogyan kilistáztam, majd takarítottam, de végül csak sikerült: 

```ksh
# docker exec -it gitlab-runner gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=86 revision=c6bb62f6 version=14.10.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
docker-runner                                       Executor=docker Token=zSmG5Fp-suCu4QE2bfd8 URL=http://192.168.56.101
docker-runner                                       Executor=docker Token=MWrfkstLixEu_nJaxKuU URL=http://192.168.56.101

# docker exec -it gitlab-runner gitlab-runner unregister --all-runners
Runtime platform                                    arch=amd64 os=linux pid=212 revision=c6bb62f6 version=14.10.0
Running in system-mode.
[...]
WARNING: Unregistering all runners                 
ERROR: Unregistering runner from GitLab forbidden   runner=zSmG5Fp-
ERROR: Failed to unregister runner docker-runner   

# docker exec -it gitlab-runner gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=156 revision=c6bb62f6 version=14.10.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
docker-runner                                       Executor=docker Token=zSmG5Fp-suCu4QE2bfd8 URL=http://192.168.56.101

# docker exec -it gitlab-runner gitlab-runner verify --delete
Runtime platform                                    arch=amd64 os=linux pid=312 revision=c6bb62f6 version=14.10.0
Running in system-mode.                            
[...]
ERROR: Verifying runner... is removed               runner=zSmG5Fp-
Updated /etc/gitlab-runner/config.toml

# docker exec -it gitlab-runner gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=323 revision=c6bb62f6 version=14.10.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
```

Ezután újra regisztráltam és már csak egy runner volt, ahogy kell. 

#### 5.4.5 Group és Shared runnerek

A **Group** típusút a GitLab GUI-n kiválasztott group (**Menu** &rarr; **Groups** &rarr; **Your groups**) kitallózása után a **CI/CD** &rarr; **Runners** oldalon, a **Register a group runner** gomb megnyomása után tudjuk elvégezni. A többi már teljesen hasonló a fentiekhez. (Érdemes lehet azért másként nevezni, én jelen esetben `rockysrv2-group` néven neveztem. Ha sikerült, a runner mindenki számára elérhetővé válik.)

A **Shared** típusúra vonatkozó beállításokat a GitLab főoldalán, a **Settings** &rarr; **CI/CD** &rarr; **Continuous Integration and Deployment** &rarr; **Expand** után érhetjük el. 

#### 5.4.6 Saját Docker image GitLab Runnerként való használata

Ide sok jegyzetem lesz, az tuti, de egyelőre talán a legfontosabb, hogy a `pull_policy`-t ennek megfelelően állítsuk be. Ezt nem tudjuk globálisan érvényre juttatni, de az adott runner definíciójának blokkjában igen: 

```ksh
# vi /etc/gitlab-runner/config.toml
[...]
[[runners]]
[...]
  [runners.docker]
    pull_policy = "if-not-present"
[...]
```

Ezzel elkerüljük, hogy a teljesen esélytelen `docker pull` megtörténjen a saját gyártású image-ünkhöz. 

### 5.5 GitLab Runner hibakeresés

#### 5.5.1 Offline runner

Előfordulhat, hogy hónapokon át hibátlanul működik a runnerünk, de aztán elveszíti a kapcsolatot a GitLab szerverrel.  A jelenség pontos okát még nem tudom, de lehet, hogy valamilyen kapcsolódó hálózati átkonfigurálások zavarták össze. 

Elsődleges tünete, hogy egyszerűen offline-nak látszódik a GitLabban: 

![alt_text](gitlab_-_runner_-_offline.png)

A runner oldalán a `gitlab-runner verify` szintén hálózati elérhetetlenségre panaszkodott. 

A megoldás egyszerűen az volt, hogy újra kellett indítani a Docker daemont. 

#### 5.5.2 Elavult runner

Korábban már regisztrált, majd újrakonfigurált GitLab esetében a `gitlab-runner verify --delete` se tud segíteni, mert a program nem tud kapcsolódni már a szerverhez. Ilyenkor ott a parancs kimenetében egyébként is említett konfigfájl, amiből egyszerűen "kiszerkeszthető" a már nem létező runner: 

```ksh
[root@rockysrv2 ~]# gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=1561 revision=f761588f version=14.10.1
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
rockysrv2                                           Executor=docker Token=yvyYmiWHNb9WBESsnzBs URL=http://192.168.56.107
rockysrv2                                           Executor=docker Token=YjhyLykVEAfaJys4R5RJ URL=http://192.168.56.101
[root@rockysrv2 ~]# gitlab-runner verify --delete
Runtime platform                                    arch=amd64 os=linux pid=1566 revision=f761588f version=14.10.1
Running in system-mode.                            
[...]
ERROR: Verifying runner... failed                   runner=yvyYmiWH status=couldn't execute POST against http://192.168.56.107/api/v4/runners/verify: Post "http://192.168.56.107/api/v4/runners/verify": dial tcp 192.168.56.107:80: connect: no route to host
Verifying runner... is alive                        runner=YjhyLykV

[root@rockysrv2 ~]# vi /etc/gitlab-runner/config.toml

[root@rockysrv2 ~]# gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=1580 revision=f761588f version=14.10.1
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
rockysrv2                                           Executor=docker Token=YjhyLykVEAfaJys4R5RJ URL=http://192.168.56.101
[root@rockysrv2 ~]# 
[root@rockysrv2 ~]# 
[root@rockysrv2 ~]# gitlab-runner verify
Runtime platform                                    arch=amd64 os=linux pid=1585 revision=f761588f version=14.10.1
Running in system-mode.                            
[...]
Verifying runner... is alive                        runner=YjhyLykV
```

### 5.6 GitLab Pipeline

A GitLab Pipeline tulajdonképpen a `.gitlab-ci.yml` nevű YAML fájl, ami a prorojekt főkönyvtárában található. Ennek a fájlnak a tartalma alapján dől el, hogy mi, mikor és hogyan fut. Természetesen ez a fájl is, ahogyan minden más a projekt könyvtárában, verziókontrollált. 

#### 5.6.1 Pipeline elkészítése

A runnerrel már felvértezett projektünk főkönyvtárában, ahol elvileg még csak egy `README.md` fájlunk van, kattintsunk a  **Web IDE** gombra annak érdekében, hogy sablonok közül választva, gyorsan létrehozhassunk `.gitlab-ci.yml` fájlt. A sablon ezúttal legyen **Bash** típusú: 

![alt_text](gitlab_-_pipeline_-_create_from_template.png)

Fontos, hogy mindegyik fázishoz adjuk hozzá az előzőekben megadott `docker` tag-et.  
Továbbá, az `image` résznél a `busybox:latest`-et is érdemes megváltoztatni `alpine:latest`-re, hiszen azt adtuk meg "default Docker image"-nek az előzőekben (bár ez nem feltétlenül szükséges, csak itt most gyorsabb): 

```
[...]
image: alpine:latest

before_script:
  - echo "Before script section"
  - echo "For example you might run an update here or install a build dependency"
  - echo "Or perhaps you might print out some debugging details"

after_script:
  - echo "After script section"
  - echo "For example you might do some cleanup here"

build1:
  tags:
    - docker
  stage: build
  script:
    - echo "Do your build here"

test1:
  tags:
    - docker
  stage: test
  script:
    - echo "Do a test here"
    - echo "For example run a test suite"

test2:
  tags:
    - docker
  stage: test
  script:
    - echo "Do another parallel test here"
    - echo "For example run a lint test"

deploy1:
  tags:
    - docker
  stage: deploy
  script:
    - echo "Do your deploy here"
```

Fontos, hogy a `tags` rész pontosan azt tükrözze, amit a runnerünknek beállítottuk, máskülönben azon fog görcsölni a Pipeline, hogy nem talál (`stuck`) olyan runnert, ami a Pipeline-hoz való lenne. 

Ha mindent jól csináltunk, a fájl commitálása után azonnal elindul a pipeline: 

![alt_text](gitlab_-_pipeline_-_firt_run.png)

A `dockerd`-t futtató session-ünkben még az is jól látszik, ahogyan az `image` letöltődik. 

A `.gitlab-ci.yml` szerkesztéséhez a **CI Lint** is hasznos funkció lehet a **CI/CD** &rarr; **Pipelines** részben. 

Amennyiben a "jó úton" járva VM-en belül tartottuk a `gitlab-runner`-t, még a Docker konténer lehúzását és elindulását is úgymond "nyakon csíphetjük". Itt történik meg, hogy végre **láthatjuk, hogy mi folyik a motorháztető alatt**: 

```ksh
# while true ; do date ; docker ps ; echo --- ; sleep 3 ; done
[...]
Mon May  9 18:34:44 CEST 2022
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
---
Mon May  9 18:34:47 CEST 2022
CONTAINER ID   IMAGE          COMMAND                 CREATED                  STATUS                  PORTS     NAMES
6e2f9073aec7   257667c17e33   "gitlab-runner-build"   Less than a second ago   Up Less than a second             runner-yvyymiwh-project-2-concurrent-0-edf1e011ad8656dd-predefined-1
---
Mon May  9 18:34:50 CEST 2022
CONTAINER ID   IMAGE          COMMAND                  CREATED        STATUS                  PORTS     NAMES
7356f0941814   257667c17e33   "gitlab-runner-helpe…"   1 second ago   Up Less than a second             runner-yvyymiwh-project-2-concurrent-0-cache-c33bcaa1fd2c77edfc3893b41966cea8-set-permission-774543a0b0a4dc07
---
Mon May  9 18:34:54 CEST 2022
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
---
Mon May  9 18:34:57 CEST 2022
CONTAINER ID   IMAGE          COMMAND                  CREATED        STATUS                  PORTS     NAMES
bfabf1e478c4   257667c17e33   "gitlab-runner-helpe…"   1 second ago   Up Less than a second             runner-yvyymiwh-project-2-concurrent-0-cache-c33bcaa1fd2c77edfc3893b41966cea8-set-permission-dacd806136a4061b
---
Mon May  9 18:35:00 CEST 2022
CONTAINER ID   IMAGE          COMMAND                 CREATED        STATUS                  PORTS     NAMES
0552974d0f78   257667c17e33   "gitlab-runner-build"   1 second ago   Up Less than a second             runner-yvyymiwh-project-2-concurrent-0-ea1e46bf7d8c6d1b-predefined-1
---
Mon May  9 18:35:03 CEST 2022
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
---
Mon May  9 18:35:06 CEST 2022
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[...]
```

#### 5.6.2 Pipeline változók

Az alábbi egy példa arra, hogy egy stage csak egy bizonyos változó értékének megadásakor fusson. 

Módosítsuk a `.gitlab-ci.yml` fájlunkat az alábbinak megfelelően: 

```
[...]
test2:
  tags:
    - docker
  stage: test
  script:
    - echo "Do another parallel test here"
    - echo "For example run a lint test"
  only: 
    variables: 
      - $TEST_LINUX == "true"
```

Commitálás után a pipeline-unk azonnal futásnak is ered és láthatjuk, hogy a `test2` alapból ki is maradt. 

A **CI/CD** &rarr; **Pipelines** &rarr; **Run pipeline** az a funkció, amit elég sokat fogunk valószínűleg használni. Ezt meghívva, az ott megnyíló felületen adhatjuk meg ezt az extra változót: 

![alt_text](gitlab_-_pipeline_-_variable_-_test_linux.png)

Amennyiben a futáshoz feltétlenül szükséges környezeti változókat is szeretnénk beállítani, azt a **Settings** &rarr; **CI/CD** &rarr; **Variables** részben tehetjük meg. Ilyen lehet pl. az `AWS_ACCESS_KEY_ID`. 

### 5.7 Ansible integráció

Rakjuk össze az egészet valami értelmessé. 

Fontos megjegyezni, hogy **<r>amit itt olvashatsz, az csak egy lehetséges megoldás a sok közül</r>**. **Nincs hivatalos lépéssorozat minderre.** (Én legalábbis eddig még nem találtam találtam ilyet.)

#### 5.7.1 Az Ansible Inventory CI-osítása

Ez egyrészt amiatt fontos, hogy a további deploymenteket végző Ansible playbook-jaink is biztosan az épp aktuális inventory-ból dolgozzanak, másrészt az inventory adminisztrálása is sokkal kényelmesebb, ésszerűbb. 

##### 5.7.1.1 Az inventory előkészítése még a gazdagépen

1. Csináljunk egy új GitLab projektet `ansible-inventory` néven, teljesen üres tartalommal a `rockylab` group alatt. 

2. Bizonyosodjunk meg róla, hogy a repository-t tartalmazó user SSH publikus kulcsa fel van töltve az **SSH Keys** részben. Jelen esetben a teljes agyhalál érdekében a `docker` usert választottam, de ez persze lehet bármi más is. 

3. Készítsük el az első inventory-bejegyzést, majd a "szokásos" módon készítsük el a `master` branch-et (bárhol, mert úgyse ez lesz használva később): 

```ksh
$ git clone git@192.168.56.101:rockylab/ansible-inventory.git
Cloning into 'ansible-inventory'...
[...]
warning: You appear to have cloned an empty repository.

$ cd ansible-inventory/

$ cat > ansible-hosts
[dev]
rockysrv3  ansible_host=192.168.56.103
^D

$ git add ansible-hosts 

$ git commit -m "firs commit adding ansible-hosts"
[master (root-commit) 74a5a60] firs commit adding ansible-hosts
 1 file changed, 2 insertions(+)
 create mode 100644 ansible-hosts

$ git push -u origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 250 bytes | 250.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To 192.168.56.101:rockylab/ansible-inventory.git
 * [new branch]      master -> master
branch 'master' set up to track 'origin/master'.
```

##### 5.7.1.2 Docker image előkészítése

1. Készítsünk elő egy image-et, ami feltétlenül tartalmazza az alábbi csomagokat/programokat: 

- `bash`
- `git`
- `ansible`

Ha nem akarunk teljesen saját image elkészítésével vesződni `scratch`-ből, itt van hozzá egy lehetéges `Dockerfile`: 

```
FROM rockylinux:latest

LABEL maintainer="gjakab@example.com"
RUN dnf install epel-release -y
RUN dnf install ansible git -y
```

2. Építsük fel az image-et: 

```ksh
$ docker build --tag rockyansible:inventory .
[...]
Successfully built a62dfb2b0448
Successfully tagged rockyansible:inventory
```

3. Indítsuk el a konténert, de úgy, hogy a működőképes SSH kulcsot és `known_hosts`-ot is már át tudjuk másolni, amiből pedig majd újragyártjuk az image-ünket: 

```ksh
$ docker run -ti -v /home/docker:/home/docker rockyansible:inventory bash

bash-4.4# cd

bash-4.4# pwd
/root

bash-4.4# mkdir .ssh

bash-4.4# cd .ssh

bash-4.4# cp /home/docker/.ssh/* .

bash-4.4# ls -la
total 28
drwxr-xr-x. 2 root root 4096 May 19 18:55 .
dr-xr-x---. 1 root root 4096 May 19 18:54 ..
-rw-------. 1 root root 2602 May 19 18:55 id_rsa
-rw-r--r--. 1 root root  570 May 19 18:55 id_rsa.pub
-rw-r--r--. 1 root root  176 May 19 18:55 known_hosts

bash-4.4# exit
exit

$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND   CREATED          STATUS                      PORTS     NAMES
94697a99596e   rockyansible:inventory   "bash"    59 seconds ago   Exited (0) 13 seconds ago             kind_mccarthy

$ docker commit kind_mccarthy rockyansible:inventory
sha256:eada5a9cd10956b2972bc3bd1dca2ba82b0a8c293f31d1f91a8e41f57f7e6c73
```

4. Állítsuk be és teszteljük az `ansible` inventory-ját egy fix kötetesre (ezt a `/builds/rockylab` könyvtárat a korábbi próbálkozásaim alapján találtam ki a pipeline-ból kinézve). A változtatásokkal ismét írjuk felül az image-ünket: 

```ksh
$ docker run -ti -v /builds/rockylab:/builds/rockylab rockyansible:inventory bash

bash-4.4# df -hPT
Filesystem                Type     Size  Used Avail Use% Mounted on
overlay                   overlay  9.8G  3.9G  5.5G  42% /
tmpfs                     tmpfs     64M     0   64M   0% /dev
tmpfs                     tmpfs    907M     0  907M   0% /sys/fs/cgroup
shm                       tmpfs     64M     0   64M   0% /dev/shm
/dev/mapper/rootvg-rootlv ext4     9.8G  3.9G  5.5G  42% /builds/rockylab
tmpfs                     tmpfs    907M     0  907M   0% /proc/asound
tmpfs                     tmpfs    907M     0  907M   0% /proc/acpi
tmpfs                     tmpfs    907M     0  907M   0% /proc/scsi
tmpfs                     tmpfs    907M     0  907M   0% /sys/firmware

bash-4.4# cd /builds/rockylab/

bash-4.4# ls -la
total 12
drwxr-xr-x. 2 root root 4096 May 19 20:31 .
drwxr-xr-x. 3 root root 4096 May 19 20:30 ..

bash-4.4# git clone git@192.168.56.101:rockylab/ansible-inventory.git
Cloning into 'ansible-inventory'...
remote: Enumerating objects: 24, done.
remote: Counting objects: 100% (24/24), done.
remote: Compressing objects: 100% (22/22), done.
remote: Total 24 (delta 4), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (24/24), done.
Resolving deltas: 100% (4/4), done.

bash-4.4# ls -la
total 16
drwxr-xr-x. 3 root root 4096 May 19 20:32 .
drwxr-xr-x. 3 root root 4096 May 19 20:30 ..
drwxr-xr-x. 3 root root 4096 May 19 20:32 ansible-inventory

bash-4.4# cd

bash-4.4# pwd
/root

bash-4.4# vi .ansible.cfg
bash: vi: command not found

bash-4.4# cat > .ansible.cfg
[defaults]
host_key_checking = False
inventory         = /builds/rockylab/ansible-inventory/ansible-hosts
^D

bash-4.4# ansible --version
ansible 2.9.27
  config file = /root/.ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3.6/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 3.6.8 (default, Apr 12 2022, 06:55:39) [GCC 8.5.0 20210514 (Red Hat 8.5.0-10)]

bash-4.4# exit
exit

$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND   CREATED          STATUS                      PORTS     NAMES
94697a99596e   rockyansible:inventory   "bash"    59 seconds ago   Exited (0) 13 seconds ago             youthful_hertz

$ docker commit youthful_hertz rockyansible:inventory
sha256:eada5a9cd10956b2961ae2bd1dca2ba82b0a8c293f31d1f91a8e41f57f7e6c73
```

(Igen, még egy `vi` sincs a konténerben, de nem is kell, hiszen beleirányítással is jól lehet egyszerű konfigfájlokat szerkeszteni.)

##### 5.7.1.3 A GitLab Runner előkészítése

1. Csináljunk egy, a fentiekben már ismertetett lépések szerint egy Group Runnert, jelen esetben az `rockyansible:inventory` image-el, mint default image-el. A `tag` és az `executor` itt is a `docker` legyen.

2. Szerkesszük meg a `/etc/gitlab-runner/config.toml` úgy, hogy 2 dologra mindenképpen figyeljünk oda: 

- a `pull_policy` értéke `"if-not-present"` legyen (a saját építésű image-em miatt kellett ez)
- a `volumes` részhez adjuk hozzá a fix kötetet, a `/builds/rockylab`-ot (ez egyelőre, a fejlesztési fázisban tök hasznos lehet)

Példa a fájl teljes tartalmára: 

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "rockylab-group"
  url = "http://192.168.56.101"
  token = "Ewezr1raq5cFzT2VKrui"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    pull_policy = "if-not-present"
    tls_verify = false
    image = "rockyansible:inventory"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/builds/rockylab:/builds/rockylab", "/cache"]
    shm_size = 0
```

##### 5.7.1.4 A GitLab CI/CD konfigurációs fájl előkészítése és futtatás

Készítettem tehát mindehhez egy nagyon egyszerű `.gitlab-ci.yml` fájlt az `ansible-inventory` projekt alá, ami tulajdonképpen nem csinál mást, mint syntax checket végez: 

```
image: rockyansible:inventory

before_script:
  - ansible --version

stages:
  - syntax

syntax_checking:
  tags:
    - docker
  stage: syntax
  script:
    - ansible-inventory --list -y 2>/tmp/errors.log
    - bash -c "if [ -s /tmp/errors.log ]; then cat /tmp/errors.log ; exit 1 ; else echo 'parsed OK' ; fi"
```

A pipeline remélhető eredménye: 

![alt_text](gitlab_-_ansible_-_inventory_-_pipeline.png)

Rákattintva a jobra, valahogy így kell kinéznie az eredménynek: 

![alt_text](gitlab_-_ansible_-_inventory_-_syntax_checking.png)

Kimenet, amennyiben szintaktikai hiba kerül az `ansible-hosts` fájlunkba (egy felesleges `"` karaktert írtam az elejére): 

![alt_text](gitlab_-_ansible_-_inventory_-_syntax_checking_-_failed.png)

Tehát csodálatosan működik!

Ugyancsak fantasztikus élmény, ha folyamatosan monitorozzuk a kis `ansible-hosts` fájlunk tartalmát (vagy már előre belerondítunk) a terminálban, miközben változtatást végzünk rajta a GitLab GUI-n. 

Felhasznált további olvasnivaló: 

- [docs.docker.com/storage/volumes](https://docs.docker.com/storage/volumes/)
- [docs.gitlab.com/...](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)

##### 5.7.1.5 Inventory fájl a gyakorlatban

Életszerűbb az olyan inventory fájl, amiben a mindennapi karbantára előkészített csoportjaink is megtalálhatók: 

- a gépek felsorolása azok elsődleges típusa alapján
- a `dev`, `test`, `prod` stb. kategorizálások
- `maintenance` csoport vagy csoportok, hogy 1-1 feladat csak akkor legyen végrehajtható, ha a gép benne van vagy épp nincs benne valamely ilyen csoportban (pl. az "OS" vagy "bootstrap" deployment csak "maintenance" csoportban lévő gépen legyen lehetséges, de az applikáció/middleware, azaz "baseline" deployment csak olyanon, ami már NINCS benne)

Minderre egy nagyon egyszerű példa a fentiek alapján: 

```
[vms]
rockysrv3  ansible_host=192.168.56.103

[dev]
rockysrv3

[test]

[prod]

[maintenance]
```

#### 5.7.2 Az Ansible Playbook 

Itt válik az egész igazán érdekessé, tehát amikor már valóban automatizálva végeztetjük el feladatok sorát távoli gépeken. 

##### 5.7.2.1 Bevezető

Mint azt [az Ansible-ről szóló förmedvényemből](/learning/it/sysadmin/ansible) is megismerhettük: a távoli gépekhez való kapcsolódáshoz nem kell más, mint az SSH kulcsot biztosítani, hogy az Ansible-t futtató user `root`-ként (hacsak nem rendelkezünk másként a `become_user` direktívával) képes legyen a távoli parancs futtatására jelszó nélkül. 

Ehhez megint csak a fentiekben már gondosan előkészített Docker image `/root/.ssh` könyvtárába is már átmásolt `.pub` fájl tartalmára van szükség, tehát a célgépre helyezzük azt el. 

**További fontos megjegyzés**: SSH-val való kapcsolódáskor egy `Host key` is hozzáíródik a `.ssh/known_hosts` fájlhoz, amire rá is kérdezne az `ssh` parancs. Ezt viszont nem tehetjük meg minden egyes újabb és újabb szerver felbukkanásakor ugyebár, emiatt van az, hogy fent már az Ansible eleve el lett hülyítve azzal, hogy ne törődjön ezzel a `host_key_checking = False` paraméter által. 

Hogy az inventory-nk is afféle "féldinamikus" legyen, a fenti `ansible-inventory` projekt teljes `include`-olása helyett egyszerűen csak `git`-el lehúzatom az épp aktuálisat. Egyelőre bekapcsolva hagyhatjuk még a `/builds/volumes` kötet gazdagépről való felcsatolását a `/etc/gitlab-runner/config.toml` fájlban a szemléltetés, a folyamatos monitorozás kedvéért, de mivel **az egész pipeline egy konténeren belül fut le**, emiatt teljesen rendben van, ha ezt itt most már kikapcsoljuk. Maradhat tehát az eredeti (a régit kikommentelve otthagyhatjuk): 

```
[...]
#    volumes = ["/builds/rockylab:/builds/rockylab", "/cache"]
    volumes = ["/cache"]
[...]
```

##### 5.7.2.2 A Git repository előkészítése

Készítsünk egy újabb groupot a már meglévőn belül, hogy szépen elkülönülhessenek majd az esetleges jövőbeli playbookjaink pl. `ansible-playbooks` néven (ez lehetne valami könyvtár is, de arra nem találtam módot). Azon belül hozzuk létre az új, teljesen üres projektet pl. `baseline` néven. Ez tehát a group nézet minderre ezen a ponton: 

![alt_text](gitlab_-_ansible_-_playbook_-_create.png)

A klónozáshoz való linket másoljuk a vágólapra, saját vagy a fentiek alapján akár a `rockysrv2` gép `docker` usere alól klónozzuk le az üres `git` repót, majd hozzuk létre az első hozzáadandó fájlt, a `playbook.yml`-t, majd `git add [...]`, `git commit [...]`, majd `git push -u origin master`. 

##### 5.7.2.3 A playbook

A teszt elsőre nagyon egyszerű lesz és mindössze egyetlen feladatból fog állni: a beépített `ping` feladatot hajtjuk végre. Ezt az `ansible-playbook playbook.yml` parancsot meghívva tesszük majd meg, így szükségünk lesz a `playbook.yml` fájlra. A "megfuttatandó" szerverlista a korábbiakban már definiált `test` nevű group összes szervere, ami egyelőre az 1 db `rockysrv3`: 

```
- name: "My first play"
  hosts: dev

  tasks:

  - name: "Reachability test"
    ping:
```

##### 5.7.2.4 A CI/CD fájl

Végül szerkesszük meg a `.gitlab-ci.yml` fájlt. Azért "végül", mert amint az már az inventory készítésekor is kiderült: alapértelmezés szerint a változások commitálása után azonnal indul is a pipeline. Ezt természetesen módosíthatjuk később. Az is egy rendkívül praktikus megoldás, ha a playbook bemeneti hostlistát várna ill. még az is megoldható, hogy a `master`-t soha ne lehessen pipeline-ként elindítani, csak a verziózott kiadásokat. 

A végén a `cleanup` stage nem is kötelező, hiszen a konténer, mint mindig, a feladatai végrehajtása után törlődik. Az érdekesség kedvéért azért most még itthagyhatjuk: 

```
stages: 
  - build
  - test
  - cleanup

fetching inventory:
  tags:
    - docker
  stage: build
  script:
    - cd /builds/rockylab && ls -la
    - if [ -d ansible-inventory ] ; then echo "cleaning up old inventory" ; rm -rf ansible-inventory ; fi
    - GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no" git clone git@192.168.56.101:rockylab/ansible-inventory.git

ping test:
  tags:
    - docker
  stage: test
  script:
    - ansible-playbook playbook.yml

cleanup:
  tags:
    - docker
  stage: cleanup
  script:
    - rm -rf /builds/rockylab/*
```

A projekt **CI/CD** &rarr; **Pipelines** oldalán máris elcsíphetjük a futó pipeline-t:

![alt_text](gitlab_-_ansible_-_playbook_-_run.png)

Mindhárom stage-et érdemes végigfutnunk, de talán a `test` stage lehet a legérdekesebb. Láthatóan működött az inventory lehúzása is, hiszen az Ansible tudott miből dolgozni: 

![alt_text](gitlab_-_ansible_-_playbook_-_ping_test.png)

###### 5.7.2.5 Pipeline változók

A gyakorlatban a pipeline meghívásakor, a futtatás elindítása előtt paramétereket szokás annak átadni. A legegyszerűbb példa erre a deploy-olandó host nevének megadása, hostnevek felsorolása vagy egy egész Ansible group meghatározása. 

Ehhez mindössze egy `variables` blokkot kell készítenünk, ahol a változó nevét, annak esetleges alapértelmezett értékét és egy rövid leírást adhatunk meg. 

Mindezzel együtt itt láthatunk egy példát a kicsit még ennél is tovább fejlesztett `.gitlab-ci.yml` fájlunkra: 

```
stages: 
  - build
  - deploy
  - cleanup

variables:
  HOSTS:
    value: ""
    description: "host or host1,host2 or group"

fetching inventory:
  tags:
    - docker
  stage: build
  script:
    - cd /builds/rockylab && ls -la
    - if [ -d ansible-inventory ] ; then echo "cleaning up old inventory" ; rm -r ansible-inventory ; fi
    - GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no" git clone git@192.168.56.101:rockylab/ansible-inventory.git

ping test and deploy:
  tags:
    - docker
  stage: deploy
  script:
    - ansible-playbook playbook.yml --limit $HOSTS

cleanup:
  tags:
    - docker
  stage: cleanup
  script:
    - rm -rf /builds/rockylab/*
```

Az ilyen pipeline futtatása előtt az űrlapunk már valahogy így néz ki: 

![alt_text](gitlab_-_ansible_-_playbook_-_variables_-_run.png)

##### 5.7.2.6 Az Ansible playbook futtatási feltételei

Ezen a ponton már a `playbook.yml` fájlt is továbbfejleszthetjük, hogy pl. ne hajtódjon végre olyan hostokon, amik benne vannak a maintenance groupban: 

```
- name: "My first play"
  hosts: '!maintenance'

  tasks:

  - name: "Reachability test"
    ping:

  - name: "Install Midnight Commander"
    dnf:
      name: mc
      state: present
```

Amennyiben a deploy-olásra meghatározott szervert közben betesszük a fentiekben már ismertetett `maintenance` groupba, úgy annak köszönhetően a job jól láthatóak már átugorja azt: 

![alt_text](gitlab_-_ansible_-_inventory_-_maintenance_matched.png)

##### 5.7.2.7 Ansible role-ok

A gyakorlatban az Ansible role-okat is szokás erősen használni és minden role illetve playbook telis-tele van vezérlési szerkezetekkel (pl. `if` és/vagy `when` feltételekkel), mint pl. itt: 

```
[...]
- name: Add default motd
  template:
    src: motd.j2
    dest: /etc/motd
    owner: root
    group: root
    mode: 0644
  when: 
    - "'jumphosts' in group_names"
    - "'deprecated' not in group_names"
  ignore_errors: "{{ ansible_check_mode }}"
[...]
```

Egyelőre ennyi. Mindezt a végtelenségig lehet még fokozni, továbbfejleszteni, bonyolítani, de ez már inkább az Ansible témaköre, mintsem a GitLab CI/CD-é. 


<a name="6"></a>
## 6. Release Management

>Under translation, sorry

...

A **Release Management** lényege röviden, hogy csak elfogadott és korábban már tesztelt kiadások kerülhessenek telepítésre és azok is visszakövethető formában. 

Itt jön képbe annak a követelménynek a teljesítése is, hogy a `master`-hez tartozó Pipeline-ok ne indulgassanak csak úgy el "lóhalálában", hanem csak tag-elt, ezáltal automatikusan ugyanilyen nevű release-ekbe mentett változat indulhasson el. 

Ehhez be kell vetnünk a GitLab előre definiált változóit, amelyek listáját és rövid ismertetőjét a [docs.gitlab.com](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) oldalon is végignézhetjük. 

### 6.1 GitLab Predefined Variables

A mindenkor elinduló pipeline-ra érvényes környezeti változókat az `env` paranccsal is megkaphatjuk. Ennek beiktatására jó módszer lehet vagy egy külön `stage` definiálása vagy egyszerűen a már meglévő `build` stage-ünket kiegészítjük egy kicsit: 

```
[...]
  script:
    - env
[...]
```

A `.gitlab-ci.yml` fájlban történő szerkesztés és kommitálás után automatikusan elinduló pipeline-on azonnal észrevehetjük az alábbi változót: 

```
CI_PIPELINE_SOURCE=push
```

Egy kézzel indított pipeline esetében ez: 

```
CI_PIPELINE_SOURCE=web
```

Néhány további, egyelőre csak észreveendő változók (és értékük, ha a `master` branch fájlját szerkesztgettük közvetlenül ekkor még): 

```
CI_COMMIT_BRANCH=master
CI_BUILD_REF_NAME=master
CI_COMMIT_REF_SLUG=master
CI_COMMIT_REF_NAME=master
CI_BUILD_REF_SLUG=master
```

### 6.2 Csak kézzel indított pipeline elindíthatóságának kikényszerítése

Magától értetődően, az előzőekben "felfedezett" `CI_PIPELINE_SOURCE` változó használatával tudjuk kikényszeríteni azt, hogy a commit-áláskor ne induljon el a pipeline, hanem csak ha azt mi kézzel, kontrollált keretek között indítjuk. Ezt a `rules` blokkban kell elhelyeznünk, pl. így: 

```
stages: 
  - build
  - deploy
  - cleanup

variables:
  HOSTS:
    value: ""
    description: "host or host1,host2 or group"

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'

fetching inventory:
  tags:
[...]
```

### 6.3 Using `when` for `rules`

As suggested in GitLab's [documentation](https://docs.gitlab.com/ee/ci/yaml/index.html#rulesif), having `rules` and the `if` after that all alone doesn't ensure the job won't be started in the expected condition. Instead, we must ensure the job in the found condition is either `never` or `always` will be initiated. 

E.g.:

```
---
stages:
  - Dry Run Tests
  - Deploy from Schedule
  - Deploy from Adhoc Initiation

[...]

Testing:
  stage: Dry Run Tests
  trigger:
    include:
      - project: 'system-management/common-cicd-config-private-cloud'
        ref: main
        file: 'ansible-role.gitlab-ci.yml'
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'push'"

Scheduled Deploy:
  extends: .common
  stage: Deploy from Schedule
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'schedule'"
    - if: "$CI_PIPELINE_SOURCE == 'push'"
      when: never

Adhoc Deploy:
  extends: .common
  stage: Deploy from Adhoc Initiation
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'web'"
    - if: "$CI_PIPELINE_SOURCE == 'push'"
      when: never
...
```


### 6.4 Tags and Releases

A visszakövethetőség elősegítésére valók a **Release**-ek, amik automatikusan jönnek létre a **Deployments** &rarr; **Releases** menüpont alatt, miután elkészültünk a kiadás tag-elésével. 

A projekt főoldalán a `+` on kattintva kell a **New tag** opciót. Adjunk a kiadásunknak egy nevet (pl. `v1.0.0`), adjunk egy összefoglalót neki, **Release notes** is ajánlott, majd **Create tag**. Fontos, hogy láthatóan ez alapértelmezés szerint az épp aktuális `master` branch-ből fogja a kiadást elkészíteni. 

![alt_text](gitlab_-_release_management_-_releases.png)

Természetesen még tovább kell javítanunk a `.gitlab-ci.yml` fájlunkat, hogy megfeleljen a minimális Release Management követelményeknek, így érdemes ezt az első tag-et és release-t a Repository &rarr; Tags menü alól törölni, és csak akkor újra létrehozni, ha a következő lépésen is már túl vagyunk. 

### 6.5 Csak release-elt változatú pipeline elindíthatóságának kikényszerítése

Ezt többféleképpen is megoldhatjuk, de maradok egy egyszerű megoldásnál, amikor is a `CI_COMMIT_REF_NAME` változót használjuk ki. Ennek értéke a **Run pipeline** űrlapon a bármi más (a változók) megadása előtt, a fenti példánál maradva, a `v1.0.0` tag-et kiválasztva a tag elnevezése lesz, amit jól kihasználhatunk. 

Engedjünk tehát neki bármilyen értéket, csak a `master`-t ne: 

```
[...]
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_REF_NAME != "master"'
[...]
```

Ennek commit-álása után készítsük el az új tag-et és az alapján az új release-t. Teszteléskor máris kiderül, hogy a `master`-t alapértelmezetten hagyva a pipeline-unk már nem hajlandó elindulni, a verziózott változat viszont igen: 

![alt_text](gitlab_-_release_management_-_workflow_rules.png)

